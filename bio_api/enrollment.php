<?php
include './validate.php';
$_POST = json_decode(file_get_contents('php://input'), true);

if ((isset($_POST['employeeID']) && $_POST['employeeID'] !== '') && (isset($_POST['fingerPrint']) && $_POST['fingerPrint'] !== '')) {
    $emp_ID = sanitize($_POST['employeeID']);
    $fingerPrint = sanitize($_POST['fingerPrint']);
    enrollUser($emp_ID, $fingerPrint, $my_database, $my_user, $my_host);
}

function enrollUser($emp_ID, $fingerPrint, $my_database, $my_user, $my_host)
{
    $query = " Select * from [EmpTbl] where [print_fin] IS NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
    $user = Scriptrunnercous($my_database, $my_user, $my_host, $query);

    if (is_array($user) && count($user)) {
        $query = "Update [EmpTbl] set [print_fin] = '$fingerPrint', [capture_date] = GETDATE() where [EmpID] = '$emp_ID'  ";
        ScriptRunnerUD($my_database, $my_user, $my_host, $query);
        $query = " Select [EmpID],[SName],[ONames],[FName],[Department],[StfImg] from [EmpTbl] where [print_fin] IS NOT NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
        $user_updated = Scriptrunnercous($my_database, $my_user, $my_host, $query);
        if(empty($user_updated['StfImg']) ){
            $user_updated['StfImg'] = 'pfimg/def_bio.jpeg';
        }
        echo json_encode([
            'success' => true,
            'message' => 'User registered successfully',
            'user' => $user_updated,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'User not active/User already registered. Kindly contact the admin.',
        ]);
    }

}

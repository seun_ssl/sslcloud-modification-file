<?php
include './validate.php';
$_POST = json_decode(file_get_contents('php://input'), true);

if ((isset($_GET['all']) && $_GET['all'] == true)) {
    getAllusers($my_database, $my_user, $my_host);
} elseif ((isset($_GET['to']) && $_GET['to'] !== '') && (isset($_GET['from']) && $_GET['from'] !== '') && isset($_POST['employeeID'])) {

    $to = explode(' ', sanitize($_GET['to']))[0];
    $from = explode(' ', sanitize($_GET['from']))[0];
    $empID = sanitize($_POST['employeeID']);

    getUserActivity($my_database, $my_user, $my_host, $to, $from, $empID);

} else {
    echo json_encode([
        'success' => false,
        'message' => 'Something went wrong',
        'users' => [],
    ]);

}

function getAllusers($my_database, $my_user, $my_host)
{
    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[StfImg] from  [EmpTbl] where [EmpStatus] = 'Active' and [print_fin] IS NOT NULL";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($users) {
        $new_users = [];
        foreach ($users as $user) {
            if (empty($user['StfImg'])) {
                $user['StfImg'] = 'pfimg/def_bio.jpeg';
            }
            $new_users[] = $user;
        }
        echo json_encode([
            'success' => true,
            'message' => 'Success',
            'users' => $new_users,
        ]);

    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Kindly register/enroll users.',
            'users' => [],
        ]);
    }

}

function getUserActivity($my_database, $my_user, $my_host, $to, $from, $empID)
{
    $query = "Select * from [EmpTbl] where [EmpStatus] = 'Active' and [EmpID] = '$empID' and [print_fin] is not null";
    $HashKey = ScriptRunner($my_database, $my_user, $my_host, $query, 'HashKey');
    $query = "Select fa.RecDate, fa.TimeIn, fa.TimeOut, et.EmpID,et.HashKey,et.SName ,et.ONames,et.FName,et.Department,et.StfImg from [Fin_Attend] fa Inner Join [EmpTbl] et on et.HashKey = fa.EmpID where  fa.[EmpID] = '$HashKey' and fa.RecDate BETWEEN '$from' AND '$to' order by fa.RecDate desc ";

    $userData = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($userData) {
        echo json_encode([
            "success" => true,
            "message" => 'Success',
            "users" => $userData,
        ]);

    } else {
        echo json_encode([
            "success" => false,
            "message" => "No data found.",
            "users" => [],
        ]);

    }

}

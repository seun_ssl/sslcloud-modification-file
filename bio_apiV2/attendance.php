<?php
include './validate.php';

$_POST =  json_decode(file_get_contents('php://input'), true);

if ((isset($_GET['to']) && $_GET['to'] !== '') && (isset($_GET['from']) && $_GET['from'] !== '')) {

    $to = explode(' ', sanitize($_GET['to']))[0];
    $from = explode(' ', sanitize($_GET['from']))[0];
    getAttendance($to, $from, $my_database, $my_user, $my_host);
} elseif ((isset($_POST['attendance_list']) && !empty($_POST['attendance_list']))) {
    $attendance_list = $_POST['attendance_list'];

    foreach ($attendance_list as $list) {

        $recdate = $list['recdate'];
        $time = $list['time'];
        $empID = $list['emp_id'];
        $id = $list['id'];
        $type = $list['type'];
        $Branch = $list['branch'];

        clockAttendance($recdate, $time, $empID, $id, $type, $Branch, $my_database, $my_user, $my_host);
    }
}



function getAttendance($to, $from, $my_database, $my_user, $my_host)
{
    // $query = "Select fa.RecDate, fa.TimeIn, fa.TimeOut, et.* from [Fin_Attend] fa Inner Join [EmpTbl] et on et.HashKey = fa.EmpID where  fa.RecDate BETWEEN '$from' AND '$to' order by fa.RecDate desc ";
    $query = "Select  CONVERT(varchar, fa.RecDate, 23) as RecDate, CONVERT(varchar, fa.TimeIn, 108) as TimeIn,BranchIn, CONVERT(varchar, fa.TimeOut, 108) as TimeOut, BranchOut, et.[EmpID],et.[SName],et.[ONames],et.[FName],et.[Department],et.[StfImg],et.[prin_token]  from [EmpTbl] et  left Join  [Fin_Attend] fa on et.HashKey = fa.EmpID where et.prin_token is not null
               and fa.RecDate BETWEEN '$from' AND '$to'";
    $userData = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($userData) {
        echo json_encode([
            "success" => true,
            "message" => 'Success',
            "users" => $userData
        ]);
    } else {
        echo json_encode([
            "success" => true,
            "message" => "No data found.",
            "users" => []
        ]);
    }
}



function clockAttendance($recdate, $time, $empID, $id, $type, $Branch, $my_database, $my_user, $my_host)
{
    // get empId
    $query = "Select * from [EmpTbl] where [EmpStatus] = 'Active' and [EmpID] = '$empID' 
    and [print_fin] is not null";
    $empdata = ScriptRunnercous($my_database, $my_user, $my_host, $query);

    // var_dump($query);
    // var_dump($empdata);

    if (is_null($empdata)) {
        echo json_encode([
            'success' => false,
            'message' => 'User not Enrolled/Active',
            'user' => ["emp_id" => $empID, "id" => $id],
        ]);
        // die;
    } else {
        // find record in DB
        $query = "Select * from [Fin_Attend] where EmpID='" . $empdata['HashKey'] . "' and RecDate='" . $recdate . "' and Status<>'D'";
        $attendata = ScriptRunnercous($my_database, $my_user, $my_host, $query);

        if (is_null($attendata)) {
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/

            if ($type === 'In') {

                $Exempt = 0;
                $RecType = 'o';
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . $time . "Attendance" . $empID;
                $HashKey = md5($UniqueKey);
                $query = "INSERT into [Fin_Attend] ([EmpID],[RecDate],[TimeIn],[BranchIn],[RecType],[Status],[AddedBy],[AddedDate],[HashKey],[OverTimeAmount],[LatenessAmount], [Exempt]) VALUES ('" . $empdata['HashKey'] . "',
                '" . $recdate . "',
                '" . $time . "',
                '" . $Branch . "',
                '" . $RecType . "',
                'N','biometric_dev',GetDate(),'" . $HashKey . "', '0.00', '0.00', '" . $Exempt . "')";
                //echo $Script;
                ScriptRunnerUD($my_database, $my_user, $my_host, $query);

                $query = "Select  CONVERT(varchar, fa.RecDate, 23) as RecDate,BranchIn,BranchOut, CONVERT(varchar, fa.TimeIn, 108) as TimeIn, CONVERT(varchar, fa.TimeOut, 108) as TimeOut, et.[EmpID],et.[SName],et.[ONames],et.[FName],et.[Department],
            et.[print_fin],
            et.[print_fin2],
            et.[print_fin3],
            et.[print_fin4],
            et.[StfImg],et.[prin_token]  from [EmpTbl] et  left Join  [Fin_Attend] fa on et.HashKey = fa.EmpID where et.prin_token is not null
                   and fa.RecDate BETWEEN '$recdate' AND '$recdate'";
                $today_attendance = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
                echo json_encode([
                    'success' => true,
                    'message' => 'Successfully Clocked In',
                    'user' => ["emp_id" => $empID, "id" => $id],
                    // 'today_attendance' => $today_attendance
                ]);
            } else {

                echo json_encode([
                    'success' => false,
                    'message' => 'User needs to clock in before clock out ',
                    'user' => ["emp_id" => $empID, "id" => $id],
                ]);
            }
        } elseif (is_array($attendata) && $attendata['TimeIn'] !== '' && is_null($attendata['TimeOut'])  &&  $type === 'Out') {

            $query = "Update [Fin_Attend] set 
            [TimeOut] = '" . $time . "',
            [BranchOut] = '" . $Branch . "'
             where EmpID = '" . $empdata['HashKey'] . "' and [Hashkey] ='" . $attendata['HashKey'] . "' ";
            ScriptRunnerUD($my_database, $my_user, $my_host, $query);
            $query = "Select  CONVERT(varchar, fa.RecDate, 23) as RecDate, CONVERT(varchar, fa.TimeIn, 108) as TimeIn, BranchIn,BranchOut,CONVERT(varchar, fa.TimeOut, 108) as TimeOut, et.[EmpID],et.[SName],et.[ONames],et.[FName],et.[Department],
            et.[print_fin],
            et.[print_fin2],
            et.[print_fin3],
            et.[print_fin4],
            et.[StfImg],et.[prin_token]  from [EmpTbl] et  left Join  [Fin_Attend] fa on et.HashKey = fa.EmpID where et.prin_token is not null
                   and fa.RecDate BETWEEN '$recdate' AND '$recdate'";
            $today_attendance = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
            echo json_encode([
                'success' => true,
                'message' => 'Successfully Clocked Out',
                'user' => ["emp_id" => $empID, "id" => $id],
                // 'today_attendance' => $today_attendance
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Action already performed/user not active/registered',
            ]);
        }
    }
}

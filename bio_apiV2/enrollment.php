<?php
include './validate.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if ((isset($_POST['emp_id']) && $_POST['emp_id'] !== '') && (isset($_POST['img_print']) && $_POST['img_print'] !== '')) {
    $emp_ID = sanitize($_POST['emp_id']);
    $fingerPrint =  sanitize($_POST['img_print']);
    $fingerPrint2 = sanitize($_POST['img_print2']);
    $fingerPrint3 = sanitize($_POST['img_print3']);
    $fingerPrint4 = sanitize($_POST['img_print4']);

    $fingerPrint2 = empty($fingerPrint2) ? $fingerPrint2 = null : $fingerPrint2 = $fingerPrint2;
    $fingerPrint3 = empty($fingerPrint3) ? $fingerPrint3 = null : $fingerPrint3 = $fingerPrint3;
    $fingerPrint4 = empty($fingerPrint4) ? $fingerPrint4 = null : $fingerPrint4 = $fingerPrint4;



    enrollUser($emp_ID, $fingerPrint, $fingerPrint2, $fingerPrint3, $fingerPrint4, $my_database, $my_user, $my_host);
}

function enrollUser($emp_ID, $fingerPrint, $fingerPrint2, $fingerPrint3, $fingerPrint4, $my_database, $my_user, $my_host)
{
    $query = " Select * from [EmpTbl] where [print_fin] IS NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
    $user = Scriptrunnercous($my_database, $my_user, $my_host, $query);


    if (is_array($user) && count($user)) {
        $token = generateUnique4DigitCode($my_database, $my_user, $my_host);

        $query = "UPDATE [EmpTbl] SET 
         [print_fin] = '$fingerPrint',";

        // Dynamically append based on the conditions
        if ($fingerPrint2) {
            $query .= "[print_fin2] = '$fingerPrint2',";
        } else {
            $query .= "[print_fin2] = NULL,";
        }

        if ($fingerPrint3) {
            $query .= "[print_fin3] = '$fingerPrint3',";
        } else {
            $query .= "[print_fin3] = NULL,";
        }

        if ($fingerPrint4) {
            $query .= "[print_fin4] = '$fingerPrint4',";
        } else {
            $query .= "[print_fin4] = NULL,";
        }

        // Complete the query by adding other static fields
        $query .= "[capture_date] = GETDATE(), [prin_token] = '$token' WHERE [EmpID] = '$emp_ID';";

        ScriptRunnerUD($my_database, $my_user, $my_host, $query);
        $query = " Select [EmpID],[SName],[ONames],[FName],[Department],[Email],[prin_token],[StfImg] from [EmpTbl] where [print_fin] IS NOT NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
        $user_updated = Scriptrunnercous($my_database, $my_user, $my_host, $query);

        sendToken($my_database, $my_user, $my_host, $user_updated);
        if (empty($user_updated['StfImg'])) {
            $user_updated['StfImg'] = 'pfimg/def_bio.jpeg';
        }
        echo json_encode([
            'success' => true,
            'message' => 'User enrolled successfully',
            'user' => $user_updated,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'User not active/User already registered. Kindly contact the admin.',
        ]);
    }
}


function sendToken($my_database, $my_user, $my_host, $user_updated)
{

    // send mail to level1

    $msg = "Dear #Name#,<br /> This is to inform you that your finger print was successfully captured. Below is your token please do not share this token with others. <br /> Token #token#";
    $Subj = "Biometric Notification (Enrollment)";
    $msg = str_replace('#Name#', "{$user_updated['SName']} {$user_updated['FName']} {$user_updated['ONames']}", $msg);
    $msg = str_replace('#token#', "{$user_updated['prin_token']}", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $user_updated['Email'];

    $query = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','Biometrics'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($my_database, $my_user, $my_host, $query);







    //Get Header and Footer

    $Script = "Select MMsg from MailTemp where HashKey='HEADER'";
    $MailHeader = ScriptRunner($my_database, $my_user, $my_host, $Script, "MMsg") . "<font color='#333333' face='Verdana, Arial, Helvetica, sans-serif' size='2'>";

    //Format MailHeader to have full details
    $Script = "Select SetValue, SetValue2, SetValue3 from settings where Setting='CompLogo'";
    $CompLogo = '<img src="' . ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue") . '" width="' . ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue2") . '" height="' . ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue3") . '">';

    $Script = "Select SetValue from settings where Setting='CompName'";
    $CompName = ScriptRunner(
        $my_database,
        $my_user,
        $my_host,
        $Script,
        "SetValue"
    );

    $Script = "Select SetValue from settings where Setting='CompAddress'";
    $CompAddress = ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue");

    $Script = "Select SetValue from settings where Setting='CompTel'";
    $CompTel = ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue");

    $Script = "Select SetValue from settings where Setting='CompEmail'";
    $CompEmail = ScriptRunner(
        $my_database,
        $my_user,
        $my_host,
        $Script,
        "SetValue"
    );

    $Script = "Select SetValue from settings where Setting='CompTel'";
    $CompTel = ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue");

    $Script = "Select SetValue from settings where Setting='CompRC'";
    $CompRC = ScriptRunner($my_database, $my_user, $my_host, $Script, "SetValue");


    //mAIL sENDING mACHINE pARAMETERS
    //	$_SESSION["StkTck"."MailSendType"]=ScriptRunner("Select SetValue from settings where Setting='SMTP'","SetValue");
    //GET MAIL SENDING CREDENTIALS FROM DATABASE
    $key = 'Onyema.Amaobi.Jm';
    $SMTP = ScriptRunner($my_database, $my_user, $my_host, "Select SetValue from settings where Setting='SMTP'", "SetValue");
    $SMTPAuth = ScriptRunner($my_database, $my_user, $my_host, "Select SetValue from settings where Setting='SMTPAuth'", "SetValue");
    $SMTPFrom = ScriptRunner($my_database, $my_user, $my_host, "Select SetValue from settings where Setting='SMTPFrom'", "SetValue");
    $SMTPPort = ScriptRunner($my_database, $my_user, $my_host, "Select SetValue from settings where Setting='SMTPort'", "SetValue");
    $SMTPEmail = ScriptRunner($my_database, $my_user, $my_host, "Select SetValue from settings where Setting='SMTPEmail'", "SetValue");
    $SMTPUSERNAME = "emailapikey";
    // $SMTPwd = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(ScriptRunner($my_database, $my_user, $my_host, "Select SetValue18 from settings where Setting='SMTPwd'", "SetValue18")), MCRYPT_MODE_CBC, md5(md5($key))), "\0");


    $SMTPwd =  safe_b64decode(ScriptRunner($my_database, $my_user, $my_host, "Select SetValue18 from settings where Setting='SMTPwd'", "SetValue18"));
    //Make changes required with values retrived and passed
    $MailHeader = str_replace('#CompLogo#', $CompLogo, $MailHeader);
    $MailHeader = str_replace('#CompName#', $CompName, $MailHeader);
    //$MailHeader=str_replace('#BranchName#','',$MailHeader);
    $MailHeader = str_replace('#CompAddress#', $CompAddress, $MailHeader);
    $MailHeader = str_replace('#CompTel#', $CompTel, $MailHeader);
    $MailHeader = str_replace('#CompEmail#', $CompEmail, $MailHeader);
    $MailHeader = str_replace('#CompRC#', $CompRC, $MailHeader);


    $Script = "Select MMsg from MailTemp where HashKey='FOOTER'";
    $MailFooter = "</font>" . ScriptRunner($my_database, $my_user, $my_host, $Script, "MMsg");






    //Get the PHP Mailer

    require("../phpmailer/class.phpmailer_G.php");

    // include '../css/myscripts.php';









    //Checks that any mail that was not fired within the last 3 minutes should now be reset and resent
    $Script = "Update MailQueue set Status='U' where Status='P' and DATEDIFF(MINUTE,AddedDate,GETDATE()) > 5 and Subject<>'' and Send_to<>''";
    ScriptRunnerUD($my_database, $my_user, $my_host, $Script, "Ct");

    //Check if there are pending mails
    $Script = "Select Count(*) as Ct from MailQueue where (Status='N' or Status='U') and TrialCnt <= 5";
    $kk = ScriptRunner($my_database, $my_user, $my_host, $Script, "Ct");

    if ($kk > 0) {
        $kk = 0;
        $dbOpen2 = ("Select * from MailQueue where (Status='N' or Status='U') and TrialCnt <= 5 and (SendTime<=GetDate() or SendTime is NUll)");

        $connectionInfo2 =
            array("Database" => $my_database, "UID" => $my_user, "PWD" => PASSWORD);
        $conn2 = sqlsrv_connect($my_host, $connectionInfo2);
        if ($conn2 === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        $result2 = sqlsrv_query($conn2, $dbOpen2);
        if ($result2 === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $kk = $kk + 1;

            if ($row2['ID'] > 0) {
                $Script = "Update MailQueue set [Status]='P' where ID=" . $row2['ID'];
                ScriptRunnerUD($my_database, $my_user, $my_host, $Script, "Ct");
            }

            $Script = "Select (TE.FName+' '+TE.SName) as Nm from EmpTbl TE where TE.Email='" . $row2['Send_to'] . "'";
            $Nm = ScriptRunner($my_database, $my_user, $my_host, $Script, "Nm");

            $subject = $row2['Subject'];
            $message = str_replace(
                "#Name#",
                $Nm,
                $row2['Body']
            );
            $to = $row2['Send_to'];
            $attch = $row2['Attachment'];


            $BranchName = "";
            $Script = "Select OName from BrhMasters where HashKey=(Select top 1 BranchID from EmpTbl where Email='" . $to . "' and BranchID is not NULL) and Status<>'D'";
            $BranchName = ScriptRunner($my_database, $my_user, $my_host, $Script, "OName");


            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = $SMTP; //"10.10.1.24"; //"200.200.200.24"; //"smtp.gmail.com";
            $mail->From = $SMTPEmail;
            $mail->FromName  = $SMTPFrom; //"SSLCloud Services";
            $mail->AddAddress($to);
            if ($attch) {

                $mail->addAttachment("$attch");
            }

            $mail->SMTPAuth = $SMTPAuth; //"false"; // Unset by Joseph - 3rd July 2014
            $mail->Username = $SMTPUSERNAME;
            $mail->Password = $SMTPwd; //"Partner@123"; // Unset by Joseph - 3rd July 2014
            $mail->Port = $SMTPPort; //"587"; //"465"; //"587"; // Unset by Joseph - 3rd July 2014 Google 465
            $mail->IsHTML(true);

            $mail->Subject = $subject;
            //$mail->Body = $_SESSION["StkTck"."MailHeader"].$message.$_SESSION["StkTck"."MailFooter"];


            $mail->Body = str_replace('#BranchName#', $BranchName, $MailHeader) . $message . $MailFooter;
            $mail->WordWrap = 50;
            // $mail->SMTPDebug = 2;

            if (!$mail->Send()) {
                // print_r("here");
                // print_r(" $mail->ErrorInfo");
                $Script = "Update MailQueue set [Status]='U', Updatedby='MailBot', UpdatedDate=GetDate(), TrialCnt=(TrialCnt+1) where ID=" . $row2['ID'];
                ScriptRunnerUD($my_database, $my_user, $my_host, $Script, "Ct");
            } else {
                $Script = "Update MailQueue set [Status]='S', Authby='MailBot', AuthDate=GetDate(), TrialCnt=(TrialCnt+1) where ID=" . $row2['ID'];
                ScriptRunnerUD($my_database, $my_user, $my_host, $Script, "Ct");
            }
        }
        //	$query2 = "FLUSH PRIVILEGES"; mssql_close($dbhandle2);
    } else {
        // echo "No Mail";
    }
}

function generateUnique4DigitCode($my_database, $my_user, $my_host)
{
    do {
        $code = sprintf("%04d", mt_rand(1000, 9999)); // Generate a random 4-digit number
    } while (!isUnique($my_database, $my_user, $my_host, $code)); // Check if the code is unique

    return $code;
}

function isUnique($my_database, $my_user, $my_host, $code)
{

    $query = " Select [EmpID],[prin_token] from [EmpTbl] where [print_fin] IS NOT NULL and [EmpStatus] = 'Active' and [prin_token] = '$code' ";
    $user = Scriptrunnercous($my_database, $my_user, $my_host, $query);

    if ($user) {
        return false;
    } else {

        return true;
    }
}

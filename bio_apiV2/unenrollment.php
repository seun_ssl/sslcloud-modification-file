<?php
include './validate.php';
$_POST = json_decode(file_get_contents('php://input'), true);

if ((isset($_POST['emp_id']) && $_POST['emp_id'] !== '')) {
    $emp_ID = sanitize($_POST['emp_id']);

    unEnrollUser($emp_ID, $my_database, $my_user, $my_host);
}

function unEnrollUser($emp_ID, $my_database, $my_user, $my_host)
{
    $query = " Select * from [EmpTbl] where [print_fin] IS NOT NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
    $user = Scriptrunnercous($my_database, $my_user, $my_host, $query);

    if (is_array($user) && count($user)) {

        $query = "Update [EmpTbl] set 
        [print_fin] = NULL,
        [print_fin2] = NULL,
        [print_fin3] = NULL,
        [print_fin4] = NULL,
         [prin_token] = NULL where [EmpID] = '$emp_ID'  ";
        ScriptRunnerUD($my_database, $my_user, $my_host, $query);
        $query = " Select [EmpID],[SName],[ONames],[FName],[Department],[Email],[prin_token],[StfImg] from [EmpTbl] where [print_fin] IS NULL and [EmpStatus] = 'Active' and [EmpID] = '$emp_ID' ";
        $user_updated = Scriptrunnercous($my_database, $my_user, $my_host, $query);

        if (empty($user_updated['StfImg'])) {
            $user_updated['StfImg'] = 'pfimg/def_bio.jpeg';
        }
        echo json_encode([
            'success' => true,
            'message' => 'User unenrolled successfully',
            'user' => $user_updated,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'User not active/User already registered. Kindly contact the admin.',
        ]);
    }
}

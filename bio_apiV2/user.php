<?php
include './validate.php';
$_POST = json_decode(file_get_contents('php://input'), true);

// var_dump($_GET);




function getAllusers($my_database, $my_user, $my_host)
{
    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],
    [print_fin],
    [print_fin2],
    [print_fin3],
    [print_fin4],
    [StfImg] from  [EmpTbl] where [EmpStatus] = 'Active'";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($users) {
        $new_users = [];
        foreach ($users as $user) {
            if (empty($user['StfImg'])) {
                $user['StfImg'] = 'pfimg/def_bio.jpeg';
            }

            if ($user['EmpID']  !== "000000") {

                $new_users[] = $user;
            }
        }
        echo json_encode([
            'success' => true,
            'message' => 'Success',
            'users' => $new_users,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Kindly register/enroll users.',
            'users' => [],
        ]);
    }
}

function getAllUnCapturedUser($my_database, $my_user, $my_host)
{
    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[print_fin], [print_fin2],
    [print_fin3],
    [print_fin4],[StfImg] from  [EmpTbl] where [EmpStatus] = 'Active' and [print_fin] IS NULL";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($users) {
        $new_users = [];
        foreach ($users as $user) {
            if (empty($user['StfImg'])) {
                $user['StfImg'] = 'pfimg/def_bio.jpeg';
            }


            if ($user['EmpID']  !== "000000") {

                $new_users[] = $user;
            }
        }
        echo json_encode([
            'success' => true,
            'message' => 'Success',
            'users' => $new_users,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Kindly register/enroll users.',
            'users' => [],
        ]);
    }
}

function getAllCapturedUser($my_database, $my_user, $my_host)
{
    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[print_fin], [print_fin2],
    [print_fin3],
    [print_fin4],[StfImg],[prin_token] from  [EmpTbl] where [EmpStatus] = 'Active' and [print_fin] IS NOT NULL";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($users) {
        $new_users = [];
        foreach ($users as $user) {
            if (empty($user['StfImg'])) {
                $user['StfImg'] = 'pfimg/def_bio.jpeg';
            }
            if ($user['EmpID']  !== "000000") {

                $new_users[] = $user;
            }
        }
        echo json_encode([
            'success' => true,
            'message' => 'Success',
            'users' => $new_users,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Kindly register/enroll users.',
            'users' => [],
        ]);
    }
}

function getUserActivity($my_database, $my_user, $my_host, $to, $from, $empID)
{
    $query = "Select * from [EmpTbl] where [EmpStatus] = 'Active' and [EmpID] = '$empID' and [print_fin] is not null";
    $HashKey = ScriptRunner($my_database, $my_user, $my_host, $query, 'HashKey');
    $query = "Select fa.RecDate, fa.TimeIn, fa.TimeOut, et.EmpID,et.HashKey,et.SName ,et.ONames,et.FName,et.Department,et.[print_fin],et.StfImg from [Fin_Attend] fa Inner Join [EmpTbl] et on et.HashKey = fa.EmpID where  fa.[EmpID] = '$HashKey' and fa.RecDate BETWEEN '$from' AND '$to' order by fa.RecDate desc ";

    $userData = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($userData) {
        echo json_encode([
            "success" => true,
            "message" => 'Success',
            "users" => $userData,
        ]);
    } else {
        echo json_encode([
            "success" => false,
            "message" => "No data found.",
            "users" => [],
        ]);
    }
}


function getOneuser($my_database, $my_user, $my_host, $EmpID)
{
    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[print_fin], [print_fin2],
    [print_fin3],
    [print_fin4],[StfImg] from  [EmpTbl] where [EmpStatus] = 'Active' and [EmpID] = '$EmpID'";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($users) {
        $new_users = [];
        foreach ($users as $user) {
            if (empty($user['StfImg'])) {
                $user['StfImg'] = 'pfimg/def_bio.jpeg';
            }

            if ($user['EmpID']  !== "000000") {

                $new_users[] = $user;
            }
        }
        echo json_encode([
            'success' => true,
            'message' => 'Success',
            'users' => $new_users,
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Kindly register/enroll users.',
            'users' => [],
        ]);
    }
}


function getUserCount($my_database, $my_user, $my_host)
{

    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],
    [print_fin],
    [print_fin2],
    [print_fin3],
    [print_fin4],
    [StfImg] from  [EmpTbl] where [EmpStatus] = 'Active'";
    $users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);


    if ($users) {
        $users = count($users);
    } else {
        $users = 0;
    }

    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[print_fin], [print_fin2],
    [print_fin3],
    [print_fin4],[StfImg] from  [EmpTbl] where [EmpStatus] = 'Active' and [print_fin] IS NULL";
    $uncaptured_users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);
    if ($uncaptured_users) {
        $uncaptured_users = count($uncaptured_users);
    } else {
        $uncaptured_users = 0;
    }

    $query = "Select [EmpID],[SName],[ONames],[FName],[Department],[print_fin], [print_fin2],
    [print_fin3],
    [print_fin4],[StfImg],[prin_token] from  [EmpTbl] where [EmpStatus] = 'Active' and [print_fin] IS NOT NULL";
    $captured_users = ScriptRunnerAll($my_database, $my_user, $my_host, $query);

    if ($captured_users) {
        $captured_users = count($captured_users);
    } else {
        $captured_users = 0;
    }
    $curren = date('Y-m-d');

    $query = "Select * from [Fin_Attend] where [RecDate] = '$curren'";

    $current_count = ScriptRunnerAll($my_database, $my_user, $my_host, $query);

    if ($current_count) {
        $current_count = count($current_count);
    } else {
        $current_count = 0;
    }

    echo json_encode([
        'success' => true,
        'message' => 'Success',
        'users' => [
            'total_users' => $users,
            'uncaptured_users' => $uncaptured_users,
            'captured_users' => $captured_users,
            'current_count' => $current_count,
        ],
    ]);
}



if ((isset($_GET['all']) && $_GET['all'] === 'true')) {
    getAllusers($my_database, $my_user, $my_host);
} elseif ((isset($_GET['EmpID']) && $_GET['EmpID'] !== '')) {

    $EmpID = sanitize($_GET['EmpID']);
    getOneuser($my_database, $my_user, $my_host, $EmpID);
} elseif ((isset($_GET['captured']) && $_GET['captured'] === 'false')) {
    getAllUnCapturedUser($my_database, $my_user, $my_host);
} elseif ((isset($_GET['captured']) && $_GET['captured'] === 'true')) {
    getAllCapturedUser($my_database, $my_user, $my_host);
} elseif ((isset($_GET['to']) && $_GET['to'] !== '') && (isset($_GET['from']) && $_GET['from'] !== '') && isset($_POST['employeeID'])) {

    $to = explode(' ', sanitize($_GET['to']))[0];
    $from = explode(' ', sanitize($_GET['from']))[0];
    $empID = sanitize($_POST['employeeID']);

    getUserActivity($my_database, $my_user, $my_host, $to, $from, $empID);
} else if ((isset($_GET['count']) && $_GET['count'] === 'true')) {

    getUserCount($my_database, $my_user, $my_host);
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Something went wrong',
        'users' => [],
    ]);
}

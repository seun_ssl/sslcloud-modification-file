<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$headers = getallheaders();

// Convert header names to uppercase
$uppercaseHeaders = array();
foreach ($headers as $name => $value) {
    $uppercaseName = strtoupper($name);
    $uppercaseHeaders[$uppercaseName] = $value;
}

$token = null;
if (isset($uppercaseHeaders['AUTHORIZATION'])) {

    $token = str_replace("Bearer ", "", $uppercaseHeaders['AUTHORIZATION']);
    define('TOKEN', $token);
}

// print_r($uppercaseHeaders['AUTHORIZATION']);
$request = json_decode(file_get_contents('php://input'), true);
// parent DB info
define('HOST', 'WIN-691I0MILC7F\SSLCLOUD_LS');
define('USER', 'SSLCloud');
define('DATABASE', 'SSLCloud');
define('PASSWORD', 'SSLCloudLive@#005');

// define('HOST', 'localhost');
// define('USER', 'sa');
// define('DATABASE', 'SSLCloud');
// define('PASSWORD', 'ssl@123');




function sanitize($data)
{
    $data = trim($data);
    return htmlentities($data, ENT_QUOTES, "UTF-8");
}

function ScriptRunnerParent($query)
{
    $connectionInfo = array("Database" => DATABASE, "UID" => USER, "PWD" => PASSWORD);
    $conn = sqlsrv_connect(HOST, $connectionInfo);
    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {
        die(print_r(sqlsrv_errors(), true));
    }

    return $result = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);

    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunner($my_database, $my_user, $my_host, $query, $rwfield)
{

    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => PASSWORD);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        return ("{$row[$rwfield]}");
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}
function ScriptRunnerAll($my_database, $my_user, $my_host, $query)
{

    $data = [];
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => PASSWORD);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        $data[] = $row;
    }
    return $data;
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}
function Scriptrunnercous($my_database, $my_user, $my_host, $query)
{

    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => PASSWORD);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    return $result = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunnerUD($my_database, $my_user, $my_host, $query)
{
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => PASSWORD);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function safe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
    return $data;
}

function safe_b64decode($string)
{
    $data = str_replace(array('-', '_'), array('+', '/'), $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}


$sql = "SELECT DISTINCT CLicense.CID AS CustomerID, DbConnStr.CustName AS CustomerName, DbConnStr.CUServer AS HostName, DbConnStr.CDbName AS DBName,DbConnStr.CUName AS DBUsername FROM CLicense LEFT JOIN DbConnStr ON CLicense.CID=DbConnStr.CustID WHERE DbConnStr.Status='A'   and DbConnStr.token = '$token' ";
// print_r($sql);
$result = ScriptRunnerParent($sql);

$my_host = $result['HostName'];
$my_database = $result['DBName'];
$my_user = $result['DBUsername'];


if (is_null($my_host) || is_null($my_database) || is_null($my_user)) {
    echo json_encode([
        "success" => false,
        "message" => "Invalid company Id.",
    ]);
    die();
}

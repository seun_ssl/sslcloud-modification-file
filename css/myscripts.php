<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../css/style_main.css" rel="stylesheet" type="text/css">

<?php
if (!isset($_REQUEST["PgDoS"])) {$_REQUEST["PgDoS"] = "";}

if (isset($Load_JQuery) && $Load_JQuery == true) {?>
<link rel="stylesheet" href="../themes/blue/style.css" type="text/css" media="print, projection, screen" />
<!-- <link rel="stylesheet" href="../jqry/jquery-ui.css" /> -->
<link rel="stylesheet" href="../assets/jquery-ui.min.css" />
<!-- <script src="../jqry/jquery-latest.min.js"></script> -->
<!-- <script src="../jqry/jquery-1.9.1.js"></script> -->
<script src="../assets/jquery.js"></script>
<!-- <script src="../jqry/jquery-ui.js"></script> -->
<script src="../assets/jquery-ui.min.js"></script>

<!--Autocomplete Loading -->
<!--Autocomplete Loading -->
<style>
.ui-autocomplete-loading {
background: white url("../jqry/images/ui-anim_basic_16x16.gif") right center no-repeat;
}
</style>
<?php
}

if (isset($Load_JQuery_Home) && $Load_JQuery_Home == true) {?>
<link rel="stylesheet" href="jqry/jquery-ui.css" />
<script src="jqry/jquery-1.9.1.js"></script>
<script src="jqry/jquery-ui.js"></script>
<link rel="stylesheet" href="jqry/style.css" />
<?php
}

if (isset($Load_JQuery_Tags) && $Load_JQuery_Tags == true) {?>
<link rel="stylesheet" type="text/css" href="../jqry/jquery.tagsinput.css" />
<script type="text/javascript" src="../jqry/jquery.tagsinput.js"></script>
<?php
}

if (isset($Load_TableSorter) && $Load_TableSorter == true) {?>
<!-- Tablesorter: required -->
<link rel="stylesheet" href="../css/theme.blue.css">
<script src="../jqry/jquery.tablesorter.js"></script>
<script src="../jqry/jquery.tablesorter.widgets.js"></script>

<!-- Tablesorter: optional -->
<link rel="stylesheet" href="../addons/pager/jquery.tablesorter.pager.css">
<script src="../addons/pager/jquery.tablesorter.pager.js"></script>

<script src="../jqry/widgets/widget-columnSelector.js"></script>
<script src="../jqry/widgets/widget-print.js"></script>

	<script id="js">$(function(){
	// **********************************
	//  Description of ALL pager options
	// **********************************
	var pagerOptions = {
		// target the pager markup - see the HTML block below
		container: $(".pager"),
		// use this url format "http:/mydatabase.com?page={page}&size={size}&{sortList:col}"
		ajaxUrl: null,
		// modify the url after all processing has been applied
		customAjaxUrl: function(table, url) { return url; },
		// process ajax so that the data object is returned along with the total number of rows
		// example: { "data" : [{ "ID": 1, "Name": "Foo", "Last": "Bar" }], "total_rows" : 100 }
		ajaxProcessing: function(ajax){
			if (ajax && ajax.hasOwnProperty('data')) {
				// return [ "data", "total_rows" ];
				return [ ajax.total_rows, ajax.data ];
			}
		},

		// output string - default is '{page}/{totalPages}'
		// possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
		output: '{startRow} to {endRow} ({totalRows})',

		// apply disabled classname to the pager arrows when the rows at either extreme is visible - default is true
		updateArrows: true,

		// starting page of the pager (zero based index)
		page: 0,

		// Number of visible rows - default is 10
		size: 20,

		// Save pager page & size if the storage script is loaded (requires $.tablesorter.storage in jquery.tablesorter.widgets.js)
		savePages : true,

		//defines custom storage key
		storageKey:'tablesorter-pager',

		// if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
		// table row set to a height to compensate; default is false
		fixedHeight: true,

		// remove rows from the table to speed up the sort of large tables.
		// setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
		removeRows: false,

		// css class names of pager arrows
		cssNext: '.next', // next page arrow
		cssPrev: '.prev', // previous page arrow
		cssFirst: '.first', // go to first page arrow
		cssLast: '.last', // go to last page arrow
		cssGoto: '.gotoPage', // select dropdown to allow choosing a page

		cssPageDisplay: '.pagedisplay', // location of where the "output" is displayed
		cssPageSize: '.pagesize', // page size selector - select dropdown that sets the "size" option

		// class added to arrows when at the extremes (i.e. prev/first arrows are "disabled" when on the first page)
		cssDisabled: 'disabled', // Note there is no period "." in front of this class name
		cssErrorRow: 'tablesorter-errorRow' // ajax error information row
	};


		// call the tablesorter plugin
	$("#table").tablesorter({
		theme: 'blue',

		// hidden filter input/selects will resize the columns, so try to minimize the change
		widthFixed : true,

		// initialize zebra striping and filter widgets
		//widgets: ["zebra", "filter"],
		widgets: ["zebra", "filter", "print", "columnSelector"],
		ignoreCase: false,
		widgetOptions : {
			// If there are child rows in the table (rows with class name from "cssChildRow" option)
			// and this option is true and a match is found anywhere in the child row, then it will make that row
			// visible; default is false
			filter_childRows : false,

			// if true, a filter will be added to the top of each table column;
			// disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
			// if you set this to false, make sure you perform a search using the second method below
			filter_columnFilters : true,

			// extra css class name(s) applied to the table row containing the filters & the inputs within that row
			// this option can either be a string (class applied to all filters) or an array (class applied to indexed filter)
			filter_cssFilter : '', // or []

			// jQuery selector (or object) pointing to an input to be used to match the contents of any column
			// please refer to the filter-any-match demo for limitations - new in v2.15
			filter_external : '',

			// class added to filtered rows (rows that are not showing); needed by pager plugin
			filter_filteredRow   : 'filtered',

			// add custom filter elements to the filter row
			// see the filter formatter demos for more specifics
			filter_formatter : null,

			// add custom filter functions using this option
			// see the filter widget custom demo for more specifics on how to use this option
			filter_functions : null,

			// if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
			// below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
			filter_hideFilters : true,

			// Set this option to false to make the searches case sensitive
			filter_ignoreCase : true,

			// if true, search column content while the user types (with a delay)
			filter_liveSearch : true,

			// a header with a select dropdown & this class name will only show available (visible) options within that drop down.
			filter_onlyAvail : 'filter-onlyAvail',

			// jQuery selector string of an element used to reset the filters
			filter_reset : 'button.reset',

			// Use the $.tablesorter.storage utility to save the most recent filters (default setting is false)
			filter_saveFilters : true,

			// Delay in milliseconds before the filter widget starts searching; This option prevents searching for
			// every character while typing and should make searching large tables faster.
			filter_searchDelay : 300,

			// if true, server-side filtering should be performed because client-side filtering will be disabled, but
			// the ui and events will still be used.
			filter_serversideFiltering: false,

			// Set this option to true to use the filter to find text from the start of the column
			// So typing in "a" will find "albert" but not "frank", both have a's; default is false
			filter_startsWith : false,

			// Filter using parsed content for ALL columns
			// be careful on using this on date columns as the date is parsed and stored as time in seconds
			filter_useParsedData : false,

			// data attribute in the header cell that contains the default filter value
			filter_defaultAttrib : 'data-value'

		}

	});

	// External search
	// buttons set up like this:
	// <button type="button" data-filter-column="4" data-filter-text="2?%">Saved Search</button>
	$('button[data-filter-column]').click(function(){
		/*** first method *** data-filter-column="1" data-filter-text="!son"
			add search value to Discount column (zero based index) input */
		var filters = [],
			$t = $(this),
			col = $t.data('filter-column'), // zero-based index
			txt = $t.data('filter-text') || $t.text(); // text to add to filter

		filters[col] = txt;
		// using "table.hasFilters" here to make sure we aren't targetting a sticky header
		$.tablesorter.setFilters( $('#table'), filters, true ); // new v2.9
		return false;
	});


	$("table")
		// Initialize tablesorter
		// ***********************
		.tablesorter({
			theme: 'blue',
			widthFixed: true,
			widgets: ["zebra", "filter", "print", "columnSelector"],
			widgetOptions : {
			columnSelector_container : $('#columnSelector'),
			columnSelector_name : 'data-name',

			print_title      : '',          // this option > caption > table id > "table"
			print_dataAttrib : 'data-name', // header attrib containing modified header name
			print_rows       : 'f',         // (a)ll, (v)isible or (f)iltered
			print_columns    : 's',         // (a)ll, (s)elected (columnSelector widget), or empty string
			print_extraCSS   : '',          // add any extra css definitions for the popup window here
			print_styleSheet : '../css/theme.blue.css' // add the url of your print stylesheet
			}

		})


		// bind to pager events
		// *********************
		.bind('pagerChange pagerComplete pagerInitialized pageMoved', function(e, c){
			var msg = '"</span> event triggered, ' + (e.type === 'pagerChange' ? 'going to' : 'now on') +
				' page <span class="typ">' + (c.page + 1) + '/' + c.totalPages + '</span>';
			$('#display')
				.append('<li><span class="str">"' + e.type + msg + '</li>')
				.find('li:first').remove();
		})

		// initialize the pager plugin
		// ****************************
		.tablesorterPager(pagerOptions);

		// Add two new rows using the "addRows" method
		// the "update" method doesn't work here because not all rows are
		// present in the table when the pager is applied ("removeRows" is false)
		// ***********************************************************************


/* COMMENTED BY ONYEMA AMAOBI JOSEPH */
		var r, $row, num = 50,
			row = '<tr><td>Student{i}</td><td>{m}</td><td>{g}</td><td>{r}</td><td>{r}</td><td>{r}</td><td>{r}</td><td><button type="button" class="remove" title="Remove this row">X</button></td></tr>' +
				'<tr><td>Student{j}</td><td>{m}</td><td>{g}</td><td>{r}</td><td>{r}</td><td>{r}</td><td>{r}</td><td><button type="button" class="remove" title="Remove this row">X</button></td></tr>';
		$('button:contains(Add)').click(function(){
			// add two rows of random data!
			r = row.replace(/\{[gijmr]\}/g, function(m){
				return {
					'{i}' : num + 1,
					'{j}' : num + 2,
					'{r}' : Math.round(Math.random() * 100),
					'{g}' : Math.random() > 0.5 ? 'male' : 'female',
					'{m}' : Math.random() > 0.5 ? 'Mathematics' : 'Languages'
				}[m];
			});
			num = num + 2;
			$row = $(r);
			$('table')
				.find('tbody').append($row)
				.trigger('addRows', [$row]);
			return false;
		});

		// Delete a row
		// *************
		$('table').delegate('button.remove', 'click' ,function(){
			var t = $('table');
			// disabling the pager will restore all table rows
			// t.trigger('disable.pager');
			// remove chosen row
			$(this).closest('tr').remove();
			// restore pager
			// t.trigger('enable.pager');
			t.trigger('update');
			return false;
		});

		// Destroy pager / Restore pager
		// **************
		$('button:contains(Destroy)').click(function(){
			// Exterminate, annhilate, destroy! http://www.youtube.com/watch?v=LOqn8FxuyFs
			var $t = $(this);
			if (/Destroy/.test( $t.text() )){
				$('table').trigger('destroy.pager');
				$t.text('Restore Pager');
			} else {
				$('table').tablesorterPager(pagerOptions);
				$t.text('Destroy Pager');
			}
			return false;
		});

		// Disable / Enable
		// **************
		$('.toggle').click(function(){
			var mode = /Disable/.test( $(this).text() );
			$('table').trigger( (mode ? 'disable' : 'enable') + '.pager');
			$(this).text( (mode ? 'Enable' : 'Disable') + 'Pager');
			return false;
		});
		$('table').bind('pagerChange', function(){
			// pager automatically enables when table is sorted.
			$('.toggle').text('Disable Pager');
		});


		$('.print').click(function(){
	$('.tablesorter').trigger('printTable');
});

});</script>


<script>
$(function(){

	// *** widgetfilter_startsWith toggle button ***
	$('button.toggle').click(function(){
		var c = $('#table')[0].config,
		$t = $(this),
		// toggle the boolean
		fsw = !c.widgetOptions.filter_startsWith,
		fic = !c.widgetOptions.filter_ignoreCase;
		if ($t.hasClass('fsw')) {
			c.widgetOptions.filter_startsWith = fsw;
			$('#start').html(fsw.toString());
		} else if ($t.hasClass('fic')) {
			c.widgetOptions.filter_ignoreCase = fic;
			$('#case').html(fic.toString());
		} else {
			$t = c.$headers.eq(1).toggleClass('filter-match');
			$t.find('span').html( $t.hasClass('filter-match') ? '' : ' No' );
		}
		// update search after option change; add false to trigger to skip search delay
		c.$table.trigger('search', false);
		return false;
	});

});
</script>

	<script id="js">$(function() {

	$(".tablesorter").tablesorter({
		theme: 'blue',
		widgets: ["zebra", "filter", "print", "columnSelector"],
		widgetOptions : {
			columnSelector_container : $('#columnSelector'),
			columnSelector_name : 'data-name',

			print_title      : '',          // this option > caption > table id > "table"
			print_dataAttrib : 'data-name', // header attrib containing modified header name
			print_rows       : 'f',         // (a)ll, (v)isible or (f)iltered
			print_columns    : 's',         // (a)ll, (s)elected (columnSelector widget), or empty string
			print_extraCSS   : '',          // add any extra css definitions for the popup window here
			print_styleSheet : '../css/theme.blue.css' // add the url of your print stylesheet
		}
	});

	$('.print').click(function(){
		$('.tablesorter').trigger('printTable');
	});

});</script>

<?php }?>


<?php if (isset($Load_JQuery_DataSet) && $Load_JQuery_DataSet == true) {?>
<!--<script src="../jqry/jquery.dataTables.js"></script>-->
<?php }?>


<?php if (isset($Load_Mult_Select) && $Load_Mult_Select == true) {?>
<script type="text/javascript">
function ChkDel()
{
	if (document.getElementById("DelChk_All").checked==true)
	{
	for (i=1;i<=document.getElementById("DelMax").value;i++)
		{
			if(document.getElementById("DelBox" + i)){

				document.getElementById("DelBox" + i).checked=true;
			}
		}
	}
else if (document.getElementById("DelChk_All").checked==false)
	{
		for (i=1;i<=document.getElementById("DelMax").value;i++)
		{
			// console.log(document.getElementById("DelBox" + i));
				if(document.getElementById("DelBox" + i)){
			document.getElementById("DelBox" + i).checked=false;
				}
		}
	}
}

function CollateChks()
{
	document.getElementById("ActLnk").value="";
	//alert (document.getElementById("ActLnk").value);
	$Mx=document.getElementById("DelMax").value;

	for ($i=1; $i<=$Mx; $i++)
	{
		if (  document.getElementById("DelBox" + $i) && document.getElementById("DelBox" + $i).checked)
		{

			document.getElementById("ActLnk").value=document.getElementById("ActLnk").value+';'+document.getElementById("DelBox"+$i).value;
		}
	}
	//alert(document.getElementById("ActLnk").value);
}
</script>
<?php }?>


<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO
function YesNo(MyTitle, MyType)
{
$('<div></div>').appendTo('body')
	.html('<div><class=TextBoxText>Do you wish to <strong>'+MyType+'</strong> the selected record(s)?</h5></div>')
	.dialog({
		modal: true, title: MyTitle, zIndex: 10000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			Yes: function () {
				document.getElementById("FAction").value=MyType+" Selected";
				// alert(document.getElementById("FAction").value=MyType+" Selected");
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			No: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>

<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO with message passed as argument
function GeneralYesNo(msg, title = 'Submit', type)
{
$('<div></div>').appendTo('body')
	.html('<div><class=TextBoxText>'+msg+'</h5></div>')
	.dialog({
		modal: true, title: title, zIndex: 10000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			Yes: function () {
				document.getElementById("FAction").value=type;
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			No: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
	$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>

<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO
function YesNo2(MyTitle, MyType)
{
$('<div></div>').appendTo('body')
	//.html('<div><class=TextBoxText><strong><font color="red">'+MyType+'</font></strong></h5></div>')
	.html('<div><class=TextBoxText><strong><font color="red">Hello! You clicked on Final Submission. This does not submit your Appraisal to your Supervisor, rather it submits directly to your HOD. Click Final Submission to submit to your HOD or Click Cancel, then go back within this page to Click on Submit for Supervisors Confirmation.</font></strong></h5></div>')
	.dialog({
		modal: true, title: MyTitle, zIndex: 20000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			FinalSubmission: function () {
				document.getElementById("FAction").value=MyType+" Selected";
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
	$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>

<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO
function YesNo3(MyTitle, MyType)
{
$('<div></div>').appendTo('body')
	//.html('<div><class=TextBoxText><strong><font color="red">'+MyType+'</font></strong></h5></div>')
	.html('<div><class=TextBoxText><strong>Do you wish to <strong>END</strong> this appraisal. This will terminate this employee appraisal process?</div>')
	.dialog({
		modal: true, title: MyTitle, zIndex: 20000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			EndAppraisal: function () {
				document.getElementById("FAction").value=MyType;
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
	$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>

<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO
function mgrAssess(MyTitle, MyType)
{
$('<div></div>').appendTo('body')
	.html('<div><class=TextBoxText><strong>Submit</strong> manager\'s assessment?</div>')
	.dialog({
		modal: true, title: MyTitle, zIndex: 20000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			Submit: function () {
				document.getElementById("FAction").value=MyType;
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
	$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>

<?php if (isset($Load_YesNo) && $Load_YesNo == true) {?>
<script type="text/javascript">
//This functions displays the gray YES/NO
function shouldCreateQuery(MyTitle, MyType)
{
$('<div></div>').appendTo('body')
	.html('<div><class="TextBoxText">Do you wish to <strong> Create </strong> query for the selected employee(s)?</h5></div>')
	.dialog({
		modal: true, title: MyTitle, zIndex: 10000, autoOpen: true,
		width: 'auto', resizable: true,
		height: 'auto', resizable: true,
		buttons: {
			Yes: function () {
				document.getElementById("FAction").value=MyType;
				document.getElementById("Records").submit();
				$(this).dialog("close");
			},
			No: function () {
				$(this).dialog("close");
			}
		},
		close: function (event, ui) {
			$(this).remove();
		}
	});
	$(".ui-dialog").scrollTop("0");
};
</script>
<?php }?>




<?php if (isset($Load_MsgBox) && $Load_MsgBox == true) {?>
<script type="text/javascript">
function msgbox($DisplayText, $Color)
{
	window.parent.scrollTo(0,0)
//This function displays a message within the main window to notify the user of an action success or failure
//Retrives the value $DisplayText and $Color from the calling function and displays it in the specified color

$DisplayMsgBoxType="<?php echo $_SESSION["StkTck" . "Session" . "NotfType"]; ?>";
	if ($DisplayText != "")
	{
		if ($DisplayMsgBoxType == "Silent" || $DisplayMsgBoxType == "")
		{
			if ($Color.toLowerCase() == "green")
			{
				$MsgImg='<img src="images/bi_butn/good_sign.gif" width="10" height="10" >';
			}
			else if ($Color.toLowerCase() == "red")
			{
				$MsgImg='<img src="images/bi_butn/bad_sign.gif" width="10" height="10" >';
			}
			else if ($Color.toLowerCase() == "blue")
			{
				$MsgImg='<img src="images/bi_butn/info_sign.gif" width="10" height="10" >';
			}

			document.getElementById("msgbox_display").innerHTML = $MsgImg + ' ' + $DisplayText;
			document.getElementById("msgbox_display").style.color = $Color;
		}

		else if ($DisplayMsgBoxType == "Pop-Up")
		{
			//Call a JQuery function for Grey Screen
			document.getElementById("GreyScreenMsg").innerHTML = "<center>"+$DisplayText+"</center>"; //"<p align=center>"+$DisplayText+"</p>";
			document.getElementById("GreyScreenMsg").title = "Alert";

				$(function() {
					$( "#GreyScreenMsg" ).dialog({
					  height: 'auto',
					  modal: true
					});
				  });
		}
	}
	else
	{
		document.getElementById("msgbox_display").innerHTML = "";
		document.getElementById("msgbox_display").style.color = 'Red';
	}
}
</script>
<?php }?>



<?php if (isset($Load_JQueryPopUp) && $Load_JQueryPopUp == true) {?>
<script type="text/javascript">
//This function displays the gray message or image as value is passed.
function ShowDisp(MyTitle, MyPg, MyHt, MyWt, MyScroll)
{
	if (MyScroll == "Yes" || MyScroll == "No"){} else {MyScroll='No';} //set scrolling default to false NO
	document.getElementById("GreyScreenMsg").title = '';
	document.getElementById("GreyScreenMsg").innerHTML = "<iframe id='frmSub' src='" + MyPg + "' marginheight='0' marginwidth='0' width='100%' height='100%' frameborder='0' scrolling='"+MyScroll+"' ></iframe>";

	// initialize dialog
	$(function() {
		$( "#GreyScreenMsg" ).dialog({
		modal: true,
	  	height: MyHt + 6,
	  	width: MyWt + 20,
		title: 'SSLCloud: '+MyTitle
		});
	});
}
</script>

<script>
function HideDisp()
{
	$(function() {
		$( "#GreyScreenMsg" ).dialog("close");
	});
}
</script>
<?php }?>


<?php if (isset($Load_ImgSwap) && $Load_ImgSwap == true) {?>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<?php }?>


<script language="JavaScript">
<!--
function autoResizeIframe(id){
    var newheight;
    var newwidth;

    if(document.getElementById){
        newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
        newwidth=document.getElementById(id).contentWindow.document.body.scrollWidth;
    }
}
//-->
</script>

<script type="text/javascript">
function setIframeHeight(iframe)
{
    if (iframe)
	{
		alert(document.getElementById("frame_main_stage").contentWindow.document.body.scrollHeight);

        var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
        if (iframeWin.document.body)
		{
            iframe.height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
        }
    }
}
</script>



<script type="text/javascript">
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
</script>


<Script language="JavaScript">
<!---
function showhide_N_But($CallerItem, $HideFrame, $FrameHeight)
{
var ActControl = document.getElementById($HideFrame);
var HtControl = parseInt($FrameHeight);
	if (document.getElementById($CallerItem).title == "Hide Top Advert")
	{
		document.getElementById($CallerItem).src = "images/img_loser.gif";
		document.getElementById($CallerItem).title="Show Top Advert"
		ActControl.style.visibility = "hidden";
		ActControl.style.height= 0;
		<?php //$_SESSION["Alumni_TopAdv"] = "Hide";?>
	}
	else
	{
		document.getElementById($CallerItem).src = "images/img_gainer.gif";
		document.getElementById($CallerItem).title="Hide Top Advert"
		ActControl.style.visibility = "visible";
		ActControl.style.height= HtControl;
		<?php //$_SESSION["Alumni_TopAdv"] = "Show";?>
	}
}
</Script>



<script type="text/javascript">
function redirect_frame($FrameToReload, $SRCPage)
{
	if ($FrameToReload == 'frame_employee_Ppt'|| $FrameToReload == 'frame_employee_Ppt_sig' )
	{
		// document.getElementById('frame_main_stage').height = document.getElementById('frame_main_stage').contentWindow.document.body.scrollHeight + "px";
		document.getElementById('frame_main_stage').contentWindow.document.getElementById($FrameToReload).src = $SRCPage;
	}
	else
	{
		// document.getElementById($FrameToReload).height = document.getElementById($FrameToReload).contentWindow.document.body.scrollHeight + "px";
		document.getElementById($FrameToReload).src = $SRCPage;
	}
}

function refresh_frame($FrameToReload)
{
	if ($FrameToReload == 'frame_employee_Ppt' || $FrameToReload == 'frame_employee_Ppt_sig')
	{
		document.getElementById('frame_main_stage').contentWindow.document.getElementById($FrameToReload).src=document.getElementById('frame_main_stage').contentWindow.document.getElementById($FrameToReload).src;
		document.getElementById('frame_main_stage').contentWindow.document.getElementById($FrameToReload).innerHTML=document.getElementById('frame_main_stage').contentWindow.document.getElementById($FrameToReload).innerHTML;
	}
	else
	{
		document.getElementById($FrameToReload).src=document.getElementById($FrameToReload).src;
		document.getElementById($FrameToReload).innerHTML = document.getElementById($FrameToReload).innerHTML;
	}
}

//Hiding and Unhiding Elements
function removeElement($EmtName)
{
	if (document.getElementById($EmtName).style.display == "none")
	{document.getElementById($EmtName).style.display = "block";}

	else if (document.getElementById($EmtName).style.display == "block")
	{document.getElementById($EmtName).style.display = "none";}

	else
	{document.getElementById($EmtName).style.display = "none";}
}

function changeVisibility($EmtName, $Actn)
{
	document.getElementById($EmtName).style.display=$Actn;
}


function ImgBorderOn($ImgName, $Size)
{
	document.getElementById($ImgName).border=$Size;
	document.getElementById($ImgName).style.opacity = 1;
}

function refresh_2self_frame($FrameToReload)
{
	document.getElementById($FrameToReload).contentWindow.location.reload()
}
</script>


<script language="javascript">
function passwordChanged()
{
	var strength = document.getElementById("strength");
	var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
	var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
	var enoughRegex = new RegExp("(?=.{6,}).*", "g");
	var pwd = document.getElementById("NewPassword1");
		if (pwd.value.length==0)
		{//strength.innerHTML = "Type Password";
		}
		else if (false == enoughRegex.test(pwd.value))
		{strength.innerHTML = "More Characters";}

		else if (strongRegex.test(pwd.value))
		{strength.innerHTML = '<span style="color:green">Strong!</span>';}

		else if (mediumRegex.test(pwd.value))
		{strength.innerHTML = '<span style="color:orange">Medium!</span>';}

		else
		{
		strength.innerHTML = '<span style="color:red">Weak!</span>';
		}
}
</script>


<script type="text/javascript">
function HideMe($SA)
{
	document.getElementById($SA).style.visibility="hidden";
}

function ChangeImg($ObjID, $ImgPath)
{
	document.getElementById($ObjID).src=$ImgPath;

	//	frame_main_stage.document.getElementById("").src=$ImgPath;
	frames["frame_main_stage"].document.getElementById("Ppt").src='lll';
	alert (frames["frame_main_stage"].document.getElementById("Ppt").src);
}

</script>



<script type="text/javascript">
function printIframe(id)
{
    var iframe = document.frames ? document.frames[id] : document.getElementById(id);
    var ifWin = iframe.contentWindow || iframe;
    ifWin.focus();
    ifWin.printPage();
    return false;
}
</script>



<script type='text/javascript'>
function ajaxLoader (el, options) {
	// Becomes this.options
	var defaults = {
		bgColor 		: '#fff',
		duration		: 800,
		opacity			: 0.7,
		classOveride 	: false
	}
	this.options 	= jQuery.extend(defaults, options);
	this.container 	= $(el);

	this.init = function() {
		var container = this.container;
		// Delete any other loaders
		this.remove();
		// Create the overlay
		var overlay = $('<div></div>').css({
				'background-color': this.options.bgColor,
				'opacity':this.options.opacity,
				'width':container.width(),
				'height':container.height(),
				'position':'absolute',
				'top':'0px',
				'left':'0px',
				'z-index':99999
		}).addClass('ajax_overlay');
		// add an overiding class name to set new loader style
		if (this.options.classOveride) {
			overlay.addClass(this.options.classOveride);
		}
		// insert overlay and loader into DOM
		container.append(
			overlay.append(
				$('<div></div>').addClass('ajax_loader')
			).fadeIn(this.options.duration)
		);
    };

	this.remove = function(){
		var overlay = this.container.children(".ajax_overlay");
		if (overlay.length) {
			overlay.fadeOut(this.options.classOveride, function() {
				overlay.remove();
			});
		}
	}

    this.init();
}
</script>


<script>
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{
			try{
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		return xmlhttp;
	}

	function getChildren($strDIV,$strURL) {
//alert($strDIV);
//alert($strURL);
		var req = getXMLHTTP();

		if (req) {
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {
						document.getElementById($strDIV).innerHTML=req.responseText;
					} else {
						//alert("There was a problem while using XMLHTTP:\n" + req.statusText);
						parent.msgbox('There was a problem retrieving the selected values', 'red');
					}
				}
			}
			req.open("GET", $strURL, true);
			req.send(null);
		}
	}
	function resizeIframes(iframe) {

		window.parent.scrollTo(0,0)

	}
</script>
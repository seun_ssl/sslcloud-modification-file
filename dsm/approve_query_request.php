<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("APPROVE QUERY REQUEST" . "V") != true) {include '../main/NoAccess.php';exit;}

if (isset($_POST['approve'])) {

    // update query_status to A
    $id = (int) sanitize($_POST['id']);
    $deadline = sanitize($_POST['deadline']);

    $Script = "UPDATE [query_request] SET [query_status] = 'A', [approve_reject_date]= GETDATE(), [deadline]='" . $deadline . "' WHERE [id] ='{$id}'";
    ScriptRunnerUD($Script, "Inst");
    // $reel_info = json_decode($item['query_info'], true);

    // notify employee
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $query_details = ScriptRunnercous($sql);
    // var_dump($query_details);
    $query_info = $query_details['query_info'];
    $info_decode = json_decode($query_info, true);
    // if (strlen($approver) !== 32 && trim($approver) === 'first_sup') {
    //   $approver = getLevel1($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'second_sup') {
    //   $approver = getLevel2($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'third_sup') {
    //   $approver = getLevel3($_SESSION['StkTckHKey']);
    // }
    $approver = $query_details['approver'];
    $correspondences = $info_decode['correspondence'];
    $query_name = $info_decode['templateName'];
    $issuer = $query_details['query_by'];
    $employee = $query_details['emp_hash'];
    $query_reason = $query_details['query_reason'];
    $issuer_details = getUserDetailsfromHash($issuer);
    $employee_details = getUserDetailsfromHash($employee);
    $approver_details = getUserDetailsfromHash($approver);

    $msg = "Dear #Name#,<br /> This is to inform you that you have been issued the query({$query_name}) by employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/>. Kindly login to answer this query on or before {$deadline}.";
    $Subj = "Query Request Notification";
    $msg = str_replace('#Name#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $employee_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify issuer

    $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) you issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/> has been approved by employee #Approver# . ";
    $Subj = "Query Request Notification (Approved)";
    $msg = str_replace('#Name#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $issuer_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify all corespondence
    foreach ($correspondences as $correspondence) {
        $correspond = getUserDetailsfromHash($correspondence);

        $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/> has been approved by employee #Approver# . ";
        $Subj = "Query Request Notification (Approved)";
        $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $correspond['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

    }
    AuditLog("Query Approved", "Query ({$query_name}) approved for employee [" . $emp_hash . "]");

    echo ("<script type='text/javascript'>{ parent.msgbox('Query request approved successfully.', 'green'); }</script>");

}
if (isset($_POST['reject'])) {
    // var_dump($_POST);
    // update query_status
    $id = (int) sanitize($_POST['id']);
    $reject_reason = sanitize($_POST['reject_reason']);
    $Script = "UPDATE [query_request] SET [query_status] = 'R', [approve_reject_date]= GETDATE() WHERE [id] ='{$id}'";
    ScriptRunnerUD($Script, "Inst");
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $query_details = ScriptRunnercous($sql);
    $query_info = $query_details['query_info'];
    $info_decode = json_decode($query_info, true);
    // if (strlen($approver) !== 32 && trim($approver) === 'first_sup') {
    //     $approver = getLevel1($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'second_sup') {
    //     $approver = getLevel2($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'third_sup') {
    //     $approver = getLevel3($_SESSION['StkTckHKey']);
    // }
    $approver = $info_decode['approver'];
    $correspondences = $info_decode['correspondence'];
    $query_name = $info_decode['templateName'];
    $issuer = $query_details['query_by'];
    $employee = $query_details['emp_hash'];
    $query_reason = $query_details['query_reason'];
    $issuer_details = getUserDetailsfromHash($issuer);
    $employee_details = getUserDetailsfromHash($employee);
    $approver_details = getUserDetailsfromHash($approver);
    // notify issuer
    $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) you issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/> has been rejected by employee #Approver#, due to the following reason(s) :<br/>{$reject_reason} ";
    $Subj = "Query Request Notification (Rejection)";
    $msg = str_replace('#Name#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $issuer_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify corespondence
    foreach ($correspondences as $correspondence) {
        $correspond = getUserDetailsfromHash($correspondence);

        $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/> has been rejected by employee #Approver#, due to the following reason(s) :<br/>{$reject_reason} ";
        $Subj = "Query Request Notification (Rejection)";
        $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $correspond['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

    }
    AuditLog("Query Declined", "Query ({$query_name}) declined for employee [" . $emp_hash . "]");

    echo ("<script type='text/javascript'>{ parent.msgbox('Query request rejected successfully.', 'green'); }</script>");

}

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
    // var_dump($_POST);
    $level1_query = "Select * from [query_request] where [query_id]='" . $_POST['bookselect1'] . "' and [query_status] = 'P' and [approver] = '" . $_SESSION['StkTckHKey'] . "'";
    // var_dump($level1_query);
    $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $level1_query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    $level1_arry = [];
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        $row['level_type'] = 1;
        array_push($level1_arry, $row);

    }

    $all_arry = array_merge($level1_arry, []);
    // var_dump($all_arry);
    if (count($all_arry) === 0) {
        $quert_tem = ucwords($_POST['hid']);
        echo ("<script type='text/javascript'>{parent.msgbox('You do not have any pending {$quert_tem} request', 'red'); }</script>");

    }

}

// var_dump($allInfo);

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
		<script>
		$(function()
		{
		<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {} else {?>
		$("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+1D", dateFormat: 'dd M yy',yearRange: "-75:+75"})
		$("#EmpDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "+1M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		<?php }?>
		$("#DOB").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "<?php
$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
if (number_format($kk, 0, '', '') == 0 || $kk == "") {echo "-18Y";} else {echo "-" . $kk . "Y";}?>
		", dateFormat: 'dd M yy', yearRange: "-75:+75"})
		});
		</script>
		<script>
		$(function() {$( "#tabs" ).tabs();});
		$(function() {
			$( document ).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value='';
			});
		});
		</script>
		<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent="+Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent="+Replmt);
			});
		});
		</script>
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
		<style>

		.table>tbody>tr>td {
				padding-left: 7px !important;
			}

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: px 5px;
    line-height: 20px;
}

		</style>

	</head>


	<body>






            <!-- Main content -->
            <section class="content">

                <div class="box">
              <div class="box-header with-border">
                    <div class="row">

                              <div class="col-md-6  text-md-left text-center">
                                <h3>
                                 Approve/Reject Query Request
                                </h3>

                              </div>
                             <div class="col-md-6">
                             	 <form id="theform1"  method="post">
                              <div class="row">

                             <div class="col-4" ></div>
                             <!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
                               <!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
                                 <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                                   <select class="form-control " name="bookselect1" id="bookselect1" >
                                            <option value="">--</option>
                                              <?php
$query = "SELECT * from [querytemplate] where  id='1'";

$getcheck = ScriptRunnercous($query);

$groupsarry = json_decode($getcheck['template_name'], true);

?>

                                              <?php foreach ($groupsarry as $eachgroup): ?>


                                                <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>


                                              <?php endforeach;?>



                                          </select>
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger"   id="getall1">Open</button>
                                          </span>
                                    </div>
                              </div>
							  <input type="hidden" name="hid"  id="hid">

								</form>

                             </div>
                            </div>
              </div>
              <div class="box-body">


<?php if (isset($all_arry) && !empty($all_arry)):
    // var_dump($all_arry);

    ?>
																																										                  <div class="row">


																																										                      <div class="col-md-12">
																																										                          <div class="row">
																																										                              <div class="col-12">
																																										                                  <div class="nav-tabs-custom" style="min-height: 400px">
																																										                                      <ul class="nav nav-tabs">
																																										                                          <li>
																																										                                              <a class="active" href="#book" data-toggle="tab"><?=$_POST['hid'] ? ucfirst($_POST['hid']) : ucfirst($_POST['nam'])?> </a>
																																										                                          </li>



																																										                                      </ul>
																																										                                      <div class="tab-content">

																																										                                          <div class="tab-pane active" id="book">


																																										                                                              <table id="example1" class="table  table-bordered table-striped table-responsive tablesorter mt-5 ">
																																										                                                                <thead>
																																										                                                                    <tr>
																																										                                                                       <th>Query Issued By </th>
																																										                                                                       <th>Query Issued To</th>
																																										                                                                       <th>Query Reason(s)</th>
																																										                                                                       <th>Query Created Date</th>
																																										                                                                       <th> Issueer's Department </th>
																																										                                                                       <th> Receiver's Department </th>
																																										                                                                        <th>View Query</th>
																																										                                                                        <th>Approve Query</th>
																																										                                                                        <th>Reject Query</th>

																																										                                                                    </tr>
																																										                                                                </thead>
																																																										<?php foreach ($all_arry as $arry): ?>
																																																										<tr>
																																																											<th>
																																																											<?php $issued_by_details = getUserDetailsfromHash($arry['query_by']);
    echo "{$issued_by_details['SName']} {$issued_by_details['FName']} {$issued_by_details['OName']}";?></th>
																																																											<th>
																																																											<?php $issued_to_details = getUserDetailsfromHash($arry['emp_hash']);
    echo "{$issued_to_details['SName']} {$issued_to_details['FName']} {$issued_to_details['OName']}";?></th>

																																																											<th>
																																																											 <?=$arry['query_reason']?>
																																																											</th>
																																																											<th><?=$arry['created_date']->format('j F, Y')?></th>
																																																											<th>
																																																												 <?=$issued_by_details['Department']?>
																																																											</th>
																																																											<th>
																																																												 <?=$issued_to_details['Department']?>
																																																											</th>
																																																											<th>

																																																											<a href="query_single.php?current=<?=$arry['id']?>&nam=<?=$_POST['hid'] ? ucfirst($_POST['hid']) : ucfirst($_POST['nam'])?>">View</a>
																																																											</th>
																																																											<th>
																																																											<form method="post" id="approve_<?=$arry['id']?>" >
																																																												<input type="hidden" name="approve" value="approve"  >
																																																												<input type="hidden" name="id" value="<?=$arry['id']?>" >
																																																												<input type="hidden" id="deadline_<?=$arry['id']?>"  name=deadline  value="" >
																																																											</form>
																																																											 <button type="button" id="approve"     class="btn btn-danger btn-sm"  onclick="approve(event,<?=$arry['id']?>); return false;">Approve </button>

																																																											</th>
																																																											<th>
																																																												<form method="post" id="reject_<?=$arry['id']?>" >
																																																												<input type="hidden" name="reject" value="reject"  >
																																																												<input type="hidden" name="id" value="<?=$arry['id']?>" >
																																																												<input type="hidden" name="reject_reason" value="" id="reject_reason_<?=$arry['id']?>" >
																																																											</form>
																																																											 <button type="button" id="reject"  class="btn btn-danger btn-sm"  onclick="reject(event,<?=$arry['id']?>); return false;">Reject </button>
																																																											</th>
																																																										</tr>
																																																										<?php endforeach;?>
                                                                <tbody>


                                                                </tbody>
                                                              </table>
                                                              <?php include '../main/pagination.php';?>


                                          </div>













                                          <!-- /.tab-content -->
                                      </div>
                                      <!-- /.nav-tabs-custom -->
                                  </div>
                                  <!-- /.col -->
                              </div>
                          </div>













                      </div>











                  </div>

<?php endif;?>






















              </div>
          </div>
          <!-- /.box-body -->




        </div>

















        <!-- /.row -->
        </section>
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<script type="text/javascript">






		 function getDetail(a,event){

		 	 event.preventDefault();

       let curren = $(a).closest("tr").attr("id");
       let nexxt= $(a).closest("tr").next().attr("id");
      let prevv=  $(a).closest("tr").prev().attr("id");

      $("#next"+curren).val(nexxt);
      $("#prev"+curren).val(prevv);

      // console.log(curren,nexxt,prevv);

      $("#form"+curren).submit();


      }


		</script>

		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>


			<!-- MinimalLite Admin App -->
 <script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <!-- MinimalLite Admin for Data Table -->
  <!-- <script src="../assets/js/pages/data-table.js"></script> -->


		 <script>



	$('#example1').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example6').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example7').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

setInterval(function () {
    $( ".tablesorter-filter-row" ).remove();
}, 2000);
		function amp(id){


			var val= $('#'+id).val();
			// console.log(val);

			if (val=='') {
				$('#com'+id).prop('disabled', true);
				window['assignedId']=val;

				 { parent.msgbox('Please select an item', 'green'); };

			}else{
				$('#com'+id).prop('disabled', false);
				window['assignedId']=val;
			}

			}
		 function approve(event,id){
			 event.preventDefault();
			 $('<div></div>').appendTo('body')
			        .html('<div><input name="dead_line" id="dead_line" type="date" class="form-control" value="" placeholder="Query deadline" /></div>')
			        .dialog({
			          modal: true, title: "Approve Query Request with query deadline", zIndex: 10000, autoOpen: true,
			           resizable: true,
			           resizable: true,
                height: 200 + 6,
			        	width: 500 + 20,
			          buttons: {
			            Yes: function () {

                    		// var msgC=document.getElementById("sd").value;
			             	 		if (document.getElementById("dead_line").value==='') {

			             	 			 { parent.msgbox('Enter query deadline', 'red'); };

			             	 			 return;

			             	 		}

									  $(`#deadline_${id}`).val(document.getElementById("dead_line").value);
						        document.getElementById(`approve_${id}`).submit();
			              $(this).dialog("close");
			            },
			            No: function () {
			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });
		 }

		 function reject(event,id){
			 event.preventDefault();
		 			 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Are you sure you want to reject this request </h5></div>')
			        .dialog({
			          modal: true, title: "Reject query request ", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {


			             	 // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

			             	  $('<div></div>').appendTo('body')
			             	 		        .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

			             	 	modal: true,title: "Reason(s) for rejecting this query request", zIndex: 10000, autoOpen: true,
			             	   	height: 200 + 6,
			             	   	width: 500 + 20,
			             	 	buttons: {

			             	 		Reject: function () {

			             	 		var msgC=document.getElementById("sd").value;
			             	 		if (document.getElementById("sd").value==='') {

			             	 			 { parent.msgbox('Enter query rejection reason(s)', 'red'); };

			             	 			 return;

			             	 		}

									  $(`#reject_reason_${id}`).val(document.getElementById("sd").value);
									   document.getElementById(`reject_${id}`).submit();
			             	 		  $(this).dialog("close");

			             	 		}
			             	 	}

			             	 	});

			              $(this).dialog("close");
			            },
			            No: function () {
			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });













































		 }

        $("#getall1").click(function(){

         let bookSelected = $('#bookselect1').val();
		 let selectedOptionText = $('#bookselect1').find(":selected").text();
		 $('#hid').val(selectedOptionText);


           if (bookSelected === "") {



             { parent.msgbox('Please select a query ', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>
	</body>

	</html>
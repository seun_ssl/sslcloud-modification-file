<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$td = date("Y-m-d");
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("MANAGE QUERY" . "V") != true) {include '../main/NoAccess.php';exit;}

$dis = false;
if (isset($_POST['query_action'])) {
    // var_dump($_POST);
    $id = (int) sanitize($_POST['id']);
    $query_action = sanitize($_POST['query_action']);
    $query_action_comment = sanitize($_POST['query_comment']);
    $Script = "UPDATE [query_request] SET [query_action] = '{$query_action}', [query_action_comment] = '{$query_action_comment}' ,[query_action_date]= GETDATE() WHERE [id] ='{$id}'";
    ScriptRunnerUD($Script, "Inst");
    // $reel_info = json_decode($item['query_info'], true);

    // notify employee
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $query_details = ScriptRunnercous($sql);
    // var_dump($query_details);
    $query_info = $query_details['query_info'];
    $info_decode = json_decode($query_info, true);
    // if (strlen($approver) !== 32 && trim($approver) === 'first_sup') {
    //     $approver = getLevel1($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'second_sup') {
    //     $approver = getLevel2($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'third_sup') {
    //     $approver = getLevel3($_SESSION['StkTckHKey']);
    // }
    $approver = $query_details['approver'];
    $correspondences = $info_decode['correspondence'];
    $query_name = $info_decode['templateName'];
    $issuer = $query_details['query_by'];
    $employee = $query_details['emp_hash'];
    $query_reason = $query_details['query_reason'];
    $issuer_details = getUserDetailsfromHash($issuer);
    $employee_details = getUserDetailsfromHash($employee);
    $approver_details = getUserDetailsfromHash($approver);

    $msg = "Dear #Name#,<br />
    After the employee #Approver# has taken a look at your response to the query({$query_name}) by employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/> He/She has taken the following action:<br/>
    Query`s decision : <strong>{$query_action}</strong><br/><em>{$query_action_comment}</em>.You can see the management team if you disagree with the decision above.";
    $Subj = "Query Request Notification(Approver`s Decision)";
    $msg = str_replace('#Name#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $employee_details['Email'];
    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify issuer

    $msg = "Dear #Name#,<br />
    The employee #Approver# has taken a decision on the query({$query_name}) you issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason}. <br/> Below is the decision taken:
      Query`s decision : <strong>{$query_action}</strong><br/>
    <em>{$query_action_comment}</em>.
    You can see the management team if you disagree with the decision above. ";
    $Subj = "Query Request Notification (Approver`s Decision)";
    $msg = str_replace('#Name#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $issuer_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify all corespondence
    foreach ($correspondences as $correspondence) {
        $correspond = getUserDetailsfromHash($correspondence);

        $msg = "Dear #Name#,<br />
    The employee #Approver# has taken a decision on the query({$query_name}) issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason}. <br/> Below is the decision taken:
      Query`s decision : <strong>{$query_action}</strong><br/>
    <em>{$query_action_comment}</em>.";

        $Subj = "Query Request Notification (Approver`s Decision)";
        $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $correspond['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

    }
    $dis = true;
    AuditLog("Query Actioned", "Query actioned for employee [" . $employee . "]");

    echo ("<script type='text/javascript'>{ parent.msgbox('Query request managed successfully.', 'green'); }</script>");

}

if (isset($_POST['Decline'])) {
    // var_dump($_POST);
    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);
    // send mail employee
    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $sender_details = getUserDetailsfromHash($data['emp_hash']);

    $msg = "Dear #Name#,<br /> This is to inform you that your request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "Form Request Notification";
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // update
    if ($level_type === 1) {
        $sql = "Update [formrequest] set [level1_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    } else if ($level_type === 2) {
        $sql = "Update [formrequest] set [level2_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    } else if ($level_type === 3) {
        $sql = "Update [formrequest] set [level3_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    }
    echo ("<script type='text/javascript'>{parent.msgbox('Request Declined successfully', 'red'); }</script>");
    $dis = true;

}

if (isset($_POST['Approve'])) {

    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);
    // var_dump($sql);
    // var_dump($data);
    if ($level_type === 1) {
        $next_approver = $data['level2'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level

            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Form Request Notification";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

            // update level1_status = A and level2_status = P
            $sql = "Update [formrequest] set [level1_status] = 'A', [level2_status] = 'P' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        } else {
            // send approver mail to employee

            // $next_approver_details = getUserDetailsfromHash($next_approver1);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            // var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Form Request Notification";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // update level1_status = A
            $sql = "Update [formrequest] set [level1_status] = 'A' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        }

    } else if ($level_type === 2) {
        $next_approver = $data['level3'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level
            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Form Request Notification";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

            // update level2_status = A and level3_status = P

            $sql = "Update [formrequest] set [level2_status] = 'A', [level3_status] = 'P' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        } else {
            // send approver mail to employee

            $sender_details = getUserDetailsfromHash($data['emp_hash']);

// var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Form Request Notification";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // update level2_status = A

            $sql = "Update [formrequest] set [level2_status] = 'A' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        }

    } else if ($level_type === 3) {
        // send approver mail to employee

        $sender_details = getUserDetailsfromHash($data['emp_hash']);

        // var_dump($sender_details);

        $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
        $Subj = "Form Request Notification";

        $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $sender_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");

        // update level3_status = A

        $sql = "Update [formrequest] set [level3_status] = 'A' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
        $dis = true;

    }

}

if (isset($_GET['current'])) {
    // unset($_SESSION)

    $level_type = $_GET['level_type'];
    // var_dump($level_type);

//   $next=$_POST['next'];
    //   $prev=$_POST['prev'];
    $id = (int) sanitize($_GET['current']);
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $item = ScriptRunnercous($sql);

    $query_info = $item['query_info'];
    $info_decode = json_decode($query_info, true);

}

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
		<script>
		$(function()
		{
		<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {} else {?>
		$("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "+0D", maxDate: "+12M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

    $("#ExDt1").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "+0D", maxDate: "+12M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

    $("#ExDt2").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "+0D", maxDate: "+12M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		$("#EmpDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "+10Y", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		<?php }?>
		$("#DOB").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "<?php
$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
if (number_format($kk, 0, '', '') == 0 || $kk == "") {echo "-18Y";} else {echo "-" . $kk . "Y";}?>
		", dateFormat: 'dd M yy', yearRange: "-75:+75"})
		});
		</script>
		<script>
		$(function() {$( "#tabs" ).tabs();});
		$(function() {
			$( document ).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value='';
			});
		});
		</script>
		<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent="+Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent="+Replmt);
			});
		});
		</script>
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
     <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">

		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

    <style type="text/css">

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: px 5px;
    line-height: 20px;
}

.form-control {
    display: block;
    width: 100%;
    padding: .5rem .75rem;
    font-size: 0.8rem;
    line-height: 1.25;
    color: #495057;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.box .box-group>.box {
    margin-bottom: 20px;
}

    </style>




	</head>


	<body>






        <!-- Main content -->
    <section class="content">

        <div class="box">
        <div class="box-header with-border">
         <div class="row">

                <div class="col-md-12  "  >
                  <h3>
                   Manage <?=ucfirst($info_decode['templateName'])?>
                  </h3>

                </div>

              </div>
        </div>
         <div class="box-body">




                 <div class="row">






























                                <?php

$groupsarry = json_decode($item['emp_response'], true);

// var_dump($groupsarry);
foreach ($groupsarry as $key => $value): ?>
                             <!-- <div class="col-md-3">
                                <div class="col-md-12"  style="padding-left: 0px; padding-right: 0px;" >
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary" >
                                      <div class="box-header">
                                         <p class="b-0 px-0" style="font-weight: bold"><?=$key?></p>
                                      </div>
                                      <div class="box-body">
                                                <p>
                                                   <textarea class="form-control" rows="2" readonly > <?=$value?>
                                                    </textarea>
                                                </p>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> -->


                  <div class="col-md-12">
                      <div class="box">
                          <div class="box-header with-border">
                          <h6 class="box-title"><strong><?=ucfirst(str_replace("_", " ", $key))?></strong></h6>
                          </div>
                            <div class="box-body">
                              <?php if (is_array($value)): ?>
                                  <?php foreach ($value as $val): ?>
                                     <p><?=$val?></p>
                                  <?php endforeach;?>

                              <?php else: ?>

                              <p><?=$value?></p>
                              <?php endif;?>
                            </div>
                        </div>
                   </div>


                                        <?php endforeach;?>
















































                 </div>



<div class="row">
  <?php
// var_dump($item);
?>
  <div class="col-1">
      <form method="post" action="manage_query.php">

        <input type="submit" value="Back" class="btn btn-danger btn-sm">
        <input type="hidden" name="bookselect1" value="<?=$item['query_id']?>">
        <input type="hidden" name="nam" value="<?=$info_decode['templateName']?>">

      </form>

  </div>





  <div class="col-7"></div>

















</div>
<hr>
<h3 class="text-center">Take a Decision</h3>

<form method="post" action="" id="query_form" style="width:100%" >
<div class="row mt-4">
    <input type="hidden" name="id" value="<?=$item['id']?>">
<div class="col-sm-6">

    	<div class="form-group row">
							    <label  class="col-sm-4 col-form-label"  style="padding-right: 0px;">Select an Action:</label>
							  	<div class="col-sm-8 input-group input-group-sm" style="">
								  <select class="form-control"  name="query_action" id="query_action">
                                  <option value="">--</option>
                                  <?php
$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Query Actions'  and Status<>'D' and Val1<>'') ORDER BY Val1");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
}
include '../login/dbClose2.php';
?>

								 </select>
									<span class="input-group-btn ">

                                       <button type="button" onClick="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Query Actions&LDb=Single&amp;Elmt=Query Actions',320,400,'No')" title="Add a new query action eg. Disciplined, Warned" id="MastersDepartment" class="btn btn-default btn-sm" style="line-height: 17px"><i class="fa fa-plus"></i></button>';


									</span>
								</div>
							</div>

</div>







   <div class="col-sm-6">
	                                     <div class="form-group row ">
	                         				<label  class="col-sm-4 col-form-label">Comment:</label>
						                    <div class="col-sm-8" >
											               <textarea type="text" rows="3" name="query_comment" maxlength="200" class="form-control" id="query_comment" /></textarea>
                                </div>
										</div>
   </div>
</div>
   </form>
     <button type="submit" id="Decline"   <?=$dis == true ? "disabled" : ""?>     class="btn btn-danger btn-sm"  onclick="decline(event); return false;">Submit </button>








              </div>
            </div>
        <!-- /.box-body -->




      </div>

















      <!-- /.row -->
    </section>
    <!-- /.content -->
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

		  <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>

		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>

		<!-- MinimalLite Admin for demo purposes -->
		<script src="../assets/js/demo.js"></script>


         <script>
    function amp(id){


      var val= $('#'+id).val();
      // console.log(val);

      if (val=='') {
        $('#com'+id).prop('disabled', true);
        window['assignedId']=val;

         { parent.msgbox('Please select an item', 'green'); };

      }else{
        $('#com'+id).prop('disabled', false);
        window['assignedId']=val;
      }

    }


     function decline(e){
     e.preventDefault();
      if($('#query_action').val()==="" ||$('#query_comment').val()==="" ){
        {parent . msgbox('All feilds are compulsory', 'green');};

      }else{
        	 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Are you sure about this decision </strong>?</h5></div>')
			        .dialog({
			          modal: true, title: "Manage Query Request", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {
					     	  document.getElementById('query_form').submit();
			              $(this).dialog("close");
			            },
			            No: function () {
			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });
        // $('#query_form').submit();
      }

     }

       $("#getall1").click(function(){

         let bookSelected = $('#bookselect1').val();


           if (bookSelected === "") {



             { parent.msgbox('Please select an item', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>

	</body>

	</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("QUERY HISTORY ALL" . "V") != true) {include '../main/NoAccess.php';exit;}

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
    // var_dump($_POST['bookselect1']);
    $level1_query = "Select * from [query_request] where [query_id]='" . $_POST['bookselect1'] . "'";
    // var_dump($level1_query);

    $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $level1_query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    $level1_arry = [];
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        // $row['level_type'] = 1;
        array_push($level1_arry, $row);

    }
    // var_dump($level1_arry);
    // $level2_query = "Select * from [formrequest] where [request_name]='" . $_POST['bookselect1'] . "' and [level2_status] in('A','C') ";
    // $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    // $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
    // if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    // $result = sqlsrv_query($conn, $level2_query);
    // if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    $level2_arry = [];
    // while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
    //     $row['level_type'] = 2;

    //     array_push($level2_arry, $row);

    // }
    // var_dump($level2_arry);

    // $level3_query = "Select * from [formrequest] where [request_name]='" . $_POST['bookselect1'] . "' and [level3_status] in('A','C') ";
    // $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    // $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
    // if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    // $result = sqlsrv_query($conn, $level3_query);
    // if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    $level3_arry = [];
    // while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
    //     $row['level_type'] = 3;

    //     array_push($level3_arry, $row);

    // }

    $all_arry = array_merge($level1_arry, $level2_arry, $level3_arry);
    // var_dump($all_arry);
    if (count($all_arry) === 0) {
        $quert_tem = ucwords($_POST['hid']);
        echo ("<script type='text/javascript'>{parent.msgbox('You do not have any {$quert_tem} request', 'red'); }</script>");

    }

}

// var_dump($all_arry);

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
		<script>
		$(function()
		{
		<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {} else {?>
		$("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+1D", dateFormat: 'dd M yy',yearRange: "-75:+75"})
		$("#EmpDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "+1M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		<?php }?>
		$("#DOB").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "<?php
$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
if (number_format($kk, 0, '', '') == 0 || $kk == "") {echo "-18Y";} else {echo "-" . $kk . "Y";}?>
		", dateFormat: 'dd M yy', yearRange: "-75:+75"})
		});
		</script>
		<script>
		$(function() {$( "#tabs" ).tabs();});
		$(function() {
			$( document ).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value='';
			});
		});
		</script>
		<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent="+Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent="+Replmt);
			});
		});
		</script>
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
		<style>

		.table>tbody>tr>td {
				padding-left: 7px !important;
			}

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: px 5px;
    line-height: 20px;
}
.box-body {

    overflow-y: scroll;
}

		</style>

	</head>


	<body>






            <!-- Main content -->
            <section class="content">

                <div class="box">
              <div class="box-header with-border">
                    <div class="row">

                              <div class="col-md-6  text-md-left text-center">
                                <h3>
                                  Query Request History
                                </h3>

                              </div>
                             <div class="col-md-6">
                             	 <form id="theform1"  method="post">
                              <div class="row">

                             <div class="col-4" ></div>
                             <!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
                               <!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
                                 <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                                   <select class="form-control " name="bookselect1" id="bookselect1" >
                                            <option value="">--</option>
                                              <?php
$query = "SELECT * from [querytemplate] where  id='1'";

$getcheck = ScriptRunnercous($query);

$groupsarry = json_decode($getcheck['template_name'], true);

?>

                                              <?php foreach ($groupsarry as $eachgroup): ?>


                                                <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>


                                              <?php endforeach;?>



                                          </select>
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger"   id="getall1">Open</button>
                                          </span>
                                    </div>
                              </div>
							  <input type="hidden" name="hid"  id="hid">

								</form>

                             </div>
                            </div>
              </div>
              <div class="box-body">


<?php if (isset($all_arry) && !empty($all_arry)): ?>
                  <div class="row">


                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-12">
                                  <div class="nav-tabs-custom" style="min-height: 400px">

                                    <?php
$query_info = $all_arry[0]['query_info'];
$info_decode = json_decode($query_info, true);
?>
                                      <ul class="nav nav-tabs">
                                          <li>
                                              <a class="active" href="#book" data-toggle="tab"><?=ucfirst($info_decode['templateName'])?> </a>
                                          </li>



                                      </ul>
                                      <div class="tab-content">

                                          <div class="tab-pane active" id="book">


                                                              <table id="example1" class="table  table-bordered table-striped table-responsive tablesorter mt-5 ">
                                                                <thead>
                                                                    <tr>
                                                                     <th>Query Issued By </th>
																																	                                                                       <th>Query Issued To</th>
																																	                                                                       <th>Query Reason(s)</th>
																																	                                                                       <th>Query Created Date</th>
																																	                                                                       <th>Query Status</th>
																																	                                                                       <th>Employee Response Date</th>
																																	                                                                       <th> Issueer's Department </th>
																																	                                                                       <th> Receiver's Department </th>
																																	                                                                       <th>Approver</th>
																																	                                                                       <th>Approver's Decision</th>
																																	                                                                       <th>Approver's Comment</th>
																																	                                                                       <th>Approver's Decision Date</th>
																																	                                                                        <th>View </th>


                                                                    </tr>
                                                                </thead>

																<?php foreach ($all_arry as $arry): ?>
																		<tr>
																																																		<th>
																																																		<?php $issued_by_details = getUserDetailsfromHash($arry['query_by']);
echo "{$issued_by_details['SName']} {$issued_by_details['FName']} {$issued_by_details['OName']}";?></th>
																																																		<th>
																																																		<?php $issued_to_details = getUserDetailsfromHash($arry['emp_hash']);
echo "{$issued_to_details['SName']} {$issued_to_details['FName']} {$issued_to_details['OName']}";?></th>

																																																		<th>
																																																			 <?=$arry['query_reason']?>
																																																		</th>
																																																		<th><?=$arry['created_date'] ? $arry['created_date']->format('j F, Y') : '--'?></th>
																																																		<th>
																																																		<?php
if (isset($arry['query_action'])) {
    echo "Completed";
} else if ($arry['query_status'] === 'R') {
    echo "Rejected";
} else if ($arry['query_status'] === 'P' && !isset($arry['approve_reject_date'])) {
    echo "With Approver ";
} else if ($arry['query_status'] === 'A' && isset($arry['approve_reject_date']) && is_null($arry['reciever_status'])) {
    echo "With Employee";
} else if ($arry['query_status'] === 'A' && $arry['reciever_status'] === 'A') {
    echo "With Approver for Decision";
}

?>
																																																		</th>
																																																		<th><?=$arry['emp_res_date'] ? $arry['emp_res_date']->format('j F, Y') : '--'?></th>
																																																		<th>
																																																			 <?=$issued_by_details['Department']?>
																																																		</th>
																																																		<th>
																																																			 <?=$issued_to_details['Department']?>
																																																		</th>
                                                                                                                                                                                                        	<th>
																																																		<?php $approver_detaials = getUserDetailsfromHash($arry['approver']);
echo "{$approver_detaials['SName']} {$approver_detaials['FName']} {$approver_detaials['OName']}";?></th>


                                                        	<th>
																																																			 <?=$arry['query_action']?>
																																																		</th>
                                                        	<th>
																																																			 <?=$arry['query_action_comment']?>
																																																		</th>
                                                                                                                                                                                                        <th><?=$arry['query_action_date'] ? $arry['query_action_date']->format('j F, Y') : '--'?></th>
                                                                                                                                                                                                        <th>
															<?php if (isset($arry['query_action'])): ?>																																<a href="manage_single_query_his.php?tk=ok&current=<?=$arry['id']?>&nam=<?=$_POST['hid'] ? ucfirst($_POST['hid']) : ucfirst($_POST['nam'])?>">View</a>

															<?php endif?>
																																																		</th>


																																																	</tr>

																<?php endforeach;?>
                                                                <tbody>


                                                                </tbody>
                                                              </table>
                                                              <?php include '../main/pagination.php';?>


                                          </div>













                                          <!-- /.tab-content -->
                                      </div>
                                      <!-- /.nav-tabs-custom -->
                                  </div>
                                  <!-- /.col -->
                              </div>
                          </div>













                      </div>











                  </div>

<?php endif;?>






















              </div>
          </div>
          <!-- /.box-body -->




        </div>

















        <!-- /.row -->
        </section>
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<script type="text/javascript">






		 function getDetail(a,event){

		 	 event.preventDefault();

       let curren = $(a).closest("tr").attr("id");
       let nexxt= $(a).closest("tr").next().attr("id");
      let prevv=  $(a).closest("tr").prev().attr("id");

      $("#next"+curren).val(nexxt);
      $("#prev"+curren).val(prevv);

      // console.log(curren,nexxt,prevv);

      $("#form"+curren).submit();


      }


		</script>

		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>


			<!-- MinimalLite Admin App -->
 <script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <!-- MinimalLite Admin for Data Table -->
  <!-- <script src="../assets/js/pages/data-table.js"></script> -->


		 <script>



	$('#example1').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example6').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example7').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

setInterval(function () {
    $( ".tablesorter-filter-row" ).remove();
}, 2000);
		function amp(id){


			var val= $('#'+id).val();
			// console.log(val);

			if (val=='') {
				$('#com'+id).prop('disabled', true);
				window['assignedId']=val;

				 { parent.msgbox('Please select an item', 'green'); };

			}else{
				$('#com'+id).prop('disabled', false);
				window['assignedId']=val;
			}

			}
		 function confirm(id,email){

			 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
			        .dialog({
			          modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {
								let name='<?=$employee_name?>';

			             	 // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

			             	  $('<div></div>').appendTo('body')
			             	 		        .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

			             	 	modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
			             	   	height: 200 + 6,
			             	   	width: 500 + 20,
			             	 	buttons: {

			             	 		Send: function () {

			             	 		var msgC=document.getElementById("sd").value;



			             	 		if (msgC=='') {

			             	 			 { parent.msgbox('Enter custom message', 'red'); };

			             	 			 return;

			             	 		}

			             	 			  $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"confirmed", val:assignedId, msgC:msgC},
	           				                         success:function(response){

	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });




			             	 		  $(this).dialog("close");

			             	 		}
			             	 	}

			             	 	});





			              $(this).dialog("close");
			            },
			            No: function () {

			            		$('<div></div>').appendTo('body')
			            		       .html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
			            		       .dialog({
			            		         modal: true, title: "Confirm Custom Message", zIndex: 10000, autoOpen: true,
			            		         width: 'auto', resizable: true,
			            		         height: 'auto', resizable: true,
			            		         buttons: {
			            		           Yes: function () {


			            		                       	let name='<?=$employee_name?>';
			            		           $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"confirmed", val:assignedId},
	           				                         success:function(response){
	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });






			            		             $(this).dialog("close");
			            		           },
			            		           No: function () {

			            		             $(this).dialog("close");
			            		           }
			            		         },
			            		         close: function (event, ui) {
			            		           $(this).remove();
			            		         }
			            		     });





			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });
		 }

		 function decline(id, email){

		 			 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
			        .dialog({
			          modal: true, title: "Delicine Custom Message", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {
								let name='<?=$employee_name?>';

			             	 // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

			             	  $('<div></div>').appendTo('body')
			             	 		        .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

			             	 	modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
			             	   	height: 200 + 6,
			             	   	width: 500 + 20,
			             	 	buttons: {

			             	 		Send: function () {

			             	 		var msgC=document.getElementById("sd").value;



			             	 		if (msgC=='') {

			             	 			 { parent.msgbox('Enter custom message', 'red'); };

			             	 			 return;

			             	 		}

			             	 			  $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"declined", val:assignedId, msgC:msgC},
	           				                         success:function(response){

	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed, cancellation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });




			             	 		  $(this).dialog("close");

			             	 		}
			             	 	}

			             	 	});





			              $(this).dialog("close");
			            },
			            No: function () {

						    $('<div></div>').appendTo('body')
						        .html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
						        .dialog({
						          modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
						          width: 'auto', resizable: true,
						          height: 'auto', resizable: true,
						          buttons: {
						            Yes: function () {

						            	let name='<?=$employee_name?>';



						            		$.ajax({
							                         url:"add_bookitem.php",
							                         method:"POST",
							                         data:{id:id, email:email, name:name, type:"declined"},
							                         success:function(response){
							                         if(response ==='OK'){

							                           // console.log(allInfo);
							                            { parent.msgbox('Request confirmed, cancellation email has been sent to '+name , 'green'); };
							                            location.reload();




							                         }else if(response==="Wrong"){

							                             { parent.msgbox('Something went wrong', 'red'); };

							                         }
							                       },
							                       error:function(){
							                           alert('Something went wrong');
							                       }
							                    });



						              $(this).dialog("close");
						            },
						            No: function () {
						              $(this).dialog("close");
						            }
						          },
						          close: function (event, ui) {
						            $(this).remove();
						          }
						      });





			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });













































		 }

        $("#getall1").click(function(){

             let bookSelected = $('#bookselect1').val();
		 let selectedOptionText = $('#bookselect1').find(":selected").text();
		 $('#hid').val(selectedOptionText);


           if (bookSelected === "") {



             { parent.msgbox('Please select a query request', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>
	</body>

	</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("QUERY SETTING" . "V") != true) {include '../main/NoAccess.php';exit;}

?>
<?php

if (isset($_POST['SubmitTrans']) && $_POST['SubmitTrans'] === 'Update Settings') {

    $query_undertaking = isset($_POST['query_undertaking']) ? ECh($_POST['query_undertaking']) : "I #employeName#  hereby declare that the details furnished above are true and correct to the best of my knowledge. I undertake to be subject to the disciplinary measures of #companyName# if the information I provided was found false or incorrect. I permit my signature to be attached to my response.";

    $Script = "Update [Settings] set [SetValue18]='" . $query_undertaking . "',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [Setting]= 'querysettings'";
    ScriptRunnerUD($Script, "Inst");

    AuditLog("UPDATE", "Disciplinary Management settings updated");
    echo ("<script type='text/javascript'>{ parent.msgbox('Disciplinary Management setting updated successfully.', 'green'); }</script>");

}

?>

<head>
<link href='../css/style_main.css' rel='stylesheet' type='text/css'/>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<style type="text/css">
	#from_time:before {
	content:'12:30 PM';
	margin-right:.6em;
	color:#9d9d9d;
	}
	#to_time:before {
	content:'12:30 PM';
	margin-right:.6em;
	color:#9d9d9d;
	}
</style>
</head>

<div class="box">
	<div class="box-header with-border  ">
		<div class="row">
			<div class="col-md-12 " >
				<h3 id="rty" >
				Query Settings
				</h3>
			</div>
		</div>
	</div>
    <div class="box-body">
        <form action="" method="Post">
            <div class="col-sm-12" style="padding-left: 0px">
                    <h4> Employee Query Undertaking</h4>
                </div>
            <div class="row">
                <div class="col-md-6">
                    		<div class="form-group row">
					<label class="col-sm-2 col-form-label">Query Undertaking:</label>
					<div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
						<?php
$Script_undertaking = "SELECT SetValue18 FROM Settings WHERE Setting='querysettings'";
$message = ScriptRunner($Script_undertaking, "SetValue18");
if ($message != "") {echo '<textarea name="query_undertaking" rows="4" cols="50"  id="query_undertaking">' . $message . '</textarea>';} else {echo '<textarea name="query_undertaking" rows="4" cols="50" id="query_undertaking"></textarea>';}
?>
						<br>
						<p class="text-danger" ><strong style="font-weight: 600">#employeName# [DON'T REMOVE]:</strong> #employeName# picks up employee's name.</p>
						<br>
						<p class="text-danger"><strong style="font-weight: 600">#companyName# [DON'T REMOVE]:</strong> #companyName# picks up Company's name.</p>
					</div>
</div>
                </div>
                <div class="col-md-6"></div>

            </div>

            <div class="form-group row ml-5">

                <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="Update Settings" />
            </div>
        </form>
    </div>
</div>



<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>


		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>

		<!-- MinimalLite Admin for demo purposes -->
		<script src="../assets/js/demo.js"></script>

		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

		<script type="text/javascript">


			$("#query_employee").click(function(e){
				e.preventDefault();

 			  const query_reason = $("#query_reason").val();
 			  const emp_hash = $("#emp_hash").val();
 			  const query_info = $("#query_info").val();


 			  // console.log(position,name,StkTckHKey,anonymous,concern,resources_needed,desired_benefit);

 			  if(query_reason==="" || emp_hash==="" || query_info==="" ){
 			  		 { parent.msgbox('All fields are are compulsory ', 'red'); };
 			  }
			   else{

					document.getElementById('queryProcess').submit();

 			  }


			})
		</script>


<!-- Bootstrap 4.0-->
<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
				<script src="html2pdf/dist/html2pdf.bundle.js"></script>













	</html>
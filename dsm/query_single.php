<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$td = date("Y-m-d");
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("APPROVE QUERY REQUEST" . "V") != true) {include '../main/NoAccess.php';exit;}

$dis = false;

if (isset($_POST['Decline'])) {
    // var_dump($_POST);
    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);
    // send mail employee
    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $sender_details = getUserDetailsfromHash($data['emp_hash']);

    $msg = "Dear #Name#,<br /> This is to inform you that your request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "Form Request Notification";
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // update
    if ($level_type === 1) {
        $sql = "Update [formrequest] set [level1_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    } else if ($level_type === 2) {
        $sql = "Update [formrequest] set [level2_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    } else if ($level_type === 3) {
        $sql = "Update [formrequest] set [level3_status] = 'C' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
    }
    echo ("<script type='text/javascript'>{parent.msgbox('Request Declined successfully', 'red'); }</script>");
    $dis = true;

}

if (isset($_POST['Approve'])) {

    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);
    // var_dump($sql);
    // var_dump($data);
    if ($level_type === 1) {
        $next_approver = $data['level2'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level

            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Form Request Notification";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

            // update level1_status = A and level2_status = P
            $sql = "Update [formrequest] set [level1_status] = 'A', [level2_status] = 'P' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        } else {
            // send approver mail to employee

            // $next_approver_details = getUserDetailsfromHash($next_approver1);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            // var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Form Request Notification";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // update level1_status = A
            $sql = "Update [formrequest] set [level1_status] = 'A' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        }

    } else if ($level_type === 2) {
        $next_approver = $data['level3'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level
            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Form Request Notification";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

            // update level2_status = A and level3_status = P

            $sql = "Update [formrequest] set [level2_status] = 'A', [level3_status] = 'P' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;

        } else {
            // send approver mail to employee

            $sender_details = getUserDetailsfromHash($data['emp_hash']);

// var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Form Request Notification";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // update level2_status = A

            $sql = "Update [formrequest] set [level2_status] = 'A' WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        }

    } else if ($level_type === 3) {
        // send approver mail to employee

        $sender_details = getUserDetailsfromHash($data['emp_hash']);

        // var_dump($sender_details);

        $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
        $Subj = "Form Request Notification";

        $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $sender_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");

        // update level3_status = A

        $sql = "Update [formrequest] set [level3_status] = 'A' WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
        $dis = true;

    }

}

if (isset($_GET['current'])) {
    // unset($_SESSION)

    $level_type = $_GET['level_type'];
    // var_dump($level_type);

//   $next=$_POST['next'];
    //   $prev=$_POST['prev'];
    $id = (int) sanitize($_GET['current']);
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $item = ScriptRunnercous($sql);
    $reel_info = json_decode($item['query_info'], true);
    $item['nam'] = $_GET['nam'];

    // var_dump($item);
    //   die();

    // var_dump($id,$prev,$next);

}

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
     <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">

		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

    <style type="text/css">

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: px 5px;
    line-height: 20px;
}

.form-control {
    display: block;
    width: 100%;
    padding: .5rem .75rem;
    font-size: 0.8rem;
    line-height: 1.25;
    color: #495057;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.box .box-group>.box {
    margin-bottom: 20px;
}

    </style>




	</head>


	<body>






        <!-- Main content -->
    <section class="content">

        <div class="box">
        <div class="box-header with-border">
         <div class="row">

                <div class="col-md-12  "  >
                  <h3>
                    <?=ucfirst($item['nam'])?>
                  </h3>

                </div>

              </div>
        </div>
         <div class="box-body">




                 <div class="row">
                 <form method="post" id="needs-validation"  style="width:100%">

                              <div class="row mx-4"  id="sortable">



                              </div>
                </form>

                 </div>



<div class="row">
  <?php
// var_dump($item);
?>
  <div class="col-1">
      <form method="post" action="approve_query_request.php" >

        <input type="submit" value="Back" class="btn btn-danger btn-sm">
        <input type="hidden" name="bookselect1" value="<?=$item['query_id']?>">
        <input type="hidden" name="nam" value="<?=$item['nam']?>">


      </form>

  </div>





  <div class="col-7"></div>

















</div>






              </div>
            </div>
        <!-- /.box-body -->




      </div>

















      <!-- /.row -->
    </section>
    <!-- /.content -->
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

		  <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>

		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>

		<!-- MinimalLite Admin for demo purposes -->
		<script src="../assets/js/demo.js"></script>


         <script>
             function convert(str)
      {
//  /[\s\S]*?/g
       str=str.replace(/&amp;/g,"&");
       str=str.replace(/&lt;/g,"<");
       str=str.replace(/&gt;/g,">");
       str=str.replace(/&quot;/g,'"');
       str=str.replace(/&#x27;/g,"'");
       str=str.replace(/&#x2F;/g,'/');
        return str;

      }
     let data = <?php echo json_encode($reel_info) ?>;
    //   let result = data.filter(obj => {
    //     return obj.id === eachgroup;
    //   });
// console.log(data.id);

    //  console.log(result[0].template.replace(/[\s\S]*?/g,''));
     let pageContent=convert(data.template);

     $('#sortable').empty();
     $('#sortable').html(pageContent);


    function amp(id){


      var val= $('#'+id).val();
      // console.log(val);

      if (val=='') {
        $('#com'+id).prop('disabled', true);
        window['assignedId']=val;

         { parent.msgbox('Please select an item', 'green'); };

      }else{
        $('#com'+id).prop('disabled', false);
        window['assignedId']=val;
      }

    }
     function confirm(id,email){

       $('<div></div>').appendTo('body')
              .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
              .dialog({
                modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
                width: 'auto', resizable: true,
                height: 'auto', resizable: true,
                buttons: {
                  Yes: function () {
                let name='<?=$employee_name?>';
                let thirdParty='<?=json_encode($thirdParty)?>';

                     // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                      $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                      modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
                        height: 200 + 6,
                        width: 500 + 20,
                      buttons: {

                        Send: function () {

                        var msgC=document.getElementById("sd").value;



                        let tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';



                        if (msgC=='') {

                           { parent.msgbox('Enter custom message', 'red'); };

                           return;

                        }

                        // console.log(tid);
                        // return;

                            $.ajax({
                                             url:"add_bookitem.php",
                                             method:"POST",
                                             data:{id:id, email:email,name:name,type:"confirmed", val:assignedId, msgC:msgC, thirdParty:thirdParty,tid:tid },
                                             success:function(response){

                                             if(response ==='OK'){

                                               // console.log(allInfo);
                                                { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
                                                location.reload();




                                             }else if(response==="Wrong"){

                                                 { parent.msgbox('Something went wrong', 'red'); };

                                             }
                                           },
                                           error:function(){
                                               alert('Something went wrong');
                                           }
                                        });




                          $(this).dialog("close");

                        }
                      }

                      });





                    $(this).dialog("close");
                  },
                  No: function () {

                      $('<div></div>').appendTo('body')
                             .html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
                             .dialog({
                               modal: true, title: "Confirm Custom Message", zIndex: 10000, autoOpen: true,
                               width: 'auto', resizable: true,
                               height: 'auto', resizable: true,
                               buttons: {
                                 Yes: function () {


                                              let name='<?=$employee_name?>';
                                              let tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';
                                               let thirdParty='<?=json_encode($thirdParty)?>';
                                 $.ajax({
                                             url:"add_bookitem.php",
                                             method:"POST",
                                             data:{id:id, email:email,name:name,type:"confirmed", val:assignedId,thirdParty:thirdParty,tid:tid},
                                             success:function(response){
                                             if(response ==='OK'){

                                               // console.log(allInfo);
                                                { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
                                                location.reload();




                                             }else if(response==="Wrong"){

                                                 { parent.msgbox('Something went wrong', 'red'); };

                                             }
                                           },
                                           error:function(){
                                               alert('Something went wrong');
                                           }
                                        });






                                   $(this).dialog("close");
                                 },
                                 No: function () {

                                   $(this).dialog("close");
                                 }
                               },
                               close: function (event, ui) {
                                 $(this).remove();
                               }
                           });





                    $(this).dialog("close");
                  }
                },
                close: function (event, ui) {
                  $(this).remove();
                }
            });
     }

     function decline(e){
     e.preventDefault()

           $('<div></div>').appendTo('body')
              .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
              .dialog({
                modal: true, title: "Delicine Custom Message", zIndex: 10000, autoOpen: true,
                width: 'auto', resizable: true,
                height: 'auto', resizable: true,
                buttons: {
                  Yes: function () {


                     // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                      $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                      modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
                        height: 200 + 6,
                        width: 500 + 20,
                      buttons: {

                        Send: function () {

                        var msgC=document.getElementById("sd").value;
                        var tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';
                        document.getElementById("decline_reason").value=msgC;



                        if (msgC=='') {

                           { parent.msgbox('Enter custom message', 'red'); };

                           return;

                        }
                          //submit form here
                          document.getElementById("decline_form").submit();

                          $(this).dialog("close");

                        }
                      }

                      });





                    $(this).dialog("close");
                  },
                  No: function () {

                    // submit form here;
                    document . getElementById("decline_form") . submit();

                    $(this).dialog("close");
                  }
                },
                close: function (event, ui) {
                  $(this).remove();
                }
            });













































     }

        $("#getall1").click(function(){

         let bookSelected = $('#bookselect1').val();


           if (bookSelected === "") {



             { parent.msgbox('Please select an item', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>

	</body>

	</html>
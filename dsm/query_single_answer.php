<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$td = date("Y-m-d");
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("ANSWER QUERY" . "V") != true) {include '../main/NoAccess.php';exit;}




$empl_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);

$dis = false;

if (isset($_POST['answer_query'])) {
    // var_dump($_POST);
    $rest = [];
    foreach ($_POST as $key => $value) {
        $rest[$key] = ECh($_POST[$key]);
    }
    if (array_key_exists('answer_query', $rest)) {
        unset($rest['answer_query']);
    }
    if (array_key_exists('id', $rest)) {
        unset($rest['id']);
    }

    $rest = json_encode($rest);
    $id = (int) sanitize($_POST['id']);
    $Script = "UPDATE [query_request] SET [reciever_status] = 'A', [emp_res_date]= GETDATE(), [emp_response]='{$rest}' WHERE [id] ='{$id}'";
    ScriptRunnerUD($Script, "Inst");
    // $reel_info = json_decode($item['query_info'], true);

    // notify employee
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $query_details = ScriptRunnercous($sql);
    // var_dump($query_details);
    $query_info = $query_details['query_info'];
    $info_decode = json_decode($query_info, true);
    // if (strlen($approver) !== 32 && trim($approver) === 'first_sup') {
    //     $approver = getLevel1($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'second_sup') {
    //     $approver = getLevel2($_SESSION['StkTckHKey']);
    // } else if (strlen($approver) !== 32 && trim($approver) === 'third_sup') {
    //     $approver = getLevel3($_SESSION['StkTckHKey']);
    // }
    $approver = $query_details['approver'];
    $correspondences = $info_decode['correspondence'];
    $query_name = $info_decode['templateName'];
    $issuer = $query_details['query_by'];
    $employee = $query_details['emp_hash'];
    $query_reason = $query_details['query_reason'];
    $issuer_details = getUserDetailsfromHash($issuer);
    $employee_details = getUserDetailsfromHash($employee);
    $approver_details = getUserDetailsfromHash($approver);

    $msg = "Dear #Name#,<br />Your response to the query({$query_name}) by employee #EmpName#. You will be notify when he/she makes her decision.";
    $Subj = "Query Request Notification(Employee Response)";
    $msg = str_replace('#Name#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $employee_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");
    // notify issuer

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has responded to the query({$query_name}) you issued due to the reason(s) below : <br/> {$query_reason} <br/> employee #Approver# has been notify and when he/she takes his/her decision you will be notify. ";
    $Subj = "Query Request Notification (Employee Response)";
    $msg = str_replace('#Name#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $issuer_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // send mail to approver
    $msg = "Dear #Name#,<br/>  Employee #EmpName#  has responded to the query({$query_name}) issued by employee #Issuer# due to the reason(s) below : <br/> {$query_reason} <br/>. Kindly login to take a decision on this query.";
    $Subj = "Query Request Notification (Employee Response)";
    $msg = str_replace('#Name#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
    $msg = str_replace('#Issuer#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $approver_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // notify corespondence
    foreach ($correspondences as $correspondence) {
        $correspond = getUserDetailsfromHash($correspondence);
        $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has responded to the query({$query_name}) you issued due to the reason(s) below : <br/> {$query_reason} <br/> employee #Approver# has been notify and when he/she takes his/her decision you will be notify. ";

        $Subj = "Query Request Notification (Employee Response)";
        $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$employee_details['SName']} {$employee_details['FName']} {$employee_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $correspond['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

    }
    AuditLog("Query Answered", "Employee ({$employee}) answered his/her query({$query_name})");

    echo ("<script type='text/javascript'>{parent.msgbox('Query submitted successfully', 'red'); }</script>");
    $dis = true;

    
}

if (isset($_GET['current'])) {
    // unset($_SESSION)

    $level_type = $_GET['level_type'];
    // var_dump($level_type);

//   $next=$_POST['next'];
    //   $prev=$_POST['prev'];
    $id = (int) sanitize($_GET['current']);
    $sql = "SELECT * FROM [query_request] WHERE id=$id";
    $item = ScriptRunnercous($sql);
    $reel_info = json_decode($item['query_info'], true);
    $item['nam'] = $reel_info['templateName'];

    // var_dump($item);
    //   die();

    // var_dump($id,$prev,$next);

}

$Script_undertaking = "SELECT SetValue18 FROM Settings WHERE Setting='querysettings'";
$message_query = ScriptRunner($Script_undertaking, "SetValue18");
	$Script_com="Select SetValue from Settings where Setting='CompName'";
	$company_name = ScriptRunner ($Script_com,"SetValue");
$message_query = str_replace('#employeName#', "{$empl_details['SName']} {$empl_details['FName']} {$empl_details['OName']}", $message_query);
$message_query = str_replace('#companyName#', "$company_name", $message_query);


?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
     <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">

		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

    <style type="text/css">

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: px 5px;
    line-height: 20px;
}

.form-control {
    display: block;
    width: 100%;
    padding: .5rem .75rem;
    font-size: 0.8rem;
    line-height: 1.25;
    color: #495057;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.box .box-group>.box {
    margin-bottom: 20px;
}

    </style>




	</head>


	<body>






        <!-- Main content -->
    <section class="content">

        <div class="box">
        <div class="box-header with-border">
         <div class="row">

                <div class="col-md-12  "  >
                  <h3>
                    <?=ucwords($item['nam'])?>
                  </h3>

                </div>

              </div>
        </div>
         <div class="box-body">



            <?php
if ($dis === false): ?>
                 <div class="row">
                 <form method="post" id="needs-validation"  style="width:100%">

                              <div class="row mx-4"  id="sortable">



                              </div>
                              <div class="form-group">
                              <input type="hidden" name="id" value="<?=$item['id']?>">
                                   <input type="submit" value="Submit" name="answer_query" class="btn btn-danger btn-sm mb-3 ml-3" >
                              </div>
                </form>

                 </div>
            <?php endif;?>



<div class="row">
  <?php
// var_dump($item);
?>
  <div class="col-1">
      <form method="post" action="answer_query.php" >

        <input type="submit" value="Back" class="btn btn-danger btn-sm">


      </form>

  </div>





  <div class="col-7"></div>

















</div>






              </div>
            </div>
        <!-- /.box-body -->




      </div>

















      <!-- /.row -->
    </section>
    <!-- /.content -->
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

		  <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>

		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>

		<!-- MinimalLite Admin for demo purposes -->
		<script src="../assets/js/demo.js"></script>


         <script>
             function convert(str)
      {
//  /[\s\S]*?/g
       str=str.replace(/&amp;/g,"&");
       str=str.replace(/&lt;/g,"<");
       str=str.replace(/&gt;/g,">");
       str=str.replace(/&quot;/g,'"');
       str=str.replace(/&#x27;/g,"'");
       str=str.replace(/&#x2F;/g,'/');
        return str;

      }
     let data = <?php echo json_encode($reel_info) ?>;
    //   let result = data.filter(obj => {
    //     return obj.id === eachgroup;
    //   });
// console.log(data.id);

    //  console.log(result[0].template.replace(/[\s\S]*?/g,''));
     let pageContent=convert(data.template);

     $('#sortable').empty();
     $('#sortable').html(pageContent);

    let empName='<?php echo ucwords("{$empl_details['SName']} {$empl_details['FName']} {$empl_details['ONames']}") ?>';
    let message_query='<?php echo $message_query ?>';

     $('#sortable').append(`<div class="col-md-12">
								      <div class="form-group row ">
								        <div class="demo-checkbox">
										   	<input name="${message_query}" id="EmpDisabled" type="checkbox"  value="Yes" required >	<label for="EmpDisabled"> ${message_query}<span style="color: red">*</span></label>
										</div>

								          </div>
								      </div>
								   </div>`);


    function amp(id){


      var val= $('#'+id).val();
      // console.log(val);

      if (val=='') {
        $('#com'+id).prop('disabled', true);
        window['assignedId']=val;

         { parent.msgbox('Please select an item', 'green'); };

      }else{
        $('#com'+id).prop('disabled', false);
        window['assignedId']=val;
      }

    }
     function confirm(id,email){

       $('<div></div>').appendTo('body')
              .html('<div><class=TextBoxText> Do you wish to <strong>add a custom _query before you confirm this request</strong>?</h5></div>')
              .dialog({
                modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
                width: 'auto', resizable: true,
                height: 'auto', resizable: true,
                buttons: {
                  Yes: function () {
                let name='<?=$employee_name?>';
                let thirdParty='<?=json_encode($thirdParty)?>';

                     // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                      $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                      modal: true,title: "Enter Custom _query", zIndex: 10000, autoOpen: true,
                        height: 200 + 6,
                        width: 500 + 20,
                      buttons: {

                        Send: function () {

                        var msgC=document.getElementById("sd").value;



                        let tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';



                        if (msgC=='') {

                           { parent.msgbox('Enter custom _query', 'red'); };

                           return;

                        }

                        // console.log(tid);
                        // return;

                            $.ajax({
                                             url:"add_bookitem.php",
                                             method:"POST",
                                             data:{id:id, email:email,name:name,type:"confirmed", val:assignedId, msgC:msgC, thirdParty:thirdParty,tid:tid },
                                             success:function(response){

                                             if(response ==='OK'){

                                               // console.log(allInfo);
                                                { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
                                                location.reload();




                                             }else if(response==="Wrong"){

                                                 { parent.msgbox('Something went wrong', 'red'); };

                                             }
                                           },
                                           error:function(){
                                               alert('Something went wrong');
                                           }
                                        });




                          $(this).dialog("close");

                        }
                      }

                      });





                    $(this).dialog("close");
                  },
                  No: function () {

                      $('<div></div>').appendTo('body')
                             .html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
                             .dialog({
                               modal: true, title: "Confirm Custom _query", zIndex: 10000, autoOpen: true,
                               width: 'auto', resizable: true,
                               height: 'auto', resizable: true,
                               buttons: {
                                 Yes: function () {


                                              let name='<?=$employee_name?>';
                                              let tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';
                                               let thirdParty='<?=json_encode($thirdParty)?>';
                                 $.ajax({
                                             url:"add_bookitem.php",
                                             method:"POST",
                                             data:{id:id, email:email,name:name,type:"confirmed", val:assignedId,thirdParty:thirdParty,tid:tid},
                                             success:function(response){
                                             if(response ==='OK'){

                                               // console.log(allInfo);
                                                { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
                                                location.reload();




                                             }else if(response==="Wrong"){

                                                 { parent.msgbox('Something went wrong', 'red'); };

                                             }
                                           },
                                           error:function(){
                                               alert('Something went wrong');
                                           }
                                        });






                                   $(this).dialog("close");
                                 },
                                 No: function () {

                                   $(this).dialog("close");
                                 }
                               },
                               close: function (event, ui) {
                                 $(this).remove();
                               }
                           });





                    $(this).dialog("close");
                  }
                },
                close: function (event, ui) {
                  $(this).remove();
                }
            });
     }

     function decline(e){
     e.preventDefault()

           $('<div></div>').appendTo('body')
              .html('<div><class=TextBoxText> Do you wish to <strong>add a custom _query before you decline this request</strong>?</h5></div>')
              .dialog({
                modal: true, title: "Delicine Custom _query", zIndex: 10000, autoOpen: true,
                width: 'auto', resizable: true,
                height: 'auto', resizable: true,
                buttons: {
                  Yes: function () {


                     // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                      $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                      modal: true,title: "Enter Custom _query", zIndex: 10000, autoOpen: true,
                        height: 200 + 6,
                        width: 500 + 20,
                      buttons: {

                        Send: function () {

                        var msgC=document.getElementById("sd").value;
                        var tid=(document.getElementById("sd2"))?document.getElementById("sd2").value:'0';
                        document.getElementById("decline_reason").value=msgC;



                        if (msgC=='') {

                           { parent.msgbox('Enter custom _query', 'red'); };

                           return;

                        }
                          //submit form here
                          document.getElementById("decline_form").submit();

                          $(this).dialog("close");

                        }
                      }

                      });





                    $(this).dialog("close");
                  },
                  No: function () {

                    // submit form here;
                    document . getElementById("decline_form") . submit();

                    $(this).dialog("close");
                  }
                },
                close: function (event, ui) {
                  $(this).remove();
                }
            });













































     }

        $("#getall1").click(function(){

         let bookSelected = $('#bookselect1').val();


           if (bookSelected === "") {



             { parent.msgbox('Please select an item', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>

	</body>

	</html>
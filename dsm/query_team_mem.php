<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("QUERY TEAM MEMBER" . "V") != true) {include '../main/NoAccess.php';exit;}

?>
<?php
if (isset($_POST['emp_hash'])) {
    $emp_hash = $_POST['emp_hash'];
    $query_id = $_POST['query_info'];
    $query_reason = sanitize(Ech($_POST['query_reason']));
    $query_by = $_SESSION["StkTck" . "HKey"];
    $query = "SELECT * from [querytemplate] where  id='1'";

    $getcheck = ScriptRunnercous($query);

    $groupsarry = json_decode($getcheck['template_name'], true);
    // var_dump($groupsarry);
    $query_info = null;
    foreach ($groupsarry as $eachgroup) {
        if (trim($eachgroup['id']) === trim($query_id)) {
            $query_info = json_encode($eachgroup);
            break;
        }

    }
    // var_dump($query_info);
    $info_decode = json_decode($query_info, true);
    $approver = $info_decode['approver'];
    if (strlen($approver) !== 32 && trim($approver) === 'first_sup') {
        $approver = getLevel1($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'second_sup') {
        $approver = getLevel2($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'third_sup') {
        $approver = getLevel3($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'branch_cont') {
        $approver = getBranchContact($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'branch_cont2') {
        $approver = getBranchContact2($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'branch_cont3') {
        $approver = getBranchContact3($emp_hash);
    } else if (strlen($approver) !== 32 && trim($approver) === 'branch_cont4') {
        $approver = getBranchContact4($emp_hash);
    }

    if (strlen($approver) === 32) {
        $correspondences = $info_decode['correspondence'];
        $query_name = $info_decode['templateName'];
        $issuer = $query_by;

        $query_script = "Insert into [query_request]([query_id],[query_info],[query_reason],[emp_hash],[query_by],[created_date],[query_status],[reciever_status],[query_action],[approver]) Values('{$query_id}','{$query_info}','{$query_reason}','{$emp_hash}','{$query_by}',GETDATE(),'P',null,null,'{$approver}')";
        ScriptRunnerUD($query_script, "Query");

        // var_dump($query_info);

        // send mail to issuer
        $issuer_details = getUserDetailsfromHash($issuer);
        $approver_details = getUserDetailsfromHash($approver);
        $emp_details = getUserDetailsfromHash($emp_hash);

        $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) issued to employee #EmpName# has been sent to #Approver# for approver. If he/she approves/rejects this query, you will be notify accordly.";
        $Subj = "Query Request Notification";
        $msg = str_replace('#Name#', "{$issuer_details['SName']} {$issuer_details['FName']} {$issuer_details['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $issuer_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

        // send mail to approver
        $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) has been issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/>. Kindly login to either approves/rejects this query.";
        $Subj = "Query Request Notification (Pending Approver)";
        $msg = str_replace('#Name#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $approver_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");

        // send mail to corespondence

        foreach ($correspondences as $correspondence) {
            $correspond = getUserDetailsfromHash($correspondence);

            $msg = "Dear #Name#,<br /> This is to inform you that the query({$query_name}) has been issued to employee #EmpName# due to the reason(s) below : <br/> {$query_reason} <br/>.You will be notify when employee #Approver# approves/regects this query request. ";
            $Subj = "Query Request Notification (Pending Approver)";
            $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
            $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $correspond['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
// var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");

        }
        AuditLog("Query Created", "Query created for employee [" . $emp_hash . "]");

        echo ("<script type='text/javascript'>{ parent.msgbox('Query created successfully.', 'green'); }</script>");
    } else {
        // AuditLog("Query Created", "Query created for employee [" . $emp_hash . "]");

        echo ("<script type='text/javascript'>{ parent.msgbox('Invalid employee query approver. kindly contact the Hr team.', 'green'); }</script>");

    }

}

?>

<head>
<link href='../css/style_main.css' rel='stylesheet' type='text/css'/>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<style type="text/css">
	#from_time:before {
	content:'12:30 PM';
	margin-right:.6em;
	color:#9d9d9d;
	}
	#to_time:before {
	content:'12:30 PM';
	margin-right:.6em;
	color:#9d9d9d;
	}
</style>
</head>

    <div class="box">
	<div class="box-header with-border  ">
		<div class="row">
			<div class="col-md-12 " >
				<h3 id="rty" >
				Query team members
				</h3>
			</div>
		</div>
	</div>
	<form action="#" method="post" id="queryProcess" enctype="multipart/form-data" >
	<div class="box-body">
					<div class="row">
					<div class="col-md-3"></div>
						<div class="col-md-6" >
							<div class="form-group row ">
							    <label  class="col-sm-4 col-form-label">Employee Name <span style="color: red">*</span>:</label>
							   	<div class="col-sm-8 input-group-sm">

														<select name="emp_hash" class="form-control" id="emp_hash">
															<option value="" selected>--</option>
															<?php
$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active'  and EmpMgr ='{$_SESSION['StkTckHKey']}'  order by SName Asc");

include '../login/dbOpen4.php';
while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
															<option value="<?php echo $row3['HashKey']; ?>">
															<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
															</option>
															<?php endwhile;
include '../login/dbClose4.php';
?>
														</select>


								</div>
							</div>
								<div class="form-group row ">
	                         				<label  class="col-sm-4 col-form-label">Select Query<span style="color: red">*</span>:</label>
						                    <div class="col-sm-8 " >
											 <select class="form-control " name="query_info" id="query_info" >
                                            <option value="">--</option>
																							<?php
$query = "SELECT * from [querytemplate] where  id='1'";

$getcheck = ScriptRunnercous($query);

$groupsarry = json_decode($getcheck['template_name'], true);

?>

                                              <?php foreach ($groupsarry as $eachgroup): ?>


                                                <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>


                                              <?php endforeach;?>



                                          </select>
						                    </div>



								</div>

							<div class="form-group row">
							    <label  class="col-sm-4 col-form-label">Reason(s) for query <span style="color: red">*</span>:</label>
							    <div class="col-sm-8 " >
									<textarea type="text" rows="5" name="query_reason"  class="form-control" id="query_reason"></textarea>
								</div>
							</div>

							<button  id="query_employee"  class="btn btn-sm btn-danger" > Submit </button>


						</div>
						<div class="col-md-3"></div>


						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>


					<!-- <div class="form-group row">
						<
					</div> -->
				</div>

			</div>
		</div>
	</form>
</div>


<!-- Main content -->



<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>


		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>

		<!-- MinimalLite Admin for demo purposes -->
		<script src="../assets/js/demo.js"></script>

		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

		<script type="text/javascript">


			$("#query_employee").click(function(e){
				e.preventDefault();

 			  const query_reason = $("#query_reason").val();
 			  const emp_hash = $("#emp_hash").val();
 			  const query_info = $("#query_info").val();


 			  // console.log(position,name,StkTckHKey,anonymous,concern,resources_needed,desired_benefit);

 			  if(query_reason==="" || emp_hash==="" || query_info==="" ){
 			  		 { parent.msgbox('All fields are are compulsory ', 'red'); };
 			  }
			   else{

					document.getElementById('queryProcess').submit();

 			  }


			})
		</script>


<!-- Bootstrap 4.0-->
<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
				<script src="html2pdf/dist/html2pdf.bundle.js"></script>













	</html>
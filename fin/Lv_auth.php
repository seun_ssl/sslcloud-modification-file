<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

$AmtMonthly = 0;

if (ValidateURths("PAID LEAVE" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}
$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");


if (!isset($EditID)) {
	$EditID = "";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($Del)) {
	$Del = 0;
}
if (!isset($Err)) {
	$Err = "";
}
if (!isset($ModMaster)) {
	$ModMaster = "";
}
if (!isset($ext)) {
	$ext = "";
}
if (!isset($SelID)) {
	$SelID = "";
}
if (!isset($RetVal)) {
	$RetVal = "";
} //Document Management return value for attached error screen shot.
$BasicPay = 0;

$dbOpen2 = "";
$TAmt = 0;

$PrefixID = $EmpID = $RecDate = $TimeIn = $TimeOut = $LateIn = $EarlyOut = $Normal = $OverTime = $Remark = ""; //Clear all variables

$_SESSION["file_size_dsp"] = "";
$_SESSION["file_name_dsp"] = "";


if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Add" && isset($_REQUEST["Doc"]) && $_REQUEST["Doc"] == "New") {
	echo '<html><head><title></title><link href="../css/style_main.css" rel="stylesheet" type="text/css"></head>
	<body oncontextmenu="return false;" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0"><form name="Upd" method="post" action="#" enctype="multipart/form-data">
	  <table width="507" height="300" border="0" align="center">
		<tr>
		  <td width="497" height="150" align="center" class="STHeaderLabel"></td>
		</tr>
		<tr>
		  <td width="497" align="center" class="STHeaderLabel">Select a document file to upload</td>
		</tr>
		<tr>
		  <td align="center" valign="middle" class="STHeaderLabel"><input type="file" name="dmsupd" id="dmsupd" class="smallButton" width="100" /></td>
		</tr>
		<tr>
		  <td align="center" valign="middle"><label>
			<input type="submit" name="SubmitDoc" id="SubmitDoc" value="Upload" class="smallButton">
			<input type="submit" name="SubmitDoc" id="SubmitDoc" value="View Existing" class="smallButton">
			<input name="PgTy" id="PgTy" type="hidden" value="Loaded" />
			<input name="Doc" id="Doc" type="hidden" value="Loaded" />
			<input name="SearchType" id="SearchType" type="hidden" value="Unauthorized" />
		    <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
		</label></td>
		</tr>
		<tr>
		  <td class="TinyTextRed" align="center">Only CSV comma delimiter files allowed. 
			Max file individual upload size is: 20Mb
			<br /><br />
			<a href="javascript:void()" onClick="../public/sample_download.csv" >View sample upload file</a></td>
		</tr>
	  </table>
	</form>
	</body></html>
	';
	exit;
}


if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["SubmitDoc"]) && $_REQUEST["SubmitDoc"] == "Upload" && isset($_REQUEST["Doc"]) && $_REQUEST["Doc"] == "Loaded") {
		/*These work for me, for 100MB uploads, lasting 2 hours:
	
	In apache-virtual-host:
	-----------------------------------------------------------
	<Directory /var/www/MyProgram>
		AllowOverride Options
	</Directory>
	-----------------------------------------------------------
	
	In .htaccess:
	-----------------------------------------------------------
	php_value session.gc_maxlifetime 10800
	php_value max_input_time         10800
	php_value max_execution_time     10800
	php_value upload_max_filesize    110M
	php_value post_max_size          120M
	-----------------------------------------------------------
	*/

		if (isset($_FILES['dmsupd']['name'])) {
			$ext = findexts($_FILES['dmsupd']['name']);
		}
		if ($ext != "csv") {
			echo ("<script type='text/javascript'>{ parent.msgbox('Invalid file upload type. Please upload a valid document.', 'red'); }</script>");
			//Redirect to Upload Page
			exit;
		}
		//*****************
		$_SESSION["file_ext"] = $ext;
		$_SESSION["file_name_dsp"] = $_FILES["dmsupd"]["name"];
		$_SESSION["file_size"] = ($_FILES["dmsupd"]["size"]);
		$_SESSION["file_size_dsp"] = ($_FILES["dmsupd"]["size"] / 1024);
		$_SESSION["file_hash"] = md5($_FILES["dmsupd"]["name"]);

		if ($_SESSION["file_size_dsp"] > 1024) {
			$_SESSION["file_size_dsp"] = number_format(($_SESSION["file_size_dsp"] / 1024), 2, '.', ',');
			$_SESSION["file_size_dsp"] = $_SESSION["file_size_dsp"] . "Mb";
		} else {
			$_SESSION["file_size_dsp"] = number_format($_SESSION["file_size_dsp"], 2, '.', ',') . "Kb";
		}

		//Move file to application temp folder
		//Value: 0; There is no error, the file uploaded with success.
		//Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
		//Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
		//Value: 3; The uploaded file was only partially uploaded.
		//Value: 4; No file was uploaded.
		//No 5
		//Value: 6; Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.
		//Value: 7; Failed to write file to disk. Introduced in PHP 5.1.0.
		//Vaue: 8; A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded
		//extensions with phpinfo() may help. Introduced in PHP 5.2.0.
		if ($_FILES["dmsupd"]["error"] == 1 || $_FILES["dmsupd"]["error"] == 2) {
			header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File size exceeds maximum allowed<br>contact upload file.</b></font>");
			exit;
		} elseif ($_FILES["dmsupd"]["error"] == 3) {
			header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File upload failed.<br>Please try again.</b></font>");
			exit;
		} elseif ($_FILES["dmsupd"]["error"] > 3) {
			header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File upload failed<br></b></font>");
			exit;
		} else {
			//			move_uploaded_file($_FILES["file"]["tmp_name"], "../players/m_songs/" . $kk . "." . $Ext[1]);

			$CntLn = 0;
			$Script = "Select * from Settings where Setting='AutoEmpID'";
			$IDType = ScriptRunner($Script, "SetValue2");

			$file = fopen($_FILES["dmsupd"]["tmp_name"], "r") or exit("Unable to open file!");
			//Output a line of the file until the end is reached
			while (!feof($file)) {
				$CntLn = $CntLn + 1; // Count what line we are on and jump first two lines of record. Begin posting from third
				//echo $CntLn;
				$Sender = str_replace('"', '', fgets($file)); // replace double quote with nothing. ASCII - 34 = "
				$Sender = str_replace("'", "", $Sender); // replace single quote with nothing. ASCII - 34 = "




				if ($CntLn > 3 && isset($LnStr[7])) {
					$GoValidate = true;
					//Strip the values. Include routine to check if record already exist
					$LnStr = explode(",", $Sender);


					if (strlen(trim($LnStr[0])) < 1) {/*$GoValidate=false; */
					} //No Know Prefix entered
					else {
						$PrefixID = trim($LnStr[0]);
					} //Pass Value to $EmpID 

					if (strlen(trim($LnStr[1])) < 1) {
						$GoValidate = false;
					} //Invalid Employee Number 
					else {
						$EmpID = trim($LnStr[1]);
						if ($IDType == "Long") {
							if (strlen($EmpID) == 1) {
								$EmpID = "000" . $EmpID;
							} elseif (strlen($EmpID) == 2) {
								$EmpID = "00" . $EmpID;
							} elseif (strlen($EmpID) == 3) {
								$EmpID = "0" . $EmpID;
							}
						}

						$EmpID = $PrefixID . $EmpID;
					} //Pass Value to $EmpID

					if (strlen(trim($LnStr[2])) != 11) {
						$GoValidate = false;
					} //Invalid Date Field Format
					else {
						$RecDate = trim($LnStr[2]);
					} //Pass Value to $RecDate 

					if (strlen(trim($LnStr[3])) < 3) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$TimeIn = trim($LnStr[3]);
					} //Pass Value to $TimeIn 

					if (strlen(trim($LnStr[4])) < 3) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$TimeOut = trim($LnStr[4]);
					} //Pass Value to $TimeIn

					if (!is_numeric(trim($LnStr[5]))) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$LateIn = trim($LnStr[5]);
					} //Pass Value to $TimeIn

					if (!is_numeric(trim($LnStr[6]))) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$EarlyOut = trim($LnStr[6]);
					} //Pass Value to $TimeIn

					if (!is_numeric(trim($LnStr[7]))) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$Normal = trim($LnStr[7]);
					} //Pass Value to $TimeIn

					if (!is_numeric(trim($LnStr[8]))) {
						$GoValidate = false;
					} //Invalid Time Field Format
					else {
						$OverTime = trim($LnStr[8]);
					} //Pass Value to $TimeIn

					/*if (strlen(trim($LnStr[8])) > 250)
					{
						$kk = strlen(trim($LnStr[8])) - 250;
						$Remark = substr(trim($LnStr[8]), 0, -$kk); //Ensure max length of Name is 250
					}
					else
					{ 	$Remark = trim($LnStr[8]);}
					*/
					$Remark = "";

					if ($GoValidate == true) {
						$Script = "Select HashKey from EmpTbl where EmpID='" . ECh($EmpID) . "' and EmpStatus='Active' and Status in ('A','U')"; //Check if group already exist
						//echo $Script."<br><br>";
						$EmpID = ScriptRunner($Script, "HashKey");

						if (strlen(trim($EmpID)) != 32) {
							$EmpID = "";
						} //Employee does not exist
						else {
							/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
							$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . $CntLn . "Attendance" . $_SESSION["StkTck" . "UName"];
							$HashKey = md5($UniqueKey);

							$Script = "Select Count(HashKey) Ct from [Fin_Attend] where EmpID='" . $EmpID . "' and RecDate='" . $RecDate . "' and Status<>'D'";
							if (ScriptRunner($Script, "Ct") == 0) {
								$Script = "INSERT into [Fin_Attend] ([EmpID],[RecDate],[TimeIn],[TimeOut],[LateIn],[EarlyOut],[Normal],[OverTime],[Remark],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $EmpID . "','" . ECh($RecDate) . "','" . ECh($TimeIn) . "','" . ECh($TimeOut) . "'," . ECh($LateIn) . "," . ECh($EarlyOut) . "," . ECh($Normal) . "," . ECh($OverTime) . ",'" . ECh($Remark) . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "')";
								//echo $Script;
								ScriptRunnerUD($Script, "Create Records"); //Records inserted. Pending autorization
							}
						} //End Insert
					}
				}
			}
			fclose($file);

			$dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, 
Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut , FA.* 
from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.AddedBy='" . ECh($_SESSION["StkTck" . "HKey"]) . "' and FA.AddedDate between(Convert(varchar(11),GetDate(),106)+' 00:00:00') and (Convert(varchar(11),GetDate(),106)+' 23:59:59')  and FA.Status='N'";
		}
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		$EditID = 0;
		for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
			if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
				$EditID = $_REQUEST["DelBox" . $i];
				//echo $EditID;
				$Script_Edit = "Select * from Fin_Attend where HashKey='" . $EditID . "'";
			}
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);

				$sql = "select [EmpID] from [Fin_LvAllw] WHERE [HashKey] = '" . $EditID . "' ";
				$id = ScriptRunner($sql, "EmpID");

				$current_year = date("Y");

				$sql = "select [EmpID] from [Fin_LvAllw] WHERE [EmpID] = '" . $id . "'   and  [Status] in ('A','B')  and YEAR([PayMonth]) = '$current_year'";
				$id = ScriptRunner($sql, "EmpID");


				if (is_null($id)) {
					$Script = "UPDATE [Fin_LvAllw]
					SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
				  ,[AuthDate] = GetDate()
				  ,[Status] = 'A'
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");

					$Script_update = "
					SELECT [EmpID], [AmtMonthly], 
					Convert(varchar(11),PayMonth,106)  AS PayMonth 
					FROM [Fin_LvAllw] 
					WHERE [HashKey] = '" . $EditID . "'";
					$emp_id = ScriptRunner($Script_update, "EmpID");

					$sql = "Select * from [Fin_PRCore] where status = 'A' and  [EmpID] = '$emp_id'";

					$annual_paye = ScriptRunner($sql, "PAYE");
					$paye_leave_allowance = ScriptRunner($sql, "paye_leave_allowance");
					$NetPay = ScriptRunner($sql, "NetPay");

					$new_paye = ($annual_paye / 12) + $paye_leave_allowance;

					$AmtMonthly = ScriptRunner($Script_update, "AmtMonthly");
					$new_net = ($NetPay / 12) + $AmtMonthly;

					$PayMonth = ScriptRunner($Script_update, "PayMonth");


					$Script_Edit =  "
					UPDATE [Fin_PRIndvPay] 
					SET 
						[LeaveAllowance] = '$AmtMonthly',
						[PAYE] = '$new_paye',
						[NetPay] = '$new_net'
					WHERE 
						[EmpID] = '$emp_id' 
						AND Convert(varchar(11),PayMonth,106)  = '$PayMonth';
				";

					ScriptRunnerUD($Script_Edit, "");


					AuditLog("AUTHORIZE", "Leave allowance data athorized successfully.");
					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee leave data authorized successfully.', 'green'); }</script>");
				} else {

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee leave data has been previously authorized for the year.', 'green'); }</script>");
				}
			}
		}
		$EditID = "";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) {
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);

				//Check if the ststus is 'P' then refuse to UNAUTHORIZE
				$Script = "SELECT Status from [Fin_LvAllw] WHERE [HashKey]='" . $EditID . "'";

				if (strtoupper(ScriptRunner($Script, "Status")) == "B") {
					echo ("<script type='text/javascript'>{ parent.msgbox('Unauthorization failed for already paid leave records','red');}</script>");
				} else {
					$Script = "UPDATE [Fin_LvAllw]
					   SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'U'
						WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Unauthorize");
					AuditLog("UNAUTHORIZE", "Leave payment data unauthorized successfully.");

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee leave payment unauthorized successfully.', 'green'); }</script>");
				}
			}
		}
		$EditID = "";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);

				$Script = "Select Status from Fin_LvAllw where HashKey='" . $EditID . "'";
				if (ScriptRunner($Script, "Status") == 'A') {
					echo ("<script type='text/javascript'>{ parent.msgbox('Cannot delete authorized records.', 'red'); }</script>");
				} else {
					$Script = "UPDATE [Fin_LvAllw] 
					SET Status='D'
					,DeletedBy='" . $_SESSION["StkTck" . "HKey"] . "'
					,DeletedDate= GetDate()
					 where HashKey='" . $EditID . "'";
					ScriptRunnerUD($Script, "Del");

					AuditLog("DELETE", "Leave allowance record(s) deleted successfully");
					echo ("<script type='text/javascript'>{ parent.msgbox('Selected leave allowance record(s) deleted successfully.', 'green'); }</script>");
				}
			}
		}
	}
}
?>

<script>
	$(function() {
		$("#DocRem").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "0D",
			maxDate: "+20Y",
			dateFormat: 'dd-mm-yy'
		})
		$("#DocDelDate").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "0D",
			maxDate: "+20Y",
			dateFormat: 'dd-mm-yy'
		})
	});
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;">
	<form action="#" method="post" name="Records" target="_self" id="Records">
		<div class="box">
			<div class="box-header with-border tdMenu_HeadBlock pt-2 pb-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<p style="color: #000; margin-bottom: 4px;">
							Leave Allowance Authorization
						</p>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12 ">
						<!-- ++++++++++++++++++++++++++++++ BUTTON +++++++++++++++++++++++++++++++-->
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						//------------------------
						if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";
							$dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),PayMonth,106) Pm, Convert(varchar(11),FA.AddedDate,106) AD, 
								AmtPaid, AmtMonthly, PayType , FA.* 
								from Fin_LvAllw FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('N','U')";
						} else {

							print "<h4>AUTHORIZED</h4>";
							$dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),PayMonth,106) Pm, Convert(varchar(11),FA.AddedDate,106) AD, 
							AmtPaid, AmtMonthly, PayType , FA.* 
							from Fin_LvAllw FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A','B','P')";
						}
						//------------------------
						?>
					</div>
					<div class="col-md-12">
						<div class="ViewPatch">
							<table width="100%" align="center" id="table" class="tablesorter table-responsive table">
								<thead>
									<tr>
										<th width="20" valign="middle" scope="col" data-sorter="false">
											<input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
											<label for="DelChk_All"></label>
										</th>
										<!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
										<th align="center" valign="middle" scope="col">Pay Month</th>
										<th align="center" valign="middle" scope="col">Full Name</th>
										<th align="center" valign="middle" scope="col">Department</th>
										<th align="center" valign="middle" scope="col">Amount</th>
										<th align="center" valign="middle" scope="col">Record Type</th>
										<th valign="middle" align="center" scope="col" data-sorter="false">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] <> '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] <> '')) {
										$PayMthDt = " and EmpDt between '" . ECh($_REQUEST["S_RptDate"]) . "' and '" . ECh($_REQUEST["E_RptDate"]) . "' ";
									} else {
										$PayMthDt = '';
									}
									$Del = 0;
									if ($dbOpen2 == "") {
										$dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),PayMonth,106) Pm, Convert(varchar(11),FA.AddedDate,106) AD,
										AmtPaid, AmtMonthly, PayType , FA.*
										from Fin_LvAllw FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('N','U')";
									}
									include '../login/dbOpen2.php';
									//mssql_num_rows
									//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//		return ($row2['$rwfield']);
										$Del = $Del + 1;
										//if ($Del==1) {$PrevID=$row2['ID'];} else {$NextID=$row2['ID'];}
									?>
										<tr>
											<td valign="middle" scope="col" class="TinyTextTight">
												<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
												<label for="<?php echo ("DelBox" . $Del); ?>"></label>
											</td>
											<!--<td width="110" align="left" valign="middle" scope="col"><?php echo (trim($row2['AD'])); ?></td> //-->
											<td align="left" valign="middle" scope="col"><?php echo (trim($row2['Pm'])); ?></td>
											<td align="left" valign="middle" scope="col"><?php echo trim($row2['Nm']); ?></td>
											<td align="left" valign="middle" scope="col"><?php echo $row2['Department']; ?></td>
											<td align="right" valign="middle" scope="col"><?php $AmtMonthly = $AmtMonthly + $row2['AmtMonthly'];
																							echo number_format($row2['AmtMonthly'], 2); ?></td>
											<td align="center" valign="middle" scope="col"><?php echo $row2['AmtPaid']; ?></td>
											<td align="center" valign="middle" scope="col">
												<?php
												if (trim($row2['Status']) == 'N' || trim($row2['Status']) == 'U') {
													echo ('<img src="../images/red.png" height="18" title="Pending Authorization">');
												} elseif (trim($row2['Status']) == 'A' || trim($row2['Status']) == 'B') {
													echo ('<img src="../images/amber.png" height="18" title="Pending Payment">');
												} elseif (trim($row2['Status']) == 'P') {
													echo ('<img src="../images/green.png" height="18" title="Paid">');
												}
												?>
											</td>
										</tr>
									<?php }
									include '../login/dbClose2.php'; ?>
								</tbody>
								<tfoot>
									<tr>
										<td style="font-weight: bolder; background: #EAEAEA; padding-top: 8px; padding-bottom:  8px;" colspan="5" align="right" valign="middle" scope="col" data-sorter="false"><?php echo number_format($AmtMonthly, 2);; ?></td>
										<!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
										<td style="background: #EAEAEA; padding-top: 8px; padding-bottom: 8px;" align="center" valign="middle" scope="col" data-sorter="false">&nbsp;</td>
										<td style="background: #EAEAEA; padding-top: 8px; padding-bottom: 8px;" valign="middle" align="center" scope="col" data-sorter="false">&nbsp;</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-6 pt-5">
						<?php include '../main/pagination.php'; ?>
					</div>
					<div class="col-md-6 pt-5">
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-9">
								<?php
								echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
									  <input name="FAction" id="FAction" type="hidden">';
								//Check if any record was spolled at all before enabling buttons
								//Check if user has Delete rights
								if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {

									if (ValidateURths("PAID LEAVE" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("PAID LEAVE" . "D");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAID LEAVE" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("PAID LEAVE**" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAID LEAVE" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("PAID LEAVE" . "T");
										$but_Type = 'Authorize';
										include '../main/buttons.php';
									}
								} else {

									if (ValidateURths("PAID LEAVE" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("PAID LEAVE" . "");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAID LEAVE" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("PAID LEAVE" . "");
										$but_Type = 'View';
										include '../main/buttons.php';
									}


									if (ValidateURths("PAID LEAVE" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("PAID LEAVE" . "T");
										$but_Type = 'Unauthorize';
										include '../main/buttons.php';
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
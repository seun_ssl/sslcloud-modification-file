<?php session_start();
// var_dump($_SESSION);
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

if (ValidateURths("PAYMENT TEMPLATE" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}
$ModMaster = ValidateURths("PAYROLL MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
	$EditID = "--";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "--";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($Script)) {
	$Script = "";
}
if (!isset($SelID)) {
	$SelID = "";
}

$CR = "";
$DR = "";
$TTaxable = "";
$TPension = "";
$AllItems = "";
$DebitNtTaxed = "";
$DebitTaxed = "";
$niz_gross = "";
$CreditNtTaxed = "";
$GoValidate = true;

$Script = "Select * from Fin_PRCore where HashKey='zzz'";
$dbOpen2 = ("SELECT * from Fin_PRSettings where HashKey='zzz'");
$EmpID_ = "";
$PayScheme_ = "";

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if ((trim($_REQUEST["EmpID"]) == "" || trim($_REQUEST["EmpID"]) == "--") && !isset($_REQUEST["FAction"])) {
		echo ("<script type='text/javascript'>{ parent.msgbox('Select an employee record to generate schedule', 'red'); }</script>");
		$GoValidate = false;
	}

	if ((isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Schedule") && (isset($_REQUEST["EmpID"]) && strlen(trim($_REQUEST["EmpID"])) == 32)) {
		//print "1Stop here ".$_REQUEST["TTaxable"];
		//exit();

		if (ValidateURths("PAYMENT TEMPLATE" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		if (isset($_REQUEST["PayScheme"]) && strlen($_REQUEST["PayScheme"]) == 32) {
			$EmpID_ = ECh($_REQUEST["EmpID"]);
			$Script = "Select HashKey, ([SName]+' '+[FName]+' '+[ONames]+' ['+Convert(varchar(24),Et.EmpID)+']') as EmpPID from EmpTbl Et where Et.HashKey='" . ECh($_REQUEST["EmpID"]) . "' and Et.Status in ('A','U') and Et.EmpStatus='Active'";

			$EmpPID = ScriptRunner($Script, "EmpPID");

			if (trim($EmpPID) == '') {
				echo ("<script type='text/javascript'>{ parent.msgbox('The selected employee does not exist as an active employee. Please review.', 'red'); }</script>");
				$GoValidate = false;
			}

			$Script = "Select Count(*) Ct from Fin_PRCore where EmpID='" . $EmpID_ . "' and Status in ('A','N','U')";
			// I added this line to view unauthorized record
			// $PR_Status = ScriptRunner($Script, "Status");
			if (ScriptRunner($Script, "Ct") == 0) {
				/*if (isset($_REQUEST["ShiftPay"]) && $_REQUEST["ShiftPay"]=="on")
                {$ShiftPay_ = "on";}
                else
                {$ShiftPay_ = "";}*/

				$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "PayMaster" . $_SESSION["StkTck" . "UName"];
				$HashKey = md5($UniqueKey);
				$EditID = $HashKey;
				$EmpID_ = ECh($_REQUEST["EmpID"]);
				$PayScheme_ = ECh($_REQUEST["PayScheme"]); //Get Scheme for the specified employee salary

				$Script = "INSERT INTO [Fin_PRCore]
				(EmpID,[GrossNm],[Scheme],[Status],[AddedBy],[AddedDate],[HashKey])
				VALUES ('" . ECh($EmpID_) . "','','" . $PayScheme_ . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
				//echo $Script;
				ScriptRunnerUD($Script, "NComp");
				AuditLog("INSERT", "New payroll schedule created for employee [" . $EmpPID . "]", $HashKey);
				MailTrail("PAYROLL", "T", '', '', '', '');

				echo ("<script type='text/javascript'>{ parent.msgbox('New payroll master schedule created for employee " . $EmpPID . ".', 'green'); }</script>");
			} else {
				$GoValidate = false;
				echo ("<script type='text/javascript'>{ parent.msgbox('A master schedule already exist for the selected employee. Modify existing','red'); }</script>");
			}
		} else {
			$GoValidate = false;
			echo ("<script type='text/javascript'>{ parent.msgbox('Select a payment scheme which this employee falls under','red'); }</script>");
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************

	//elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Schedule" && $_REQUEST["DelMax"] > 0)
	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Schedule") {
		//print "2Stop here ".$_REQUEST["TTaxable"];
		//exit();

		if (ValidateURths("PAYMENT TEMPLATE" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set HashKey of Account to be Updated */
		$EditID = $_REQUEST["UpdID"];
		$HashKey = $_REQUEST["UpdID"];

		$Script = "select f.MaxAmt from Fin_PRCore c inner join Fin_PRSettings f on c.Scheme = f.Hashkey where c.HashKey='" . $HashKey . "' and f.Status = 'A'";
		$maxAmt = ScriptRunner($Script, "MaxAmt");

		if (str_replace(',', '', trim($_REQUEST["Gross"])) > $maxAmt) {

			echo ("<script type='text/javascript'>{ parent.msgbox('Employee payroll group maximum amount exceeded.', 'green'); }</script>");
			include '../main/clr_val.php';
		} else if ($GoValidate == true) {

			//print "3Stop here ".$_REQUEST["TTaxable"];
			//exit();

			//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
			if (ValidateUpdDel("SELECT Status from [Fin_PRCore] where [HashKey]= '" . $HashKey . "'") == true) {

				// $Script = "select * from Fin_PRCore where HashKey='" . $HashKey . "'";
				// $schme = ScriptRunner($Script, "Scheme");

				// var_dump($_REQUEST);

				$Script = "SELECT EmpID from EmpTbl where HashKey=(select EmpID from Fin_PRCore where HashKey='" . $HashKey . "') ";
				$kk = ScriptRunner($Script, "EmpID");

				if (isset($_REQUEST["paye_x"])  && !empty($_REQUEST["paye_x"])) {
					$paye_leave_allowance = number_format(str_replace(',', '', trim($_REQUEST["paye_x"])), '2', '.', '');
				} else {
					$paye_leave_allowance = 0.0;
				}
				if (isset($_REQUEST["niz_gross"])  && !empty($_REQUEST["niz_gross"])) {
					$v_niz_gross = number_format(str_replace(',', '', trim($_REQUEST["niz_gross"])), '2', '.', '');
				} else {
					$v_niz_gross = 0.0;
				}
				if (isset($_REQUEST["PayItem1"])) {
					$v_PayItem1 = number_format(str_replace(',', '', trim($_REQUEST["PayItem1"])), '2', '.', '');
				} else {
					$v_PayItem1 = 0.0;
				}


				if (isset($_REQUEST["PayItem2"])) {
					$v_PayItem2 = number_format(str_replace(',', '', trim($_REQUEST["PayItem2"])), '2', '.', '');
				} else {
					$v_PayItem2 = 0.0;
				}
				if (isset($_REQUEST["PayItem3"]) && $_REQUEST["PayItem3"] != "") {
					$v_PayItem3 = number_format(str_replace(',', '', trim($_REQUEST["PayItem3"])), '2', '.', '');
				} else {
					$v_PayItem3 = 0.0;
				}
				if (isset($_REQUEST["PayItem4"])) {
					$v_PayItem4 = number_format(str_replace(',', '', trim($_REQUEST["PayItem4"])), '2', '.', '');
				} else {
					$v_PayItem4 = 0.0;
				}
				if (isset($_REQUEST["PayItem5"])) {
					$v_PayItem5 = number_format(str_replace(',', '', trim($_REQUEST["PayItem5"])), '2', '.', '');
				} else {
					$v_PayItem5 = 0.0;
				}
				if (isset($_REQUEST["PayItem6"])) {
					$v_PayItem6 = number_format(str_replace(',', '', trim($_REQUEST["PayItem6"])), '2', '.', '');
				} else {
					$v_PayItem6 = 0.0;
				}
				if (isset($_REQUEST["PayItem7"])) {
					$v_PayItem7 = number_format(str_replace(',', '', trim($_REQUEST["PayItem7"])), '2', '.', '');
				} else {
					$v_PayItem7 = 0.0;
				}
				if (isset($_REQUEST["PayItem8"])) {
					$v_PayItem8 = number_format(str_replace(',', '', trim($_REQUEST["PayItem8"])), '2', '.', '');
				} else {
					$v_PayItem8 = 0.0;
				}
				if (isset($_REQUEST["PayItem9"])) {
					$v_PayItem9 = number_format(str_replace(',', '', trim($_REQUEST["PayItem9"])), '2', '.', '');
				} else {
					$v_PayItem9 = 0.0;
				}
				if (isset($_REQUEST["PayItem10"])) {
					$v_PayItem10 = number_format(str_replace(',', '', trim($_REQUEST["PayItem10"])), '2', '.', '');
				} else {
					$v_PayItem10 = 0.0;
				}
				if (isset($_REQUEST["PayItem11"])) {
					$v_PayItem11 = number_format(str_replace(',', '', trim($_REQUEST["PayItem11"])), '2', '.', '');
				} else {
					$v_PayItem11 = 0.0;
				}
				if (isset($_REQUEST["PayItem12"])) {
					$v_PayItem12 = number_format(str_replace(',', '', trim($_REQUEST["PayItem12"])), '2', '.', '');
				} else {
					$v_PayItem12 = 0.0;
				}
				if (isset($_REQUEST["PayItem13"])) {
					$v_PayItem13 = number_format(str_replace(',', '', trim($_REQUEST["PayItem13"])), '2', '.', '');
				} else {
					$v_PayItem13 = 0.0;
				}
				if (isset($_REQUEST["PayItem14"])) {
					$v_PayItem14 = number_format(str_replace(',', '', trim($_REQUEST["PayItem14"])), '2', '.', '');
				} else {
					$v_PayItem14 = 0.0;
				}
				if (isset($_REQUEST["PayItem15"])) {
					$v_PayItem15 = number_format(str_replace(',', '', trim($_REQUEST["PayItem15"])), '2', '.', '');
				} else {
					$v_PayItem15 = 0.0;
				}
				if (isset($_REQUEST["PayItem16"])) {
					$v_PayItem16 = number_format(str_replace(',', '', trim($_REQUEST["PayItem16"])), '2', '.', '');
				} else {
					$v_PayItem16 = 0.0;
				}
				if (isset($_REQUEST["PayItem17"])) {
					$v_PayItem17 = number_format(str_replace(',', '', trim($_REQUEST["PayItem17"])), '2', '.', '');
				} else {
					$v_PayItem17 = 0.0;
				}
				if (isset($_REQUEST["PayItem18"])) {
					$v_PayItem18 = number_format(str_replace(',', '', trim($_REQUEST["PayItem18"])), '2', '.', '');
				} else {
					$v_PayItem18 = 0.0;
				}
				if (isset($_REQUEST["PayItem19"])) {
					$v_PayItem19 = number_format(str_replace(',', '', trim($_REQUEST["PayItem19"])), '2', '.', '');
				} else {
					$v_PayItem19 = 0.0;
				}
				if (isset($_REQUEST["PayItem20"])) {
					$v_PayItem20 = number_format(str_replace(',', '', trim($_REQUEST["PayItem20"])), '2', '.', '');
				} else {
					$v_PayItem20 = 0.0;
				}
				if (isset($_REQUEST["PayItem21"])) {
					$v_PayItem21 = number_format(str_replace(',', '', trim($_REQUEST["PayItem21"])), '2', '.', '');
				} else {
					$v_PayItem21 = 0.0;
				}
				if (isset($_REQUEST["PayItem22"])) {
					$v_PayItem22 = number_format(str_replace(',', '', trim($_REQUEST["PayItem22"])), '2', '.', '');
				} else {
					$v_PayItem22 = 0.0;
				}
				if (isset($_REQUEST["PayItem23"])) {
					$v_PayItem23 = number_format(str_replace(',', '', trim($_REQUEST["PayItem23"])), '2', '.', '');
				} else {
					$v_PayItem23 = 0.0;
				}
				if (isset($_REQUEST["PayItem24"])) {
					$v_PayItem24 = number_format(str_replace(',', '', trim($_REQUEST["PayItem24"])), '2', '.', '');
				} else {
					$v_PayItem24 = 0.0;
				}
				if (isset($_REQUEST["PayItem25"])) {
					$v_PayItem25 = number_format(str_replace(',', '', trim($_REQUEST["PayItem25"])), '2', '.', '');
				} else {
					$v_PayItem25 = 0.0;
				}

				//if (isset($_REQUEST["PensionEmployee"])){
				if ($_REQUEST["PensionEmployee"] != "") {
					$v_PensionEmployee = number_format(str_replace(',', '', trim($_REQUEST["PensionEmployee"])), '2', '.', '');
				} else {
					$v_PensionEmployee = 0.0;
				}

				if (isset($_REQUEST["PensionEmployer"])) {
					$v_PensionEmployer = number_format(str_replace(',', '', trim($_REQUEST["PensionEmployer"])), '2', '.', '');
				} else {
					$v_PensionEmployer = 0.0;
				}
				if (isset($_REQUEST["LeaveAllowance"]) && is_numeric(str_replace(',', '', trim($_REQUEST["LeaveAllowance"])))) {
					$v_LeaveAllowance = number_format(str_replace(',', '', trim($_REQUEST["LeaveAllowance"])), '2', '.', '');
				} else {
					$v_LeaveAllowance = 0.0;
				}

				if (isset($_REQUEST["TRelief"]) && is_numeric(str_replace(',', '', trim($_REQUEST["TRelief"])))) {
					$v_TRelief = number_format(str_replace(',', '', trim($_REQUEST["TRelief"])), '2', '.', '');
				} else {
					$v_TRelief = 0.0;
				}

				//if (isset($_REQUEST["PAYE"])){
				if ($_REQUEST["PAYE"] != "") {
					$v_PAYE = number_format(str_replace(',', '', trim($_REQUEST["PAYE"])), '2', '.', '');
				} else {
					$v_PAYE = 0.0;
				}

				//if (isset($_REQUEST["TTaxable"])){
				if ($_REQUEST["TTaxable"] != "") {
					$v_TTaxable = number_format(str_replace(',', '', trim($_REQUEST["TTaxable"])), '2', '.', '');
				} else {
					$v_TTaxable = 0.0;
				}

				$v_NetPay = 0.0;

				//if (isset($_REQUEST["NetPay"]))
				if ($_REQUEST["NetPay"] != "") {
					$v_NetPay = number_format(str_replace(',', '', trim($_REQUEST["NetPay"])), '2', '.', '');
					if (!is_numeric($v_NetPay)) {
						$v_NetPay = 0.0;
					}
				}

				if (isset($_REQUEST["AuthCalc"])) {
					$AutoCalc_ = 'on';
				} else {
					$AutoCalc_ = '';
				}

				/*if (isset($_REQUEST["ShiftPay"]) && $_REQUEST["ShiftPay"]=="on")
                {$ShiftPay_ = "on";} else {$ShiftPay_ = "";}*/

				if (isset($_REQUEST["Gross"])) {
					$v_Gross = number_format(str_replace(',', '', trim($_REQUEST["Gross"])), '2', '.', '');
				} else {
					$v_Gross = 0.0;
				}
				//                echo $_REQUEST["Gross"]."===========".$v_Gross;

				$Script = "UPDATE [Fin_PRCore]
				SET
				[Gross] ='" . ECh($v_Gross) . "'
				,[PayItem1] ='" . ECh($v_PayItem1) . "'
				,[niz_gross] ='" . ECh($v_niz_gross) . "'
				,[paye_leave_allowance] ='" . ECh($paye_leave_allowance) . "'
				,[PayItem2] ='" . ECh($v_PayItem2) . "'
				,[PayItem3] ='" . ECh($v_PayItem3) . "'
				,[PayItem4] ='" . ECh($v_PayItem4) . "'
				,[PayItem5] ='" . ECh($v_PayItem5) . "'
				,[PayItem6] ='" . ECh($v_PayItem6) . "'
				,[PayItem7] ='" . ECh($v_PayItem7) . "'
				,[PayItem8] ='" . ECh($v_PayItem8) . "'
				,[PayItem9] ='" . ECh($v_PayItem9) . "'
				,[PayItem10] ='" . ECh($v_PayItem10) . "'
				,[PayItem11] ='" . ECh($v_PayItem11) . "'
				,[PayItem12] ='" . ECh($v_PayItem12) . "'
				,[PayItem13] ='" . ECh($v_PayItem13) . "'
				,[PayItem14] ='" . ECh($v_PayItem14) . "'
				,[PayItem15] ='" . ECh($v_PayItem15) . "'
				,[PayItem16] ='" . ECh($v_PayItem16) . "'
				,[PayItem17] ='" . ECh($v_PayItem17) . "'
				,[PayItem18] ='" . ECh($v_PayItem18) . "'
				,[PayItem19] ='" . ECh($v_PayItem19) . "'
				,[PayItem20] ='" . ECh($v_PayItem20) . "'
				,[PayItem21] ='" . ECh($v_PayItem21) . "'
				,[PayItem22] ='" . ECh($v_PayItem22) . "'
				,[PayItem23] ='" . ECh($v_PayItem23) . "'
				,[PayItem24] ='" . ECh($v_PayItem24) . "'
				,[PayItem25] ='" . ECh($v_PayItem25) . "'
				,[PensionEmployee] ='" . ECh($v_PensionEmployee) . "'
				,[PensionEmployer] ='" . ECh($v_PensionEmployer) . "'
				,[PAYE] ='" . ECh($v_PAYE) . "'
				,[LeaveAllowance] ='" . ECh($v_LeaveAllowance) . "'

				,[TTaxable] ='" . ECh($v_TTaxable) . "'

				,[NetPay] ='" . ECh($v_NetPay) . "'
				,[TRelief] ='" . ECh($v_TRelief) . "'

				,[Scheme] ='" . ECh($_REQUEST["PayScheme"]) . "'
				,[AuthCalc] ='" . $AutoCalc_ . "'
				,[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "'
				,[UpdatedDate] = GetDate()
				,[Status] = 'U'
				WHERE [HashKey] = '" . $EditID . "'";

				//,[TTaxable] ='".ECh($v_TTaxable)."'
				//

				// print_r($Script);


				ScriptRunnerUD($Script, "Inst");
				//AuditLog("INSERT","Employee payroll schedule updated for account ID [".$kk."]");
				//MailTrail("PAYROLL","T",'','','','');

				//Reopen the updated schedule. After SAVE file is not closed automatically
				$EditID = $HashKey;
				$EmpID_ = ECh($_REQUEST["EmpID"]);
				$PayScheme_ = ECh($_REQUEST["PayScheme"]); //Get Scheme for the specified employee salary
				$Script_Edit = "select * from [Fin_PRCore] where HashKey='" . $EditID . "'";

				echo ("<script type='text/javascript'>{ parent.msgbox('Employee payroll schedule updated successfully', 'green'); }</script>");
			} else {
				echo ("<script type='text/javascript'>{ parent.msgbox('Cannot update authorized record', 'red'); }</script>");
				include '../main/clr_val.php'; /* CLEARS $EditID */
			}
		}
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
		include '../main/clr_val.php';
	}

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected") {
		if (ValidateURths("PAYMENT TEMPLATE" . "V") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {

				$EditID = ECh($HashVals[$i]);
				$Script_Edit = "select * from [Fin_PRCore] where HashKey='" . $EditID . "'";
				$EmpID_ = ScriptRunner($Script_Edit, "EmpID");
				$PayScheme_ = ScriptRunner($Script_Edit, "Scheme");
				//Get Scheme for the specified employee salary
				$PR_Status = ScriptRunner($Script_Edit, "Status");
			}
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("PAYMENT TEMPLATE" . "V") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) {
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "Select ([SName]+' '+[FName]+' '+[ONames]+' ['+[EmpID]+']') as EmpPID, HashKey from EmpTbl where [HashKey]=(select EmpID from Fin_PRCore where HashKey='" . $EditID . "') and ([Status] not in ('D','R','T'))";
				//$EditID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [Fin_PRCore] WHERE [HashKey]='" . $EditID . "'") == true) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [Fin_PRCore]
					SET [Status] = 'A'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";

					//                    echo $Script;
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("AUTHORIZE", "Employee payroll template details authorized for " . ScriptRunner($Script_ID, "EmpPID"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee payroll template authorized successfully.', 'green'); }</script>");
				}
			}
		}
		$EditID = "";
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("PAYMENT TEMPLATE" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "Select ([SName]+' '+[FName]+' '+[ONames]+' ['+[EmpID]+']') as EmpPID, HashKey from EmpTbl where EmpStatus='Active' and  [HashKey]=(select EmpID from Fin_PRCore where HashKey='" . $EditID . "') and ([Status] not in ('D','R','T'))";

				$EmpPID = ScriptRunner($Script_ID, "EmpPID");
				$NwEmpID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [Fin_PRCore] WHERE [HashKey] = '" . $EditID . "'") == false) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [Fin_PRCore]
					SET [Status] = 'U'
					,[UpdatedBy] = '" . $_SESSION["StkTck" . "HKey"] . "'
					,[UpdatedDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("UNAUTHORIZE", "Employee payroll template unauthorized for " . $EmpPID);

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee account(s) unauthorized successfully.', 'green'); }</script>");
					// commented out by Seun
					//                     if (trim($NwEmpID) == '') //Employee record HashKey Check
					//                     {
					//                         echo ("<script type='text/javascript'>{ parent.msgbox('The selected record does not exist as an active employee. Unauthorize failed.', 'red'); }</script>");
					//                     } elseif (strlen($NwEmpID) == 32) //Employee record HashKey Check
					//                     {
					//                         //Create New Payroll record
					//                         $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "PayMaster" . $_SESSION["StkTck" . "UName"];
					//                         $HashKey = md5($UniqueKey);

					//                         $dbOpen2 = ("SELECT * from Fin_PRCore where HashKey='" . $EditID . "'");
					//                         include '../login/dbOpen2.php';
					//                         while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
					//                             $Script = "INSERT INTO [Fin_PRCore]    ([EmpID],[Gross],[NetPay],[TTaxable],[TRelief],[PAYE],[LeaveAllowance],[PensionEmployer],[PensionEmployee],[Scheme],[PayItem1],[PayItem2],[PayItem3],[PayItem4],[PayItem5],[PayItem6],[PayItem7],[PayItem8],[PayItem9],[PayItem10],[PayItem11],[PayItem12],[PayItem13],[PayItem14],[PayItem15],[PayItem16],[PayItem17],[PayItem18],[PayItem19],[PayItem20],[PayItem21],[PayItem22],[PayItem23],[PayItem24],[PayItem25],[Status],[AddedBy],[AddedDate],[UpdatedBy],[UpdatedDate],[HashKey])
					//                     VALUES ('" . $NwEmpID . "'," . $row2['Gross'] . "," . $row2['NetPay'] . "," . $row2['TTaxable'] . "," . $row2['TRelief'] . "," . $row2['PAYE'] . "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployer'] . "," . $row2['PensionEmployee'] . ",'" . $row2['Scheme'] . "'," . $row2['PayItem1'] . "," . $row2['PayItem2'] . "," . $row2['PayItem3'] . "," . $row2['PayItem4'] . "," . $row2['PayItem5'] . "," . $row2['PayItem6'] . "," . $row2['PayItem7'] . "," . $row2['PayItem8'] . "," . $row2['PayItem9'] . "," . $row2['PayItem10'] . "," . $row2['PayItem11'] . "," . $row2['PayItem12'] . "," . $row2['PayItem13'] . "," . $row2['PayItem14'] . "," . $row2['PayItem15'] . "," . $row2['PayItem16'] . "," . $row2['PayItem17'] . "," . $row2['PayItem18'] . "," . $row2['PayItem19'] . "," . $row2['PayItem20'] . "," . $row2['PayItem21'] . "," . $row2['PayItem22'] . "," . $row2['PayItem23'] . "," . $row2['PayItem24'] . "," . $row2['PayItem25'] . ",'U','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";

					// //                    echo $Script;
					//                             ScriptRunnerUD($Script, "NComp");
					//                         }

					//                         echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee account(s) unauthorized successfully.', 'green'); }</script>");
					//                     }
				}
			}
		}
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("PAYMENT TEMPLATE" . "D") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT EmpID, HashKey from Fin_PRCore where HashKey='" . $EditID . "'";
				$EditID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [Fin_PRCore] WHERE [HashKey] = '" . $EditID . "'") == true) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [Fin_PRCore]
					SET [Status] = 'D'
					,[DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[DeletedDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("DELETE", "Employee account(s) deleted for " . ScriptRunner($Script_ID, "EmpID"), $HashKey);

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee pay template deleted successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		include '../main/clr_val.php'; /* CLEARS $EditID */
	}
}
?>


<script type="text/javascript">
	function GetTotal() {
		$Basic = 0;
		$NetPay = 0;
		$GrossPay = 0;
		$Total_DR = 0;
		$Total_CR = 0;
		$TotAllowance = 0;
		$Total_TX = 0;
		$Total_PEN = 0;
		NwGross = 0;
		$TRelief = 0;
		// $niz_gross = 0;

		// alert('dgdg');

		//SORT OUT ALL VALUES
		var arrayOfStrings = document.getElementById("H_ALL").value.split(";");
		// console.log('all', arrayOfStrings);
		var NwGross = document.getElementById("Gross").value.replace(',', '');
		NwGross = NwGross.replace(',', '');
		NwGross = NwGross.replace(',', '');
		NwGross = NwGross.replace(',', '');
		NwGross = NwGross.replace(',', '');
		NwGross = NwGross.replace(',', '');
		NwGross = NwGross.replace(',', ''); //Do not remove. It was not replacing all the "," comma's at once.




		//AUTOCAL IS HERE
		if (document.getElementById("AuthCalc").checked == true) {

			for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
				var ElementNm = document.getElementById(arrayOfStrings[i]);
				var NwElement = "PCent_" + arrayOfStrings[i];
				var NwPCent = document.getElementById(NwElement);

				if ((document.getElementById(NwElement).value * 1) > 0) {
					//				var NwGross=document.getElementById("Gross").value.replace(',','');
					//				NwGross = NwGross.replace(',','');
					//				NwGross = NwGross.replace(',','');
					//				NwGross = NwGross.replace(',',''); //Do not remove. It was not replacing all the "," comma's at once.
					document.getElementById(NwElement).value = document.getElementById(NwElement).value.replace(',', '');
					ElementNm.value = parseFloat(NwGross).toFixed(2) * (parseFloat(document.getElementById(NwElement).value).toFixed(2) / 100);
				}

				//Reset Element back to standards with comma and decimal place
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');

				ElementNm.value = thousand_separator(parseFloat(ElementNm.value).toFixed(2));
			}


			//+++++++++++++++++++++++++++++++++++++ INSERT START ++++++++++++++++++++++++++++++++++++++++++
			var $Total_CR = 0;
			if (document.getElementById("H_CR").value != "") {
				var arrayOfStrings = document.getElementById("H_CR").value.split(";");
				// console.log('cr',arrayOfStrings);
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');

					$T_CR = ElementNm.value.replace(',', '');
					if (parseFloat($T_CR).toFixed(2) > 0) {
						if ($Total_CR == 0) {
							$Total_CR = parseFloat($T_CR).toFixed(2);
						} else {
							$Total_CR = (parseFloat($Total_CR).toFixed(2) * 1) + (parseFloat($T_CR).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}

					//Reset Element back to standards with comma and decimal place
					ElementNm.value = thousand_separator(parseFloat(ElementNm.value).toFixed(2));
					//alert($Total_CR);
				}
			}

			//Get TOTAL DEBIT VALUE
			var $Total_DR = 0;
			if (document.getElementById("H_DR").value != "") {
				var arrayOfStrings = document.getElementById("H_DR").value.split(";");
				console.log('dr', arrayOfStrings);
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');

					$T_DR = ElementNm.value.replace(',', '');
					//			alert($T_DR);
					if (parseFloat($T_DR).toFixed(2) > 0) {
						if ($Total_DR == 0) {
							$Total_DR = parseFloat($T_DR).toFixed(2);
						} else {
							$Total_DR = (parseFloat($Total_DR).toFixed(2) * 1) + (parseFloat($T_DR).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}

					//Reset Element back to standards with comma and decimal place
					ElementNm.value = thousand_separator(parseFloat(ElementNm.value).toFixed(2));
				}
			}


			//Get TOTAL PEN VALUE
			var $Total_PEN = 0;
			// alert(document.getElementById("H_Pension").value)
			// if (document.getElementById("H_Pension").value != "")
			// {
			var arrayOfStrings = document.getElementById("H_Pension").value.split(";");
			// console.log('pension', arrayOfStrings );
			for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
				var ElementNm = document.getElementById(arrayOfStrings[i]);
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				ElementNm.value = ElementNm.value.replace(',', '');
				$T_PEN = ElementNm.value.replace(',', '');
				// console.log($T_PEN)

				if (parseFloat($T_PEN).toFixed(2) > 0) {
					if ($Total_PEN == 0) {
						$Total_PEN = parseFloat($T_PEN).toFixed(2);
					} else {
						$Total_PEN = (parseFloat($Total_PEN).toFixed(2) * 1) + (parseFloat($T_PEN).toFixed(2) * 1);
					}
				} else {
					ElementNm.value = "0.00";
				}

				//Reset Element back to standards with comma and decimal place
				ElementNm.value = thousand_separator(parseFloat(ElementNm.value).toFixed(2));
			}


			document.getElementById("PensionEmployee").value = $Total_PEN * parseFloat((document.getElementById("PCent_PensionEmployee").value * 1) / 100).toFixed(2);
			document.getElementById("PensionEmployer").value = $Total_PEN * parseFloat((document.getElementById("PCent_PensionEmployer").value * 1) / 100).toFixed(2);
			$Pension = $Total_PEN * parseFloat((document.getElementById("PCent_PensionEmployee").value * 1) / 100).toFixed(2);
			// alert($Pension);



			document.getElementById("PensionEmployee").value = thousand_separator(parseFloat(document.getElementById("PensionEmployee").value).toFixed(2));
			document.getElementById("PensionEmployer").value = thousand_separator(parseFloat(document.getElementById("PensionEmployer").value).toFixed(2));

			$Basic = document.getElementById("PayItem1").value.replace(',', '');
			$Basic = $Basic.replace(',', '');
			$Basic = $Basic.replace(',', '');
			$Basic = $Basic.replace(',', '');
			$Basic = $Basic.replace(',', '');
			$Basic = $Basic.replace(',', '');

			// console.log($Basic);
			var leavePerGross = document.getElementById("leavePerGross").value;
			var per_leave = document.getElementById("PCent_LeaveAllw").value * 1;
			var $Gross_pay = (parseFloat(Math.round(document.getElementById("Gross").value.replace(/\,/g, '') * 100) / 100).toFixed(2));
			// console.log({
			// 	leavePerGross,
			// 	$Gross_pay,
			// 	per_leave
			// });
			if (leavePerGross == "on") {

				// alert("here");
				$LeaveA = parseFloat(($Gross_pay / 12 * document.getElementById("PCent_LeaveAllw").value) / 100).toFixed(2);
				var leave_al = document.getElementById("LeaveAllowance").value = parseFloat(($Gross_pay / 12 * document.getElementById("PCent_LeaveAllw").value) / 100).toFixed(2);
				document.getElementById("LeaveAllowance").value = thousand_separator(parseFloat(document.getElementById("LeaveAllowance").value).toFixed(2));

			} else {
				// alert("here2");
				$LeaveA = parseFloat(($Basic * document.getElementById("PCent_LeaveAllw").value) / 100).toFixed(2);
				var leave_al = document.getElementById("LeaveAllowance").value = parseFloat(($Basic * document.getElementById("PCent_LeaveAllw").value) / 100).toFixed(2);
				document.getElementById("LeaveAllowance").value = thousand_separator(parseFloat(document.getElementById("LeaveAllowance").value).toFixed(2));


			}


			// console.log({
			// 	leave_al,

			// });



			//Get TOTAL TAXABLE VALUE
			var $Total_TX = 0;
			if (document.getElementById("H_TTaxable").value != "") {
				var arrayOfStrings = document.getElementById("H_TTaxable").value.split(";");
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					//alert($Total_TX);
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					$T_TX = ElementNm.value.replace(',', '');
					$T_TX = $T_TX.replace(',', '');
					$T_TX = $T_TX.replace(',', '');
					$T_TX = $T_TX.replace(',', '');
					$T_TX = $T_TX.replace(',', '');
					$T_TX = $T_TX.replace(',', '');

					if (parseFloat($T_TX).toFixed(2) > 0) {
						if ($Total_TX == 0) {
							$Total_TX = parseFloat($T_TX).toFixed(2);
						} else {
							$Total_TX = (parseFloat($Total_TX).toFixed(2) * 1) + (parseFloat($T_TX).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}
				}
			}
			// console.log($Total_TX);
			// console.log(parseFloat(leave_al).toFixed(2) *1);
			//  $Total_TX=$Total_TX + (parseFloat(leave_al).toFixed(2)* 1);


			//Get TOTAL DEBIT VALUE NOT Taxable
			var $Total_DR_NTax = 0;
			if (document.getElementById("DebitNtTax").value != "") {
				var arrayOfStrings = document.getElementById("DebitNtTax").value.split(";");
				console.log({
					arrayOfStrings
				});
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					$T_DR_NTax = ElementNm.value.replace(',', '');

					if (parseFloat($T_DR_NTax).toFixed(2) > 0) {
						if ($Total_DR_NTax == 0) {
							$Total_DR_NTax = parseFloat($T_DR_NTax).toFixed(2);
						} else {
							$Total_DR_NTax = (parseFloat($Total_DR_NTax).toFixed(2) * 1) + (parseFloat($T_DR_NTax).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}
				}
			}


			//Get TOTAL DEBIT VALUE Taxable
			var $Total_DR_Tax = 0;
			if (document.getElementById("DebitTax").value != "") {
				var arrayOfStrings = document.getElementById("DebitTax").value.split(";");
				console.log({
					arrayOfStrings
				});
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					$T_DR_Tax = ElementNm.value.replace(',', '');

					if (parseFloat($T_DR_Tax).toFixed(2) > 0) {
						if ($Total_DR_Tax == 0) {
							$Total_DR_Tax = parseFloat($T_DR_Tax).toFixed(2);
						} else {
							$Total_DR_Tax = (parseFloat($Total_DR_Tax).toFixed(2) * 1) + (parseFloat($T_DR_Tax).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}
				}
			}








			//Get TOTAL RELIEF VALUE (tax or not taxed) $Total_RL_NTax equals all relief be it taxed or not taxed
			var $Total_RL_NTax = 0;
			if (document.getElementById("ReliefNtTax").value != "") {
				var arrayOfStrings = document.getElementById("ReliefNtTax").value.split(";");
				// console.log(arrayOfStrings);
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					$T_RL_NTax = ElementNm.value.replace(',', '');

					if (parseFloat($T_RL_NTax).toFixed(2) > 0) {
						if ($Total_RL_NTax == 0) {
							$Total_RL_NTax = parseFloat($T_RL_NTax).toFixed(2);
						} else {
							$Total_RL_NTax = (parseFloat($Total_RL_NTax).toFixed(2) * 1) + (parseFloat($T_RL_NTax).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}
				}
			}
			// console.log($Total_RL_NTax);
			// added by Seun
			//Get TOTAL CREDIT NOT Taxable VALUE
			var $Total_CR_NTax = 0;
			if (document.getElementById("CreditNtTax").value != "") {
				var arrayOfStrings = document.getElementById("CreditNtTax").value.split(";");
				for (var i = 0; i < (arrayOfStrings.length - 1); i++) {
					var ElementNm = document.getElementById(arrayOfStrings[i]);
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					ElementNm.value = ElementNm.value.replace(',', '');
					$T_CR_NTax = ElementNm.value.replace(',', '');

					if (parseFloat($T_CR_NTax).toFixed(2) > 0) {
						if ($Total_CR_NTax == 0) {
							$Total_CR_NTax = parseFloat($T_CR_NTax).toFixed(2);
						} else {
							$Total_CR_NTax = (parseFloat($Total_CR_NTax).toFixed(2) * 1) + (parseFloat($T_CR_NTax).toFixed(2) * 1);
						}
					} else {
						ElementNm.value = "0.00";
					}
				}
			}

			/*alert ("Total CR = "+$Total_CR);
			alert ("Total DR = "+$Total_DR);
			alert ("Total taxable = "+$Total_TX);
			alert ("Total Pensionable = "+$Total_PEN);
			*/

			//Get GROSS pay from Object
			var $GrossPay_user = (parseFloat(Math.round(document.getElementById("Gross").value.replace(/\,/g, '') * 100) / 100).toFixed(2));
			var $GrossPay_cr = $Total_CR;
			if ($GrossPay_cr > $GrossPay_user) {
				$GrossPay = $GrossPay_cr;
			} else {
				$GrossPay = $GrossPay_user;
			}

			$TotTaxable = $Total_TX;

			console.log({
				$TotTaxable,
				$Total_TX
			})

			//alert($TotTaxable);
			NetPay = 0;

			//alert($Total_DR_NTax);
			$Total_DR_NTax = $Total_DR_NTax * 1;
			$Total_CR_NTax = $Total_CR_NTax * 1;
			$Total_RL_NTax = $Total_RL_NTax * 1;

			//Total Relief is 200k plus 20% of gross plus Non Taxable deductions
			// $TRelief = 200000 + ($GrossPay * (20/100)) + $Pension + $Total_DR_NTax;
			$new_grossV2 = $GrossPay - $Total_DR_Tax - $Pension;
			$one_per_gross = 0.01 * $new_grossV2;

			$pay_200000 = $one_per_gross > 200000 ? $one_per_gross : 200000;
			var $comp = "<?php echo $_SESSION['StkTckCustID'] ?>";

			console.log({
				$comp
			})


			if ($comp === '092f904d7a11f9ad2c827afe889d7cda') {
				document.getElementById("niz_gross").value = $new_grossV2;


				$TRelief = $pay_200000 + ($new_grossV2 * (20 / 100)) + $Pension + parseFloat($Total_DR_Tax) + parseFloat($Total_RL_NTax);




			} else if ($comp === '89bacdc8f7403aea089dcc66881b48a6') {
				var l_gross = parseFloat($GrossPay) + parseFloat(leave_al) - parseFloat($Pension);
				var l_gross_percent = (parseFloat($GrossPay) + parseFloat(leave_al) - parseFloat($Pension)) * (20 / 100);

				$TRelief = 200000 + l_gross_percent + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax;


			} else {

				$TRelief = 200000 + ($GrossPay * (20 / 100)) + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax;
			}




			// $GrossPay = $GrossPay * 1;
			// $TRelief = $TRelief * 1;

			var leave_active = document.getElementById("LeaveField").value;
			var per_leave = document.getElementById("PCent_LeaveAllw").value * 1;
			if (leave_active !== "on" && per_leave > 0) {

				if ($comp === '89bacdc8f7403aea089dcc66881b48a6') {

					$TotTaxable = (parseFloat($GrossPay) + parseFloat(leave_al)) - parseFloat($TRelief);


				} else {
					$TotTaxable = parseFloat($GrossPay) - parseFloat($TRelief);
				}



			} else {

				$TotTaxable = parseFloat($GrossPay) - parseFloat($TRelief);
			}

			// $TotTaxable = parseFloat($GrossPay) - parseFloat($TRelief);


			// console.log({
			// 	$TRelief,
			// 	$Total_RL_NTax,
			// 	$Total_DR_NTax,
			// 	$Total_CR_NTax
			// })


			//	alert($TRelief);
			// alert($Total_CR_NTax);
			// alert($Pension);

			// $TotTaxable = $TotTaxable - $TRelief;
			// $TotTaxable = $GrossPay - $TRelief;



			// console.log($TotTaxable);

			//Get the total amount for all relief
			//Get the total taxable pay

			$PAYE = 0;
			$PAYE0 = parseFloat(document.getElementById("S_PAYE0").value).toFixed(2) * 1;
			$PAYE1 = parseFloat(document.getElementById("S_PAYE1").value).toFixed(2) * 1;
			$PAYE2 = parseFloat(document.getElementById("S_PAYE2").value).toFixed(2) * 1;
			$PAYE3 = parseFloat(document.getElementById("S_PAYE3").value).toFixed(2) * 1;
			$PAYE4 = parseFloat(document.getElementById("S_PAYE4").value).toFixed(2) * 1;
			$PAYE5 = parseFloat(document.getElementById("S_PAYE5").value).toFixed(2) * 1;
			$PAYE6 = parseFloat(document.getElementById("S_PAYE6").value).toFixed(2) * 1;
			$PAYE7 = parseFloat(document.getElementById("S_PAYE7").value).toFixed(2) * 1;

			//alert($PAYE6);
			//alert(document.getElementById("S_PAYE_Comm1").value);
			$PAYE_Comm0 = parseFloat(document.getElementById("S_PAYE_Comm0").value).toFixed(2) * 1;
			$PAYE_Comm1 = parseFloat(document.getElementById("S_PAYE_Comm1").value).toFixed(2) * 1;
			$PAYE_Comm2 = parseFloat(document.getElementById("S_PAYE_Comm2").value).toFixed(2) * 1;
			$PAYE_Comm3 = parseFloat(document.getElementById("S_PAYE_Comm3").value).toFixed(2) * 1;
			$PAYE_Comm4 = parseFloat(document.getElementById("S_PAYE_Comm4").value).toFixed(2) * 1;
			$PAYE_Comm5 = parseFloat(document.getElementById("S_PAYE_Comm5").value).toFixed(2) * 1;
			$PAYE_Comm6 = parseFloat(document.getElementById("S_PAYE_Comm6").value).toFixed(2) * 1;
			$PAYE_Comm7 = parseFloat(document.getElementById("S_PAYE_Comm7").value).toFixed(2) * 1;




			if (($TotTaxable <= $PAYE0)) {

				$PAYE = ($GrossPay * $PAYE_Comm0) / 100;
			} else {


				/*
						// Applies the flat rate of 1%
						if ($GrossPay <= $PAYE0)
						{$PAYE = ($GrossPay * $PAYE_Comm0)/100;}

						// Applies first band of 7%
						if (($GrossPay > ($PAYE0)) && ($TotTaxable < ($PAYE1)))
						{
							$PAYE_Rem = $TotTaxable;
							$PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);

						}
						*/

				//Amount taxed on first band
				if (($TotTaxable <= ($PAYE1)) && ($GrossPay > $PAYE0)) {

					$PAYE_Rem = $TotTaxable;
					$PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
				}


				if ($TotTaxable >= ($PAYE1)) {
					$PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
				}

				//If Amount Left is not up to SECOND BAND (PAYE2)
				if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {

					$PAYE_Rem = $TotTaxable - ($PAYE1);
					$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
				}

				if ($TotTaxable >= ($PAYE1 + $PAYE2)) {

					$PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
				}

				//If Amount Left is not up to THIRD BAND (PAYE3)
				if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {

					$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
					$PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
				}

				if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
					$PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
				}

				//If Amount Left is not up to FOURTH BAND (PAYE4)
				if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
					$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
					$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
				}

				if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
					$PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
				}

				//alert ($TotTaxable);

				//If Amount Left is not up to FIFTH BAND (PAYE5)
				if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
					$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
					$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
				}

				if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
					$PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
				}
				//alert ($PAYE);

				//If Amount Left is not up to SIXTH BAND (PAYE6)
				//if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)))
				//{$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5); $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

				if ($TotTaxable > $PAYE6) {
					$PAYE = $PAYE + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
				}





			}


			// for $TotTaxable1

			if (leave_active !== "on" && per_leave > 0) {

				$TotTaxable1 = parseFloat(leave_al);

				// if (($TotTaxable1 <= $PAYE0)) {

				// 	$PAYE_X = ($GrossPay * $PAYE_Comm0) / 100;
				// } else {


				/*
						// Applies the flat rate of 1%
						if ($GrossPay <= $PAYE0)
						{$PAYE = ($GrossPay * $PAYE_Comm0)/100;}

						// Applies first band of 7%
						if (($GrossPay > ($PAYE0)) && ($TotTaxable < ($PAYE1)))
						{
							$PAYE_Rem = $TotTaxable;
							$PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);

						}
						*/

				//Amount taxed on first band
				if (($TotTaxable1 <= ($PAYE1)) && ($GrossPay > $PAYE0)) {

					$PAYE_Rem = $TotTaxable1;
					$PAYE_X = (($PAYE_Rem * $PAYE_Comm1) / 100);
				}


				if ($TotTaxable1 >= ($PAYE1)) {
					$PAYE_X = $PAYE_X + (($PAYE1 * $PAYE_Comm1) / 100);
				}

				//If Amount Left is not up to SECOND BAND (PAYE2)
				if (($TotTaxable1 > ($PAYE1)) && ($TotTaxable1 < ($PAYE1 + $PAYE2))) {

					$PAYE_Rem = $TotTaxable1 - ($PAYE1);
					$PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm2) / 100);
				}

				if ($TotTaxable1 >= ($PAYE1 + $PAYE2)) {

					$PAYE_X = $PAYE_X + (($PAYE2 * $PAYE_Comm2) / 100);
				}

				//If Amount Left is not up to THIRD BAND (PAYE3)
				if (($TotTaxable1 > ($PAYE1 + $PAYE2)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3))) {

					$PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2);
					$PAYE_X = ($PAYE_X + (($PAYE_Rem * $PAYE_Comm3) / 100));
				}

				if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3)) {
					$PAYE_X = $PAYE_X + (($PAYE3 * $PAYE_Comm3) / 100);
				}

				//If Amount Left is not up to FOURTH BAND (PAYE4)
				if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
					$PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2 + $PAYE3);
					$PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm4) / 100);
				}

				if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
					$PAYE_X = $PAYE_X + (($PAYE4 * $PAYE_Comm4) / 100);
				}

				//alert ($TotTaxable);

				//If Amount Left is not up to FIFTH BAND (PAYE5)
				if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
					$PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
					$PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm5) / 100);
				}

				if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
					$PAYE_X = $PAYE_X + (($PAYE5 * $PAYE_Comm5) / 100);
				}
				//alert ($PAYE);

				//If Amount Left is not up to SIXTH BAND (PAYE6)
				//if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)))
				//{$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5); $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

				if ($TotTaxable1 > $PAYE6) {
					$PAYE_X = $PAYE_X + ((($TotTaxable1 - $PAYE6) * $PAYE_Comm6) / 100);
				}



				// for now make $PAYE_X zero

				$PAYE_X = 0;

				// }

				// $PAYE_X = $PAYE;
				document.getElementById("paye_x").value = $PAYE_X;

			}

























			//if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6))
			//{$PAYE = $PAYE + ((($TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)) * $PAYE_Comm6)/100);}
			//{$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

			// console.log($PAYE);
			//alert ($PAYE);
			//alert ($Pension);

			// alert(document.getElementById("LeaveField").value);
			/*if (document.getElementById("LeaveField").value=='on') //If Leave allowance is inclusive then add Pension to the Net
			{
				$TotAllowance = $PAYE + $Pension;
			}
			else
			{
				$TotAllowance = $PAYE;
			}

			alert("Gross:" + $GrossPay);
			alert("Total_DR:" + $Total_DR);
			alert("PAYE:" + $PAYE);
			alert("Pension:" + $Pension);
			*/

			// get all free pay
			var tfreepay = 0;
			var tfreepay_midd = 0;
			var freepay = document.getElementById('PayItem20');
			var freepay19 = document.getElementById('PayItem19');
			if (freepay) {

				freepay.value = freepay.value.replace(',', '');
				freepay.value = freepay.value.replace(',', '');
				freepay.value = freepay.value.replace(',', '');
				freepay.value = freepay.value.replace(',', '');
				freepay.value = freepay.value.replace(',', '');
				freepay.value = freepay.value.replace(',', '');
				//   console.log((parseFloat(freepay.value).toFixed(2)*1));
				if ((parseFloat(freepay.value).toFixed(2) * 1) > 0) {
					tfreepay = (parseFloat(tfreepay).toFixed(2) * 1) + (parseFloat(freepay.value.replace(',', '')).toFixed(2) * 1);
					freepay.value = thousand_separator(parseFloat(freepay.value).toFixed(2));


				} else {
					freepay.value = '0.00';
					tfreepay = '0';
				}
			}
			tfreepay_midd = tfreepay;
			if (freepay19) {

				freepay19.value = freepay19.value.replace(',', '');
				freepay19.value = freepay19.value.replace(',', '');
				freepay19.value = freepay19.value.replace(',', '');
				freepay19.value = freepay19.value.replace(',', '');
				freepay19.value = freepay19.value.replace(',', '');
				freepay19.value = freepay19.value.replace(',', '');
				//   console.log((parseFloat(freepay19.value).toFixed(2)*1));
				if ((parseFloat(freepay19.value).toFixed(2) * 1) > 0) {
					tfreepay = (parseFloat(tfreepay).toFixed(2) * 1) + (parseFloat(freepay19.value.replace(',', '')).toFixed(2) * 1);
					freepay19.value = thousand_separator(parseFloat(freepay19.value).toFixed(2));


				} else {
					freepay19.value = '0.00';
					tfreepay = tfreepay_midd;
				}
			}

			//   console.log(tfreepay);

			if ($comp === '092f904d7a11f9ad2c827afe889d7cda') {
				$NetPay = (($GrossPay - $Total_DR) - $PAYE - $Pension); // - $LeaveA

			} else {

				$NetPay = (($GrossPay - $Total_DR) - $PAYE - $Pension + parseFloat(tfreepay)); // - $LeaveA
			}



			// $NetPay = (($GrossPay - $Total_DR)- $PAYE - $Pension + (parseFloat(leave_al).toFixed(2)* 1)); // +$LeaveA

			/*alert ("N="+$NetPay);
			alert ($PAYE);
			alert ($Pension);
			alert ($GrossPay);
			alert ("DR="+$Total_DR);
			alert ($TotAllowance);
			*/


			document.getElementById("TTaxable").value = thousand_separator(parseFloat($TotTaxable).toFixed(2));
			document.getElementById("TRelief").value = thousand_separator(parseFloat($TRelief).toFixed(2));
			document.getElementById("PAYE").value = thousand_separator(parseFloat($PAYE).toFixed(2));
			document.getElementById("NetPay").value = thousand_separator(parseFloat($NetPay).toFixed(2));

			//document.getElementById("PensionEmployee").value = thousand_separator(parseFloat($NetPay).toFixed(2));

			/*	document.getElementById("TTaxable").value.replace(/\,/g,'') +
				document.getElementById("PensionEmployee").value.replace(/\,/g,'') +
				document.getElementById("NetPay").value.replace(/\,/g,'') +
				document.getElementById("LeaveAllowance").value.replace(/\,/g,'') +
				document.getElementById("Gross").value.replace(/\,/g,'') +
				document.getElementById("paye_d").value.replace(/\,/g,'') +
				document.getElementById("pension").value;
			*/
			//++++++++++++++++++++++++++++++++++++++ INSERT END +++++++++++++++++++++++++++++++++++++++++

		}

		//Get TOTAL CREDIT VALUE
		//alert("Hello");

	}

	function thousand_separator(v, s, d) {
		if (arguments.length == 2) d = ".";
		if (arguments.length == 1) {
			s = ",";
			d = ".";
		}

		v = v.toString();
		// separate the whole number and the fraction if possible
		var a = v.split(d);
		var x = a[0]; // decimal
		var y = a[1]; // fraction
		var z = "";
		var l = x.length;
		while (l > 3) {
			z = "," + x.substr(l - 3, 3) + z;
			l -= 3;
		}
		z = v.substr(0, l) + z;

		if (a.length > 1)
			z = z + d + y;
		return z;
	}

	$(function() {
		$(document).tooltip();
	});
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	function MM_swapImgRestore() { //v3.0
		var i, x, a = document.MM_sr;
		for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
	}

	function MM_preloadImages() { //v3.0
		var d = document;
		if (d.images) {
			if (!d.MM_p) d.MM_p = new Array();
			var i, j = d.MM_p.length,
				a = MM_preloadImages.arguments;
			for (i = 0; i < a.length; i++)
				if (a[i].indexOf("#") != 0) {
					d.MM_p[j] = new Image;
					d.MM_p[j++].src = a[i];
				}
		}
	}

	function MM_findObj(n, d) { //v4.01
		var p, i, x;
		if (!d) d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all) x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
		if (!x && d.getElementById) x = d.getElementById(n);
		return x;
	}

	function MM_swapImage() { //v3.0
		var i, j = 0,
			x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = MM_findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc) x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;" onLoad="GetTotal()">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" onSubmit="GetTotal()">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-3 col-2 text-center">
					</div>
					<div class="col-md-6 col-8 text-center" style="margin-top: 1%">
						<h5>
							Create/Edit Employee Master Schedule
						</h5>
					</div>
					<div class="col-md-3 col-2">
						<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Clear" style="margin-top: 1%" />
						<input name="RetPg" id="RetPg" type="hidden" value="" />
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-3 col-form-label">Select Staff:</label>
							<div class=" col-sm-8  input-group input-group-sm">
								<select class="form-control" name="EmpID" id="EmpID">
									<option value="">--</option>
									<?php
									$BrhID = "";
									if ($EditID != '' && $EditID != '--') {
										$SelID = $EmpID_;
									} else {
										$SelID = $EmpID_ = '';
									}
									//Loading from a search
									if (isset($_REQUEST["RetPg"]) && $_REQUEST["RetPg"] != "" && isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32) {
										$EmpID_ = ECh($_REQUEST["AcctNo"]);
									}
									if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
										$BrhID = "'" . ECh($_REQUEST["BranchID"]) . "'";
									}
									if (!isset($_REQUEST["BranchID"])) {
										$BrhID = "'" . ScriptRunner("SELECT BranchID from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'", "BranchID") . "'";
									}
									if (isset($_REQUEST["BranchID"]) && ($_REQUEST["BranchID"] == "" || $_REQUEST["BranchID"] == "--")) //Set user branch as current branch
									{
										$BrhID = "'" . ScriptRunner("SELECT BranchID from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'", "BranchID") . "'";
									}

									if (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'ALL') {
										$BrhID = ScriptRunnerJoin("SELECT OName, HashKey FROM BrhMasters where Status='A' ORDER BY OName", "HashKey");
									}
									//    $dbOpen2="SELECT HashKey, (SName+' '+FName+' '+' ['+Convert(Varchar(24),EmpID)+']') as Nm from EmpTbl where (Status in ('A','U') and EmpStatus='Active' and BranchID in (".$BrhID.") and HashKey NOT IN (Select EmpID from Fin_PRCore where Status in ('N','U','A') and HashKey ='".$EmpID_."')) order by SName";
									$my_id = $_SESSION["StkTck" . "HKey"];
									//$dbOpen2="SELECT HashKey, (SName+' '+FName+' '+' ['+Convert(Varchar(24),EmpID)+']') as Nm from EmpTbl where (HashKey <> '$my_id' and Status in ('A','U') and EmpStatus='Active' and (BranchID in (".$BrhID.") or HashKey ='".$EmpID_."')) order by SName";
									$dbOpen2 = "SELECT HashKey, (SName+' '+FName+' '+ONames+' '+' ['+Convert(Varchar(24),EmpID)+']') as Nm from EmpTbl where (Status in ('A','U') and EmpStatus='Active' and (BranchID in (" . $BrhID . ") or HashKey ='" . $EmpID_ . "')) order by SName";

									//    and HashKey NOT IN (Select EmpID from Fin_PRCore where Status in ('N','U','A'))) order by SName";
									//    echo $dbOpen2;
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//if ($SelID==$row2['HashKey'])
										if ($EmpID_ == $row2['HashKey']) {
											echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
										} else {
											if (strlen($row2['HashKey']) == 32) {
												echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
											}
										}
									}

									include '../login/dbClose2.php';
									?>
								</select>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default btn-sm" style="line-height: 17px" alt="Find a Dashboard" name="FindanEmployee" width="18" height="18" border="0" id="FindanEmployee" onClick="parent.ShowDisp('Search&nbsp;Employee','hrm/srh_Emp.php?RetPg=fin/Nw_EmpTemp.php&GroupName=Nationality&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')">
										<i class="fa fa-search"></i>
									</button>
								</span>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default btn-sm" style="line-height: 17px" alt="Change Database Connection" name="EditDashboard" width="18" height="18" border="0" id="EditDashboard" onclick="removeElement('DbName_TR');" onMouseOver="ImgBorderOn('EditDashboard', 1);">
										<i class="fa fa-gears"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-3 col-form-label">Select Pay Group <span style="color: red">*</span>:</label>
							<div class=" col-sm-8  input-group input-group-sm">
								<select class="form-control" name="PayScheme" id="PayScheme">
									<?php
									if ($PayScheme_ != '') {
										$SelID = $PayScheme_;
									} else {
										$SelID = "";
									}
									$dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') order by GName Asc";
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										if ($SelID == $row2['HashKey']) {
											echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['GName'] . '</option>';
										} else {
											echo '<option value="' . $row2['HashKey'] . '">' . $row2['GName'] . '</option>';
										}
									}
									include '../login/dbClose2.php';
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6" id="DbName_TR" name="DbName_TR">
						<div class="form-group row">
							<label class="col-sm-3"></label>
							<div class=" col-sm-8  input-group input-group-sm">
								<?php
								echo ("<script type='text/javascript'>{ removeElement('DbName_TR'); }</script>");

								echo '<select name="BranchID" class="form-control" id="BranchID">
									<option value="--" selected="selected">--</option>';
								if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
									$SelID = ECh($_REQUEST["BranchID"]);
								}

								if (!isset($SelID)) {
									$SelID = "";
								}

								if (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'ALL') {
									echo '<option selected value="ALL">All Branches</option>';
								} else {
									echo '<option value="ALL">All Branches</option>';
								}

								$dbOpen2 = ("SELECT OName, HashKey FROM BrhMasters where Status='A' ORDER BY OName");
								include '../login/dbOpen2.php';
								while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
									if ($SelID == $row2['HashKey']) {
										echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
									} else {
										echo '<option value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
									}
								}
								//include '../login/dbClose2.php';

								echo '</select>';
								?>
								<span class="input-group-btn ">
									<?php echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Open" />';
									/*if ($EditID != "" || $EditID > 0)
{
$Script_Edit ="select *, Convert(Varchar(11),EmpDt,106) as EmpDt_, Convert(Varchar(11),DOB,106) as DOB_ from [EmpTbl] where HashKey='".$EditID."'";
}*/
									?>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php
						if (ValidateURths("PAYMENT TEMPLATE" . "A") == true) {
							echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Schedule" />';
						}
						?>
					</div>
				</div>
				<?php if (!isset($PayScheme_) || (isset($PayScheme_) && strlen($PayScheme_) != 32)) : ?>
					<div class="row ">
						<input name="PgDoS" id="PgDoS" type="hidden" value="'.DoSFormToken().'">
						<div class="col-md-12 text-center">
							<br>
							<p> Select an employee and a group you wish the employee to belong to
							</p>
						</div>
					</div>
					<?php // exit; 
					?>
				<?php endif ?>
				<?php
				if (strlen($EditID) == 32) {
					$dbOpen2 = "SELECT count(*) Ct from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
					if (ScriptRunner($dbOpen2, "Ct") == 0) {
						echo ("<script type='text/javascript'>{ parent.msgbox('Employee payroll group has been unauthorized', 'red'); }</script>");
					}
					$dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
					// var_dump($dbOpen2);
					// die();
					include '../login/dbOpen2.php';
					while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
				?>
						<div class="row">
							<div class="col-sm-12 text-center">
								<h6 class="page-header">Gross Pay</h6>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm1']; ?><span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										$AllItems = 'PayItem1;';
										if ($row2['PayItemCD1'] == "CR") {
											$CR = $CR . 'PayItem1;';
										} elseif ($row2['PayItemCD1'] == "DR") {
											$DR = $DR . 'PayItem1;';
										}

										if ($row2['PayItemTX1'] == 1) {
											$TTaxable = $TTaxable . 'PayItem1;';
										}
										if ($row2['PayItemPen1'] == 1) {
											$TPension = $TPension . 'PayItem1;';
										}
										// else { $TPension = 0;}

										echo '
									<input type="text" name="PayItem1" id="PayItem1" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem1") + 0.0), 2, '.', ',') . '" onchange="GetTotal()"  />
									<input type="hidden" name="PCent_PayItem1" id="PCent_PayItem1" value="' . $row2["PayItem1"] . '" />
									';
										?>
										<span class="input-group-btn ">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm1"] . ": " . $row2["PayItem1"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
												<i class="fa fa-info"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm3']; ?><span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										$AllItems = $AllItems . 'PayItem3;';
										if ($row2['PayItemCD3'] == "CR") {
											$CR = $CR . 'PayItem3;';
										} elseif ($row2['PayItemCD3'] == "DR") {
											$DR = $DR . 'PayItem3;';
										}

										if ($row2['PayItemTX3'] == 1) {
											$TTaxable = $TTaxable . 'PayItem3;';
										}
										if ($row2['PayItemPen3'] == 1) {
											$TPension = $TPension . 'PayItem3;';
										}
										// else { $TPension = 0;}

										echo '
									<input type="text" name="PayItem3" id="PayItem3" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem3") + 0.0), 2, '.', ',') . '" onchange="GetTotal()"  />
									<input type="hidden" name="PCent_PayItem3" id="PCent_PayItem3" value="' . $row2["PayItem3"] . '" />
									';
										?>
										<span class="input-group-btn ">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm3"] . ": " . $row2["PayItem3"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
												<i class="fa fa-info"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm2']; ?>
										<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										$AllItems = $AllItems . 'PayItem2;';
										if ($row2['PayItemCD2'] == "CR") {
											$CR = $CR . 'PayItem2;';
										} elseif ($row2['PayItemCD2'] == "DR") {
											$DR = $DR . 'PayItem2;';
										}

										if ($row2['PayItemTX2'] == 1) {
											$TTaxable = $TTaxable . 'PayItem2;';
										}
										if ($row2['PayItemPen2'] == 1) {
											$TPension = $TPension . 'PayItem2;';
										}
										// else { $TPension = 0;}

										echo '
										<input type="text" name="PayItem2" id="PayItem2" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem2") + 0.0), 2, '.', ',') . '" onchange="GetTotal()"  />
										<input type="hidden" name="PCent_PayItem2" id="PCent_PayItem2" value="' . $row2["PayItem2"] . '" />';
										?>
										<span class="input-group-btn ">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm2"] . ": " . $row2["PayItem2"] . "% of Gross"; ?>" name="AuthInfo21" id="AuthInfo21">
												<i class="fa fa-info"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary " style="padding-right: 0px; font-weight: bolder;">Gross Pay
										<?php
										if (isset($row2['CurrencyType'])) {
											$script_curr_name = "SELECT Val1 FROM Masters WHERE HashKey='" . trim($row2['CurrencyType']) . "'";
											echo "(" . $curr_name = ScriptRunner($script_curr_name, "Val1") . ")";
										} else {
											echo ":Naira (NGR)";
										}
										?>
										:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										//echo $Script_Edit;
										echo '<input type="text" name="Gross" id="Gross" size="18" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "Gross") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="TaxExtp">
							<div class="col-sm-12 text-center">
								<h6 class="page-header">Allowances
									<!-- <span class="float-right">
										Shift
										<button type="button" class="btn btn-default  btn-sm" Title="Check if this employee qualifies for Shift Allowance Payment" name="AuthInfo" id="AuthInfo3" onClick="Popup.show('simplediv');return false;">
											<i class="fa fa-info"></i>
										</button>
									</span> -->
								</h6>
							</div>
							<?php
							$EvOd = 0;
							for ($i = 4; $i <= 25; $i++) {
								if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1) {
									$AllItems = $AllItems . 'PayItem' . $i . ';';

									if ($row2['PayItemCD' . $i] == "CR") {
										$CR = $CR . 'PayItem' . $i . ';';
										if ($row2['PayItemTX' . $i] == 0) {
											$CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
										}
									} elseif ($row2['PayItemCD' . $i] == "DR") {
										$DR = $DR . 'PayItem' . $i . ';';
										if ($row2['PayItemTX' . $i] == 0) {
											$DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
										} else {
											$DebitTaxed = $DebitTaxed . 'PayItem' . $i . ';';
										}
									}
									if ($row2['PayItemTX' . $i] == 1) {
										$TTaxable = $TTaxable . 'PayItem' . $i . ';';
									}

									if ($row2['PayItemPen' . $i] == 1) {
										$TPension = $TPension . 'PayItem' . $i . ';';
									}
									//else{$TPension = 0;}

									$EvOd = $EvOd + 1;
									if ($EvOd == 1) {
										$Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
										echo '<div class="col-md-6">
																	<div class="form-group row">
																	<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
																	<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
																	<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
																	<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
							?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
									<?php
										echo '</div></div></div>';
									} elseif ($EvOd == 2) {

										echo '<div class="col-md-6">
															<div class="form-group row">
															<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
															<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
																<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
																<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
									?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
							<?php
										echo '</div></div></div>';
										$EvOd = 0;
									}
								}
							}

							if ($EvOd == 1) {
								echo '<td>&nbsp;</td><td>&nbsp;</td></tr>';
							}
							//    }
							?>
						</div>
						<div class="row" id="TaxExtp">
							<div class="col-sm-12 text-center">
								<h6 class="page-header">Reliefs
									<!-- <span class="float-right">
										Shift
										<button type="button" class="btn btn-default  btn-sm" Title="Check if this employee qualifies for Shift Allowance Payment" name="AuthInfo" id="AuthInfo3" onClick="Popup.show('simplediv');return false;">
											<i class="fa fa-info"></i>
										</button>
									</span> -->
								</h6>
							</div>
							<?php
							$EvOd = 0;
							for ($i = 4; $i <= 25; $i++) {
								if ($row2['PayItemCD' . $i] == "RL" && $row2['PayItemOF' . $i] == 1) {
									$AllItems = $AllItems . 'PayItem' . $i . ';';

									if ($row2['PayItemCD' . $i] == "RL") {
										$RL = $RL . 'PayItem' . $i . ';';
										// if ($row2['PayItemTX' . $i] == 0) {
										$ReliefNtTaxed = $ReliefNtTaxed . 'PayItem' . $i . ';';
										// }
									}


									if ($row2['PayItemTX' . $i] == 1) {
										$TTaxable = $TTaxable . 'PayItem' . $i . ';';
									}

									if ($row2['PayItemPen' . $i] == 1) {
										$TPension = $TPension . 'PayItem' . $i . ';';
									}
									//else{$TPension = 0;}

									$EvOd = $EvOd + 1;
									if ($EvOd == 1) {
										$Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
										echo '<div class="col-md-6">
																	<div class="form-group row">
																	<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
																	<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
																	<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
																	<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
							?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
									<?php
										echo '</div></div></div>';
									} elseif ($EvOd == 2) {

										echo '<div class="col-md-6">
															<div class="form-group row">
															<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
															<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
																<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
																<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
									?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
							<?php
										echo '</div></div></div>';
										$EvOd = 0;
									}
								}
							}

							if ($EvOd == 1) {
								echo '<td>&nbsp;</td><td>&nbsp;</td></tr>';
							}
							//    }
							?>
						</div>





						<div class="row">
							<div class="col-sm-12 text-center">
								<h6 class="page-header">Deductions</h6>
							</div>
							<?php
							$EvOd = 0;
							for ($i = 4; $i <= 25; $i++) {
								if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
									$AllItems = $AllItems . 'PayItem' . $i . ';';
									if ($row2['PayItemCD' . $i] == "CR") {
										$CR = $CR . 'PayItem' . $i . ';';
										if ($row2['PayItemTX' . $i] == 0) {
											$CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
										}
									} elseif ($row2['PayItemCD' . $i] == "DR") {
										$DR = $DR . 'PayItem' . $i . ';';
										if ($row2['PayItemTX' . $i] == 0) {
											$DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
										} else {
											$DebitTaxed = $DebitTaxed . 'PayItem' . $i . ';';
										}
									}

									if ($row2['PayItemTX' . $i] == 1) {
										$TTaxable = $TTaxable . 'PayItem' . $i . ';';
									}

									if ($row2['PayItemPen' . $i] == 1) {
										$TPension = $TPension . 'PayItem' . $i . ';';
									}

									$EvOd = $EvOd + 1;
									if ($EvOd == 1) {
										$Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
										echo '<div class="col-md-6">
												<div class="form-group row">
												<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
												<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
												<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
							?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
									<?php
										echo '</div></div></div>';
									} elseif ($EvOd == 2) {
										echo '<div class="col-md-6">
										<div class="form-group row">
										<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
											<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . $i) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
											<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';
									?>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
												<i class="fa fa-info"></i>
											</button>
										</span>
							<?php
										echo '</div></div></div>';
										$EvOd = 0;
									}
								}
							}
							if ($EvOd == 1) {
								echo '<td>&nbsp;</td><td>&nbsp;</td></tr>';
							}
							?>
						</div>



						<?php if ((int) $row2["PayItemOF20"] === 1 || (int) $row2["PayItemOF19"] === 1) : ?>
							<div class="row">

								<div class="col-sm-12 text-center">
									<h6 class="page-header">Free Pay</h6>

								</div>

								<?php
								if ((int) $row2["PayItemOF20"] === 1) {

									echo '<div class="col-md-6">
										<div class="form-group row ml-1">
										<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm20'] . ':<span style="color: red">*</span>:</label>
											<input type="text" name="PayItem' . 20 . '" id="PayItem' . 20 . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . 20) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
										</div>
								</diV>';
								}
								if ((int) $row2["PayItemOF19"] === 1) {

									echo '<div class="col-md-6">
										<div class="form-group row ml-1">
										<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm19'] . ':<span style="color: red">*</span>:</label>
											<input type="text" name="PayItem' . 19 . '" id="PayItem' . 19 . '" maxlength="12" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PayItem" . 19) + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />
										</div>
								</diV>';
								}

								?>


							</div>
						<?php endif; ?>





						<div class="row">
							<div class="col-sm-12 text-center">
								<h6 class="page-header">Net Details</h6>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px;font-weight: bolder;">Taxable Pay
										<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" || $EditID > 0) {
											echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TTaxable") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										} else {
											echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
										}

										/*
        if ($EditID != "" || $EditID > 0)
        {
        echo '<input type="text" name="TTaxable" size="25" maxlength="25" class="TextBoxText_Medium_Comp" value="'.number_format((ScriptRunner($Script_Edit,"TTaxable")+0.0),2,'.',',').'" onchange="GetTotal()" />';
        }
        else
        {
        echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="TextBoxText_Medium_Comp" value="0.00" onchange="GetTotal()" />';
        }
         */
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px;font-weight: bolder;">PAYE
										<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PAYE") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder;">Pension<span style="color: red">*</span>:
									</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										$ScriptPension = "select * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "')";

										echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "PensionEmployee") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />

						        	<input name="paye_x" id="paye_x" type="hidden" value="0.0">
						        	<input name="PensionEmployer" id="PensionEmployer" type="hidden" value="0.0">
						        	<input type="hidden" name="PCent_PensionEmployee" id="PCent_PensionEmployee" value="0' . number_format(ScriptRunner($ScriptPension, "PensionEmployee"), 2, '.', ',') . '" />
						        	<input type="hidden" name="PCent_LeaveAllw" id="PCent_LeaveAllw" value="0' . number_format(ScriptRunner($ScriptPension, "LeaveAllw"), 2, '.', ',') . '" />
						        	<input type="hidden" name="leavePerGross" id="leavePerGross" value="' . ScriptRunner($ScriptPension, "leavePerGross") . '" />
						        	<input type="hidden" name="LeaveAllw_InOut" id="LeaveAllw_InOut" value="0' . number_format(ScriptRunner($ScriptPension, "LeaveAllw"), 2, '.', ',') . '" />
						        	<input type="hidden" name="PCent_PensionEmployer" id="PCent_PensionEmployer" value="0' . number_format(ScriptRunner($ScriptPension, "PensionEmployer"), 2, '.', ',') . '" />';
										?>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Total Relief:<span style="color: red">*</span>
									</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" || $EditID > 0) {
											echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TRelief") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										} else {
											echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
										}

										//echo '<div id="theid" onMouseOver="Welcome">here</div>';
										//}
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px;font-weight: bolder;">Leave Allowance
										<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" || $EditID > 0) {
											echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "LeaveAllowance") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										} else {
											echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
										}
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Net Pay<span style="color: red">*</span>:
									</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "NetPay") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<?php
								$Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";
								//Get % tax band values
								echo '
							<input type="hidden" name="LeaveField" id="LeaveField" value="' . ScriptRunner($Script_TAX, "LeaveField") . '">
							<input type="hidden" name="S_PAYE1" id="S_PAYE0" value="0' . ScriptRunner($Script_TAX, "Limit0") . '">
							<input type="hidden" name="S_PAYE1" id="S_PAYE1" value="0' . ScriptRunner($Script_TAX, "Limit1") . '">
							<input type="hidden" name="S_PAYE2" id="S_PAYE2" value="0' . ScriptRunner($Script_TAX, "Limit2") . '">
							<input type="hidden" name="S_PAYE3" id="S_PAYE3" value="0' . ScriptRunner($Script_TAX, "Limit3") . '">
							<input type="hidden" name="S_PAYE4" id="S_PAYE4" value="0' . ScriptRunner($Script_TAX, "Limit4") . '">
							<input type="hidden" name="S_PAYE5" id="S_PAYE5" value="0' . ScriptRunner($Script_TAX, "Limit5") . '">
							<input type="hidden" name="S_PAYE6" id="S_PAYE6" value="0' . ScriptRunner($Script_TAX, "Limit6") . '">
							<input type="hidden" name="S_PAYE7" id="S_PAYE7" value="0' . ScriptRunner($Script_TAX, "Limit7") . '">
							<input type="hidden" name="H_Leave" id="H_Leave" value="0' . ScriptRunner($Script_TAX, "LeaveAllw") . '">';

								//Get % Percent values
								echo '
							<input type="hidden" name="S_PAYE_Comm0" id="S_PAYE_Comm0" value="0' . ScriptRunner($Script_TAX, "pcent0") . '">
							<input type="hidden" name="S_PAYE_Comm1" id="S_PAYE_Comm1" value="0' . ScriptRunner($Script_TAX, "pcent1") . '">
							<input type="hidden" name="S_PAYE_Comm2" id="S_PAYE_Comm2" value="0' . ScriptRunner($Script_TAX, "pcent2") . '">
							<input type="hidden" name="S_PAYE_Comm3" id="S_PAYE_Comm3" value="0' . ScriptRunner($Script_TAX, "pcent3") . '">
							<input type="hidden" name="S_PAYE_Comm4" id="S_PAYE_Comm4" value="0' . ScriptRunner($Script_TAX, "pcent4") . '">
							<input type="hidden" name="S_PAYE_Comm5" id="S_PAYE_Comm5" value="0' . ScriptRunner($Script_TAX, "pcent5") . '">
							<input type="hidden" name="S_PAYE_Comm6" id="S_PAYE_Comm6" value="0' . ScriptRunner($Script_TAX, "pcent6") . '">
							<input type="hidden" name="S_PAYE_Comm7" id="S_PAYE_Comm7" value="0' . ScriptRunner($Script_TAX, "pcent7") . '">';
								echo '
							<input name="H_TTaxable" id="H_TTaxable" type="hidden" value="' . $TTaxable . '" />
							<input name="H_CR" id="H_CR" type="hidden" value="' . $CR . '">
							<input name="H_DR" id="H_DR" type="hidden" value="' . $DR . '">
							<input name="H_ALL" id="H_ALL" type="hidden" value="' . $AllItems . '">
							<input name="DebitNtTax" id="DebitNtTax" type="hidden" value="' . $DebitNtTaxed . '">
							<input name="DebitTax" id="DebitTax" type="hidden" value="' . $DebitTaxed . '">
							<input name="CreditNtTax" id="CreditNtTax" type="hidden" value="' . $CreditNtTaxed . '">
							<input name="ReliefNtTax" id="ReliefNtTax" type="hidden" value="' . $ReliefNtTaxed . '">
							<input name="niz_gross" id="niz_gross" type="hidden" value="' . ScriptRunner($Script_Edit, "niz_gross") . '">
							<input name="H_Pension" id="H_Pension" type="hidden" value="' . $TPension . '">';

								echo $row2['niz_gross'];
								?>
								<?php
								if ($_SESSION["StkTck" . "CustID"] == "e0d2415d2bc6436f19a0fecced7dea29") {
									print '<input type="hidden" name="AuthCalc2" id="AuthCalc2" value="1">'; //DONT
								} else {
									print '<input type="hidden" name="AuthCalc2" id="AuthCalc2" value="0">'; //auto calculate
								}
								?>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6"></div>
									<div class="col-md-2">
										<div class="demo-checkbox">
											<?php
											/*
        if (ScriptRunner($Script_Edit,"AuthCalc")=='on')
        {echo '<input type="checkbox" checked name="AuthCalc" id="AuthCalc">';}
        else
        {echo '<input type="checkbox" name="AuthCalc" id="AuthCalc">';}
         */ //Lawrence modified
											if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected") {
												echo '<input type="checkbox" name="AuthCalc" id="AuthCalc"  >';
											} else {
												echo '<input type="checkbox" name="AuthCalc" id="AuthCalc" checked>';
											}

											?>
											<label for="AuthCalc">
												<button type="button" class="btn btn-default btn-sm" style="line-height: 17px" Title="Check here for auto calculation of wage items as a (%) of gross" name="AuthInfo22" id="AuthInfo22" onClick="Popup.show('simplediv');return false;">
													<i class="fa fa-info"></i>
												</button>
											</label>
										</div>
									</div>
									<div class="col-md-4 pull-right">
										<?php
										if (ValidateURths("PAYMENT TEMPLATE" . "A") == true) {

											if ( /*$_REQUEST["PgTy"] == "AddIndTrans" &&*/($EditID == "" || $EditID == '--')) {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Schedule" />';
											} elseif (($EditID != "" || $EditID > 0) && $PR_Status != 'A') {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update Schedule" />';
												echo '<input name="UpdID" type="hidden" id="UpdID" value="' . $EditID . '" />';

												// To remove action button
											} elseif ($PR_Status == 'A') {
												echo '';
											} else {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Schedule" />';
											}
										}
										?>
									</div>
								</div>
							</div>
						</div>
				<?php }
				} ?>
				<br>
				<hr style="margin-bottom: 1.0rem">
				<div class="row">
					<div class="col-md-12">
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
						<a href="batch_temp.php" class="btn btn-danger btn-sm ml-auto pull-right "> Batch Create </a>
						<?php //print $_REQUEST["FAction"]; 
						?>
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),Fm.AddedDate,106) as Dt, Fm.*, Fm.HashKey as HashKey_, (Em.SName+' '+Em.FName+' '+Em.ONames+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRCore Fm where Em.HashKey=Fm.EmpID and (Fm.Status in ('N','U')) and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
						} else {
							print "<h4>AUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),Fm.AddedDate,106) as Dt, Fm.*, Fm.HashKey as HashKey_, (Em.SName+' '+Em.FName+' '+Em.ONames+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRCore Fm where Em.HashKey=Fm.EmpID and (Fm.Status in ('A')) and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
						}
						?>
					</div>
					<div class="col-md-12">
						<div class="ViewPatch">
							<table width="100%" align="left" cellpadding="1" cellspacing="1" id="table" class="tablesorter">
								<thead>
									<tr>
										<th valign="middle" width="10" class="smallText" scope="col" data-sorter="false"><span class="TinyTextTight">
												<input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
												<label for="DelChk_All"></label>
											</span></th>
										<th valign="middle" scope="col"><span class="TinyTextTightBold">Updated</span></th>
										<th align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Employee Name</span></th>
										<th valign="middle" scope="col">Basic</th>
										<th valign="middle" scope="col">Housing</th>
										<th valign="middle" scope="col">Transport</th>
										<th valign="middle" scope="col">
											<div align="center" class="TinyTextTightBold">Pension</div>
										</th>
										<th align="center" valign="middle" scope="col"><span class="TinyTextTightBold">PAYE</span></th>
										<th align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Net Pay</span></th>
										<th align="center" valign="middle" class="smallText" scope="col"><span class="TinyTextTightBold">Gross Pay</span></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$Del = 0;
									include '../login/dbOpen2.php';
									//echo $dbOpen2;
									//exit;
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										$Del = $Del + 1;
									?>
										<tr>
											<td height="25" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey_']); ?>" />
												<label for="<?php echo ("DelBox" . $Del); ?>"></label>
											</td>
											<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
											<td align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['Nm'])); ?></td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem1'], 2); ?></td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem2'], 2); ?></td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem3'], 2); ?></td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PensionEmployee'] + $row2['PensionEmployer'], 2); ?>&nbsp;</td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PAYE'], 2); ?>&nbsp;</td>
											<td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['NetPay'], 2); ?>&nbsp;</td>
											<td width="107" align="right" valign="middle" scope="col" class="TinyText"><?php echo number_format($row2['Gross'], 2); ?>&nbsp;</td>
										</tr>
									<?php
									}
									include '../login/dbClose2.php'; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6 pt-5">
						<?php include '../main/pagination.php'; ?>
					</div>
					<div class="col-md-6 pt-5">
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-9">
								<?php
								echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
										<input name="FAction" id="FAction" type="hidden" value=""/>
										<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

								//Check if any record was spolled at all before enabling buttons

								if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {

									if (ValidateURths("PAYMENT TEMPLATE" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "D");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAYMENT TEMPLATE" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAYMENT TEMPLATE" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "T");
										$but_Type = 'Authorize';
										include '../main/buttons.php';
									}
								} else {

									if (ValidateURths("PAYMENT TEMPLATE" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAYMENT TEMPLATE" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}

									if (ValidateURths("PAYMENT TEMPLATE" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("PAYMENT TEMPLATE" . "T");
										$but_Type = 'Unauthorize';
										include '../main/buttons.php';
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
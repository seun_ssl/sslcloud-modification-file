<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (ValidateURths("PAYROLL SETTING" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
} // Check User Access Right

if (isset($_REQUEST["UpdID"])) {
    $EditID = $_REQUEST["UpdID"];
} else {
    $EditID = "";
}

if (!isset($EditID)) {
    $EditID = "";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($Del)) {
    $Del = 0;
}
if (!isset($Err)) {
    $Err = "";
}
if (!isset($AllEmployee)) {
    $AllEmployee = "";
}
if (!isset($ModMaster)) {
    $ModMaster = "";
}
if (!isset($SelID)) {
    $SelID = "";
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("PAYROLL SETTING" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        } // Check User Access Right

        if (isset($_REQUEST["GName"])) {
            $v_GName = trim($_REQUEST["GName"]);
        } else {
            $v_GName = "";
        }
        if (isset($_REQUEST["AddPen"])) {
            $v_AddPen = trim($_REQUEST["AddPen"]);
        } else {
            $v_AddPen = "";
        }
        $v_GrossPCent = 100;
        $v_GrossNm = "Gross";
        if (isset($_REQUEST["GrossOF"])) {
            $v_GrossOF = trim($_REQUEST["GrossOF"]);
        } else {
            $v_GrossOF = 1;
        }

        /*for ($i=1; $i <= $_REQUEST["DelMax"]; $i++)
        {
        if (isset($_REQUEST["PayItem".$i])){$v_PayItem.$i=number_format(trim($_REQUEST["PayItem".$i])+0.0,'2','.','');} else{$v_PayItem.$i=0.0;}
        if (isset($_REQUEST["PayItemNm".$i])){$v_PayItemNm.$i=trim($_REQUEST["PayItemNm".$i]);} else{$v_PayItemNm.$i="";}
        if (isset($_REQUEST["PayItemOF".$i])){$v_PayItemOF.$i=$_REQUEST["PayItemOF".$i];} else{$v_PayItemOF.$i="";}
        if (isset($_REQUEST["PayItemTX".$i])){$v_PayItemTX.$i=$_REQUEST["PayItemTX".$i];} else{$v_PayItemTX.$i="";}
        }*/

        if (isset($_REQUEST["PayItem1"])) {
            $v_PayItem1 = number_format(trim($_REQUEST["PayItem1"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem1 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm1"])) {
            $v_PayItemNm1 = trim($_REQUEST["PayItemNm1"]);
        } else {
            $v_PayItemNm1 = "";
        }
        if (isset($_REQUEST["PayItemOF1"])) {
            $v_PayItemOF1 = 1;
        } else {
            $v_PayItemOF1 = 0;
        }
        if (isset($_REQUEST["PayItemPEN1"])) {
            $v_PayItemPEN1 = 1;
        } else {
            $v_PayItemPEN1 = 0;
        }
        if (isset($_REQUEST["PayItemTX1"])) {
            $v_PayItemTX1 = 1;
        } else {
            $v_PayItemTX1 = 0;
        }

        if (isset($_REQUEST["PayItem2"])) {
            $v_PayItem2 = number_format(trim($_REQUEST["PayItem2"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem2 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm2"])) {
            $v_PayItemNm2 = trim($_REQUEST["PayItemNm2"]);
        } else {
            $v_PayItemNm2 = "";
        }
        if (isset($_REQUEST["PayItemOF2"])) {
            $v_PayItemOF2 = 1;
        } else {
            $v_PayItemOF2 = 0;
        }
        if (isset($_REQUEST["PayItemPEN2"])) {
            $v_PayItemPEN2 = 1;
        } else {
            $v_PayItemPEN2 = 0;
        }
        if (isset($_REQUEST["PayItemTX2"])) {
            $v_PayItemTX2 = 1;
        } else {
            $v_PayItemTX2 = 0;
        }

        if (isset($_REQUEST["PayItem3"])) {
            $v_PayItem3 = number_format(trim($_REQUEST["PayItem3"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem3 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm3"])) {
            $v_PayItemNm3 = trim($_REQUEST["PayItemNm3"]);
        } else {
            $v_PayItemNm3 = "";
        }
        if (isset($_REQUEST["PayItemOF3"])) {
            $v_PayItemOF3 = 1;
        } else {
            $v_PayItemOF3 = 0;
        }
        if (isset($_REQUEST["PayItemPEN3"])) {
            $v_PayItemPEN3 = 1;
        } else {
            $v_PayItemPEN3 = 0;
        }
        if (isset($_REQUEST["PayItemTX3"])) {
            $v_PayItemTX3 = 1;
        } else {
            $v_PayItemTX3 = 0;
        }

        if (isset($_REQUEST["PayItem4"])) {
            $v_PayItem4 = number_format(trim($_REQUEST["PayItem4"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem4 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm4"])) {
            $v_PayItemNm4 = trim($_REQUEST["PayItemNm4"]);
        } else {
            $v_PayItemNm4 = "";
        }
        if (isset($_REQUEST["PayItemOF4"])) {
            $v_PayItemOF4 = 1;
        } else {
            $v_PayItemOF4 = 0;
        }
        if (isset($_REQUEST["PayItemCD4"])) {
            $v_PayItemCD4 = trim($_REQUEST["PayItemCD4"]);
        } else {
            $v_PayItemCD4 = 'CR';
        }
        if (isset($_REQUEST["PayItemPEN4"])) {
            $v_PayItemPEN4 = 1;
        } else {
            $v_PayItemPEN4 = 0;
        }
        if (isset($_REQUEST["PayItemTX4"])) {
            $v_PayItemTX4 = 1;
        } else {
            $v_PayItemTX4 = 0;
        }

        if (isset($_REQUEST["PayItem5"])) {
            $v_PayItem5 = number_format(trim($_REQUEST["PayItem5"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem5 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm5"])) {
            $v_PayItemNm5 = trim($_REQUEST["PayItemNm5"]);
        } else {
            $v_PayItemNm5 = "";
        }
        if (isset($_REQUEST["PayItemOF5"])) {
            $v_PayItemOF5 = 1;
        } else {
            $v_PayItemOF5 = 0;
        }
        if (isset($_REQUEST["PayItemCD5"])) {
            $v_PayItemCD5 = trim($_REQUEST["PayItemCD5"]);
        } else {
            $v_PayItemCD5 = 'CR';
        }
        if (isset($_REQUEST["PayItemPEN5"])) {
            $v_PayItemPEN5 = 1;
        } else {
            $v_PayItemPEN5 = 0;
        }
        if (isset($_REQUEST["PayItemTX5"])) {
            $v_PayItemTX5 = 1;
        } else {
            $v_PayItemTX5 = 0;
        }

        if (isset($_REQUEST["PayItem6"])) {
            $v_PayItem6 = number_format(trim($_REQUEST["PayItem6"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem6 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm6"])) {
            $v_PayItemNm6 = trim($_REQUEST["PayItemNm6"]);
        } else {
            $v_PayItemNm6 = "";
        }
        if (isset($_REQUEST["PayItemOF6"])) {
            $v_PayItemOF6 = 1;
        } else {
            $v_PayItemOF6 = 0;
        }
        if (isset($_REQUEST["PayItemPEN6"])) {
            $v_PayItemPEN6 = 1;
        } else {
            $v_PayItemPEN6 = 0;
        }
        if (isset($_REQUEST["PayItemCD6"])) {
            $v_PayItemCD6 = trim($_REQUEST["PayItemCD6"]);
        } else {
            $v_PayItemCD6 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX6"])) {
            $v_PayItemTX6 = 1;
        } else {
            $v_PayItemTX6 = 0;
        }

        if (isset($_REQUEST["PayItem7"])) {
            $v_PayItem7 = number_format(trim($_REQUEST["PayItem7"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem7 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm7"])) {
            $v_PayItemNm7 = trim($_REQUEST["PayItemNm7"]);
        } else {
            $v_PayItemNm7 = "";
        }
        if (isset($_REQUEST["PayItemOF7"])) {
            $v_PayItemOF7 = 1;
        } else {
            $v_PayItemOF7 = 0;
        }
        if (isset($_REQUEST["PayItemPEN7"])) {
            $v_PayItemPEN7 = 1;
        } else {
            $v_PayItemPEN7 = 0;
        }
        if (isset($_REQUEST["PayItemCD7"])) {
            $v_PayItemCD7 = trim($_REQUEST["PayItemCD7"]);
        } else {
            $v_PayItemCD7 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX7"])) {
            $v_PayItemTX7 = 1;
        } else {
            $v_PayItemTX7 = 0;
        }

        if (isset($_REQUEST["PayItem8"])) {
            $v_PayItem8 = number_format(trim($_REQUEST["PayItem8"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem8 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm8"])) {
            $v_PayItemNm8 = trim($_REQUEST["PayItemNm8"]);
        } else {
            $v_PayItemNm8 = "";
        }
        if (isset($_REQUEST["PayItemOF8"])) {
            $v_PayItemOF8 = 1;
        } else {
            $v_PayItemOF8 = 0;
        }
        if (isset($_REQUEST["PayItemPEN8"])) {
            $v_PayItemPEN8 = 1;
        } else {
            $v_PayItemPEN8 = 0;
        }
        if (isset($_REQUEST["PayItemCD8"])) {
            $v_PayItemCD8 = trim($_REQUEST["PayItemCD8"]);
        } else {
            $v_PayItemCD8 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX8"])) {
            $v_PayItemTX8 = 1;
        } else {
            $v_PayItemTX8 = 0;
        }

        if (isset($_REQUEST["PayItem9"])) {
            $v_PayItem9 = number_format(trim($_REQUEST["PayItem9"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem9 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm9"])) {
            $v_PayItemNm9 = trim($_REQUEST["PayItemNm9"]);
        } else {
            $v_PayItemNm9 = "";
        }
        if (isset($_REQUEST["PayItemOF9"])) {
            $v_PayItemOF9 = 1;
        } else {
            $v_PayItemOF9 = 0;
        }
        if (isset($_REQUEST["PayItemPEN9"])) {
            $v_PayItemPEN9 = 1;
        } else {
            $v_PayItemPEN9 = 0;
        }
        if (isset($_REQUEST["PayItemCD9"])) {
            $v_PayItemCD9 = trim($_REQUEST["PayItemCD9"]);
        } else {
            $v_PayItemCD9 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX9"])) {
            $v_PayItemTX9 = 1;
        } else {
            $v_PayItemTX9 = 0;
        }

        if (isset($_REQUEST["PayItem10"])) {
            $v_PayItem10 = number_format(trim($_REQUEST["PayItem10"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem10 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm10"])) {
            $v_PayItemNm10 = trim($_REQUEST["PayItemNm10"]);
        } else {
            $v_PayItemNm10 = "";
        }
        if (isset($_REQUEST["PayItemOF10"])) {
            $v_PayItemOF10 = 1;
        } else {
            $v_PayItemOF10 = 0;
        }
        if (isset($_REQUEST["PayItemPEN10"])) {
            $v_PayItemPEN10 = 1;
        } else {
            $v_PayItemPEN10 = 0;
        }
        if (isset($_REQUEST["PayItemCD10"])) {
            $v_PayItemCD10 = trim($_REQUEST["PayItemCD10"]);
        } else {
            $v_PayItemCD10 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX10"])) {
            $v_PayItemTX10 = 1;
        } else {
            $v_PayItemTX10 = 0;
        }

        if (isset($_REQUEST["PayItem11"])) {
            $v_PayItem11 = number_format(trim($_REQUEST["PayItem11"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem11 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm11"])) {
            $v_PayItemNm11 = trim($_REQUEST["PayItemNm11"]);
        } else {
            $v_PayItemNm11 = "";
        }
        if (isset($_REQUEST["PayItemOF11"])) {
            $v_PayItemOF11 = 1;
        } else {
            $v_PayItemOF11 = 0;
        }
        if (isset($_REQUEST["PayItemPEN11"])) {
            $v_PayItemPEN11 = 1;
        } else {
            $v_PayItemPEN11 = 0;
        }
        if (isset($_REQUEST["PayItemCD11"])) {
            $v_PayItemCD11 = trim($_REQUEST["PayItemCD11"]);
        } else {
            $v_PayItemCD11 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX11"])) {
            $v_PayItemTX11 = 1;
        } else {
            $v_PayItemTX11 = 0;
        }

        if (isset($_REQUEST["PayItem12"])) {
            $v_PayItem12 = number_format(trim($_REQUEST["PayItem12"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem12 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm12"])) {
            $v_PayItemNm12 = trim($_REQUEST["PayItemNm12"]);
        } else {
            $v_PayItemNm12 = "";
        }
        if (isset($_REQUEST["PayItemOF12"])) {
            $v_PayItemOF12 = 1;
        } else {
            $v_PayItemOF12 = 0;
        }
        if (isset($_REQUEST["PayItemPEN12"])) {
            $v_PayItemPEN12 = 1;
        } else {
            $v_PayItemPEN12 = 0;
        }
        if (isset($_REQUEST["PayItemCD12"])) {
            $v_PayItemCD12 = trim($_REQUEST["PayItemCD12"]);
        } else {
            $v_PayItemCD12 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX12"])) {
            $v_PayItemTX12 = 1;
        } else {
            $v_PayItemTX12 = 0;
        }

        if (isset($_REQUEST["PayItem13"])) {
            $v_PayItem13 = number_format(trim($_REQUEST["PayItem13"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem13 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm13"])) {
            $v_PayItemNm13 = trim($_REQUEST["PayItemNm13"]);
        } else {
            $v_PayItemNm13 = "";
        }
        if (isset($_REQUEST["PayItemOF13"])) {
            $v_PayItemOF13 = 1;
        } else {
            $v_PayItemOF13 = 0;
        }
        if (isset($_REQUEST["PayItemPEN13"])) {
            $v_PayItemPEN13 = 1;
        } else {
            $v_PayItemPEN13 = 0;
        }
        if (isset($_REQUEST["PayItemCD13"])) {
            $v_PayItemCD13 = trim($_REQUEST["PayItemCD13"]);
        } else {
            $v_PayItemCD13 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX13"])) {
            $v_PayItemTX13 = 1;
        } else {
            $v_PayItemTX13 = 0;
        }

        if (isset($_REQUEST["PayItem14"])) {
            $v_PayItem14 = number_format(trim($_REQUEST["PayItem14"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem14 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm14"])) {
            $v_PayItemNm14 = trim($_REQUEST["PayItemNm14"]);
        } else {
            $v_PayItemNm14 = "";
        }
        if (isset($_REQUEST["PayItemOF14"])) {
            $v_PayItemOF14 = 1;
        } else {
            $v_PayItemOF14 = 0;
        }
        if (isset($_REQUEST["PayItemPEN14"])) {
            $v_PayItemPEN14 = 1;
        } else {
            $v_PayItemPEN14 = 0;
        }
        if (isset($_REQUEST["PayItemCD14"])) {
            $v_PayItemCD14 = trim($_REQUEST["PayItemCD14"]);
        } else {
            $v_PayItemCD14 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX14"])) {
            $v_PayItemTX14 = 1;
        } else {
            $v_PayItemTX14 = 0;
        }

        if (isset($_REQUEST["PayItem15"])) {
            $v_PayItem15 = number_format(trim($_REQUEST["PayItem15"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem15 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm15"])) {
            $v_PayItemNm15 = trim($_REQUEST["PayItemNm15"]);
        } else {
            $v_PayItemNm15 = "";
        }
        if (isset($_REQUEST["PayItemOF15"])) {
            $v_PayItemOF15 = 1;
        } else {
            $v_PayItemOF15 = 0;
        }
        if (isset($_REQUEST["PayItemPEN15"])) {
            $v_PayItemPEN15 = 1;
        } else {
            $v_PayItemPEN15 = 0;
        }
        if (isset($_REQUEST["PayItemCD15"])) {
            $v_PayItemCD15 = trim($_REQUEST["PayItemCD15"]);
        } else {
            $v_PayItemCD15 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX15"])) {
            $v_PayItemTX15 = 1;
        } else {
            $v_PayItemTX15 = 0;
        }

        if (isset($_REQUEST["PayItem16"])) {
            $v_PayItem16 = number_format(trim($_REQUEST["PayItem16"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem16 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm16"])) {
            $v_PayItemNm16 = trim($_REQUEST["PayItemNm16"]);
        } else {
            $v_PayItemNm16 = "";
        }
        if (isset($_REQUEST["PayItemOF16"])) {
            $v_PayItemOF16 = 1;
        } else {
            $v_PayItemOF16 = 0;
        }
        if (isset($_REQUEST["PayItemPEN16"])) {
            $v_PayItemPEN16 = 1;
        } else {
            $v_PayItemPEN16 = 0;
        }
        if (isset($_REQUEST["PayItemCD16"])) {
            $v_PayItemCD16 = trim($_REQUEST["PayItemCD16"]);
        } else {
            $v_PayItemCD16 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX16"])) {
            $v_PayItemTX16 = 1;
        } else {
            $v_PayItemTX16 = 0;
        }

        if (isset($_REQUEST["PayItem17"])) {
            $v_PayItem17 = number_format(trim($_REQUEST["PayItem17"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem17 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm17"])) {
            $v_PayItemNm17 = trim($_REQUEST["PayItemNm17"]);
        } else {
            $v_PayItemNm17 = "";
        }
        if (isset($_REQUEST["PayItemOF17"])) {
            $v_PayItemOF17 = 1;
        } else {
            $v_PayItemOF17 = 0;
        }
        if (isset($_REQUEST["PayItemPEN17"])) {
            $v_PayItemPEN17 = 1;
        } else {
            $v_PayItemPEN17 = 0;
        }
        if (isset($_REQUEST["PayItemCD17"])) {
            $v_PayItemCD17 = trim($_REQUEST["PayItemCD17"]);
        } else {
            $v_PayItemCD17 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX17"])) {
            $v_PayItemTX17 = 1;
        } else {
            $v_PayItemTX17 = 0;
        }

        if (isset($_REQUEST["PayItem18"])) {
            $v_PayItem18 = number_format(trim($_REQUEST["PayItem18"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem18 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm18"])) {
            $v_PayItemNm18 = trim($_REQUEST["PayItemNm18"]);
        } else {
            $v_PayItemNm18 = "";
        }
        if (isset($_REQUEST["PayItemOF18"])) {
            $v_PayItemOF18 = 1;
        } else {
            $v_PayItemOF18 = 0;
        }
        if (isset($_REQUEST["PayItemPEN18"])) {
            $v_PayItemPEN18 = 1;
        } else {
            $v_PayItemPEN18 = 0;
        }
        if (isset($_REQUEST["PayItemCD18"])) {
            $v_PayItemCD18 = trim($_REQUEST["PayItemCD18"]);
        } else {
            $v_PayItemCD18 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX18"])) {
            $v_PayItemTX18 = 1;
        } else {
            $v_PayItemTX18 = 0;
        }

        if (isset($_REQUEST["PayItem19"])) {
            $v_PayItem19 = number_format(trim($_REQUEST["PayItem19"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem19 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm19"])) {
            $v_PayItemNm19 = trim($_REQUEST["PayItemNm19"]);
        } else {
            $v_PayItemNm19 = "";
        }
        if (isset($_REQUEST["PayItemOF19"])) {
            $v_PayItemOF19 = 1;
        } else {
            $v_PayItemOF19 = 0;
        }
        if (isset($_REQUEST["PayItemPEN19"])) {
            $v_PayItemPEN19 = 1;
        } else {
            $v_PayItemPEN19 = 0;
        }
        if (isset($_REQUEST["PayItemCD19"])) {
            $v_PayItemCD19 = trim($_REQUEST["PayItemCD19"]);
        } else {
            $v_PayItemCD19 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX19"])) {
            $v_PayItemTX19 = 1;
        } else {
            $v_PayItemTX19 = 0;
        }

        if (isset($_REQUEST["PayItem20"])) {
            $v_PayItem20 = number_format(trim($_REQUEST["PayItem20"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem20 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm20"])) {
            $v_PayItemNm20 = trim($_REQUEST["PayItemNm20"]);
        } else {
            $v_PayItemNm20 = "";
        }
        if (isset($_REQUEST["PayItemOF20"])) {
            $v_PayItemOF20 = 1;
        } else {
            $v_PayItemOF20 = 0;
        }
        if (isset($_REQUEST["PayItemPEN20"])) {
            $v_PayItemPEN20 = 1;
        } else {
            $v_PayItemPEN20 = 0;
        }
        if (isset($_REQUEST["PayItemCD20"])) {
            $v_PayItemCD20 = trim($_REQUEST["PayItemCD20"]);
        } else {
            $v_PayItemCD20 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX20"])) {
            $v_PayItemTX20 = 1;
        } else {
            $v_PayItemTX20 = 0;
        }

        if (isset($_REQUEST["PayItem21"])) {
            $v_PayItem21 = number_format(trim($_REQUEST["PayItem21"]) + 0.0, '2', '.', '');
        } else {
            $v_PayItem21 = 0.0;
        }
        if (isset($_REQUEST["PayItemNm21"])) {
            $v_PayItemNm21 = trim($_REQUEST["PayItemNm21"]);
        } else {
            $v_PayItemNm21 = "";
        }
        if (isset($_REQUEST["PayItemOF21"])) {
            $v_PayItemOF21 = 1;
        } else {
            $v_PayItemOF21 = 0;
        }
        if (isset($_REQUEST["PayItemPEN21"])) {
            $v_PayItemPEN21 = 1;
        } else {
            $v_PayItemPEN21 = 0;
        }
        if (isset($_REQUEST["PayItemCD21"])) {
            $v_PayItemCD21 = trim($_REQUEST["PayItemCD21"]);
        } else {
            $v_PayItemCD21 = 'CR';
        }
        if (isset($_REQUEST["PayItemTX21"])) {
            $v_PayItemTX21 = 1;
        } else {
            $v_PayItemTX21 = 0;
        }

        // I added this line of code

        if (isset($_REQUEST["PayItemAn1"]) && isset($_REQUEST["PayItemAnType1"])) {
            $v_PayItemAn1 = 1;
            $v_PayItemAnType1 = trim($_REQUEST["PayItemAnType1"]);
        } elseif (isset($_REQUEST["PayItemAn1"])) {
            $v_PayItemAn1 = 1;
            $v_PayItemAnType1 = 'ANN';
        } else {
            $v_PayItemAn1 = 0;
            $v_PayItemAnType1 = '';
        }

        if (isset($_REQUEST["PayItemAn2"]) && isset($_REQUEST["PayItemAnType2"])) {
            $v_PayItemAn2 = 1;
            $v_PayItemAnType2 = trim($_REQUEST["PayItemAnType2"]);
        } elseif (isset($_REQUEST["PayItemAn2"])) {
            $v_PayItemAn2 = 1;
            $v_PayItemAnType2 = 'ANN';
        } else {
            $v_PayItemAn2 = 0;
            $v_PayItemAnType2 = '';
        }

        if (isset($_REQUEST["PayItemAn3"]) && isset($_REQUEST["PayItemAnType3"])) {
            $v_PayItemAn3 = 1;
            $v_PayItemAnType3 = trim($_REQUEST["PayItemAnType3"]);
        } elseif (isset($_REQUEST["PayItemAn3"])) {
            $v_PayItemAn3 = 1;
            $v_PayItemAnType3 = 'ANN';
        } else {
            $v_PayItemAn3 = 0;
            $v_PayItemAnType3 = '';
        }

        if (isset($_REQUEST["PayItemAn4"]) && isset($_REQUEST["PayItemAnType4"])) {
            $v_PayItemAn4 = 1;
            $v_PayItemAnType4 = trim($_REQUEST["PayItemAnType4"]);
        } elseif (isset($_REQUEST["PayItemAn4"])) {
            $v_PayItemAn4 = 1;
            $v_PayItemAnType4 = 'ANN';
        } else {
            $v_PayItemAn4 = 0;
            $v_PayItemAnType4 = '';
        }

        if (isset($_REQUEST["PayItemAn5"]) && isset($_REQUEST["PayItemAnType5"])) {
            $v_PayItemAn5 = 1;
            $v_PayItemAnType5 = trim($_REQUEST["PayItemAnType5"]);
        } elseif (isset($_REQUEST["PayItemAn5"])) {
            $v_PayItemAn5 = 1;
            $v_PayItemAnType5 = 'ANN';
        } else {
            $v_PayItemAn5 = 0;
            $v_PayItemAnType5 = '';
        }

        if (isset($_REQUEST["PayItemAn6"]) && isset($_REQUEST["PayItemAnType6"])) {
            $v_PayItemAn6 = 1;
            $v_PayItemAnType6 = trim($_REQUEST["PayItemAnType6"]);
        } elseif (isset($_REQUEST["PayItemAn6"])) {
            $v_PayItemAn6 = 1;
            $v_PayItemAnType6 = 'ANN';
        } else {
            $v_PayItemAn6 = 0;
            $v_PayItemAnType6 = '';
        }

        if (isset($_REQUEST["PayItemAn7"]) && isset($_REQUEST["PayItemAnType7"])) {
            $v_PayItemAn7 = 1;
            $v_PayItemAnType7 = trim($_REQUEST["PayItemAnType7"]);
        } elseif (isset($_REQUEST["PayItemAn7"])) {
            $v_PayItemAn7 = 1;
            $v_PayItemAnType7 = 'ANN';
        } else {
            $v_PayItemAn7 = 0;
            $v_PayItemAnType7 = '';
        }

        if (isset($_REQUEST["PayItemAn8"]) && isset($_REQUEST["PayItemAnType8"])) {
            $v_PayItemAn8 = 1;
            $v_PayItemAnType8 = trim($_REQUEST["PayItemAnType8"]);
        } elseif (isset($_REQUEST["PayItemAn8"])) {
            $v_PayItemAn8 = 1;
            $v_PayItemAnType8 = 'ANN';
        } else {
            $v_PayItemAn8 = 0;
            $v_PayItemAnType8 = '';
        }

        if (isset($_REQUEST["PayItemAn9"]) && isset($_REQUEST["PayItemAnType9"])) {
            $v_PayItemAn9 = 1;
            $v_PayItemAnType9 = trim($_REQUEST["PayItemAnType9"]);
        } elseif (isset($_REQUEST["PayItemAn9"])) {
            $v_PayItemAn9 = 1;
            $v_PayItemAnType9 = 'ANN';
        } else {
            $v_PayItemAn9 = 0;
            $v_PayItemAnType9 = '';
        }

        if (isset($_REQUEST["PayItemAn10"]) && isset($_REQUEST["PayItemAnType10"])) {
            $v_PayItemAn10 = 1;
            $v_PayItemAnType10 = trim($_REQUEST["PayItemAnType10"]);
        } elseif (isset($_REQUEST["PayItemAn10"])) {
            $v_PayItemAn10 = 1;
            $v_PayItemAnType10 = 'ANN';
        } else {
            $v_PayItemAn10 = 0;
            $v_PayItemAnType10 = '';
        }

        if (isset($_REQUEST["PayItemAn11"]) && isset($_REQUEST["PayItemAnType11"])) {
            $v_PayItemAn11 = 1;
            $v_PayItemAnType11 = trim($_REQUEST["PayItemAnType11"]);
        } elseif (isset($_REQUEST["PayItemAn11"])) {
            $v_PayItemAn11 = 1;
            $v_PayItemAnType11 = 'ANN';
        } else {
            $v_PayItemAn11 = 0;
            $v_PayItemAnType11 = '';
        }

        if (isset($_REQUEST["PayItemAn12"]) && isset($_REQUEST["PayItemAnType12"])) {
            $v_PayItemAn12 = 1;
            $v_PayItemAnType12 = trim($_REQUEST["PayItemAnType12"]);
        } elseif (isset($_REQUEST["PayItemAn12"])) {
            $v_PayItemAn12 = 1;
            $v_PayItemAnType12 = 'ANN';
        } else {
            $v_PayItemAn12 = 0;
            $v_PayItemAnType12 = '';
        }

        if (isset($_REQUEST["PayItemAn13"]) && isset($_REQUEST["PayItemAnType13"])) {
            $v_PayItemAn13 = 1;
            $v_PayItemAnType13 = trim($_REQUEST["PayItemAnType13"]);
        } elseif (isset($_REQUEST["PayItemAn13"])) {
            $v_PayItemAn13 = 1;
            $v_PayItemAnType13 = 'ANN';
        } else {
            $v_PayItemAn13 = 0;
            $v_PayItemAnType13 = '';
        }

        if (isset($_REQUEST["PayItemAn14"]) && isset($_REQUEST["PayItemAnType14"])) {
            $v_PayItemAn14 = 1;
            $v_PayItemAnType14 = trim($_REQUEST["PayItemAnType14"]);
        } elseif (isset($_REQUEST["PayItemAn14"])) {
            $v_PayItemAn14 = 1;
            $v_PayItemAnType14 = 'ANN';
        } else {
            $v_PayItemAn14 = 0;
            $v_PayItemAnType14 = '';
        }

        if (isset($_REQUEST["PayItemAn15"]) && isset($_REQUEST["PayItemAnType15"])) {
            $v_PayItemAn15 = 1;
            $v_PayItemAnType15 = trim($_REQUEST["PayItemAnType15"]);
        } elseif (isset($_REQUEST["PayItemAn15"])) {
            $v_PayItemAn15 = 1;
            $v_PayItemAnType15 = 'ANN';
        } else {
            $v_PayItemAn15 = 0;
            $v_PayItemAnType15 = '';
        }

        if (isset($_REQUEST["PayItemAn16"]) && isset($_REQUEST["PayItemAnType16"])) {
            $v_PayItemAn16 = 1;
            $v_PayItemAnType16 = trim($_REQUEST["PayItemAnType16"]);
        } elseif (isset($_REQUEST["PayItemAn16"])) {
            $v_PayItemAn16 = 1;
            $v_PayItemAnType16 = 'ANN';
        } else {
            $v_PayItemAn16 = 0;
            $v_PayItemAnType16 = '';
        }

        if (isset($_REQUEST["PayItemAn17"]) && isset($_REQUEST["PayItemAnType17"])) {
            $v_PayItemAn17 = 1;
            $v_PayItemAnType17 = trim($_REQUEST["PayItemAnType17"]);
        } elseif (isset($_REQUEST["PayItemAn17"])) {
            $v_PayItemAn17 = 1;
            $v_PayItemAnType17 = 'ANN';
        } else {
            $v_PayItemAn17 = 0;
            $v_PayItemAnType17 = '';
        }

        if (isset($_REQUEST["PayItemAn18"]) && isset($_REQUEST["PayItemAnType18"])) {
            $v_PayItemAn18 = 1;
            $v_PayItemAnType18 = trim($_REQUEST["PayItemAnType18"]);
        } elseif (isset($_REQUEST["PayItemAn18"])) {
            $v_PayItemAn18 = 1;
            $v_PayItemAnType18 = 'ANN';
        } else {
            $v_PayItemAn18 = 0;
            $v_PayItemAnType18 = '';
        }

        if (isset($_REQUEST["PayItemAn19"]) && isset($_REQUEST["PayItemAnType19"])) {
            $v_PayItemAn19 = 1;
            $v_PayItemAnType19 = trim($_REQUEST["PayItemAnType19"]);
        } elseif (isset($_REQUEST["PayItemAn19"])) {
            $v_PayItemAn19 = 1;
            $v_PayItemAnType19 = 'ANN';
        } else {
            $v_PayItemAn19 = 0;
            $v_PayItemAnType19 = '';
        }

        if (isset($_REQUEST["PayItemAn20"]) && isset($_REQUEST["PayItemAnType20"])) {
            $v_PayItemAn20 = 1;
            $v_PayItemAnType20 = trim($_REQUEST["PayItemAnType20"]);
        } elseif (isset($_REQUEST["PayItemAn20"])) {
            $v_PayItemAn20 = 1;
            $v_PayItemAnType20 = 'ANN';
        } else {
            $v_PayItemAn20 = 0;
            $v_PayItemAnType20 = '';
        }

        if (isset($_REQUEST["PayItemAn21"]) && isset($_REQUEST["PayItemAnType21"])) {
            $v_PayItemAn21 = 1;
            $v_PayItemAnType21 = trim($_REQUEST["PayItemAnType21"]);
        } elseif (isset($_REQUEST["PayItemAn21"])) {
            $v_PayItemAn21 = 1;
            $v_PayItemAnType21 = 'ANN';
        } else {
            $v_PayItemAn21 = 0;
            $v_PayItemAnType21 = '';
        }

        // if (isset($_REQUEST["PayItemAn2"])){$v_PayItemAn2=1;} else{$v_PayItemAn2=0;}
        // if (isset($_REQUEST["PayItemAn3"])){$v_PayItemAn3=1;} else{$v_PayItemAn3=0;}
        // if (isset($_REQUEST["PayItemAn4"])){$v_PayItemAn4=1;} else{$v_PayItemAn4=0;}
        // if (isset($_REQUEST["PayItemAn5"])){$v_PayItemAn5=1;} else{$v_PayItemAn5=0;}
        // if (isset($_REQUEST["PayItemAn6"])){$v_PayItemAn6=1;} else{$v_PayItemAn6=0;}
        // if (isset($_REQUEST["PayItemAn7"])){$v_PayItemAn7=1;} else{$v_PayItemAn7=0;}
        // if (isset($_REQUEST["PayItemAn8"])){$v_PayItemAn8=1;} else{$v_PayItemAn8=0;}
        // if (isset($_REQUEST["PayItemAn9"])){$v_PayItemAn9=1;} else{$v_PayItemAn9=0;}
        // if (isset($_REQUEST["PayItemAn10"])){$v_PayItemAn10=1;} else{$v_PayItemAn10=0;}
        // if (isset($_REQUEST["PayItemAn11"])){$v_PayItemAn11=1;} else{$v_PayItemAn11=0;}
        // if (isset($_REQUEST["PayItemAn12"])){$v_PayItemAn12=1;} else{$v_PayItemAn12=0;}
        // if (isset($_REQUEST["PayItemAn13"])){$v_PayItemAn13=1;} else{$v_PayItemAn13=0;}
        // if (isset($_REQUEST["PayItemAn14"])){$v_PayItemAn14=1;} else{$v_PayItemAn14=0;}
        // if (isset($_REQUEST["PayItemAn15"])){$v_PayItemAn15=1;} else{$v_PayItemAn15=0;}
        // if (isset($_REQUEST["PayItemAn16"])){$v_PayItemAn16=1;} else{$v_PayItemAn16=0;}
        // if (isset($_REQUEST["PayItemAn17"])){$v_PayItemAn17=1;} else{$v_PayItemAn17=0;}
        // if (isset($_REQUEST["PayItemAn18"])){$v_PayItemAn18=1;} else{$v_PayItemAn18=0;}
        // if (isset($_REQUEST["PayItemAn19"])){$v_PayItemAn19=1;} else{$v_PayItemAn19=0;}
        // if (isset($_REQUEST["PayItemAn20"])){$v_PayItemAn20=1;} else{$v_PayItemAn20=0;}
        // if (isset($_REQUEST["PayItemAn21"])){$v_PayItemAn21=1;} else{$v_PayItemAn21=0;}

        // if (isset($_REQUEST["PayItemAnType1"]) && $v_PayItemAn1=1){$v_PayItemAnType1=trim($_REQUEST["PayItemAnType1"]);}
        // else{$v_PayItemAnType1='ANN';}
        // if (isset($_REQUEST["PayItemAnType2"])){$v_PayItemAnType2=trim($_REQUEST["PayItemAnType2"]);} else{$v_PayItemAnType2='ANN';}
        // if (isset($_REQUEST["PayItemAnType3"])){$v_PayItemAnType3=trim($_REQUEST["PayItemAnType3"]);} else{$v_PayItemAnType3='ANN';}
        // if (isset($_REQUEST["PayItemAnType4"])){$v_PayItemAnType4=trim($_REQUEST["PayItemAnType4"]);} else{$v_PayItemAnType4='ANN';}
        // if (isset($_REQUEST["PayItemAnType5"])){$v_PayItemAnType5=trim($_REQUEST["PayItemAnType5"]);} else{$v_PayItemAnType5='ANN';}
        // if (isset($_REQUEST["PayItemAnType6"])){$v_PayItemAnType6=trim($_REQUEST["PayItemAnType6"]);} else{$v_PayItemAnType6='ANN';}
        // if (isset($_REQUEST["PayItemAnType7"])){$v_PayItemAnType7=trim($_REQUEST["PayItemAnType7"]);} else{$v_PayItemAnType7='ANN';}
        // if (isset($_REQUEST["PayItemAnType8"])){$v_PayItemAnType8=trim($_REQUEST["PayItemAnType8"]);} else{$v_PayItemAnType8='ANN';}
        // if (isset($_REQUEST["PayItemAnType9"])){$v_PayItemAnType9=trim($_REQUEST["PayItemAnType9"]);} else{$v_PayItemAnType9='ANN';}
        // if (isset($_REQUEST["PayItemAnType10"])){$v_PayItemAnType10=trim($_REQUEST["PayItemAnType10"]);} else{$v_PayItemAnType10='ANN';}
        // if (isset($_REQUEST["PayItemAnType11"])){$v_PayItemAnType11=trim($_REQUEST["PayItemAnType11"]);} else{$v_PayItemAnType11='ANN';}
        // if (isset($_REQUEST["PayItemAnType12"])){$v_PayItemAnType12=trim($_REQUEST["PayItemAnType12"]);} else{$v_PayItemAnType12='ANN';}
        // if (isset($_REQUEST["PayItemAnType13"])){$v_PayItemAnType13=trim($_REQUEST["PayItemAnType13"]);} else{$v_PayItemAnType13='ANN';}
        // if (isset($_REQUEST["PayItemAnType14"])){$v_PayItemAnType14=trim($_REQUEST["PayItemAnType14"]);} else{$v_PayItemAnType14='ANN';}
        // if (isset($_REQUEST["PayItemAnType15"])){$v_PayItemAnType15=trim($_REQUEST["PayItemAnType15"]);} else{$v_PayItemAnType15='ANN';}
        // if (isset($_REQUEST["PayItemAnType16"])){$v_PayItemAnType16=trim($_REQUEST["PayItemAnType16"]);} else{$v_PayItemAnType16='ANN';}
        // if (isset($_REQUEST["PayItemAnType17"])){$v_PayItemAnType17=trim($_REQUEST["PayItemAnType17"]);} else{$v_PayItemAnType17='ANN';}
        // if (isset($_REQUEST["PayItemAnType18"])){$v_PayItemAnType18=trim($_REQUEST["PayItemAnType18"]);} else{$v_PayItemAnType18='ANN';}
        // if (isset($_REQUEST["PayItemAnType19"])){$v_PayItemAnType19=trim($_REQUEST["PayItemAnType19"]);} else{$v_PayItemAnType19='ANN';}
        // if (isset($_REQUEST["PayItemAnType20"])){$v_PayItemAnType20=trim($_REQUEST["PayItemAnType20"]);} else{$v_PayItemAnType20='ANN';}
        // if (isset($_REQUEST["PayItemAnType21"])){$v_PayItemAnType21=trim($_REQUEST["PayItemAnType21"]);} else{$v_PayItemAnType21='ANN';}

        //------------------ extended -------------
        if (isset($_REQUEST["ShowPaySlip1"])) {
            $ShowPaySlip1 = 1;
        } else {
            $ShowPaySlip1 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip2"])) {
            $ShowPaySlip2 = 1;
        } else {
            $ShowPaySlip2 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip3"])) {
            $ShowPaySlip3 = 1;
        } else {
            $ShowPaySlip3 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip4"])) {
            $ShowPaySlip4 = 1;
        } else {
            $ShowPaySlip4 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip5"])) {
            $ShowPaySlip5 = 1;
        } else {
            $ShowPaySlip5 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip6"])) {
            $ShowPaySlip6 = 1;
        } else {
            $ShowPaySlip6 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip7"])) {
            $ShowPaySlip7 = 1;
        } else {
            $ShowPaySlip7 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip8"])) {
            $ShowPaySlip8 = 1;
        } else {
            $ShowPaySlip8 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip9"])) {
            $ShowPaySlip9 = 1;
        } else {
            $ShowPaySlip9 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip10"])) {
            $ShowPaySlip10 = 1;
        } else {
            $ShowPaySlip10 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip11"])) {
            $ShowPaySlip11 = 1;
        } else {
            $ShowPaySlip11 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip12"])) {
            $ShowPaySlip12 = 1;
        } else {
            $ShowPaySlip12 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip13"])) {
            $ShowPaySlip13 = 1;
        } else {
            $ShowPaySlip13 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip14"])) {
            $ShowPaySlip14 = 1;
        } else {
            $ShowPaySlip14 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip15"])) {
            $ShowPaySlip15 = 1;
        } else {
            $ShowPaySlip15 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip16"])) {
            $ShowPaySlip16 = 1;
        } else {
            $ShowPaySlip16 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip17"])) {
            $ShowPaySlip17 = 1;
        } else {
            $ShowPaySlip17 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip18"])) {
            $ShowPaySlip18 = 1;
        } else {
            $ShowPaySlip18 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip19"])) {
            $ShowPaySlip19 = 1;
        } else {
            $ShowPaySlip19 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip20"])) {
            $ShowPaySlip20 = 1;
        } else {
            $ShowPaySlip20 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip21"])) {
            $ShowPaySlip21 = 1;
        } else {
            $ShowPaySlip21 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip22"])) {
            $ShowPaySlip22 = 1;
        } else {
            $ShowPaySlip22 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip23"])) {
            $ShowPaySlip23 = 1;
        } else {
            $ShowPaySlip23 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip24"])) {
            $ShowPaySlip24 = 1;
        } else {
            $ShowPaySlip24 = 0;
        }
        if (isset($_REQUEST["ShowPaySlip25"])) {
            $ShowPaySlip25 = 1;
        } else {
            $ShowPaySlip25 = 0;
        }

        if (isset($_REQUEST["UpdID"]) && strlen(trim($_REQUEST["UpdID"])) == 32) {
            $EditID = ECh($_REQUEST["UpdID"]);

            $Script_CCode = "SELECT [GName] from [Fin_PRSettings] where HashKey='" . $EditID . "'";

            //            [GName] ='".ECh($v_GName)."'
            $Script = "UPDATE [Fin_PRSettings]
			SET
			[AddPen] ='" . ECh($v_AddPen) . "'
			,[GrossNm] ='" . ECh($v_GrossNm) . "'
			,[GrossOF] ='" . ECh($v_GrossOF) . "'
			,[PayItem1] ='" . ECh($v_PayItem1) . "'
			,[PayItemCode1] = '" . ECh($_REQUEST["PayItemCode1"]) . "'
			,[PayItemNm1] ='" . ECh($v_PayItemNm1) . "'
			,[PayItemOF1] ='" . ECh($v_PayItemOF1) . "'
			,[PayItemPen1] ='" . ECh($v_PayItemPEN1) . "'
			,[PayItemTX1] ='" . ECh($v_PayItemTX1) . "'
			,[PayItemCD1] ='CR'
			,[PayItem2] ='" . ECh($v_PayItem2) . "'
			,[PayItemCode2] = '" . ECh($_REQUEST["PayItemCode2"]) . "'
			,[PayItemNm2] ='" . ECh($v_PayItemNm2) . "'
			,[PayItemOF2] ='" . ECh($v_PayItemOF2) . "'
			,[PayItemPen2] ='" . ECh($v_PayItemPEN2) . "'
			,[PayItemCD2] ='CR'
			,[PayItemTX2] ='" . ECh($v_PayItemTX2) . "'
			,[PayItem3] ='" . ECh($v_PayItem3) . "'
			,[PayItemCode3] = '" . ECh($_REQUEST["PayItemCode3"]) . "'
			,[PayItemNm3] ='" . ECh($v_PayItemNm3) . "'
			,[PayItemOF3] ='" . ECh($v_PayItemOF3) . "'
			,[PayItemPen3] ='" . ECh($v_PayItemPEN3) . "'
			,[PayItemCD3] ='CR'
			,[PayItemTX3] ='" . ECh($v_PayItemTX3) . "'
			,[PayItem4] ='" . ECh($v_PayItem4) . "'
			,[PayItemCode4] = '" . ECh($_REQUEST["PayItemCode4"]) . "'
			,[PayItemNm4] ='" . ECh($v_PayItemNm4) . "'
			,[PayItemOF4] ='" . ECh($v_PayItemOF4) . "'
			,[PayItemPen4] ='" . ECh($v_PayItemPEN4) . "'
			,[PayItemCD4] ='" . ECh($v_PayItemCD4) . "'
			,[PayItemTX4] ='" . ECh($v_PayItemTX4) . "'
			,[PayItem5] ='" . ECh($v_PayItem5) . "'
			,[PayItemCode5] = '" . ECh($_REQUEST["PayItemCode5"]) . "'
			,[PayItemNm5] ='" . ECh($v_PayItemNm5) . "'
			,[PayItemOF5] ='" . ECh($v_PayItemOF5) . "'
			,[PayItemPen5] ='" . ECh($v_PayItemPEN5) . "'
			,[PayItemCD5] ='" . ECh($v_PayItemCD5) . "'
			,[PayItemTX5] ='" . ECh($v_PayItemTX5) . "'
			,[PayItem6] ='" . ECh($v_PayItem6) . "'
			,[PayItemCode6] = '" . ECh($_REQUEST["PayItemCode6"]) . "'
			,[PayItemNm6] ='" . ECh($v_PayItemNm6) . "'
			,[PayItemOF6] ='" . ECh($v_PayItemOF6) . "'
			,[PayItemPen6] ='" . ECh($v_PayItemPEN6) . "'
			,[PayItemCD6] ='" . ECh($v_PayItemCD6) . "'
			,[PayItemTX6] ='" . ECh($v_PayItemTX6) . "'
			,[PayItem7] ='" . ECh($v_PayItem7) . "'
			,[PayItemCode7] = '" . ECh($_REQUEST["PayItemCode7"]) . "'
			,[PayItemNm7] ='" . ECh($v_PayItemNm7) . "'
			,[PayItemOF7] ='" . ECh($v_PayItemOF7) . "'
			,[PayItemPen7] ='" . ECh($v_PayItemPEN7) . "'
			,[PayItemCD7] ='" . ECh($v_PayItemCD7) . "'
			,[PayItemTX7] ='" . ECh($v_PayItemTX7) . "'
			,[PayItem8] ='" . ECh($v_PayItem8) . "'
			,[PayItemCode8] = '" . ECh($_REQUEST["PayItemCode8"]) . "'
			,[PayItemNm8] ='" . ECh($v_PayItemNm8) . "'
			,[PayItemOF8] ='" . ECh($v_PayItemOF8) . "'
			,[PayItemPen8] ='" . ECh($v_PayItemPEN8) . "'
			,[PayItemCD8] ='" . ECh($v_PayItemCD8) . "'
			,[PayItemTX8] ='" . ECh($v_PayItemTX8) . "'
			,[PayItem9] ='" . ECh($v_PayItem9) . "'
			,[PayItemCode9] = '" . ECh($_REQUEST["PayItemCode9"]) . "'
			,[PayItemNm9] ='" . ECh($v_PayItemNm9) . "'
			,[PayItemOF9] ='" . ECh($v_PayItemOF9) . "'
			,[PayItemPen9] ='" . ECh($v_PayItemPEN9) . "'
			,[PayItemCD9] ='" . ECh($v_PayItemCD9) . "'
			,[PayItemTX9] ='" . ECh($v_PayItemTX9) . "'
			,[PayItem10] ='" . ECh($v_PayItem10) . "'
			,[PayItemCode10] = '" . ECh($_REQUEST["PayItemCode10"]) . "'
			,[PayItemNm10] ='" . ECh($v_PayItemNm10) . "'
			,[PayItemOF10] ='" . ECh($v_PayItemOF10) . "'
			,[PayItemPen10] ='" . ECh($v_PayItemPEN10) . "'
			,[PayItemCD10] ='" . ECh($v_PayItemCD10) . "'
			,[PayItemTX10] ='" . ECh($v_PayItemTX10) . "'
			,[PayItem11] ='" . ECh($v_PayItem11) . "'
			,[PayItemCode11] = '" . ECh($_REQUEST["PayItemCode11"]) . "'
			,[PayItemNm11] ='" . ECh($v_PayItemNm11) . "'
			,[PayItemOF11] ='" . ECh($v_PayItemOF11) . "'
			,[PayItemPen11] ='" . ECh($v_PayItemPEN11) . "'
			,[PayItemCD11] ='" . ECh($v_PayItemCD11) . "'
			,[PayItemTX11] ='" . ECh($v_PayItemTX11) . "'
			,[PayItem12] ='" . ECh($v_PayItem12) . "'
			,[PayItemCode12] = '" . ECh($_REQUEST["PayItemCode12"]) . "'
			,[PayItemNm12] ='" . ECh($v_PayItemNm12) . "'
			,[PayItemOF12] ='" . ECh($v_PayItemOF12) . "'
			,[PayItemPen12] ='" . ECh($v_PayItemPEN12) . "'
			,[PayItemCD12] ='" . ECh($v_PayItemCD12) . "'
			,[PayItemTX12] ='" . ECh($v_PayItemTX12) . "'
			,[PayItem13] ='" . ECh($v_PayItem13) . "'
			,[PayItemCode13] = '" . ECh($_REQUEST["PayItemCode13"]) . "'
			,[PayItemNm13] ='" . ECh($v_PayItemNm13) . "'
			,[PayItemOF13] ='" . ECh($v_PayItemOF13) . "'
			,[PayItemPen13] ='" . ECh($v_PayItemPEN13) . "'
			,[PayItemCD13] ='" . ECh($v_PayItemCD13) . "'
			,[PayItemTX13] ='" . ECh($v_PayItemTX13) . "'
			,[PayItem14] ='" . ECh($v_PayItem14) . "'
			,[PayItemCode14] = '" . ECh($_REQUEST["PayItemCode14"]) . "'
			,[PayItemNm14] ='" . ECh($v_PayItemNm14) . "'
			,[PayItemOF14] ='" . ECh($v_PayItemOF14) . "'
			,[PayItemPen14] ='" . ECh($v_PayItemPEN14) . "'
			,[PayItemCD14] ='" . ECh($v_PayItemCD14) . "'
			,[PayItemTX14] ='" . ECh($v_PayItemTX14) . "'
			,[PayItem15] ='" . ECh($v_PayItem15) . "'
			,[PayItemCode15] = '" . ECh($_REQUEST["PayItemCode15"]) . "'
			,[PayItemNm15] ='" . ECh($v_PayItemNm15) . "'
			,[PayItemOF15] ='" . ECh($v_PayItemOF15) . "'
			,[PayItemPen15] ='" . ECh($v_PayItemPEN15) . "'
			,[PayItemCD15] ='" . ECh($v_PayItemCD15) . "'
			,[PayItemTX15] ='" . ECh($v_PayItemTX15) . "'
			,[PayItem16] ='" . ECh($v_PayItem16) . "'
			,[PayItemCode16] = '" . ECh($_REQUEST["PayItemCode16"]) . "'
			,[PayItemNm16] ='" . ECh($v_PayItemNm16) . "'
			,[PayItemOF16] ='" . ECh($v_PayItemOF16) . "'
			,[PayItemPen16] ='" . ECh($v_PayItemPEN16) . "'
			,[PayItemCD16] ='" . ECh($v_PayItemCD16) . "'
			,[PayItemTX16] ='" . ECh($v_PayItemTX16) . "'
			,[PayItem17] ='" . ECh($v_PayItem17) . "'
			,[PayItemCode17] = '" . ECh($_REQUEST["PayItemCode17"]) . "'
			,[PayItemNm17] ='" . ECh($v_PayItemNm17) . "'
			,[PayItemOF17] ='" . ECh($v_PayItemOF17) . "'
			,[PayItemPen17] ='" . ECh($v_PayItemPEN17) . "'
			,[PayItemCD17] ='" . ECh($v_PayItemCD17) . "'
			,[PayItemTX17] ='" . ECh($v_PayItemTX17) . "'
			,[PayItem18] ='" . ECh($v_PayItem18) . "'
			,[PayItemCode18] = '" . ECh($_REQUEST["PayItemCode18"]) . "'
			,[PayItemNm18] ='" . ECh($v_PayItemNm18) . "'
			,[PayItemOF18] ='" . ECh($v_PayItemOF18) . "'
			,[PayItemPen18] ='" . ECh($v_PayItemPEN18) . "'
			,[PayItemCD18] ='" . ECh($v_PayItemCD18) . "'
			,[PayItemTX18] ='" . ECh($v_PayItemTX18) . "'
			,[PayItem19] ='" . ECh($v_PayItem19) . "'
			,[PayItemCode19] = '" . ECh($_REQUEST["PayItemCode19"]) . "'
			,[PayItemNm19] ='" . ECh($v_PayItemNm19) . "'
			,[PayItemOF19] ='" . ECh($v_PayItemOF19) . "'
			,[PayItemPen19] ='" . ECh($v_PayItemPEN19) . "'
			,[PayItemCD19] ='" . ECh($v_PayItemCD19) . "'
			,[PayItemTX19] ='" . ECh($v_PayItemTX19) . "'
			,[PayItem20] ='" . ECh($v_PayItem20) . "'
			,[PayItemCode20] = '" . ECh($_REQUEST["PayItemCode20"]) . "'
			,[PayItemNm20] ='" . ECh($v_PayItemNm20) . "'
			,[PayItemOF20] ='" . ECh($v_PayItemOF20) . "'
			,[PayItemPen20] ='" . ECh($v_PayItemPEN20) . "'
			,[PayItemCD20] ='" . ECh($v_PayItemCD20) . "'
			,[PayItemTX20] ='" . ECh($v_PayItemTX20) . "'
			,[ShowPaySlip1] ='" . ECh($ShowPaySlip1) . "'
			,[ShowPaySlip2] ='" . ECh($ShowPaySlip2) . "'
			,[ShowPaySlip3] ='" . ECh($ShowPaySlip3) . "'
			,[ShowPaySlip4] ='" . ECh($ShowPaySlip4) . "'
			,[ShowPaySlip5] ='" . ECh($ShowPaySlip5) . "'
			,[ShowPaySlip6] ='" . ECh($ShowPaySlip6) . "'
			,[ShowPaySlip7] ='" . ECh($ShowPaySlip7) . "'
			,[ShowPaySlip8] ='" . ECh($ShowPaySlip8) . "'
			,[ShowPaySlip9] ='" . ECh($ShowPaySlip9) . "'
			,[ShowPaySlip10] ='" . ECh($ShowPaySlip10) . "'
			,[ShowPaySlip11] ='" . ECh($ShowPaySlip11) . "'
			,[ShowPaySlip12] ='" . ECh($ShowPaySlip12) . "'
			,[ShowPaySlip13] ='" . ECh($ShowPaySlip13) . "'
			,[ShowPaySlip14] ='" . ECh($ShowPaySlip14) . "'
			,[ShowPaySlip15] ='" . ECh($ShowPaySlip15) . "'
			,[ShowPaySlip16] ='" . ECh($ShowPaySlip16) . "'
			,[ShowPaySlip17] ='" . ECh($ShowPaySlip17) . "'
			,[ShowPaySlip18] ='" . ECh($ShowPaySlip18) . "'
			,[ShowPaySlip19] ='" . ECh($ShowPaySlip19) . "'
			,[ShowPaySlip20] ='" . ECh($ShowPaySlip20) . "'
			,[ShowPaySlip21] ='" . ECh($ShowPaySlip21) . "'
			,[ShowPaySlip22] ='" . ECh($ShowPaySlip22) . "'
			,[ShowPaySlip23] ='" . ECh($ShowPaySlip23) . "'
			,[ShowPaySlip24] ='" . ECh($ShowPaySlip24) . "'
			,[ShowPaySlip25] ='" . ECh($ShowPaySlip25) . "'

			,[PayItemAn1] ='" . ECh($v_PayItemAn1) . "'
			,[PayItemAn2] ='" . ECh($v_PayItemAn2) . "'
			,[PayItemAn3] ='" . ECh($v_PayItemAn3) . "'
			,[PayItemAn4] ='" . ECh($v_PayItemAn4) . "'
			,[PayItemAn5] ='" . ECh($v_PayItemAn5) . "'
			,[PayItemAn6] ='" . ECh($v_PayItemAn6) . "'
			,[PayItemAn7] ='" . ECh($v_PayItemAn7) . "'
			,[PayItemAn8] ='" . ECh($v_PayItemAn8) . "'
			,[PayItemAn9] ='" . ECh($v_PayItemAn9) . "'
			,[PayItemAn10] ='" . ECh($v_PayItemAn10) . "'
			,[PayItemAn11] ='" . ECh($v_PayItemAn11) . "'
			,[PayItemAn12] ='" . ECh($v_PayItemAn12) . "'
			,[PayItemAn13] ='" . ECh($v_PayItemAn13) . "'
			,[PayItemAn14] ='" . ECh($v_PayItemAn14) . "'
			,[PayItemAn15] ='" . ECh($v_PayItemAn15) . "'
			,[PayItemAn16] ='" . ECh($v_PayItemAn16) . "'
			,[PayItemAn17] ='" . ECh($v_PayItemAn17) . "'
			,[PayItemAn18] ='" . ECh($v_PayItemAn18) . "'
			,[PayItemAn19] ='" . ECh($v_PayItemAn19) . "'
			,[PayItemAn20] ='" . ECh($v_PayItemAn20) . "'
			,[PayItemAn21] ='" . ECh($v_PayItemAn21) . "'

			,PayItemAnType1 ='" . ECh($v_PayItemAnType1) . "',
			PayItemAnType2 ='" . ECh($v_PayItemAnType2) . "',
PayItemAnType3 ='" . ECh($v_PayItemAnType3) . "',
PayItemAnType4 ='" . ECh($v_PayItemAnType4) . "',
PayItemAnType5 ='" . ECh($v_PayItemAnType5) . "',
PayItemAnType6 ='" . ECh($v_PayItemAnType6) . "',
PayItemAnType7 ='" . ECh($v_PayItemAnType7) . "',
PayItemAnType8 ='" . ECh($v_PayItemAnType8) . "',
PayItemAnType9 ='" . ECh($v_PayItemAnType9) . "',
PayItemAnType10 ='" . ECh($v_PayItemAnType10) . "',
PayItemAnType11 ='" . ECh($v_PayItemAnType11) . "',
PayItemAnType12 ='" . ECh($v_PayItemAnType12) . "',
PayItemAnType13 ='" . ECh($v_PayItemAnType13) . "',
PayItemAnType14 ='" . ECh($v_PayItemAnType14) . "',
PayItemAnType15 ='" . ECh($v_PayItemAnType15) . "',
PayItemAnType16 ='" . ECh($v_PayItemAnType16) . "',
PayItemAnType17 ='" . ECh($v_PayItemAnType17) . "',
PayItemAnType18 ='" . ECh($v_PayItemAnType18) . "',
PayItemAnType19 ='" . ECh($v_PayItemAnType19) . "',
PayItemAnType20 ='" . ECh($v_PayItemAnType20) . "',
PayItemAnType21 ='" . ECh($v_PayItemAnType21) . "'

			,[PAYEScheme] ='" . ECh($_REQUEST["PAYEScheme"]) . "'
			,[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "'
			,[UpdatedDate] = GetDate()
			,[Status] = 'A'
			WHERE [HashKey] = '" . $EditID . "'";
            // print_r($Script);die();
            ScriptRunnerUD($Script, "Authorize");

            /*
            $Script = "UPDATE [Fin_PRSettings]
            SET
            [GrossNm] ='".ECh($v_GrossNm)."'
            ,[PayItemNm1] ='".ECh($v_PayItemNm1)."'
            ,[PayItemNm2] ='".ECh($v_PayItemNm2)."'
            ,[PayItemNm3] ='".ECh($v_PayItemNm3)."'
            ,[PayItemNm4] ='".ECh($v_PayItemNm4)."'
            ,[PayItemNm5] ='".ECh($v_PayItemNm5)."'
            ,[PayItemNm6] ='".ECh($v_PayItemNm6)."'
            ,[PayItemNm7] ='".ECh($v_PayItemNm7)."'
            ,[PayItemNm8] ='".ECh($v_PayItemNm8)."'
            ,[PayItemNm9] ='".ECh($v_PayItemNm9)."'
            ,[PayItemNm10] ='".ECh($v_PayItemNm10)."'
            ,[PayItemNm11] ='".ECh($v_PayItemNm11)."'
            ,[PayItemNm12] ='".ECh($v_PayItemNm12)."'
            ,[PayItemNm13] ='".ECh($v_PayItemNm13)."'
            ,[PayItemNm14] ='".ECh($v_PayItemNm14)."'
            ,[PayItemNm15] ='".ECh($v_PayItemNm15)."'
            ,[PayItemNm16] ='".ECh($v_PayItemNm16)."'
            ,[PayItemNm17] ='".ECh($v_PayItemNm17)."'
            ,[PayItemNm18] ='".ECh($v_PayItemNm18)."'
            ,[PayItemNm19] ='".ECh($v_PayItemNm19)."'
            ,[PayItemNm20] ='".ECh($v_PayItemNm20)."'

            ,[PensionEmployeeCode] = '".ECh($_REQUEST["PensionEmployeeCode"])."'
            ,[PensionEmployerCode] = '".ECh($_REQUEST["PensionEmployerCode"])."'
            ,[LeaveAllowanceCode] = '".ECh($_REQUEST["LeaveAllowanceCode"])."'
            ,[PAYECode] = '".ECh($_REQUEST["PAYECode"])."'
            ,[NetPayCode] = '".ECh($_REQUEST["NetPayCode"])."'
            ,[PayItemCode1] = '".ECh($_REQUEST["PayItemCode1"])."'
            ,[PayItemCode2] = '".ECh($_REQUEST["PayItemCode2"])."'
            ,[PayItemCode3] = '".ECh($_REQUEST["PayItemCode3"])."'
            ,[PayItemCode4] = '".ECh($_REQUEST["PayItemCode4"])."'
            ,[PayItemCode5] = '".ECh($_REQUEST["PayItemCode5"])."'
            ,[PayItemCode6] = '".ECh($_REQUEST["PayItemCode6"])."'
            ,[PayItemCode7] = '".ECh($_REQUEST["PayItemCode7"])."'
            ,[PayItemCode8] = '".ECh($_REQUEST["PayItemCode8"])."'
            ,[PayItemCode9] = '".ECh($_REQUEST["PayItemCode9"])."'
            ,[PayItemCode10] = '".ECh($_REQUEST["PayItemCode10"])."'
            ,[PayItemCode11] = '".ECh($_REQUEST["PayItemCode11"])."'
            ,[PayItemCode12] = '".ECh($_REQUEST["PayItemCode12"])."'
            ,[PayItemCode13] = '".ECh($_REQUEST["PayItemCode13"])."'
            ,[PayItemCode14] = '".ECh($_REQUEST["PayItemCode14"])."'
            ,[PayItemCode15] = '".ECh($_REQUEST["PayItemCode15"])."'
            ,[PayItemCode16] = '".ECh($_REQUEST["PayItemCode16"])."'
            ,[PayItemCode17] = '".ECh($_REQUEST["PayItemCode17"])."'
            ,[PayItemCode18] = '".ECh($_REQUEST["PayItemCode18"])."'
            ,[PayItemCode19] = '".ECh($_REQUEST["PayItemCode19"])."'
            ,[PayItemCode20] = '".ECh($_REQUEST["PayItemCode20"])."'
            ,[PAYEScheme] ='".ECh($_REQUEST["PAYEScheme"])."'
            WHERE [Status] <> 'D'";
            ScriptRunnerUD ($Script, "Authorize");
             */

            AuditLog("UPDATE", "Update of wage item setting [" . ScriptRunner($Script_CCode, "GName") . "].");

            echo ("<script type='text/javascript'>{parent.msgbox('Selected  wage item setting(s) updated successfully.','green');}</script>");
        }
    }

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("PAYROLL SETTING" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        } // Check User Access Right

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_CCode = "SELECT [GName] from [Fin_PRSettings] where HashKey='" . $EditID . "'";

                $Script = "UPDATE [Fin_PRSettings]
				SET [AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
				  ,[AuthDate] = GetDate()
				  ,[Status] = 'A'
				WHERE [HashKey] = '" . $EditID . "'";
                ScriptRunnerUD($Script, "Authorize");

                AuditLog("AUTHORIZE", "Wage item setting authorized for [" . ScriptRunner($Script_CCode, "GName") . "].", $_REQUEST["DelBox" . $i]);

                echo ("<script type='text/javascript'>{parent.msgbox('Wage item setting authorized successfully.','green');}</script>");
            }
        }
    }

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("PAYROLL SETTING" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        } // Check User Access Right

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_CCode = "SELECT [GName] from [Fin_PRSettings] where HashKey='" . $EditID . "'";

                $Script = "UPDATE [Fin_PRSettings]
				SET [AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
				  ,[AuthDate] = GetDate()
				  ,[Status] = 'U'
				WHERE [HashKey] = '" . $EditID . "'";
                ScriptRunnerUD($Script, "Authorize");
                AuditLog("UNAUTHORIZE", "Unauthorization of system reports [" . ScriptRunner($Script_CCode, "GName") . "].", $_REQUEST["DelBox" . $i]);

                echo ("<script type='text/javascript'>{parent.msgbox('Selected system report(s) unauthorized successfully.','green');}</script>");
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("PAYROLL SETTING" . "D") != true) {
            include '../main/NoAccess.php';
            exit;
        } // Check User Access Right

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {

            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = $_REQUEST["DelBox" . $i];

                $Script = "UPDATE [Fin_PRSettings]
				SET [DeletedBy]='" . $_SESSION["StkTck" . "UName"] . "'
				  ,[DeletedDate] = GetDate()
				  ,[Status] = 'D'
				WHERE [HashKey] = '" . $EditID . "'";
                ScriptRunnerUD($Script, "Authorize");
                AuditLog("DELETE", "Deletion of system report [" . ScriptRunner($Script_CCode, "GName") . "].", $_REQUEST["DelBox" . $i]);

                echo ("<script type='text/javascript'>{parent.msgbox('Selected system report(s) deleted successfully.','green');}</script>");
            }
        }
    }
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;">
    <table width="100%" border="0" align="center" class="table table-responsive">
        <form action="#" method="post" name="Records" target="_self" id="Records">
            <tr class="tdMenu_HeadBlock">
                <td height="11" width="27%" align="left" class="subHeader pb-0"><a href="Pay_Setup.php"><img src="../images/bi_butn/prev.png"></a></td>
                <td width="28%" align="right" class="subHeader pb-0">Payroll Items Settings</td>
                <td height="11" align="right" class="subHeader pb-0">
                    <table width="100" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td height="20"><select name="AcctNo" class="TextBoxText" id="AcctNo">
                                    <?php
                                    echo '<option value="--" selected="selected">--</option>';
                                    if (isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32) {
                                        $SelID = $_REQUEST["AcctNo"];
                                    }

                                    $dbOpen2 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");
                                    include '../login/dbOpen2.php';
                                    //while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($SelID == $row2['HashKey']) {
                                            echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['GName'] . '</option>';
                                            $EditID = $row2['HashKey'];
                                            $GName = $row2['GName'];
                                            $RecStatus = $row2['Status'];
                                        } else {
                                            echo '<option value="' . $row2['HashKey'] . '">' . $row2['GName'] . '</option>';
                                        }
                                    }
                                    include '../login/dbClose2.php';
                                    ?>
                                </select></td>
                            <td><?php echo '<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Open" />';
                                if (isset($GName)) {
                                } else {
                                    exit;
                                }
                                ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="643" colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="tdMenu_HeadBlock_Light">
                            <td width="20%" height="31" align="right" class="subHeader">Group: </td>
                            <td width="80%" align="left" class="subHeader">&nbsp;
                                <?php if (isset($GName)) {
                                    echo $GName;
                                } ?></td>
                        </tr>
                    </table>




                </td>
                <td width="657" height="12" align="right" class="TinyText">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="tdMenu_HeadBlock_Light">
                            <td width="64%" height="31" align="right" class="subHeader">PAYE Setting: </td>
                            <td width="36%" align="left" class="subHeader"><select name="PAYEScheme" class="TextBoxText" id="PAYEScheme">
                                    <?php
                                    echo '<option value="--" selected="selected">--</option>';
                                    $Script = ("SELECT * from Fin_PRSettings where HashKey='" . $EditID . "'");
                                    $SelID = ScriptRunner($Script, "PAYEScheme");

                                    $dbOpen2 = ("SELECT PAYEName, HashKey from FinTax where Status in ('A') order by PAYEName Asc");
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($SelID == $row2['HashKey']) {
                                            echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['PAYEName'] . '</option>';
                                        } else {
                                            echo '<option value="' . $row2['HashKey'] . '">' . $row2['PAYEName'] . '</option>';
                                        }
                                    }
                                    include '../login/dbClose2.php';
                                    ?>
                                </select></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" height="100%" class="ViewPatch">
                    <table width="100%" align="center" id="gradient-style">
                        <thead>
                            <tr>
                                <th width="19" valign="middle" scope="col" data-sorter="false">
                                    <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                    <label for="DelChk_All"></label>
                                </th>
                                <th width="234" align="center" valign="middle" scope="col" data-sorter="false">Payroll Item Name</th>
                                <th width="196" align="center" valign="middle" scope="col" data-sorter="false">% of Gross</th>
                                <th width="92" align="center" valign="middle" scope="col" data-sorter="false">CR/DR/FP/RL</th>
                                <th width="288" align="center" valign="middle" scope="col" data-sorter="false">Account Code</th>
                                <th width="288" align="center" valign="middle" scope="col" data-sorter="false">Show on Payslip</th>
                                <th width="113" align="center" valign="middle" scope="col" data-sorter="false">Taxable</th>
                                <th width="99" align="center" valign="middle" scope="col" data-sorter="false">Pensionable</th>
                                <th width="113" align="center" valign="middle" scope="col" data-sorter="false">Active Status</th>
                                <th width="113" align="center" valign="middle" scope="col" data-sorter="false">Annual</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $Del = 0;
                            if (strlen($EditID) == 32) {
                                $dbOpen2 = ("SELECT * from Fin_PRSettings where HashKey='" . $EditID . "'");
                            } else {

                                exit;
                            }
                            include '../login/dbOpen2.php';
                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                $Del = $Del + 1;
                                if ($row2['Status'] == "A") {
                                    //$MenuStatus="disabled";
                                    $MenuStatus = "";
                                } else {
                                    $MenuStatus = "";
                                }

                                for ($i = 1; $i <= 20; $i++) { ?>
                                    <td width="19" height="30" valign="middle" scope="col" class="TinyTextTight">
                                        <input name="<?php echo ("DelBox" . $i); ?>" type="checkbox" id="<?php echo ("DelBox" . $i); ?>" value="<?php echo ($row2['HashKey']); ?>" />
                                        <label for="<?php echo ("DelBox" . $i); ?>"></label>
                                    </td>

                                    <td width="234" align="center" valign="middle" scope="col"><?php echo '<input name="PayItemNm' . $i . '" id="PayItemNm' . $i . '" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText" value=" ' . trim($row2['PayItemNm' . $i]) . ' "';
                                                                                                if ($i <= 3) {
                                                                                                    echo ' readonly';
                                                                                                }
                                                                                                if ($i == 5) {
                                                                                                    echo ' readonly';
                                                                                                }
                                                                                                echo '>'; ?></td>

                                    <td width="196" align="center" valign="middle" scope="col"><?php
                                                                                                /*
        if ($i ==4 || $i==5)
        {
        if (trim($row2['PayItem'.$i])==0)
        {$PayItem=0;} else {$PayItem=$row2['PayItem'.$i];}
        echo '<input name="PayItem'.$i.'" id="PayItem'.$i.'" '.$MenuStatus.' readonly maxlength="2" size="5" class="TextBoxText_Short" value="'.$PayItem.'">';
        }
        else
        {
         */
                                                                                                if (trim($row2['PayItem' . $i]) == 0) {
                                                                                                    $PayItem = 0;
                                                                                                } else {
                                                                                                    $PayItem = $row2['PayItem' . $i];
                                                                                                }
                                                                                                echo '<input name="PayItem' . $i . '" id="PayItem' . $i . '" ' . $MenuStatus . '  size="5" class="TextBoxText_Short" value="' . $PayItem . '">';
                                                                                                //}
                                                                                                ?></td>
                                    <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>" id="<?php echo ("PayItemCD" . $i); ?>">
                                            <?php
                                            if ($i <= 4) {
                                                echo '<option selected=selected value="CR">CR</option>';
                                            } elseif ($i == 5) {
                                                echo '<option selected=selected value="CR">CR</option>';
                                            } elseif ($i == 6) {
                                                echo '<option selected=selected value="CR">CR</option>';
                                            } elseif ($i == 7) {
                                                echo '<option selected=selected value="DR">DR</option>';
                                            } elseif ($i == 19) {
                                                echo '<option selected=selected value="FP">FP</option>';
                                            } elseif ($i == 20) {
                                                echo '<option selected=selected value="FP">FP</option>';
                                            } else {
                                                if (trim($row2['PayItemCD' . $i]) == 'CR') {
                                                    echo '<option selected=selected value="CR">CR</option>';
                                                    echo '<option value="DR">DR</option>';
                                                    echo '<option value="RL">RL</option>';
                                                } elseif (trim($row2['PayItemCD' . $i]) == 'DR') {
                                                    echo '<option value="CR">CR</option>';
                                                    echo '<option selected=selected value="DR">DR</option>';
                                                    echo '<option value="RL">RL</option>';
                                                } elseif (trim($row2['PayItemCD' . $i]) == 'RL') {
                                                    echo '<option value="CR">CR</option>';
                                                    echo '<option value="DR">DR</option>';
                                                    echo '<option selected=selected value="RL">RL</option>';
                                                } else {
                                                    echo '<option value="CR">CR</option>';
                                                    echo '<option value="DR">DR</option>';
                                                    echo '<option value="RL">RL</option>';
                                                }
                                            } ?>
                                        </select></td>
                                    <td width="288" align="center" valign="middle" scope="col">
                                        <?php echo '<input name="PayItemCode' . $i . '" id="PayItemCode' . $i . '" ' . $MenuStatus . ' maxlength="24" size="24" class="TextBoxText_Medium" value="' . (trim($row2['PayItemCode' . $i])) . '">'; ?></td>
                                    <td width="288" align="center" valign="middle" scope="col">
                                        <?php // Show on Send/Print Payslip 
                                        ?>
                                        <input name="<?php echo ("ShowPaySlip" . $i); ?>" type="checkbox" id="<?php echo ("ShowPaySlip" . $i); ?>" <?php if (($row2['ShowPaySlip' . $i]) == 1) {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?> />
                                        <label for="<?php echo ("ShowPaySlip" . $i); ?>"></label>

                                    </td>
                                    <td width="113" align="center" valign="middle" scope="col"><span class="TinyTextTight">
                                            <input name="<?php echo ("PayItemTX" . $i); ?>" type="checkbox" id="<?php echo ("PayItemTX" . $i); ?>" <?php if (($row2['PayItemTX' . $i]) == 1) {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?> />
                                            <label for="<?php echo ("PayItemTX" . $i); ?>"></label>
                                        </span></td>
                                    <td width="99" align="center" valign="middle" scope="col"><span class="TinyTextTight">
                                            <input name="<?php echo ("PayItemPEN" . $i); ?>" type="checkbox" id="<?php echo ("PayItemPEN" . $i); ?>" <?php if (($row2['PayItemPen' . $i]) == 1) {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?> />
                                            <label for="<?php echo ("PayItemPEN" . $i); ?>"></label>
                                        </span></td>
                                    <!-- Active Status -->
                                    <td align="center" valign="middle" scope="col"><span class="TinyTextTight">
                                            <input name="<?php echo ("PayItemOF" . $i); ?>" type="checkbox" id="<?php echo ("PayItemOF" . $i); ?>" <?php if (($row2['PayItemOF' . $i]) == 1) {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?> />
                                            <label for="<?php echo ("PayItemOF" . $i); ?>"></label>
                                        </span></td>

                                    <!-- Annual Status -->
                                    <td align="center" valign="middle" scope="col">
                                        <!-- <span class="TinyTextTight"> -->
                                        <!-- <div class="form-group row"> -->

                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon" style="padding-right:0px; margin-right:0px">
                                                <input name="<?php echo ("PayItemAn" . $i); ?>" type="checkbox" id="<?php echo ("PayItemAn" . $i); ?>" <?php if (($row2['PayItemAn' . $i]) == 1) {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?> />
                                                <label for="<?php echo ("PayItemAn" . $i); ?>"></label>
                                            </span>

                                            <select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemAnType" . $i); ?>" style="padding-left:0px">
                                                <?php
                                                // $scriptfirstPeriod = "SELECT * FROM FinTax"
                                                // $scriptfirstPeriod = ScriptRunner($scriptfirstPeriod, "firstPeriod");

                                                if (trim($row2['PayItemAnType' . $i]) == 'ANN') {
                                                    echo '<option selected=selected value="ANN">Anniversary</option>';
                                                    echo '<option value="ANL">Annual Leave</option>';
                                                    echo '<option value="JAN">January</option>';
                                                    echo '<option value="DEC">December</option>';
                                                } elseif (trim($row2['PayItemAnType' . $i]) == 'ANL') {
                                                    echo '<option selected=selected value="ANL">Annual Leave</option>';
                                                    echo '<option value="ANN">Anniversary</option>';
                                                    echo '<option value="JAN">January</option>';
                                                    echo '<option value="DEC">December</option>';
                                                } elseif (trim($row2['PayItemAnType' . $i]) == 'JAN') {
                                                    echo '<option selected=selected value="JAN">January</option>';
                                                    echo '<option value="ANN">Anniversary</option>';
                                                    echo '<option value="ANL">Annual Leave</option>';
                                                    echo '<option value="DEC">December</option>';
                                                } elseif (trim($row2['PayItemAnType' . $i]) == 'DEC') {
                                                    echo '<option selected=selected value="DEC">December</option>';
                                                    echo '<option value="ANN">Anniversary</option>';
                                                    echo '<option value="ANL">Annual Leave</option>';
                                                    echo '<option value="JAN">January</option>';
                                                } else {
                                                    echo '<option value="ANN">Anniversary</option>';
                                                    echo '<option value="ANL">Annual Leave</option>';
                                                    echo '<option value="JAN">January</option>';
                                                    echo '<option value="DEC">December</option>';
                                                }

                                                ?>
                                            </select>



                                        </div>
                                        <!-- </div> -->


                                        <!-- </span> -->
                                    </td>


            </tr>
    <?php
                                    $PensionEmployeeCode_ = $row2['PensionEmployeeCode'];
                                    $PensionEmployerCode_ = $row2['PensionEmployerCode'];
                                    $LeaveAllowanceCode_ = $row2['LeaveAllowanceCode'];
                                    $NetPayCode_ = $row2['NetPayCode'];
                                    $PAYECode_ = $row2['PAYECode'];
                                }
                            }
                            include '../login/dbClose2.php'; ?>

    <tr>
        <th align="center" height="30" colspan="9" valign="middle" scope="col">Static Wage Item Components</th>
    </tr>

    <tr>
        <td height="25" width="19" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTight">
                <input name="<?php echo ("DelBox31"); ?>" type="checkbox" id="<?php echo ("DelBox31"); ?>" value="<?php echo ($row2['HashKey']); ?>" />
                <label for="<?php echo ("DelBox31"); ?>"></label>
            </span></td>
        <td width="234" align="center" valign="middle" scope="col">Employee Pension</td>
        <td width="196" align="center" valign="middle" scope="col"></td>
        <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>" id="<?php echo ("PayItemCD" . $i); ?>">
                <?php
                echo '<option selected=selected value="CR">CR</option>'; ?>
            </select></td>
        <td width="288" align="center" valign="middle" scope="col">
            <?php echo '<input name="PensionEmployeeCode" id="PensionEmployeeCode" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText_Medium" value="' . (trim($PensionEmployeeCode_)) . '">'; ?>
        </td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
    </tr>

    <tr>
        <td height="25" width="19" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTight">
                <input name="<?php echo ("DelBox31"); ?>2" type="checkbox" id="<?php echo ("DelBox31"); ?>2" value="<?php echo ($row2['HashKey']); ?>" />
                <label for="<?php echo ("DelBox31"); ?>2"></label>
            </span></td>
        <td width="234" align="center" valign="middle" scope="col">Employer Pension</td>
        <td width="196" align="center" valign="middle" scope="col"></td>
        <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>2" id="<?php echo ("PayItemCD" . $i); ?>2">
                <?php
                echo '<option selected=selected value="CR">CR</option>'; ?>
            </select></td>
        <td width="288" align="center" valign="middle" scope="col">
            <?php echo '<input name="PensionEmployerCode" id="PensionEmployerCode" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText_Medium" value="' . (trim($PensionEmployerCode_)) . '">'; ?>
        </td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
    </tr>

    <tr>
        <td height="25" width="19" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTight">
                <input name="<?php echo ("DelBox31"); ?>3" type="checkbox" id="<?php echo ("DelBox31"); ?>3" value="<?php echo ($row2['HashKey']); ?>" />
                <label for="<?php echo ("DelBox31"); ?>3"></label>
            </span></td>
        <td width="234" align="center" valign="middle" scope="col">Leave Allowance</td>
        <td width="196" align="center" valign="middle" scope="col"></td>
        <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>3" id="<?php echo ("PayItemCD" . $i); ?>3">
                <?php
                echo '<option selected=selected value="CR">CR</option>'; ?>
            </select></td>
        <td width="288" align="center" valign="middle" scope="col">
            <?php echo '<input name="LeaveAllowanceCode" id="LeaveAllowanceCode" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText_Medium" value="' . (trim($LeaveAllowanceCode_)) . '">'; ?>
        </td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
    </tr>

    <tr>
        <td height="25" width="19" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTight">
                <input name="<?php echo ("DelBox31"); ?>4" type="checkbox" id="<?php echo ("DelBox31"); ?>4" value="<?php echo ($row2['HashKey']); ?>" />
                <label for="<?php echo ("DelBox31"); ?>4"></label>
            </span></td>
        <td width="234" align="center" valign="middle" scope="col">PAYE</td>
        <td width="196" align="center" valign="middle" scope="col"></td>
        <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>4" id="<?php echo ("PayItemCD" . $i); ?>4">
                <?php
                echo '<option selected=selected value="CR">CR</option>'; ?>
            </select></td>
        <td width="288" align="center" valign="middle" scope="col">
            <?php echo '<input name="PAYECode" id="PAYECode" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText_Medium" value="' . (trim($PAYECode_)) . '">'; ?>
        </td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
    </tr>

    <tr>
        <td height="25" width="19" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTight">
                <input name="<?php echo ("DelBox31"); ?>5" type="checkbox" id="<?php echo ("DelBox31"); ?>5" value="<?php echo ($row2['HashKey']); ?>" />
                <label for="<?php echo ("DelBox31"); ?>5"></label>
            </span></td>
        <td width="234" align="center" valign="middle" scope="col">NetPay</td>
        <td width="196" align="center" valign="middle" scope="col"></td>
        <td width="92" align="center" valign="middle" scope="col"><select class="TextBoxText_Short_Comp" name="<?php echo ("PayItemCD" . $i); ?>5" id="<?php echo ("PayItemCD" . $i); ?>5">
                <?php
                echo '<option selected=selected value="CR">CR</option>'; ?>
            </select></td>
        <td width="288" align="center" valign="middle" scope="col">
            <?php echo '<input name="NetPayCode" id="NetPayCode" ' . $MenuStatus . ' maxlength="24" size="30" class="TextBoxText_Medium" value="' . (trim($NetPayCode_)) . '">'; ?>
        </td>
        <td width="113" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="99" align="center" valign="middle" scope="col"></td>
        <td width="113" align="center" valign="middle" scope="col"></td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    <tr class="tdMenu_HeadBlock_Light">
        <td colspan="2" align="left"><?php //include '../main/pagination.php';
                                        ?></td>
        <td align="right"><?php
                            //<input name="PgTy" id="PgTy" type="hidden" value="'.$_REQUEST["PgTy"].'" />
                            echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
		<input name="FAction" id="FAction" type="hidden" />
	  	<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                            echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';

                            //Check if any record was spolled at all before enabling buttons
                            //Check if user has Delete rights
                            //$but_HasRth=("PAYROLL **  SETTING"."D"); $but_Type = 'Delete'; include '../main/buttons.php';

                            //Check if user has Add/Edit rights

                            if (ValidateURths("PAYROLL SETTING" . "A") == true) {

                                if ($RecStatus == 'N' || $RecStatus == 'U') {
                                    $but_HasRth = ("PAYROLL SETTING" . "A");
                                    $but_Type = 'Update';
                                    include '../main/buttons.php';
                                } else {
                                    //$but_HasRth=("PAYROLL ** SETTING"."A"); $but_Type = 'Update'; include '../main/buttons.php';
                                    $but_HasRth = ("PAYROLL SETTING" . "A");
                                    $but_Type = 'Update';
                                    include '../main/buttons.php';
                                }
                            }

                            //Check if user has Authorization rights
                            //$but_HasRth=("PAYROLL ** SETTING"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
                            //Set FAction Hidden Value for each button
                            ?></td>
    </tr>
    </form>
    <tr>
        <td colspan="2"></td>
        </table>
</body>
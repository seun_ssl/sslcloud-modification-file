<?php session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

if (ValidateURths("OVERTIME SETTING" . "V") != true) {include '../main/NoAccess.php';exit;}

$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($Err)) {$Err = "";}
if (!isset($ModMaster)) {$ModMaster = "";}
if (!isset($ext)) {$ext = "";}
if (!isset($SelID)) {$SelID = "";}
if (!isset($RetVal)) {$RetVal = "";} //Document Management return value for attached error screen shot.

$dbOpen2 = "";
$OpenID = "";

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

//echo "hrer";
    // if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open")
    // {
    //     if (ValidateURths("OVERTIME SETTING"."V")!=true){include '../main/NoAccess.php';exit;}

    //     $OpenID = ECh($_REQUEST["GrpName"]);
    //     $Script="Select * from Fin_AttendSet where EID='".ECh(intval($OpenID))."'";

    //     $EditID = ScriptRunner($Script,"HashKey");

    //     if (strlen($EditID)!=32 && ($_REQUEST["GrpName"]>=1  && $_REQUEST["GrpName"]<=5))
    //     {
    //         /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
    //         $UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."Fin_AttendSet";
    //         $HashKey = md5($UniqueKey);

    //         $Script="Insert into Fin_AttendSet (EID,PCent, CloseTime, TimeIn, ActiveTime,OverTime,[Status],[AddedBy],[AddedDate],[HashKey]) VALUES (".intval($_REQUEST["GrpName"]).",0.0,'','','',0.0,'N','".$_SESSION["StkTck"."HKey"]."',GetDate(),'".$HashKey."')";
    //         ScriptRunnerUD($Script,"Inst");

    //         $EditID = $HashKey;
    //     }
    // }

    //Create here
    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create") {
        if (ValidateURths("OVERTIME SETTING" . "A") != true) {include '../main/NoAccess.php';exit;}

        // var_dump($_POST);
        // die();

        /* Set HashKey of Account to be Updated */
        //$EditID = ECh($_REQUEST["UpdID"]);
        //$HashKey = ECh($_REQUEST["UpdID"]);

        $GoValidate = true;
        $OvPCent = 0;
        $OvTime = 0;
        $UseType = "None";

        if (isset($_REQUEST["ToUse"]) && $_REQUEST["ToUse"] == "Value") {
            $UseType = "Value";
            if (!is_numeric(str_replace(",", "", $_REQUEST["OverTime"]))) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Enter a numeric amount for this groups overtime.', 'red'); }</script>");
                $GoValidate = false;
            } else { $OvTime = floatval(ECh(str_replace(",", "", $_REQUEST["OverTime"])));}
        }

        if (isset($_REQUEST["ToUse"]) && $_REQUEST["ToUse"] == "Percent") {
            $UseType = "Percent";
            if (!is_numeric(str_replace(",", "", $_REQUEST["PCent"]))) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Enter a numeric percentile of basic salary for this groups overtime.', 'red'); }</script>");
                $GoValidate = false;
            } else { $OvPCent = floatval(ECh(str_replace(",", "", $_REQUEST["PCent"])));}
        }

        $Script = "SELECT count(HashKey) Ct from [Fin_AttendSet] where c_type='" . $_REQUEST["c_type"] . "'
		and
        ActiveTime ='" . $_REQUEST["ActiveTime"] . "'
        and
        EID='" . intval($_REQUEST["GrpName"]) . "'
        and
         Status<>'D'";

        if (ScriptRunner($Script, "Ct") > 0) {

            echo ("<script type='text/javascript'>{ parent.msgbox('Record already existing you can only Update', 'red'); }</script>");
            $GoValidate = false;
            //exit('stop');
        }

         if (isset( $UseType) &&  $UseType === "None") {
            echo ("<script type='text/javascript'>{ parent.msgbox('Kindly Check if Value/percent type will be use to calculate this component.', 'red'); }</script>");
            $GoValidate = false;
      }

        if ($GoValidate == true) {
            $OpenID = ECh($_REQUEST["GrpName"]);
            $Script = "Select * from Fin_AttendSet where EID='" . ECh(intval($OpenID)) . "' and PayCalcType='" . ECh($_REQUEST["PayCalcType"]) . "'";
            //echo $Script;
            //exit;
            $EditID = ScriptRunner($Script, "HashKey");

            //if (strlen($EditID)!=32 && ($_REQUEST["GrpName"]>=1  && $_REQUEST["GrpName"]<=5))
            //if ($_REQUEST["GrpName"]>=1  && $_REQUEST["GrpName"]<=5)
            //{
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Fin_AttendSet";
            $HashKey = md5($UniqueKey);
            $Script = "Insert into Fin_AttendSet (EID,CloseTime, TimeIn, ActiveTime,  PCent,  [UseType], OverTime,[GpDesc],[PayCalcType],
				[Status],[AddedBy],[c_type],[AddedDate],[HashKey]) VALUES
                (" . intval($_REQUEST["GrpName"]) . ",
				'" . $_REQUEST["CloseTime"] . "'
                ,'" . $_REQUEST["TimeIn"] . "',
                '" . $_REQUEST["ActiveTime"] . "',
               " . $OvPCent . ",
               '" . $UseType . "',
                " . $OvTime . ",
				'" . ECh($_REQUEST["GpDesc"]) . "',
				'" . ECh($_REQUEST["PayCalcType"]) . "',
                'N',
                '" . $_SESSION["StkTck" . "HKey"] . "',
                '" . ECh($_REQUEST["c_type"]) . "',
                       GetDate(),
                '" . $HashKey . "')";
            ScriptRunnerUD($Script, "Inst");
            //}

/*
$Script_ID="select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='".$HashKey."')";

$Script = "Update [Fin_AttendSet] set CloseTime='".ECh($_REQUEST["CloseTime"])."',  TimeIn='".ECh($_REQUEST["TimeIn"])."', ActiveTime='".ECh($_REQUEST["ActiveTime"])."', [PCent]=".$OvPCent.",[OverTime]=".$OvTime.",[UseType]='".$UseType."',[PayCalcType]='".ECh($_REQUEST["PayCalcType"])."',[GpDesc]='".ECh($_REQUEST["GpDesc"])."',[Status]='U',[UpdatedBy]='".$_SESSION["StkTck"."HKey"]."',[UpdatedDate]=GetDate() where [HashKey]= '".$HashKey."'";
//echo $Script;
ScriptRunnerUD($Script,"Inst");
 */

            echo ("<script type='text/javascript'>{ parent.msgbox('Attendance setting was successfully Ceated.', 'green'); }</script>");
            AuditLog("UPDATE", "Payment setting created successfully");
            $EditID = "";
        }
    }

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Setting") {
        if (ValidateURths("OVERTIME SETTING" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set HashKey of Account to be Updated */
        $EditID = ECh($_REQUEST["UpdID"]);
        $HashKey = ECh($_REQUEST["UpdID"]);

        

        $GoValidate = true;
        $OvPCent = 0;
        $OvTime = 0;
        $UseType = "None";

        if (isset($_REQUEST["ToUse"]) && $_REQUEST["ToUse"] == "Value") {
            $UseType = "Value";
            if (!is_numeric(str_replace(",", "", $_REQUEST["OverTime"]))) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Enter a numeric amount for this groups overtime.', 'red'); }</script>");
                $GoValidate = false;
            } else { $OvTime = floatval(ECh(str_replace(",", "", $_REQUEST["OverTime"])));}
        }

        if (isset($_REQUEST["ToUse"]) && $_REQUEST["ToUse"] == "Percent") {
            $UseType = "Percent";
            if (!is_numeric(str_replace(",", "", $_REQUEST["PCent"]))) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Enter a numeric percentile of basic salary for this groups overtime.', 'red'); }</script>");
                $GoValidate = false;
            } else { $OvPCent = floatval(ECh(str_replace(",", "", $_REQUEST["PCent"])));}
        }

         if (isset( $UseType) &&  $UseType === "None") {
            echo ("<script type='text/javascript'>{ parent.msgbox('Kindly Check if Value/percent type will be use to calculate this component.', 'red'); }</script>");
            $GoValidate = false;
      }




        if ($GoValidate == true) {
            $OpenID = ECh($_REQUEST["GrpName"]);
            $Script = "Select * from Fin_AttendSet where EID='" . ECh(intval($OpenID)) . "' and PayCalcType='" . ECh($_REQUEST["PayCalcType"]) . "'";
            //echo $Script;
            //exit;
            $EditID = ScriptRunner($Script, "HashKey");

            if (strlen($EditID) != 32 && ($_REQUEST["GrpName"] >= 1 && $_REQUEST["GrpName"] <= 5)) {
                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Fin_AttendSet";
                $HashKey = md5($UniqueKey);

                $Script = "Insert into Fin_AttendSet (EID,CloseTime, TimeIn, ActiveTime,  PCent,OverTime,[GpDesc],
				[PayCalcType],[Status],[AddedBy],[c_type],[AddedDate],[HashKey])
				VALUES (" . intval($_REQUEST["GrpName"]) . ",'" . $_REQUEST["CloseTime"] . "'
				,'" . $_REQUEST["TimeIn"] . "',
				'" . $_REQUEST["ActiveTime"] . "',
				0.0,
				" . $OvTime . ",
				'" . ECh($_REQUEST["GpDesc"]) . "',
				'" . ECh($_REQUEST["PayCalcType"]) . "',
				'N',
				'" . $_SESSION["StkTck" . "HKey"] . "',
				'" . ECh($_REQUEST["c_type"]) . "',
				GetDate(),
				'" . $HashKey . "')";
                ScriptRunnerUD($Script, "Inst");
            }

            $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $HashKey . "')";

            $Script = "Update [Fin_AttendSet] set CloseTime='" . ECh($_REQUEST["CloseTime"]) . "',
			TimeIn='" . ECh($_REQUEST["TimeIn"]) . "',
            ActiveTime='" . ECh($_REQUEST["ActiveTime"]) . "',
			[PCent]=" . $OvPCent . ",
            [OverTime]=" . $OvTime . ",
            c_type='" . ECh($_REQUEST["c_type"]) . "',
            [UseType]='" . $UseType . "',
			[PayCalcType]='" . ECh($_REQUEST["PayCalcType"]) . "',[GpDesc]='" . ECh($_REQUEST["GpDesc"]) . "',
			[Status]='U',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $HashKey . "'";
            //echo $Script;
            ScriptRunnerUD($Script, "Inst");

            echo ("<script type='text/javascript'>{ parent.msgbox('Attendance setting updated successfully.', 'green'); }</script>");
            AuditLog("UPDATE", "Overtime payment setting [" . ECh(ScriptRunner($Script_ID, "SetValue2")) . "] updated successfully");
            $EditID = "";
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("OVERTIME SETTING" . "V") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_Edit = "Select * from Fin_AttendSet where HashKey='" . $EditID . "'";
                $Script = "Select * from Fin_AttendSet where HashKey='" . $EditID . "'";
                $OpenID = ScriptRunner($Script, "EID");

            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("OVERTIME SETTING" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $HashKey . "')";

                $Script = "UPDATE [Fin_AttendSet]
				SET [Status] = 'A'
				,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
				,[AuthDate]=GetDate()
				WHERE [HashKey] = '" . $EditID . "'";
                ScriptRunnerUD($Script, "Authorize");

                echo ("<script type='text/javascript'>{ parent.msgbox('Attendance setting authorized successfully.', 'green'); }</script>");
                AuditLog("AUTHORIZE", "Overtime payment setting [" . ECh(ScriptRunner($Script_ID, "SetValue2")) . "] authorized successfully", $EditID);
                $EditID = "";
            }
        }
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected") {
        if (ValidateURths("OVERTIME SETTING" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $EditID . "')";

                $Script = "UPDATE [Fin_AttendSet]
				SET [Status] = 'U'
				,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
				,[AuthDate]=GetDate()
				WHERE [HashKey] = '" . $EditID . "'";
                ScriptRunnerUD($Script, "Authorize");
                AuditLog("UNAUTHORIZE", "Overtime payment setting [" . ECh(ScriptRunner($Script_ID, "SetValue2")) . "] unauthorized successfully", $EditID);

                echo ("<script type='text/javascript'>{ parent.msgbox('Attendance setting unauthorized successfully.', 'green'); }</script>");
                $EditID = "";
            }
        }
        $Script_Edit = "Select * from UGpRights where HashKey='z'";
        $EditID = "--";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("OVERTIME SETTING" . "D") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [EmpItm] WHERE [HashKey] = '" . $EditID . "'") == true) {
                    $Script_ID = "SELECT * from Fin_AttendSet where HashKey='" . $EditID . "'";

                    $Script = "UPDATE [Fin_AttendSet]
				SET [Status] = 'D'
				,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
				,[AuthDate]=GetDate()
				WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("DELETE", "Item setting for overtime/lateness/weekend deleted successfully");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected setting(s) deleted successfully.', 'green'); }</script>");
                    $EditID = "";
                }
            }
        }
        $EditID = "";
    }
}
?>
<script>
  $(function()
  {
	$("#ItemStart").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+0D", dateFormat: 'dd M yy'})
	$("#ItemEnd").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+0D", dateFormat: 'dd M yy'})
	$("#ItemExpiry").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+20Y", dateFormat: 'dd M yy'})
  });

$(function() {
	$( document ).tooltip();
});

$(function() {
	$("#ClearDate").click(function() {
		document.getElementById("ExDt").value='';
	});
});
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong{
		font-weight: 550;
	}
</style>

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">

				<div class="col-md-12 text-center ">
						<h4>
						Employee Overtime Settings
						</h4>
					</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row ">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Name <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px" >
								<select class="form-control" name="PayCalcType" id="PayCalcType">
									<?php
/*
$SelID = ScriptRunner($Script_Edit,"PayCalcType");
if (trim($SelID)=="Overtime")
{
echo '<option selected value="Overtime">Overtime</option>
<option value="Lateness">Lateness</option>
<option value="Weekend">Weekend</option>';
}
elseif (trim($SelID)=="Lateness")
{
echo '<option value="Overtime">Overtime</option>
<option selected value="Lateness">Lateness</option>
<option value="Weekend ">Weekend</option>';
}
elseif (trim($SelID)=="Weekend")
{
echo '<option value="Overtime">Overtime</option>
<option value="Lateness">Lateness</option>
<option selected value="Weekend ">Weekend</option>';
}
else
{    echo '<option value="Overtime">Overtime</option>
<option value="Lateness">Lateness</option>
<option value="Weekend ">Weekend</option>';
}
 */
?>
									<!-- <option value="--" selected="selected">--</option> -->
									<?php
if ($EditID == "" || $EditID == "--") {
    if (isset($_REQUEST["KRA"])) {$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Attendance Settings' and Val1<>'" . $_REQUEST["KRA"] . "') ORDER BY Val1");} else { $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Attendance Settings' AND Status <> 'D') ORDER BY Val1");}
} else {
    $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Attendance Settings' AND Status <> 'D') ORDER BY Val1");
}
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
}
include '../login/dbClose2.php';
if ($EditID != "") {
    echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "PayCalcType") . '">' . ScriptRunner($Script_Edit, "PayCalcType") . '</option>';
} else {
    if (isset($_REQUEST["PayCalcType"])) {echo '<option selected=selected value="' . $_REQUEST["PayCalcType"] . '">' . $_REQUEST["PayCalcType"] . '</option>';}
}
?>
								</select>
								<span class="input-group-btn ">
									<?php
$ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
$imgID = "MastersDepartment";
if ($ModMaster == true) {
    $imgPath = "fa fa-plus";
    $Titl = "Add Attendance Settings eg. Overtime, Lateness, Weekend";
    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Attendance+Settings&LDb=Single&amp;Elmt=Attendance+Settings',320,400,'No')";
} else {
    $imgPath = "fa fa-info";
    $Titl = "Select Attendance Settings";
    $OnClk = "";
}
echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
?>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;" >Minutes Amount<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
								<span class="input-group-addon ">

									<input type="radio" name="ToUse" id="ToUse" value="Value"  <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "UseType") === 'Value') ? 'checked' : '' ?>>
									<label for="ToUse" style="padding-left: 20px;height: 13px;"></label>
								</span>
								<?php
if ($EditID != "" && $EditID != '--') {echo '<input type="text" name="OverTime" maxlength="8" class="form-control" id="OverTime" value="' . number_format((ScriptRunner($Script_Edit, "OverTime") + 0.0), 2, '.', ',') . '"/>';} elseif (isset($_REQUEST["OverTime"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<input type="text" name="OverTime" maxlength="8" class="form-control" id="OverTime" value="' . number_format(($_REQUEST["OverTime"] + 0.0), 2, '.', ',') . '"/>';} else {echo '<input type="text" name="OverTime" maxlength="8" class="form-control" id="OverTime" value=""/>';}
?>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" id="MastersDepartment" title="Select to use equal set minutes rate per employee on each level"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;" >% of Basic Salary<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
								<span class="input-group-addon ">
									<input type="radio" name="ToUse" id="ToUse2" value="Percent"  <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "UseType") === 'Percent') ? 'checked' : '' ?>>
									<label for="ToUse2" style="padding-left: 20px;height: 13px;"></label>
								</span>
								<?php
if ($EditID != "" && $EditID != '--') {echo '<input type="text" name="PCent" maxlength="4" class="form-control" id="PCent" value="' . number_format((ScriptRunner($Script_Edit, "PCent") + 0.0), 2, '.', ',') . '"/>';} elseif (isset($_REQUEST["PCent"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<input type="text" name="PCent" maxlength="4" class="form-control" id="PCent" value="' . number_format(($_REQUEST["PCent"] + 0.0), 2, '.', ',') . '"/>';} else {echo '<input type="text" name="PCent" maxlength="4" class="form-control" id="PCent" value=""/>';}
?>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" id="MastersDepartment4" title="Select to use a (%) of employee monthly basic pay as minutes rate"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;" >OverTime/Lateness Type<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
							<select class="form-control" name="c_type" class="TextBoxText" id="GrpName">
								<option value="workdays" <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "c_type") === 'workdays') ? 'selected' : '' ?> >Workdays</option>
								<option value="weekends" <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "c_type") === 'weekends') ? 'selected' : '' ?>  >Weekends</option>
								<option value="holidays" <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "c_type") === 'holidays') ? 'selected' : '' ?>   >Holidays</option>
								<option value="events" <?php echo ($EditID != "" && $EditID != '--' && ScriptRunner($Script_Edit, "c_type") === 'events') ? 'selected' : '' ?>      >Events</option>
							</select>
							</div>
						</div>



					</div>
					<div class="col-md-6" >
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label"> Details:</label>
							<div class="col-sm-8" style=" padding-right: 35px">
								<?php
if ($EditID != "" || $EditID > 0) {echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc">' . ScriptRunner($Script_Edit, "GpDesc") . '</textarea>';} elseif (isset($_REQUEST["GpDesc"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc">' . $_REQUEST["GpDesc"] . '</textarea>';} else {echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc"></textarea>';}
?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;" >Resumption-Time<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
								<?php
if ($EditID != "" && $EditID != '--') {
    //echo '<input type="text" name="CloseTime" maxlength="5" class="form-control" id="CloseTime" value="'.date('H:i',ScriptRunner($Script_Edit,"CloseTime")).'"/>';
    echo '<input type="text" name="TimeIn" maxlength="5" class="form-control" id="TimeIn" value="' . ScriptRunner($Script_Edit, "TimeIn") . '"/>';
} elseif (isset($_REQUEST["TimeIn"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<input type="text" name="TimeIn" maxlength="5" class="form-control" id="TimeIn" value="' . number_format(($_REQUEST["TimeIn"] + 0.0), 2, ':', ',') . '"/>';
} else {
    echo '<input type="text" name="TimeIn" maxlength="5" class="form-control" id="TimeIn" value="08:00"/>';
}
//check ActiveTine
$ActiveTime = ScriptRunner($Script_Edit, "ActiveTime");
if ($ActiveTime == "TimeIn") {
    $in_active_box = "checked='checked'";
    $out_active_box = "";
} elseif ($ActiveTime == "TimeOut") {
    $out_active_box = "checked='checked'";
    $in_active_box = "";
} else {
    $in_active_box = "checked='checked'";
    $out_active_box = "";
}
?>
								<span class="input-group-addon ">
									<input type="radio" name="ActiveTime" id="ActiveTime" value="TimeIn" <?php print $in_active_box;?> >
									<label for="ActiveTime" style="padding-left: 20px;height: 13px;"></label>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;" >Closure-Time<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
								<?php
if ($EditID != "" && $EditID != '--') {
    //echo '<input type="text" name="CloseTime" maxlength="5" class="form-control" id="CloseTime" value="'.date('H:i',ScriptRunner($Script_Edit,"CloseTime")).'"/>';
    echo '<input type="text" name="CloseTime" maxlength="5" class="form-control" id="CloseTime" value="' . ScriptRunner($Script_Edit, "CloseTime") . '"/>';
} elseif (isset($_REQUEST["CloseTime"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<input type="text" name="CloseTime" maxlength="5" class="form-control" id="CloseTime" value="' . number_format(($_REQUEST["CloseTime"] + 0.0), 2, ':', ',') . '"/>';
} else {
    echo '<input type="text" name="CloseTime" maxlength="5" class="form-control" id="CloseTime" value="17:00"/>';
}
?>
								<span class="input-group-addon">
									<input type="radio" name="ActiveTime" id="ActiveTime2" value="TimeOut" <?php print $out_active_box;?> >
									<label for="ActiveTime2" style="padding-left: 20px;height: 13px;"></label>
								</span>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" id="MastersDepartment4" title="For Overtime please enter the Time Out, for Lateness  time in GMT, e.g 17:00 is same as 5:00pm"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>


							<div class="form-group row ">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Level Name <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px" >
									<select class="form-control" name="GrpName" class="TextBoxText" id="GrpName">
						          	<?php
echo '<option value="0" selected="selected">--</option>';
if (isset($_REQUEST["GrpName"]) && $OpenID == "") {
    $OpenID = $_REQUEST["GrpName"];
}

$dbOpen2 = ("select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue2<>''");
include '../login/dbOpen2.php';

while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($OpenID == $row2['SetValue']) {
        if ($OpenID != '' && $OpenID != '--') {echo '<option selected value="' . $row2['SetValue'] . '">' . $row2['SetValue2'] . '</option>';}
    } else {echo '<option value="' . $row2['SetValue'] . '">' . $row2['SetValue2'] . '</option>';}
}
include '../login/dbClose2.php';
?>
								</select>

							</div>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6 ">
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-2">
								<div class="demo-checkbox">
									<input type="checkbox" checked name="AuthNotifier" id="AuthNotifier">
									<label for="AuthNotifier">
									<button type="button" class="btn btn-default btn-sm" Title="Check if you want a request for authorization sent" name="AuthInfo" id="AuthInfo" onClick="Popup.show('simplediv');return false;" style="line-height: 17px" ><i class="fa fa-info"></i></button></label>
								</div>
							</div>
							<div class="col-md-4 pull-right  ">
								<?php
if (ValidateURths("OVERTIME SETTING" . "A") == true) {

    $Script = "Select Status from Fin_AttendSet where HashKey='" . $EditID . "'";
    if (strlen($EditID) == 32 && ScriptRunner($Script, "Status") != 'A') {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update Setting" />';
        echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';} else {echo '<input name="SubmitTrans" type="submit"  class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create" />';}

}
?>
							</div>
						</div>
					</div>
				</div>
				<hr style="margin-bottom: 1.0rem ;margin-top: 0px">
				<div class="row">
					<div class="col-md-12 ">
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE"/>
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE"/>
						<?php //$Load_Date=false; $Load_Search=true; $Load_Auth=true; include '../main/report_opt.php'; ?>
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
//------------------------
if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    print "<h4>UNAUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),UpdatedDate,106) as Dt, Convert(varchar(5),CloseTime,114) CloseT, Convert(varchar(5),TimeIn,114) InTime, * from Fin_AttendSet where Status NOT IN ('A','D') order by EID asc");
} else {

    print "<h4>AUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),UpdatedDate,106) as Dt, Convert(varchar(5),CloseTime,114) CloseT,  Convert(varchar(5),TimeIn,114) InTime, * from Fin_AttendSet where Status IN ('A') order by EID asc");
}
//------------------------
?>
					</div>
					<div class="col-md-12">
						<table class="tablesorter table table-responsive" id="table">
							<thead>
								<tr>
									<th valign="middle" class="smallText" scope="col" data-sorter="false">
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
										<label for="DelChk_All"></label>
									</th>
									<th  align="center" valign="middle" scope="col">Name</th>
									<th  align="center" valign="middle" scope="col">Level Name</th>
									<th  align="center" valign="middle" scope="col">Component Type</th>
									<th  align="center" valign="middle" scope="col">Overtime/Lateness Type</th>
									<th align="center" valign="middle" scope="col">In-Use</th>
									<th  align="center" valign="middle" scope="col">Amount</th>
									<th  align="center" valign="middle" scope="col">(%) of Basic</th>
									<th align="center" valign="middle" scope="col"> Resumption-Time</th>
									<th align="center" valign="middle" scope="col"> Closure-Time</th>
									<th  align="center" valign="middle" scope="col" data-sorter="false">Updated By</th>
									<th  align="center" valign="middle" scope="col"data-sorter="false">Updated On</th>
									<th align="center" valign="middle" scope="col"data-sorter="false">Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
$Del = 0;
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    ?>
								<tr>
									<td width="25" height="23" valign="middle" scope="col" class="TinyTextTight"><?php echo '<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" />
										<label for="' . ("DelBox" . $Del) . '"></label>'; ?></td>
                                    <td  valign="middle" class="TinyText" scope="col"><?php echo (trim($row2['PayCalcType'])); ?></td>
									<td  valign="middle" class="TinyText" scope="col">
										<?php
$Script = "SELECT Setting, SetValue, SetValue2 FROM Settings where SetValue='" . $row2['EID'] . "' and Setting='JobRoleLevel'";
    echo ScriptRunner($Script, "SetValue2");
    ?>
									</td>
									<td  valign="middle" class="TinyText" scope="col"><?php echo trim($row2['ActiveTime']) === 'TimeIn' ? 'Lateness' : 'Overtime'  ; ?></td>
									<td  valign="middle" class="TinyText" scope="col"><?php echo trim($row2['c_type'])  ?></td>
									<td align="center"  valign="middle" class="TinyText" scope="col"><?php echo (trim($row2['UseType'])); ?></td>
									<td align="right" valign="middle" class="TinyText" >&nbsp;<?php echo (trim($row2['OverTime'])); ?></td>
									<td align="right" valign="middle" class="TinyText" scope="col" >&nbsp;<?php echo (trim($row2['PCent'])); ?></td>
									<td align="right" valign="middle" class="TinyText" >&nbsp; <?php echo $row2['InTime']; ?></td>
									<td align="right" valign="middle" class="TinyText" >&nbsp; <?php echo $row2['CloseT']; ?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" >&nbsp;
										<?php
$Script = "SELECT (SName+' '+FName+' ['+convert(varchar(24),EmpID)+']') Nm from EmpTbl where HashKey='" . $row2['UpdatedBy'] . "'";
    echo ScriptRunner($Script, "Nm");
    ?>
									</td>
									<td align="center" valign="middle" class="TinyText" scope="col" >&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" >
										<?php
if (trim($row2['Status']) == 'N' || trim($row2['Status']) == 'U') {echo ('<img src="../images/red.png" height="18" title="Pending Authorization">');} elseif (trim($row2['Status']) == 'A') {echo ('<img src="../images/amber.png" height="18" title="Active">');}
    ?>
									</td>
								</tr>
								<?php }
include '../login/dbClose2.php';?>
							</tbody>
      					</table>
      				</div>
      				<div class="col-md-6 pt-10">
 				    	<?php include '../main/pagination.php';?>
 				  	</div>
 				  	<div class="col-md-6 pt-10">
 				  		<div class="row">
 							<div class="col-md-3"></div>
 							<div class="col-md-9">
 								<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
	 								      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
	 									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

//Check if any record was spolled at all before enabling buttons
if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {

    if (ValidateURths("OVERTIME SETTING" . "D") == true) {
        //Check if user has Delete rights
        $but_HasRth = ("OVERTIME SETTING" . "D");
        $but_Type = 'Delete';include '../main/buttons.php';
    }

    if (ValidateURths("OVERTIME SETTING" . "V") == true) {
        //Check if user has Add/Edit rights
        $but_HasRth = ("OVERTIME SETTING" . "V");
        $but_Type = 'View';include '../main/buttons.php';
    }

    if (ValidateURths("OVERTIME SETTING" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("OVERTIME SETTING" . "T");
        $but_Type = 'Authorize';include '../main/buttons.php';
    }

} else {

    if (ValidateURths("OVERTIME SETTING" . "D") == true) {
        //Check if user has Delete rights
        $but_HasRth = ("OVERTIME SETTING" . "");
        $but_Type = 'Delete';include '../main/buttons.php';
    }

    if (ValidateURths("OVERTIME SETTING" . "V") == true) {
        //Check if user has Add/Edit rights
        $but_HasRth = ("OVERTIME SETTING" . "");
        $but_Type = 'View';include '../main/buttons.php';
    }

    if (ValidateURths("OVERTIME SETTING" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("OVERTIME SETTING" . "T");
        $but_Type = 'Unauthorize';include '../main/buttons.php';
    }
}
//Set FAction Hidden Value for each button
echo "<input name='FAction' id='FAction' type='hidden'>";
?>
 						  	</div>
 						</div>
 				  	</div>
 				</div>
 			</div>
 		</div>
 	</form>
</body>

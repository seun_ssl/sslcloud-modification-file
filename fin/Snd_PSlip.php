<?php session_start();
// var_dump('here');
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
// require_once '../dompdf/dompdf_config.inc.php';
require_once('../mpdf/vendor/autoload.php');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("RESEND PAYSLIP" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
$CmdTyp = "";
$QueryStrCR = $QueryStrDR = $QueryStr = "";

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Send Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT (Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm, Em.EmpID as EmpID, Em.HashKey as EmpHKey, Em.Email,Em.Sec_Email ,Fp.HashKey,(DATENAME(month,Fp.PayMonth)+' '+DATENAME(year,Fp.PayMonth)) as PMth from EmpTbl Em, Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";

                $EmpHKey = ScriptRunner($Script_ID, "EmpHKey");

                $Script_core = "Select * from Fin_PRCore where EmpID='$EmpHKey' and Status='A' order by AuthDate desc";
                $Scheme_PRSetting_HashKey = ScriptRunner($Script_core, "Scheme");

                //    AuditLog("INSERT","Employee payslip resent to ".ScriptRunner($Script_ID,"Nm")." for ".ScriptRunner($Script_ID,"PMth"));
                $dbOpen2 = " SELECT Et.HashKey AS HKey, Et.SName + ' ' + Et.ONames + ' ' + Et.FName AS [Full Name], Et.HashKey, Et.Status, Et.EmpStatus,Et.EmpID,Et.EmpCategory,EmpDt,
				Et.Email, Et.PENCommNo, Et.HInsurNo, Et.SalAcctNo, Et.SalBank, Et.Department, Et.SalSortCode, Et.TaxID, Et.PenCustID, Et.BranchID, FMc.PayItem1, FMc.PayItem2, FMc.Gross, (datename(M,FMc.PayMonth)+' '+datename(YYYY,FMc.PayMonth)) PMth,
				FMc.PayItem3, FMc.PayItem4, FMc.PayItem5, FMc.PayItem6, FMc.PayItem7, FMc.PayItem8, FMc.PayItem9, FMc.PayItem10, FMc.PayItem11, FMc.PayItem12, FMc.PayItem13, FMc.PayItem14,
				FMc.PayItem15, FMc.PayItem16, FMc.PayItem17, FMc.PayItem18, FMc.PayItem19, FMc.PayItem20, FMc.PayItem21, FMc.PayItem22, FMc.PayItem23, FMc.PayItem24,
				FMc.PayItem25, FMs.PayItemNm1, FMs.PayItemOF1, FMs.PayItemTX1, FMs.PayItemCD1, FMs.PayItemPen1, FMs.PayItemNm2, FMs.PayItemOF2, FMs.PayItemTX2,
				FMs.PayItemCD2, FMs.PayItemPen2, FMs.PayItemNm3, FMs.PayItemOF3, FMs.PayItemTX3, FMs.PayItemCD3, FMs.PayItemPen3, FMs.PayItemNm4,
				FMs.PayItemOF4, FMs.PayItemTX4, FMs.PayItemCD4, FMs.PayItemPen4, FMs.PayItemNm5, FMs.PayItemOF5, FMs.PayItemTX5, FMs.PayItemCD5,
				FMs.PayItemPen5, FMs.PayItemNm6, FMs.PayItemOF6, FMs.PayItemTX6, FMs.PayItemCD6, FMs.PayItemPen6, FMs.PayItemNm7, FMs.PayItemOF7,
				FMs.PayItemTX7, FMs.PayItemCD7, FMs.PayItemPen7, FMs.PayItemNm8, FMs.PayItemOF8, FMs.PayItemTX8, FMs.PayItemCD8, FMs.PayItemPen8,
				FMs.PayItemNm9, FMs.PayItemOF9, FMs.PayItemTX9, FMs.PayItemCD9, FMs.PayItemPen9, FMs.PayItemNm10, FMs.PayItemOF10, FMs.PayItemTX10,
				FMs.PayItemCD10, FMs.PayItemPen10, FMs.PayItemNm11, FMs.PayItemOF11, FMs.PayItemTX11, FMs.PayItemCD11, FMs.PayItemPen11, FMs.PayItemNm12,
				FMs.PayItemOF12, FMs.PayItemTX12, FMs.PayItemCD12, FMs.PayItemPen12, FMs.PayItemNm13, FMs.PayItemOF13, FMs.PayItemTX13, FMs.PayItemCD13,
				FMs.PayItemPen13, FMs.PayItemNm14, FMs.PayItemOF14, FMs.PayItemTX14, FMs.PayItemCD14, FMs.PayItemPen14, FMs.PayItemNm15, FMs.PayItemOF15,
				FMs.PayItemTX15, FMs.PayItemCD15, FMs.PayItemPen15, FMs.PayItemNm16, FMs.PayItemOF16, FMs.PayItemTX16, FMs.PayItemCD16, FMs.PayItemPen16,
				FMs.PayItemNm17, FMs.PayItemOF17, FMs.PayItemTX17, FMs.PayItemCD17, FMs.PayItemPen17, FMs.PayItemNm18, FMs.PayItemOF18, FMs.PayItemTX18,
				FMs.PayItemCD18, FMs.PayItemPen18, FMs.PayItemNm19, FMs.PayItemOF19, FMs.PayItemTX19, FMs.PayItemCD19, FMs.PayItemPen19, FMs.PayItemNm20,
				FMs.PayItemOF20, FMs.PayItemTX20, FMs.PayItemCD20, FMs.PayItemPen20, FMs.PayItemNm21, FMs.PayItemOF21, FMs.PayItemTX21, FMs.PayItemCD21,
				FMs.PayItemPen21, FMs.PayItemNm22, FMs.PayItemOF22, FMs.PayItemTX22, FMs.PayItemCD22, FMs.PayItemPen22, FMs.PayItemNm23, FMs.PayItemOF23,
				FMs.PayItemTX23, FMs.PayItemCD23, FMs.PayItemPen23, FMs.PayItemNm24, FMs.PayItemOF24, FMs.PayItemTX24, FMs.PayItemCD24, FMs.PayItemPen24,
				FMs.PayItemNm25, FMs.PayItemOF25, FMs.PayItemTX25, FMs.PayItemCD25, FMs.PayItemPen25, FMc.PensionEmployee, FMc.PensionEmployer,
				FMc.LeaveAllowance, FMc.PAYE, FMc.TTaxable, FMc.NetPay, FMc.Scheme,
				FMc.AnniversaryBonus, FMc.LatenessAmount,
                 FMc.AbsentAmount,
                 FMc.loanAmount,
                 FMc.OverTimeAmount, FMs.CurrencyType,
       FMs.[ShowPaySlip1]
      ,FMs.[ShowPaySlip2]
      ,FMs.[ShowPaySlip3]
      ,FMs.[ShowPaySlip4]
      ,FMs.[ShowPaySlip5]
      ,FMs.[ShowPaySlip6]
      ,FMs.[ShowPaySlip7]
      ,FMs.[ShowPaySlip8]
      ,FMs.[ShowPaySlip9]
      ,FMs.[ShowPaySlip10]
      ,FMs.[ShowPaySlip11]
      ,FMs.[ShowPaySlip12]
      ,FMs.[ShowPaySlip13]
      ,FMs.[ShowPaySlip14]
      ,FMs.[ShowPaySlip15]
      ,FMs.[ShowPaySlip16]
      ,FMs.[ShowPaySlip17]
      ,FMs.[ShowPaySlip18]
      ,FMs.[ShowPaySlip19]
      ,FMs.[ShowPaySlip20]
      ,FMs.[ShowPaySlip21]
      ,FMs.[ShowPaySlip22]
      ,FMs.[ShowPaySlip23]
      ,FMs.[ShowPaySlip24]
      ,FMs.[ShowPaySlip25],




				(Select OName from BrhMasters where HashKey=Et.BranchID) BranchName,
				(Select OAddress from BrhMasters where HashKey=Et.BranchID) BranchAddress,
				(Select OType from BrhMasters where HashKey=Et.BranchID) BranchType
				FROM dbo.EmpTbl AS Et, dbo.Fin_PRIndvPay AS FMc, dbo.Fin_PRSettings AS FMs
				WHERE
				Et.HashKey = FMc.EmpID AND
				Et.EmpStatus = 'Active' AND Et.Status IN ('A','U') AND (FMc.Status = 'A') AND FMc.HashKey='" . $EditID . "' AND FMs.HashKey='" . $Scheme_PRSetting_HashKey . "'";
                // print_r($dbOpen2);

                include '../login/dbOpen2.php';
                $total_ded = 0;
                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                    $BranchName = $row2["BranchName"];
                    $BranchAddress = $row2['BranchAddress'];
                    $BranchType = $row2['BranchType'];
                    $paymonth_yr = $row2['PMth'];

                    //$Script="Select MMsg from MailTemp where MSub='PaySlip'";
                    //$PaySlip = ScriptRunner($Script,"MMsg");

                    $Script = "Select MMsg, MSub from MailTemp where MSub='PaySlip Mail'"; //MSub='TimeOff Approved'";
                    $PaySlip = ScriptRunner($Script, "MMsg");
                    $Subj = ScriptRunner($Script, "MSub"); //"Payslip for the month of ".$row2['PMth'];
                    $Subj = str_replace('#MonthPaid#', $row2['PMth'], $Subj);
                    $QueryStr = "";
                    $QueryStrDR = "";
                    $QueryStrCR = "";
                    $QueryStrFP = "";

                    if ($BranchType == "Head Office") {
                        $PaySlip = str_replace('#BranchName#', $BranchName . "<br/>", $PaySlip);
                        $PaySlip = str_replace('#BranchAddress#', "Address: " . $BranchAddress . "<hr/>", $PaySlip);
                    } else {
                        $PaySlip = str_replace('#BranchName#', "Branch Name:" . $BranchName . "<br/>", $PaySlip);
                        $PaySlip = str_replace('#BranchAddress#', "Branch Address: " . $BranchAddress . "<hr/>", $PaySlip);
                    }
                    $total_fp = 0;
                    $ty = 0;

                    for ($io = 1; $io <= 25; $io++) {
                        if ($row2['ShowPaySlip' . $io] == "1") {
                            if ($row2['PayItemCD' . $io] == "CR" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px'><td width='150' align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'>" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px'>" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                            }

                            if ($row2['PayItemCD' . $io] == "DR" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                $total_ded = $total_ded + $row2['loanAmount'];
                                $QueryStrDR = $QueryStrDR . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'>" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px'>" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                            }
                            // temporary hack to be fix later
                            //****************************** */
                            if (isset($row2['loanAmount']) && $row2['loanAmount'] > 0 && $ty == 0) {
                                $QueryStrDR = $QueryStrDR . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock_Light'style='font-size:11px'>" . 'LOAN REPAYMENT' . ": </td><td style='font-size:11px'>" . number_format($row2['loanAmount'], 2) . "</td></tr>";
                                $ty = 1;
                            }
                            //****************************** */

                            if ($row2['PayItemCD' . $io] == "FP" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                $total_fp += $row2["PayItem" . $io];
                                $QueryStrFP = $QueryStrFP . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock_Light'style='font-size:11px'>" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px'>" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                            }
                        }
                    }

                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    $Script = "SELECT * FROM Fin_PRIndvPay WHERE HashKey = '" . $EditID . "'";

                    $more_item_add_name1 = ScriptRunner($Script, "more_item_add_name1");
                    $more_item_add_value1 = ScriptRunner($Script, "more_item_add_value1");
                    $more_item_add_name2 = ScriptRunner($Script, "more_item_add_name2");
                    $more_item_add_value2 = ScriptRunner($Script, "more_item_add_value2");
                    $more_item_add_name3 = ScriptRunner($Script, "more_item_add_name3");
                    $more_item_add_value3 = ScriptRunner($Script, "more_item_add_value3");
                    $more_item_add_name4 = ScriptRunner($Script, "more_item_add_name4");
                    $more_item_add_value4 = ScriptRunner($Script, "more_item_add_value4");
                    $more_item_add_name5 = ScriptRunner($Script, "more_item_add_name5");
                    $more_item_add_value5 = ScriptRunner($Script, "more_item_add_value5");

                    $more_item_remove_name1 = ScriptRunner($Script, "more_item_remove_name1");
                    $more_item_remove_value1 = ScriptRunner($Script, "more_item_remove_value1");
                    $more_item_remove_name2 = ScriptRunner($Script, "more_item_remove_name2");
                    $more_item_remove_value2 = ScriptRunner($Script, "more_item_remove_value2");
                    $more_item_remove_name3 = ScriptRunner($Script, "more_item_remove_name3");
                    $more_item_remove_value3 = ScriptRunner($Script, "more_item_remove_value3");
                    $more_item_remove_name4 = ScriptRunner($Script, "more_item_remove_name4");
                    $more_item_remove_value4 = ScriptRunner($Script, "more_item_remove_value4");
                    $more_item_remove_name5 = ScriptRunner($Script, "more_item_remove_name5");
                    $more_item_remove_value5 = ScriptRunner($Script, "more_item_remove_value5");

                    //++++++++++++++++++++++++++++++ Individual specific Allowance, not tied to group +++++++++++++++++++++++++++++++
                    if (($more_item_add_name1 != "") || ($more_item_add_name2 != "") || ($more_item_add_name3 != "") || ($more_item_add_name4 != "") || ($more_item_add_name5 != "")) {
                        $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Allowances & Reliefs</td></tr>";
                    }

                    if ($more_item_add_name1 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name1 . "</td>
							<td>" . number_format($more_item_add_value1, 2) . "</td></tr>";
                    }
                    if ($more_item_add_name2 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name2 . "</td>
							<td>" . number_format($more_item_add_value2, 2) . "</td></tr>";
                    }
                    if ($more_item_add_name3 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name3 . "</td>
							<td>" . number_format($more_item_add_value3, 2) . "</td></tr>";
                    }
                    if ($more_item_add_name4 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name4 . "</td>
							<td>" . number_format($more_item_add_value4, 2) . "</td></tr>";
                    }
                    if ($more_item_add_name5 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name5 . "</td>
							<td>" . number_format($more_item_add_value5, 2) . "</td></tr>";
                    }

                    //+++++++++++++++++++++ Individual specific Deductions, not tied to group ++++++++++++++++++++++++++++++

                    if (($more_item_remove_name1 != "") || ($more_item_remove_name2 != "") || ($more_item_remove_name3 != "") || ($more_item_remove_name4 != "") || ($more_item_remove_name5 != "")) {
                        $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Deductions</td></tr>";
                    }

                    if ($more_item_remove_name1 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name1 . "</td>
							<td>" . number_format($more_item_remove_value1, 2) . "</td></tr>";
                    }
                    if ($more_item_remove_name2 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name2 . "</td>
							<td>" . number_format($more_item_remove_value2, 2) . "</td></tr>";
                    }
                    if ($more_item_remove_name3 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name3 . "</td>
							<td>" . number_format($more_item_remove_value3, 2) . "</td></tr>";
                    }
                    if ($more_item_remove_name4 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name4 . "</td>
							<td>" . number_format($more_item_remove_value4, 2) . "</td></tr>";
                    }
                    if ($more_item_remove_name5 != "") {
                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name5 . "</td>
							<td>" . number_format($more_item_remove_value5, 2) . "</td></tr>";
                    }

                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                    //Extension for LOAN DISPLAY

                    $dbOpen3 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, (datename(M,ExpPayDate)+' '+datename(YYYY,ExpPayDate)) LnPayDate, * from LnIndvOff
		where EID='" . $EmpHKey . "' and Status = 'P' and Status <> 'D' Order by ExpPayDate ASC");

                    //if (ScriptRunner($dbOpen3,"Cnt") > 0){
                    //    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr></td></tr>";

                    include '../login/dbOpen3.php';
                    $row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH);
                    //while($row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH))        {

                    $ExpPay = $row3['ExpPay'];
                    $EID = $row3['EID'];
                    $LID = $row3['LID'];
                    $ExpPDate = $row3['ExpPD'];
                    $HashKey = $row3['HashKey'];
                    $LnPayDate = $row3['LnPayDate'];

                    if (($LnPayDate == $paymonth_yr) && (($ExpPDate != "") || ($ExpPay != ""))) {
                        //($paymonth_yr)[$LnPayDate]date('F Y', strtotime(date('M Y')))
                        $QueryStr = $QueryStr . "<tr ><td colspan='2' height='1'><hr><br/>Loan repayment</td></tr>";

                        $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $ExpPDate . "</td>
							<td>" . $ExpPay . "</td></tr>";
                    }
                    //}
                    //}

                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





                    if ($row2['LeaveAllowance'] > 0) {
                        $QueryStrCR = $QueryStrCR . "<tr style='font-size:10px' ><td align='right' class='tdMenu_HeadBlock_Light'>LEAVE ALLOWANCE</td><td style='font-size:10px' >" . number_format($row2['LeaveAllowance'], 2) . "</td></tr>";
                    }

                    $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr></td></tr>";
                    $total_gross = $row2['Gross'] + $total_fp;
                    $QueryStrCR = $QueryStrCR . "<tr><td align='right' class='tdMenu_HeadBlock'  style='font-size:10px'>GROSS</td><td>" . number_format($total_gross, 2) . "</td></tr>";


                    // ANNIVERSARY BONUS
                    if ($row2['AnniversaryBonus'] > 0) {
                        $QueryStrCR = $QueryStrCR . "<tr style='font-size:10px' ><td align='right' class='tdMenu_HeadBlock_Light'>ANNIVERSARY BONUS</td><td style='font-size:10px' >" . number_format($row2['AnniversaryBonus'], 2) . "</td></tr>";
                    }
                    // OVERTIME BONUS
                    if ($row2['OverTimeAmount'] > 0) {
                        $QueryStrCR = $QueryStrCR . "<tr style='font-size:10px' ><td align='right' class='tdMenu_HeadBlock_Light'>Overtime</td><td style='font-size:10px' >" . number_format($row2['OverTimeAmount'], 2) . "</td></tr>";
                    }

                    // LATENESS BONUS
                    if ($row2['LatenessAmount'] > 0) {
                        $total_ded = $total_ded + $row2['LatenessAmount'];
                        $QueryStrDR = $QueryStrDR . "<tr style='font-size:10px' ><td align='right' class='tdMenu_HeadBlock_Light'>Lateness</td><td style='font-size:10px' >" . number_format($row2['LatenessAmount'], 2) . "</td></tr>";
                    }

                    // ABSENT
                    if ($row2['AbsentAmount'] > 0) {
                        $total_ded = $total_ded + $row2['AbsentAmount'];
                        $QueryStrDR = $QueryStrDR . "<tr style='font-size:10px' ><td align='right' class='tdMenu_HeadBlock_Light'>Absent</td><td  >" . number_format($row2['AbsentAmount'], 2) . "</td></tr>";
                    }

                    if ($row2['CurrencyType'] != "") {
                        $script_curr_name = "SELECT Val1 FROM Masters WHERE HashKey='" . trim($row2['CurrencyType']) . "'";
                        $curr_name = " (" . ScriptRunner($script_curr_name, "Val1") . ")";
                    } else {
                        $curr_name = " (Naira NGR)";
                    }

                    // $QueryStr = $QueryStr . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >PENSION:</td><td style='font-size:11px'>" . number_format($row2['PensionEmployee'], 2) . "</td></tr>";
                    // $QueryStr = $QueryStr . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >PAYE:</td><td style='font-size:11px'>" . number_format($row2['PAYE'], 2) . "</td></tr>";
                    $QueryStr = $QueryStr . "<tr style='font-size:11px'><td height=1></td><td height=1><hr></td></tr>";
                    $QueryStr = $QueryStr . "<tr style='font-size:11px'><td align='right' class='tdMenu_HeadBlock' style='font-size:11px' ><strong>NET PAY:</strong></td><td style='font-size:11px'><strong>" . number_format($row2['NetPay'], 2) . "$curr_name </strong></td></tr>";
                    $QueryStr = $QueryStr . "<tr style='font-size:11px'><td height=1></td><td height=1><hr></td></tr>";

                    //$LeaveAllowance=$row2['LeaveAllowance'];
                    //$PensionEmployer=$row2['PensionEmployer'];
                    //if (floatval($LeaveAllowance)>0){$LeaveAllowance=$LeaveAllowance/12;} else{$LeaveAllowance=0;}
                    //$TTaxable=$row2['TTaxable'];
                    //if (floatval($TTaxable)>0){$TTaxable=$TTaxable/12;} else{$TTaxable=0;}
                    $PaySlip = str_replace('#MonthPaid#', $row2['PMth'], $PaySlip);
                    $PaySlip = str_replace('#Name#', $row2['Full Name'], $PaySlip);
                    $empdt = $row2['EmpDt'] ? $row2['EmpDt']->format('F j, Y') : "N/A";
                    $empcat = $row2['EmpCategory'] ? $row2['EmpCategory'] : "N/A";
                    $PaySlip = str_replace('#MonthPaid#', $row2['PMth'], $PaySlip);
                    $PaySlip = str_replace('#Name#', $row2['Full Name'], $PaySlip);
                    $PaySlip = str_replace('#Idnumber#', $row2['EmpID'], $PaySlip);
                    $PaySlip = str_replace('#category#', $empcat, $PaySlip);
                    $PaySlip = str_replace('#Employmentdate#', $empdt, $PaySlip);

                    if (strlen($QueryStrCR) > 3) {
                        $QueryStrCR = "<tr><th class='tdMenu_HeadBlock' style='color:blue ; font-size:10px'   >Earnings</th></tr>" . $QueryStrCR;
                    }
                    // if (strlen($QueryStrDR) > 3) {
                    $QueryStrDR = "<tr><th colspan='2'  class='tdMenu_HeadBlock' style='color:blue ; font-size:10px'>Deductions</th></tr>" . $QueryStrDR;
                    $total_ded = $total_ded + $row2['PensionEmployee'];
                    $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:10px'>PENSION</td><td>" . number_format($row2['PensionEmployee'], 2) . "</td></tr>";
                    $total_ded = $total_ded + $row2['PAYE'];
                    $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light'  style='font-size:10px'>PAYE</td><td>" . number_format($row2['PAYE'], 2) . "</td></tr>";
                    $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock'  style='font-size:10px'>Total Deduction</td><td>" . number_format($total_ded, 2) . "</td></tr>";
                    // }
                    if (strlen($QueryStrFP) > 3) {
                        $QueryStrFP = "<tr><th class='tdMenu_HeadBlock'  style='color:blue ; font-size:10px'>Others</th></tr>" . $QueryStrFP;
                    }

                    $PaySlip = str_replace('#Values#', ('<TABLE width="385">' . $QueryStrCR . $QueryStrDR . $QueryStr . '</TABLE>'), $PaySlip);
                }
                include '../login/dbClose2.php';

                $To = ScriptRunner($Script_ID, "Email");
                $To2 = ScriptRunner($Script_ID, "Sec_Email");



                $Bdy = $PaySlip;

                // $dompdf = new DOMPDF();
                $mpdf = new mPDF();

                // Upload your HTML code
                // $dompdf->load_html("$Bdy");
                $pdf_body = str_replace('#BranchName#', $BranchName, $_SESSION["StkTck" . "MailHeader"]) . $Bdy . $_SESSION["StkTck" . "MailFooter"];
                $mpdf->WriteHTML($pdf_body);



                //Render html
                // $dompdf->render();
                $custId = $_SESSION["StkTck" . "CustID"];
                $path = "../uploads_files/$custId/payslips";
                $name = md5(uniqid(rand(), true)) . ".pdf";

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                // //Create and output the pdf 
                // Output to a file
                // $ok = $dompdf->output();

                $mpdf->Output("$path/$name", "F");

                // file_put_contents("$path/$name", $ok);
                $Atth = "$path/$name";

                QueueMail($To, $Subj, $Bdy, $Atth, ''); //Sends the mail
                if ($To2) {
                    QueueMail($To2, $Subj, $Bdy, $Atth, ''); //Sends the mail
                }

                echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee payslip sent successfully.', 'green'); }</script>");
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT Em.EmpID as EmpID, Fp.HashKey from EmpTbl Em, fin_payslip Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";
                $EditID = ScriptRunner($Script_ID, "HashKey");

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [fin_payslip] WHERE [HashKey]='" . $EditID . "'") == false) {
                    //Check if account is customer account and if SMS alert is set to fire
                    $Script = "UPDATE [fin_payslip]
					SET [Status] = 'U'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Unauthorize");
                    AuditLog("UNAUTHORIZE", "Employee record unauthorized for " . ScriptRunner($Script_ID, "EmpID"), $HashKey);

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee record(s) unauthorized successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
        include '../main/clr_val.php'; /* CLEARS $EditID */
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            $EditID = $_REQUEST["DelBox" . $i];
            if (strlen(trim($EditID)) == 32) {
                $Script_ID = "SELECT Em.EmpID as EmpID, Fp.HashKey from EmpTbl Em, fin_payslip Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $_REQUEST["DelBox" . $i] . "'";
                $EditID = ScriptRunner($Script_ID, "HashKey");

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [fin_payslip] WHERE [HashKey] = '" . $EditID . "'") == true) {
                    //Check if account is customer account and if SMS alert is set to fire
                    $Script = "UPDATE [fin_payslip]
					SET [Status] = 'D'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Unauthorize");
                    AuditLog("UNAUTHORIZE", "Employee record deleted for " . ScriptRunner($Script_ID, "EmpID"), $HashKey);

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee record(s) deleted successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
        include '../main/clr_val.php'; /* CLEARS $EditID */
    }
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Calendar API -->

<script>
    $(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(function() {
        $(document).tooltip();
    });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">


<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <form action="#" method="post" name="Records" target="_self" id="Records">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4 col-2 text-center">
                    </div>
                    <div class="col-md-4 col-8 text-center">
                        <h4>
                            Send / Print Employee Payslip
                        </h4>
                    </div>
                    <div class="col-md-4 col-2">
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php $Load_Date = true;
                    $Load_Search = true;
                    $Load_Auth = false;
                    include '../main/report_opt.php'; ?>
                </div>
                <?php if (!empty($_POST)) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-responsive" id="table">
                                <thead>
                                    <tr>
                                        <th valign="middle" class="smallText" scope="col" data-sorter="false">
                                            <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                            <label for="DelChk_All"></label>
                                        </th>
                                        <th align="left" valign="middle" class="smallText" scope="col">Pay Month</th>
                                        <th align="left" valign="middle" class="smallText" scope="col">Staff ID</th>
                                        <th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Employee Name</span></th>
                                        <th align="left" valign="middle" class="smallText" scope="col">Pension</th>
                                        <th align="left" valign="middle" class="smallText" scope="col">PAYE</th>
                                        <th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Net Pay</span></th>
                                        <th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Gross Pay</span></th>
                                        <th align="left" valign="middle" scope="col" data-sorter="false">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {
                                        $PayMthDt = " and PayMonth between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "' ";
                                    } else {
                                        $PayMthDt = '';
                                    }
                                    $Del = 0;
                                    $dbOpen2 = ("SELECT Convert(Varchar(11),Fm.AddedDate,106) as Dt, (DATENAME(month,Fm.PayMonth)+' '+DATENAME(year,Fm.PayMonth)) as PMth, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' '+Em.ONames) AS Nm, Em.EmpID AS EID from EmpTbl Em, Fin_PRIndvPay Fm where Em.HashKey=Fm.EmpID and (Fm.Status in ('A','P') and Fm.Status <> 'D') and (Em.EmpStatus='Active') and (Em.EmpID like '%" . $SearchVal . "%' or Em.FName like '%" . $SearchVal . "%' or Em.SName like '%" . $SearchVal . "%' or Em.ONames like '%" . $SearchVal . "%') " . $PayMthDt . " order by SName asc, UpdatedDate desc");
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        $Del = $Del + 1;
                                    ?>
                                        <tr>
                                            <td valign="middle" scope="col" class="TinyTextTight">
                                                <input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
                                                <label for="<?php echo ("DelBox" . $Del); ?>"></label>
                                            </td>
                                            <td align="center" valign="middle" scope="col" class="TinyText">&nbsp;<?php echo $row2['PMth']; ?></td>
                                            <td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['EID'])); ?></td>
                                            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['Nm'])); ?></td>
                                            <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PensionEmployee'], 2); ?>&nbsp;</td>
                                            <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PAYE'], 2); ?>&nbsp;</td>
                                            <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['NetPay'], 2); ?>&nbsp;</td>
                                            <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['Gross'], 2); ?>&nbsp;</td>
                                            <td align="center" valign="middle" class="TinyText" scope="col">
                                                <?php if (ValidateURths("RESEND PAYSLIP" . "P") == true) { ?>
                                                    <input name="SubmitTrans" type="button" class="btn btn-danger btn-sm" id="SubmitTrans" value="Print" onClick="parent.ShowDisp('Payslip&nbsp;Print','fin/PSlip_Prnt.php?EmpTyp=Mrg&RecStatus=<?php echo $row2['Status']; ?>&EmpIDHash=<?php echo $EditID; ?>&HID=<?php echo $row2['HashKey']; ?>',600,730,'Yes')" />
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php }
                                    include '../login/dbClose2.php'; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 pt-5">
                            <?php include '../main/pagination.php'; ?>
                        </div>
                        <div class="col-md-6 pt-5">
                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <?php if (ValidateURths("RESEND PAYSLIP" . "S") == true) { ?>
                                        <input name="SubmitTrans" type="button" class="btn btn-danger btn-sm" id="SubmitTrans" value="Send Pay Slip" onClick="CollateChks(); YesNo('Send','Send')" />
                                    <?php } ?>

                                    <?php
                                    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								  	<input name="FAction" id="FAction" type="hidden">
							  	  	<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </form>
</body>
<?php

// OVERTIME
//if((isset($_REQUEST['PayCalcType']))&&($_REQUEST['PayCalcType']=="Overtime")){
if ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] === "Overtime")) {

    echo '
            <thead>
                <tr>
                    <th width="20"valign="middle" scope="col" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label></th>
                    <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                    <th >Date</th>
                    <th >Full Name</th>
                    <th >Department</th>
                    <th >Level</th>
                    <th data-sorter="false">Resumption</th>
                    <th data-sorter="false">Closure</th>
                    <th data-sorter="false">Time-In</th>
                    <th data-sorter="false">Time-Out</th>
                    <th >Overtime</th>
                    <th >Amount</th>
                    <th> Type</th>
                    <th>Exemption</th>
                </tr>
            </thead>
            <tbody>
        ';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        //return ($row2['$rwfield']);
        //if(isset($close_time)&&(strtotime($close_time)) > strtotime($row2['TOut'])){
        //print "$Del) " . $close_time ." < ".$row2['TOut'] ."<br/>";

        $Script_atd = "Select count(*) Ct from Fin_AttendSet where ActiveTime='TimeOut' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";

        $Script_atdnset = "Select Convert(varchar(5),CloseTime,114) ClsT, Convert(varchar(5),TimeIn,114) RsmT, * from Fin_AttendSet where ActiveTime='TimeOut' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";


        $amt_unit = ScriptRunner($Script_atdnset, "OverTime"); //amount set for this
        $ActiveTime = ScriptRunner($Script_atdnset, "ActiveTime");
        $close_time = ScriptRunner($Script_atdnset, "ClsT");
        $resume_time = ScriptRunner($Script_atdnset, "RsmT");
        if ($close_time && $resume_time) {

            $actual_close_time = new DateTime($close_time);
            $actual_resume_time = new DateTime($resume_time);

            // Calculate the difference
            $actual_time_exit_diff = $actual_resume_time->diff($actual_close_time);

            // Convert the difference to minutes
            $actual_time_exit_diff_minutes = ($actual_time_exit_diff->h * 60) + $actual_time_exit_diff->i;
            // var_dump($actual_time_exit_diff_minutes);
        }


        $close_time = ScriptRunner($Script_atdnset, "ClsT");
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $Del = $Del + 1;
        if (ScriptRunner($Script_atd, "Ct") > 0 && isset($close_time)) {
            //close if
?>
            <tr>
                <td width="20" height="20" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
                <!--<td width="110" align="left" valign="middle" scope="col"><?php echo (trim($row2['AD'])); ?></td> //-->
                <td width="120" align="left" valign="middle" scope="col"><?php echo (trim($row2['RD'])); ?></td>
                <td align="left" valign="middle" scope="col"><?php echo trim($row2['Nm']); ?></td>
                <td width="233" align="left" valign="middle" scope="col"><?php echo $row2['Department']; ?></td>
                <td align="left" valign="middle" scope="col">
                    <?php
                    $Script_set = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'";
                    $Fin_AttendSet_HashKey = ScriptRunner($Script_set, "HashKey");
                    $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $Fin_AttendSet_HashKey . "')";
                    $level_name = ScriptRunner($Script_ID, "SetValue2");
                    print $level_name;
                    ?>
                </td>
                <td width="98" align="center" valign="middle" scope="col">
                    <?php echo $resume_time; ?>
                </td>
                <td width="98" align="center" valign="middle" scope="col">
                    <?php echo $close_time; ?>
                </td>
                <td align="center" valign="middle" scope="col">
                    <?php
                    echo "<font color=\"blue\"><b>" . $row2['TIn'] . "</b></font>";
                    ?>
                </td>
                <td align="center" valign="middle" scope="col">
                    <?php
                    echo "<font color=\"blue\"><b>" . $row2['TOut'] . "</b></font>";
                    ?>
                </td>
                <td width="108" align="center" valign="middle" scope="col">
                    <?php
                    //Calculate overtime
                    $time_out = $row2['TOut'];
                    $time_inn = $row2['TIn'];
                    if (is_null($time_out)) {
                        $time_exit_diff = 0;
                        echo '0.00 mins';
                    } else {
                        $e_resume_time = new DateTime($resume_time);
                        $exit_time = new DateTime($time_out);
                        // var_dump($exit_time);
                        $time_exit_diff = $exit_time->diff($e_resume_time);

                        // var_dump($time_exit_diff);

                        if (
                            $exit_time > $e_resume_time
                        ) {
                            if ($time_exit_diff->h >= 1) {
                                // var_dump($time_exit_diff->h);
                                $time_exit_diff = ($time_exit_diff->h * 60) + $time_exit_diff->i;
                                if ($time_exit_diff > $actual_time_exit_diff_minutes) {
                                    $time_exit_diff = $time_exit_diff - $actual_time_exit_diff_minutes;
                                    echo $time_exit_diff . 'mins';
                                } else {
                                    $time_exit_diff = 0;
                                    echo '0.00 mins';
                                }
                            } else {

                                $time_exit_diff = $time_exit_diff->i;
                                if ($time_exit_diff > $actual_time_exit_diff_minutes) {
                                    $time_exit_diff = $time_exit_diff - $actual_time_exit_diff_minutes;
                                    echo $time_exit_diff . 'mins';
                                } else {
                                    $time_exit_diff = 0;
                                    echo '0.00 mins';
                                }
                            }
                        } else {
                            $time_exit_diff = 0;
                            echo '0.00 mins';
                        }
                    }
                    ?>
                </td>
                <td width="108" align="right" valign="middle" scope="col">
                    <?php
                    $type_arr = [];
                    // $Script="select * from Fin_AttendSet where PayCalcType='Overtime' AND  EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='".$row2['HKey']."' order by ItemStart desc)";
                    $dbOpen3 = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'";
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        // var_dump($row3['c_type']);
                        if (isset($row3['c_type'])) {
                            $type_arr[] = $row3['c_type'];
                        }
                    }
                    include '../login/Close3.php';
                    $ctty = '';
                    if (in_array('workdays', $type_arr)) {
                        $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'  and [c_type] = 'workdays'  ";
                        $ctty = 'workdays';
                    }

                    if (in_array('weekends', $type_arr)) {
                        // check if that day is a weekend
                        $weekend_arry = [];
                        $dbOpen3 = ("SELECT * from Masters where ItemName='Weekends' ");
                        include '../login/dbOpen3.php';
                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                            $weekend_arry[] = strtolower($row3['Val1']);
                        }

                        include '../login/Close3.php';
                        $day_words = strtolower(date("l", strtotime(trim($row2['RD']))));
                        if (in_array($day_words, $weekend_arry)) {

                            $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'  and [c_type] = 'weekends'  ";
                            $ctty = 'weekends';
                        }
                    }

                    if (in_array('events', $type_arr)) {
                        // check if that day is an events

                        $dbOpen3 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * from LvPHolsDetails where Status='A' and [HType] ='Event' and [OffOn] = 1 order by [SDate] desc, AuthDate desc");
                        include '../login/dbOpen3.php';
                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                            $paymentDate = date('Y-m-d', strtotime(trim($row2['RD'])));
                            //echo $paymentDate; // echos today!
                            $contractDateBegin = date('Y-m-d', strtotime(trim($row3['SDt'])));
                            $contractDateEnd = date('Y-m-d', strtotime(trim($row3['EDt'])));

                            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
                                $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'  and [c_type] = 'events'  ";
                                $ctty = 'events';
                            }
                        }
                        include '../login/Close3.php';
                    }

                    if (in_array('holidays', $type_arr)) {
                        // check if that day is an holiday
                        $dbOpen3 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * from LvPHolsDetails where Status='A' and [HType] ='Holiday' and [OffOn] = 1 order by [SDate] desc, AuthDate desc");
                        include '../login/dbOpen3.php';
                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                            $paymentDate = date('Y-m-d', strtotime(trim($row2['RD'])));
                            //echo $paymentDate; // echos today!
                            $contractDateBegin = date('Y-m-d', strtotime(trim($row3['SDt'])));
                            $contractDateEnd = date('Y-m-d', strtotime(trim($row3['EDt'])));

                            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
                                $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'  and [c_type] = 'holidays'  ";

                                $ctty = 'holidays';
                            }
                        }
                        include '../login/Close3.php';
                    }

                    // $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeOut'";

                    if (ScriptRunner($Script, "UseType") == "Percent") {
                        $Script_exmp = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                        $exempt = ScriptRunner($Script_exmp, "Exempt");
                        if ($exempt == '1') {
                            $amount_over_pay = 0;
                        } else {

                            $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                            $BasicPay = ScriptRunner("Select PayItem1 from Fin_PRCore where EmpID='" . $row2['HKey'] . "' and Status='A'", "PayItem1");
                            if ($BasicPay > 0) {
                                $BasicPay = $BasicPay / 12;
                            }
                            $minutesPay = ($BasicPay * $Amt) / 100;
                            $amount_over_pay = $minutesPay * $time_exit_diff;

                            if ($amount_over_pay < 0) {
                                $amount_over_pay = 0;
                            }
                        }

                        echo number_format($amount_over_pay, 2);

                        $Script = "UPDATE [Fin_Attend] SET [OverTimeAmount] = CAST($amount_over_pay as decimal) WHERE [Status] NOT IN ('B', 'D') AND [HashKey] = '" . $row2['HashKey'] . "' ";
                        ScriptRunnerUD($Script, "Inst");

                        $TAmt = $TAmt + $amount_over_pay;

                        if (isset($row2['TOut']) && $row2['TOut'] !== '' && isset($row2['TIn']) && $row2['TIn'] !== '') {

                            $close_time_min = new DateTime($close_time);
                            $time_diff_resume_close = $close_time_min->diff(new DateTime($resume_time));
                            if ($time_diff_resume_close->h >= 1) {
                                $time_diff_resume_close_min = ($time_diff_resume_close->h * 60) + ($time_diff_resume_close->i);
                            } else {
                                $time_diff_resume_close_min = $time_diff_resume_close->i;
                            }

                            if (new DateTime($row2['TIn']) < new DateTime($close_time)) {
                                $time_out_min = new DateTime($close_time);
                                $time_diff_out_in = $time_out_min->diff(new DateTime($row2['TIn']));
                                if ($time_diff_out_in->h >= 1) {
                                    $time_diff_out_in_min = ($time_diff_out_in->h * 60) + ($time_diff_out_in->i);
                                } else {
                                    $time_diff_out_in_min = $time_diff_out_in->i;
                                }
                            } else {
                                $time_diff_out_in_min = 0;
                            }

                            $cal_amt = $minutesPay * $time_diff_out_in_min;
                            if ($cal_amt > ($minutesPay * $time_diff_resume_close_min)) {
                                $cal_amt = ($minutesPay * $time_diff_resume_close_min);
                            }

                            $rec_date = $row2['RecDate']->format('Y-m-d');

                            $Script_shm = "UPDATE [shm_allocate_shifts] SET
                    [OverTimeAmount] = CAST($cal_amt  as decimal),
                    [TimeIn] = '" . $row2['TIn'] . "',
                    [TimeOut] = '" . $row2['TOut'] . "',
                    [expected_min] = '" . $time_diff_resume_close_min . "',
                    [actual_min] = '" . $time_diff_out_in_min . "',
                    [per_min_charge] = '" . $minutesPay . "',
                    [resume_time] = '" . $resume_time . "',
                    [closure_time] ='" . $close_time . "',
                    [type] = '" . $ctty . "',
                    [HashKey]  ='" . $row2['HashKey'] . "'
                    WHERE [Status] NOT IN ('B', 'D') AND [RecDate] = '" . $rec_date . "' And [EmpID] = '" . $row2['EmpID'] . "' ";
                            ScriptRunnerUD($Script_shm, "Inst");
                        }
                    } elseif (ScriptRunner($Script, "UseType") == "Value") {
                        //$Amt = floatval(ScriptRunner($Script,"OverTime"));
                        //$OvPay = $row2['OverTime'] * $Amt;
                        //echo number_format($OvPay,2);
                        //$TAmt = $TAmt + ($OvPay);
                        $Script_exmpt = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                        $exempt = ScriptRunner($Script_exmpt, "Exempt");
                        if ($exempt == '1') {

                            $amount_over_pay = 0;
                        } else {
                            $Overtime_minutes_Amount = ScriptRunner($Script, "OverTime");

                            $amount_over_pay = $Overtime_minutes_Amount * $time_exit_diff;

                            if ($amount_over_pay < 0) {
                                $amount_over_pay = 0;
                            }
                        }

                        echo number_format($amount_over_pay, 2);
                        $Script = "UPDATE [Fin_Attend] SET [OverTimeAmount] = CAST($amount_over_pay as decimal) WHERE [Status] NOT IN ('B', 'D') AND [HashKey] = '" . $row2['HashKey'] . "' ";
                        ScriptRunnerUD($Script, "Inst");
                        $TAmt = $TAmt + ($amount_over_pay);

                        if (isset($row2['TOut']) && $row2['TOut'] !== '' && isset($row2['TIn']) && $row2['TIn'] !== '') {

                            $close_time_min = new DateTime($close_time);
                            $time_diff_resume_close = $close_time_min->diff(new DateTime($resume_time));
                            if ($time_diff_resume_close->h >= 1) {
                                $time_diff_resume_close_min = ($time_diff_resume_close->h * 60) + ($time_diff_resume_close->i);
                            } else {
                                $time_diff_resume_close_min = $time_diff_resume_close->i;
                            }

                            if (new DateTime($row2['TIn']) < new DateTime($close_time)) {
                                $time_out_min = new DateTime($close_time);
                                $time_diff_out_in = $time_out_min->diff(new DateTime($row2['TIn']));
                                if ($time_diff_out_in->h >= 1) {
                                    $time_diff_out_in_min = ($time_diff_out_in->h * 60) + ($time_diff_out_in->i);
                                } else {
                                    $time_diff_out_in_min = $time_diff_out_in->i;
                                }
                            } else {
                                $time_diff_out_in_min = 0;
                            }

                            $cal_amt = $Overtime_minutes_Amount * $time_diff_out_in_min;
                            if ($cal_amt > ($Overtime_minutes_Amount * $time_diff_resume_close_min)) {
                                $cal_amt = ($Overtime_minutes_Amount * $time_diff_resume_close_min);
                            }
                            $rec_date = $row2['RecDate']->format('Y-m-d');

                            $Script_shm = "UPDATE [shm_allocate_shifts] SET
                    [OverTimeAmount] = CAST($cal_amt  as decimal),
                    [TimeIn] = '" . $row2['TIn'] . "',
                    [TimeOut] = '" . $row2['TOut'] . "',
                    [expected_min] = '" . $time_diff_resume_close_min . "',
                    [actual_min] = '" . $time_diff_out_in_min . "',
                    [per_min_charge] = '" . $Overtime_minutes_Amount . "',
                    [resume_time] = '" . $resume_time . "',
                    [closure_time] ='" . $close_time . "',
                    [type] = '" . $ctty . "',
                    [HashKey]  ='" . $row2['HashKey'] . "'
                    WHERE [Status] NOT IN ('B', 'D') AND [RecDate] = '" . $rec_date . "' And [EmpID] = '" . $row2['EmpID'] . "' ";
                            ScriptRunnerUD($Script_shm, "Inst");
                        }
                    } else {
                    }
                    ?>
                </td>
                <td width="69" align="center" valign="middle" scope="col">
                    <?php
                    /* if ($row2['RecType']=='O')
            {echo 'Overtime';}
            elseif (trim($row2['RecType'])=='W')
            {echo 'Weekend';}
            elseif (trim($row2['RecType'])=='L')
            {echo 'Lateness';}
             */
                    if (isset($resume_time) && ($amount_over_pay == 0)) { //(isset($resume_time)&&($resume_time > strtotime($row2['TIn']))){
                        echo 'Nil';
                    } elseif (isset($resume_time) && ($amount_over_pay > 0)) {
                        echo $ctty;
                    } else { //isset($close_time)&&($close_time > strtotime($row2['TOut']))))
                        echo 'Lateness';
                    }

                    ?>
                </td>
                <td>
                <?php
                // Exemption tab
                $Script = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                $exempt = ScriptRunner($Script, "Exempt");
                if ($exempt == '1') {
                    echo 'YES';
                } else {
                    echo 'NO';
                }
            }
                ?>
                </td>



            </tr>
            <?php
        }
        //include('Upld_Attd_inc.php');
    } //close while

    // ABSENT
    //if((isset($_REQUEST['PayCalcType']))&&($_REQUEST['PayCalcType']=="Overtime")){
    if ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] == "Absent")) {
        echo '
            <thead>
                <tr >
                    <th width="20"valign="middle" scope="col" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label></th>
                    <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                    <th >Date</th>
                    <th>Full Name</th>
                    <th>Department</th>
                    <th>Level</th>
                    <th>Amount</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
        ';
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            //return ($row2['$rwfield']);
            //if(isset($close_time)&&(strtotime($close_time)) > strtotime($row2['TOut'])){
            //print "$Del) " . $close_time ." < ".$row2['TOut'] ."<br/>";

            $Script_atdnset = "Select Convert(varchar(5),CloseTime,114) ClsT, Convert(varchar(5),TimeIn,114) RsmT, * from Fin_AttendSet where PayCalcType='Absent' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";
            $amt_unit = ScriptRunner($Script_atdnset, "OverTime"); //amount set for this
            $ActiveTime = ScriptRunner($Script_atdnset, "ActiveTime");
            $close_time = ScriptRunner($Script_atdnset, "ClsT");
            $resume_time = ScriptRunner($Script_atdnset, "RsmT");

            $close_time = ScriptRunner($Script_atdnset, "ClsT");
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if ($row2['RecType'] === 'A') {
                $Del = $Del + 1;
            ?>
                <tr>
                    <td width="20" height="20" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
                    <!--<td width="110" align="left" valign="middle" scope="col"><?php echo (trim($row2['AD'])); ?></td> //-->
                    <td width="120" align="left" valign="middle" scope="col"><?php echo (trim($row2['RD'])); ?></td>
                    <td align="left" valign="middle" scope="col"><?php echo trim($row2['Nm']); ?></td>
                    <td width="233" align="left" valign="middle" scope="col"><?php echo $row2['Department']; ?></td>
                    <td align="left" valign="middle" scope="col">
                        <?php
                        // $Script_set="select * from Fin_AttendSet where EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='".$row2['HKey']."' order by ItemStart desc)";

                        $Script_set = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and PayCalcType ='Absent'";

                        $Fin_AttendSet_HashKey = ScriptRunner($Script_set, "HashKey");
                        $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $Fin_AttendSet_HashKey . "')";
                        $level_name = ScriptRunner($Script_ID, "SetValue2");
                        print $level_name;
                        ?>
                    </td>
                    <td width="108" align="right" valign="middle" scope="col">
                        <?php
                        $Script_set = "SELECT Gross from Fin_PRCore where EmpID='" . $row2['HKey'] . "' AND Status = 'A'";
                        $gross = ScriptRunner($Script_set, "Gross");
                        if ($gross) {
                            $absentAmount = $gross / 365;
                            echo number_format($absentAmount);
                            $Script = "UPDATE [Fin_Attend] SET [AbsentAmount] = CAST($absentAmount as decimal) WHERE [Status] NOT IN ('B', 'D') AND [HashKey] = '" . $row2['HashKey'] . "' ";
                            ScriptRunnerUD($Script, "Inst");
                            $TAmt = $TAmt + $absentAmount;
                        }
                        ?>
                    </td>
                    <td width="69" align="center" valign="middle" scope="col">
                        Absent
                    </td>
                </tr>
    <?php
            }
        }
        //include('Upld_Attd_inc.php');
    } //close while

    ?>
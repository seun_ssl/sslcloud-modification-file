<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

if (ValidateURths("PAY SALARY" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$ModMaster = ValidateURths("PAYROLL MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($Script)) {
    $Script = "";
}
if (!isset($SelID)) {
    $SelID = "";
}
//if (!isset($Dt)) {$Dt="";}

$LoanAmt = 0;
$PaySlip = $CmdTyp = "";
$CR = "";
$DR = "";
$TTaxable = "";
$TPension = "";
$AllItems = "";
$GoValidate = true;
$TTaxable = 0;
$TDR_NTaxable = 0;
$TAmtLate = 0; // Variable to hold total amount to be subtracted for lateness
$TAmtAbsent = 0; // Variable to hold total amount to be subtracted for being absent
$TAmtOverTime = 0; // Variable to hold total amount to be added for overtime
//$gross_for_anniversary=0; // Total gross to get anniversary bonus
//$bonus=0;        // Total anniversary bonus
$ExpPay2 = 0;
$total_bonus = 0;

$Script = "Select * from Fin_PRCore where HashKey='zzz'";
$dbOpen2 = ("SELECT * from Fin_PRSettings where HashKey='zzz'");
$EmpID_ = "";
$PayScheme_ = "";

if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] = "View Selected" ) {
        if (ValidateURths("PAY SALARY" . "V") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $CmdTyp = "Edit";
                $EditID = ECh($HashVals[$i]);

                //$Script_Edit ="select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='ebece94938fa75a73f08389dc5622eaa' and Status not in ('A','D')";

                $Script_Edit = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";

                $EmpID_ = ScriptRunner($Script_Edit, "EmpID");
                $Script_Edit__ = "select * from [Fin_PRCore] where EmpID='" . $EmpID_ . "' and Status='A'";
                $PayScheme_ = ScriptRunner($Script_Edit__, "Scheme"); //Get Scheme for the specified employee salary
                $loan_amt = ScriptRunner($Script_Edit, "loanAmount");

                $script_lvType = "SELECT SetValue11 FROM Settings WHERE Setting = 'LeaveTyp'";
                $lvType = Scriptrunner($script_lvType, "SetValue11");

                $script_applied_leave = "SELECT *  FROM LvIndTOff WHERE LType = '0b23845da0e280008e5b9c7a2adfb462'
AND EmpID='" . $EmpID_ . "' AND [Status] = 'A' AND Year(CONVERT(char(11),LvDate,106)) = YEAR(CONVERT(char(11),GETDATE(),106))
ORDER BY ApprovedDate ASC";

                $approved = Scriptrunner($script_applied_leave, "Status");

                $script_bonus_pay = "SELECT TOP(1) * FROM Fin_PRIndvPay
WHERE Year(CONVERT(char(11),PayMonth,106)) = YEAR(CONVERT(char(11),GETDATE(),106)) AND [Status] IN ('A')
AND EmpID='" . $EmpID_ . "'
ORDER BY  PayMonth ASC";
                $PayBonus = Scriptrunner($script_bonus_pay, "payStatusBonus");

                $script_paymonth = "select Month(CONVERT(char(11),PayMonth,106)) as payDt
from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";

                $Pay_month = Scriptrunner($script_paymonth, "payDt");

                $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $EmpID_ . "'";
                $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
            }
        }
    }

?>


<script type="text/javascript">
    function GetTotal_PAYE() {
        // alert('here');
        //SORT OUT ALL VALUES
        var GrossPay = document.getElementById("Gross").value.replace(',', '');
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);

        var TTaxable_ = document.getElementById("TTaxable_H").value.replace(',', '');
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        //alert(TTaxable_);

        var Pension = document.getElementById("PensionEmployee").value.replace(',', '');
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);

        var OverTime = document.getElementById("PayItem4").value.replace(',', '');
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);

        var WeekEnd = document.getElementById("PayItem6").value.replace(',', '');
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);

        var DR = document.getElementById("H_DR").value.replace(',', '');
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);

        var Total_DR_NTax = document.getElementById("H_TDR_NTaxable").value.replace(',', '');
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);

        var LeaveAllowance_DisMth = document.getElementById("LeaveAllowance").value.replace(',', '');
        //alert(LeaveAllowance_DisMth);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);

        /*
        	var Leave=document.getElementById("LeaveAllowance_H").value.replace(',','');
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        */

        NetPay = 0;
        if (parseFloat(OverTime) > 0) {
            GrossPay = parseFloat(GrossPay) + parseFloat(OverTime);
        }

        if (parseFloat(WeekEnd) > 0) {
            GrossPay = parseFloat(GrossPay) + parseFloat(WeekEnd);
        }


        //Total Relief is 200k plus 20% of gross or 1% of Gross, which ever is greater
        GrossPay = GrossPay * 12;
        Pension = Pension * 12;
        Leave = Leave * 12;

        Total_DR_NTax = (Total_DR_NTax * 1);
        //alert(Total_DR_NTax);
        TRelief = 200000 + (parseFloat(GrossPay) * (20 / 100)) + parseFloat(Pension) + Total_DR_NTax;
        //alert(TRelief);
        $TotTaxable = (parseFloat(TTaxable_) * 12) - parseFloat(TRelief);
        console.log($TotTaxable);

        //Get the total amount for all relief
        //Get the total taxable pay
        $PAYE = 0;
        $PAYE0 = parseFloat(document.getElementById("S_PAYE0").value).toFixed(2) * 1;
        $PAYE1 = parseFloat(document.getElementById("S_PAYE1").value).toFixed(2) * 1;
        $PAYE2 = parseFloat(document.getElementById("S_PAYE2").value).toFixed(2) * 1;
        $PAYE3 = parseFloat(document.getElementById("S_PAYE3").value).toFixed(2) * 1;
        $PAYE4 = parseFloat(document.getElementById("S_PAYE4").value).toFixed(2) * 1;
        $PAYE5 = parseFloat(document.getElementById("S_PAYE5").value).toFixed(2) * 1;
        $PAYE6 = parseFloat(document.getElementById("S_PAYE6").value).toFixed(2) * 1;
        $PAYE7 = parseFloat(document.getElementById("S_PAYE7").value).toFixed(2) * 1;

        $PAYE_Comm0 = parseFloat(document.getElementById("S_PAYE_Comm0").value).toFixed(2) * 1;
        $PAYE_Comm1 = parseFloat(document.getElementById("S_PAYE_Comm1").value).toFixed(2) * 1;
        $PAYE_Comm2 = parseFloat(document.getElementById("S_PAYE_Comm2").value).toFixed(2) * 1;
        $PAYE_Comm3 = parseFloat(document.getElementById("S_PAYE_Comm3").value).toFixed(2) * 1;
        $PAYE_Comm4 = parseFloat(document.getElementById("S_PAYE_Comm4").value).toFixed(2) * 1;
        $PAYE_Comm5 = parseFloat(document.getElementById("S_PAYE_Comm5").value).toFixed(2) * 1;
        $PAYE_Comm6 = parseFloat(document.getElementById("S_PAYE_Comm6").value).toFixed(2) * 1;
        $PAYE_Comm7 = parseFloat(document.getElementById("S_PAYE_Comm7").value).toFixed(2) * 1;


        // Applies the flat rate of 1%
        if ($GrossPay <= $PAYE0) {
            $PAYE = ($GrossPay * $PAYE_Comm0) / 100;
        }

        // Applies first band of 7%
        if (($GrossPay > ($PAYE0)) && ($TotTaxable < ($PAYE1))) {
            $PAYE_Rem = $TotTaxable;
            $PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
        }

        //alert($TotTaxable);
        /*
        if ($TotTaxable < $PAYE0)
        {$PAYE = (GrossPay * $PAYE_Comm0)/100;}

        //Amount taxed on first band
        if (($TotTaxable > ($PAYE0)) && ($TotTaxable < ($PAYE1)))
        {$PAYE_Rem = $TotTaxable; $PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);}
        */

        if ($TotTaxable >= ($PAYE1)) {
            $PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
        }

        //If Amount Left is not up to SECOND BAND (PAYE2)
        if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2)) {
            $PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
        }

        //If Amount Left is not up to THIRD BAND (PAYE3)
        if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
            $PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
            $PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
        }

        //If Amount Left is not up to FOURTH BAND (PAYE4)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
            $PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
        }

        //alert ($TotTaxable);

        //If Amount Left is not up to FIFTH BAND (PAYE5)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
            $PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
        }
        //alert ($PAYE);

        //If Amount Left is not up to SIXTH BAND (PAYE6)
        //if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)))
        //{$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5); $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

        if ($TotTaxable > $PAYE6) {
            $PAYE = $PAYE + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
        }

        //if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6))
        //{$PAYE = $PAYE + ((($TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)) * $PAYE_Comm6)/100);}
        //{$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

        //alert($PAYE);
        //alert ($PAYE);
        //alert ($Pension);

        //alert (document.getElementById("LeaveField").value);
        /*if (document.getElementById("LeaveField").value=='on') //If Leave allowance is inclusive then add Pension to the Net
        {
        	$TotAllowance = $PAYE + $Pension;
        }
        else
        {
        	$TotAllowance = $PAYE;
        }

        alert("Gross:" + $GrossPay);
        alert("Total_DR:" + $Total_DR);
        alert("PAYE:" + $PAYE);
        alert("Pension:" + $Pension);
        */

        $NetPay = ((GrossPay - $Total_DR) - $PAYE - $Pension); // - $LeaveA

        /*alert ("N="+$NetPay);
        alert ($PAYE);
        alert ($Pension);
        alert ($GrossPay);
        alert ("DR="+$Total_DR);
        alert ($TotAllowance);
        */

        document.getElementById("TTaxable").value = thousand_separator(parseFloat($TotTaxable).toFixed(2));
        document.getElementById("TRelief").value = thousand_separator(parseFloat($TRelief).toFixed(2));
        document.getElementById("PAYE").value = thousand_separator(parseFloat($PAYE).toFixed(2));
        document.getElementById("NetPay").value = thousand_separator(parseFloat($NetPay).toFixed(2));

        //document.getElementById("PensionEmployee").value = thousand_separator(parseFloat($NetPay).toFixed(2));

        /*	document.getElementById("TTaxable").value.replace(/\,/g,'') +
        	document.getElementById("PensionEmployee").value.replace(/\,/g,'') +
        	document.getElementById("NetPay").value.replace(/\,/g,'') +
        	document.getElementById("LeaveAllowance").value.replace(/\,/g,'') +
        	document.getElementById("Gross").value.replace(/\,/g,'') +
        	document.getElementById("paye_d").value.replace(/\,/g,'') +
        	document.getElementById("pension").value;
        */

        //alert (GrossPay);
        //alert (Pension);
        //alert ($PAYE);

        //NetPay = ((GrossPay -  Pension)-$PAYE); // - parseFloat(Leave));
        //NetPay = (NetPay + parseFloat(LeaveAllowance_DisMth))
        //alert(NetPay);
        /*alert ("Net="+NetPay);
        alert ("PAYE="+$PAYE);
        alert ($Pension);
        //alert ($GrossPay);
        alert ("DR="+$Total_DR);
        //alert ($TotAllowance);
        //*/

        document.getElementById("Gross").value = thousand_separator((parseFloat(GrossPay) / 12).toFixed(2));
        //document.getElementById("TTaxable").value = thousand_separator(parseFloat($TotTaxable).toFixed(2));
        document.getElementById("TRelief").value = thousand_separator(parseFloat(TRelief / 12).toFixed(2));
        //document.getElementById("PAYE").value = thousand_separator(parseFloat($PAYE).toFixed(2));
        document.getElementById("NetPay").value = thousand_separator((parseFloat(NetPay) / 12).toFixed(2));
    }

    function thousand_separator(v, s, d) {
        if (arguments.length == 2) d = ".";
        if (arguments.length == 1) {
            s = ",";
            d = ".";
        }

        v = v.toString();
        // separate the whole number and the fraction if possible
        var a = v.split(d);
        var x = a[0]; // decimal
        var y = a[1]; // fraction
        var z = "";
        var l = x.length;
        while (l > 3) {
            z = "," + x.substr(l - 3, 3) + z;
            l -= 3;
        }
        z = v.substr(0, l) + z;

        if (a.length > 1)
            z = z + d + y;
        return z;
    }

    $(function() {
        $(document).tooltip();
    });

    $(function() {
        // $("#PayMonth").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y",
        //  maxDate: "+1D",
        //  dateFormat: 'MM yy',yearRange: "-75:+75"})
        // 	$("#PayMonth").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y",
        //      maxDate: "+1D",
        //       dateFormat: 'MM yy'})
        //   });
        $("#PayMonth").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'MM yy'
        })
    });
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<style>
    h6 {
        font-weight: 550;
    }
</style>

<body oncontextmenu="return false;" onLoad="GetTotal_PAYE();" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
    <?php // echo '</td></tr></table>';
    ?>
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" onSubmit="GetTotal()">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4 col-2 text-center" style="margin-top: 1%">
                    </div>
                    <div class="col-md-4 col-8 text-center" style="margin-top: 1%">
                        <h5>
                            Employee Salary Payment
                        </h5>
                    </div>
                    <div class="col-md-4 col-2">
                        <!-- <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Clear" style="margin-top: 1%" /> -->
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php

                    if ($EditID != '' && $EditID != '--') {
                        if ($PayScheme_ !== '') {
                            $dbOpen2_ = "SELECT count(*) Ct from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                            if (ScriptRunner($dbOpen2_, "Ct") == 0) {
                                echo ("<script type='text/javascript'>{ parent.msgbox('Employee payroll group/Payment template has been unauthorized', 'red'); }</script>");
                            }
                        }

                        $CurrencyType_query = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $CurrencyType = ScriptRunner($CurrencyType_query, "CurrencyType");
                        $group_nam = ScriptRunner($CurrencyType_query, "GName");

                        $Script_Curr = "SELECT * FROM Masters WHERE HashKey='" . trim($CurrencyType) . "'";
                        $CurrName = ScriptRunner($Script_Curr, "Val1");
                        $CurrCode = ScriptRunner($Script_Curr, "ItemCode");
                        $ExchRate = ScriptRunner($Script_Curr, "Desc");
                        if (!isset($CurrName)) {
                            $currency_display = "Naira (NGR)";
                        } else {
                            $currency_display = "<b>" . $CurrName . "</b> exchange rate: 1 " . $CurrName . " = " . $ExchRate . " Naira";
                        }
                        //$currency_display = $EditID;
                        echo '<div class="col-md-6">
						<p class="">Currency Type:
						<span>' . $currency_display . '</span></p>
						</div>';

                        echo '<div class="col-md-6">
						<p class="">Payroll Group:
						<span>' . $group_nam . '</span></p>
			</div>';
                    }
                    ?>
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Select Staff <span style="color: red">*</span> :</label>
                            <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
                                <select name="EmpID" class="form-control" id="EmpID">
                                    <option value="">--</option>
                                    <?php
                                    if ($EditID != '' && $EditID != '--') {
                                        $SelID = $EmpID_;
                                    } else {
                                        $EmpID_ = '';
                                    }
                                    $dbOpen2 = "SELECT HashKey, (SName+' '+FName+' '+ONames +' ['+Convert(Varchar(24),EmpID)+']') as Nm from EmpTbl where (EmpID<>'000000' and Status in ('A','U') and EmpStatus='Active') order by SName";
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($SelID == $row2['HashKey']) {
                                            echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
                                        } else {
                                            echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
                                        }
                                    }
                                    echo '<input name="AcctNo" id="AcctNo" type="hidden" value="' . $EmpID_ . '" />';
                                    include '../login/dbClose2.php';
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Select Month:</label>
                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                <?php

                                if (strlen($EditID) == 32 && $CmdTyp == "Edit") {
                                    echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value="' . ScriptRunner($Script_Edit, "PM") . '"   autocomplete="off"  />';
                                } else {
                                    if ((isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") || !isset($_REQUEST["PayMonth"])) {
                                        echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value=""  autocomplete="off" />';
                                    } elseif (isset($_REQUEST["PayMonth"])) {

                                        echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value="' . ECh($_REQUEST["PayMonth"]) . '"  autocomplete="off" />';
                                    }
                                    if ((ValidateURths("PAY SALARY" . "A") == true)) {

                                        echo '&nbsp; <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Generate Schedule" />';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="clearfix" ></div> -->
                <?php
                $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                include '../login/dbOpen2.php';
                while ($row2 = @sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { ?>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Gross Pay</h6>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm1']; ?><span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = 'PayItem1;';
                                    $DR = 0;
                                    if ($row2['PayItemCD1'] == "CR") {
                                        $CR = $CR . 'PayItem1;';
                                    } elseif ($row2['PayItemCD1'] == "DR") {
                                        $DR = $DR + $row2['PayItem1'];
                                    }

                                    if ($row2['PayItemPen1'] == 1) {
                                        $TPension = $TPension . 'PayItem1;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        //echo $Script_Edit;
                                        //echo ScriptRunner($Script_Edit,"PayItem1");
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem1");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX1'] == 1 && $row2['PayItemAn1'] == 0) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }

                                        echo '<input type="text" name="PayItem1" id="PayItem1" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem1" id="PayItem1" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem1" id="PCent_PayItem1" value="' . $row2["PayItem1"] . '" />'; ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm1"] . ": " . $row2["PayItem1"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm3']; ?><span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = $AllItems . 'PayItem3;';
                                    if ($row2['PayItemCD3'] == "CR") {
                                        $CR = $CR . 'PayItem3;';
                                    } elseif ($row2['PayItemCD3'] == "DR") {
                                        $DR = $DR + $row2['PayItem3'];
                                    }

                                    //if ($row2['PayItemTX3']==1)
                                    //{$TTaxable=$TTaxable.'PayItem3;';}
                                    if ($row2['PayItemPen3'] == 1) {
                                        $TPension = $TPension . 'PayItem3;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem3");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX3'] == 1) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }
                                        echo '<input type="text" name="PayItem3" id="PayItem3" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem3" id="PayItem3" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem3" id="PCent_PayItem3" value="' . $row2["PayItem3"] . '" />';
                                    ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm3"] . ": " . $row2["PayItem3"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row ">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm2']; ?>
                                    <span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = $AllItems . 'PayItem2;';
                                    if ($row2['PayItemCD2'] == "CR") {
                                        $CR = $CR . 'PayItem2;';
                                    } elseif ($row2['PayItemCD2'] == "DR") {
                                        $DR = $DR + $row2['PayItem2'];
                                    }

                                    //if ($row2['PayItemTX2']==1)
                                    //{$TTaxable=$TTaxable.'PayItem2;';}
                                    if ($row2['PayItemPen2'] == 1) {
                                        $TPension = $TPension . 'PayItem2;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem2");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX2'] == 1) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }
                                        echo '<input type="text" name="PayItem2" id="PayItem2"  maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem2" id="PayItem2"  maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem2" id="PCent_PayItem2" value="' . $row2["PayItem2"] . '" />';
                                    ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm2"] . ": " . $row2["PayItem2"] . "% of Gross"; ?>" name="AuthInfo21" id="AuthInfo21">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-primary " style="padding-right: 0px; font-weight: bolder;">Gross Pay
                                    :</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php

                                    //  var_dump($Script_Edit);
                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Generate Schedule") {

                                            $re_gross = ScriptRunner($Script_Edit, "real_gross");
                                            $kval = isset($re_gross) && $re_gross !== '' ? ScriptRunner($Script_Edit, "real_gross") : ScriptRunner($Script_Edit, "Gross");
                                            // var_dump($kval);

                                        } else {

                                            $kval = ScriptRunner($Script_Edit, "Gross");
                                        }

                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $gross_for_anniversary = $kval;

                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $gross_for_anniversary = $kval;
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        }

                                        $GrossPay = $kval * 12;

                                        echo '<input type="text" name="Gross" id="Gross" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="Gross" id="Gross" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    //        echo '<input type="text" name="Gross" id="Gross" size="18" maxlength="25" class="subHeader" value="'.number_format((ScriptRunner($Script_Edit,"Gross")+0.0),2,'.',',').'" onchange="GetTotal()" />';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Allowances & Reliefs
                                <?php
                                $imgID = "MastersDepartment";
                                if ($ModMaster == true) {
                                    $imgPath = "fa fa-plus";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters_acc.php?GroupName=AllowancesReliefs&Emp=$SelID&LDb=Single&amp;Elmt=Currency',320,400,'No')";
                                } else {
                                    $imgPath = "fa fa-info";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "";
                                }
                                echo '<button type="button" class="btn btn-default  btn-xs" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
                                                                <i class="' . $imgPath . '"></i>
                                                            </button> ';
                                ?>
                            </h6>
                        </div>
                        <?php
                        //exit('stop here '.$EditID);
                        $EvOd = 0;
                        for ($i = 4; $i <= 25; $i++) {
                            // Hail Lawrence

                            if (($row2['PayItemCD' . $i] == "CR"  || $row2['PayItemCD' . $i] == "RL") && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                // if ($row2['PayItemTX'.$i]==1)
                                // {$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                                                                            <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                        ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                    <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
                                                    </td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            } // End of checking taxable and active

                            // Added condition for annual pay

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANN' && $Employed_date == $Pay_month) {
                                // echo $row2['PayItemAnType'.$i];
                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                                                                            <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of annual pay condition

                            // last month pay begins
                            $lastMonth = '12';
                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'DEC' && $lastMonth == $Pay_month) {

                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of first month pay

                            // First month pay begins
                            $firstMonth = '1';

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'JAN' && $firstMonth == $Pay_month) {

                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of last month pay

                            // Get the authorized leave type for the fellow
                            // For leave bonus

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANL' && $approved == 'A') {
                                // && $approved == 'A' && $PayBonus != 'A'
                                $AllItems = $AllItems . 'PayItem' . $i . ';';


                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                        <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of annual pay condition

                        }
                        echo '</div><hr>';
                        //------------------------------------------- Individual specific Allowance, not tied to group ---------------------------------
                        $Script = "Select * from Masters where (ItemName='AllowancesReliefs' and ItemCode='$SelID' and Status<>'D' and Val1<>'')";

                        if (trim(ScriptRunner($Script, "Val1")) != "") {

                            $dbOpen3 = ("SELECT * FROM Masters where (ItemName='AllowancesReliefs' and ItemCode='$SelID' and Status<>'D' and Val1<>'') ORDER BY Val1");
                            include '../login/dbOpen3.php';

                            echo ' <div class="row">
			                    <div class="col-md-6">
			                    <p>Employee specific monthly Allowances & Reliefs</p>';
                            $n = 1;
                            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                echo '
							<div class="form-group row">
			                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row3['Val1'] . '
			                        <span style="color: red">*</span>:</label>
			                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
			                        <input type="text" readonly name="add_value' . $n . '" id="add_value' . $n . '" maxlength="25" class="form-control" value="' . number_format($row3['Desc'], 2, '.', ',') . '">
									<input type="hidden" name="add_name' . $n . '" value="' . $row3['Val1'] . '">
			                    </div>
			                </div>
						</div>
						<div class="col-md-6"></div>
			            </div>';
                                $n++;
                            }
                        }
                        ?>
                        <!--  END OF ALLOWANCE AND RELIEF -->


                        <div class="row">
                            <!--++++++++++++++++++++++++++++++++++++++++ ANNIVERSARY BONUS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <?php
                            $Script_for_anniversary = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";

                            $anniversary_percent = Scriptrunner($Script_for_anniversary, "AnniversaryAllw");
                            $anniversary_switch = Scriptrunner($Script_for_anniversary, "AnniversaryField");

                            $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $EmpID_ . "'";
                            $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
                            $Pay_month = date("n", strtotime(ECh($_REQUEST["PayMonth"])));

                            if ($Employed_date == $Pay_month && $anniversary_switch == "on") {

                                $bonus = floatval(($gross_for_anniversary) * ($anniversary_percent / 100));
                                //$bonus = number_format($bonus,2);
                            } else {
                                $bonus = floatval(($gross_for_anniversary * ($anniversary_percent / 100)) / 12);
                                //$bonus = number_format($bonus,2);
                            }

                            ?>
                            <!--I commented out annivasary bonus  -->
                            <!-- <div class="col-md-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Anniversary Bonus
							<span style="color: red">*</span>:</label>
						<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
							<?php
                            // if ($CmdTyp=="Open"){
                            // echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value=""  />';
                            // }
                            // else{

                            // $Script_Edit ="select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='".$EditID."' and Status not in ('A','D')";
                            // $bonus=ScriptRunner($Script_Edit,"AnniversaryBonus");
                            // echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value=""  />';
                            // }
                            ?>
						</div>
					</div>
				</div> -->

                            <!-- SPLITS THE LEAVE BONUS INTO THE 12 MONTHS OF THE YEAR -->
                            <!--<div class="col-md-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label" style="padding-right: 0px;">ANNIVERSARY BONUS
							<span style="color: red">*</span>:</label>
						<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
							<?php
                            //echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value="'.number_format($bonus,2).'"  />';
                            ?>
						</div>
					</div>
				</div>-->

                            <!--+++++++++++++++++++++++++++++++++++++++++++++++++ END OF ANNIVERSARY BONUS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
                            <?php
                            ////////////////////////////////////// OVERTIME BEGINS HERE

                            if (ValidateURths("OVERTIME SETTING" . "V") == true) { // Check for Overtime Setting view right
                            ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" style="padding-right: 0px;">OVERTIME
                                            <span style="color: red">*</span>:</label>
                                        <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                            <?php

                                            $date_for_overtime = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));

                                            $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
								Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut , FA.*
								from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND Et.HashKey = '$SelID' AND CONVERT(CHAR(7),FA.RecDate,120) = '$date_for_overtime' and FA.[o_status] = 'A'  ";

                                            // var_dump($dbOpen2);
                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                                $amount_over_pay = $row2['OverTimeAmount'];

                                                $TAmtOverTime += ($amount_over_pay);
                                            } // End of While loop

                                            include '../login/dbClose2.php';

                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="overtime_now" id="overtime_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtOverTime, 2) . '" readonly/>';
                                            } else {
                                                $Script_Lateness = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                                $TAmtOverTime = ScriptRunner($Script_Lateness, "OverTimeAmount");
                                                echo '<input type="text" name="overtime_now" id="overtime_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtOverTime, 2) . '" readonly/>';
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <!-- End of Overtime form row -->
                    <?php
                            } // Close if condition for checking attendance view right
                    ?>

                    <?php if ($EvOd == 1) {
                        echo '
				<td>&nbsp;</td><td>&nbsp;</td></tr>';
                    }
                    //    }
                    ?>


                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Deductions
                                <?php
                                $imgID = "MastersDepartment";
                                if ($ModMaster == true) {
                                    $imgPath = "fa fa-plus";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters_acc.php?GroupName=Deductions&Emp=$SelID&LDb=Single&amp;Elmt=Currency',320,400,'No')";
                                } else {
                                    $imgPath = "fa fa-info";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "";
                                }
                                echo '<button type="button" class="btn btn-default  btn-xs" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
					            		<i class="' . $imgPath . '"></i>
					        		</button> ';
                                ?>
                            </h6>
                        </div>
                        <!-- Lateness form row added to the pay monthly sheet-->
                        <?php
                        if (ValidateURths("ATTENDANCE" . "V") == true) { // Check for attendance view right

                        ?>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">LATENESS
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php

                                        $date_for_late = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));

                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut , FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND Et.HashKey = '" . $SelID . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '" . $date_for_late . "'  and FA.[l_status] = 'A'";
                                        include '../login/dbOpen2.php';

                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                            $amount_incurr_pay = $row2['LatenessAmount'];

                                            $TAmtLate += ($amount_incurr_pay);
                                        } // End of While loop

                                        include '../login/dbClose2.php';

                                        if ($CmdTyp == "Open") {

                                            echo '<input type="text" name="lateness_now" id="lateness_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtLate, 2) . '" readonly/>';
                                        } else {
                                            $Script_Lateness = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                            $TAmtLate = ScriptRunner($Script_Lateness, "LatenessAmount");
                                            echo '<input type="text" name="lateness_now" id="lateness_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtLate, 2) . '" readonly/>';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End of lateness form row -->
                            <!-- ABSENT STARTS HERE -->
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">ABSENT
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $absent_month = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND FA.RecType = 'A' AND Et.HashKey = '" . $SelID . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '" . $absent_month . "' AND FA.Exempt='0' ";
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                            $Script_set = "SELECT Gross from Fin_PRCore where EmpID='" . $row2['HKey'] . "' AND Status = 'A'";
                                            $gross = ScriptRunner($Script_set, "Gross");
                                            if ($gross) {
                                                $absentAmount = $gross / 365;
                                                if ($absentAmount < 0) {
                                                    $absentAmount = 0;
                                                }
                                                number_format($absentAmount, 2);
                                                $TAmtAbsent += ($absentAmount);
                                            }
                                        } // End of While loop

                                        include '../login/dbClose2.php';

                                        if ($CmdTyp == "Open") {

                                            echo '<input type="text" name="absent" id="absent" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtAbsent, 2) . '" readonly/>';
                                        } else {
                                            $Script_Absent = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                            $TAmtAbsent = ScriptRunner($Script_Absent, "AbsentAmount");
                                            echo '<input type="text" name="absent" id="absent" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtAbsent, 2) . '" readonly/>';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- ABSENT ENDS HERE -->
                        <?php
                        } // Close if condition for checking attendance view right

                        ?>
                        <?php
                        $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $row2 = Scriptrunnercous($dbOpen2);

                        $EvOd = 0;
                        for ($i = 4; $i <= 25; $i++) {
                            if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
                                $AllItems = $AllItems . 'PayItem' . $i . ';';
                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                    if ($row2['PayItemTX' . $i] == 0) {
                                        $TDR_NTaxable = $TDR_NTaxable . 'PayItem' . $i . ';';
                                    }
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}
                                //if ($row2['PayItemPen'.$i]==1)
                                //{$TPension=$TPension.'PayItem'.$i.';';}

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) //LOAN repayment
                                    {
                                        if ($i == 7) {
                                            // var_dump('here');
                                            /*
                        $Script_Loan="Update LnIndvOff set Status='B', Paid=ExpPay where EID='".$EmpID_."' and Status='A' and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))";
                        ScriptRunnerUD($Script_Loan,"Sm");
                        //on authorization UPDATE outstanding Payment in LnDetails table

                        $Script_Loan="Select ExpPay from LnIndvOff where EID='".$EmpID_."' and Status in ('B') and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))"; //Status 'B' menas BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid

                        //                $Script_Loan="Select ExpPay from LnIndvOff where EID='".$EmpID_."' and Status='A' and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))";
                        //echo $Script_Loan;

                        $LoanAmt=ScriptRunner($Script_Loan,"ExpPay");
                         */

                                            if (is_numeric($LoanAmt)) {
                                                $kval = number_format($LoanAmt, 2, '.', ',');
                                            } else {
                                                $kval = "0.00";
                                            }
                                        } else {
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '			<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($i != 5) {
                                        //Loan is not taxable
                                        //if ($row2['PayItemTX'.$i]==1)
                                        //{$TTaxable=$TTaxable + floatval(str_replace(",","",$kval));}
                                    }
                        ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">';
                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }

                                        if ($row2['PayItemTX' . $i] == 1) {
                                            $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                        <?php echo '</div></div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                        }

                        //--------------------------------------------------------------- Individual specific Deductions, not tied to group ---------------------------------
                        $Script = "Select * from Masters where (ItemName='Deductions' and ItemCode='$SelID' and Status<>'D' and Val1<>'')";

                        if (trim(ScriptRunner($Script, "Val1")) != "") {

                            $dbOpen3 = ("SELECT * FROM Masters where (ItemName='Deductions' and ItemCode='$SelID' and Status<>'D' and Val1<>'') ORDER BY Val1");
                            include '../login/dbOpen3.php';
                            echo ' <div class="row">
					                    <div class="col-lg-md-6">
					                    <p> Employee specific monthly Deductions</p>';
                            $m = 1;
                            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                echo '
						<div class="form-group row">
			                    <label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row3['Val1'] . '
			                        <span style="color: red">*</span>:</label>
			                    <div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">
			                        <input type="text" readonly name="remove_value' . $m . '" id="remove_value' . $m . '" size="25" maxlength="25" class="form-control" value="' . number_format($row3['Desc'], 2, '.', ',') . '">
									<input type="hidden" name="remove_name' . $m . '" value="' . $row3['Val1'] . '">
			                    </div>
			                </div>
						</div>
						<div class="col-md-6"></div>
			            </div><hr>';
                                $m++;
                            }
                        }

                        //LOAN EXTENSION
                        /*
                                                     $dbOpen7=("SELECT (select SUM(Paid) from LnIndvOff Where LID=Ld.HashKey and (convert(varchar(11),AddedDate)=convert(varchar(11),Ld.AddedDate)) and Status<>'D' Group By convert(varchar(11),AddedDate), EID, LID) Pd, Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, Convert(Varchar(11),AuthDate,106) AuthDt, * from LnDetails Ld where EID='".ECh($_REQUEST["EmpID"])."' and Status not in ('D') order by SRepayDate asc");
                                                                 */
                        if ($_REQUEST["FAction"] === "View Selected") {
                            if (isset($loan_amt)) {
                                echo ' <div class="row">
			                <div class="col-lg-12 col-md-6">
			                <p class"text-danger"><b>Loan Repayment</b></p>';
                                echo '
							<div class="form-group row">
				                    <div class="col-md-7 input-group input-group-md" style=" padding-right: 45px">
				                        <input type="text" readonly name="repay_loan" id="" size="25" maxlength="25" class="form-control" value="' . number_format($loan_amt, 2) . '">
				                    </div>
				                </div>
							</div>
							<div class="col-md-6"></div>
							</div><hr>';
                            }
                        } else {
                            $Script = "Select Count(*) Ct  from [LnDetails] where Status ='A' and LnAmt <> Paid and EID='" . $EmpID_ . "'";
                            if (ScriptRunner($Script, "Ct") > 0) {
                                //$date_paid_for = "2017-02-28";
                                //$dbOpen7=("SELECT (select (convert(varchar(11),ExpPayDate)=convert(varchar(11),$date_paid_for)), * from LnIndvOff Where LID=Ld.HashKey from LnDetails Ld where EID='".$EmpID_."' and Status not in ('D')");

                                $dbOpen7 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, * from LnIndvOff  where EID='" . $EmpID_ . "' and Status not in ('D','P','B') Order by ExpPayDate ASC");

                                // var_dump($dbOpen7);

                                include '../login/dbOpen7.php';

                                //$LnAmt = 0;
                                //$ExpPay =0;

                                //$OutSPay2= 0;
                                //$RePayAmt2 = 0;

                                echo ' <div class="row">
			                <div class="col-lg-12 col-md-6">
			                <p class"text-danger"><b>Loan Repayment</b></p>';
                                //$m=1;
                                //while($row7 = sqlsrv_fetch_array($result7,SQLSRV_FETCH_BOTH)){
                                $row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH);
                                //$OutSPay2 = $OutSPay2 + $row7['OutSPay'];
                                //$RePayAmt2 = $RePayAmt2 + $row7['RePayAmt'];
                                $ExpPay2 = $row7['ExpPay'];
                                $EID = $row7['EID'];
                                $LID = $row7['LID'];
                                $ExpPDate = $row7['ExpPD'];
                                $HashKey = $row7['HashKey'];

                                echo '
							<div class="form-group row">
								<input type="hidden" name="_pay_date" value="' . $ExpPDate . '">
								<input type="hidden" name="_ind_loan_id" value="' . $HashKey . '">
								<input type="hidden" name="_emp_hashkey" value="' . $EID . '">
				                    <label class="col-md-5 col-form-label" style="padding-right: 0px;">' . $ExpPDate . '</label>
				                    <div class="col-md-7 input-group input-group-md" style=" padding-right: 45px">
				                        <input type="text" readonly name="repay_loan" id="" size="25" maxlength="25" class="form-control" value="' . number_format($ExpPay2, 2) . '">
				                    </div>
				                </div>
							</div>
							<div class="col-md-6"></div>
							</div><hr>';

                                //$m++;
                                //}
                            }
                        }
                        //---------------------------------------------------------------------------------------------------------------------------------------------------

                        if ($EvOd == 1) {
                            echo '<td>&nbsp;</td><td>&nbsp;</td></tr>';
                        }
                        ?>
                        <?php
                        $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $row2 = Scriptrunnercous($dbOpen2);
                        ?>
                        <?php if ((int) $row2["PayItemOF20"] === 1 || (int) $row2["PayItemOF19"] === 1) : ?>

                            <div class="row" style="width: 100%;">
                                <div class="col-sm-12 text-center">
                                    <h6 class="page-header">Free Pay</h6>
                                </div>
                                <?php if ((int) $row2["PayItemOF20"] === 1) : ?>
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;"> <?php echo $row2['PayItemNm20'] ?>
                                                <span style="color: red">*</span>:</label>
                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                <?php
                                                if ($EditID != "" || $EditID > 0) {
                                                    $kval = 0;
                                                    $kval = ScriptRunner($Script_Edit, "PayItem20");
                                                    // $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                                    if ($kval > 0) {
                                                        if ($CmdTyp == "Open") {
                                                            echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        } else {
                                                            echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        }
                                                    } else {
                                                        echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                    }
                                                } else {

                                                    echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>









                                <?php if ((int) $row2["PayItemOF19"] === 1) : ?>
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;"> <?php echo $row2['PayItemNm19'] ?>
                                                <span style="color: red">*</span>:</label>
                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                <?php
                                                if ($EditID != "" || $EditID > 0) {
                                                    $kval = 0;
                                                    $kval = ScriptRunner($Script_Edit, "PayItem19");
                                                    // $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                                    if ($kval > 0) {
                                                        if ($CmdTyp == "Open") {
                                                            echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        } else {
                                                            echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        }
                                                    } else {
                                                        echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                    }
                                                } else {

                                                    echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>





                            </div>

                        <?php endif; ?>



                        <div class="row" style="width: 100%;">
                            <div class="col-sm-12 text-center">
                                <h6 class="page-header">Net Details</h6>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Taxable Pay
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        if ($EditID != "" || $EditID > 0) {
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "TTaxable");
                                            $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                } else {
                                                    echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                }
                                            } else {
                                                echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        } else {

                                            echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        echo '<input type="hidden" name="TTaxable_H" id="TTaxable_H" size="25" maxlength="25" class="form-control" value="' . $TTaxable . '" onchange="GetTotal_PAYG()" />';

                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">PAYE
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PAYE");

                                        //print $kval;
                                        //exit();

                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                //$kval = $kval + $LeaveAllw;
                                                echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Total Relief:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        if ($CmdTyp != "Open") // Update Button active
                                        {
                                            if ($EditID != "" || $EditID > 0) {
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TRelief") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                $GrossPay = $GrossPay + $OTimeAmtPay;

                                                echo '<input type="text" name="GrossPay_" id="GrossPay_" size="25" maxlength="25" class="form-control" value="' . $GrossPay . '" onchange="GetTotal()" />';
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        } else { // Create button active
                                            if ($EditID != "" || $EditID > 0) {

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TRelief") / 12 + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {

                                                $GrossPay = $GrossPay + $OTimeAmtPay;
                                                echo '<input type="text" name="GrossPay_" id="GrossPay_" size="25" maxlength="25" class="form-control" value="' . $GrossPay . '" onchange="GetTotal()" />';
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Allowance
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "LeaveAllowance");
                                        if ($CmdTyp != "Open") // Update Button active
                                        {

                                            //++++++++++++++++++++ START +++++++++++++++
                                            echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class=" form-control TextBoxText_Medium_Comp" value="' . number_format($kval, 2, '.', ',') . '" onchange="GetTotal()" />';

                                            //++++++++++++++++++++ END +++++++++++++++++

                                        } else //Create button active
                                        {
                                            $kval = $kval / 12; //Reading annual leave figure. Divide by 12 to get the monthly leave figure
                                            $kval_div = $kval;
                                            $PayMonth = '27 ' . ECh($_REQUEST["PayMonth"]);

                                            //++++++++++++++++++++ START +++++++++++++++
                                            /*
        echo '<input type="hidden" name="LeaveAllowance_H" id="LeaveAllowance_H" size="25" maxlength="25" class="TextBoxText_Medium_Comp" value="'.$kval.'" />';
         */

                                            //Calculate if Leave Allowance is in or Out
                                            $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";
                                            if (ScriptRunner($Script_TAX, "LeaveField") == 'on') {
                                                $LStatus = 'A';
                                            } else {
                                                $LStatus = 'N';
                                            }

                                            //This area checks if a leave allowance is raised for the month and if inclusive, then add automatically else if exclusive, then wait for it to be authorized and then add.
                                            $Script_Leave = "Select count(*) Ct from Fin_LvAllw where (Status not in ('D','P')) and EmpID='" . $EmpID_ . "' and (Month(PayMonth) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(PayMonth) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))"; //Status 'B' menas BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid
                                            if (ScriptRunner($Script_Leave, "Ct") > 0) {
                                                $Script_Leave = "Update Fin_LvAllw set AmtMonthly=" . $kval . ", Status='" . $LStatus . "',[AddedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[AddedDate]=GetDate() where (Status not in ('D','P')) and EmpID='" . $EmpID_ . "' and (Month(PayMonth) = MONTH('" . ECh($PayMonth) . "') and YEAR(PayMonth) = YEAR('" . ECh($PayMonth) . "'))";
                                                ScriptRunnerUD($Script_Leave, "");
                                            } else {
                                                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                                                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "LeaveAllw" . $_SESSION["StkTck" . "UName"];
                                                $HashKey__ = md5($UniqueKey);

                                                $Script_Leave = "insert into Fin_LvAllw (EmpID,PayMonth,AmtMonthly,[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $EmpID_ . "','" . ECh($PayMonth) . "'," . $kval . ",'" . $LStatus . "','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey__ . "')";
                                                ScriptRunnerUD($Script_Leave, "");
                                            }

                                            //Block out leave authorized for payment for the month
                                            $Script_Leave = "Update Fin_LvAllw set Status='B' where (Status in ('A')) and EmpID='" . $EmpID_ . "'"; // and (Month(PayMonth) = MONTH('".ECh($PayMonth)."') and YEAR(PayMonth) = YEAR('".ECh($PayMonth)."'))";

                                            // var_dump($Script_Leave);
                                            ScriptRunnerUD($Script_Leave, "");

                                            //Calculate leave allowance to be paid
                                            $Script_Leave = "Select Sum(AmtMonthly) LvPay from Fin_LvAllw where (Status in ('B')) and EmpID='" . $EmpID_ . "'";
                                            $kval = ScriptRunner($Script_Leave, "LvPay");
                                            // $LeaveAllw = $kval; //Save the value of the leave to add to the NET Payable for the month
                                            $LeaveAllw = $kval_div; //Save the value of the leave to add to the NET Payable for the month

                                            //if ($kval > 0)
                                            if ($kval_div > 0) {
                                                echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="' . number_format($kval_div, 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                            //++++++++++++++++++++ END +++++++++++++++++
                                        }
                                        ?>
                                    </div>

                                    <!-- ++++++++++++++++++++ OverTime Payment +++++++++++++++++  	-->


                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder;">Pension:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $ScriptPension = "select * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "')";

                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PensionEmployee");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }

                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PensionEmployer");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="0.00" onchange="GetTotal()" />';
                                        }

                                        echo '<input type="hidden" name="PCent_PensionEmployee" id="PCent_PensionEmployee" value="' . number_format(ScriptRunner($ScriptPension, "PensionEmployee"), 2, '.', ',') . '" />
			        			<input type="hidden" name="PCent_PensionEmployer" id="PCent_PensionEmployer" value="' . number_format(ScriptRunner($ScriptPension, "PensionEmployer"), 2, '.', ',') . '" />';
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Net Pay:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "NetPay");

                                        if ($kval > 0) {

                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="' . number_format((($kval / 12) - $ExpPay2 - $LoanAmt - $TAmtLate - $TAmtAbsent + $LeaveAllw + $TAmtOverTime + (float) $total_bonus), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";
                                //Get % tax band values
                                echo '
						<input type="hidden" name="S_PAYE0" id="S_PAYE0" value="' . ScriptRunner($Script_TAX, "Limit0") . '">
						<input type="hidden" name="S_PAYE1" id="S_PAYE1" value="' . ScriptRunner($Script_TAX, "Limit1") . '">
						<input type="hidden" name="S_PAYE2" id="S_PAYE2" value="' . ScriptRunner($Script_TAX, "Limit2") . '">
						<input type="hidden" name="S_PAYE3" id="S_PAYE3" value="' . ScriptRunner($Script_TAX, "Limit3") . '">
						<input type="hidden" name="S_PAYE4" id="S_PAYE4" value="' . ScriptRunner($Script_TAX, "Limit4") . '">
						<input type="hidden" name="S_PAYE5" id="S_PAYE5" value="' . ScriptRunner($Script_TAX, "Limit5") . '">
						<input type="hidden" name="S_PAYE6" id="S_PAYE6" value="' . ScriptRunner($Script_TAX, "Limit6") . '">
						<input type="hidden" name="S_PAYE7" id="S_PAYE7" value="' . ScriptRunner($Script_TAX, "Limit7") . '">
						<input type="hidden" name="H_Leave" id="H_Leave" value="' . ScriptRunner($Script_TAX, "LeaveAllw") . '">';

                                //Get % Percent values
                                echo '
						<input type="hidden" name="S_PAYE_Comm0" id="S_PAYE_Comm0" value="' . ScriptRunner($Script_TAX, "pcent0") . '">
						<input type="hidden" name="S_PAYE_Comm1" id="S_PAYE_Comm1" value="' . ScriptRunner($Script_TAX, "pcent1") . '">
						<input type="hidden" name="S_PAYE_Comm2" id="S_PAYE_Comm2" value="' . ScriptRunner($Script_TAX, "pcent2") . '">
						<input type="hidden" name="S_PAYE_Comm3" id="S_PAYE_Comm3" value="' . ScriptRunner($Script_TAX, "pcent3") . '">
						<input type="hidden" name="S_PAYE_Comm4" id="S_PAYE_Comm4" value="' . ScriptRunner($Script_TAX, "pcent4") . '">
						<input type="hidden" name="S_PAYE_Comm5" id="S_PAYE_Comm5" value="' . ScriptRunner($Script_TAX, "pcent5") . '">
						<input type="hidden" name="S_PAYE_Comm6" id="S_PAYE_Comm6" value="' . ScriptRunner($Script_TAX, "pcent6") . '">
						<input type="hidden" name="S_PAYE_Comm7" id="S_PAYE_Comm7" value="' . ScriptRunner($Script_TAX, "pcent7") . '">';

                                echo '
						<input name="H_TTaxable" id="H_TTaxable" type="hidden" value="' . $TTaxable . '" />
						<input name="H_TDR_NTaxable" id="H_TDR_NTaxable" type="hidden" value="' . $TDR_NTaxable . '" />
						<input name="H_CR" id="H_CR" type="hidden" value="' . $CR . '">
						<input name="H_DR" id="H_DR" type="hidden" value="' . $DR . '">
						<input name="H_ALL" id="H_ALL" type="hidden" value="' . $AllItems . '">
						<input name="H_Pension" id="H_Pension" type="hidden" value="' . $TPension . '">';
                                ?>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-2">
                                        <div class="demo-checkbox">
                                            <?php if (ScriptRunner($Script_Edit__, "AuthCalc") == 'on') : ?>
                                                <input type="checkbox" checked name="AuthCalc" id="AuthCalc">
                                            <?php else : ?>
                                                <input type="checkbox" name="AuthCalc" id="AuthCalc">
                                            <?php endif ?>
                                            <!-- <label for="AuthCalc">
                                                <button type="button" class="btn btn-default btn-sm" style="line-height: 17px" Title="Check if you want a request for authorization sent" name="AuthInfo" id="AuthInfo2" onClick="Popup.show('simplediv');return false;">
                                                    <i class="fa fa-info"></i>
                                                </button>
                                            </label> -->
                                        </div>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <?php
                                        if (ValidateURths("PAY SALARY" . "T") == true) {
                                            if ($CmdTyp == "Open" && strlen($EditID) == 32) {

                                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Payment" />';
                                            } else {
                                                // echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update Payment" />';
                                                // echo '<input name="UpdID" type="hidden" id="UpdID" value="' . ECh($EditID) . '" />';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <br>
                    <hr style="margin-bottom: 1.0rem">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
                            <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" /> -->
                            <?php //print $_REQUEST["FAction"]; 
                            ?>
                        </div>
                    </div>
                    <hr style="margin-top: 1.0rem;margin-bottom: .5rem">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php
                            if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {

                                // print "<h4>UNAUTHORIZED</h4>";
                                $dbOpen2 = ("SELECT (DateName(MM,Fm.PayMonth)+' '+CONVERT(varchar(4),datepart(YEAR,Fm.PayMonth))) Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where Em.HashKey=Fm.EmpID and (Fm.Status in ('N','U')) and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
                            } else {
                                // print "<h4>AUTHORIZED</h4>";
                                $dbOpen2 = ("SELECT (DateName(MM,Fm.PayMonth)+' '+CONVERT(varchar(4),datepart(YEAR,Fm.PayMonth))) Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+'  ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where Em.HashKey=Fm.EmpID and (Fm.Status = 'A') and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
                            }
                            ?>
                        </div>
                        
                        </div>
                        <div class="col-md-6 pt-5">
                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <?php
                                    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								  	  <input name="FAction" id="FAction" type="hidden" />
									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                    //Check if any record was spolled at all before enabling buttons

                                    if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                                        //Check if user has Delete rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Delete'; include '../main/buttons.php';
                                        //Check if user has Add/Edit rights
                                        $but_HasRth = ("PAY SALARY" . "V");
                                        $but_Type = 'View';
                                        include '../main/buttons.php';
                                        //Check if user has Authorization rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Authorize'; include '../main/buttons.php';
                                    } else { // AUTHORIZE
                                        //Check if user has Delete rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Delete'; include '../main/buttons.php';
                                        //Check if user has Add/Edit rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'View'; include '../main/buttons.php';
                                        //Check if user has Authorization rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Unauthorize'; include '../main/buttons.php';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
            </div>
        </div>
    </form>
</body>
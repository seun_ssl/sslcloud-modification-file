<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

if (ValidateURths("PAY SALARY" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$ModMaster = ValidateURths("PAYROLL MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($Script)) {
    $Script = "";
}
if (!isset($SelID)) {
    $SelID = "";
}
//if (!isset($Dt)) {$Dt="";}

$LoanAmt = 0;
$PaySlip = $CmdTyp = "";
$CR = "";
$DR = "";
$TTaxable = "";
$TPension = "";
$AllItems = "";
$GoValidate = true;
$TTaxable = 0;
$TDR_NTaxable = 0;
$TAmtLate = 0; // Variable to hold total amount to be subtracted for lateness
$TAmtAbsent = 0; // Variable to hold total amount to be subtracted for being absent
$TAmtOverTime = 0; // Variable to hold total amount to be added for overtime
//$gross_for_anniversary=0; // Total gross to get anniversary bonus
//$bonus=0;        // Total anniversary bonus
$ExpPay2 = 0;
$total_bonus = 0;

$Script = "Select * from Fin_PRCore where HashKey='zzz'";
$dbOpen2 = ("SELECT * from Fin_PRSettings where HashKey='zzz'");
$EmpID_ = "";
$PayScheme_ = "";

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Generate Schedule") {
        if (ValidateURths("PAY SALARY" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        include '../main/clr_val.php'; /* CLEARS $EditID */
        if (strlen($_REQUEST["EmpID"]) > 2) {
            $EditID = ""; //ECh($_REQUEST["EmpID"]);
            $EmpID_ = ECh($_REQUEST["EmpID"]);

            $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $EmpID_ . "'";
            $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
            $Pay_month = date("n", strtotime(ECh($_REQUEST["PayMonth"])));

            $script_lvType = "SELECT SetValue11 FROM Settings WHERE Setting = 'LeaveTyp'";
            $lvType = Scriptrunner($script_lvType, "SetValue11");
            // 0b23845da0e280008e5b9c7a2adfb462
            $script_applied_leave = "SELECT *  FROM LvIndTOff WHERE LType = '0b23845da0e280008e5b9c7a2adfb462'
AND EmpID='" . $EmpID_ . "' AND [Status] = 'A' AND Year(CONVERT(char(11),LvDate,106)) = YEAR(CONVERT(char(11),GETDATE(),106))
ORDER BY ApprovedDate ASC";

            $approved = Scriptrunner($script_applied_leave, "Status");
            $script_bonus_pay = "SELECT TOP(1) * FROM Fin_PRIndvPay
WHERE Year(CONVERT(char(11),PayMonth,106)) = YEAR(CONVERT(char(11),GETDATE(),106)) AND [Status] = 'A'
AND EmpID='" . $EmpID_ . "'
ORDER BY  PayMonth ASC";
            $PayBonus = Scriptrunner($script_bonus_pay, "payStatusBonus");

            if ($_REQUEST["PayMonth"] == '') // Request for a month for which record is to be created for
            {
                $Script_Edit = "select * from [Fin_PRIndvPay] where EmpID='z'";
                echo ("<script type='text/javascript'>{ parent.msgbox('Select a month to generate an employee a payment for', 'blue'); }</script>");
            } else //Select Existing Record
            {
                $Script_Edit = "select count(*) Ct from [Fin_PRIndvPay] where EmpID='" . $EmpID_ . "' and PayMonth between '01 " . ECh($_REQUEST["PayMonth"]) . "' and '28 " . ECh($_REQUEST["PayMonth"]) . "' and Status<>'D'";
                if (ScriptRunner($Script_Edit, "Ct") > 0) {
                    $Script_Edit = "select * from [Fin_PRIndvPay] where EmpID='z'";
                    echo ("<script type='text/javascript'>{ parent.msgbox('A payment record already exist for this employee for the selected month. Edit existing.', 'red'); }</script>");
                } else {
                    $Script_Edit = "select Count(*) Ct from [Fin_PRCore] where EmpID='" . $EmpID_ . "' and Status='A'";
                    if (ScriptRunner($Script_Edit, "Ct") > 0) {
                        $Script_Edit = "select * from [Fin_PRCore] where EmpID='" . $EmpID_ . "' and Status='A'";
                        $EditID = ScriptRunner($Script_Edit, "HashKey");
                        $Script_Edit__ = "select * from [Fin_PRCore] where EmpID='" . $EmpID_ . "' and Status='A'";
                        $CmdTyp = "Open";
                        //$EmpID_=ECh($_REQUEST["EmpID"]);
                        $PayScheme_ = ScriptRunner($Script_Edit__, "Scheme");
                    } else //this is the annual tie limit
                    {
                        $Script_Edit = "select * from [Fin_PRIndvPay] where EmpID='z' and Status='z'";
                        echo ("<script type='text/javascript'>{ parent.msgbox('No authorized master payment record exist for this employee.', 'red'); }</script>");
                    }
                }
            }
        }
    } elseif ((isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Payment")
        && (isset($_REQUEST["EmpID"])
            && strlen(trim($_REQUEST["EmpID"])) == 32)
        && isset($_REQUEST["PayMonth"])

    ) {

        if (ValidateURths("PAY SALARY" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        // var_dump("here1");
        // die();
        $Script = "Select HashKey, ([SName]+' '+[FName]+' '+[ONames]+' ['+Convert(varchar(24),Et.EmpID)+']') as EmpPID from EmpTbl Et where Et.HashKey='" . ECh($_REQUEST["EmpID"]) . "' and Et.Status in ('A','U') and Et.EmpStatus='Active'";

        $EmpNm = ScriptRunner($Script, "EmpPID");
        $EmpPID = $_REQUEST["AcctNo"];

        if (trim($EmpPID) == '') {
            echo ("<script type='text/javascript'>{ parent.msgbox('The selected employee does not exist as an active employee. Please review.', 'red'); }</script>");
            $GoValidate = false;
        }

        $Script = "Select Count(*) Ct from [Fin_PRIndvPay] where EmpID='" . $EmpPID . "' and PayMonth between '01 " . $_REQUEST["PayMonth"] . "' and (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'01 " . $_REQUEST["PayMonth"] . "')+1,0))) and Status in ('A','N','U')";

        if (ScriptRunner($Script, "Ct") == 0 && $GoValidate == true) {

            if (isset($_REQUEST["PayItem1"])) {
                if ($_REQUEST["PayItem1"] == "") {
                    $PayItem1 = 0.0;
                } else {
                    $PayItem1 = $_REQUEST["PayItem1"];
                }
                $v_PayItem1 = number_format(str_replace(',', '', trim($PayItem1)), '2', '.', '');
            } else {
                $v_PayItem1 = 0.0;
            }

            if (isset($_REQUEST["PayItem2"])) {
                if ($_REQUEST["PayItem2"] == "") {
                    $PayItem2 = 0.0;
                } else {
                    $PayItem2 = $_REQUEST["PayItem2"];
                }
                $v_PayItem2 = number_format(str_replace(',', '', trim($PayItem2)), '2', '.', '');
            } else {
                $v_PayItem2 = 0.0;
            }

            if (isset($_REQUEST["PayItem3"])) {
                if ($_REQUEST["PayItem3"] == "") {
                    $PayItem3 = 0.0;
                } else {
                    $PayItem3 = $_REQUEST["PayItem3"];
                }
                $v_PayItem3 = number_format(str_replace(',', '', trim($PayItem3)), '2', '.', '');
            } else {
                $v_PayItem3 = 0.0;
            }

            if (isset($_REQUEST["PayItem4"])) {
                if ($_REQUEST["PayItem4"] == "") {
                    $PayItem4 = 0.0;
                } else {
                    $PayItem4 = $_REQUEST["PayItem4"];
                }
                $v_PayItem4 = number_format(str_replace(',', '', trim($PayItem4)), '2', '.', '');
            } else {
                $v_PayItem4 = 0.0;
            }

            if (isset($_REQUEST["PayItem5"])) {
                if ($_REQUEST["PayItem5"] == "") {
                    $PayItem5 = 0.0;
                } else {
                    $PayItem5 = $_REQUEST["PayItem5"];
                }
                $v_PayItem5 = number_format(str_replace(',', '', trim($PayItem5)), '2', '.', '');
            } else {
                $v_PayItem5 = 0.0;
            }

            if (isset($_REQUEST["PayItem6"])) {
                if ($_REQUEST["PayItem6"] == "") {
                    $PayItem6 = 0.0;
                } else {
                    $PayItem6 = $_REQUEST["PayItem6"];
                }
                $v_PayItem6 = number_format(str_replace(',', '', trim($PayItem6)), '2', '.', '');
            } else {
                $v_PayItem6 = 0.0;
            }

            //-------------------------------------------------------
            if (isset($_REQUEST["PayItem7"])) {
                if ($_REQUEST["PayItem7"] == "") {
                    $PayItem7 = 0.0;
                } else {
                    $PayItem7 = $_REQUEST["PayItem7"];
                }
                $v_PayItem7 = number_format(str_replace(',', '', trim($PayItem7)), '2', '.', '');
            } else {
                $v_PayItem7 = 0.0;
            }

            if (isset($_REQUEST["PayItem8"])) {
                if ($_REQUEST["PayItem8"] == "") {
                    $PayItem8 = 0.0;
                } else {
                    $PayItem8 = $_REQUEST["PayItem8"];
                }
                $v_PayItem8 = number_format(str_replace(',', '', trim($PayItem8)), '2', '.', '');
            } else {
                $v_PayItem8 = 0.0;
            }

            if (isset($_REQUEST["PayItem9"])) {
                if ($_REQUEST["PayItem9"] == "") {
                    $PayItem9 = 0.0;
                } else {
                    $PayItem9 = $_REQUEST["PayItem9"];
                }
                $v_PayItem9 = number_format(str_replace(',', '', trim($PayItem9)), '2', '.', '');
            } else {
                $v_PayItem9 = 0.0;
            }

            if (isset($_REQUEST["PayItem10"])) {
                if ($_REQUEST["PayItem10"] == "") {
                    $PayItem10 = 0.0;
                } else {
                    $PayItem10 = $_REQUEST["PayItem10"];
                }
                $v_PayItem10 = number_format(str_replace(',', '', trim($PayItem10)), '2', '.', '');
            } else {
                $v_PayItem10 = 0.0;
            }

            if (isset($_REQUEST["PayItem11"])) {
                if ($_REQUEST["PayItem11"] == "") {
                    $PayItem11 = 0.0;
                } else {
                    $PayItem11 = $_REQUEST["PayItem11"];
                }
                $v_PayItem11 = number_format(str_replace(',', '', trim($PayItem11)), '2', '.', '');
            } else {
                $v_PayItem11 = 0.0;
            }

            if (isset($_REQUEST["PayItem12"])) {
                if ($_REQUEST["PayItem12"] == "") {
                    $PayItem12 = 0.0;
                } else {
                    $PayItem12 = $_REQUEST["PayItem12"];
                }
                $v_PayItem12 = number_format(str_replace(',', '', trim($PayItem12)), '2', '.', '');
            } else {
                $v_PayItem12 = 0.0;
            }

            if (isset($_REQUEST["PayItem13"])) {
                if ($_REQUEST["PayItem13"] == "") {
                    $PayItem13 = 0.0;
                } else {
                    $PayItem13 = $_REQUEST["PayItem13"];
                }
                $v_PayItem13 = number_format(str_replace(',', '', trim($PayItem13)), '2', '.', '');
            } else {
                $v_PayItem13 = 0.0;
            }

            if (isset($_REQUEST["PayItem14"])) {
                if ($_REQUEST["PayItem14"] == "") {
                    $PayItem14 = 0.0;
                } else {
                    $PayItem14 = $_REQUEST["PayItem14"];
                }
                $v_PayItem14 = number_format(str_replace(',', '', trim($PayItem14)), '2', '.', '');
            } else {
                $v_PayItem14 = 0.0;
            }

            if (isset($_REQUEST["PayItem15"])) {
                if ($_REQUEST["PayItem15"] == "") {
                    $PayItem15 = 0.0;
                } else {
                    $PayItem15 = $_REQUEST["PayItem15"];
                }
                $v_PayItem15 = number_format(str_replace(',', '', trim($PayItem15)), '2', '.', '');
            } else {
                $v_PayItem15 = 0.0;
            }

            if (isset($_REQUEST["PayItem16"])) {
                if ($_REQUEST["PayItem16"] == "") {
                    $PayItem16 = 0.0;
                } else {
                    $PayItem16 = $_REQUEST["PayItem16"];
                }
                $v_PayItem16 = number_format(str_replace(',', '', trim($PayItem16)), '2', '.', '');
            } else {
                $v_PayItem16 = 0.0;
            }

            if (isset($_REQUEST["PayItem17"])) {
                if ($_REQUEST["PayItem17"] == "") {
                    $PayItem17 = 0.0;
                } else {
                    $PayItem17 = $_REQUEST["PayItem17"];
                }
                $v_PayItem17 = number_format(str_replace(',', '', trim($PayItem17)), '2', '.', '');
            } else {
                $v_PayItem17 = 0.0;
            }

            if (isset($_REQUEST["PayItem18"])) {
                if ($_REQUEST["PayItem18"] == "") {
                    $PayItem18 = 0.0;
                } else {
                    $PayItem18 = $_REQUEST["PayItem18"];
                }
                $v_PayItem18 = number_format(str_replace(',', '', trim($PayItem18)), '2', '.', '');
            } else {
                $v_PayItem18 = 0.0;
            }

            if (isset($_REQUEST["PayItem19"])) {
                if ($_REQUEST["PayItem19"] == "") {
                    $PayItem19 = 0.0;
                } else {
                    $PayItem19 = $_REQUEST["PayItem19"];
                }
                $v_PayItem19 = number_format(str_replace(',', '', trim($PayItem19)), '2', '.', '');
            } else {
                $v_PayItem19 = 0.0;
            }

            if (isset($_REQUEST["PayItem20"])) {
                if ($_REQUEST["PayItem20"] == "") {
                    $PayItem20 = 0.0;
                } else {
                    $PayItem20 = $_REQUEST["PayItem20"];
                }
                $v_PayItem20 = number_format(str_replace(',', '', trim($PayItem20)), '2', '.', '');
            } else {
                $v_PayItem20 = 0.0;
            }

            if (isset($_REQUEST["PayItem21"])) {
                if ($_REQUEST["PayItem21"] == "") {
                    $PayItem21 = 0.0;
                } else {
                    $PayItem21 = $_REQUEST["PayItem21"];
                }
                $v_PayItem21 = number_format(str_replace(',', '', trim($PayItem21)), '2', '.', '');
            } else {
                $v_PayItem21 = 0.0;
            }

            if (isset($_REQUEST["PayItem22"])) {
                if ($_REQUEST["PayItem22"] == "") {
                    $PayItem22 = 0.0;
                } else {
                    $PayItem22 = $_REQUEST["PayItem22"];
                }
                $v_PayItem22 = number_format(str_replace(',', '', trim($PayItem22)), '2', '.', '');
            } else {
                $v_PayItem22 = 0.0;
            }

            if (isset($_REQUEST["PayItem23"])) {
                if ($_REQUEST["PayItem23"] == "") {
                    $PayItem23 = 0.0;
                } else {
                    $PayItem23 = $_REQUEST["PayItem23"];
                }
                $v_PayItem23 = number_format(str_replace(',', '', trim($PayItem23)), '2', '.', '');
            } else {
                $v_PayItem23 = 0.0;
            }

            if (isset($_REQUEST["PayItem24"])) {
                if ($_REQUEST["PayItem24"] == "") {
                    $PayItem24 = 0.0;
                } else {
                    $PayItem24 = $_REQUEST["PayItem24"];
                }
                $v_PayItem24 = number_format(str_replace(',', '', trim($PayItem24)), '2', '.', '');
            } else {
                $v_PayItem24 = 0.0;
            }

            if (isset($_REQUEST["PayItem25"])) {
                if ($_REQUEST["PayItem25"] == "") {
                    $PayItem25 = 0.0;
                } else {
                    $PayItem25 = $_REQUEST["PayItem25"];
                }
                $v_PayItem25 = number_format(str_replace(',', '', trim($PayItem25)), '2', '.', '');
            } else {
                $v_PayItem25 = 0.0;
            }

            if (isset($_REQUEST["PensionEmployee"])) {
                if ($_REQUEST["PensionEmployee"] == "") {
                    $PensionEmployee = 0.0;
                } else {
                    $PensionEmployee = $_REQUEST["PensionEmployee"];
                }
                $v_PensionEmployee = number_format(str_replace(',', '', trim($PensionEmployee)), '2', '.', '');
            } else {
                $v_PensionEmployee = 0.0;
            }

            if (isset($_REQUEST["PensionEmployer"])) {
                if ($_REQUEST["PensionEmployer"] == "") {
                    $PensionEmployer = 0.0;
                } else {
                    $PensionEmployer = $_REQUEST["PensionEmployer"];
                }
                $v_PensionEmployer = number_format(str_replace(',', '', trim($PensionEmployer)), '2', '.', '');
            } else {
                $v_PensionEmployer = 0.0;
            }

            if (isset($_REQUEST["LeaveAllowance"]) && is_numeric(str_replace(',', '', trim($_REQUEST["LeaveAllowance"])))) {
                if ($_REQUEST["LeaveAllowance"] == "") {
                    $LeaveAllowance = 0.0;
                } else {
                    $LeaveAllowance = $_REQUEST["LeaveAllowance"];
                }
                $v_LeaveAllowance = number_format(str_replace(',', '', trim($LeaveAllowance)), '2', '.', '');
            } else {
                $v_LeaveAllowance = 0.0;
            }

            if (isset($_REQUEST["PAYE"])) {
                if ($_REQUEST["PAYE"] == "") {
                    $PAYE = 0.0;
                } else {
                    $PAYE = $_REQUEST["PAYE"];
                }
                $v_PAYE = number_format(str_replace(',', '', trim($PAYE)), '2', '.', '');
            } else {
                $v_PAYE = 0.0;
            }

            if (isset($_REQUEST["TTaxable"])) {
                if ($_REQUEST["TTaxable"] == "") {
                    $TTaxable = 0.0;
                } else {
                    $TTaxable = $_REQUEST["TTaxable"];
                }
                $v_TTaxable = number_format(str_replace(',', '', trim($TTaxable)), '2', '.', '');
            } else {
                $v_TTaxable = 0.0;
            }

            if (isset($_REQUEST["NetPay"])) {
                if ($_REQUEST["NetPay"] == "") {
                    $NetPay = 0.0;
                } else {
                    $NetPay = $_REQUEST["NetPay"];
                }
                $v_NetPay = number_format(str_replace(',', '', trim($NetPay)), '2', '.', '');
            } else {
                $v_NetPay = 0.0;
            }

            if (isset($_REQUEST["Gross"])) {
                if ($_REQUEST["Gross"] == "") {
                    $Gross = 0.0;
                } else {
                    $Gross = $_REQUEST["Gross"];
                }
                $v_Gross = number_format(str_replace(',', '', trim($Gross)), '2', '.', '');
            } else {
                $v_Gross = 0.0;
            }

            if (isset($_REQUEST["anniversary"])) {
                if ($_REQUEST["anniversary"] == "") {
                    $bonus = 0.0;
                } else {
                    $bonus = $_REQUEST["anniversary"];
                }
                $v_bonus = number_format(str_replace(',', '', trim($bonus)), '2', '.', '');
            } else {
                $v_bonus = 0.0;
            }

            //-------Additional (optional)

            if (isset($_REQUEST["add_name1"])) {
                $add_name1 = $_REQUEST["add_name1"];
            } else {
                $add_name1 = "";
            }
            if (isset($_REQUEST["add_value1"])) {
                $add_value1 = number_format(str_replace(',', '', trim($_REQUEST["add_value1"])), '2', '.', '');
            } else {
                $add_value1 = 0.0;
            }

            if (isset($_REQUEST["add_name2"])) {
                $add_name2 = $_REQUEST["add_name2"];
            } else {
                $add_name2 = "";
            }
            if (isset($_REQUEST["add_value2"])) {
                $add_value2 = number_format(str_replace(',', '', trim($_REQUEST["add_value2"])), '2', '.', '');
            } else {
                $add_value2 = 0.0;
            }

            if (isset($_REQUEST["add_name3"])) {
                $add_name3 = $_REQUEST["add_name3"];
            } else {
                $add_name3 = "";
            }
            if (isset($_REQUEST["add_value3"])) {
                $add_value3 = number_format(str_replace(',', '', trim($_REQUEST["add_value3"])), '2', '.', '');
            } else {
                $add_value3 = 0.0;
            }

            if (isset($_REQUEST["add_name4"])) {
                $add_name4 = $_REQUEST["add_name4"];
            } else {
                $add_name4 = "";
            }
            if (isset($_REQUEST["add_value4"])) {
                $add_value4 = number_format(str_replace(',', '', trim($_REQUEST["add_value4"])), '2', '.', '');
            } else {
                $add_value4 = 0.0;
            }

            if (isset($_REQUEST["add_name5"])) {
                $add_name5 = $_REQUEST["add_name5"];
            } else {
                $add_name5 = "";
            }
            if (isset($_REQUEST["add_value5"])) {
                $add_value5 = number_format(str_replace(',', '', trim($_REQUEST["add_value5"])), '2', '.', '');
            } else {
                $add_value5 = 0.0;
            }

            // Deduct

            if (isset($_REQUEST["remove_name1"])) {
                $remove_name1 = $_REQUEST["remove_name1"];
            } else {
                $remove_name1 = "";
            }
            if (isset($_REQUEST["remove_value1"])) {
                $remove_value1 = number_format(str_replace(',', '', trim($_REQUEST["remove_value1"])), '2', '.', '');
            } else {
                $remove_value1 = 0.0;
            }

            if (isset($_REQUEST["remove_name2"])) {
                $remove_name2 = $_REQUEST["remove_name2"];
            } else {
                $remove_name2 = "";
            }
            if (isset($_REQUEST["remove_value2"])) {
                $remove_value2 = number_format(str_replace(',', '', trim($_REQUEST["remove_value2"])), '2', '.', '');
            } else {
                $remove_value2 = 0.0;
            }

            if (isset($_REQUEST["remove_name3"])) {
                $remove_name3 = $_REQUEST["remove_name3"];
            } else {
                $remove_name3 = "";
            }
            if (isset($_REQUEST["remove_value3"])) {
                $remove_value3 = number_format(str_replace(',', '', trim($_REQUEST["remove_value3"])), '2', '.', '');
            } else {
                $remove_value3 = 0.0;
            }

            if (isset($_REQUEST["remove_name4"])) {
                $remove_name4 = $_REQUEST["remove_name4"];
            } else {
                $remove_name4 = "";
            }
            if (isset($_REQUEST["remove_value4"])) {
                $remove_value4 = number_format(str_replace(',', '', trim($_REQUEST["remove_value4"])), '2', '.', '');
            } else {
                $remove_value4 = 0.0;
            }

            if (isset($_REQUEST["remove_name5"])) {
                $remove_name5 = $_REQUEST["remove_name5"];
            } else {
                $remove_name5 = "";
            }
            if (isset($_REQUEST["remove_value5"])) {
                $remove_value5 = number_format(str_replace(',', '', trim($_REQUEST["remove_value5"])), '2', '.', '');
            } else {
                $remove_value5 = 0.0;
            }

            if (isset($_REQUEST["TRelief"])) {
                $vTRelief = number_format(str_replace(',', '', trim($_REQUEST["TRelief"])), '2', '.', '');
            } else {
                $vTRelief = 0.0;
            }

            if (isset($_REQUEST["lateness_now"])) {
                $vLateness = number_format(str_replace(',', '', trim($_REQUEST["lateness_now"])), '2', '.', '');
            } else {
                $vLateness = 0.0;
            }

            if (isset($_REQUEST["absent"])) {
                $vAbsent = number_format(str_replace(',', '', trim($_REQUEST["absent"])), '2', '.', '');
            } else {
                $vAbsent = 0.0;
            }

            if (isset($_REQUEST["overtime_now"])) {
                $vOvertime = number_format(str_replace(',', '', trim($_REQUEST["overtime_now"])), '2', '.', '');
            } else {
                $vOvertime = 0.0;
            }

            if (isset($_REQUEST["repay_loan"])) {
                $vLoan = number_format(str_replace(',', '', trim($_REQUEST["repay_loan"])), '2', '.', '');
            } else {
                $vLoan = 0.0;
            }

            /*            $dbOpen2 = ("select top 1 * from Fin_PRSettings where HashKey=(select Scheme from Fin_PRCore where EmpID='".$EmpPID."')");
include '../login/dbOpen2.php';
while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{ */
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MonthlyPay" . $_SESSION["StkTck" . "UName"];
            $HashKey = md5($UniqueKey);
            $EditID = $HashKey;

            /*
            $Script = "INSERT INTO [Fin_PRIndvPay]
            (EmpID,[PayMonth],[Gross],[Status],[AddedBy],[AddedDate],[HashKey],[TRelief],[LeaveAllowance])
            VALUES ('".ECh($EmpPID)."','".ECh('27 '.$_REQUEST["PayMonth"])."','0','N','".ECh($_SESSION["StkTck"."HKey"])."',GetDate(),'".$HashKey."','".ECh($_REQUEST["TRelief"])."','".ECh($_REQUEST["LeaveAllowance"])."'";
            //            echo $Script."<br><br>";
            ScriptRunnerUD($Script,"NComp");
             */
            // var_dump($add_name1);
            // var_dump($add_value1);
            // die();
            $Script = "INSERT INTO [Fin_PRIndvPay]
				(EmpID,[PayMonth],[Gross],[Status],[AddedBy],[AddedDate],[HashKey])
				VALUES ('" . ECh($EmpPID) . "','" . ECh('27 ' . $_REQUEST["PayMonth"]) . "','0','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
            //            echo $Script."<br><br>";
            ScriptRunnerUD($Script, "NComp");

            $script_lvType = "SELECT SetValue11 FROM Settings WHERE Setting = 'LeaveTyp'";
            $lvType = Scriptrunner($script_lvType, "SetValue11");

            $script_applied_leave = "SELECT *  FROM LvIndTOff WHERE LType = '0b23845da0e280008e5b9c7a2adfb462'
AND EmpID='" . $EmpID_ . "' AND [Status] = 'A' AND Year(CONVERT(char(11),LvDate,106)) = YEAR(CONVERT(char(11),GETDATE(),106))
ORDER BY ApprovedDate ASC";

            $approved = Scriptrunner($script_applied_leave, "Status");

            $script_bonus_pay = "SELECT TOP(1) * FROM Fin_PRIndvPay
WHERE Year(CONVERT(char(11),PayMonth,106)) = YEAR(CONVERT(char(11),GETDATE(),106)) AND [Status] IN ('N', 'U')
AND EmpID='" . $EmpID_ . "'
ORDER BY  PayMonth ASC";
            $PayBonus = Scriptrunner($script_bonus_pay, "payStatusBonus");
            // if($PayBonus != 'A'){
            //     $PayBonus = 'A';
            // }

            // $Script_bonus_update = "UPDATE [Fin_PRIndvPay]
            //                     SET [payStatusBonus] = 'A'
            //                     ,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
            //                     ,[AuthDate] = GetDate()
            //                     WHERE [EmpID] = '".$EmpID_."'";
            //                     ScriptRunnerUD($Script_bonus_update, "Authorize");
            //Get Account code and write same to the specified table column

            $Script = "UPDATE [Fin_PRIndvPay]
				SET
				[Gross] ='" . ECh($v_Gross) . "'
				,[PayMonth] ='" . ECh('27 ' . $_REQUEST["PayMonth"]) . "'
				,[PayItem1] ='" . ECh($v_PayItem1) . "'
				,[PayItem2] ='" . ECh($v_PayItem2) . "'
				,[PayItem3] ='" . ECh($v_PayItem3) . "'
				,[PayItem4] ='" . ECh($v_PayItem4) . "'
				,[PayItem5] ='" . ECh($v_PayItem5) . "'
				,[PayItem6] ='" . ECh($v_PayItem6) . "'
				,[PayItem7] ='" . ECh($v_PayItem7) . "'
				,[PayItem8] ='" . ECh($v_PayItem8) . "'
				,[PayItem9] ='" . ECh($v_PayItem9) . "'
				,[PayItem10] ='" . ECh($v_PayItem10) . "'
				,[PayItem11] ='" . ECh($v_PayItem11) . "'
				,[PayItem12] ='" . ECh($v_PayItem12) . "'
				,[PayItem13] ='" . ECh($v_PayItem13) . "'
				,[PayItem14] ='" . ECh($v_PayItem14) . "'
				,[PayItem15] ='" . ECh($v_PayItem15) . "'
				,[PayItem16] ='" . ECh($v_PayItem16) . "'
				,[PayItem17] ='" . ECh($v_PayItem17) . "'
				,[PayItem18] ='" . ECh($v_PayItem18) . "'
				,[PayItem19] ='" . ECh($v_PayItem19) . "'
				,[PayItem20] ='" . ECh($v_PayItem20) . "'
				,[PayItem21] ='" . ECh($v_PayItem21) . "'
				,[PayItem22] ='" . ECh($v_PayItem22) . "'
				,[PayItem23] ='" . ECh($v_PayItem23) . "'
				,[PayItem24] ='" . ECh($v_PayItem24) . "'
				,[PayItem25] ='" . ECh($v_PayItem25) . "'
				,[PensionEmployee] ='" . ECh($v_PensionEmployee) . "'
				,[PensionEmployer] ='" . ECh($v_PensionEmployer) . "'
				,[PAYE] ='" . ECh($v_PAYE) . "'
				,[LeaveAllowance] ='" . ECh($v_LeaveAllowance) . "'
				,[TTaxable] ='" . ECh($v_TTaxable) . "'
				,[NetPay] ='" . ECh($v_NetPay) . "'
				,[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "'
				,[UpdatedDate] = GetDate()
				,[Status] = 'U'
				,[more_item_add_name1]='" . ECh($add_name1) . "'
				,[more_item_add_value1]='" . ECh($add_value1) . "'
				,[more_item_add_name2]='" . ECh($add_name2) . "'
				,[more_item_add_value2]='" . ECh($add_value2) . "'
				,[more_item_add_name3]='" . ECh($add_name3) . "'
				,[more_item_add_value3]='" . ECh($add_value3) . "'
				,[more_item_add_name4]='" . ECh($add_name4) . "'
				,[more_item_add_value4]='" . ECh($add_value4) . "'
				,[more_item_add_name5]='" . ECh($add_name5) . "'
				,[more_item_add_value5]='" . ECh($add_value5) . "'
				,[more_item_remove_name1]='" . ECh($remove_name1) . "'
				,[more_item_remove_value1]='" . ECh($remove_value1) . "'
				,[more_item_remove_name2]='" . ECh($remove_name2) . "'
				,[more_item_remove_value2]='" . ECh($remove_value2) . "'
				,[more_item_remove_name3]='" . ECh($remove_name3) . "'
				,[more_item_remove_value3]='" . ECh($remove_value3) . "'
				,[more_item_remove_name4]='" . ECh($remove_name4) . "'
				,[more_item_remove_value4]='" . ECh($remove_value4) . "'
				,[more_item_remove_name5]='" . ECh($remove_name5) . "'
				,[TRelief]='" . ECh($vTRelief) . "'
				,[more_item_remove_value5]='" . ECh($remove_value5) . "'
				,[AnniversaryBonus]='" . ECh($v_bonus) . "'
				,[payStatusBonus] = 'A'
				,[LatenessAmount]='" . ECh($vLateness) . "'
				,[AbsentAmount]='" . ECh($vAbsent) . "'
				,[OverTimeAmount]='" . ECh($vOvertime) . "'
				,[loanAmount]='" . ECh($vLoan) . "'
				WHERE [HashKey] = '" . $HashKey . "'";

            ScriptRunnerUD($Script, "Inst");
            /*            } */
            $Script_OvTime = "Update Fin_Attend Set Status='B' where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpPID . "' and Status in ('A')";

            //    var_dump($Script_OvTime);
            //    die();

            ScriptRunnerUD($Script_OvTime, "Sm");

            echo ("<script type='text/javascript'>{ parent.msgbox('New salary payment raised for - " . $EmpNm . ".','green'); }</script>");

            include 'loan_deduction_mgr.php';
        } else {
            $GoValidate = false;
            echo ("<script type='text/javascript'>{ parent.msgbox('A salary payment already exist for the selected employee on the selected month','red'); }</script>");
        }
    }

    //**************************************************************************************************
    //**************************************************************************************************
    /*
    elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Payment" && isset($_REQUEST["AcctNo"]) && strlen(trim($_REQUEST["AcctNo"]))==32 && $_REQUEST["DelMax"] > 0 && isset($_REQUEST["PayMonth"]) && strlen($_REQUEST["PayMonth"])==8)
    {
     */ elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Payment") {

        //exit('updating...');
        if (ValidateURths("PAY SALARY" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        /* Set HashKey of Account to be Updated */
        $EditID = $_REQUEST["UpdID"];
        $HashKey = $_REQUEST["UpdID"];

        if ($GoValidate == true) {
            //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
            if (ValidateUpdDel("SELECT Status from [Fin_PRIndvPay] where [HashKey]= '" . $HashKey . "'") == true) {
                $Script = "SELECT EmpID from EmpTbl where HashKey=(select EmpID from Fin_PRCore where HashKey='" . $HashKey . "') ";
                $kk = ScriptRunner($Script, "EmpID");

                if (isset($_REQUEST["PayItem1"])) {
                    $v_PayItem1 = number_format(str_replace(',', '', trim($_REQUEST["PayItem1"])), '2', '.', '');
                } else {
                    $v_PayItem1 = 0.0;
                }
                if (isset($_REQUEST["PayItem2"])) {
                    $v_PayItem2 = number_format(str_replace(',', '', trim($_REQUEST["PayItem2"])), '2', '.', '');
                } else {
                    $v_PayItem2 = 0.0;
                }
                if (isset($_REQUEST["PayItem3"])) {
                    $v_PayItem3 = number_format(str_replace(',', '', trim($_REQUEST["PayItem3"])), '2', '.', '');
                } else {
                    $v_PayItem3 = 0.0;
                }
                if (isset($_REQUEST["PayItem4"])) {
                    $v_PayItem4 = number_format(str_replace(',', '', trim($_REQUEST["PayItem4"])), '2', '.', '');
                } else {
                    $v_PayItem4 = 0.0;
                }
                if (isset($_REQUEST["PayItem5"])) {
                    $v_PayItem5 = number_format(str_replace(',', '', trim($_REQUEST["PayItem5"])), '2', '.', '');
                } else {
                    $v_PayItem5 = 0.0;
                }
                if (isset($_REQUEST["PayItem6"])) {
                    $v_PayItem6 = number_format(str_replace(',', '', trim($_REQUEST["PayItem6"])), '2', '.', '');
                } else {
                    $v_PayItem6 = 0.0;
                }
                if (isset($_REQUEST["PayItem7"])) {
                    $v_PayItem7 = number_format(str_replace(',', '', trim($_REQUEST["PayItem7"])), '2', '.', '');
                } else {
                    $v_PayItem7 = 0.0;
                }
                if (isset($_REQUEST["PayItem8"])) {
                    $v_PayItem8 = number_format(str_replace(',', '', trim($_REQUEST["PayItem8"])), '2', '.', '');
                } else {
                    $v_PayItem8 = 0.0;
                }
                if (isset($_REQUEST["PayItem9"])) {
                    $v_PayItem9 = number_format(str_replace(',', '', trim($_REQUEST["PayItem9"])), '2', '.', '');
                } else {
                    $v_PayItem9 = 0.0;
                }
                if (isset($_REQUEST["PayItem10"])) {
                    $v_PayItem10 = number_format(str_replace(',', '', trim($_REQUEST["PayItem10"])), '2', '.', '');
                } else {
                    $v_PayItem10 = 0.0;
                }
                if (isset($_REQUEST["PayItem11"])) {
                    $v_PayItem11 = number_format(str_replace(',', '', trim($_REQUEST["PayItem11"])), '2', '.', '');
                } else {
                    $v_PayItem11 = 0.0;
                }
                if (isset($_REQUEST["PayItem12"])) {
                    $v_PayItem12 = number_format(str_replace(',', '', trim($_REQUEST["PayItem12"])), '2', '.', '');
                } else {
                    $v_PayItem12 = 0.0;
                }
                if (isset($_REQUEST["PayItem13"])) {
                    $v_PayItem13 = number_format(str_replace(',', '', trim($_REQUEST["PayItem13"])), '2', '.', '');
                } else {
                    $v_PayItem13 = 0.0;
                }
                if (isset($_REQUEST["PayItem14"])) {
                    $v_PayItem14 = number_format(str_replace(',', '', trim($_REQUEST["PayItem14"])), '2', '.', '');
                } else {
                    $v_PayItem14 = 0.0;
                }
                if (isset($_REQUEST["PayItem15"])) {
                    $v_PayItem15 = number_format(str_replace(',', '', trim($_REQUEST["PayItem15"])), '2', '.', '');
                } else {
                    $v_PayItem15 = 0.0;
                }
                if (isset($_REQUEST["PayItem16"])) {
                    $v_PayItem16 = number_format(str_replace(',', '', trim($_REQUEST["PayItem16"])), '2', '.', '');
                } else {
                    $v_PayItem16 = 0.0;
                }
                if (isset($_REQUEST["PayItem17"])) {
                    $v_PayItem17 = number_format(str_replace(',', '', trim($_REQUEST["PayItem17"])), '2', '.', '');
                } else {
                    $v_PayItem17 = 0.0;
                }
                if (isset($_REQUEST["PayItem18"])) {
                    $v_PayItem18 = number_format(str_replace(',', '', trim($_REQUEST["PayItem18"])), '2', '.', '');
                } else {
                    $v_PayItem18 = 0.0;
                }
                if (isset($_REQUEST["PayItem19"])) {
                    $v_PayItem19 = number_format(str_replace(',', '', trim($_REQUEST["PayItem19"])), '2', '.', '');
                } else {
                    $v_PayItem19 = 0.0;
                }
                if (isset($_REQUEST["PayItem20"])) {
                    $v_PayItem20 = number_format(str_replace(',', '', trim($_REQUEST["PayItem20"])), '2', '.', '');
                } else {
                    $v_PayItem20 = 0.0;
                }
                if (isset($_REQUEST["PayItem21"])) {
                    $v_PayItem21 = number_format(str_replace(',', '', trim($_REQUEST["PayItem21"])), '2', '.', '');
                } else {
                    $v_PayItem21 = 0.0;
                }
                if (isset($_REQUEST["PayItem22"])) {
                    $v_PayItem22 = number_format(str_replace(',', '', trim($_REQUEST["PayItem22"])), '2', '.', '');
                } else {
                    $v_PayItem22 = 0.0;
                }
                if (isset($_REQUEST["PayItem23"])) {
                    $v_PayItem23 = number_format(str_replace(',', '', trim($_REQUEST["PayItem23"])), '2', '.', '');
                } else {
                    $v_PayItem23 = 0.0;
                }
                if (isset($_REQUEST["PayItem24"])) {
                    $v_PayItem24 = number_format(str_replace(',', '', trim($_REQUEST["PayItem24"])), '2', '.', '');
                } else {
                    $v_PayItem24 = 0.0;
                }
                if (isset($_REQUEST["PayItem25"])) {
                    $v_PayItem25 = number_format(str_replace(',', '', trim($_REQUEST["PayItem25"])), '2', '.', '');
                } else {
                    $v_PayItem25 = 0.0;
                }
                if (isset($_REQUEST["PensionEmployee"])) {
                    $v_PensionEmployee = number_format(str_replace(',', '', trim($_REQUEST["PensionEmployee"])), '2', '.', '');
                } else {
                    $v_PensionEmployee = 0.0;
                }
                if (isset($_REQUEST["PensionEmployer"])) {
                    $v_PensionEmployer = number_format(str_replace(',', '', trim($_REQUEST["PensionEmployer"])), '2', '.', '');
                } else {
                    $v_PensionEmployer = 0.0;
                }
                if (isset($_REQUEST["LeaveAllowance"]) && is_numeric($_REQUEST["LeaveAllowance"])) {
                    $v_LeaveAllowance = number_format(str_replace(',', '', trim($_REQUEST["LeaveAllowance"])), '2', '.', '');
                } else {
                    $v_LeaveAllowance = 0.0;
                }
                if (isset($_REQUEST["PAYE"])) {
                    $v_PAYE = number_format(str_replace(',', '', trim($_REQUEST["PAYE"])), '2', '.', '');
                } else {
                    $v_PAYE = 0.0;
                }
                if (isset($_REQUEST["TTaxable"])) {
                    $v_TTaxable = number_format(str_replace(',', '', trim($_REQUEST["TTaxable"])), '2', '.', '');
                } else {
                    $v_TTaxable = 0.0;
                }
                if (isset($_REQUEST["NetPay"])) {
                    $v_NetPay = number_format(str_replace(',', '', trim($_REQUEST["NetPay"])), '2', '.', '');
                } else {
                    $v_NetPay = 0.0;
                }
                if (isset($_REQUEST["Gross"])) {
                    $v_Gross = number_format(str_replace(',', '', trim($_REQUEST["Gross"])), '2', '.', '');
                } else {
                    $v_Gross = 0.0;
                }
                if (isset($_REQUEST["anniversary"])) {
                    $v_bonus = number_format(str_replace(',', '', trim($_REQUEST["anniversary"])), '2', '.', '');
                } else {
                    $v_anniversary = 0.0;
                }

                //-------Additional (optional)

                if (isset($_REQUEST["add_name1"])) {
                    $add_name1 = $_REQUEST["add_name1"];
                } else {
                    $add_name1 = "";
                }
                if (isset($_REQUEST["add_value1"])) {
                    $add_value1 = number_format(str_replace(',', '', trim($_REQUEST["add_value1"])), '2', '.', '');
                } else {
                    $add_value1 = 0.0;
                }

                if (isset($_REQUEST["add_name2"])) {
                    $add_name2 = $_REQUEST["add_name2"];
                } else {
                    $add_name2 = "";
                }
                if (isset($_REQUEST["add_value2"])) {
                    $add_value2 = number_format(str_replace(',', '', trim($_REQUEST["add_value2"])), '2', '.', '');
                } else {
                    $add_value2 = 0.0;
                }

                if (isset($_REQUEST["add_name3"])) {
                    $add_name3 = $_REQUEST["add_name3"];
                } else {
                    $add_name3 = "";
                }
                if (isset($_REQUEST["add_value3"])) {
                    $add_value3 = number_format(str_replace(',', '', trim($_REQUEST["add_value3"])), '2', '.', '');
                } else {
                    $add_value3 = 0.0;
                }

                if (isset($_REQUEST["add_name4"])) {
                    $add_name4 = $_REQUEST["add_name4"];
                } else {
                    $add_name4 = "";
                }
                if (isset($_REQUEST["add_value4"])) {
                    $add_value4 = number_format(str_replace(',', '', trim($_REQUEST["add_value4"])), '2', '.', '');
                } else {
                    $add_value4 = 0.0;
                }

                if (isset($_REQUEST["add_name5"])) {
                    $add_name5 = $_REQUEST["add_name5"];
                } else {
                    $add_name5 = "";
                }
                if (isset($_REQUEST["add_value5"])) {
                    $add_value5 = number_format(str_replace(',', '', trim($_REQUEST["add_value5"])), '2', '.', '');
                } else {
                    $add_value5 = 0.0;
                }

                // Deduct

                if (isset($_REQUEST["remove_name1"])) {
                    $remove_name1 = $_REQUEST["remove_name1"];
                } else {
                    $remove_name1 = "";
                }
                if (isset($_REQUEST["remove_value1"])) {
                    $remove_value1 = number_format(str_replace(',', '', trim($_REQUEST["remove_value1"])), '2', '.', '');
                } else {
                    $remove_value1 = 0.0;
                }

                if (isset($_REQUEST["remove_name2"])) {
                    $remove_name2 = $_REQUEST["remove_name2"];
                } else {
                    $remove_name2 = "";
                }
                if (isset($_REQUEST["remove_value2"])) {
                    $remove_value2 = number_format(str_replace(',', '', trim($_REQUEST["remove_value2"])), '2', '.', '');
                } else {
                    $remove_value2 = 0.0;
                }

                if (isset($_REQUEST["remove_name3"])) {
                    $remove_name3 = $_REQUEST["remove_name3"];
                } else {
                    $remove_name3 = "";
                }
                if (isset($_REQUEST["remove_value3"])) {
                    $remove_value3 = number_format(str_replace(',', '', trim($_REQUEST["remove_value3"])), '2', '.', '');
                } else {
                    $remove_value3 = 0.0;
                }

                if (isset($_REQUEST["remove_name4"])) {
                    $remove_name4 = $_REQUEST["remove_name4"];
                } else {
                    $remove_name4 = "";
                }
                if (isset($_REQUEST["remove_value4"])) {
                    $remove_value4 = number_format(str_replace(',', '', trim($_REQUEST["remove_value4"])), '2', '.', '');
                } else {
                    $remove_value4 = 0.0;
                }

                if (isset($_REQUEST["remove_name5"])) {
                    $remove_name5 = $_REQUEST["remove_name5"];
                } else {
                    $remove_name5 = "";
                }
                if (isset($_REQUEST["remove_value5"])) {
                    $remove_value5 = number_format(str_replace(',', '', trim($_REQUEST["remove_value5"])), '2', '.', '');
                } else {
                    $remove_value5 = 0.0;
                }

                //---------------
                if (isset($_REQUEST["TRelief"])) {
                    $vTRelief = number_format(str_replace(',', '', trim($_REQUEST["TRelief"])), '2', '.', '');
                } else {
                    $vTRelief = 0.0;
                }

                if (isset($_REQUEST["repay_loan"])) {
                    $vLoan = number_format(str_replace(',', '', trim($_REQUEST["repay_loan"])), '2', '.', '');
                } else {
                    $vLoan = 0.0;
                }

                $Script = "UPDATE [Fin_PRIndvPay]
				SET
				[Gross] ='" . ECh($v_Gross) . "'
				,[PayMonth] ='" . ECh('27 ' . $_REQUEST["PayMonth"]) . "'
				,[PayItem1] ='" . ECh($v_PayItem1) . "'
				,[PayItem2] ='" . ECh($v_PayItem2) . "'
				,[PayItem3] ='" . ECh($v_PayItem3) . "'
				,[PayItem4] ='" . ECh($v_PayItem4) . "'
				,[PayItem5] ='" . ECh($v_PayItem5) . "'
				,[PayItem6] ='" . ECh($v_PayItem6) . "'
				,[PayItem7] ='" . ECh($v_PayItem7) . "'
				,[PayItem8] ='" . ECh($v_PayItem8) . "'
				,[PayItem9] ='" . ECh($v_PayItem9) . "'
				,[PayItem10] ='" . ECh($v_PayItem10) . "'
				,[PayItem11] ='" . ECh($v_PayItem11) . "'
				,[PayItem12] ='" . ECh($v_PayItem12) . "'
				,[PayItem13] ='" . ECh($v_PayItem13) . "'
				,[PayItem14] ='" . ECh($v_PayItem14) . "'
				,[PayItem15] ='" . ECh($v_PayItem15) . "'
				,[PayItem16] ='" . ECh($v_PayItem16) . "'
				,[PayItem17] ='" . ECh($v_PayItem17) . "'
				,[PayItem18] ='" . ECh($v_PayItem18) . "'
				,[PayItem19] ='" . ECh($v_PayItem19) . "'
				,[PayItem20] ='" . ECh($v_PayItem20) . "'
				,[PayItem21] ='" . ECh($v_PayItem21) . "'
				,[PayItem22] ='" . ECh($v_PayItem22) . "'
				,[PayItem23] ='" . ECh($v_PayItem23) . "'
				,[PayItem24] ='" . ECh($v_PayItem24) . "'
				,[PayItem25] ='" . ECh($v_PayItem25) . "'
				,[TRelief] ='" . ECh($vTRelief) . "'
				,[PensionEmployer] ='" . ECh($v_PensionEmployer) . "'
				,[PensionEmployee] ='" . ECh($v_PensionEmployee) . "'
				,[PAYE] ='" . ECh($v_PAYE) . "'
				,[LeaveAllowance] ='" . ECh($v_LeaveAllowance) . "'
				,[TTaxable] ='" . ECh($v_TTaxable) . "'
				,[NetPay] ='" . ECh($v_NetPay) . "'
				,[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "'
				,[UpdatedDate] = GetDate()
				,[Status] = 'U'
				,[more_item_add_name1]='" . ECh($add_name1) . "'
				,[more_item_add_value1]='" . ECh($add_value1) . "'
				,[more_item_add_name2]='" . ECh($add_name2) . "'
				,[more_item_add_value2]='" . ECh($add_value2) . "'
				,[more_item_add_name3]='" . ECh($add_name3) . "'
				,[more_item_add_value3]='" . ECh($add_value3) . "'
				,[more_item_add_name4]='" . ECh($add_name4) . "'
				,[more_item_add_value4]='" . ECh($add_value4) . "'
				,[more_item_add_name5]='" . ECh($add_name5) . "'
				,[more_item_add_value5]='" . ECh($add_value5) . "'
				,[more_item_remove_name1]='" . ECh($remove_name1) . "'
				,[more_item_remove_value1]='" . ECh($remove_value1) . "'
				,[more_item_remove_name2]='" . ECh($remove_name2) . "'
				,[more_item_remove_value2]='" . ECh($remove_value2) . "'
				,[more_item_remove_name3]='" . ECh($remove_name3) . "'
				,[more_item_remove_value3]='" . ECh($remove_value3) . "'
				,[more_item_remove_name4]='" . ECh($remove_name4) . "'
				,[more_item_remove_value4]='" . ECh($remove_value4) . "'
				,[more_item_remove_name5]='" . ECh($remove_name5) . "'
				,[more_item_remove_value5]='" . ECh($remove_value5) . "'
				,[loanAmount]='" . ECh($vLoan) . "'
				WHERE [HashKey] = '" . $HashKey . "'";
                //echo $Script."<br>";
                ScriptRunnerUD($Script, "Inst");

                //AuditLog("INSERT","Employee payroll schedule updated for account ID [".$kk."]");
                //MailTrail("PAYROLL","T",'','','','');

                echo ("<script type='text/javascript'>{ parent.msgbox('Employee salary payment updated successfully, 'green'); }</script>");

                //Reopen the updated schedule. After SAVE file is not closed automatically
                $EditID = $HashKey;
                $EmpID_ = ECh($_REQUEST["EmpID"]);
                //$PayScheme_=ECh($_REQUEST["PayScheme"]);    //Get Scheme for the specified employee salary
                $Script_Edit = "select * from [Fin_PRCore] where HashKey='" . $EditID . "'";

                include 'loan_deduction_mgr.php';
            } else {
                echo ("<script type='text/javascript'>{ parent.msgbox('Cannot update authorized record', 'red'); }</script>");
                include '../main/clr_val.php'; /* CLEARS $EditID */
            }
        }
    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
        include '../main/clr_val.php';
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] = "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {

        if (ValidateURths("PAY SALARY" . "V") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $CmdTyp = "Edit";
                $EditID = ECh($HashVals[$i]);

                //$Script_Edit ="select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='ebece94938fa75a73f08389dc5622eaa' and Status not in ('A','D')";

                $Script_Edit = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";

                $EmpID_ = ScriptRunner($Script_Edit, "EmpID");
                $Script_Edit__ = "select * from [Fin_PRCore] where EmpID='" . $EmpID_ . "' and Status='A'";
                $PayScheme_ = ScriptRunner($Script_Edit__, "Scheme"); //Get Scheme for the specified employee salary
                $loan_amt = ScriptRunner($Script_Edit, "loanAmount");

                $script_lvType = "SELECT SetValue11 FROM Settings WHERE Setting = 'LeaveTyp'";
                $lvType = Scriptrunner($script_lvType, "SetValue11");

                $script_applied_leave = "SELECT *  FROM LvIndTOff WHERE LType = '0b23845da0e280008e5b9c7a2adfb462'
AND EmpID='" . $EmpID_ . "' AND [Status] = 'A' AND Year(CONVERT(char(11),LvDate,106)) = YEAR(CONVERT(char(11),GETDATE(),106))
ORDER BY ApprovedDate ASC";

                $approved = Scriptrunner($script_applied_leave, "Status");

                $script_bonus_pay = "SELECT TOP(1) * FROM Fin_PRIndvPay
WHERE Year(CONVERT(char(11),PayMonth,106)) = YEAR(CONVERT(char(11),GETDATE(),106)) AND [Status] IN ('A')
AND EmpID='" . $EmpID_ . "'
ORDER BY  PayMonth ASC";
                $PayBonus = Scriptrunner($script_bonus_pay, "payStatusBonus");

                $script_paymonth = "select Month(CONVERT(char(11),PayMonth,106)) as payDt
from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";

                $Pay_month = Scriptrunner($script_paymonth, "payDt");

                $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $EmpID_ . "'";
                $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
            }
        }
    }

    //------------------------------------------- Authorize record BEN

    /*
elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0)
{
if (ValidateURths("PAY SALARY"."T")!=true){include '../main/NoAccess.php';exit;}

$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
for ($i=0; $i <= $ArrayCount; $i++)
{//ActLnk
if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
{
$EditID = ECh($HashVals[$i]);
$Script_ID = "SELECT Em.HashKey HKey, (Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm, Em.EmpID as EmpID, Em.Email, Fp.HashKey from EmpTbl Em, Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='".$EditID."'";
//$EditID = ScriptRunner($Script_ID, "HashKey");
$EmpID_ = ScriptRunner($Script_ID, "HKey");

//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
if (ValidateUpdDel("SELECT Status from [Fin_PRIndvPay] WHERE [HashKey]='".$EditID."'") == true)
{
//Check if account is customer account and if SMS alert is set to fire
//                    echo "Chiken";
$Script = "UPDATE [Fin_PRIndvPay]
SET [Status] = 'A'
,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
,[AuthDate] = GetDate()
WHERE [HashKey] = '".$EditID."'";
ScriptRunnerUD ($Script, "Authorize");
//AuditLog("AUTHORIZE","Employee payment authorized for ".ScriptRunner($Script_ID,"Nm"));

$Script="Select Convert(varchar(11),PayMonth,106) PayMth from Fin_PRIndvPay WHERE [HashKey]='".$EditID."'";
$PayMonth=ScriptRunner($Script,"PayMth");

$Script_Loan="Select HashKey from LnIndvOff where EID='".$EmpID_."' and Status='B' and
(Month(ExpPayDate) = MONTH('".$PayMonth."') and
YEAR(ExpPayDate) = YEAR('".$PayMonth."'))";

$Script = "UPDATE [LnIndvOff]
SET [Status] = 'P'
,[PaidBy] = '".$_SESSION["StkTck"."HKey"]."'
,[PaidDate] = GetDate()
WHERE [HashKey]=(

Select HashKey from LnIndvOff where EID='".$EmpID_."' and Status='B' and
(Month(ExpPayDate) = MONTH('".$PayMonth."') and
YEAR(ExpPayDate) = YEAR('".$PayMonth."'))

)";

ScriptRunnerUD ($Script, "Authorize");
//echo "<br>".$Script;

//                    $dbOpen2 = ("SELECT *,(DATENAME(month,Fm.PayMonth)+' '+DATENAME(year,Fm.PayMonth)) as PMth from [Fin_PRIndvPay] Fm WHERE Fm.[HashKey]='".$EditID."'");

$dbOpen2="SELECT Et.HashKey AS HKey, Et.SName + ' ' + Et.ONames + ' ' + Et.FName AS [Full Name], Et.HashKey, Et.Status, Et.EmpStatus,
Et.Email, Et.PENCommNo, Et.HInsurNo, Et.SalAcctNo, Et.SalBank, Et.Department, Et.SalSortCode, Et.TaxID, Et.PenCustID, Et.BranchID, FMc.PayItem1, FMc.PayItem2, FMc.Gross, (datename(M,FMc.PayMonth)+' '+datename(YYYY,FMc.PayMonth)) PMth,
FMc.PayItem3, FMc.PayItem4, FMc.PayItem5, FMc.PayItem6, FMc.PayItem7, FMc.PayItem8, FMc.PayItem9, FMc.PayItem10, FMc.PayItem11, FMc.PayItem12, FMc.PayItem13, FMc.PayItem14,
FMc.PayItem15, FMc.PayItem16, FMc.PayItem17, FMc.PayItem18, FMc.PayItem19, FMc.PayItem20, FMc.PayItem21, FMc.PayItem22, FMc.PayItem23, FMc.PayItem24,
FMc.PayItem25, FMs.PayItemNm1, FMs.PayItemOF1, FMs.PayItemTX1, FMs.PayItemCD1, FMs.PayItemPen1, FMs.PayItemNm2, FMs.PayItemOF2, FMs.PayItemTX2,
FMs.PayItemCD2, FMs.PayItemPen2, FMs.PayItemNm3, FMs.PayItemOF3, FMs.PayItemTX3, FMs.PayItemCD3, FMs.PayItemPen3, FMs.PayItemNm4,
FMs.PayItemOF4, FMs.PayItemTX4, FMs.PayItemCD4, FMs.PayItemPen4, FMs.PayItemNm5, FMs.PayItemOF5, FMs.PayItemTX5, FMs.PayItemCD5,
FMs.PayItemPen5, FMs.PayItemNm6, FMs.PayItemOF6, FMs.PayItemTX6, FMs.PayItemCD6, FMs.PayItemPen6, FMs.PayItemNm7, FMs.PayItemOF7,
FMs.PayItemTX7, FMs.PayItemCD7, FMs.PayItemPen7, FMs.PayItemNm8, FMs.PayItemOF8, FMs.PayItemTX8, FMs.PayItemCD8, FMs.PayItemPen8,
FMs.PayItemNm9, FMs.PayItemOF9, FMs.PayItemTX9, FMs.PayItemCD9, FMs.PayItemPen9, FMs.PayItemNm10, FMs.PayItemOF10, FMs.PayItemTX10,
FMs.PayItemCD10, FMs.PayItemPen10, FMs.PayItemNm11, FMs.PayItemOF11, FMs.PayItemTX11, FMs.PayItemCD11, FMs.PayItemPen11, FMs.PayItemNm12,
FMs.PayItemOF12, FMs.PayItemTX12, FMs.PayItemCD12, FMs.PayItemPen12, FMs.PayItemNm13, FMs.PayItemOF13, FMs.PayItemTX13, FMs.PayItemCD13,
FMs.PayItemPen13, FMs.PayItemNm14, FMs.PayItemOF14, FMs.PayItemTX14, FMs.PayItemCD14, FMs.PayItemPen14, FMs.PayItemNm15, FMs.PayItemOF15,
FMs.PayItemTX15, FMs.PayItemCD15, FMs.PayItemPen15, FMs.PayItemNm16, FMs.PayItemOF16, FMs.PayItemTX16, FMs.PayItemCD16, FMs.PayItemPen16,
FMs.PayItemNm17, FMs.PayItemOF17, FMs.PayItemTX17, FMs.PayItemCD17, FMs.PayItemPen17, FMs.PayItemNm18, FMs.PayItemOF18, FMs.PayItemTX18,
FMs.PayItemCD18, FMs.PayItemPen18, FMs.PayItemNm19, FMs.PayItemOF19, FMs.PayItemTX19, FMs.PayItemCD19, FMs.PayItemPen19, FMs.PayItemNm20,
FMs.PayItemOF20, FMs.PayItemTX20, FMs.PayItemCD20, FMs.PayItemPen20, FMs.PayItemNm21, FMs.PayItemOF21, FMs.PayItemTX21, FMs.PayItemCD21,
FMs.PayItemPen21, FMs.PayItemNm22, FMs.PayItemOF22, FMs.PayItemTX22, FMs.PayItemCD22, FMs.PayItemPen22, FMs.PayItemNm23, FMs.PayItemOF23,
FMs.PayItemTX23, FMs.PayItemCD23, FMs.PayItemPen23, FMs.PayItemNm24, FMs.PayItemOF24, FMs.PayItemTX24, FMs.PayItemCD24, FMs.PayItemPen24,
FMs.PayItemNm25, FMs.PayItemOF25, FMs.PayItemTX25, FMs.PayItemCD25, FMs.PayItemPen25, FMc.PensionEmployee, FMc.PensionEmployer,
FMc.LeaveAllowance, FMc.PAYE, FMc.TTaxable, FMc.NetPay, FMc.Scheme
FROM         dbo.EmpTbl AS Et INNER JOIN
dbo.Fin_PRIndvPay AS FMc ON Et.HashKey = FMc.EmpID INNER JOIN
dbo.Fin_PRSettings AS FMs ON FMc.Scheme = FMs.HashKey
WHERE     (Et.EmpStatus = 'Active') AND (Et.Status IN ('A', 'U')) AND (FMc.Status = 'A') AND FMc.HashKey='".$EditID."'";

include '../login/dbOpen2.php';
while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
//$Script="Select MMsg from MailTemp where MSub='PaySlip'";
//$PaySlip = ScriptRunner($Script,"MMsg");

$Script="Select MMsg, MSub from MailTemp where HashKey='36cc94aa012f06f92914645ae3e072d9'"; //MSub='TimeOff Approved'";
$PaySlip=ScriptRunner($Script,"MMsg");
$Subj = ScriptRunner($Script,"MSub"); //"Payslip for the month of ".$row2['PMth'];
$Subj=str_replace('#MonthPaid#',$row2['PMth'],$Subj);

for ($i=1;$i<=25;$i++)
{
if ($row2['PayItemCD'.$i]=="CR" && $row2['PayItemOF'.$i]==1)
{$QueryStrCR=$QueryStrCR."<tr><td><b>".$row2["PayItemNm".$i]."</b></td><td>".$row2["PayItem".$i]."</td></tr>";}

if ($row2['PayItemCD'.$i]=="DR" && $row2['PayItemOF'.$i]==1)
{$QueryStrDR=$QueryStrDR."<tr><td><b>".$row2["PayItemNm".$i]."</b></td><td>".$row2["PayItem".$i]."</td></tr>";}
}

//$HKey=$row2['HKey'];
$QueryStr=$QueryStr."<tr><td><b>Gross</b></td><td>".$row2['Gross']."</td></tr>";
$QueryStr=$QueryStr."<tr><td><b>Pension</b></td><td>".$row2['PensionEmployee']."</td></tr>";
$QueryStr=$QueryStr."<tr><td><b>PAYE</b></td><td>".$row2['PAYE']."</td></tr>";
$QueryStr=$QueryStr."<tr><td><b>Net Pay</b></td><td>".$row2['NetPay']."</td></tr>";

//$LeaveAllowance=$row2['LeaveAllowance'];
//$PensionEmployer=$row2['PensionEmployer'];
//if (floatval($LeaveAllowance)>0){$LeaveAllowance=$LeaveAllowance/12;} else{$LeaveAllowance=0;}
//$TTaxable=$row2['TTaxable'];
//if (floatval($TTaxable)>0){$TTaxable=$TTaxable/12;} else{$TTaxable=0;}

$PaySlip='<TABLE>'.$QueryStrCR.$QueryStrDR.$QueryStr.'</TABLE>';
}
include '../login/dbClose2.php';

//echo $PaySlip;
//                    exit;

$To=ScriptRunner($Script_ID,"Email");
$Bdy = $PaySlip;
$Subj="";
//$Subj = "Payslip for the month of ".$row2['PMth'];
$Atth="";

QueueMail($To,$Subj,$Bdy,$Atth,''); //Sends the mail
echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee payment(s) authorized successfully. Payslip sent', 'green'); }</script>");
}
}
}
}

 */ elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {

        if (ValidateURths("PAY SALARY" . "D") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                //exit();
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT EmpID, HashKey from Fin_PRCore where HashKey='" . $EditID . "'";
                $EditID = ScriptRunner($Script_ID, "HashKey");

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [Fin_PRCore] WHERE [HashKey] = '" . $EditID . "'") == true) {
                    //Check if account is customer account and if SMS alert is set to fire
                    $Script = "UPDATE [Fin_PRCore]
					SET [Status] = 'D'
					,[DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[DeletedDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("DELETE", "Employee payment(s) deleted for " . ScriptRunner($Script_ID, "EmpID"));

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee pay template deleted successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
        include '../main/clr_val.php'; /* CLEARS $EditID */
    }
} ?>


<script type="text/javascript">
    function GetTotal_PAYE() {
        // alert('here');
        //SORT OUT ALL VALUES
        var GrossPay = document.getElementById("Gross").value.replace(',', '');
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);
        GrossPay = parseFloat(GrossPay.replace(',', '')).toFixed(2);

        var TTaxable_ = document.getElementById("TTaxable_H").value.replace(',', '');
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        TTaxable_ = parseFloat(TTaxable_.replace(',', '')).toFixed(2);
        //alert(TTaxable_);

        var Pension = document.getElementById("PensionEmployee").value.replace(',', '');
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);
        Pension = parseFloat(Pension.replace(',', '')).toFixed(2);

        var OverTime = document.getElementById("PayItem4").value.replace(',', '');
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);
        OverTime = parseFloat(OverTime.replace(',', '')).toFixed(2);

        var WeekEnd = document.getElementById("PayItem6").value.replace(',', '');
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);
        WeekEnd = parseFloat(WeekEnd.replace(',', '')).toFixed(2);

        var DR = document.getElementById("H_DR").value.replace(',', '');
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);
        DR = parseFloat(DR.replace(',', '')).toFixed(2);

        var Total_DR_NTax = document.getElementById("H_TDR_NTaxable").value.replace(',', '');
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);
        Total_DR_NTax = parseFloat(Total_DR_NTax.replace(',', '')).toFixed(2);

        var LeaveAllowance_DisMth = document.getElementById("LeaveAllowance").value.replace(',', '');
        //alert(LeaveAllowance_DisMth);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);
        LeaveAllowance_DisMth = parseFloat(LeaveAllowance_DisMth.replace(',', '')).toFixed(2);

        /*
        	var Leave=document.getElementById("LeaveAllowance_H").value.replace(',','');
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        	Leave = parseFloat(Leave.replace(',','')).toFixed(2);
        */

        NetPay = 0;
        if (parseFloat(OverTime) > 0) {
            GrossPay = parseFloat(GrossPay) + parseFloat(OverTime);
        }

        if (parseFloat(WeekEnd) > 0) {
            GrossPay = parseFloat(GrossPay) + parseFloat(WeekEnd);
        }


        //Total Relief is 200k plus 20% of gross or 1% of Gross, which ever is greater
        GrossPay = GrossPay * 12;
        Pension = Pension * 12;
        Leave = Leave * 12;

        Total_DR_NTax = (Total_DR_NTax * 1);
        //alert(Total_DR_NTax);
        TRelief = 200000 + (parseFloat(GrossPay) * (20 / 100)) + parseFloat(Pension) + Total_DR_NTax;
        //alert(TRelief);
        $TotTaxable = (parseFloat(TTaxable_) * 12) - parseFloat(TRelief);
        console.log($TotTaxable);

        //Get the total amount for all relief
        //Get the total taxable pay
        $PAYE = 0;
        $PAYE0 = parseFloat(document.getElementById("S_PAYE0").value).toFixed(2) * 1;
        $PAYE1 = parseFloat(document.getElementById("S_PAYE1").value).toFixed(2) * 1;
        $PAYE2 = parseFloat(document.getElementById("S_PAYE2").value).toFixed(2) * 1;
        $PAYE3 = parseFloat(document.getElementById("S_PAYE3").value).toFixed(2) * 1;
        $PAYE4 = parseFloat(document.getElementById("S_PAYE4").value).toFixed(2) * 1;
        $PAYE5 = parseFloat(document.getElementById("S_PAYE5").value).toFixed(2) * 1;
        $PAYE6 = parseFloat(document.getElementById("S_PAYE6").value).toFixed(2) * 1;
        $PAYE7 = parseFloat(document.getElementById("S_PAYE7").value).toFixed(2) * 1;

        $PAYE_Comm0 = parseFloat(document.getElementById("S_PAYE_Comm0").value).toFixed(2) * 1;
        $PAYE_Comm1 = parseFloat(document.getElementById("S_PAYE_Comm1").value).toFixed(2) * 1;
        $PAYE_Comm2 = parseFloat(document.getElementById("S_PAYE_Comm2").value).toFixed(2) * 1;
        $PAYE_Comm3 = parseFloat(document.getElementById("S_PAYE_Comm3").value).toFixed(2) * 1;
        $PAYE_Comm4 = parseFloat(document.getElementById("S_PAYE_Comm4").value).toFixed(2) * 1;
        $PAYE_Comm5 = parseFloat(document.getElementById("S_PAYE_Comm5").value).toFixed(2) * 1;
        $PAYE_Comm6 = parseFloat(document.getElementById("S_PAYE_Comm6").value).toFixed(2) * 1;
        $PAYE_Comm7 = parseFloat(document.getElementById("S_PAYE_Comm7").value).toFixed(2) * 1;


        // Applies the flat rate of 1%
        if ($GrossPay <= $PAYE0) {
            $PAYE = ($GrossPay * $PAYE_Comm0) / 100;
        }

        // Applies first band of 7%
        if (($GrossPay > ($PAYE0)) && ($TotTaxable < ($PAYE1))) {
            $PAYE_Rem = $TotTaxable;
            $PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
        }

        //alert($TotTaxable);
        /*
        if ($TotTaxable < $PAYE0)
        {$PAYE = (GrossPay * $PAYE_Comm0)/100;}

        //Amount taxed on first band
        if (($TotTaxable > ($PAYE0)) && ($TotTaxable < ($PAYE1)))
        {$PAYE_Rem = $TotTaxable; $PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);}
        */

        if ($TotTaxable >= ($PAYE1)) {
            $PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
        }

        //If Amount Left is not up to SECOND BAND (PAYE2)
        if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2)) {
            $PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
        }

        //If Amount Left is not up to THIRD BAND (PAYE3)
        if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
            $PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
            $PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
        }

        //If Amount Left is not up to FOURTH BAND (PAYE4)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
            $PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
        }

        //alert ($TotTaxable);

        //If Amount Left is not up to FIFTH BAND (PAYE5)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
            $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
            $PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
        }
        //alert ($PAYE);

        //If Amount Left is not up to SIXTH BAND (PAYE6)
        //if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)))
        //{$PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5); $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

        if ($TotTaxable > $PAYE6) {
            $PAYE = $PAYE + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
        }

        //if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6))
        //{$PAYE = $PAYE + ((($TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)) * $PAYE_Comm6)/100);}
        //{$PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

        //alert($PAYE);
        //alert ($PAYE);
        //alert ($Pension);

        //alert (document.getElementById("LeaveField").value);
        /*if (document.getElementById("LeaveField").value=='on') //If Leave allowance is inclusive then add Pension to the Net
        {
        	$TotAllowance = $PAYE + $Pension;
        }
        else
        {
        	$TotAllowance = $PAYE;
        }

        alert("Gross:" + $GrossPay);
        alert("Total_DR:" + $Total_DR);
        alert("PAYE:" + $PAYE);
        alert("Pension:" + $Pension);
        */

        $NetPay = ((GrossPay - $Total_DR) - $PAYE - $Pension); // - $LeaveA

        /*alert ("N="+$NetPay);
        alert ($PAYE);
        alert ($Pension);
        alert ($GrossPay);
        alert ("DR="+$Total_DR);
        alert ($TotAllowance);
        */

        document.getElementById("TTaxable").value = thousand_separator(parseFloat($TotTaxable).toFixed(2));
        document.getElementById("TRelief").value = thousand_separator(parseFloat($TRelief).toFixed(2));
        document.getElementById("PAYE").value = thousand_separator(parseFloat($PAYE).toFixed(2));
        document.getElementById("NetPay").value = thousand_separator(parseFloat($NetPay).toFixed(2));

        //document.getElementById("PensionEmployee").value = thousand_separator(parseFloat($NetPay).toFixed(2));

        /*	document.getElementById("TTaxable").value.replace(/\,/g,'') +
        	document.getElementById("PensionEmployee").value.replace(/\,/g,'') +
        	document.getElementById("NetPay").value.replace(/\,/g,'') +
        	document.getElementById("LeaveAllowance").value.replace(/\,/g,'') +
        	document.getElementById("Gross").value.replace(/\,/g,'') +
        	document.getElementById("paye_d").value.replace(/\,/g,'') +
        	document.getElementById("pension").value;
        */

        //alert (GrossPay);
        //alert (Pension);
        //alert ($PAYE);

        //NetPay = ((GrossPay -  Pension)-$PAYE); // - parseFloat(Leave));
        //NetPay = (NetPay + parseFloat(LeaveAllowance_DisMth))
        //alert(NetPay);
        /*alert ("Net="+NetPay);
        alert ("PAYE="+$PAYE);
        alert ($Pension);
        //alert ($GrossPay);
        alert ("DR="+$Total_DR);
        //alert ($TotAllowance);
        //*/

        document.getElementById("Gross").value = thousand_separator((parseFloat(GrossPay) / 12).toFixed(2));
        //document.getElementById("TTaxable").value = thousand_separator(parseFloat($TotTaxable).toFixed(2));
        document.getElementById("TRelief").value = thousand_separator(parseFloat(TRelief / 12).toFixed(2));
        //document.getElementById("PAYE").value = thousand_separator(parseFloat($PAYE).toFixed(2));
        document.getElementById("NetPay").value = thousand_separator((parseFloat(NetPay) / 12).toFixed(2));
    }

    function thousand_separator(v, s, d) {
        if (arguments.length == 2) d = ".";
        if (arguments.length == 1) {
            s = ",";
            d = ".";
        }

        v = v.toString();
        // separate the whole number and the fraction if possible
        var a = v.split(d);
        var x = a[0]; // decimal
        var y = a[1]; // fraction
        var z = "";
        var l = x.length;
        while (l > 3) {
            z = "," + x.substr(l - 3, 3) + z;
            l -= 3;
        }
        z = v.substr(0, l) + z;

        if (a.length > 1)
            z = z + d + y;
        return z;
    }

    $(function() {
        $(document).tooltip();
    });

    $(function() {
        // $("#PayMonth").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y",
        //  maxDate: "+1D",
        //  dateFormat: 'MM yy',yearRange: "-75:+75"})
        // 	$("#PayMonth").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y",
        //      maxDate: "+1D",
        //       dateFormat: 'MM yy'})
        //   });
        $("#PayMonth").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'MM yy'
        })
    });
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<style>
    h6 {
        font-weight: 550;
    }
</style>

<body oncontextmenu="return false;" onLoad="GetTotal_PAYE();" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
    <?php // echo '</td></tr></table>';
    ?>
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" onSubmit="GetTotal()">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4 col-2 text-center" style="margin-top: 1%">
                    </div>
                    <div class="col-md-4 col-8 text-center" style="margin-top: 1%">
                        <h5>
                            Employee Salary Payment
                        </h5>
                    </div>
                    <div class="col-md-4 col-2">
                        <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Clear" style="margin-top: 1%" />
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php

                    if ($EditID != '' && $EditID != '--') {
                        if ($PayScheme_ !== '') {
                            $dbOpen2_ = "SELECT count(*) Ct from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                            if (ScriptRunner($dbOpen2_, "Ct") == 0) {
                                echo ("<script type='text/javascript'>{ parent.msgbox('Employee payroll group/Payment template has been unauthorized', 'red'); }</script>");
                            }
                        }

                        $CurrencyType_query = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $CurrencyType = ScriptRunner($CurrencyType_query, "CurrencyType");
                        $group_nam = ScriptRunner($CurrencyType_query, "GName");

                        $Script_Curr = "SELECT * FROM Masters WHERE HashKey='" . trim($CurrencyType) . "'";
                        $CurrName = ScriptRunner($Script_Curr, "Val1");
                        $CurrCode = ScriptRunner($Script_Curr, "ItemCode");
                        $ExchRate = ScriptRunner($Script_Curr, "Desc");
                        if (!isset($CurrName)) {
                            $currency_display = "Naira (NGR)";
                        } else {
                            $currency_display = "<b>" . $CurrName . "</b> exchange rate: 1 " . $CurrName . " = " . $ExchRate . " Naira";
                        }
                        //$currency_display = $EditID;
                        echo '<div class="col-md-6">
						<p class="">Currency Type:
						<span>' . $currency_display . '</span></p>
						</div>';

                        echo '<div class="col-md-6">
						<p class="">Payroll Group:
						<span>' . $group_nam . '</span></p>
			</div>';
                    }
                    ?>
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Select Staff <span style="color: red">*</span> :</label>
                            <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
                                <select name="EmpID" class="form-control" id="EmpID">
                                    <option value="">--</option>
                                    <?php
                                    if ($EditID != '' && $EditID != '--') {
                                        $SelID = $EmpID_;
                                    } else {
                                        $EmpID_ = '';
                                    }
                                    $dbOpen2 = "SELECT HashKey, (SName+' '+FName+' '+ONames +' ['+Convert(Varchar(24),EmpID)+']') as Nm from EmpTbl where (EmpID<>'000000' and Status in ('A','U') and EmpStatus='Active') order by SName";
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($SelID == $row2['HashKey']) {
                                            echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
                                        } else {
                                            echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . "" . '</option>';
                                        }
                                    }
                                    echo '<input name="AcctNo" id="AcctNo" type="hidden" value="' . $EmpID_ . '" />';
                                    include '../login/dbClose2.php';
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Select Month:</label>
                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                <?php

                                if (strlen($EditID) == 32 && $CmdTyp == "Edit") {
                                    echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value="' . ScriptRunner($Script_Edit, "PM") . '"   autocomplete="off"  />';
                                } else {
                                    if ((isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") || !isset($_REQUEST["PayMonth"])) {
                                        echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value=""  autocomplete="off" />';
                                    } elseif (isset($_REQUEST["PayMonth"])) {

                                        echo '<input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" onClick="GetMonth();" value="' . ECh($_REQUEST["PayMonth"]) . '"  autocomplete="off" />';
                                    }
                                    if ((ValidateURths("PAY SALARY" . "A") == true)) {

                                        echo '&nbsp; <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Generate Schedule" />';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="clearfix" ></div> -->
                <?php
                $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                include '../login/dbOpen2.php';
                while ($row2 = @sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { ?>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Gross Pay</h6>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm1']; ?><span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = 'PayItem1;';
                                    $DR = 0;
                                    if ($row2['PayItemCD1'] == "CR") {
                                        $CR = $CR . 'PayItem1;';
                                    } elseif ($row2['PayItemCD1'] == "DR") {
                                        $DR = $DR + $row2['PayItem1'];
                                    }

                                    if ($row2['PayItemPen1'] == 1) {
                                        $TPension = $TPension . 'PayItem1;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        //echo $Script_Edit;
                                        //echo ScriptRunner($Script_Edit,"PayItem1");
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem1");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX1'] == 1 && $row2['PayItemAn1'] == 0) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }

                                        echo '<input type="text" name="PayItem1" id="PayItem1" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem1" id="PayItem1" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem1" id="PCent_PayItem1" value="' . $row2["PayItem1"] . '" />'; ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm1"] . ": " . $row2["PayItem1"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm3']; ?><span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = $AllItems . 'PayItem3;';
                                    if ($row2['PayItemCD3'] == "CR") {
                                        $CR = $CR . 'PayItem3;';
                                    } elseif ($row2['PayItemCD3'] == "DR") {
                                        $DR = $DR + $row2['PayItem3'];
                                    }

                                    //if ($row2['PayItemTX3']==1)
                                    //{$TTaxable=$TTaxable.'PayItem3;';}
                                    if ($row2['PayItemPen3'] == 1) {
                                        $TPension = $TPension . 'PayItem3;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem3");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX3'] == 1) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }
                                        echo '<input type="text" name="PayItem3" id="PayItem3" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem3" id="PayItem3" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem3" id="PCent_PayItem3" value="' . $row2["PayItem3"] . '" />';
                                    ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm3"] . ": " . $row2["PayItem3"] . "% of Gross"; ?>" name="AuthInfo1" id="AuthInfo1">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row ">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;"><?php echo $row2['PayItemNm2']; ?>
                                    <span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php
                                    $AllItems = $AllItems . 'PayItem2;';
                                    if ($row2['PayItemCD2'] == "CR") {
                                        $CR = $CR . 'PayItem2;';
                                    } elseif ($row2['PayItemCD2'] == "DR") {
                                        $DR = $DR + $row2['PayItem2'];
                                    }

                                    //if ($row2['PayItemTX2']==1)
                                    //{$TTaxable=$TTaxable.'PayItem2;';}
                                    if ($row2['PayItemPen2'] == 1) {
                                        $TPension = $TPension . 'PayItem2;';
                                    }

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem2");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }

                                            if ($row2['PayItemTX2'] == 1) {
                                                $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                            }
                                        }
                                        echo '<input type="text" name="PayItem2" id="PayItem2"  maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem2" id="PayItem2"  maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
								<input type="hidden" name="PCent_PayItem2" id="PCent_PayItem2" value="' . $row2["PayItem2"] . '" />';
                                    ?>
                                    <span class="input-group-btn ">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm2"] . ": " . $row2["PayItem2"] . "% of Gross"; ?>" name="AuthInfo21" id="AuthInfo21">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-primary " style="padding-right: 0px; font-weight: bolder;">Gross Pay
                                    :</label>
                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                    <?php

                                    //  var_dump($Script_Edit);
                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Generate Schedule") {

                                            $re_gross = ScriptRunner($Script_Edit, "real_gross");
                                            $kval = isset($re_gross) && $re_gross !== '' ? ScriptRunner($Script_Edit, "real_gross") : ScriptRunner($Script_Edit, "Gross");
                                            // var_dump($kval);

                                        } else {

                                            $kval = ScriptRunner($Script_Edit, "Gross");
                                        }

                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $gross_for_anniversary = $kval;

                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $gross_for_anniversary = $kval;
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        }

                                        $GrossPay = $kval * 12;

                                        echo '<input type="text" name="Gross" id="Gross" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="Gross" id="Gross" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    //        echo '<input type="text" name="Gross" id="Gross" size="18" maxlength="25" class="subHeader" value="'.number_format((ScriptRunner($Script_Edit,"Gross")+0.0),2,'.',',').'" onchange="GetTotal()" />';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Allowances & Reliefs
                                <?php
                                $imgID = "MastersDepartment";
                                if ($ModMaster == true) {
                                    $imgPath = "fa fa-plus";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters_acc.php?GroupName=AllowancesReliefs&Emp=$SelID&LDb=Single&amp;Elmt=Currency',320,400,'No')";
                                } else {
                                    $imgPath = "fa fa-info";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "";
                                }
                                echo '<button type="button" class="btn btn-default  btn-xs" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
                                                                <i class="' . $imgPath . '"></i>
                                                            </button> ';
                                ?>
                            </h6>
                        </div>
                        <?php
                        //exit('stop here '.$EditID);
                        $EvOd = 0;
                        for ($i = 4; $i <= 25; $i++) {
                            // Hail Lawrence

                            if (($row2['PayItemCD' . $i] == "CR"  || $row2['PayItemCD' . $i] == "RL") && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                // if ($row2['PayItemTX'.$i]==1)
                                // {$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                                                                            <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                        ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                    <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
                                                    </td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            } // End of checking taxable and active

                            // Added condition for annual pay

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANN' && $Employed_date == $Pay_month) {
                                // echo $row2['PayItemAnType'.$i];
                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
                                                                                                            <input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
                                                            <div class="form-group row">
                                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
                                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of annual pay condition

                            // last month pay begins
                            $lastMonth = '12';
                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'DEC' && $lastMonth == $Pay_month) {

                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of first month pay

                            // First month pay begins
                            $firstMonth = '1';

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'JAN' && $firstMonth == $Pay_month) {

                                $AllItems = $AllItems . 'PayItem' . $i . ';';

                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of last month pay

                            // Get the authorized leave type for the fellow
                            // For leave bonus

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANL' && $approved == 'A') {
                                // && $approved == 'A' && $PayBonus != 'A'
                                $AllItems = $AllItems . 'PayItem' . $i . ';';


                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension . 'PayItem' . $i . ';';
                                }

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) { // for overtime
                                        if ($i == 4) {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='O'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= OVERTIME ENDS HERE  =======================================
                                        //======================================= WEEKEND BEGINS HERE  =======================================
                                        if ($i == 5) //WEEKEND
                                        {
                                            if ($CmdTyp == "Edit") {
                                                $Dt = ScriptRunner($Script_Edit, "PM");
                                                if ($Dt == "") {
                                                    $Dt = "01 Jan 2015";
                                                }
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'" . $Dt . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                                //echo $Script;
                                            } else {
                                                $Script = "Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $EmpID_ . "' and Status in ('A') and RecType='W'";
                                            }
                                            $OTimeHrs = ScriptRunner($Script, "Sm"); //Get Total Overtime Hours
                                            if (!is_numeric($OTimeHrs)) {
                                                $OTimeHrs = 0;
                                            } // Set value of overtime hours to 0 if value is not numeric

                                            $Script = "select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $EmpID_ . "' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)

                                            if (ScriptRunner($Script, "UseType") == "Percent") {
                                                $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                $BasicPay = ScriptRunner($Script_Edit, "PayItem1");
                                                if ($BasicPay > 0) {
                                                    $BasicPay = $BasicPay / 12;
                                                } // Get Annual Basic and divide by 12 to give monthly basic

                                                $HourlyPay = ($BasicPay * $Amt) / 100; // Get the Hourly amount paid
                                                $kval = number_format($HourlyPay * $OTimeHrs, 2);
                                            } elseif (ScriptRunner($Script, "UseType") == "Value") {
                                                $OTimeAmt = ScriptRunner($Script, "OverTime");
                                                if (!is_numeric($OTimeAmt)) {
                                                    $OTimeAmt = 0;
                                                }

                                                if (is_numeric($OTimeAmt)) {
                                                    $kval = number_format($OTimeAmt * $OTimeHrs, 2);
                                                } //Get Overtime Hours multiplied by Overtime set amount for employee level
                                                else {
                                                    $kval = "0.0";
                                                }
                                            }
                                            $OTimeAmtPay = $kval * 12;
                                        }
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        //======================================= WEEKEND ENDS HERE  =======================================
                                        else {
                                            //$OTimeAmtNet=0;
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }

                                                //if ($row2['PayItemTX'.$i]==1)
                                                //{$TTaxable=$TTaxable + $kval;}
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        // Get total bonus for each employee
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } // End of if selection

                                    else {

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
																				<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }
                                        // Get the total bonus package
                                        $GP_convert = floatval(str_replace('.', '.', str_replace(',', '', $kval)));
                                        (float) $total_bonus += $GP_convert;

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    } ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                        <?php echo '</div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                            //  End of annual pay condition

                        }
                        echo '</div><hr>';
                        //------------------------------------------- Individual specific Allowance, not tied to group ---------------------------------
                        $Script = "Select * from Masters where (ItemName='AllowancesReliefs' and ItemCode='$SelID' and Status<>'D' and Val1<>'')";

                        if (trim(ScriptRunner($Script, "Val1")) != "") {

                            $dbOpen3 = ("SELECT * FROM Masters where (ItemName='AllowancesReliefs' and ItemCode='$SelID' and Status<>'D' and Val1<>'') ORDER BY Val1");
                            include '../login/dbOpen3.php';

                            echo ' <div class="row">
			                    <div class="col-md-6">
			                    <p>Employee specific monthly Allowances & Reliefs</p>';
                            $n = 1;
                            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                echo '
							<div class="form-group row">
			                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">' . $row3['Val1'] . '
			                        <span style="color: red">*</span>:</label>
			                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
			                        <input type="text" readonly name="add_value' . $n . '" id="add_value' . $n . '" maxlength="25" class="form-control" value="' . number_format($row3['Desc'], 2, '.', ',') . '">
									<input type="hidden" name="add_name' . $n . '" value="' . $row3['Val1'] . '">
			                    </div>
			                </div>
						</div>
						<div class="col-md-6"></div>
			            </div>';
                                $n++;
                            }
                        }
                        ?>
                        <!--  END OF ALLOWANCE AND RELIEF -->


                        <div class="row">
                            <!--++++++++++++++++++++++++++++++++++++++++ ANNIVERSARY BONUS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <?php
                            $Script_for_anniversary = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";

                            $anniversary_percent = Scriptrunner($Script_for_anniversary, "AnniversaryAllw");
                            $anniversary_switch = Scriptrunner($Script_for_anniversary, "AnniversaryField");

                            $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $EmpID_ . "'";
                            $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
                            $Pay_month = date("n", strtotime(ECh($_REQUEST["PayMonth"])));

                            if ($Employed_date == $Pay_month && $anniversary_switch == "on") {

                                $bonus = floatval(($gross_for_anniversary) * ($anniversary_percent / 100));
                                //$bonus = number_format($bonus,2);
                            } else {
                                $bonus = floatval(($gross_for_anniversary * ($anniversary_percent / 100)) / 12);
                                //$bonus = number_format($bonus,2);
                            }

                            ?>
                            <!--I commented out annivasary bonus  -->
                            <!-- <div class="col-md-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Anniversary Bonus
							<span style="color: red">*</span>:</label>
						<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
							<?php
                            // if ($CmdTyp=="Open"){
                            // echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value=""  />';
                            // }
                            // else{

                            // $Script_Edit ="select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='".$EditID."' and Status not in ('A','D')";
                            // $bonus=ScriptRunner($Script_Edit,"AnniversaryBonus");
                            // echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value=""  />';
                            // }
                            ?>
						</div>
					</div>
				</div> -->

                            <!-- SPLITS THE LEAVE BONUS INTO THE 12 MONTHS OF THE YEAR -->
                            <!--<div class="col-md-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label" style="padding-right: 0px;">ANNIVERSARY BONUS
							<span style="color: red">*</span>:</label>
						<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
							<?php
                            //echo '<input type="text" name="anniversary" id="anniversary" size="25" maxlength="25" class="form-control" value="'.number_format($bonus,2).'"  />';
                            ?>
						</div>
					</div>
				</div>-->

                            <!--+++++++++++++++++++++++++++++++++++++++++++++++++ END OF ANNIVERSARY BONUS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
                            <?php
                            ////////////////////////////////////// OVERTIME BEGINS HERE

                            if (ValidateURths("OVERTIME SETTING" . "V") == true) { // Check for Overtime Setting view right
                            ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" style="padding-right: 0px;">OVERTIME
                                            <span style="color: red">*</span>:</label>
                                        <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                            <?php

                                            $date_for_overtime = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));

                                            $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
								Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut , FA.*
								from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND Et.HashKey = '$SelID' AND CONVERT(CHAR(7),FA.RecDate,120) = '$date_for_overtime' and FA.[o_status] = 'A'  ";

                                            // var_dump($dbOpen2);
                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                                $amount_over_pay = $row2['OverTimeAmount'];

                                                $TAmtOverTime += ($amount_over_pay);
                                            } // End of While loop

                                            include '../login/dbClose2.php';

                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="overtime_now" id="overtime_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtOverTime, 2) . '" readonly/>';
                                            } else {
                                                $Script_Lateness = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                                $TAmtOverTime = ScriptRunner($Script_Lateness, "OverTimeAmount");
                                                echo '<input type="text" name="overtime_now" id="overtime_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtOverTime, 2) . '" readonly/>';
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <!-- End of Overtime form row -->
                    <?php
                            } // Close if condition for checking attendance view right
                    ?>

                    <?php if ($EvOd == 1) {
                        echo '
				<td>&nbsp;</td><td>&nbsp;</td></tr>';
                    }
                    //    }
                    ?>


                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h6 class="page-header">Deductions
                                <?php
                                $imgID = "MastersDepartment";
                                if ($ModMaster == true) {
                                    $imgPath = "fa fa-plus";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters_acc.php?GroupName=Deductions&Emp=$SelID&LDb=Single&amp;Elmt=Currency',320,400,'No')";
                                } else {
                                    $imgPath = "fa fa-info";
                                    $Titl = "Add Allowances & Reliefs specific to just this employee";
                                    $OnClk = "";
                                }
                                echo '<button type="button" class="btn btn-default  btn-xs" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
					            		<i class="' . $imgPath . '"></i>
					        		</button> ';
                                ?>
                            </h6>
                        </div>
                        <!-- Lateness form row added to the pay monthly sheet-->
                        <?php
                        if (ValidateURths("ATTENDANCE" . "V") == true) { // Check for attendance view right

                        ?>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">LATENESS
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php

                                        $date_for_late = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));

                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut , FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND Et.HashKey = '" . $SelID . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '" . $date_for_late . "'  and FA.[l_status] = 'A'";
                                        include '../login/dbOpen2.php';

                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                            $amount_incurr_pay = $row2['LatenessAmount'];

                                            $TAmtLate += ($amount_incurr_pay);
                                        } // End of While loop

                                        include '../login/dbClose2.php';

                                        if ($CmdTyp == "Open") {

                                            echo '<input type="text" name="lateness_now" id="lateness_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtLate, 2) . '" readonly/>';
                                        } else {
                                            $Script_Lateness = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                            $TAmtLate = ScriptRunner($Script_Lateness, "LatenessAmount");
                                            echo '<input type="text" name="lateness_now" id="lateness_now" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtLate, 2) . '" readonly/>';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End of lateness form row -->
                            <!-- ABSENT STARTS HERE -->
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">ABSENT
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $absent_month = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.Status in ('A') AND FA.RecType = 'A' AND Et.HashKey = '" . $SelID . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '" . $absent_month . "' AND FA.Exempt='0' ";
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                                            $Script_set = "SELECT Gross from Fin_PRCore where EmpID='" . $row2['HKey'] . "' AND Status = 'A'";
                                            $gross = ScriptRunner($Script_set, "Gross");
                                            if ($gross) {
                                                $absentAmount = $gross / 365;
                                                if ($absentAmount < 0) {
                                                    $absentAmount = 0;
                                                }
                                                number_format($absentAmount, 2);
                                                $TAmtAbsent += ($absentAmount);
                                            }
                                        } // End of While loop

                                        include '../login/dbClose2.php';

                                        if ($CmdTyp == "Open") {

                                            echo '<input type="text" name="absent" id="absent" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtAbsent, 2) . '" readonly/>';
                                        } else {
                                            $Script_Absent = "select (Convert(varchar(3),(DateName(MM,PayMonth)))+' '+CONVERT(varchar(4),datepart(YEAR,PayMonth))) PM, * from [Fin_PRIndvPay] where HashKey='" . $EditID . "' and Status not in ('A','D')";
                                            $TAmtAbsent = ScriptRunner($Script_Absent, "AbsentAmount");
                                            echo '<input type="text" name="absent" id="absent" size="25" maxlength="25" class="form-control" value="' . number_format((float) $TAmtAbsent, 2) . '" readonly/>';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- ABSENT ENDS HERE -->
                        <?php
                        } // Close if condition for checking attendance view right

                        ?>
                        <?php
                        $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $row2 = Scriptrunnercous($dbOpen2);

                        $EvOd = 0;
                        for ($i = 4; $i <= 25; $i++) {
                            if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
                                $AllItems = $AllItems . 'PayItem' . $i . ';';
                                if ($row2['PayItemCD' . $i] == "CR") {
                                    $CR = $CR . 'PayItem' . $i . ';';
                                } elseif ($row2['PayItemCD' . $i] == "DR") {
                                    $DR = $DR + $row2['PayItem' . $i];
                                    if ($row2['PayItemTX' . $i] == 0) {
                                        $TDR_NTaxable = $TDR_NTaxable . 'PayItem' . $i . ';';
                                    }
                                }

                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable.'PayItem'.$i.';';}
                                //if ($row2['PayItemPen'.$i]==1)
                                //{$TPension=$TPension.'PayItem'.$i.';';}

                                $EvOd = $EvOd + 1;
                                if ($EvOd == 1) {
                                    $Script = "Select PayItemNm" . $i . " from [Fin_PRCore] where EmpID='" . $EditID . "' and Status='L'";
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">';

                                    if ($EditID != "" || $EditID > 0) //LOAN repayment
                                    {
                                        if ($i == 7) {
                                            // var_dump('here');
                                            /*
                        $Script_Loan="Update LnIndvOff set Status='B', Paid=ExpPay where EID='".$EmpID_."' and Status='A' and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))";
                        ScriptRunnerUD($Script_Loan,"Sm");
                        //on authorization UPDATE outstanding Payment in LnDetails table

                        $Script_Loan="Select ExpPay from LnIndvOff where EID='".$EmpID_."' and Status in ('B') and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))"; //Status 'B' menas BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid

                        //                $Script_Loan="Select ExpPay from LnIndvOff where EID='".$EmpID_."' and Status='A' and (Month(ExpPayDate) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(ExpPayDate) = YEAR('".ECh($_REQUEST["PayMonth"])."'))";
                        //echo $Script_Loan;

                        $LoanAmt=ScriptRunner($Script_Loan,"ExpPay");
                         */

                                            if (is_numeric($LoanAmt)) {
                                                $kval = number_format($LoanAmt, 2, '.', ',');
                                            } else {
                                                $kval = "0.00";
                                            }
                                        } else {
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    $kval = number_format($kval / 12, 2, '.', ',');
                                                } else {
                                                    $kval = number_format($kval, 2, '.', ',');
                                                }
                                            } else {
                                                $kval = "0.00";
                                            }
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '			<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($i != 5) {
                                        //Loan is not taxable
                                        //if ($row2['PayItemTX'.$i]==1)
                                        //{$TTaxable=$TTaxable + floatval(str_replace(",","",$kval));}
                                    }
                        ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                                <?php echo '</div></div></div>';
                                } elseif ($EvOd == 2) {
                                    echo '<div class="col-md-6">
								<div class="form-group row">
								<label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row2['PayItemNm' . $i] . ':<span style="color: red">*</span>:</label>
								<div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">';
                                    if ($EditID != "" || $EditID > 0) {
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PayItem" . $i);
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                $kval = number_format($kval / 12, 2, '.', ',');
                                            } else {
                                                $kval = number_format($kval, 2, '.', ',');
                                            }
                                        } else {
                                            $kval = "0.00";
                                        }

                                        if ($row2['PayItemTX' . $i] == 1) {
                                            $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                        }

                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="' . $kval . '" onchange="GetTotal()" />';
                                    } else {
                                        echo '<input type="text" name="PayItem' . $i . '" id="PayItem' . $i . '" size="25" maxlength="25" class="form-control" value="" onchange="GetTotal()" />';
                                    }
                                    echo '
						<input type="hidden" name="PCent_PayItem' . $i . '" id="PCent_PayItem' . $i . '" value="' . $row2["PayItem" . $i] . '" />
						</td><td>';

                                    if ($row2['PayItemTX' . $i] == 1) {
                                        $TTaxable = $TTaxable + floatval(str_replace(",", "", $kval));
                                    }
                                ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" Title="<?php echo $row2["PayItemNm" . $i] . ": " . $row2["PayItem" . $i] . "% of Gross"; ?>" name="AuthInfo<?php echo $i; ?>" id="AuthInfo<?php echo $i; ?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </span>
                        <?php echo '</div></div></div></div>';
                                    $EvOd = 0;
                                }
                            }
                        }

                        //--------------------------------------------------------------- Individual specific Deductions, not tied to group ---------------------------------
                        $Script = "Select * from Masters where (ItemName='Deductions' and ItemCode='$SelID' and Status<>'D' and Val1<>'')";

                        if (trim(ScriptRunner($Script, "Val1")) != "") {

                            $dbOpen3 = ("SELECT * FROM Masters where (ItemName='Deductions' and ItemCode='$SelID' and Status<>'D' and Val1<>'') ORDER BY Val1");
                            include '../login/dbOpen3.php';
                            echo ' <div class="row">
					                    <div class="col-lg-md-6">
					                    <p> Employee specific monthly Deductions</p>';
                            $m = 1;
                            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                echo '
						<div class="form-group row">
			                    <label class="col-sm-5 col-form-label" style="padding-right: 0px;">' . $row3['Val1'] . '
			                        <span style="color: red">*</span>:</label>
			                    <div class="col-sm-7 input-group input-group-sm" style=" padding-right: 35px">
			                        <input type="text" readonly name="remove_value' . $m . '" id="remove_value' . $m . '" size="25" maxlength="25" class="form-control" value="' . number_format($row3['Desc'], 2, '.', ',') . '">
									<input type="hidden" name="remove_name' . $m . '" value="' . $row3['Val1'] . '">
			                    </div>
			                </div>
						</div>
						<div class="col-md-6"></div>
			            </div><hr>';
                                $m++;
                            }
                        }

                        //LOAN EXTENSION
                        /*
                                                     $dbOpen7=("SELECT (select SUM(Paid) from LnIndvOff Where LID=Ld.HashKey and (convert(varchar(11),AddedDate)=convert(varchar(11),Ld.AddedDate)) and Status<>'D' Group By convert(varchar(11),AddedDate), EID, LID) Pd, Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, Convert(Varchar(11),AuthDate,106) AuthDt, * from LnDetails Ld where EID='".ECh($_REQUEST["EmpID"])."' and Status not in ('D') order by SRepayDate asc");
                                                                 */
                        if ($_REQUEST["FAction"] === "View Selected") {
                            if (isset($loan_amt)) {
                                echo ' <div class="row">
			                <div class="col-lg-12 col-md-6">
			                <p class"text-danger"><b>Loan Repayment</b></p>';
                                echo '
							<div class="form-group row">
				                    <div class="col-md-7 input-group input-group-md" style=" padding-right: 45px">
				                        <input type="text" readonly name="repay_loan" id="" size="25" maxlength="25" class="form-control" value="' . number_format($loan_amt, 2) . '">
				                    </div>
				                </div>
							</div>
							<div class="col-md-6"></div>
							</div><hr>';
                            }
                        } else {
                            $Script = "Select Count(*) Ct  from [LnDetails] where Status ='A' and LnAmt <> Paid and EID='" . $EmpID_ . "'";
                            if (ScriptRunner($Script, "Ct") > 0) {
                                //$date_paid_for = "2017-02-28";
                                //$dbOpen7=("SELECT (select (convert(varchar(11),ExpPayDate)=convert(varchar(11),$date_paid_for)), * from LnIndvOff Where LID=Ld.HashKey from LnDetails Ld where EID='".$EmpID_."' and Status not in ('D')");

                                $dbOpen7 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, * from LnIndvOff  where EID='" . $EmpID_ . "' and Status not in ('D','P','B') Order by ExpPayDate ASC");

                                // var_dump($dbOpen7);

                                include '../login/dbOpen7.php';

                                //$LnAmt = 0;
                                //$ExpPay =0;

                                //$OutSPay2= 0;
                                //$RePayAmt2 = 0;

                                echo ' <div class="row">
			                <div class="col-lg-12 col-md-6">
			                <p class"text-danger"><b>Loan Repayment</b></p>';
                                //$m=1;
                                //while($row7 = sqlsrv_fetch_array($result7,SQLSRV_FETCH_BOTH)){
                                $row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH);
                                //$OutSPay2 = $OutSPay2 + $row7['OutSPay'];
                                //$RePayAmt2 = $RePayAmt2 + $row7['RePayAmt'];
                                $ExpPay2 = $row7['ExpPay'];
                                $EID = $row7['EID'];
                                $LID = $row7['LID'];
                                $ExpPDate = $row7['ExpPD'];
                                $HashKey = $row7['HashKey'];

                                echo '
							<div class="form-group row">
								<input type="hidden" name="_pay_date" value="' . $ExpPDate . '">
								<input type="hidden" name="_ind_loan_id" value="' . $HashKey . '">
								<input type="hidden" name="_emp_hashkey" value="' . $EID . '">
				                    <label class="col-md-5 col-form-label" style="padding-right: 0px;">' . $ExpPDate . '</label>
				                    <div class="col-md-7 input-group input-group-md" style=" padding-right: 45px">
				                        <input type="text" readonly name="repay_loan" id="" size="25" maxlength="25" class="form-control" value="' . number_format($ExpPay2, 2) . '">
				                    </div>
				                </div>
							</div>
							<div class="col-md-6"></div>
							</div><hr>';

                                //$m++;
                                //}
                            }
                        }
                        //---------------------------------------------------------------------------------------------------------------------------------------------------

                        if ($EvOd == 1) {
                            echo '<td>&nbsp;</td><td>&nbsp;</td></tr>';
                        }
                        ?>
                        <?php
                        $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";
                        $row2 = Scriptrunnercous($dbOpen2);
                        ?>
                        <?php if ((int) $row2["PayItemOF20"] === 1 || (int) $row2["PayItemOF19"] === 1) : ?>

                            <div class="row" style="width: 100%;">
                                <div class="col-sm-12 text-center">
                                    <h6 class="page-header">Free Pay</h6>
                                </div>
                                <?php if ((int) $row2["PayItemOF20"] === 1) : ?>
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;"> <?php echo $row2['PayItemNm20'] ?>
                                                <span style="color: red">*</span>:</label>
                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                <?php
                                                if ($EditID != "" || $EditID > 0) {
                                                    $kval = 0;
                                                    $kval = ScriptRunner($Script_Edit, "PayItem20");
                                                    // $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                                    if ($kval > 0) {
                                                        if ($CmdTyp == "Open") {
                                                            echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        } else {
                                                            echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        }
                                                    } else {
                                                        echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                    }
                                                } else {

                                                    echo '<input type="text" name="PayItem20" id="PayItem20" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>









                                <?php if ((int) $row2["PayItemOF19"] === 1) : ?>
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;"> <?php echo $row2['PayItemNm19'] ?>
                                                <span style="color: red">*</span>:</label>
                                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                <?php
                                                if ($EditID != "" || $EditID > 0) {
                                                    $kval = 0;
                                                    $kval = ScriptRunner($Script_Edit, "PayItem19");
                                                    // $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                                    if ($kval > 0) {
                                                        if ($CmdTyp == "Open") {
                                                            echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        } else {
                                                            echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                        }
                                                    } else {
                                                        echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                    }
                                                } else {

                                                    echo '<input type="text" name="PayItem19" id="PayItem19" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>





                            </div>

                        <?php endif; ?>



                        <div class="row" style="width: 100%;">
                            <div class="col-sm-12 text-center">
                                <h6 class="page-header">Net Details</h6>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Taxable Pay
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        if ($EditID != "" || $EditID > 0) {
                                            $kval = 0;
                                            $kval = ScriptRunner($Script_Edit, "TTaxable");
                                            $TTax = $TTaxable - (ScriptRunner($Script_Edit, "TRelief") / 12);

                                            if ($kval > 0) {
                                                if ($CmdTyp == "Open") {
                                                    echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                } else {
                                                    echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                                }
                                            } else {
                                                echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        } else {

                                            echo '<input type="text" name="TTaxable" id="TTaxable" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        echo '<input type="hidden" name="TTaxable_H" id="TTaxable_H" size="25" maxlength="25" class="form-control" value="' . $TTaxable . '" onchange="GetTotal_PAYG()" />';

                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">PAYE
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PAYE");

                                        //print $kval;
                                        //exit();

                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                //$kval = $kval + $LeaveAllw;
                                                echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="PAYE" id="PAYE" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Total Relief:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        if ($CmdTyp != "Open") // Update Button active
                                        {
                                            if ($EditID != "" || $EditID > 0) {
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TRelief") + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                $GrossPay = $GrossPay + $OTimeAmtPay;

                                                echo '<input type="text" name="GrossPay_" id="GrossPay_" size="25" maxlength="25" class="form-control" value="' . $GrossPay . '" onchange="GetTotal()" />';
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        } else { // Create button active
                                            if ($EditID != "" || $EditID > 0) {

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="' . number_format((ScriptRunner($Script_Edit, "TRelief") / 12 + 0.0), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {

                                                $GrossPay = $GrossPay + $OTimeAmtPay;
                                                echo '<input type="text" name="GrossPay_" id="GrossPay_" size="25" maxlength="25" class="form-control" value="' . $GrossPay . '" onchange="GetTotal()" />';
                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';

                                                echo '<input type="text" name="TRelief" id="TRelief" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Allowance
                                        <span style="color: red">*</span>:</label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "LeaveAllowance");
                                        if ($CmdTyp != "Open") // Update Button active
                                        {

                                            //++++++++++++++++++++ START +++++++++++++++
                                            echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class=" form-control TextBoxText_Medium_Comp" value="' . number_format($kval, 2, '.', ',') . '" onchange="GetTotal()" />';

                                            //++++++++++++++++++++ END +++++++++++++++++

                                        } else //Create button active
                                        {
                                            $kval = $kval / 12; //Reading annual leave figure. Divide by 12 to get the monthly leave figure
                                            $kval_div = $kval;
                                            $PayMonth = '27 ' . ECh($_REQUEST["PayMonth"]);

                                            //++++++++++++++++++++ START +++++++++++++++
                                            /*
        echo '<input type="hidden" name="LeaveAllowance_H" id="LeaveAllowance_H" size="25" maxlength="25" class="TextBoxText_Medium_Comp" value="'.$kval.'" />';
         */

                                            //Calculate if Leave Allowance is in or Out
                                            $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";
                                            if (ScriptRunner($Script_TAX, "LeaveField") == 'on') {
                                                $LStatus = 'A';
                                            } else {
                                                $LStatus = 'N';
                                            }

                                            //This area checks if a leave allowance is raised for the month and if inclusive, then add automatically else if exclusive, then wait for it to be authorized and then add.
                                            $Script_Leave = "Select count(*) Ct from Fin_LvAllw where (Status not in ('D','P')) and EmpID='" . $EmpID_ . "' and (Month(PayMonth) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(PayMonth) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))"; //Status 'B' menas BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid
                                            if (ScriptRunner($Script_Leave, "Ct") > 0) {
                                                $Script_Leave = "Update Fin_LvAllw set AmtMonthly=" . $kval . ", Status='" . $LStatus . "',[AddedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[AddedDate]=GetDate() where (Status not in ('D','P')) and EmpID='" . $EmpID_ . "' and (Month(PayMonth) = MONTH('" . ECh($PayMonth) . "') and YEAR(PayMonth) = YEAR('" . ECh($PayMonth) . "'))";
                                                ScriptRunnerUD($Script_Leave, "");
                                            } else {
                                                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                                                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "LeaveAllw" . $_SESSION["StkTck" . "UName"];
                                                $HashKey__ = md5($UniqueKey);

                                                $Script_Leave = "insert into Fin_LvAllw (EmpID,PayMonth,AmtMonthly,[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $EmpID_ . "','" . ECh($PayMonth) . "'," . $kval . ",'" . $LStatus . "','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey__ . "')";
                                                ScriptRunnerUD($Script_Leave, "");
                                            }

                                            //Block out leave authorized for payment for the month
                                            $Script_Leave = "Update Fin_LvAllw set Status='B' where (Status in ('A')) and EmpID='" . $EmpID_ . "'"; // and (Month(PayMonth) = MONTH('".ECh($PayMonth)."') and YEAR(PayMonth) = YEAR('".ECh($PayMonth)."'))";

                                            // var_dump($Script_Leave);
                                            ScriptRunnerUD($Script_Leave, "");

                                            //Calculate leave allowance to be paid
                                            $Script_Leave = "Select Sum(AmtMonthly) LvPay from Fin_LvAllw where (Status in ('B')) and EmpID='" . $EmpID_ . "'";
                                            $kval = ScriptRunner($Script_Leave, "LvPay");
                                            // $LeaveAllw = $kval; //Save the value of the leave to add to the NET Payable for the month
                                            $LeaveAllw = $kval_div; //Save the value of the leave to add to the NET Payable for the month

                                            //if ($kval > 0)
                                            if ($kval_div > 0) {
                                                echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="' . number_format($kval_div, 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="LeaveAllowance" id="LeaveAllowance" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                            }
                                            //++++++++++++++++++++ END +++++++++++++++++
                                        }
                                        ?>
                                    </div>

                                    <!-- ++++++++++++++++++++ OverTime Payment +++++++++++++++++  	-->


                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder;">Pension:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $ScriptPension = "select * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "')";

                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PensionEmployee");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="PensionEmployee" id="PensionEmployee" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }

                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "PensionEmployer");
                                        if ($kval > 0) {
                                            if ($CmdTyp == "Open") {
                                                echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="' . number_format(($kval / 12), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="hidden" name="PensionEmployer" id="PensionEmployer" size="25" maxlength="25" value="0.00" onchange="GetTotal()" />';
                                        }

                                        echo '<input type="hidden" name="PCent_PensionEmployee" id="PCent_PensionEmployee" value="' . number_format(ScriptRunner($ScriptPension, "PensionEmployee"), 2, '.', ',') . '" />
			        			<input type="hidden" name="PCent_PensionEmployer" id="PCent_PensionEmployer" value="' . number_format(ScriptRunner($ScriptPension, "PensionEmployer"), 2, '.', ',') . '" />';
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-primary" style="padding-right: 0px; font-weight: bolder; ">Net Pay:
                                    </label>
                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                        <?php
                                        $kval = 0;
                                        $kval = ScriptRunner($Script_Edit, "NetPay");

                                        if ($kval > 0) {

                                            if ($CmdTyp == "Open") {

                                                echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="' . number_format((($kval / 12) - $ExpPay2 - $LoanAmt - $TAmtLate - $TAmtAbsent + $LeaveAllw + $TAmtOverTime + (float) $total_bonus), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            } else {
                                                echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="' . number_format(($kval), 2, '.', ',') . '" onchange="GetTotal()" />';
                                            }
                                        } else {
                                            echo '<input type="text" name="NetPay" id="NetPay" size="25" maxlength="25" class="form-control" value="0.00" onchange="GetTotal()" />';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";
                                //Get % tax band values
                                echo '
						<input type="hidden" name="S_PAYE0" id="S_PAYE0" value="' . ScriptRunner($Script_TAX, "Limit0") . '">
						<input type="hidden" name="S_PAYE1" id="S_PAYE1" value="' . ScriptRunner($Script_TAX, "Limit1") . '">
						<input type="hidden" name="S_PAYE2" id="S_PAYE2" value="' . ScriptRunner($Script_TAX, "Limit2") . '">
						<input type="hidden" name="S_PAYE3" id="S_PAYE3" value="' . ScriptRunner($Script_TAX, "Limit3") . '">
						<input type="hidden" name="S_PAYE4" id="S_PAYE4" value="' . ScriptRunner($Script_TAX, "Limit4") . '">
						<input type="hidden" name="S_PAYE5" id="S_PAYE5" value="' . ScriptRunner($Script_TAX, "Limit5") . '">
						<input type="hidden" name="S_PAYE6" id="S_PAYE6" value="' . ScriptRunner($Script_TAX, "Limit6") . '">
						<input type="hidden" name="S_PAYE7" id="S_PAYE7" value="' . ScriptRunner($Script_TAX, "Limit7") . '">
						<input type="hidden" name="H_Leave" id="H_Leave" value="' . ScriptRunner($Script_TAX, "LeaveAllw") . '">';

                                //Get % Percent values
                                echo '
						<input type="hidden" name="S_PAYE_Comm0" id="S_PAYE_Comm0" value="' . ScriptRunner($Script_TAX, "pcent0") . '">
						<input type="hidden" name="S_PAYE_Comm1" id="S_PAYE_Comm1" value="' . ScriptRunner($Script_TAX, "pcent1") . '">
						<input type="hidden" name="S_PAYE_Comm2" id="S_PAYE_Comm2" value="' . ScriptRunner($Script_TAX, "pcent2") . '">
						<input type="hidden" name="S_PAYE_Comm3" id="S_PAYE_Comm3" value="' . ScriptRunner($Script_TAX, "pcent3") . '">
						<input type="hidden" name="S_PAYE_Comm4" id="S_PAYE_Comm4" value="' . ScriptRunner($Script_TAX, "pcent4") . '">
						<input type="hidden" name="S_PAYE_Comm5" id="S_PAYE_Comm5" value="' . ScriptRunner($Script_TAX, "pcent5") . '">
						<input type="hidden" name="S_PAYE_Comm6" id="S_PAYE_Comm6" value="' . ScriptRunner($Script_TAX, "pcent6") . '">
						<input type="hidden" name="S_PAYE_Comm7" id="S_PAYE_Comm7" value="' . ScriptRunner($Script_TAX, "pcent7") . '">';

                                echo '
						<input name="H_TTaxable" id="H_TTaxable" type="hidden" value="' . $TTaxable . '" />
						<input name="H_TDR_NTaxable" id="H_TDR_NTaxable" type="hidden" value="' . $TDR_NTaxable . '" />
						<input name="H_CR" id="H_CR" type="hidden" value="' . $CR . '">
						<input name="H_DR" id="H_DR" type="hidden" value="' . $DR . '">
						<input name="H_ALL" id="H_ALL" type="hidden" value="' . $AllItems . '">
						<input name="H_Pension" id="H_Pension" type="hidden" value="' . $TPension . '">';
                                ?>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-2">
                                        <div class="demo-checkbox">
                                            <?php if (ScriptRunner($Script_Edit__, "AuthCalc") == 'on') : ?>
                                                <input type="checkbox" checked name="AuthCalc" id="AuthCalc">
                                            <?php else : ?>
                                                <input type="checkbox" name="AuthCalc" id="AuthCalc">
                                            <?php endif ?>
                                            <label for="AuthCalc">
                                                <button type="button" class="btn btn-default btn-sm" style="line-height: 17px" Title="Check if you want a request for authorization sent" name="AuthInfo" id="AuthInfo2" onClick="Popup.show('simplediv');return false;">
                                                    <i class="fa fa-info"></i>
                                                </button>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <?php
                                        if (ValidateURths("PAY SALARY" . "T") == true) {
                                            if ($CmdTyp == "Open" && strlen($EditID) == 32) {

                                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Payment" />';
                                            } else {
                                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update Payment" />';
                                                echo '<input name="UpdID" type="hidden" id="UpdID" value="' . ECh($EditID) . '" />';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <br>
                    <hr style="margin-bottom: 1.0rem">
                    <div class="row">
                        <div class="col-md-12">
                            <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
                            <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
                            <?php //print $_REQUEST["FAction"]; 
                            ?>
                        </div>
                    </div>
                    <hr style="margin-top: 1.0rem;margin-bottom: .5rem">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php
                            if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {

                                print "<h4>UNAUTHORIZED</h4>";
                                $dbOpen2 = ("SELECT (DateName(MM,Fm.PayMonth)+' '+CONVERT(varchar(4),datepart(YEAR,Fm.PayMonth))) Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where Em.HashKey=Fm.EmpID and (Fm.Status in ('N','U')) and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
                            } else {
                                print "<h4>AUTHORIZED</h4>";
                                $dbOpen2 = ("SELECT (DateName(MM,Fm.PayMonth)+' '+CONVERT(varchar(4),datepart(YEAR,Fm.PayMonth))) Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+'  ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where Em.HashKey=Fm.EmpID and (Fm.Status = 'A') and (Em.EmpStatus='Active') order by SName asc, UpdatedDate desc");
                            }
                            ?>
                        </div>
                        <div class="col-md-12">
                            <div class="ViewPatch">
                                <table width="100%" align="left" cellpadding="1" cellspacing="1" id="table" class="tablesorter table table-responsive">
                                    <thead>
                                        <tr>
                                            <th valign="middle" width="10" scope="col" data-sorter="false">
                                                <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                                <label for="DelChk_All"></label>
                                            </th>
                                            <th valign="middle" scope="col"><span class="TinyTextTightBold">Pay Month</span></th>
                                            <th align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Employee Name</span></th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false">Basic</th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false">Housing</th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false">Transport</th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false">Pension</th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false">PAYE</th>
                                            <th align="center" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTightBold">Net Pay</span></th>
                                            <th align="center" valign="middle" class="smallText" scope="col" data-sorter="false">Gross Pay</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $Del = 0;
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            $Del = $Del + 1;
                                        ?>
                                            <tr>
                                                <td valign="middle" scope="col" class="TinyTextTight">
                                                    <input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
                                                    <label for="<?php echo ("DelBox" . $Del); ?>"></label>
                                                </td>
                                                <td valign="middle" class="TinyText" scope="col"><?php echo (trim($row2['Dt'])); ?></td>
                                                <td align="left" valign="middle" class="TinyText"><?php echo (trim($row2['Nm'])); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem1'], 2); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem2'], 2); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PayItem3'], 2); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PensionEmployee'] + $row2['PensionEmployer'], 2); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['PAYE'], 2); ?></td>
                                                <td align="right" valign="middle" class="TinyText" scope="col"><?php echo number_format($row2['NetPay'], 2); ?></td>
                                                <td align="right" valign="middle" scope="col" class="TinyText"><?php echo number_format($row2['Gross'], 2); ?></td>
                                            </tr>
                                        <?php
                                        }
                                        include '../login/dbClose2.php'; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 pt-5">
                            <?php include '../main/pagination.php'; ?>
                        </div>
                        <div class="col-md-6 pt-5">
                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <?php
                                    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								  	  <input name="FAction" id="FAction" type="hidden" />
									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                    //Check if any record was spolled at all before enabling buttons

                                    if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                                        //Check if user has Delete rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Delete'; include '../main/buttons.php';
                                        //Check if user has Add/Edit rights
                                        $but_HasRth = ("PAY SALARY" . "V");
                                        $but_Type = 'View';
                                        include '../main/buttons.php';
                                        //Check if user has Authorization rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Authorize'; include '../main/buttons.php';
                                    } else { // AUTHORIZE
                                        //Check if user has Delete rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Delete'; include '../main/buttons.php';
                                        //Check if user has Add/Edit rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'View'; include '../main/buttons.php';
                                        //Check if user has Authorization rights
                                        //$but_HasRth=("PAY SALARY".""); $but_Type = 'Unauthorize'; include '../main/buttons.php';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
            </div>
        </div>
    </form>
</body>
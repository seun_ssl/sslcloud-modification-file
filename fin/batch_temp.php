<?php
session_start();
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
include '../css/myscripts.php';
include '../login/scriptrunner.php';

function reduce_arr($array, $gross)
{

    $total = (float) 0;
    foreach ($array as $arr) {

        // var_dump($GLOBALS);
        $per_of_gross = (float) $arr[1];
        $item_wage = (float) $GLOBALS[lcfirst($arr[0])];
        $gr = (float) $gross;
        if ($per_of_gross === (float) 0) {
            $total = ((float) $total + (float) ($item_wage));
            $GLOBALS["v_$arr[0]"] = (float) ($item_wage);
        } elseif ($per_of_gross > (float) 0) {
            $total = ((float) $total + (float) (($per_of_gross / 100) * $gr));
            $GLOBALS["v_$arr[0]"] = (float) (($per_of_gross / 100) * $gr);
        } else {
            $total = ((float) $total + (float) (0));
            $GLOBALS["v_$arr[0]"] = (float) (0);
        }
    }

    return (float) $total;
}
function reduce_arr_update($array, $gross, $EmpID_)
{

    $Script = "Select * from Fin_PRCore where EmpID='" . $EmpID_ . "' and Status in ('A','N','U')";

    $total = (float) 0;
    foreach ($array as $arr) {
        // var_dump($arr);

        $per_of_gross = (float) $arr[1];
        $item_wage =  $GLOBALS[lcfirst($arr[0])];
        $gr = (float) $gross;
        if ($per_of_gross === (float) 0) {

            if ($item_wage === '') {

                $item_wage = Scriptrunner($Script, $arr[0]);
            } elseif ((float) $item_wage  === (float) 0) {

                $item_wage = 0;
            }


            $total = ((float) $total + (float) ($item_wage));
            $GLOBALS["v_$arr[0]"] = (float) ($item_wage);
        } elseif ($per_of_gross > (float) 0) {
            $total = ((float) $total + (float) (($per_of_gross / 100) * $gr));
            $GLOBALS["v_$arr[0]"] = (float) (($per_of_gross / 100) * $gr);
        } else {
            $total = ((float) $total + (float) (0));
            $GLOBALS["v_$arr[0]"] = (float) (0);
        }
    }

    return (float) $total;
}
function uploadEmpTemplat($data)
{
    list($emp_id, $gross, $payScheme, $payItem1, $payItem2, $payItem3, $payItem4, $payItem5, $payItem6, $payItem7, $payItem8, $payItem9, $payItem10, $payItem11, $payItem12, $payItem13, $payItem14, $payItem15, $payItem16, $payItem17, $payItem18, $payItem19, $payItem20, $payItem21, $payItem22, $payItem23, $payItem24, $payItem25) = $data;

    // get the payment scheme hash from  name
    $script = "select * from [EmpTbl] where [EmpID] = '" . sanitize($emp_id) . "' ";
    $emp_record = ScriptRunnercous($script);
    $script = "select * from [Fin_PRSettings] where [GName] = '" . sanitize($payScheme) . "' ";
    $scheme_record = ScriptRunnercous($script);
    if (is_null($scheme_record) && !is_array($scheme_record)) {
        return ['status' => false, 'msg' => 'Invalid pay Scheme'];
    }
    if (is_null($emp_record) && !is_array($emp_record)) {
        return ['status' => false, 'msg' => 'Invalid employee ID'];
    }

    if (isset($scheme_record['HashKey']) && strlen($scheme_record['HashKey']) == 32) {
        $EmpID_ = ECh($emp_record['HashKey']);
        $Script = "Select HashKey, ([SName]+' '+[FName]+' '+[ONames]+' ['+Convert(varchar(24),Et.EmpID)+']') as EmpPID from EmpTbl Et where Et.HashKey='" . ECh($emp_record['HashKey']) . "' and Et.Status in ('A','U') and Et.EmpStatus='Active'";

        $EmpPID = ScriptRunner($Script, "EmpPID");

        if (trim($EmpPID) == '') {
            return ['status' => false, 'msg' => 'The selected employee does not exist as an active employee. Please review.'];
            $GoValidate = false;
        }

        $Script = "Select Count(*) Ct from Fin_PRCore where EmpID='" . $EmpID_ . "' and Status in ('A','N','U')";
        if (ScriptRunner($Script, "Ct") == 0) {
            // if (ScriptRunner($Script, "Ct") !== 0) {
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "PayMaster" . $EmpID_;
            $HashKey = md5($UniqueKey);
            $EditID = $HashKey;
            $EmpID_ = $emp_record['HashKey'];
            $PayScheme_ = $scheme_record['HashKey']; //Get Scheme for the specified employee salary

            $Script = "INSERT INTO [Fin_PRCore]
                (EmpID,[GrossNm],[Scheme],[Status],[AddedBy],[AddedDate],[HashKey])
                VALUES ('" . ECh($EmpID_) . "','Gross','" . $PayScheme_ . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
            //echo $Script;
            ScriptRunnerUD($Script, "NComp");
            AuditLog("INSERT", "New payroll schedule created for employee [" . $EmpPID . "]", $HashKey);
            MailTrail("PAYROLL", "T", '', '', '', '');
            //  continue to update

            $AllItems = [];
            // $Pcent_all = '';
            $CR = [];
            $DR = [];
            $FP = [];
            $TTaxable = [];
            $TPension = [];
            $CreditNtTaxed = [];
            $DebitNtTaxed = [];
            $DebitTaxed = [];
            $ReliefNtTaxed = [];

            // get some settings values

            $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";

            include '../login/dbOpen2.php';

            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
                // $AllItems = $AllItems . 'PayItem1;';

                $AllItems[] = ['PayItem1', $row2['PayItem1']];
                if ($row2['PayItemCD1'] == "CR") {

                    // $CR = $CR . 'PayItem1;';
                    $CR[] = ["PayItem1", $row2['PayItem1']];
                } elseif ($row2['PayItemCD1'] == "DR") {
                    // $DR = $DR . 'PayItem1;';
                    $DR[] = ["PayItem1", $row2['PayItem1']];
                }
                if ($row2['PayItemTX1'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem1;';
                    $TTaxable[] = ["PayItem1", $row2['PayItem1']];
                }
                if ($row2['PayItemPen1'] == 1) {
                    // $TPension = $TPension . 'PayItem1;';
                    $TPension[] = ["PayItem1", $row2['PayItem1']];
                }
                // $AllItems = $AllItems . 'PayItem3;';
                $AllItems[] = ['PayItem3', $row2['PayItem3']];

                if ($row2['PayItemCD3'] == "CR") {
                    // $CR = $CR . 'PayItem3;';
                    $CR[] = ["PayItem3", $row2['PayItem3']];
                } elseif ($row2['PayItemCD3'] == "DR") {
                    // $DR = $DR . 'PayItem3;';
                    $DR[] = ["PayItem3", $row2['PayItem3']];
                }
                if ($row2['PayItemTX3'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem3;';
                    $TTaxable[] = ["PayItem3", $row2['PayItem3']];
                }
                if ($row2['PayItemPen3'] == 1) {
                    // $TPension = $TPension . 'PayItem3;';
                    $TPension[] = ["PayItem3", $row2['PayItem3']];
                }

                // $AllItems = $AllItems . 'PayItem2;';
                $AllItems[] = ['PayItem2', $row2['PayItem2']];
                if ($row2['PayItemCD2'] == "CR") {
                    // $CR = $CR . 'PayItem2;';
                    $CR[] = ['PayItem2', $row2['PayItem2']];
                } elseif ($row2['PayItemCD2'] == "DR") {
                    // $DR = $DR . 'PayItem2;';
                    $DR[] = ['PayItem2', $row2['PayItem2']];
                }

                if ($row2['PayItemTX2'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem2;';
                    $TTaxable[] = ['PayItem2', $row2['PayItem2']];
                }
                if ($row2['PayItemPen2'] == 1) {
                    // $TPension = $TPension . 'PayItem2;';
                    $TPension[] = ['PayItem2', $row2['PayItem2']];
                }

                for ($i = 4; $i <= 25; $i++) {
                    if ($row2['PayItemCD' . $i] == "FP" && $row2['PayItemOF' . $i] == 1) {
                        $FP[] = ["PayItem$i", $row2["PayItem$i"]];
                    }
                    if ($row2['PayItemCD' . $i] == "RL" && $row2['PayItemOF' . $i] == 1) {
                        if ($row2['PayItemTX' . $i] == 0) {
                            // $CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
                            $ReliefNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                        }
                    }
                    if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1) {
                        // $AllItems = $AllItems . 'PayItem' . $i . ';';
                        $AllItems[] = ["PayItem$i", $row2["PayItem$i"]];

                        if ($row2['PayItemCD' . $i] == "CR") {
                            // $CR = $CR . 'PayItem' . $i . ';';
                            $CR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
                                $CreditNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        } elseif ($row2['PayItemCD' . $i] == "DR") {
                            // $DR = $DR . 'PayItem' . $i . ';';
                            $DR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
                                $DebitNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        }
                        if ($row2['PayItemTX' . $i] == 1) {
                            // $TTaxable = $TTaxable . 'PayItem' . $i . ';';
                            $TTaxable[] = ["PayItem$i", $row2["PayItem$i"]];
                        }

                        if ($row2['PayItemPen' . $i] == 1) {
                            // $TPension = $TPension . 'PayItem' . $i . ';';
                            $TPension[] = ["PayItem$i", $row2["PayItem$i"]];
                        }
                    }
                }

                for ($i = 4; $i <= 25; $i++) {
                    if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
                        // $AllItems = $AllItems . 'PayItem' . $i . ';';
                        $AllItems[] = ["PayItem$i", $row2["PayItem$i"]];

                        if ($row2['PayItemCD' . $i] == "CR") {
                            // $CR = $CR . 'PayItem' . $i . ';';
                            $CR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
                                $CreditNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        } elseif ($row2['PayItemCD' . $i] == "DR") {
                            // $DR = $DR . 'PayItem' . $i . ';';
                            $DR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
                                $DebitNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            } else {
                                $DebitTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        }
                        // added by seun
                        if ($row2['PayItemTX' . $i] == 1) {
                            // $TTaxable = $TTaxable . 'PayItem' . $i . ';';
                            $TTaxable[] = ["PayItem$i", $row2["PayItem$i"]];
                        }

                        if ($row2['PayItemPen' . $i] == 1) {
                            // $TPension = $TPension . 'PayItem' . $i . ';';
                            $TPension[] = ["PayItem$i", $row2["PayItem$i"]];
                        }
                    }
                }

                // if ((int) $row2["PayItemOF20"] === 1) {
                //     $freepay20 = 'active';
                // }
                // if ((int) $row2["PayItemOF19"] === 1) {
                //     $freepay19 = 'active';
                // }

            }
            include '../login/dbclose2.php';

            // pension
            $ScriptPension = "select * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "')";

            $PCent_PensionEmployee = ScriptRunner($ScriptPension, "PensionEmployee");

            $PCent_LeaveAllw = ScriptRunner($ScriptPension, "LeaveAllw");
            $PCent_PensionEmployer = ScriptRunner($ScriptPension, "PensionEmployer");
            $per_leave = ScriptRunner($ScriptPension, "LeaveAllw");
            $leave_active = ScriptRunner($ScriptPension, "LeaveField");
            $leavePerGross = ScriptRunner($ScriptPension, "leavePerGross");

            $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";

            // $LeaveField = ScriptRunner($Script_TAX, "LeaveField");
            $S_PAYE0 = ScriptRunner($Script_TAX, "Limit0");
            $S_PAYE1 = ScriptRunner($Script_TAX, "Limit1");
            $S_PAYE2 = ScriptRunner($Script_TAX, "Limit2");
            $S_PAYE3 = ScriptRunner($Script_TAX, "Limit3");
            $S_PAYE4 = ScriptRunner($Script_TAX, "Limit4");
            $S_PAYE5 = ScriptRunner($Script_TAX, "Limit5");
            $S_PAYE6 = ScriptRunner($Script_TAX, "Limit6");
            $S_PAYE7 = ScriptRunner($Script_TAX, "Limit7");
            $H_Leave = ScriptRunner($Script_TAX, "LeaveAllw");
            $S_PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
            $S_PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
            $S_PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
            $S_PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
            $S_PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
            $S_PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
            $S_PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
            $S_PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");
            $H_TTaxable = $TTaxable;
            $H_CR = $CR;
            $H_DR = $DR;
            $H_ALL = $AllItems;
            $DebitNtTax = $DebitNtTaxed;
            $DebitTax = $DebitTaxed;
            $CreditNtTax = $CreditNtTaxed;
            $ReliefNtTax = $ReliefNtTaxed;
            $H_Pension = $TPension;

            // javascript path

            $Basic = 0;
            $NetPay = 0;
            $GrossPay = 0;
            $Total_DR = 0;
            $Total_CR = 0;
            $TotAllowance = 0;
            $Total_TX = 0;
            $Total_PEN = 0;

            // varaibles to be calculated
            $PensionEmployee = 0;
            $PensionEmployer = 0;
            $LeaveAllowance = 0;
            $TRelief = 0;
            $NetPay = 0;
            $PAYE = 0;
            $TTaxable = 0;
            $AuthCalc = true;
            $Gross = $gross;
            $NwGross = $gross;
            $v_niz_gross = 0;

            //Get TOTAL FREE PAY VALUE

            $Total_FP = reduce_arr($FP, $NwGross);
            // var_dump($Total_FP);
            //Get TOTAL DEBIT VALUE
            $Total_CR = reduce_arr($H_CR, $NwGross);
            //Get TOTAL DEBIT VALUE
            $Total_DR = reduce_arr($H_DR, $NwGross);
            //Get TOTAL PEN VALUE
            $Total_PEN = reduce_arr($H_Pension, $NwGross);

            $PensionEmployee = ((float) $PCent_PensionEmployee / 100) * (float) $Total_PEN;
            $PensionEmployer = ((float) $PCent_PensionEmployer / 100) * (float) $Total_PEN;
            $Pension = $PensionEmployee;

            $Basic = reduce_arr([$H_ALL[0]], $NwGross);

            if ($leavePerGross == "on") {

                $LeaveAllowance = (float) ((float) $gross / 12 * ((float) $PCent_LeaveAllw / 100));
            } else {

                $LeaveAllowance = (float) ((float) $Basic * ((float) $PCent_LeaveAllw / 100));
            }

            //  var_dump($LeaveAllowance);

            //Get TOTAL TAXABLE VALUE
            $Total_TX = reduce_arr($H_TTaxable, $NwGross);
            //Get TOTAL DEBIT VALUE NOT Taxable
            $Total_DR_NTax = reduce_arr($DebitNtTax, $NwGross);
            //Get TOTAL DEBIT VALUE NOT Taxable
            $Total_DR_Tax = reduce_arr($DebitTax, $NwGross);
            //Get TOTAL CREDIT NOT Taxable VALUE
            $Total_CR_NTax = reduce_arr($CreditNtTax, $NwGross);
            //Get TOTAL RELIEFs NOT Taxable VALUE
            $Total_RL_NTax = reduce_arr($ReliefNtTax, $NwGross);
            // var_dump($Total_TX,$Total_DR_NTax);
            $GrossPay_user = $NwGross;
            $GrossPay_cr = $Total_CR;

            if ($GrossPay_cr > $GrossPay_user) {
                $GrossPay = $GrossPay_cr;
            } else {
                $GrossPay = $GrossPay_user;
            }
            $TotTaxable = $Total_TX;
            $new_grossV2 = $GrossPay - $Total_DR_Tax - $Pension;
            $one_per_gross = (float) 0.01 * $new_grossV2;

            $pay_200000 = $one_per_gross > 200000 ? $one_per_gross : 200000;


            if ($_SESSION['StkTckCustID'] === '092f904d7a11f9ad2c827afe889d7cda') {

                $TRelief = (float) ((float) $pay_200000 + (float) ($new_grossV2 * (20 / 100)) + $Pension + $Total_DR_Tax + $Total_RL_NTax);
            } else if ($_SESSION['StkTckCustID'] === '89bacdc8f7403aea089dcc66881b48a6') {
                $TRelief = (float) ((float) 200000 + (float) (($GrossPay + $LeaveAllowance - $Pension) * (20 / 100)) + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax);
            } else {

                $TRelief = (float) ((float) 200000 + (float) ($GrossPay * (20 / 100)) + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax);
            }



            if ($leave_active !== "on" && $per_leave > 0) {
                if ($_SESSION['StkTckCustID'] === '89bacdc8f7403aea089dcc66881b48a6') {
                    $TotTaxable = (float) $GrossPay + (float) $LeaveAllowance - (float) $TRelief;
                } else {

                    $TotTaxable = (float) $GrossPay - (float) $TRelief;
                }
            } else {

                $TotTaxable = (float) $GrossPay - (float) $TRelief;
            }

            $TTaxable = $TotTaxable;

            $PAYE = 0;
            $PAYE0 = ScriptRunner($Script_TAX, "Limit0");
            $PAYE1 = ScriptRunner($Script_TAX, "Limit1");
            $PAYE2 = ScriptRunner($Script_TAX, "Limit2");
            $PAYE3 = ScriptRunner($Script_TAX, "Limit3");
            $PAYE4 = ScriptRunner($Script_TAX, "Limit4");
            $PAYE5 = ScriptRunner($Script_TAX, "Limit5");
            $PAYE6 = ScriptRunner($Script_TAX, "Limit6");
            $PAYE7 = ScriptRunner($Script_TAX, "Limit7");
            $PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
            $PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
            $PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
            $PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
            $PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
            $PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
            $PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
            $PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");

            if (($TotTaxable < $PAYE0)) {
                $PAYE = ($GrossPay * $PAYE_Comm0) / 100;
            } else {


                /*
                    // Applies the flat rate of 1%
                    if ($GrossPay <= $PAYE0)
                    {$PAYE = ($GrossPay * $PAYE_Comm0)/100;}

                    // Applies first band of 7%
                    if (($GrossPay > ($PAYE0)) && ($TotTaxable < ($PAYE1)))
                    {
                    $PAYE_Rem = $TotTaxable;
                    $PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);

                    }
                    */

                //Amount taxed on first band
                if (($TotTaxable <= ($PAYE1)) && ($GrossPay > $PAYE0)) {
                    $PAYE_Rem = $TotTaxable;
                    $PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
                }

                if ($TotTaxable >= ($PAYE1)) {
                    $PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
                }

                //If Amount Left is not up to SECOND BAND (PAYE2)
                if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2)) {
                    $PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
                }

                //If Amount Left is not up to THIRD BAND (PAYE3)
                if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
                    $PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
                    $PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
                }

                //If Amount Left is not up to FOURTH BAND (PAYE4)
                if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
                    $PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
                }

                //alert ($TotTaxable);

                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
                }
                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
                    $PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
                }

                //If Amount Left is not up to SIXTH BAND (PAYE6)

                if ($TotTaxable > $PAYE6) {
                    $PAYE = $PAYE + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
                }
            }


            if ($leave_active !== "on" && $per_leave > 0) {

                $TotTaxable1 = (float) $LeaveAllowance;
                // if (($TotTaxable1 < $PAYE0)) {
                //     $PAYE_X = ($GrossPay * $PAYE_Comm0) / 100;
                // } else {

                //Amount taxed on first band
                if (($TotTaxable1 <= ($PAYE1)) && ($GrossPay > $PAYE0)) {
                    $PAYE_Rem = $TotTaxable1;
                    $PAYE_X = (($PAYE_Rem * $PAYE_Comm1) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1)) {
                    $PAYE_X = $PAYE_X + (($PAYE1 * $PAYE_Comm1) / 100);
                }

                //If Amount Left is not up to SECOND BAND (PAYE2)
                if (($TotTaxable1 > ($PAYE1)) && ($TotTaxable1 < ($PAYE1 + $PAYE2))) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm2) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2)) {
                    $PAYE_X = $PAYE_X + (($PAYE2 * $PAYE_Comm2) / 100);
                }

                //If Amount Left is not up to THIRD BAND (PAYE3)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3))) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2);
                    $PAYE_X = ($PAYE_X + (($PAYE_Rem * $PAYE_Comm3) / 100));
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3)) {
                    $PAYE_X = $PAYE_X + (($PAYE3 * $PAYE_Comm3) / 100);
                }

                //If Amount Left is not up to FOURTH BAND (PAYE4)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))
                ) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2 + $PAYE3);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm4) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
                    $PAYE_X = $PAYE_X + (($PAYE4 * $PAYE_Comm4) / 100);
                }

                //alert ($TotTaxable);

                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm5) / 100);
                }
                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
                    $PAYE_X = $PAYE_X + (($PAYE5 * $PAYE_Comm5) / 100);
                }

                //If Amount Left is not up to SIXTH BAND (PAYE6)

                if ($TotTaxable1 > $PAYE6) {
                    $PAYE_X = $PAYE_X + ((($TotTaxable1 - $PAYE6) * $PAYE_Comm6) / 100);
                }
                // }

                // for now make $PAYE_X zero

                $PAYE_X = 0;
            }

            if ($_SESSION['StkTckCustID'] === '092f904d7a11f9ad2c827afe889d7cda') {
                $NetPay = (float) (($GrossPay - $Total_DR) - $PAYE - $Pension);
            } else {

                $NetPay = (float) (($GrossPay - $Total_DR) - $PAYE - $Pension + $Total_FP);
            }

            if (isset($GLOBALS["v_PayItem1"])) {

                $v_PayItem1 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem1"])), '2', '.', '');
            } else {
                $v_PayItem1 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem2"])) {
                $v_PayItem2 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem2"])), '2', '.', '');
            } else {
                $v_PayItem2 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem3"])) {
                $v_PayItem3 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem3"])), '2', '.', '');
            } else {
                $v_PayItem3 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem4"])) {
                $v_PayItem4 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem4"])), '2', '.', '');
            } else {
                $v_PayItem4 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem5"])) {
                $v_PayItem5 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem5"])), '2', '.', '');
            } else {
                $v_PayItem5 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem6"])) {
                $v_PayItem6 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem6"])), '2', '.', '');
            } else {
                $v_PayItem6 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem7"])) {
                $v_PayItem7 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem7"])), '2', '.', '');
            } else {
                $v_PayItem7 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem8"])) {
                $v_PayItem8 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem8"])), '2', '.', '');
            } else {
                $v_PayItem8 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem9"])) {
                $v_PayItem9 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem9"])), '2', '.', '');
            } else {
                $v_PayItem9 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem10"])) {
                $v_PayItem10 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem10"])), '2', '.', '');
            } else {
                $v_PayItem10 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem11"])) {
                $v_PayItem11 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem11"])), '2', '.', '');
            } else {
                $v_PayItem11 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem12"])) {
                $v_PayItem12 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem12"])), '2', '.', '');
            } else {
                $v_PayItem12 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem13"])) {
                $v_PayItem13 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem13"])), '2', '.', '');
            } else {
                $v_PayItem13 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem14"])) {
                $v_PayItem14 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem14"])), '2', '.', '');
            } else {
                $v_PayItem14 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem15"])) {
                $v_PayItem15 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem15"])), '2', '.', '');
            } else {
                $v_PayItem15 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem16"])) {
                $v_PayItem16 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem16"])), '2', '.', '');
            } else {
                $v_PayItem16 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem17"])) {
                $v_PayItem17 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem17"])), '2', '.', '');
            } else {
                $v_PayItem17 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem18"])) {
                $v_PayItem18 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem18"])), '2', '.', '');
            } else {
                $v_PayItem18 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem19"])) {
                $v_PayItem19 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem19"])), '2', '.', '');
            } else {
                $v_PayItem19 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem20"])) {
                $v_PayItem20 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem20"])), '2', '.', '');
            } else {
                $v_PayItem20 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem21"])) {
                $v_PayItem21 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem21"])), '2', '.', '');
            } else {
                $v_PayItem21 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem22"])) {
                $v_PayItem22 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem22"])), '2', '.', '');
            } else {
                $v_PayItem22 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem23"])) {
                $v_PayItem23 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem23"])), '2', '.', '');
            } else {
                $v_PayItem23 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem24"])) {
                $v_PayItem24 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem24"])), '2', '.', '');
            } else {
                $v_PayItem24 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem25"])) {
                $v_PayItem25 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem25"])), '2', '.', '');
            } else {
                $v_PayItem25 = 0.0;
            }

            //if (isset($_REQUEST["PensionEmployee"])){
            if ($PensionEmployee != "") {
                $v_PensionEmployee = number_format(str_replace(',', '', trim($PensionEmployee)), '2', '.', '');
            } else {
                $v_PensionEmployee = 0.0;
            }

            if (isset($PensionEmployer)) {
                $v_PensionEmployer = number_format(str_replace(',', '', trim($PensionEmployer)), '2', '.', '');
            } else {
                $v_PensionEmployer = 0.0;
            }
            if (isset($LeaveAllowance) && is_numeric(str_replace(',', '', trim($LeaveAllowance)))) {
                $v_LeaveAllowance = number_format(str_replace(',', '', trim($LeaveAllowance)), '2', '.', '');
            } else {
                $v_LeaveAllowance = 0.0;
            }

            if (isset($TRelief) && is_numeric(str_replace(',', '', trim($TRelief)))) {
                $v_TRelief = number_format(str_replace(',', '', trim($TRelief)), '2', '.', '');
            } else {
                $v_TRelief = 0.0;
            }

            //if (isset($_REQUEST["PAYE"])){
            if ($PAYE != "") {
                $v_PAYE = number_format(str_replace(',', '', trim($PAYE)), '2', '.', '');
            } else {
                $v_PAYE = 0.0;
            }

            //if (isset($_REQUEST["TTaxable"])){
            if ($TTaxable != "") {
                $v_TTaxable = number_format(str_replace(',', '', trim($TTaxable)), '2', '.', '');
            } else {
                $v_TTaxable = 0.0;
            }

            $v_NetPay = 0.0;

            //if (isset($_REQUEST["NetPay"]))
            if ($NetPay != "") {
                $v_NetPay = number_format(str_replace(',', '', trim($NetPay)), '2', '.', '');
                if (!is_numeric($v_NetPay)) {
                    $v_NetPay = 0.0;
                }
            }

            if (isset($AuthCalc)) {
                $AutoCalc_ = 'on';
            } else {
                $AutoCalc_ = '';
            }

            if (isset($new_grossV2)) {
                $v_niz_gross = number_format($new_grossV2, '2', '.', '');
            } else {
                $v_niz_gross = 0.0;
            }

            /*if (isset($_REQUEST["ShiftPay"]) && $_REQUEST["ShiftPay"]=="on")
            {$ShiftPay_ = "on";} else {$ShiftPay_ = "";}*/

            if (isset($Gross)) {
                $v_Gross = number_format(str_replace(',', '', trim($Gross)), '2', '.', '');
            } else {
                $v_Gross = 0.0;
            }

            if (isset($PAYE_X)) {
                $paye_leave_allowance = number_format(str_replace(',', '', trim($PAYE_X)), '2', '.', '');
            } else {
                $paye_leave_allowance = 0.0;
            }
            //                echo $_REQUEST["Gross"]."===========".$v_Gross;

            $Script = "UPDATE [Fin_PRCore]
				SET
				[Gross] ='" . ECh($v_Gross) . "'
				,[PayItem1] ='" . ECh($v_PayItem1) . "'
				,[PayItem2] ='" . ECh($v_PayItem2) . "'
				,[PayItem3] ='" . ECh($v_PayItem3) . "'
				,[PayItem4] ='" . ECh($v_PayItem4) . "'
				,[PayItem5] ='" . ECh($v_PayItem5) . "'
				,[PayItem6] ='" . ECh($v_PayItem6) . "'
				,[PayItem7] ='" . ECh($v_PayItem7) . "'
				,[PayItem8] ='" . ECh($v_PayItem8) . "'
				,[PayItem9] ='" . ECh($v_PayItem9) . "'
				,[PayItem10] ='" . ECh($v_PayItem10) . "'
				,[PayItem11] ='" . ECh($v_PayItem11) . "'
				,[PayItem12] ='" . ECh($v_PayItem12) . "'
				,[PayItem13] ='" . ECh($v_PayItem13) . "'
				,[PayItem14] ='" . ECh($v_PayItem14) . "'
				,[PayItem15] ='" . ECh($v_PayItem15) . "'
				,[PayItem16] ='" . ECh($v_PayItem16) . "'
				,[PayItem17] ='" . ECh($v_PayItem17) . "'
				,[PayItem18] ='" . ECh($v_PayItem18) . "'
				,[PayItem19] ='" . ECh($v_PayItem19) . "'
				,[PayItem20] ='" . ECh($v_PayItem20) . "'
				,[PayItem21] ='" . ECh($v_PayItem21) . "'
				,[PayItem22] ='" . ECh($v_PayItem22) . "'
				,[PayItem23] ='" . ECh($v_PayItem23) . "'
				,[PayItem24] ='" . ECh($v_PayItem24) . "'
				,[PayItem25] ='" . ECh($v_PayItem25) . "'
				,[PensionEmployee] ='" . ECh($v_PensionEmployee) . "'
				,[PensionEmployer] ='" . ECh($v_PensionEmployer) . "'
				,[PAYE] ='" . ECh($v_PAYE) . "'
				,[LeaveAllowance] ='" . ECh($v_LeaveAllowance) . "'

				,[TTaxable] ='" . ECh($v_TTaxable) . "'

				,[NetPay] ='" . ECh($v_NetPay) . "'
				,[TRelief] ='" . ECh($v_TRelief) . "'

				,[Scheme] ='" . ECh($PayScheme_) . "'
				,[AuthCalc] ='" . $AutoCalc_ . "'
				,[niz_gross] ='" . $v_niz_gross . "'
                ,[paye_leave_allowance] ='" . ECh($paye_leave_allowance) . "'
				,[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "'
				,[UpdatedDate] = GetDate()
				,[Status] = 'U'
				WHERE [HashKey] = '" . $HashKey . "'";

            //,[TTaxable] ='".ECh($v_TTaxable)."'
            //
            ScriptRunnerUD($Script, "Inst");

            // die;
        } else {
            //  modify/ update here
            $Script = "Select * from Fin_PRCore where EmpID='" . $EmpID_ . "' and Status in ('A','N','U')";
            $HashKey = ScriptRunner($Script, "HashKey");
            $EditID = $HashKey;
            $EmpID_ = $emp_record['HashKey'];
            $PayScheme_ = $scheme_record['HashKey']; //Get Scheme for the specified employee salary

            //  continue to update

            $AllItems = [];
            // $Pcent_all = '';
            $CR = [];
            $DR = [];
            $FP = [];
            $TTaxable = [];
            $TPension = [];
            $CreditNtTaxed = [];
            $DebitNtTaxed = [];
            $DebitTaxed = [];

            // get some settings values

            $dbOpen2 = "SELECT * from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "'";

            include '../login/dbOpen2.php';

            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
                // $AllItems = $AllItems . 'PayItem1;';

                $AllItems[] = ['PayItem1', $row2['PayItem1']];
                if ($row2['PayItemCD1'] == "CR") {
                    // $CR = $CR . 'PayItem1;';
                    $CR[] = ["PayItem1", $row2['PayItem1']];
                } elseif ($row2['PayItemCD1'] == "DR") {
                    // $DR = $DR . 'PayItem1;';
                    $DR[] = ["PayItem1", $row2['PayItem1']];
                }
                if ($row2['PayItemTX1'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem1;';
                    $TTaxable[] = ["PayItem1", $row2['PayItem1']];
                }
                if ($row2['PayItemPen1'] == 1) {
                    // $TPension = $TPension . 'PayItem1;';
                    $TPension[] = ["PayItem1", $row2['PayItem1']];
                }
                // $AllItems = $AllItems . 'PayItem3;';
                $AllItems[] = ['PayItem3', $row2['PayItem3']];

                if ($row2['PayItemCD3'] == "CR") {
                    // $CR = $CR . 'PayItem3;';
                    $CR[] = ["PayItem3", $row2['PayItem3']];
                } elseif ($row2['PayItemCD3'] == "DR") {
                    // $DR = $DR . 'PayItem3;';
                    $DR[] = ["PayItem3", $row2['PayItem3']];
                }
                if ($row2['PayItemTX3'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem3;';
                    $TTaxable[] = ["PayItem3", $row2['PayItem3']];
                }
                if ($row2['PayItemPen3'] == 1) {
                    // $TPension = $TPension . 'PayItem3;';
                    $TPension[] = ["PayItem3", $row2['PayItem3']];
                }

                // $AllItems = $AllItems . 'PayItem2;';
                $AllItems[] = ['PayItem2', $row2['PayItem2']];
                if ($row2['PayItemCD2'] == "CR") {
                    // $CR = $CR . 'PayItem2;';
                    $CR[] = ['PayItem2', $row2['PayItem2']];
                } elseif ($row2['PayItemCD2'] == "DR") {
                    // $DR = $DR . 'PayItem2;';
                    $DR[] = ['PayItem2', $row2['PayItem2']];
                }

                if ($row2['PayItemTX2'] == 1) {
                    // $TTaxable = $TTaxable . 'PayItem2;';
                    $TTaxable[] = ['PayItem2', $row2['PayItem2']];
                }
                if ($row2['PayItemPen2'] == 1) {
                    // $TPension = $TPension . 'PayItem2;';
                    $TPension[] = ['PayItem2', $row2['PayItem2']];
                }

                for ($i = 4; $i <= 25; $i++) {
                    if ($row2['PayItemCD' . $i] == "FP" && $row2['PayItemOF' . $i] == 1) {
                        $FP[] = ["PayItem$i", $row2["PayItem$i"]];
                    }
                    if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1) {
                        // $AllItems = $AllItems . 'PayItem' . $i . ';';
                        $AllItems[] = ["PayItem$i", $row2["PayItem$i"]];

                        if ($row2['PayItemCD' . $i] == "CR") {
                            // $CR = $CR . 'PayItem' . $i . ';';
                            $CR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
                                $CreditNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        } elseif ($row2['PayItemCD' . $i] == "DR") {
                            // $DR = $DR . 'PayItem' . $i . ';';
                            $DR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
                                $DebitNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        }
                        if ($row2['PayItemTX' . $i] == 1) {
                            // $TTaxable = $TTaxable . 'PayItem' . $i . ';';
                            $TTaxable[] = ["PayItem$i", $row2["PayItem$i"]];
                        }

                        if ($row2['PayItemPen' . $i] == 1) {
                            // $TPension = $TPension . 'PayItem' . $i . ';';
                            $TPension[] = ["PayItem$i", $row2["PayItem$i"]];
                        }
                    }
                }

                for ($i = 4; $i <= 25; $i++) {
                    if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
                        // $AllItems = $AllItems . 'PayItem' . $i . ';';
                        $AllItems[] = ["PayItem$i", $row2["PayItem$i"]];

                        if ($row2['PayItemCD' . $i] == "CR") {
                            // $CR = $CR . 'PayItem' . $i . ';';
                            $CR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $CreditNtTaxed = $CreditNtTaxed . 'PayItem' . $i . ';';
                                $CreditNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        } elseif ($row2['PayItemCD' . $i] == "DR") {
                            // $DR = $DR . 'PayItem' . $i . ';';
                            $DR[] = ["PayItem$i", $row2["PayItem$i"]];
                            if ($row2['PayItemTX' . $i] == 0) {
                                // $DebitNtTaxed = $DebitNtTaxed . 'PayItem' . $i . ';';
                                $DebitNtTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            } else {
                                $DebitTaxed[] = ["PayItem$i", $row2["PayItem$i"]];
                            }
                        }
                        // added by seun
                        if ($row2['PayItemTX' . $i] == 1) {
                            // $TTaxable = $TTaxable . 'PayItem' . $i . ';';
                            $TTaxable[] = ["PayItem$i", $row2["PayItem$i"]];
                        }

                        if ($row2['PayItemPen' . $i] == 1) {
                            // $TPension = $TPension . 'PayItem' . $i . ';';
                            $TPension[] = ["PayItem$i", $row2["PayItem$i"]];
                        }
                    }
                }

                // if ((int) $row2["PayItemOF20"] === 1) {
                //     $freepay20 = 'active';
                // }
                // if ((int) $row2["PayItemOF19"] === 1) {
                //     $freepay19 = 'active';
                // }

            }
            include '../login/dbclose2.php';

            // pension
            $ScriptPension = "select * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where Status in ('A') and HashKey='" . $PayScheme_ . "')";

            $PCent_PensionEmployee = ScriptRunner($ScriptPension, "PensionEmployee");

            $PCent_LeaveAllw = ScriptRunner($ScriptPension, "LeaveAllw");
            $PCent_PensionEmployer = ScriptRunner($ScriptPension, "PensionEmployer");
            $per_leave = ScriptRunner($ScriptPension, "LeaveAllw");
            $leave_active = ScriptRunner($ScriptPension, "LeaveField");
            $leavePerGross = ScriptRunner($ScriptPension, "leavePerGross");

            $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";

            // $LeaveField = ScriptRunner($Script_TAX, "LeaveField");
            $S_PAYE0 = ScriptRunner($Script_TAX, "Limit0");
            $S_PAYE1 = ScriptRunner($Script_TAX, "Limit1");
            $S_PAYE2 = ScriptRunner($Script_TAX, "Limit2");
            $S_PAYE3 = ScriptRunner($Script_TAX, "Limit3");
            $S_PAYE4 = ScriptRunner($Script_TAX, "Limit4");
            $S_PAYE5 = ScriptRunner($Script_TAX, "Limit5");
            $S_PAYE6 = ScriptRunner($Script_TAX, "Limit6");
            $S_PAYE7 = ScriptRunner($Script_TAX, "Limit7");
            $H_Leave = ScriptRunner($Script_TAX, "LeaveAllw");
            $S_PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
            $S_PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
            $S_PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
            $S_PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
            $S_PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
            $S_PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
            $S_PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
            $S_PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");
            $H_TTaxable = $TTaxable;
            $H_CR = $CR;
            $H_DR = $DR;
            $H_ALL = $AllItems;
            $DebitNtTax = $DebitNtTaxed;
            $DebitTax = $DebitTaxed;
            $CreditNtTax = $CreditNtTaxed;
            $H_Pension = $TPension;

            // javascript path

            $Basic = 0;
            $NetPay = 0;
            $GrossPay = 0;
            $Total_DR = 0;
            $Total_CR = 0;
            $TotAllowance = 0;
            $Total_TX = 0;
            $Total_PEN = 0;

            // varaibles to be calculated
            $PensionEmployee = 0;
            $PensionEmployer = 0;
            $LeaveAllowance = 0;
            $TRelief = 0;
            $NetPay = 0;
            $PAYE = 0;
            $TTaxable = 0;
            $AuthCalc = true;
            $Gross = $gross;
            $NwGross = $gross;
            $v_niz_gross = 0;

            //Get TOTAL FREE PAY VALUE

            $Total_FP = reduce_arr_update($FP, $NwGross, $EmpID_);
            // var_dump($Total_FP);
            //Get TOTAL DEBIT VALUE
            $Total_CR = reduce_arr_update($H_CR, $NwGross, $EmpID_);
            //Get TOTAL DEBIT VALUE
            $Total_DR = reduce_arr_update($H_DR, $NwGross, $EmpID_);
            //Get TOTAL PEN VALUE
            $Total_PEN = reduce_arr_update($H_Pension, $NwGross, $EmpID_);

            $PensionEmployee = ((float) $PCent_PensionEmployee / 100) * (float) $Total_PEN;
            $PensionEmployer = ((float) $PCent_PensionEmployer / 100) * (float) $Total_PEN;
            $Pension = $PensionEmployee;

            $Basic = reduce_arr_update([$H_ALL[0]], $NwGross, $EmpID_);

            if ($leavePerGross == "on") {

                $LeaveAllowance = (float) ((float) $gross / 12 * ((float) $PCent_LeaveAllw / 100));
            } else {

                $LeaveAllowance = (float) ((float) $Basic * ((float) $PCent_LeaveAllw / 100));
            }
            //  var_dump($LeaveAllowance);

            //Get TOTAL TAXABLE VALUE
            $Total_TX = reduce_arr_update($H_TTaxable, $NwGross, $EmpID_);
            //Get TOTAL DEBIT VALUE NOT Taxable
            $Total_DR_NTax = reduce_arr_update($DebitNtTax, $NwGross, $EmpID_);
            //Get TOTAL CREDIT NOT Taxable VALUE
            $Total_CR_NTax = reduce_arr_update($CreditNtTax, $NwGross, $EmpID_);
            // var_dump($Total_TX,$Total_DR_NTax);
            $GrossPay_user = $NwGross;
            $GrossPay_cr = $Total_CR;

            if ($GrossPay_cr > $GrossPay_user) {
                $GrossPay = $GrossPay_cr;
            } else {
                $GrossPay = $GrossPay_user;
            }
            $TotTaxable = $Total_TX;

            $one_per_gross = (float) 0.01 * $GrossPay;

            $pay_200000 = $one_per_gross > 200000 ? $one_per_gross : 200000;


            if (
                $_SESSION['StkTckCustID'] === '092f904d7a11f9ad2c827afe889d7cda'
            ) {
                $new_grossV2 = $GrossPay - $Total_DR_Tax - $Pension;

                $TRelief = (float) ((float) $pay_200000 + (float) ($new_grossV2 * (20 / 100)) + $Pension + $Total_DR_Tax + $Total_RL_NTax);
            } else if ($_SESSION['StkTckCustID'] === '89bacdc8f7403aea089dcc66881b48a6') {
                $TRelief = (float) ((float) 200000 + (float) (($GrossPay + $LeaveAllowance - $Pension) * (20 / 100)) + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax);
            } else {

                $TRelief = (float) ((float) 200000 + (float) ($GrossPay * (20 / 100)) + $Pension + $Total_DR_NTax + $Total_CR_NTax + $Total_RL_NTax);
            }


            if ($leave_active !== "on" && $per_leave > 0) {
                if ($_SESSION['StkTckCustID'] === '89bacdc8f7403aea089dcc66881b48a6') {
                    $TotTaxable = (float) $GrossPay + (float) $LeaveAllowance - (float) $TRelief;
                } else {

                    $TotTaxable = (float) $GrossPay - (float) $TRelief;
                }
            } else {

                $TotTaxable = (float) $GrossPay - (float) $TRelief;
            }

            $TTaxable = $TotTaxable;

            $PAYE = 0;
            $PAYE0 = ScriptRunner($Script_TAX, "Limit0");
            $PAYE1 = ScriptRunner($Script_TAX, "Limit1");
            $PAYE2 = ScriptRunner($Script_TAX, "Limit2");
            $PAYE3 = ScriptRunner($Script_TAX, "Limit3");
            $PAYE4 = ScriptRunner($Script_TAX, "Limit4");
            $PAYE5 = ScriptRunner($Script_TAX, "Limit5");
            $PAYE6 = ScriptRunner($Script_TAX, "Limit6");
            $PAYE7 = ScriptRunner($Script_TAX, "Limit7");
            $PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
            $PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
            $PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
            $PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
            $PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
            $PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
            $PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
            $PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");

            if (($TotTaxable < $PAYE0)) {
                $PAYE = ($GrossPay * $PAYE_Comm0) / 100;
            } else {

                //Amount taxed on first band
                if (($TotTaxable <= ($PAYE1)) && ($GrossPay > $PAYE0)) {
                    $PAYE_Rem = $TotTaxable;
                    $PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
                }

                if ($TotTaxable >= ($PAYE1)) {
                    $PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
                }

                //If Amount Left is not up to SECOND BAND (PAYE2)
                if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2)) {
                    $PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
                }

                //If Amount Left is not up to THIRD BAND (PAYE3)
                if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
                    $PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
                    $PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
                }

                //If Amount Left is not up to FOURTH BAND (PAYE4)
                if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
                }

                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
                    $PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
                }

                //alert ($TotTaxable);

                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
                    $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
                }
                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
                    $PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
                }

                //If Amount Left is not up to SIXTH BAND (PAYE6)

                if ($TotTaxable > $PAYE6) {
                    $PAYE = $PAYE + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
                }
            }





            if ($leave_active !== "on" && $per_leave > 0) {

                $TotTaxable1 = (float) $LeaveAllowance;
                // if (($TotTaxable1 < $PAYE0)) {
                //     $PAYE_X = ($GrossPay * $PAYE_Comm0) / 100;
                // } else {

                //Amount taxed on first band
                if (($TotTaxable1 <= ($PAYE1)) && ($GrossPay > $PAYE0)) {
                    $PAYE_Rem = $TotTaxable1;
                    $PAYE_X = (($PAYE_Rem * $PAYE_Comm1) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1)) {
                    $PAYE_X = $PAYE_X + (($PAYE1 * $PAYE_Comm1) / 100);
                }

                //If Amount Left is not up to SECOND BAND (PAYE2)
                if (($TotTaxable1 > ($PAYE1)) && ($TotTaxable1 < ($PAYE1 + $PAYE2))) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm2) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2)) {
                    $PAYE_X = $PAYE_X + (($PAYE2 * $PAYE_Comm2) / 100);
                }

                //If Amount Left is not up to THIRD BAND (PAYE3)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3))) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2);
                    $PAYE_X = ($PAYE_X + (($PAYE_Rem * $PAYE_Comm3) / 100));
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3)) {
                    $PAYE_X = $PAYE_X + (($PAYE3 * $PAYE_Comm3) / 100);
                }

                //If Amount Left is not up to FOURTH BAND (PAYE4)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
                    $PAYE_Rem = $TotTaxable1 - ($PAYE1 + $PAYE2 + $PAYE3);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm4) / 100);
                }

                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
                    $PAYE_X = $PAYE_X + (($PAYE4 * $PAYE_Comm4) / 100);
                }

                //alert ($TotTaxable);

                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if (($TotTaxable1 > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable1 < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
                    $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
                    $PAYE_X = $PAYE_X + (($PAYE_Rem * $PAYE_Comm5) / 100);
                }
                //If Amount Left is not up to FIFTH BAND (PAYE5)
                if ($TotTaxable1 >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
                    $PAYE_X = $PAYE_X + (($PAYE5 * $PAYE_Comm5) / 100);
                }

                //If Amount Left is not up to SIXTH BAND (PAYE6)

                if ($TotTaxable1 > $PAYE6) {
                    $PAYE_X = $PAYE_X + ((($TotTaxable1 - $PAYE6) * $PAYE_Comm6) / 100);
                }
                // }

                // for now make $PAYE_X zero

                $PAYE_X = 0;
            }


            if ($_SESSION['StkTckCustID'] === '092f904d7a11f9ad2c827afe889d7cda') {
                $NetPay = (float) (($GrossPay - $Total_DR) - $PAYE - $Pension);
            } else {
                $NetPay = (float) (($GrossPay - $Total_DR) - $PAYE - $Pension + $Total_FP);
            }


            if (isset($GLOBALS["v_PayItem1"])) {

                $v_PayItem1 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem1"])), '2', '.', '');
            } else {
                $v_PayItem1 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem2"])) {
                $v_PayItem2 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem2"])), '2', '.', '');
            } else {
                $v_PayItem2 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem3"])) {
                $v_PayItem3 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem3"])), '2', '.', '');
            } else {
                $v_PayItem3 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem4"])) {
                $v_PayItem4 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem4"])), '2', '.', '');
            } else {
                $v_PayItem4 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem5"])) {
                $v_PayItem5 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem5"])), '2', '.', '');
            } else {
                $v_PayItem5 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem6"])) {
                $v_PayItem6 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem6"])), '2', '.', '');
            } else {
                $v_PayItem6 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem7"])) {
                $v_PayItem7 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem7"])), '2', '.', '');
            } else {
                $v_PayItem7 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem8"])) {
                $v_PayItem8 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem8"])), '2', '.', '');
            } else {
                $v_PayItem8 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem9"])) {
                $v_PayItem9 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem9"])), '2', '.', '');
            } else {
                $v_PayItem9 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem10"])) {
                $v_PayItem10 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem10"])), '2', '.', '');
            } else {
                $v_PayItem10 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem11"])) {
                $v_PayItem11 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem11"])), '2', '.', '');
            } else {
                $v_PayItem11 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem12"])) {
                $v_PayItem12 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem12"])), '2', '.', '');
            } else {
                $v_PayItem12 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem13"])) {
                $v_PayItem13 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem13"])), '2', '.', '');
            } else {
                $v_PayItem13 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem14"])) {
                $v_PayItem14 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem14"])), '2', '.', '');
            } else {
                $v_PayItem14 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem15"])) {
                $v_PayItem15 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem15"])), '2', '.', '');
            } else {
                $v_PayItem15 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem16"])) {
                $v_PayItem16 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem16"])), '2', '.', '');
            } else {
                $v_PayItem16 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem17"])) {
                $v_PayItem17 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem17"])), '2', '.', '');
            } else {
                $v_PayItem17 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem18"])) {
                $v_PayItem18 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem18"])), '2', '.', '');
            } else {
                $v_PayItem18 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem19"])) {
                $v_PayItem19 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem19"])), '2', '.', '');
            } else {
                $v_PayItem19 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem20"])) {
                $v_PayItem20 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem20"])), '2', '.', '');
            } else {
                $v_PayItem20 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem21"])) {
                $v_PayItem21 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem21"])), '2', '.', '');
            } else {
                $v_PayItem21 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem22"])) {
                $v_PayItem22 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem22"])), '2', '.', '');
            } else {
                $v_PayItem22 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem23"])) {
                $v_PayItem23 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem23"])), '2', '.', '');
            } else {
                $v_PayItem23 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem24"])) {
                $v_PayItem24 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem24"])), '2', '.', '');
            } else {
                $v_PayItem24 = 0.0;
            }
            if (isset($GLOBALS["v_PayItem25"])) {
                $v_PayItem25 = number_format(str_replace(',', '', trim($GLOBALS["v_PayItem25"])), '2', '.', '');
            } else {
                $v_PayItem25 = 0.0;
            }

            //if (isset($_REQUEST["PensionEmployee"])){
            if ($PensionEmployee != "") {
                $v_PensionEmployee = number_format(str_replace(',', '', trim($PensionEmployee)), '2', '.', '');
            } else {
                $v_PensionEmployee = 0.0;
            }

            if (isset($PensionEmployer)) {
                $v_PensionEmployer = number_format(str_replace(',', '', trim($PensionEmployer)), '2', '.', '');
            } else {
                $v_PensionEmployer = 0.0;
            }
            if (isset($LeaveAllowance) && is_numeric(str_replace(',', '', trim($LeaveAllowance)))) {
                $v_LeaveAllowance = number_format(str_replace(',', '', trim($LeaveAllowance)), '2', '.', '');
            } else {
                $v_LeaveAllowance = 0.0;
            }

            if (isset($TRelief) && is_numeric(str_replace(',', '', trim($TRelief)))) {
                $v_TRelief = number_format(str_replace(',', '', trim($TRelief)), '2', '.', '');
            } else {
                $v_TRelief = 0.0;
            }

            //if (isset($_REQUEST["PAYE"])){
            if ($PAYE != "") {
                $v_PAYE = number_format(str_replace(',', '', trim($PAYE)), '2', '.', '');
            } else {
                $v_PAYE = 0.0;
            }

            //if (isset($_REQUEST["TTaxable"])){
            if ($TTaxable != "") {
                $v_TTaxable = number_format(str_replace(',', '', trim($TTaxable)), '2', '.', '');
            } else {
                $v_TTaxable = 0.0;
            }

            $v_NetPay = 0.0;

            //if (isset($_REQUEST["NetPay"]))
            if ($NetPay != "") {
                $v_NetPay = number_format(str_replace(',', '', trim($NetPay)), '2', '.', '');
                if (!is_numeric($v_NetPay)) {
                    $v_NetPay = 0.0;
                }
            }

            if (isset($AuthCalc)) {
                $AutoCalc_ = 'on';
            } else {
                $AutoCalc_ = '';
            }

            /*if (isset($_REQUEST["ShiftPay"]) && $_REQUEST["ShiftPay"]=="on")
            {$ShiftPay_ = "on";} else {$ShiftPay_ = "";}*/

            if (isset($new_gross)) {
                $v_niz_gross = number_format(str_replace(',', '', trim($new_gross)), '2', '.', '');
            } else {
                $v_niz_gross = 0.0;
            }
            if (isset($Gross)) {
                $v_Gross = number_format(str_replace(',', '', trim($Gross)), '2', '.', '');
            } else {
                $v_Gross = 0.0;
            }

            if (isset($PAYE_X)) {
                $paye_leave_allowance = number_format(str_replace(',', '', trim($PAYE_X)), '2', '.', '');
            } else {
                $paye_leave_allowance = 0.0;
            }
            //                echo $_REQUEST["Gross"]."===========".$v_Gross;

            $Script = "UPDATE [Fin_PRCore]
				SET
				[Gross] ='" . ECh($v_Gross) . "'
				,[PayItem1] ='" . ECh($v_PayItem1) . "'
				,[PayItem2] ='" . ECh($v_PayItem2) . "'
				,[PayItem3] ='" . ECh($v_PayItem3) . "'
				,[PayItem4] ='" . ECh($v_PayItem4) . "'
				,[PayItem5] ='" . ECh($v_PayItem5) . "'
				,[PayItem6] ='" . ECh($v_PayItem6) . "'
				,[PayItem7] ='" . ECh($v_PayItem7) . "'
				,[PayItem8] ='" . ECh($v_PayItem8) . "'
				,[PayItem9] ='" . ECh($v_PayItem9) . "'
				,[PayItem10] ='" . ECh($v_PayItem10) . "'
				,[PayItem11] ='" . ECh($v_PayItem11) . "'
				,[PayItem12] ='" . ECh($v_PayItem12) . "'
				,[PayItem13] ='" . ECh($v_PayItem13) . "'
				,[PayItem14] ='" . ECh($v_PayItem14) . "'
				,[PayItem15] ='" . ECh($v_PayItem15) . "'
				,[PayItem16] ='" . ECh($v_PayItem16) . "'
				,[PayItem17] ='" . ECh($v_PayItem17) . "'
				,[PayItem18] ='" . ECh($v_PayItem18) . "'
				,[PayItem19] ='" . ECh($v_PayItem19) . "'
				,[PayItem20] ='" . ECh($v_PayItem20) . "'
				,[PayItem21] ='" . ECh($v_PayItem21) . "'
				,[PayItem22] ='" . ECh($v_PayItem22) . "'
				,[PayItem23] ='" . ECh($v_PayItem23) . "'
				,[PayItem24] ='" . ECh($v_PayItem24) . "'
				,[PayItem25] ='" . ECh($v_PayItem25) . "'
				,[PensionEmployee] ='" . ECh($v_PensionEmployee) . "'
				,[PensionEmployer] ='" . ECh($v_PensionEmployer) . "'
				,[PAYE] ='" . ECh($v_PAYE) . "'
				,[LeaveAllowance] ='" . ECh($v_LeaveAllowance) . "'

				,[TTaxable] ='" . ECh($v_TTaxable) . "'

				,[NetPay] ='" . ECh($v_NetPay) . "'
				,[TRelief] ='" . ECh($v_TRelief) . "'
				,[Scheme] ='" . ECh($PayScheme_) . "'
				,[AuthCalc] ='" . $AutoCalc_ . "'
                ,[paye_leave_allowance] ='" . ECh($paye_leave_allowance) . "'
                ,[niz_gross] ='" . $v_niz_gross . "'
				,[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "'
				,[UpdatedDate] = GetDate()
				,[Status] = 'U'
				WHERE [HashKey] = '" . $HashKey . "'";

            //,[TTaxable] ='".ECh($v_TTaxable)."'
            //
            ScriptRunnerUD($Script, "Inst");
        }
    } else {
        $GoValidate = false;
        return ['status' => false, 'msg' => 'Select a payment scheme which this employee falls under.'];
    }

    return ['status' => true, 'msg' => 'Success'];

    // return ['status' => false, 'msg' => 'Employee ID is required'];

}
if (ValidateURths("PAYMENT TEMPLATE" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$GoValidate = true;
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitDoc"]) && $_REQUEST["SubmitDoc"] == "Upload") {

        $success = 0;
        $error = 0;
        $errorRows = [];
        $msg = '';
        $file = $_FILES['dmsupd']['tmp_name'];
        if ($file) {
            $csv_mimetypes = ['text/csv', 'application/csv', 'text/comma-separated-values', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/octet-stream'];
            if (!in_array($_FILES['dmsupd']['type'], $csv_mimetypes)) {
                echo ("<script type='text/javascript'>{ parent.msgbox('File format not accepted.', 'red'); }</script>");
            } elseif (($_FILES['dmsupd']['size'] >= 20000000)) {
                echo ("<script type='text/javascript'>{ parent.msgbox('File too large. File must not be greater than 20MB.', 'red'); }</script>");
            } else {
                if (($handle = fopen($file, "r")) !== false) {
                    fgetcsv($handle);
                    fgetcsv($handle);
                    fgetcsv($handle);
                    $headers = fgetcsv($handle);

                    $count = 5;
                    while (($data = fgetcsv($handle, 10000, ",")) !== false) {
                        list($emp_id, $gross, $payScheme, $payItem1, $payItem2, $payItem3, $payItem4, $payItem5, $payItem6, $payItem7, $payItem8, $payItem9, $payItem10, $payItem11, $payItem12, $payItem13, $payItem14, $payItem15, $payItem16, $payItem17, $payItem18, $payItem19, $payItem20, $payItem21, $payItem22, $payItem23, $payItem24, $payItem25) = $data;
                        list($status, $msg) = array_values(uploadEmpTemplat($data));

                        if ($status) {
                            $success++;
                        } else {
                            $error++;
                            $errorRows[$count] = $msg;
                        }

                        $count++;
                    }
                    fclose($handle);

                    $_SESSION['errorRows'] = (count($errorRows)) ? $errorRows : false;
                    echo ("<script type='text/javascript'>{parent.msgbox('{$msg}Completed<br>{$success} Records Uploded <br> {$error} Records Failed', 'red'); }</script>");
                } else {
                    echo ("<script type='text/javascript'>{ parent.msgbox('Something went wrong. Please Try Again', 'red'); }</script>");
                }
            }
        } else {
            echo ("<script type='text/javascript'>{ parent.msgbox('Select a file to upload', 'red'); }</script>");
        }
    }
}

?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
        echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
    } else { ?>
        <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>

    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

    <!-- Select 2-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
</head>

<body>
    <div class="col-md-12 text-center mt-70">
        <form name="Upd" method="post" enctype="multipart/form-data">
            <p style="color:black; font-weight:bold;">Upload Employee Payment Template</p>
            <p> Select a document file to upload</p>
            <div class="form-group  row" style="margin-bottom: 5px">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="box bg-primary" style="margin-bottom: 0px">
                        <div class="box-body text-center" style="padding: 0.7rem">
                            <div class="col-sm-6 input-group-sm">
                                <input type="file" name="dmsupd" id="dmsupd" accept=".csv" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-12 text-center mt-30 mb-20">

                    <input type="submit" name="SubmitDoc" id="SubmitDoc" value="Upload" class="btn btn-info btn-sm">
                    <input name="PgDoS" id="PgDoS" type="hidden" value="<?php echo DoSFormToken() ?>">
                    <p style="margin-top: 1rem">Only CSV comma delimiter files allowed. Max file individual upload size is: 20Mb</p>

                </div>
                <div class="col-md-6">
                    <?php if (isset($_SESSION['errorRows'])) : ?>
                        <?php if ($_SESSION['errorRows']) : ?>
                            <?php $errors = $_SESSION['errorRows'] ?>
                            <?php foreach ($errors as $key => $value) : ?>
                                <p><strong style="font-weight: 700">Row Number <?php echo $key ?> not inserted (<?php echo $value ?>)</strong></p>
                            <?php endforeach ?>
                        <?php endif ?>
                        <?php unset($_SESSION['errorRows']); ?>
                    <?php endif ?>
                </div>
            </div>
        </form>
    </div>

</body>
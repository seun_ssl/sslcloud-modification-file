<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';

/*
$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';
 */
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

$QueryStrVal = "";
$QueryStr = "";
$DidRun = "";
$Del = 0;
$Gross = $Net = $Pension = $PAYE = $Housing = $Transport = $Basic = $Total_DR_NTax = $total_bonus = 0;
$WeekEndAmt = $OTimeAmt = $OTimeHrs = $TDR_NTaxable = 0;
$TotalAmount = 0;

if (ValidateURths("BATCH PAY" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$ModMaster = ValidateURths("PAYROLL MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

$GoValidate = true;
//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

// Notify primary managers/Contact Officers after Salary has been created
if (isset($_REQUEST['SubmitTrans']) && $_REQUEST["SubmitTrans"] == "Create Payment") {
    // Fetch existing authorizer from Payroll Settings
    $Script = "SELECT SetValue from Settings where Setting ='Payroll Approver'";
    $id = ScriptRunner($Script, "SetValue");

    $sender_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);
    $sender_name = "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}";
    $branch_details_query = "Select OName From BrhMasters where Hashkey = '" . $_REQUEST['BranchID'] . "'";
    $branch_details = ScriptRunnercous($branch_details_query, "BranchDetails");
    $recipient_name = getUserDetailsfromHash(getBranchContact($_SESSION['StkTckHKey']))['FName'];

    if ($_REQUEST['BranchID'] == "ALL") {
        $branch_details = ['All branches'];
    }

    $subject = 'Salary Created Notification (' . $branch_details[0] . ') - ' . $_REQUEST['PayMonth'];
    $checkIfNotificationExistsQuery = "Select * from MailQueue where Subject = '" . $subject . "'";
    $queryResult = getData($checkIfNotificationExistsQuery);

    if ($id == "branch_contact" && $_REQUEST['PayMonth'] !== "") {
        if (count($queryResult) < 1) {
            sendMailToContactOfficer(getUserDetailsfromHash(getBranchContact($_SESSION['StkTckHKey']))['Email'], $recipient_name, $sender_name, $_REQUEST['PayMonth'], $branch_details[0]);
        }
    }

    if ($id == "branch_contact2" && $_REQUEST['PayMonth'] !== "") {
        if (count($queryResult) < 1) {
            sendMailToContactOfficer(getUserDetailsfromHash(getBranchContact2($_SESSION['StkTckHKey']))['Email'], $recipient_name, $sender_name, $_REQUEST['PayMonth'], $branch_details[0]);
        }
    }

    if ($id == "branch_contact3" && $_REQUEST['PayMonth'] !== "") {
        if (count($queryResult) < 1) {
            sendMailToContactOfficer(getUserDetailsfromHash(getBranchContact3($_SESSION['StkTckHKey']))['Email'], $recipient_name, $sender_name, $_REQUEST['PayMonth'], $branch_details[0]);
        }
    }

    if ($id == "branch_contact4" && $_REQUEST['PayMonth'] !== "") {
        if (count($queryResult) < 1) {
            sendMailToContactOfficer(getUserDetailsfromHash(getBranchContact4($_SESSION['StkTckHKey']))['Email'], $recipient_name, $sender_name, $_REQUEST['PayMonth'], $branch_details[0]);
        }
    }
}

if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}

$BrhID = "'A'";

if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
    $BrhID = "'" . ECh($_REQUEST["BranchID"]) . "'";
} elseif (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'ALL') {
    $Script = "Select HashKey from BrhMasters where Status='A'";
    $BrhID = ScriptRunnerJoin($Script, "HashKey");
} elseif (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'WITHOUT') {
    //$Script="Select HashKey from BrhMasters where Status='A'";
    //$BrhID=    ScriptRunnerJoin($Script,"HashKey");
    $BrhID = "W";
}

if ($BrhID == "W") {
    $branch_check_max = " Et.BranchID = '' ";
    $branch_check_min = " Em.BranchID = '' ";
} else {
    $branch_check_min = "Em.BranchID in (" . $BrhID . ")";
    $branch_check_max = "Et.BranchID in (" . $BrhID . ")";
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Payment") {

        if (ValidateURths("PAY SALARY" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $GoValidate = true;
        if (isset($_REQUEST["PayMonth"]) && strlen($_REQUEST["PayMonth"]) != 8) {
            echo ("<script type='text/javascript'>{ parent.msgbox('You must select a month to batch pay.', 'red'); }</script>");
            $GoValidate = false;
        }

        $PayDt = ECh("27 " . $_REQUEST["PayMonth"]);

        if ($GoValidate == true) {
            //get list of unauthorized employees and send warning

            $EmpCount = 0;

            if ($BrhID == "W") {
                $branch_check_max = " Et.BranchID = null ";
                $branch_check_min = " Em.BranchID = null ";
            } else {
                $branch_check_max = " Et.BranchID in (" . $BrhID . ") ";
                $branch_check_min = " Em.BranchID in (" . $BrhID . ") ";
            }

            $dbOpen2 = ("
                SELECT Et.HashKey AS HKey, Et.SName + ' ' + Et.ONames + ' ' + Et.FName AS [Full Name], Et.HashKey, Et.Status, Et.EmpStatus,
                (SELECT   TOP (1)  OverTime
                FROM       dbo.Fin_AttendSet AS Fs
                WHERE      (EID =
                (SELECT     TOP (1) ItemLevel
                FROM          dbo.EmpDtl AS Ed
                WHERE      (ItemCode = 'EMP') AND (StaffID = Et.HashKey)
                ORDER BY ItemStart DESC))) *
                (SELECT     SUM(OverTime) AS Expr1
                FROM          dbo.Fin_Attend AS Fa
                WHERE      (EmpID = Et.HashKey) AND (RecDate <=
                (SELECT     DATEADD(s, - 1, DATEADD(mm, DATEDIFF(m, 0, '" . $PayDt . "') + 1, 0)) AS Expr1)) AND (Status = 'A')) AS PayItem4,
                (SELECT     SUM(ExpPay) AS Pay5
                FROM          dbo.LnIndvOff AS LiO
                WHERE      (ExpPayDate <=
                (SELECT     DATEADD(s, - 1, DATEADD(mm, DATEDIFF(m, 0, '" . $PayDt . "') + 1, 0)) AS Expr1)) AND (Status = 'A') AND (EID = Et.HashKey)) AS PayItem5,
                Et.Email, Et.PENCommNo, Et.HInsurNo, Et.SalAcctNo, Et.SalBank, Et.Department, Et.SalSortCode, Et.TaxID, Et.PenCustID, Et.BranchID, FMc.PayItem1, FMc.PayItem2, Gross, FMc.real_gross,
                FMc.PayItem3,FMc.PayItem4,FMc.PayItem5, FMc.PayItem6, FMc.PayItem7, FMc.PayItem8, FMc.PayItem9, FMc.PayItem10, FMc.PayItem11, FMc.PayItem12, FMc.PayItem13, FMc.PayItem14,
                FMc.PayItem15, FMc.PayItem16, FMc.PayItem17, FMc.PayItem18, FMc.PayItem19, FMc.PayItem20, FMc.PayItem21, FMc.PayItem22, FMc.PayItem23, FMc.PayItem24,

                FMs.PayItemAn1, FMs.PayItemAn2, FMs.PayItemAn3, FMs.PayItemAn4, FMs.PayItemAn5, FMs.PayItemAn6, FMs.PayItemAn7, FMs.PayItemAn8,
                FMs.PayItemAn9, FMs.PayItemAn10, FMs.PayItemAn11, FMs.PayItemAn12, FMs.PayItemAn13, FMs.PayItemAn14, FMs.PayItemAn15, FMs.PayItemAn16,
                FMs.PayItemAn17, FMs.PayItemAn18, FMs.PayItemAn19, FMs.PayItemAn20, FMs.PayItemAn21, FMs.PayItemAn22, FMs.PayItemAn23, FMs.PayItemAn24, FMs.PayItemAn25,

                FMs.PayItemAnType1, FMs.PayItemAnType2, FMs.PayItemAnType3, FMs.PayItemAnType4, FMs.PayItemAnType5, FMs.PayItemAnType6,
                FMs.PayItemAnType7, FMs.PayItemAnType8,
                FMs.PayItemAnType9, FMs.PayItemAnType10, FMs.PayItemAnType11, FMs.PayItemAnType12, FMs.PayItemAnType13, FMs.PayItemAnType14,
                FMs.PayItemAnType15, FMs.PayItemAnType16,
                FMs.PayItemAnType17, FMs.PayItemAnType18, FMs.PayItemAnType19, FMs.PayItemAnType20, FMs.PayItemAnType21, FMs.PayItemAnType22,
                FMs.PayItemAnType23, FMs.PayItemAnType24, FMs.PayItemAnType25,

                FMc.PayItem25, FMs.PayItemNm1, FMs.PayItemOF1, FMs.PayItemTX1, FMs.PayItemCD1, FMs.PayItemPen1, FMs.PayItemNm2, FMs.PayItemOF2, FMs.PayItemTX2,
                FMs.PayItemCD2, FMs.PayItemPen2, FMs.PayItemNm3, FMs.PayItemOF3, FMs.PayItemTX3, FMs.PayItemCD3, FMs.PayItemPen3, FMs.PayItemNm4,
                FMs.PayItemOF4, FMs.PayItemTX4, FMs.PayItemCD4, FMs.PayItemPen4, FMs.PayItemNm5, FMs.PayItemOF5, FMs.PayItemTX5, FMs.PayItemCD5,
                FMs.PayItemPen5, FMs.PayItemNm6, FMs.PayItemOF6, FMs.PayItemTX6, FMs.PayItemCD6, FMs.PayItemPen6, FMs.PayItemNm7, FMs.PayItemOF7,
                FMs.PayItemTX7, FMs.PayItemCD7, FMs.PayItemPen7, FMs.PayItemNm8, FMs.PayItemOF8, FMs.PayItemTX8, FMs.PayItemCD8, FMs.PayItemPen8,
                FMs.PayItemNm9, FMs.PayItemOF9, FMs.PayItemTX9, FMs.PayItemCD9, FMs.PayItemPen9, FMs.PayItemNm10, FMs.PayItemOF10, FMs.PayItemTX10,
                FMs.PayItemCD10, FMs.PayItemPen10, FMs.PayItemNm11, FMs.PayItemOF11, FMs.PayItemTX11, FMs.PayItemCD11, FMs.PayItemPen11, FMs.PayItemNm12,
                FMs.PayItemOF12, FMs.PayItemTX12, FMs.PayItemCD12, FMs.PayItemPen12, FMs.PayItemNm13, FMs.PayItemOF13, FMs.PayItemTX13, FMs.PayItemCD13,
                FMs.PayItemPen13, FMs.PayItemNm14, FMs.PayItemOF14, FMs.PayItemTX14, FMs.PayItemCD14, FMs.PayItemPen14, FMs.PayItemNm15, FMs.PayItemOF15,
                FMs.PayItemTX15, FMs.PayItemCD15, FMs.PayItemPen15, FMs.PayItemNm16, FMs.PayItemOF16, FMs.PayItemTX16, FMs.PayItemCD16, FMs.PayItemPen16,
                FMs.PayItemNm17, FMs.PayItemOF17, FMs.PayItemTX17, FMs.PayItemCD17, FMs.PayItemPen17, FMs.PayItemNm18, FMs.PayItemOF18, FMs.PayItemTX18,
                FMs.PayItemCD18, FMs.PayItemPen18, FMs.PayItemNm19, FMs.PayItemOF19, FMs.PayItemTX19, FMs.PayItemCD19, FMs.PayItemPen19, FMs.PayItemNm20,
                FMs.PayItemOF20, FMs.PayItemTX20, FMs.PayItemCD20, FMs.PayItemPen20, FMs.PayItemNm21, FMs.PayItemOF21, FMs.PayItemTX21, FMs.PayItemCD21,
                FMs.PayItemPen21, FMs.PayItemNm22, FMs.PayItemOF22, FMs.PayItemTX22, FMs.PayItemCD22, FMs.PayItemPen22, FMs.PayItemNm23, FMs.PayItemOF23,
                FMs.PayItemTX23, FMs.PayItemCD23, FMs.PayItemPen23, FMs.PayItemNm24, FMs.PayItemOF24, FMs.PayItemTX24, FMs.PayItemCD24, FMs.PayItemPen24,
                FMs.PayItemNm25, FMs.PayItemOF25, FMs.PayItemTX25, FMs.PayItemCD25, FMs.PayItemPen25, FMc.PensionEmployee, FMc.PensionEmployer,
                FMc.LeaveAllowance, FMc.PAYE, FMc.TTaxable, FMc.NetPay, FMc.TRelief ,FMc.Scheme, FMs.PAYEScheme
                FROM         dbo.EmpTbl AS Et INNER JOIN
                dbo.Fin_PRCore AS FMc ON Et.HashKey = FMc.EmpID INNER JOIN
                dbo.Fin_PRSettings AS FMs ON FMc.Scheme = FMs.HashKey
                WHERE     (Et.EmpStatus = 'Active') AND (Et.Status IN ('A', 'U')) AND (FMc.Status = 'A') AND " . $branch_check_max . " order by Et.SName asc
                            ");
            //exit($branch_check_max);
            include '../login/dbOpen2.php';
            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                $TotalAmount = $TotalAmountOverTime = 0;
                $TotalAmountAbsent = 0;
                $CR = $DR = $TTaxable = $TPension = $TTaxable = $NetPay = $LoanAmt = $TRelief = $PensionEmployer = $PensionEmployee = $Gross = $PAYE = 0;

                $GoValidate = true;
                $Script = "SELECT count(EmpID) Ct from [Fin_PRIndvPay] where EmpID='" . $row2['HKey'] . "' and PayMonth between '1 " . ECh($_REQUEST["PayMonth"]) . "' and '27 " . ECh($_REQUEST["PayMonth"]) . "' and Status<>'D'";
                //echo $Script;
                //exit;
                if (ScriptRunner($Script, "Ct") > 0) {
                    echo ("<script type='text/javascript'>{ parent.msgbox('A payroll record already exist for some staff for the month of " . $_REQUEST["PayMonth"] . ". Unpaid staff payment raised.', 'blue'); }</script>");
                    $GoValidate = false;
                }

                if ($GoValidate == true && strlen($row2['HKey']) == 32) {
                    $EmpCount = $EmpCount + 1;
                    $EvOd = 0;

                    $HKey = $row2['HKey'];
                    $QueryStrVal = "";
                    $QueryStr = "";
                    $TFP = 0;

                    // my data

                    // Lawrence added this to capture the annual bonus for each employee
                    // Added condition for annual pay

                    $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $HKey . "'";
                    $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
                    $Pay_month = date("n", strtotime(ECh($_REQUEST["PayMonth"])));

                    // Annual leave type

                    $script_lvType = "SELECT SetValue11 FROM Settings WHERE Setting = 'LeaveTyp'";
                    $lvType = Scriptrunner($script_lvType, "SetValue11");

                    $script_applied_leave = "SELECT *  FROM LvIndTOff WHERE LType = '0b23845da0e280008e5b9c7a2adfb462'
                    AND EmpID='" . $HKey . "' AND [Status] = 'A' AND Year(CONVERT(char(11),LvDate,106)) = YEAR(CONVERT(char(11),GETDATE(),106))
                    ORDER BY ApprovedDate ASC";

                    $approved = Scriptrunner($script_applied_leave, "Status");
                    $script_bonus_pay = "SELECT TOP(1) * FROM Fin_PRIndvPay
                    WHERE Year(CONVERT(char(11),PayMonth,106)) = YEAR(CONVERT(char(11),GETDATE(),106)) AND [Status] = 'A'
                    AND EmpID='" . $HKey . "'
                    ORDER BY  PayMonth ASC";
                    $PayBonus = Scriptrunner($script_bonus_pay, "payStatusBonus");

                    // First Month
                    $firstMonth = '1';

                    // last month pay begins
                    $lastMonth = '12';

                    for ($i = 1; $i <= 25; $i++) {


                        if ($i == 19 || $i == 20) {

                            if ($row2['PayItemCD' . $i] == "FP" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                $QueryStrVal = $QueryStrVal . "," . $row2['PayItem' . $i] / 12;
                                $QueryStr = $QueryStr . ",PayItem" . $i;


                                (float) $TFP += (float)  $row2['PayItem' . $i] / 12;
                            }
                        }
                        if ($i == 7) //Calculate LOAN repayment amount
                        {
                            // var_dump("dgdgfd");

                            //     $Script_Loan = "Update LnIndvOff set Status='B', Paid=ExpPay where EID='" . $row2['HKey'] . "' and Status='A'
                            // and (Month(ExpPayDate) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(ExpPayDate) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))";
                            //     ScriptRunnerUD($Script_Loan, "Sm");
                            //     //on authorization UPDATE outstanding Payment in LnDetails table

                            //     $Script_Loan = "Select ExpPay, HashKey from LnIndvOff where EID='" . $row2['HKey'] . "' and Status in ('B')
                            // and (Month(ExpPayDate) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(ExpPayDate) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))"; //Status 'B' means BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid

                            //     $LoanAmt = (float) ScriptRunner($Script_Loan, "ExpPay");
                            //     $_ind_loan_id = ScriptRunner($Script_Loan, "HashKey");

                            $dbOpen7 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, * from LnIndvOff  where EID='" . $row2['HKey'] . "' and Status not in ('D','P','B') Order by ExpPayDate ASC");
                            // var_dump($dbOpen7);

                            include '../login/dbOpen7.php';

                            $row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH);
                            // var_dump($row7);
                            //$OutSPay2 = $OutSPay2 + $row7['OutSPay'];
                            //$RePayAmt2 = $RePayAmt2 + $row7['RePayAmt'];
                            $ExpPay2 = $row7['ExpPay'];
                            $EID = $row7['EID'];
                            $LID = $row7['LID'];
                            $ExpPDate = $row7['ExpPD'];
                            $HashKey = $row7['HashKey'];

                            $_REQUEST['_pay_date'] = $ExpPDate;
                            $_REQUEST['_ind_loan_id'] = $HashKey;
                            $_REQUEST['_emp_hashkey'] = $EID;
                            $_REQUEST['repay_loan'] = $ExpPay2;


                            include '../login/dbClose7.php';
                            // var_dump($ExpPay2);
                            if (is_numeric($ExpPay2)) {
                                include 'loan_deduction_mgr.php';
                            } //Multiply the loan amount by 12 cos it would be divided below by 12

                        }
                        if ($row2['PayItemOF' . $i] == 1) {

                            $PVal = "PayItem" . $i;
                            $$PVal = $row2["PayItem" . $i];

                            if (!is_numeric($$PVal)) {
                                $$PVal = 0.0;
                            } //Check if the value is numeric to enable it be added

                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            /* I commented out Overtime and weekend allowance
                            if ($i==4) // Calculations for Overtime Allowance -- PAYITEM 4
                            {
                            $Script_OvTime="Update Fin_Attend Set Status='B' where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 ".ECh($_REQUEST["PayMonth"])."')+1,0))) and EmpID='".$row2['HKey']."' and Status in ('A') and RecType='O'";
                            //                            echo $Script_OvTime."<br>";
                            ScriptRunnerUD($Script_OvTime,"Sm");

                            $Script_OvTime="Select Sum(OverTime) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 ".ECh($_REQUEST["PayMonth"])."')+1,0))) and EmpID='".$row2['HKey']."' and Status in ('B') and RecType='O'";
                            //                            echo $Script_OvTime."<br>";

                            $OTimeHrs=ScriptRunner($Script_OvTime,"Sm"); //Get Total Overtime Hours
                            if (!is_numeric($OTimeHrs)) {$OTimeHrs=0;} // Set value of overtime hours to 0 if value is not numeric

                            $Script_OvTime="select * from Fin_AttendSet where PayCalcType='Overtime' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='".$row2['HKey']."' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)
                            //
                            if (ScriptRunner($Script_OvTime,"UseType")=="Percent")
                            {
                            $Amt = floatval(ScriptRunner($Script_OvTime,"PCent")); // Gets the % of basic to be paid
                            //echo "Amt=".$Amt."<br>";
                            $BasicPay = $row2["PayItem1"];
                            //echo "Basic=".$BasicPay."<br>";

                            $HourlyPay = (($BasicPay/12) * $Amt) / 100;  // Get the Hourly amount paid
                            //echo "HourlyPay=".$HourlyPay."<br>";

                            $$PVal = ($HourlyPay * $OTimeHrs) * 12; //To convert to annual rate *12
                            //echo "PVal=".$$PVal."<br>";
                            //exit;
                            }
                            elseif (ScriptRunner($Script_OvTime,"UseType")=="Value")
                            {
                            $OTimeAmt=ScriptRunner($Script_OvTime,"OverTime");
                            if (!is_numeric($OTimeAmt)) {$OTimeAmt=0;}

                            if (is_numeric($OTimeAmt))
                            {$$PVal = $OTimeAmt*$OTimeHrs;} //Get Overtime Hours multiplied by Overtime set amount for employee level
                            else
                            {$$PVal = "0.0";}
                            }
                            //echo "My Overtime===".$$PVal;
                            $$PVal = $$PVal * 12;
                            $OTimeAmt=$$PVal;
                            }
                             */
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //   var_dump($i);

                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            /*
                            if ($i==5) // Calculations for WEEKEND Allowance -- PAYITEM 5
                            {
                            $Script_OvTime="Update Fin_Attend Set Status='B' where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 ".ECh($_REQUEST["PayMonth"])."')+1,0))) and EmpID='".$row2['HKey']."' and Status in ('A') and RecType='W'";
                            //                            echo $Script_OvTime."<br>";
                            ScriptRunnerUD($Script_OvTime,"Sm");

                            $Script_OvTime="Select Count(*) Sm from Fin_Attend where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 ".ECh($_REQUEST["PayMonth"])."')+1,0))) and EmpID='".$row2['HKey']."' and Status in ('B') and RecType='W'";
                            //                            echo $Script_OvTime."<br>";

                            $OTimeHrs=ScriptRunner($Script_OvTime,"Sm"); //Get Total Overtime Hours
                            if (!is_numeric($OTimeHrs)) {$OTimeHrs=0;} // Set value of overtime hours to 0 if value is not numeric

                            $Script_OvTime="select * from Fin_AttendSet where PayCalcType='Weekend' and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='".$row2['HKey']."' order by ItemStart desc)"; //Get the type of calculation for overtime specified (% of Basic or Vaule per employee level)
                            //                            echo $Script_OvTime."<br>";

                            if (ScriptRunner($Script_OvTime,"UseType")=="Percent")
                            {
                            $Amt = floatval(ScriptRunner($Script_OvTime,"PCent")); // Gets the % of basic to be paid
                            //echo "Amt=".$Amt."<br>";
                            $BasicPay = $row2["PayItem1"];
                            //echo "Basic=".$BasicPay."<br>";

                            $HourlyPay = (($BasicPay/12) * $Amt) / 100;  // Get the Hourly amount paid
                            //echo "HourlyPay=".$HourlyPay."<br>";

                            $$PVal = ($HourlyPay * $OTimeHrs) * 12; //To convert to annual rate *12
                            //echo "PVal=".$$PVal."<br>";
                            //exit;
                            }
                            elseif (ScriptRunner($Script_OvTime,"UseType")=="Value")
                            {
                            $WeekEndAmt=ScriptRunner($Script_OvTime,"OverTime");
                            if (!is_numeric($WeekEndAmt)) {$WeekEndAmt=0;}

                            if (is_numeric($WeekEndAmt))
                            {$$PVal = $WeekEndAmt*$OTimeHrs;} //Get Overtime Hours multiplied by Overtime set amount for employee level
                            else
                            {$$PVal = "0.0";}
                            }
                            //echo "My Overtime===".$$PVal;
                            $$PVal = $$PVal * 12;
                            $WeekEndAmt=$$PVal;
                            }
                             */
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                $test = $row2['PayItemOF' . $i];

                                $CR = $CR + $$PVal;

                                if ($row2['PayItemTX' . $i] == 1) {
                                    $TTaxable = $TTaxable + $$PVal;
                                }

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension + $$PVal;
                                }
                            } elseif ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1) {
                                $DR = $DR + $$PVal;
                                //if ($row2['PayItemTX'.$i]==1)
                                //{$TTaxable=$TTaxable + floatval($$PVal);} //Commented by Onyema Amaobi 18th Jan. You can't tax deductions

                                if ($row2['PayItemPen' . $i] == 1) {
                                    $TPension = $TPension + $$PVal;
                                }

                                if ($row2['PayItemTX' . $i] == 0) {
                                    $TDR_NTaxable = $TDR_NTaxable . 'PayItem' . $i . ';';
                                }

                                //echo $TTaxable."<br>--".$$PVal."--<br>";
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANN' && $Employed_date == $Pay_month) {
                                // $CR = $CR + $$PVal;
                                // var_dump('here');
                                // Get total bonus for each employee
                                (float) $total_bonus += $$PVal * 12;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANL' && $approved == 'A' && $PayBonus != 'A') {
                                // $CR = $CR + $$PVal;

                                // Get total bonus for each employee
                                (float) $total_bonus += $$PVal * 12;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'JAN' && $firstMonth == $Pay_month) {
                                // $CR = $CR + $$PVal;

                                // Get total bonus for each employee
                                (float) $total_bonus += $$PVal * 12;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'DEC' && $lastMonth == $Pay_month) {
                                // $CR = $CR + $$PVal;

                                // Get total bonus for each employee
                                (float) $total_bonus += $$PVal * 12;
                            }

                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================
                            //=============================================================================================================

                            if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANL' && $approved == 'A' && $PayBonus != 'A') {
                                if (floatval($$PVal) > 0) {
                                    // echo $$PVal;
                                    $QueryStrVal = $QueryStrVal . "," . $$PVal;
                                } else {
                                    $QueryStrVal = $QueryStrVal . ",0";
                                }

                                // echo " PayItem ".$i;
                                $QueryStr = $QueryStr . ",PayItem" . $i;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'DEC' && $lastMonth == $Pay_month) {
                                if (floatval($$PVal) > 0) {
                                    // echo $$PVal;
                                    $QueryStrVal = $QueryStrVal . "," . $$PVal;
                                } else {
                                    $QueryStrVal = $QueryStrVal . ",0";
                                }

                                // echo " PayItem ".$i;
                                $QueryStr = $QueryStr . ",PayItem" . $i;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'JAN' && $firstMonth == $Pay_month) {
                                if (floatval($$PVal) > 0) {
                                    // echo $$PVal;
                                    $QueryStrVal = $QueryStrVal . "," . $$PVal;
                                } else {
                                    $QueryStrVal = $QueryStrVal . ",0";
                                }

                                // echo " PayItem ".$i;
                                $QueryStr = $QueryStr . ",PayItem" . $i;
                            } elseif ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 1 && $row2['PayItemAnType' . $i] == 'ANN' && $Employed_date == $Pay_month) {
                                if (floatval($$PVal) > 0) {
                                    // echo $$PVal;
                                    $QueryStrVal = $QueryStrVal . "," . $$PVal;
                                } else {
                                    $QueryStrVal = $QueryStrVal . ",0";
                                }

                                // echo " PayItem ".$i;
                                $QueryStr = $QueryStr . ",PayItem" . $i;
                            } else {

                                if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                    if (floatval($$PVal) > 0) {
                                        // echo $$PVal;
                                        $QueryStrVal = $QueryStrVal . "," . $$PVal / 12;
                                    } else {
                                        $QueryStrVal = $QueryStrVal . ",0";
                                    }

                                    // echo " PayItem ".$i;
                                    $QueryStr = $QueryStr . ",PayItem" . $i;
                                } else if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItemAn' . $i] == 0) {
                                    if (floatval($$PVal) > 0) {
                                        // echo $$PVal;
                                        $QueryStrVal = $QueryStrVal . "," . $$PVal / 12;
                                    } else {
                                        $QueryStrVal = $QueryStrVal . ",0";
                                    }

                                    // echo " PayItem ".$i;
                                    $QueryStr = $QueryStr . ",PayItem" . $i;
                                }
                            }
                        }
                    }

                    $Scheme = $row2['PAYEScheme'];
                    $HKey = $row2['HKey'];

                    if (floatval($row2['real_gross']) > 0) {
                        $Gross = $row2['real_gross'];
                    } else if (floatval($row2['Gross']) > 0) {
                        $Gross = $row2['Gross'];
                    } else {
                        $Gross = 1;
                    }

                    //echo "TTaxable=".$TTaxable."<br>";
                    if (!isset($TPension)) {
                        $TPension = 0;
                    }
                    //if (!isset($TTaxable )){$TTaxable =0;}
                    if (!isset($DR)) {
                        $DR = 0;
                    }
                    if (!isset($CR)) {
                        $CR = 0;
                    }
                    if (!isset($TTaxable)) {
                        $TTaxable = 0;
                    }

                    $Script_TAX = "SELECT * from FinTax where HashKey='" . $row2['PAYEScheme'] . "'";
                    $PAYE_Rem = 0;
                    $PCent_PensionEmployee = ScriptRunner($Script_TAX, "PensionEmployee");
                    $PCent_PensionEmployer = ScriptRunner($Script_TAX, "PensionEmployer");

                    $PensionEmployer = ($TPension * $PCent_PensionEmployer) / 100;
                    $PensionEmployee = ($TPension * $PCent_PensionEmployee) / 100;
                    //echo "TTax=".$TTaxable."<br>----<br>";

                    $Gross = $Gross + $OTimeAmt; //Add Overtimr amount to gross
                    $TRelief = 200000 + ($Gross * (20 / 100)) + $PensionEmployee + $Total_DR_NTax;

                    $TTaxable = $Gross - $TRelief; //Remove all reliefs before taxing

                    /*echo "Gross=".$Gross."<br>----<br>";
                    echo "PensionEmployee=".$PensionEmployee."<br>----<br>";
                    echo "Total_DR_NTax=".$Total_DR_NTax."<br>----<br>";
                    echo "TRelief=".$TRelief."<br>----<br>";
                    echo "TTax=".$TTaxable."<br>----<br>";
                    exit;*/

                    $PAYE = 0; //ScriptRunner($Script_TAX,"Limit0");
                    $PAYE0 = ScriptRunner($Script_TAX, "Limit0");
                    $PAYE1 = ScriptRunner($Script_TAX, "Limit1");
                    $PAYE2 = ScriptRunner($Script_TAX, "Limit2");
                    $PAYE3 = ScriptRunner($Script_TAX, "Limit3");
                    $PAYE4 = ScriptRunner($Script_TAX, "Limit4");
                    $PAYE5 = ScriptRunner($Script_TAX, "Limit5");
                    $PAYE6 = ScriptRunner($Script_TAX, "Limit6");
                    $PAYE7 = ScriptRunner($Script_TAX, "Limit7");

                    $PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
                    $PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
                    $PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
                    $PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
                    $PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
                    $PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
                    $PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
                    $PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");

                    // Applies the flat rate of 1%
                    /*
                    if ($GrossPay <= $PAYE0)
                    {$PAYE = ($GrossPay * $PAYE_Comm0)/100;}

                    // Applies first band of 7%
                    if (($TTaxable > ($PAYE0)) && ($TTaxable < ($PAYE1)))
                    {
                    $PAYE_Rem = $TTaxable;
                    $PAYE = (($PAYE_Rem * $PAYE_Comm1)/100);
                    }
                     */

                    // if ($TTaxable < $PAYE0)
                    if (($TTaxable < $PAYE0) && ($Gross <= $PAYE0)) {
                        $PAYE = ($Gross * $PAYE_Comm0) / 100;
                    }

                    //Amount taxed on first band
                    // if (($TTaxable > ($PAYE0)) && ($TTaxable < ($PAYE1)))
                    if (($TTaxable <= ($PAYE1)) && ($Gross > $PAYE0)) {
                        $PAYE_Rem = $TTaxable;
                        $PAYE = (($PAYE_Rem * $PAYE_Comm1) / 100);
                    }

                    if ($TTaxable >= ($PAYE1)) {
                        $PAYE = $PAYE + (($PAYE1 * $PAYE_Comm1) / 100);
                    }

                    //If Amount Left is not up to SECOND BAND (PAYE2)
                    if (($TTaxable > ($PAYE1)) && ($TTaxable < ($PAYE1 + $PAYE2))) {
                        $PAYE_Rem = $TTaxable - ($PAYE1);
                        $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm2) / 100);
                    }

                    if ($TTaxable >= ($PAYE1 + $PAYE2)) {
                        $PAYE = $PAYE + (($PAYE2 * $PAYE_Comm2) / 100);
                    }

                    //If Amount Left is not up to THIRD BAND (PAYE3)
                    if (($TTaxable > ($PAYE1 + $PAYE2)) && ($TTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
                        $PAYE_Rem = $TTaxable - ($PAYE1 + $PAYE2);
                        $PAYE = ($PAYE + (($PAYE_Rem * $PAYE_Comm3) / 100));
                    }

                    if ($TTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
                        $PAYE = $PAYE + (($PAYE3 * $PAYE_Comm3) / 100);
                    }

                    //If Amount Left is not up to FOURTH BAND (PAYE4)
                    if (($TTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
                        $PAYE_Rem = $TTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
                        $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm4) / 100);
                    }

                    if ($TTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
                        $PAYE = $PAYE + (($PAYE4 * $PAYE_Comm4) / 100);
                    }

                    //If Amount Left is not up to FIFTH BAND (PAYE5)
                    if (($TTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
                        $PAYE_Rem = $TTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
                        $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm5) / 100);
                    }

                    if ($TTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
                        $PAYE = $PAYE + (($PAYE5 * $PAYE_Comm5) / 100);
                    }

                    //If Amount Left is not up to SIXTH BAND (PAYE6)
                    //if (($TTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5 + $PAYE6)))
                    //{$PAYE_Rem = $TTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5); $PAYE = $PAYE + (($PAYE_Rem * $PAYE_Comm6)/100);}

                    if ($TTaxable > $PAYE6) {
                        $PAYE = $PAYE + ((($TTaxable - $PAYE6) * $PAYE_Comm6) / 100);
                    }

                    //$TotAllowance = ($PAYE + $PensionEmployer);
                    //echo $NetPay."<br>";
                    //echo $row2['LeaveAllowance']."<br>";
                    $NetPay = (($Gross - $DR) - $PAYE - $PensionEmployee); //-$row2['LeaveAllowance']);
                    //echo $NetPay;
                    //exit;
                    /*
                    echo "TTaxable: ".$TTaxable."<br>";
                    echo "Net: ".$NetPay."<br>";
                    echo "TotAllowance: ".$TRelief."<br>";
                    echo "PensionEmployer: ".$PensionEmployer."<br>";
                    echo "PensionEmployee: ".$PensionEmployee."<br>";
                    echo "Gross: ".$Gross."<br>";
                    echo "PAYE: ".$PAYE."<br>";
                    echo "DR: ".$DR."<br>";

                    exit;
                    //*/

                    // $PAYE = ($PAYE/12);
                    $PAYE = $row2["PAYE"] / 12;
                    $TRelief = ($TRelief / 12);
                    $NetPay = ($NetPay / 12);
                    $PensionEmployer = ($PensionEmployer / 12);
                    $PensionEmployee = ($PensionEmployee / 12);
                    // gross to be use for anniversary payment of salary
                    $gross_for_anniversary = $Gross;

                    $Gross = ($Gross / 12);
                    $TTaxable = ($TTaxable / 12);

                    // TO ADD ANNIVERSARY BONUS

                    $Script_for_anniversary = "SELECT * from FinTax where HashKey='" . $row2['PAYEScheme'] . "'";

                    $anniversary_percent = Scriptrunner($Script_for_anniversary, "AnniversaryAllw");
                    $anniversary_switch = Scriptrunner($Script_for_anniversary, "AnniversaryField");

                    $Script_to_get_EmpDt = "SELECT Month(CONVERT(char(11),EmpDt,106)) as EmpDt from EmpTbl where HashKey='" . $HKey . "'";
                    $Employed_date = Scriptrunner($Script_to_get_EmpDt, "EmpDt");
                    $Pay_month = date("n", strtotime(ECh($_REQUEST["PayMonth"])));

                    if ($Employed_date == $Pay_month && $anniversary_switch == "on") {

                        $bonus = $gross_for_anniversary * ($anniversary_percent / 100);
                    } else {

                        $bonus = ($gross_for_anniversary * ($anniversary_percent / 100) / 12);
                    }

                    // END OF ANNIVERSARY BONUS

                    /*
                    BATCH PAYMENT FOR DEDUCTION
                    I remove overtime column from the Select statement because no overtime for Wadoye yet.
                     */

                    $date_for_late = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));
                    // $dbOpen7 = "Select LatenessAmount, Exempt, OverTimeAmount from Fin_Attend FA where FA.Status in ('A') AND FA.RecType <> 'A'
                    //     AND FA.EmpID = '" . $HKey . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '$date_for_late'
                    //     AND Exempt='0'";
                    $dbOpen7 = "Select OverTimeAmount from Fin_Attend FA where FA.Status in ('A') and FA.[o_status] in ('A')
						AND FA.EmpID = '" . $HKey . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '$date_for_late'";

                    include '../login/dbOpen7.php';

                    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {

                        $OverTimeAmount = $row7['OverTimeAmount'];

                        $OverTimeAmount = (float) $OverTimeAmount;

                        $TotalAmountOverTime += $OverTimeAmount; // Total amount for overtime

                    }

                    include '../login/dbClose7.php';

                    $dbOpen7 = "Select LatenessAmount  from Fin_Attend FA where FA.Status in ('A') and FA.[l_status] in ('A')
						AND FA.EmpID = '" . $HKey . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '$date_for_late'";

                    include '../login/dbOpen7.php';

                    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {

                        $latnessAmount = $row7['LatenessAmount'];

                        $latnessAmount = (float) $latnessAmount;

                        $TotalAmount += $latnessAmount; // Total amount for lateness

                    }

                    include '../login/dbClose7.php';

                    // >>>>>>>>>>>>>>>>>>>> START ABSENTEEISM <<<<<<<<<<<<<<<<<<<<<<<<

                    $absent_month = date("Y-m", strtotime(ECh($_REQUEST["PayMonth"])));
                    $totalAbsentCount = ScriptRunner("SELECT COUNT(*) as total FROM Fin_Attend FA WHERE FA.Status in ('A') AND FA.RecType = 'A' AND FA.EmpID = '" . $HKey . "' AND CONVERT(CHAR(7),FA.RecDate,120) = '$absent_month' AND Exempt='0' AND AbsentAmount > 0", "total");
                    $Script_set = "SELECT Gross from Fin_PRCore where EmpID='" . $HKey . "' AND Status = 'A'";
                    $gross = ScriptRunner($Script_set, "Gross");
                    if ($gross) {
                        $dailyAbsentAmount = $gross / 365;
                        $AbsentAmount = (float) ($dailyAbsentAmount * $totalAbsentCount);
                        $TotalAmountAbsent += $AbsentAmount; // Total amount for absent
                    }
                    // >>>>>>>>>>>>>>>>>>>> END ABSENTEEISM <<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    $LeaveAllowance = $row2['LeaveAllowance'];
                    // var_dump($LeaveAllowance);
                    if (floatval($LeaveAllowance) > 0) {
                        $LeaveAllowance = $LeaveAllowance / 12;
                    } else {
                        $LeaveAllowance = 0;
                    }
                    // var_dump($LeaveAllowance);

                    //                    $PAYE=$row2['PAYE'];
                    //                    if (floatval($PAYE)>0){$PAYE=$PAYE/12;} else{$PAYE=0;}

                    //                    $TTaxable=$row2['TTaxable'];
                    //                    if (floatval($TTaxable)>0){$TTaxable=$TTaxable/12;} else{$TTaxable=0;}

                    //                    $NetPay=$row2['NetPay'];
                    //                    if (floatval($NetPay)>0){$NetPay=$NetPay/12;} else{$NetPay=0;}

                    //====================================================================================================================
                    //====================================================================================================================
                    //====================================================================================================================
                    //Calculate if Leave Allowance is in or Out
                    // $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $row2['PAYEScheme'] . "' and Status in ('A'))";
                    $Script_TAX = "SELECT * from FinTax where HashKey='" . $row2['PAYEScheme'] . "'";

                    if (ScriptRunner($Script_TAX, "LeaveField") == 'on') {
                        $LStatus = 'A';
                    } else {
                        if (floatval($LeaveAllowance) > 0) {
                            $LeaveAllowance = $LeaveAllowance * 12;
                        }
                        $LStatus = 'N';
                    }

                    //This area checks if a leave allowance is raised for the month and if inclusive, then add automatically else if exclusive, then wait for it to be authorized and then add.
                    $Script_Leave = "Select count(*) Ct from Fin_LvAllw where (Status not in ('D','P')) and EmpID='" . $HKey . "' and (Month(PayMonth) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(PayMonth) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))"; //Status 'B' menas BLOCKED. The amount has been blocked to be repaid. Waiting for authorization of the payroll to change to 'P' Paid
                    if (ScriptRunner($Script_Leave, "Ct") > 0) {
                        $Script_Leave = "Update Fin_LvAllw set AmtMonthly=" . $LeaveAllowance . ", Status='" . $LStatus . "',[AddedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[AddedDate]=GetDate() where (Status not in ('D','P')) and EmpID='" . $HKey . "' and (Month(PayMonth) = MONTH('" . ECh($_REQUEST["PayMonth"]) . "') and YEAR(PayMonth) = YEAR('" . ECh($_REQUEST["PayMonth"]) . "'))";
                        ScriptRunnerUD($Script_Leave, "");
                    } else {
                        if ($LStatus == 'N') {

                            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "LeaveAllw" . $_SESSION["StkTck" . "UName"];
                            $HashKey__ = md5($UniqueKey);

                            $Script_Leave = "insert into Fin_LvAllw (EmpID,PayMonth,AmtMonthly,[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $HKey . "','27 " . ECh($_REQUEST["PayMonth"]) . "'," . $LeaveAllowance . ",'" . $LStatus . "','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey__ . "')";
                            ScriptRunnerUD($Script_Leave, "");
                        }
                    }

                    //Block out leave authorized for payment for the month
                    $Script_Leave = "Update Fin_LvAllw set Status='B' where (Status in ('A')) and EmpID='" . $HKey . "'"; // and (Month(PayMonth) = MONTH('".ECh($_REQUEST["PayMonth"])."') and YEAR(PayMonth) = YEAR('".ECh($_REQUEST["PayMonth"])."'))";
                    ScriptRunnerUD($Script_Leave, "");
                    //echo $Script_Leave;




                    if ($LStatus == 'N') {

                        // var_dump("here");
                        // //Calculate leave allowance to be paid
                        // $Script_Leave = "Select Sum(AmtMonthly) LvPay from Fin_LvAllw where (Status in ('B')) and EmpID='" . $HKey . "'";
                        // $LeaveAllowance = ScriptRunner($Script_Leave, "LvPay");

                        if (intval($LeaveAllowance) > 0) {
                            $LeaveAllowance = 0;
                        }
                    } else if ($LStatus == 'A') {
                        // var_dump("here2");

                        $LeaveAllowance = $row2['LeaveAllowance'] / 12;
                    }


                    // var_dump($LeaveAllowance);
                    // var_dump($TotalAmountOverTime, 'overtime');
                    // var_dump($LoanAmt, 'loan');
                    // var_dump($TotalAmountAbsent, 'absent');
                    // var_dump("LeaveAllowance system", $LeaveAllowance);

                    // $LeaveAllowance = $row2['LeaveAllowance'] / 12;
                    $LeaveAllowance = floatval($LeaveAllowance);
                    $NetPay = $row2['NetPay'] / 12;
                    $NetPay = floatval($NetPay);

                    $Script = "Select Count(*) Ct  from [LnDetails] where Status ='A' and LnAmt <> Paid and EID='" . $row2['HKey'] . "'";
                    $ExpPay2 = 0;
                    if (ScriptRunner($Script, "Ct") > 0) {
                        //$date_paid_for = "2017-02-28";
                        //$dbOpen7=("SELECT (select (convert(varchar(11),ExpPayDate)=convert(varchar(11),$date_paid_for)), * from LnIndvOff Where LID=Ld.HashKey from LnDetails Ld where EID='".$ $row2['HKey']."' and Status not in ('D')");

                        $dbOpen7 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, * from LnIndvOff  where EID='" . $row2['HKey'] . "' and Status not in ('D','P') Order by ExpPayDate ASC");

                        include '../login/dbOpen7.php';

                        //$LnAmt = 0;
                        //$ExpPay =0;

                        //$OutSPay2= 0;
                        //$RePayAmt2 = 0;
                        //$m=1;
                        //while($row7 = sqlsrv_fetch_array($result7,SQLSRV_FETCH_BOTH)){
                        $row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH);
                        //$OutSPay2 = $OutSPay2 + $row7['OutSPay'];
                        //$RePayAmt2 = $RePayAmt2 + $row7['RePayAmt'];
                        $ExpPay2 = $row7['ExpPay'];
                        $ExpPay2 = floatval($ExpPay2);

                        // $EID = $row7['EID'];
                        // $LID = $row7['LID'];
                        // $ExpPDate = $row7['ExpPD'];
                        // $HashKey = $row7['HashKey'];

                        //$m++;
                        //}
                    }

                    // var_dump("netpay", $row2['NetPay']);
                    // var_dump("netpay/12", $NetPay);
                    // var_dump("total_bonus", ($total_bonus / 12));
                    // var_dump("LeaveAllowance", $LeaveAllowance);
                    // var_dump("bonus", $bonus);
                    // var_dump("TotalAmountOverTime", $TotalAmountOverTime);
                    // var_dump("LoanAmt", $LoanAmt);
                    // var_dump("TotalAmount", $TotalAmount);
                    // var_dump("TotalAmountAbsent", $TotalAmountAbsent);

                    // $LeaveAllowance = ScriptRunner($Script_Leave, "LvPay");  will look into dis later

                    // //Calculate leave allowance to be paid
                    // $Script_Leave = "Select Sum(AmtMonthly) LvPay from Fin_LvAllw where (Status in ('B')) and EmpID='" . $HKey . "'";
                    // $LeaveAllowance = ScriptRunner($Script_Leave, "LvPay");
                    // // $LeaveAllowance = $row2['LeaveAllowance'] / 12;
                    // if (floatval($LeaveAllowance) == 0) {
                    //     $LeaveAllowance = 0;
                    // }

                    // $NetPay = $NetPay + ($total_bonus / 12) + $LeaveAllowance + $bonus + $TotalAmountOverTime - $ExpPay2 - $TotalAmount - $TotalAmountAbsent;

                    /*
                    echo "TTaxable: ".$TTaxable."<br>";
                    echo "Net: ".$NetPay."<br>";
                    echo "LoanAmt: ".$LoanAmt."<br>";
                    echo "TRelief: ".$TRelief."<br>";
                    echo "PensionEmployer: ".$PensionEmployer."<br>";
                    echo "PensionEmployee: ".$PensionEmployee."<br>";
                    echo "Gross: ".$Gross."<br>";
                    echo "PAYE: ".$PAYE."<br>";
                    echo "DR: ".$DR."<br>";
                    echo "LeaveAllowance: ".$LeaveAllowance."<br>";
                    echo "NetPay: ".$NetPay."<br>";
                     */

                    //====================================================================================================================
                    //====================================================================================================================
                    //====================================================================================================================
                    // var_dump("$TRelief");
                    // $LeaveAllowance = $row2['LeaveAllowance'] / 12;
                    $LeaveAllowance = floatval($LeaveAllowance);
                    $TRelief = $row2['TRelief'] / 12;
                    // var_dump("$TRelief");

                    $QueryStrVal = $QueryStrVal . ",'$Scheme','$HKey',$Gross,$PensionEmployee,$PensionEmployer,$LeaveAllowance,$PAYE,$TTaxable,$NetPay,$TRelief";
                    $QueryStr = $QueryStr . ",Scheme,EmpID,Gross,PensionEmployee,PensionEmployer,LeaveAllowance,PAYE,TTaxable,NetPay,TRelief";

                    // die();

                    //$Script="Select (Max(ID) + 1) as Mx from fin_payslip";
                    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "BatchPay" . $_SESSION["StkTck" . "UName"];
                    $HashKey = md5($UniqueKey . '28 ' . $_REQUEST["PayMonth"] . $row2['HKey'] . $EmpCount);

                    if ($PayBonus != 'A') {
                        $PayBonus = 'A';
                    }

                    $Script = "INSERT INTO [Fin_PRIndvPay] ([PayMonth]" .
                        $QueryStr .
                        ",[Status],[AddedBy],[AddedDate],[UpdatedBy],[UpdatedDate],[HashKey],[AnniversaryBonus], [LatenessAmount], [AbsentAmount], [OverTimeAmount], [payStatusBonus], [loanAmount]) VALUES ('" . ('27 ' . ECh($_REQUEST["PayMonth"])) . "'" .
                        $QueryStrVal .
                        ",'N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "', '" . $bonus . "', '" . $TotalAmount . "', '" . $TotalAmountAbsent . "', '" . $TotalAmountOverTime . "', '" . $PayBonus . "', '" . $ExpPay2 . "');";
                    //echo $Script.";<br>";
                    // print_r($Script);
                    // die();
                    //                    $Script="INSERT INTO Fin_LvAllw (EmpID,PayMonth,
                    //                    $Script="INSERT INTO Fin_Pension

                    if (strlen($QueryStr) > 30) {
                        ScriptRunnerUD($Script, "NComp");
                        $DidRun = true;
                    }
                }

                $Script_OvTime = "Update Fin_Attend Set Status='B' where RecDate<=(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'27 " . ECh($_REQUEST["PayMonth"]) . "')+1,0))) and EmpID='" . $row2['HKey'] . "' and Status in ('A')";

                ScriptRunnerUD($Script_OvTime, "Sm");
            }
        }

        if ($DidRun == true) {
            echo ("<script type='text/javascript'>{ parent.msgbox('New batch payroll payment created for all active employees.', 'green'); }</script>");
        } else {
            echo ("<script type='text/javascript'>{ parent.msgbox('No new employee payment records created.', 'blue'); }</script>");
        }

        //MailTrail("PAYROLL","T",'','','',''); //Sends a mail notifications
    }
    include '../main/clr_val.php'; /* CLEARS $EditID */
}
//**************************************************************************************************
//**************************************************************************************************

elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
    include '../main/clr_val.php';
}
?>


<script type="text/javascript">
    function GetMonth() {
        if (document.getElementById("PayMonth").value == '') { //document.getElementById("basic_pay").value='0.00';
        } else {
            $kk = document.getElementById("PayMonth").value.split(/-/);
            if ($kk[1] == 01) {
                document.getElementById("PayMonth").value = "Jan " + $kk[0];
            }
            if ($kk[1] == 02) {
                document.getElementById("PayMonth").value = "Feb " + $kk[0];
            }
            if ($kk[1] == 03) {
                document.getElementById("PayMonth").value = "Mar " + $kk[0];
            }
            if ($kk[1] == 04) {
                document.getElementById("PayMonth").value = "Apr " + $kk[0];
            }
            if ($kk[1] == 05) {
                document.getElementById("PayMonth").value = "May " + $kk[0];
            }
            if ($kk[1] == 06) {
                document.getElementById("PayMonth").value = "Jun " + $kk[0];
            }
            if ($kk[1] == 07) {
                document.getElementById("PayMonth").value = "Jul " + $kk[0];
            }
            if ($kk[1] == 08) {
                document.getElementById("PayMonth").value = "Aug " + $kk[0];
            }
            if ($kk[1] == 09) {
                document.getElementById("PayMonth").value = "Sep " + $kk[0];
            }
            if ($kk[1] == 10) {
                document.getElementById("PayMonth").value = "Oct " + $kk[0];
            }
            if ($kk[1] == 11) {
                document.getElementById("PayMonth").value = "Nov " + $kk[0];
            }
            if ($kk[1] == 12) {
                document.getElementById("PayMonth").value = "Dec " + $kk[0];
            }
        }
    }
</script>

<link href="../css/style_main.css" rel="stylesheet" type="text/css">

<script>
    $(function() {
        $("#PayMonth").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-12M",
            maxDate: "+12M",
            dateFormat: 'M yy'
        })
    });
</script>

<script>
    $(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(function() {
        $(document).tooltip();
    });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;" onLoad="doOnLoad();">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" onSubmit="GetMonth();">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4 col-2 text-center ">
                    </div>
                    <div class="col-md-4 col-8 text-center ">
                        <h4>
                            Raise a Batch Payment
                        </h4>
                    </div>
                    <div class="col-md-4 col-2 ">
                        <!-- <button class="btn btn-danger btn-sm pull-right " style="margin-top: 4%">Clear</button> -->
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Select Month :
                            </label>
                            <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                <input name="PayMonth" id="PayMonth" type="text" class="form-control" size="12" onChange="GetMonth();" readonly onClick="GetMonth();" value="<?php if (isset($_REQUEST["PayMonth"])) {
                                                                                                                                                                                    echo ($_REQUEST["PayMonth"]);
                                                                                                                                                                                } ?>" />
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Payment Branch
                                :</label>
                            <div class="col-sm-8 input-group input-group-sm" style="padding-right: 35px">
                                <?php
                                echo '<select name="BranchID" class="form-control" id="BranchID">
									<option value="--" selected="selected">--</option>';
                                if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
                                    $SelID = ECh($_REQUEST["BranchID"]);
                                }

                                if (!isset($SelID)) {
                                    $SelID = "";
                                }

                                if (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'ALL') {
                                    echo '<option selected value="ALL">All Branches</option>';
                                } else {
                                    echo '<option value="ALL">All Branches</option>';
                                }

                                if (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'WITHOUT') {
                                    echo '<option selected value="WITHOUT">Without Branches</option>';
                                } else {
                                    echo '<option value="WITHOUT">Without Branches</option>';
                                }

                                $dbOpen2 = ("SELECT OName, HashKey FROM BrhMasters where Status='A' ORDER BY OName");
                                include '../login/dbOpen2.php';
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    if ($SelID == $row2['HashKey']) {
                                        echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
                                    } else {
                                        echo '<option value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
                                    }
                                }
                                //include '../login/dbClose2.php';
                                echo '</select>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pull-right">
                    <div class="col-md-12">
                        <?php
                        if (isset($CmdTyp) && $CmdTyp == "Open") //
                        {
                            $EditID = '';
                        }
                        if (ValidateURths("BATCH PAY" . "C") == true) {
                            if ( /*$_REQUEST["PgTy"] == "AddIndTrans" &&*/($EditID == "" || $EditID == '--')) { ?>
                                <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Payment" />
                        <?php
                            } elseif ($EditID != "" || $EditID > 0) {
                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Schedule" />';
                                echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
                            } else {
                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Payment" />';
                            }
                        }
                        ?>
                        <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Load" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row" style="margin-top: 2%">
                    <div class="col-md-12">
                        <table class="table table-responsive" id="table">
                            <thead>
                                <tr>
                                    <th data-sorter="false"><span class="TinyTextTight">
                                            <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                            <label for="DelChk_All"></label>
                                        </span></th>
                                    <th>Pay Month</th>
                                    <th>Employee Name</th>
                                    <th>Basic</th>
                                    <th align="center" valign="middle" class="smallText" scope="col">Housing</th>
                                    <th>Transport</th>
                                    <th>PAYE</th>
                                    <th>Pension</th>
                                    <th>Net Pay</th>
                                    <th>Gross</th>
                                    <th>Payment Review</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Del = 0;

                                $dbOpen2 = ("SELECT (Convert(Varchar(3),DateName(M,Fm.PayMonth))+' '+Convert(Varchar(4),DateName(YYYY,Fm.PayMonth))) as Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where " . $branch_check_min . " and Em.HashKey=Fm.EmpID and (Fm.Status = 'N') and (Em.Status in ('A','U') and EmpStatus='Active')  order by PayMonth Desc, SName asc, UpdatedDate desc");
                                //echo $dbOpen2;
                                include '../login/dbOpen2.php';
                                $Basic = $Housing = $Transport = $PAYE = $Pension = $Net = $Gross = "";
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    $Del = $Del + 1;
                                ?>
                                    <tr>
                                        <td valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
                                            <label for="<?php echo ("DelBox" . $Del); ?>"></label>
                                        </td>
                                        <td class="TinyText">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
                                        <td class="TinyText">&nbsp;<?php echo (trim($row2['Nm'])); ?></td>
                                        <td align="right" valign="middle" class="TinyText" scope="col"><?php
                                                                                                        $Basic = $Basic + $row2['PayItem1'];
                                                                                                        echo number_format($row2['PayItem1'], 2); ?></td>
                                        <td align="right" valign="middle" class="TinyText" scope="col"><?php
                                                                                                        $Housing = $Housing + $row2['PayItem2'];
                                                                                                        echo number_format($row2['PayItem2'], 2); ?></td>
                                        <td align="right" valign="middle" class="TinyText" scope="col"><?php
                                                                                                        $Transport = $Transport + $row2['PayItem3'];
                                                                                                        echo number_format($row2['PayItem3'], 2); ?></td>
                                        <td align="right" valign="middle" class="TinyText" scope="col"><?php
                                                                                                        $PAYE = $PAYE + $row2['PAYE'];
                                                                                                        echo number_format($row2['PAYE'], 2); ?></td>
                                        <td align="right" valign="middle" class="TinyText" scope="col"><?php
                                                                                                        $Pension = $Pension + $row2['PensionEmployee'];
                                                                                                        echo number_format($row2['PensionEmployee'], 2); ?></td>
                                        <td align="right" valign="middle" scope="col" class="TinyText"><?php
                                                                                                        $Net = $Net + $row2['NetPay'];
                                                                                                        echo number_format($row2['NetPay'], 2); ?></td>
                                        <td align="right" valign="middle" scope="col" class="TinyText"><?php
                                                                                                        $Gross = $Gross + $row2['Gross'];
                                                                                                        echo number_format($row2['Gross'], 2); ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                }
                                echo '<input name="FAction" id="FAction" type="hidden" />
									<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                //include '../login/dbClose2.php';
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td valign="middle" class="smallText" scope="col" data-sorter="false">&nbsp;</td>
                                    <td align="center" valign="middle" scope="col">&nbsp;</td>
                                    <td align="center" valign="middle" scope="col">&nbsp;</td>
                                    <td align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Basic, 2); ?></td>
                                    <td align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Housing, 2); ?></td>
                                    <td align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Transport, 2); ?></td>
                                    <td align="right" valign="middle" scope="col"><span class="smallText"><?php echo number_format($PAYE, 2); ?></span></td>
                                    <td align="right" valign="middle" scope="col"><span class="smallText"><?php echo number_format($Pension, 2); ?></span></td>
                                    <td align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Net, 2); ?></td>
                                    <td align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Gross, 2); ?></td>
                                    <td>Payment Review</td>
                                    <td>Comment</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-6 d-flex">
                        <?php include '../main/pagination.php';
                        $Script = "SELECT SetValue from Settings where Setting ='Payroll Approver'";
                        $hashkey = ScriptRunner($Script, "SetValue");

                        ?>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <?php if (strlen($hashkey) == 32) : ?>
                            <button class="btn btn-danger btn-sm mt-1" type="button" onclick="deleteFun(); return;">Notify</button>

                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
	      						<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                        /*
//Check if any record was spolled at all before enabling buttons
//Check if user has Delete rights
$but_HasRth=("PAY SALARY"."D"); $but_Type = 'Delete'; include '../main/buttons.php';
//Check if user has Add/Edit rights
$but_HasRth=("PAY SALARY"."A"); $but_Type = 'View'; include '../main/buttons.php';
//Check if user has Authorization rights
$but_HasRth=("PAY SALARY"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
//Set FAction Hidden Value for each button
echo "<input name='FAction' id='FAction' type='hidden'>";

 */
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        function deleteFun() {

            $('<div></div>').appendTo('body')
                .html('<div><class=TextBoxText>Do you wish to <strong>notify the payroll authorizer</strong>?</h5></div>')
                .dialog({
                    modal: true,
                    title: "notify the payroll authorizer",
                    zIndex: 10000,
                    autoOpen: true,
                    width: 'auto',
                    resizable: true,
                    height: 'auto',
                    resizable: true,
                    buttons: {
                        Yes: function() {
                            $.ajax({
                                url: "notify_authorizer.php",
                                method: "POST",

                                success: function(response) {
                                    if (response === 'OK') {

                                        // console.log(allInfo);
                                        {
                                            parent.msgbox('payroll authorizer notified successfully', 'green');
                                        };
                                        location.reload();




                                    } else if (response === "Wrong") {

                                        {
                                            parent.msgbox('Something went wrong', 'red');
                                        };

                                    }
                                },
                                error: function() {
                                    alert('Something went wrong');
                                }
                            });

                            $(this).dialog("close");
                        },
                        No: function() {
                            $(this).dialog("close");
                        }
                    },
                    close: function(event, ui) {
                        $(this).remove();
                    }
                });

        }
    </script>
</body>
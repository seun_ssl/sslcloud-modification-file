<?php session_start();
error_reporting(E_ERROR | E_WARNING);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

if (ValidateURths("DEDUCTION MASTERS" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$ModMaster = ValidateURths("PAYROLL MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

$GoValidate = true;
$ClearPg = false;
//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
	$EditID = "--";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "--";
}
if (!isset($HashKey)) {
	$HashKey = "";
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Template") {
		if (ValidateURths("DEDUCTION MASTERS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}
		//    include 'payroll_validate.php';

		$Script = "Select * from fintax where [PAYEName]='" . ECh($_REQUEST["PAYEName"]) . "' and Status<>'D'";
		if (strlen(ScriptRunner($Script, "PAYEName")) > 0) {
			echo ("<script type='text/javascript'>{ parent.msgbox('A PAYE master record already exist with this name. Please review.', 'red'); }</script>");
			$GoValidate = false;
		}

		if ($GoValidate == true) {
			/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
			$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "PAYEGroupSetting";
			$HashKey = md5($UniqueKey);

			if (isset($_REQUEST["Incluv"])) {
				$LeaveInOut = "on";
			} else {
				$LeaveInOut = "";
			}
			if (isset($_REQUEST["Anniversary"])) {
				$AnniversaryBonus = "on";
			} else {
				$AnniversaryBonus = "";
			}
			if (isset($_REQUEST["leavePerGross"])) {
				$leavePerGross = "on";
			} else {
				$leavePerGross = "";
			}

			$Script = "INSERT INTO [FinTax]
			   ([PAYEName]
			   ,[PAYENarr]
			   ,[Limit0]
			   ,[pcent0]
			   ,[Limit1]
			   ,[pcent1]
			   ,[Limit2]
			   ,[pcent2]
			   ,[Limit3]
			   ,[pcent3]
			   ,[Limit4]
			   ,[pcent4]
			   ,[Limit5]
			   ,[pcent5]
			   ,[Limit6]
			   ,[pcent6]
			   ,[Limit7]
			   ,[pcent7]
			   ,[PensionEmployee]
			   ,[PensionEmployer]
			   ,[LeaveAllw]
			   ,[leavePerGross]
			   ,[LeaveField]
			   ,[AnniversaryAllw]
			   ,[AnniversaryField]
			   ,[Status]
			   ,[AddedBy]
			   ,[AddedDate]
			   ,[HashKey])
				Values(
				 '" . ECh($_REQUEST["PAYEName"]) . "'
			    ,'" . ECh($_REQUEST["PAYENarr"]) . "'
		,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit0"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent0"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit1"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent1"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit2"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent2"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit3"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent3"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit4"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent4"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit5"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent5"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit6"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent6"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit7"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent7"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["PensionEmployee"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["PensionEmployer"])) + 0.0, '2', '.', '')) . "'
				,'" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["LeaveAllw"])) + 0.0, '2', '.', '')) . "'
				,'" . $LeaveInOut . "'
				,'" . $leavePerGross . "'
				,NULL
				,'" . $AnniversaryBonus . "'
				,'N'
				,'" . ECh($_SESSION["StkTck" . "HKey"]) . "'
				,GetDate()
				,'" . $HashKey . "')";
			//            echo $Script;
			ScriptRunnerUD($Script, "NComp");
			AuditLog("INSERT", "New PAYE setting created [" . $_REQUEST["PAYEName"] . "]");

			echo ("<script type='text/javascript'>{ parent.msgbox('New PAYE setting created successfully.', 'green'); }</script>");
			include '../main/clr_val.php'; /* CLEARS $EditID */
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************

	//elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Template" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0)
	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Template") {
		//exit('update issue');
		if (ValidateURths("DEDUCTION MASTERS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$Script = "SELECT PAYEName from FinTax where PAYEName='" . ECh($_REQUEST["PAYEName"]) . "' and HashKey<>'" . $HashKey . "'";
		if (strlen(ScriptRunner($Script, "PAYEName")) > 0) {
			echo ("<script type='text/javascript'>{ parent.msgbox('Selected band name already exist', 'red'); }</script>");
			$GoValidate = false;
		}
		/* Set HashKey of Account to be Updated */
		$EditID = ECh($_REQUEST["UpdID"]);
		$HashKey = ECh($_REQUEST["UpdID"]);
		$GoValidate = true;

		if ($GoValidate == true) {
			//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
			if (ValidateUpdDel("SELECT Status from [FinTax] where [HashKey]= '" . $HashKey . "'") == true) {
				if (isset($_REQUEST["Incluv"])) {
					$LeaveInOut = "on";
				} else {
					$LeaveInOut = "";
				}

				//    Lawrence
				if (isset($_REQUEST["Anniversary"])) {
					$AnniversaryBonus = "on";
				} else {
					$AnniversaryBonus = "";
				}


				if (isset($_REQUEST["leavePerGross"])) {
					$leavePerGross = "on";
				} else {
					$leavePerGross = "";
				}

				$Script = "Update [FinTax] set
				 [PAYEName]='" . ECh($_REQUEST["PAYEName"]) . "'
			    ,[PAYENarr]='" . ECh($_REQUEST["PAYENarr"]) . "'
				,[Limit0]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit0"])) + 0.0, '2', '.', '')) . "'
				,[pcent0]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent0"])) + 0.0, '2', '.', '')) . "'
				,[Limit1]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit1"])) + 0.0, '2', '.', '')) . "'
				,[pcent1]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent1"])) + 0.0, '2', '.', '')) . "'
				,[Limit2]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit2"])) + 0.0, '2', '.', '')) . "'
				,[pcent2]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent2"])) + 0.0, '2', '.', '')) . "'
				,[Limit3]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit3"])) + 0.0, '2', '.', '')) . "'
				,[pcent3]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent3"])) + 0.0, '2', '.', '')) . "'
				,[Limit4]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit4"])) + 0.0, '2', '.', '')) . "'
				,[pcent4]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent4"])) + 0.0, '2', '.', '')) . "'
				,[Limit5]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit5"])) + 0.0, '2', '.', '')) . "'
				,[pcent5]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent5"])) + 0.0, '2', '.', '')) . "'
				,[Limit6]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit6"])) + 0.0, '2', '.', '')) . "'
				,[pcent6]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent6"])) + 0.0, '2', '.', '')) . "'
				,[Limit7]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["Limit7"])) + 0.0, '2', '.', '')) . "'
				,[pcent7]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["pcent7"])) + 0.0, '2', '.', '')) . "'
				,[PensionEmployee]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["PensionEmployee"])) + 0.0, '2', '.', '')) . "'
				,[PensionEmployer]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["PensionEmployer"])) + 0.0, '2', '.', '')) . "'
				,[LeaveAllw]='" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["LeaveAllw"])) + 0.0, '2', '.', '')) . "'
				,[LeaveField]='" . $LeaveInOut . "'
				,[AnniversaryAllw] = '" . ECh(number_format(trim(str_replace(",", "", $_REQUEST["AnniversaryAllw"])) + 0.0, '2', '.', '')) . "'
				,[AnniversaryField]='" . $AnniversaryBonus . "'
				,[leavePerGross]='" . $leavePerGross . "'
				,[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "'
				,[UpdatedDate]=GetDate()
				 where [HashKey]= '" . $HashKey . "'";

				//echo $Script;
				ScriptRunnerUD($Script, "Inst");
				AuditLog("INSERT", "PAYE band settings updated");

				echo ("<script type='text/javascript'>{ parent.msgbox('PAYE band setting updated successfully', 'green'); }</script>");
				include '../main/clr_val.php'; /* CLEARS $EditID */
			}
		}
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
		include '../main/clr_val.php';
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
		include '../main/clr_val.php'; /* CLEARS $EditID */
		if (strlen($_REQUEST["ClientName"]) > 2) {
			$EditID = $_REQUEST["ClientName"];
			$Script_Edit = "select * from [CliTbl] where HashKey='" . $EditID . "'";
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		//if (ValidateURths("PAYROLL SETTING"."V")!=true){include '../main/NoAccess.php';exit;} //Already at the top

		include '../main/clr_val.php'; /* CLEARS $EditID */
		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_Edit = "select * from [FinTax] where HashKey='" . $EditID . "'";
			}
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("DEDUCTION MASTERS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT PAYEName, HashKey from FinTax where HashKey='" . $EditID . "'";
				$EditID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [FinTax] WHERE [HashKey] = '" . $EditID . "'") == true) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [FinTax]
					SET [Status] = 'A'
					,[AuthBy] =  '" . ECh($_SESSION["StkTck" . "HKey"]) . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("AUTHORIZE", "Payroll deduction masters record authorized for " . ScriptRunner($Script_ID, "PAYEName"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected payroll deduction masters authorized successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		include '../main/clr_val.php'; /* CLEARS $EditID */
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("DEDUCTION MASTERS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT PAYEName, HashKey from FinTax where HashKey='" . $EditID . "'";
				$EditID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [FinTax] WHERE [HashKey] = '" . $EditID . "'") == false) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [FinTax]
					SET [Status] = 'U'
					,[AuthBy] =  '" . ECh($_SESSION["StkTck" . "HKey"]) . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("UNAUTHORIZE", "Payroll deduction masters record unauthorized for " . ScriptRunner($Script_ID, "PAYEName"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected payroll deduction masters unauthorized successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		include '../main/clr_val.php'; /* CLEARS $EditID */
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("DEDUCTION MASTERS" . "D") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT PAYEName, HashKey from FinTax where HashKey='" . $EditID . "'";
				$EditID = ScriptRunner($Script_ID, "HashKey");

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from [FinTax] WHERE [HashKey] = '" . $EditID . "'") == true) {
					//Check if account is customer account and if SMS alert is set to fire
					$Script = "UPDATE [FinTax]
					SET [Status] = 'D'
					,[DeletedBy] =  '" . ECh($_SESSION["StkTck" . "HKey"]) . "'
					,[DeletedDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("DELETE", "Payroll deduction masters record deleted for " . ScriptRunner($Script_ID, "PAYEName"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected payroll deduction masters deleted successfully.', 'green'); }</script>");
				}
			}
		}
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		include '../main/clr_val.php'; /* CLEARS $EditID */
	}
} ?>


<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	function MM_preloadImages() { //v3.0
		var d = document;
		if (d.images) {
			if (!d.MM_p) d.MM_p = new Array();
			var i, j = d.MM_p.length,
				a = MM_preloadImages.arguments;
			for (i = 0; i < a.length; i++)
				if (a[i].indexOf("#") != 0) {
					d.MM_p[j] = new Image;
					d.MM_p[j++].src = a[i];
				}
		}
	}

	function MM_swapImgRestore() { //v3.0
		var i, x, a = document.MM_sr;
		for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
		var p, i, x;
		if (!d) d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all) x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
		if (!x && d.getElementById) x = d.getElementById(n);
		return x;
	}

	function MM_swapImage() { //v3.0
		var i, j = 0,
			x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = MM_findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc) x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong {
		font-weight: 550;
	}
</style>

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-2">
						<a href="Pay_Setup.php" class="btn btn-default btn-sm" style="margin-top: 2%"><span class="fa  fa fa fa-arrow-circle-left"></span></a>
					</div>
					<div class="col-md-8 text-center">
						<h4>
							PAYE BAND SETTINGS
						</h4>
					</div>
					<div class="col-md-2">
						<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right btn-sm" style="margin-top: 2%" id="SubmitTrans" value="Clear" />
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row ">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Band Name <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								if ($EditID != "" && $EditID != "--") {
									echo '<input type="text" name="PAYEName" maxlength="32" class="form-control" id="PAYEName" value="' . ScriptRunner($Script_Edit, "PAYEName") . '" />';
								} elseif (isset($_REQUEST["PAYEName"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<input type="text" name="PAYEName" maxlength="32" class="form-control" id="PAYEName" value="' . $_REQUEST["PAYEName"] . '" />';
								} else {
									echo '<input type="text" name="PAYEName" maxlength="32" class="form-control" id="PAYEName" value="" />';
								}
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row ">
							<label class="col-sm-4 col-form-label"> Description :</label>
							<div class="col-sm-8" style=" padding-right: 35px">
								<?php
								if ($EditID != "" && $EditID != "--") {
									echo '<textarea name="PAYENarr" rows="2" class="form-control" id="PAYENarr">' . ScriptRunner($Script_Edit, "PAYENarr") . '</textarea>';
								} elseif (isset($_REQUEST["PAYENarr"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<textarea name="PAYENarr" rows="2" class="form-control" id="PAYENarr"></textarea>';
								} else {
									echo '<textarea name="PAYENarr" rows="2" class="form-control" id="PAYENarr"></textarea>';
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-responsive">
							<tr>
								<th style="font-weight: bolder;">Amount (of Taxable Pay)</th>
								<th style="font-weight: bolder;">% Tax Applied</th>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row" style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 0 - Flat</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit0" maxlength="12" class="form-control" id="Limit0" value="' . number_format(ScriptRunner($Script_Edit, "Limit0"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit0"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit0" maxlength="12" class="form-control" id="Limit0" value="' . $_REQUEST["Limit0"] . '" />';
											} else {
												echo '<input type="text" name="Limit0" maxlength="12" class="form-control" id="Limit0" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12  input-group input-group-sm" style=" padding-right: 35px">
											<datalist id="PCENTages">
												<?php for ($i = 0; $i <= 24; $i++) {
													echo '<option value="' . $i . '">' . $i . '</option>';
												} ?>
											</datalist>
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent0" style="border-left: 2px solid #fc4b6c;" id="pcent0" type="text" class="form-control" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent0") . '"/>';
											} else {
												echo '<input name="pcent0" style="border-left: 2px solid #fc4b6c;" id="pcent0" type="text" class="form-control" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 1 - First</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit1" maxlength="12" class="form-control" id="Limit1" value="' . number_format(ScriptRunner($Script_Edit, "Limit1"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit1"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit1" maxlength="12" class="form-control" id="Limit1" value="' . $_REQUEST["Limit1"] . '" />';
											} else {
												echo '<input type="text" name="Limit1" maxlength="12" class="form-control" id="Limit1" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent1" id="pcent1" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent1") . '"/>';
											} else {
												echo '<input name="pcent1" id="pcent1" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 2 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit2" maxlength="12" class="form-control" id="Limit2" value="' . number_format(ScriptRunner($Script_Edit, "Limit2"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit2"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit2" maxlength="12" class="form-control" id="Limit2" value="' . $_REQUEST["Limit2"] . '" />';
											} else {
												echo '<input type="text" name="Limit2" maxlength="12" class="form-control" id="Limit2" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent2" id="pcent2" style="border-left: 2px solid #fc4b6c;" type="text" class="form-control" size="5" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent2") . '"/>';
											} else {
												echo '<input name="pcent2" id="pcent2" style="border-left: 2px solid #fc4b6c;" type="text" class="form-control" size="5" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 3 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit3" maxlength="12" class="form-control" id="Limit3" value="' . number_format(ScriptRunner($Script_Edit, "Limit3"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit3"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit3" maxlength="12" class="form-control" id="Limit3" value="' . $_REQUEST["Limit3"] . '" />';
											} else {
												echo '<input type="text" name="Limit3" maxlength="12" class="form-control" id="Limit3" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent3" id="pcent3" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent3") . '"/>';
											} else {
												echo '<input name="pcent3" id="pcent3" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 4 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit4" maxlength="12" class="form-control" id="Limit4" value="' . number_format(ScriptRunner($Script_Edit, "Limit4"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit4"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit4" maxlength="12" class="form-control" id="Limit4" value="' . $_REQUEST["Limit4"] . '" />';
											} else {
												echo '<input type="text" name="Limit4" maxlength="12" class="form-control" id="Limit4" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent4" id="pcent4" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent4") . '"/>';
											} else {
												echo '<input name="pcent4" id="pcent4" type="text" class="form-control" style="border-left: 2px solid #fc4b6c;" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 5 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit5" maxlength="12" class="form-control" id="Limit5" value="' . number_format(ScriptRunner($Script_Edit, "Limit5"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit1"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit5" maxlength="12" class="form-control" id="Limit5" value="' . $_REQUEST["Limit5"] . '" />';
											} else {
												echo '<input type="text" name="Limit5" maxlength="12" class="form-control" id="Limit5" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent5" id="pcent5" style="border-left: 2px solid #fc4b6c;" type="text" class="form-control" size="5" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent5") . '"/>';
											} else {
												echo '<input name="pcent5" id="pcent5" style="border-left: 2px solid #fc4b6c;" type="text" class="form-control" size="5" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 6 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit6" maxlength="12" class="form-control" id="Limit6" value="' . number_format(ScriptRunner($Script_Edit, "Limit6"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit1"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit6" maxlength="12" class="form-control" id="Limit6" value="' . $_REQUEST["Limit6"] . '" />';
											} else {
												echo '<input type="text" name="Limit6" maxlength="12" class="form-control" id="Limit6" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent6" id="pcent6" type="text" style="border-left: 2px solid #fc4b6c;" class="form-control" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent6") . '"/>';
											} else {
												echo '<input name="pcent6" id="pcent6" type="text" style="border-left: 2px solid #fc4b6c;" class="form-control" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<label class="col-sm-3 col-form-label" style="padding-right: 0px;">Band 7 - Next</label>
										<div class="col-sm-6 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if ($EditID != "" && $EditID != "--") {
												echo '<input type="text" name="Limit7" maxlength="12" class="form-control" id="Limit7" value="' . number_format(ScriptRunner($Script_Edit, "Limit7"), '2', '.', ',') . '" />';
											} elseif (isset($_REQUEST["Limit7"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
												echo '<input type="text" name="Limit7" maxlength="12" class="form-control" id="Limit7" value="' . $_REQUEST["Limit7"] . '" />';
											} else {
												echo '<input type="text" name="Limit7" maxlength="12" class="form-control" id="Limit7" value="" />';
											}
											?>
										</div>
									</div>
								</td>
								<td style="padding: 5px;">
									<div class="form-group row " style="margin-bottom: 0px; padding-bottom: 0px">
										<div class="col-sm-12 input-group input-group-sm" style=" padding-right: 35px">
											<?php
											if (strlen($EditID) == 32) {
												echo '<input name="pcent7" id="pcent7" type="text" style="border-left: 2px solid #fc4b6c;" class="form-control" maxlength="3" list="PCENTages" value="' . ScriptRunner($Script_Edit, "pcent7") . '"/>';
											} else {
												echo '<input name="pcent7" id="pcent7" type="text" style="border-left: 2px solid #fc4b6c;" class="form-control" maxlength="3" list="PCENTages" value="0"/>';
											}
											?>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<div class="form-group row" style="margin-top: 50px">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Pension - Employee% :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								if ($EditID != "" && $EditID != "--") {
									echo '<input type="text" name="PensionEmployee" maxlength="4" class="form-control" id="PensionEmployee" value="' . ScriptRunner($Script_Edit, "PensionEmployee") . '" />';
								} elseif (isset($_REQUEST["PensionEmployee"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<input type="text" name="PensionEmployee" maxlength="4" class="form-control" id="PensionEmployee" value="' . $_REQUEST["PensionEmployee"] . '" />';
								} else {
									echo '<input type="text" name="PensionEmployee" maxlength="4" class="form-control" id="PensionEmployee" value="" />';
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Pension - Employer% :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								if ($EditID != "" && $EditID != "--") {
									echo '<input type="text" name="PensionEmployer" maxlength="3" class="form-control" id="PensionEmployer" value="' . ScriptRunner($Script_Edit, "PensionEmployer") . '" />';
								} elseif (isset($_REQUEST["PensionEmployer"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<input type="text" name="PensionEmployer" maxlength="3" class="form-control" id="PensionEmployer" value="' . $_REQUEST["PensionEmployer"] . '" />';
								} else {
									echo '<input type="text" name="PensionEmployer" maxlength="3" class="form-control" id="PensionEmployer" value="" />';
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Allowance %<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8" style=" padding-right: 35px">
								<span class="input-group-addon">
									<?php
									if ($EditID != "" && ScriptRunner($Script_Edit, "LeaveField") == 'on') {
										echo '<input name="Incluv" type="checkbox" id="Incluv" checked="checked" /><label for="Incluv" style="padding-left: 20px;height: 13px;"></label>';
									} else {
										echo '<input name="Incluv" type="checkbox" id="Incluv" /><label for="Incluv" style="padding-left: 20px;height: 13px;"></label>';
									}
									?>
								</span>
								<?php
								if ($EditID != "" || $EditID > 0) {
									echo '<input type="text" name="LeaveAllw" id="LeaveAllw"  class="form-control" value="' . ScriptRunner($Script_Edit, "LeaveAllw") . '" />';
								}
								// {echo '<input type="text" name="LeaveAllw" id="LeaveAllw" size="2" maxlength="2" class="form-control" value="'.ScriptRunner($Script_Edit,"LeaveAllw").'" />';}
								else {
									echo '<input type="text" name="LeaveAllw" id="LeaveAllw"  class="form-control" value="0.00" />';
								}
								// {echo '<input type="text" name="LeaveAllw" id="LeaveAllw" size="2" maxlength="2" class="form-control" value="0.00" />';}
								?>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" Title="Check if you want leave allowance paid monthly within the employee monthly salary" name="AuthInfo" id="AuthInfo2" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Allowance % Gross<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8" style=" padding-right: 35px">
								<span class="input-group-addon">
									<?php
									if ($EditID != "" && ScriptRunner($Script_Edit, "leavePerGross") == 'on') {
										echo '<input name="leavePerGross" type="checkbox" id="leavePerGross" checked="checked" /><label for="leavePerGross" style="padding-left: 20px;height: 13px;"></label>';
									} else {
										echo '<input name="leavePerGross" type="checkbox" id="leavePerGross" /><label for="leavePerGross" style="padding-left: 20px;height: 13px;"></label>';
									}
									?>
								</span>

							</div>
						</div>

						<!-- <div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Anniversary Bonus %<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8"  style=" padding-right: 35px">
								<span class="input-group-addon">
						            <?php
									if ($EditID != "" && ScriptRunner($Script_Edit, "AnniversaryField") == 'on') {
										echo '<input name="Anniversary" type="checkbox" id="Anniversary" checked="checked" /><label for="Anniversary" style="padding-left: 20px;height: 13px;"></label>';
									} else {
										echo '<input name="Anniversary" type="checkbox" id="Anniversary" /><label for="Anniversary" style="padding-left: 20px;height: 13px;"></label>';
									}
									?>
								</span>
									<?php
									if ($EditID != "" || $EditID > 0) {
										echo '<input type="text" name="AnniversaryAllw" id="AnniversaryAllw" size="2" maxlength="2" class="form-control" value="' . ScriptRunner($Script_Edit, "AnniversaryAllw") . '" />';
									} else {
										echo '<input type="text" name="AnniversaryAllw" id="AnniversaryAllw" size="2" maxlength="2" class="form-control" value="0.00" />';
									}
									?>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" Title="Check if you want anniversary bonus to be paid annually within the employee monthly salary" name="AuthInfo" id="AuthInfo2" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div> -->

						<br>
						<p class="text-danger"><strong>FLAT:</strong> [Initial amount where Total Taxable pay is less than]</p>
						<p class="text-danger"><strong>PENSION (%):</strong> [Calculated on Basic + Housing + Transport]</p>
						<p class="text-danger"><strong>LEAVE ALLOWANCE:</strong> Calculated as a (%) of the annual basic pay</p>
						<!-- <p class="text-danger" ><strong>ANNIVERSARY BONUS:</strong> Calculated as a (%) of the annual gross pay</p> -->
					</div>
				</div>
				<div class="row pull-right">
					<div class="col-md-12">
						<?php
						if (ValidateURths("DEDUCTION MASTERS" . "A") == true) {
							if ($EditID == "" || $EditID == '--') {
								echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Template" />';
							} elseif ($EditID != "" || $EditID > 0) {
								echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Template" />';
								echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
							} else {
								echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Template" />';
							}
						}
						?>
					</div>
				</div>
				<br>
				<hr style="margin-bottom: 1.0rem">
				<div class="row">
					<div class="col-md-12">
						<!-- ++++++++++++++++++++++++++++++ BUTTON +++++++++++++++++++++++++++++++-->
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11), AddedDate,106) as Dt, * from FinTax where (Status<>'A' and Status<>'D') order by PAYEName asc, UpdatedDate desc");
						} else {
							print "<h4>AUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11), AddedDate,106) as Dt, * from FinTax where Status='A'  order by PAYEName asc, UpdatedDate desc");
						}
						?>
					</div>
					<div class="col-md-12">
						<div class="ViewPatch">
							<table id="table" class="tablesorter table table-responsive">
								<thead>
									<tr>
										<th valign="middle" class="smallText" scope="col" data-sorter="false"><span class="TinyTextTight">
												<input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
												<label for="DelChk_All"></label>
											</span></th>
										<th valign="middle" scope="col" data-sorter="false"><span class="TinyTextTightBold">Updated</span></th>
										<th align="center" valign="middle" scope="col">PAYE Name</th>
										<th valign="middle" class="smallText" scope="col">Min Taxable</th>
										<th valign="middle" class="smallText" scope="col">Min Rate (%)</th>
										<th valign="middle" class="smallText" scope="col">Employee Pen</th>
										<th valign="middle" class="smallText" scope="col">Employer Pen</th>
										<th align="center" valign="middle" scope="col">Leave</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$Del = 0;
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										$Del = $Del + 1;
									?>
										<tr>
											<td valign="middle" scope="col" class="TinyTextTight">
												<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
												<label for="<?php echo ("DelBox" . $Del); ?>"></label>
											</td>
											<td valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
											<td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['PAYEName'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Limit0'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['pcent0'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['PensionEmployee'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['PensionEmployer'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['LeaveAllw'])); ?></td>
										</tr>
									<?php }
									include '../login/dbClose2.php'; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6 pt-10">
						<?php include '../main/pagination.php'; ?>
					</div>
					<div class="col-md-6 pt-10">
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-9">
								<?php
								echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" />
	 								      <input name="ActLnk" id="ActLnk" type="hidden" value="">
	 									  <input name="FAction" id="FAction" type="hidden" />
	 									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
								//      <input name="PgTy" id="PgTy" type="hidden" value="'.$_REQUEST["PgTy"].'" />

								//Check if any record was spolled at all before enabling buttons
								if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {

									if (ValidateURths("DEDUCTION MASTERS" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("DEDUCTION MASTERS" . "D");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}
									if (ValidateURths("DEDUCTION MASTERS" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("DEDUCTION MASTERS" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}


									if (ValidateURths("DEDUCTION MASTERS" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("DEDUCTION MASTERS" . "T");
										$but_Type = 'Authorize';
										include '../main/buttons.php';
									}
								} else {

									if (ValidateURths("DEDUCTION MASTERS" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("DEDUCTION MASTERS" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}
									if (ValidateURths("DEDUCTION MASTERS" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("DEDUCTION MASTERS" . "T");
										$but_Type = 'Unauthorize';
										include '../main/buttons.php';
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
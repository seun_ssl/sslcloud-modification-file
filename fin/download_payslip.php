<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

if (isset($_REQUEST["HID"]) && strlen($_REQUEST["HID"])) {
    $EditID = ECh($_REQUEST["HID"]);
    $EditID33 = ECh($_REQUEST["EmpIDHash"]);
    // var_dump($EditID);
    // var_dump($EditID33);
    $QueryStrCR = $QueryStrDR = $QueryStr = "";
} else {
    exit;
}
?>

<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <script src="../hrm/html2pdf/dist/html2pdf.bundle.js"></script>
    <div id="invoice">



        <?php

        $Script_ID = "SELECT (Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm, Em.EmpID as EmpID, Em.HashKey as EmpHKey, Em.email, Fp.HashKey,(DATENAME(month,Fp.PayMonth)+' '+DATENAME(year,Fp.PayMonth)) as PMth from EmpTbl Em, Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";
        // print_r($Script_ID);
        echo "<br>";
        $EmpHKey = ScriptRunner($Script_ID, "EmpHKey");

        $Script_core = "Select * from Fin_PRCore where EmpID='$EmpHKey' and Status='A' order by AuthDate desc";

        // print_r($Script_core);
        echo "<br>";

        $Scheme_PRSetting_HashKey = ScriptRunner($Script_core, "Scheme");

        //exit('stop here: '.$Scheme_PRSetting_HashKey);

        //    AuditLog("INSERT","Employee payslip resent to ".ScriptRunner($Script_ID,"Nm")." for ".ScriptRunner($Script_ID,"PMth"));

        $dbOpen2 = "SELECT Et.HashKey AS HKey, Et.SName + ' ' + Et.ONames + ' ' + Et.FName AS [Full Name], Et.HashKey, Et.Status, Et.EmpStatus,
Et.Email, Et.PENCommNo, Et.HInsurNo, Et.SalAcctNo, Et.SalBank, Et.Department, Et.SalSortCode, Et.TaxID, Et.PenCustID, Et.BranchID, FMc.PayItem1, FMc.PayItem2, FMc.Gross, (datename(M,FMc.PayMonth)+' '+datename(YYYY,FMc.PayMonth)) PMth,
FMc.PayItem3, FMc.PayItem4, FMc.PayItem5, FMc.PayItem6, FMc.PayItem7, FMc.PayItem8, FMc.PayItem9, FMc.PayItem10, FMc.PayItem11, FMc.PayItem12, FMc.PayItem13, FMc.PayItem14,
FMc.PayItem15, FMc.PayItem16, FMc.PayItem17, FMc.PayItem18, FMc.PayItem19, FMc.PayItem20, FMc.PayItem21, FMc.PayItem22, FMc.PayItem23, FMc.PayItem24,
FMc.PayItem25, FMs.PayItemNm1, FMs.PayItemOF1, FMs.PayItemTX1, FMs.PayItemCD1, FMs.PayItemPen1, FMs.PayItemNm2, FMs.PayItemOF2, FMs.PayItemTX2,
FMs.PayItemCD2, FMs.PayItemPen2, FMs.PayItemNm3, FMs.PayItemOF3, FMs.PayItemTX3, FMs.PayItemCD3, FMs.PayItemPen3, FMs.PayItemNm4,
FMs.PayItemOF4, FMs.PayItemTX4, FMs.PayItemCD4, FMs.PayItemPen4, FMs.PayItemNm5, FMs.PayItemOF5, FMs.PayItemTX5, FMs.PayItemCD5,
FMs.PayItemPen5, FMs.PayItemNm6, FMs.PayItemOF6, FMs.PayItemTX6, FMs.PayItemCD6, FMs.PayItemPen6, FMs.PayItemNm7, FMs.PayItemOF7,
FMs.PayItemTX7, FMs.PayItemCD7, FMs.PayItemPen7, FMs.PayItemNm8, FMs.PayItemOF8, FMs.PayItemTX8, FMs.PayItemCD8, FMs.PayItemPen8,
FMs.PayItemNm9, FMs.PayItemOF9, FMs.PayItemTX9, FMs.PayItemCD9, FMs.PayItemPen9, FMs.PayItemNm10, FMs.PayItemOF10, FMs.PayItemTX10,
FMs.PayItemCD10, FMs.PayItemPen10, FMs.PayItemNm11, FMs.PayItemOF11, FMs.PayItemTX11, FMs.PayItemCD11, FMs.PayItemPen11, FMs.PayItemNm12,
FMs.PayItemOF12, FMs.PayItemTX12, FMs.PayItemCD12, FMs.PayItemPen12, FMs.PayItemNm13, FMs.PayItemOF13, FMs.PayItemTX13, FMs.PayItemCD13,
FMs.PayItemPen13, FMs.PayItemNm14, FMs.PayItemOF14, FMs.PayItemTX14, FMs.PayItemCD14, FMs.PayItemPen14, FMs.PayItemNm15, FMs.PayItemOF15,
FMs.PayItemTX15, FMs.PayItemCD15, FMs.PayItemPen15, FMs.PayItemNm16, FMs.PayItemOF16, FMs.PayItemTX16, FMs.PayItemCD16, FMs.PayItemPen16,
FMs.PayItemNm17, FMs.PayItemOF17, FMs.PayItemTX17, FMs.PayItemCD17, FMs.PayItemPen17, FMs.PayItemNm18, FMs.PayItemOF18, FMs.PayItemTX18,
FMs.PayItemCD18, FMs.PayItemPen18, FMs.PayItemNm19, FMs.PayItemOF19, FMs.PayItemTX19, FMs.PayItemCD19, FMs.PayItemPen19, FMs.PayItemNm20,
FMs.PayItemOF20, FMs.PayItemTX20, FMs.PayItemCD20, FMs.PayItemPen20, FMs.PayItemNm21, FMs.PayItemOF21, FMs.PayItemTX21, FMs.PayItemCD21,
FMs.PayItemPen21, FMs.PayItemNm22, FMs.PayItemOF22, FMs.PayItemTX22, FMs.PayItemCD22, FMs.PayItemPen22, FMs.PayItemNm23, FMs.PayItemOF23,
FMs.PayItemTX23, FMs.PayItemCD23, FMs.PayItemPen23, FMs.PayItemNm24, FMs.PayItemOF24, FMs.PayItemTX24, FMs.PayItemCD24, FMs.PayItemPen24,
FMs.PayItemNm25, FMs.PayItemOF25, FMs.PayItemTX25, FMs.PayItemCD25, FMs.PayItemPen25, FMc.PensionEmployee, FMc.PensionEmployer,
FMc.LeaveAllowance, FMc.PAYE, FMc.TTaxable, FMc.NetPay, FMc.Scheme, FMc.AnniversaryBonus, FMc.LatenessAmount,
FMc.AbsentAmount,
FMc.loanAmount,
FMc.OverTimeAmount, FMs.CurrencyType,
FMs.ShowPaySlip1,
FMs.ShowPaySlip2, FMs.ShowPaySlip3, FMs.ShowPaySlip4, FMs.ShowPaySlip5,  FMs.ShowPaySlip6 ,
FMs.ShowPaySlip7, FMs.ShowPaySlip8, FMs.ShowPaySlip9, FMs.ShowPaySlip10, FMs.ShowPaySlip11,  FMs.ShowPaySlip12,
FMs.ShowPaySlip13, FMs.ShowPaySlip14, FMs.ShowPaySlip15, FMs.ShowPaySlip16, FMs.ShowPaySlip17,  FMs.ShowPaySlip18,
FMs.ShowPaySlip19, FMs.ShowPaySlip20, FMs.ShowPaySlip21, FMs.ShowPaySlip22, FMs.ShowPaySlip23, FMs.ShowPaySlip24,
FMs.ShowPaySlip25,
(Select OName from BrhMasters where HashKey=Et.BranchID) BranchName, (Select OAddress from BrhMasters where HashKey=Et.BranchID) BranchAddress FROM dbo.EmpTbl AS Et, dbo.Fin_PRIndvPay AS FMc, dbo.Fin_PRSettings AS FMs WHERE (Et.HashKey = FMc.EmpID AND Et.EmpStatus = 'Active' AND Et.Status IN ('A','U')) AND
((FMc.Status = 'A') AND FMc.HashKey='" . $EditID . "' AND FMs.HashKey='" . $Scheme_PRSetting_HashKey . "')";

        // print_r($dbOpen2);
        //b630dfa6b1e2d9c763a1736eb754f072

        include '../login/dbOpen2.php';

        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $BranchName = $row2["BranchName"];
            $BranchAddress = $row2['BranchAddress'];
            $EmployeeHKey = $row2['HKey'];

            $paymonth_yr = $row2['PMth'];

            $Script = "Select MMsg, MSub from MailTemp where HashKey='36cc94aa012f06f92914645ae3e072d9'"; //MSub='TimeOff Approved'";
            $PaySlip = ScriptRunner($Script, "MMsg");
            $Subj = ScriptRunner($Script, "MSub"); //"Payslip for the month of ".$row2['PMth'];
            $Subj = str_replace('#MonthPaid#', $row2['PMth'], $Subj);
            $total_fp = 0;
            $ty = 0;
            for ($i = 1; $i <= 25; $i++) {
                if ($row2['ShowPaySlip' . $i] == "1") {

                    if ($row2['PayItemCD' . $i] == "CR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItem' . $i] > 0) {
                        $QueryStrCR = $QueryStrCR . "<tr><td width='150' align='right' class='tdMenu_HeadBlock_Light'>" . strtoupper($row2["PayItemNm" . $i]) . ": </td><td>" . number_format($row2["PayItem" . $i], 2) . "</td></tr>";
                    }
                    if ($row2['PayItemCD' . $i] == "DR" && $row2['PayItemOF' . $i] == 1 && $row2['PayItem' . $i] > 0) {
                        $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . strtoupper($row2["PayItemNm" . $i]) . ": </td><td>" . number_format($row2["PayItem" . $i], 2) . "</td></tr>";
                    }
                    // temporary hack to be fix later
                    //****************************** */
                    if (isset($row2['loanAmount']) && $row2['loanAmount'] > 0 && $ty == 0) {
                        $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . 'LOAN REPAYMENT' . ": </td><td>" . number_format($row2['loanAmount'], 2) . "</td></tr>";
                        $ty = 1;
                    }
                    //****************************** */

                    if ($row2['PayItemCD' . $i] == "FP" && $row2['PayItemOF' . $i] == 1 && $row2['PayItem' . $i] > 0) {
                        $total_fp += $row2["PayItem" . $i];
                        $QueryStrFP = $QueryStrFP . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . strtoupper($row2["PayItemNm" . $i]) . ": </td><td>" . number_format($row2["PayItem" . $i], 2) . "</td></tr>";
                    }
                }
            }

            //include('PSlip_Prnt_inc.php');
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            /*
$Script="Select * from Masters where (ItemName='AllowancesReliefs' and ItemCode='".$EmployeeHKey."' and Status<>'D' and Val1<>'')";

if (trim(ScriptRunner($Script,"Val1")) != ""){

$dbOpen3 = ("SELECT * FROM Masters where (ItemName='AllowancesReliefs' and ItemCode='".$EmployeeHKey."' and Status<>'D' and Val1<>'') ORDER BY Val1");
include '../login/dbOpen3.php';

$QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Allowances & Reliefs</td></tr>";
$n=1;
while($row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH)){

$QueryStr=$QueryStr."<tr><td align='right' class='tdMenu_HeadBlock_Light'>".$row3['Val1']."</td><td>".number_format($row3['Desc'],2)."</td></tr>";
$n++;
}
}

 */

            $Script = "SELECT * FROM Fin_PRIndvPay WHERE HashKey = '" . $EditID . "'";

            $more_item_add_name1 = ScriptRunner($Script, "more_item_add_name1");
            $more_item_add_value1 = ScriptRunner($Script, "more_item_add_value1");
            $more_item_add_name2 = ScriptRunner($Script, "more_item_add_name2");
            $more_item_add_value2 = ScriptRunner($Script, "more_item_add_value2");
            $more_item_add_name3 = ScriptRunner($Script, "more_item_add_name3");
            $more_item_add_value3 = ScriptRunner($Script, "more_item_add_value3");
            $more_item_add_name4 = ScriptRunner($Script, "more_item_add_name4");
            $more_item_add_value4 = ScriptRunner($Script, "more_item_add_value4");
            $more_item_add_name5 = ScriptRunner($Script, "more_item_add_name5");
            $more_item_add_value5 = ScriptRunner($Script, "more_item_add_value5");

            $more_item_remove_name1 = ScriptRunner($Script, "more_item_remove_name1");
            $more_item_remove_value1 = ScriptRunner($Script, "more_item_remove_value1");
            $more_item_remove_name2 = ScriptRunner($Script, "more_item_remove_name2");
            $more_item_remove_value2 = ScriptRunner($Script, "more_item_remove_value2");
            $more_item_remove_name3 = ScriptRunner($Script, "more_item_remove_name3");
            $more_item_remove_value3 = ScriptRunner($Script, "more_item_remove_value3");
            $more_item_remove_name4 = ScriptRunner($Script, "more_item_remove_name4");
            $more_item_remove_value4 = ScriptRunner($Script, "more_item_remove_value4");
            $more_item_remove_name5 = ScriptRunner($Script, "more_item_remove_name5");
            $more_item_remove_value5 = ScriptRunner($Script, "more_item_remove_value5");

            //++++++++++++++++++++++++++++++ Individual specific Allowance, not tied to group +++++++++++++++++++++++++++++++
            if (($more_item_add_name1 != "") || ($more_item_add_name2 != "") || ($more_item_add_name3 != "") || ($more_item_add_name4 != "") || ($more_item_add_name5 != "")) {
                $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Allowances & Reliefs</td></tr>";
            }

            if ($more_item_add_name1 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name1 . "</td>
			<td>" . number_format($more_item_add_value1, 2) . "</td></tr>";
            }
            if ($more_item_add_name2 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name2 . "</td>
			<td>" . number_format($more_item_add_value2, 2) . "</td></tr>";
            }
            if ($more_item_add_name3 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name3 . "</td>
			<td>" . number_format($more_item_add_value3, 2) . "</td></tr>";
            }
            if ($more_item_add_name4 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name4 . "</td>
			<td>" . number_format($more_item_add_value4, 2) . "</td></tr>";
            }
            if ($more_item_add_name5 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name5 . "</td>
			<td>" . number_format($more_item_add_value5, 2) . "</td></tr>";
            }

            //++++++++++++++++++++++++++++++ Individual specific Deductions, not tied to group ++++++++++++++++++++++++++++++++++++

            if (($more_item_remove_name1 != "") || ($more_item_remove_name2 != "") || ($more_item_remove_name3 != "") || ($more_item_remove_name4 != "") || ($more_item_remove_name5 != "")) {
                $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Deductions</td></tr>";
            }

            if ($more_item_remove_name1 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name1 . "</td>
			<td>" . number_format($more_item_remove_value1, 2) . "</td></tr>";
            }
            if ($more_item_remove_name2 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name2 . "</td>
			<td>" . number_format($more_item_remove_value2, 2) . "</td></tr>";
            }
            if ($more_item_remove_name3 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name3 . "</td>
			<td>" . number_format($more_item_remove_value3, 2) . "</td></tr>";
            }
            if ($more_item_remove_name4 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name4 . "</td>
			<td>" . number_format($more_item_remove_value4, 2) . "</td></tr>";
            }
            if ($more_item_remove_name5 != "") {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name5 . "</td>
			<td>" . number_format($more_item_remove_value5, 2) . "</td></tr>";
            }

            //Extension for LOAN DISPLAY

            //$dbOpen3="SELECT * FROM LnIndvOff";
            //$dbOpen3=("SELECT count(*) Cnt, Convert(Varchar(11),ExpPayDate,106) as ExpPD, * from LnIndvOff  where EID='".$EmpHKey."' and Status <> 'D' Order by ExpPayDate ASC");

            // $dbOpen3 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD,
            //     (datename(M,ExpPayDate)+' '+datename(YYYY,ExpPayDate)) LnPayDate, * from LnIndvOff
            //     where EID='" . $EmpHKey . "' and Status = 'P' and Status <> 'D'
            //     and Month(ExpPayDate) = MONTH('" . $row2['PMth'] . "')
            //     and YEAR(ExpPayDate) = YEAR('" . $row2['PMth'] . "')
            //      Order by ExpPayDate ASC");

            // //if (ScriptRunner($dbOpen3,"Cnt") > 0){
            // //    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr></td></tr>";

            // include '../login/dbOpen3.php';
            // $row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH);
            // //while($row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH))        {

            // $ExpPay = $row3['ExpPay'];
            // $EID = $row3['EID'];
            // $LID = $row3['LID'];
            // $ExpPDate = $row3['ExpPD'];
            // $HashKey = $row3['HashKey'];
            // $LnPayDate = $row3['LnPayDate'];
            // //$LnPayDate="May 2017";

            // // date('M Y', strtotime(date('M Y')));

            // if (($LnPayDate == $paymonth_yr) && (($ExpPDate != "") || ($ExpPay != ""))) {

            //     $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>LOAN REPAYMENT</td></tr>";

            //     $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $ExpPDate . "</td>
            //                         <td>" . number_format($ExpPay, 2) . "</td></tr>";
            // }

            // include '../login/dbClose3.php';

            $total_loan = 0;
            $dbOpen3 = ("SELECT Convert(Varchar(11),AuthDate,106) as AuthPD,
		(datename(M,AuthDate)+' '+datename(YYYY,AuthDate)) LnAuthDate, * from LnDetails
		where EID='" . $EmpHKey . "'  and Status <> 'D'
		and Month(AuthDate) = MONTH('" . $row2['PMth'] . "')
		and YEAR(AuthDate) = YEAR('" . $row2['PMth'] . "')
        Order by AuthDate ASC");

            // var_dump( $row2['PMth']);

            include '../login/dbOpen3.php';
            //if (ScriptRunner($dbOpen3,"Cnt") > 0){
            //    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr></td></tr>";

            // $row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH);
            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                $total_loan += (float) $row3['LnAmt'];
                $loan_amt = $row3['LnAmt'];
                $AuthPD = ($row3['AuthDate'] == null) ? null : $row3['AuthDate']->format("F j Y");
                //$LnPayDate="May 2017";

                // date('M Y', strtotime(date('M Y')));

                $Script = "SELECT SetValue2 from Settings where Setting ='Payroll Approver'";
                $id_loan = ScriptRunner($Script, "SetValue2");

                if ($id_loan == '0') {
                    $total_loan = 0;
                }



                if (isset($loan_amt) &&  $id_loan == '1') {

                    $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>LOAN</td></tr>";

                    $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $AuthPD . "</td>
							<td>" . number_format($loan_amt, 2) . "</td></tr>";
                }
                //}
            }
            include '../login/dbClose3.php';

            /*$Script="Select * from Masters where (ItemName='Deductions' and ItemCode='".$EmployeeHKey."' and Status<>'D' and Val1<>'')";

    if (trim(ScriptRunner($Script,"Val1")) != ""){

    $dbOpen3 = ("SELECT * FROM Masters where (ItemName='Deductions' and ItemCode='".$EmployeeHKey."' and Status<>'D' and Val1<>'') ORDER BY Val1");
    include '../login/dbOpen3.php';

    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Deductions</td></tr>";
    $m=1;
    while($row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH)){
    $QueryStr=$QueryStr."<tr><td align='right' class='tdMenu_HeadBlock_Light'>".$row3['Val1']."</td><td>".number_format($row3['Desc'],2)."</td></tr>";
    $m++;
    }
    }
     */

            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr></td></tr>";
            $total_gross = $row2['Gross'] + $total_fp;
            $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock'>GROSS</td><td>" . number_format($total_gross, 2) . "</td></tr>";

            if ($row2['LeaveAllowance'] > 0) {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>LEAVE</td><td>" . number_format($row2['LeaveAllowance'], 2) . "</td></tr>";
            }

            // ANNIVERSARY BONUS
            if ($row2['AnniversaryBonus'] > 0) {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>ANNIVERSARY BONUS</td><td>" . number_format($row2['AnniversaryBonus'], 2) . "</td></tr>";
            }
            // OVERTIME BONUS
            if ($row2['OverTimeAmount'] > 0) {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>Overtime</td><td>" . number_format($row2['OverTimeAmount'], 2) . "</td></tr>";
            }

            // LATENESS BONUS
            if ($row2['LatenessAmount'] > 0) {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>Lateness</td><td>" . number_format($row2['LatenessAmount'], 2) . "</td></tr>";
            }

            // ABSENT
            if ($row2['AbsentAmount'] > 0) {
                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>Absent</td><td>" . number_format($row2['AbsentAmount'], 2) . "</td></tr>";
            }

            if ($row2['CurrencyType'] != "") {
                $script_curr_name = "SELECT Val1 FROM Masters WHERE HashKey='" . trim($row2['CurrencyType']) . "'";
                $curr_name = " (" . ScriptRunner($script_curr_name, "Val1") . ")";
            } else {
                $curr_name = " (Naira NGR)";
            }

            $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>PENSION</td><td>" . number_format($row2['PensionEmployee'], 2) . "</td></tr>";
            $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>PAYE</td><td>" . number_format($row2['PAYE'], 2) . "</td></tr>";
            $QueryStr = $QueryStr . "<tr><td height=1></td><td height=1><hr></td></tr>";
            $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock'><strong>NET PAY</strong></td><td><strong>" . number_format($row2['NetPay'] + (float)$total_loan, 2) .
                // $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock'><strong>NET PAY</strong></td><td><strong>" . number_format($row2['NetPay'], 2) .

                "$curr_name </strong></td></tr>";
            $QueryStr = $QueryStr . "<tr><td height=1></td><td height=1><hr></td></tr>";

            //$LeaveAllowance=$row2['LeaveAllowance'];
            //$PensionEmployer=$row2['PensionEmployer'];
            //if (floatval($LeaveAllowance)>0){$LeaveAllowance=$LeaveAllowance/12;} else{$LeaveAllowance=0;}
            //$TTaxable=$row2['TTaxable'];
            //if (floatval($TTaxable)>0){$TTaxable=$TTaxable/12;} else{$TTaxable=0;}
            $empdt = $row2['EmpDt'] ? date('F j, Y', strtotime($row2['EmpDt'])) : "N/A";
            $empcat = $row2['EmpCategory'] ? $row2['EmpCategory'] : "N/A";
            $PaySlip = str_replace('#MonthPaid#', $row2['PMth'], $PaySlip);
            $PaySlip = str_replace('#Name#', $row2['Full Name'], $PaySlip);
            $PaySlip = str_replace('#Idnumber#', $row2['EmpID'], $PaySlip);
            $PaySlip = str_replace('#category#', $empcat, $PaySlip);
            $PaySlip = str_replace('#Employmentdate#', $empdt, $PaySlip);

            if (strlen($QueryStrCR) > 3) {
                $QueryStrCR = "<tr><th class='tdMenu_HeadBlock' style='color:blue ; font-size:10px'   >Earnings</th></tr>" . $QueryStrCR;
            }
            if (strlen($QueryStrDR) > 3) {
                $QueryStrDR = "<tr><th  class='tdMenu_HeadBlock'  style='color:blue ; font-size:10px'>Deductions</th></tr>" . $QueryStrDR;
            }
            if (strlen($QueryStrFP) > 3) {
                $QueryStrFP = "<tr><th class='tdMenu_HeadBlock'  style='color:blue ; font-size:10px'>Others</th></tr>" . $QueryStrFP;
            }
            $PaySlip = str_replace('#Values#', ('<TABLE width="385">' . $QueryStrCR . $QueryStrDR . $QueryStrFP . $QueryStr . '</TABLE>'), $PaySlip);

            break;
        }
        include '../login/dbClose2.php';

        echo '<link href="../css/style_main.css" rel="stylesheet" type="text/css">';

        $Script = "Select MMsg from MailTemp where HashKey='HEADER'";
        $MailHeader = ScriptRunner($Script, "MMsg") . "<font color='#333333' face='Verdana, Arial, Helvetica, sans-serif' size='2'>";
        //Format MailHeader to have full details
        $Script = "Select SetValue, SetValue2, SetValue3 from settings where Setting='CompLogo'";
        $CompLogo = '<img src="../pfimg/' . $_SESSION["StkTck" . "CustID"] . "/" . ScriptRunner($Script, "SetValue") . '" width="' . ScriptRunner($Script, "SetValue2") . '" height="' . ScriptRunner($Script, "SetValue3") . '">';

        $Script = "Select SetValue from settings where Setting='CompName'";
        $CompName = ScriptRunner($Script, "SetValue");

        $Script = "Select SetValue from settings where Setting='CompAddress'";
        $CompAddress = ScriptRunner($Script, "SetValue");

        $Script = "Select SetValue from settings where Setting='CompTel'";
        $CompTel = ScriptRunner($Script, "SetValue");

        $Script = "Select SetValue from settings where Setting='CompEmail'";
        $CompEmail = ScriptRunner($Script, "SetValue");

        $Script = "Select SetValue from settings where Setting='CompTel'";
        $CompTel = ScriptRunner($Script, "SetValue");

        $Script = "Select SetValue from settings where Setting='CompRC'";
        $CompRC = ScriptRunner($Script, "SetValue");

        //Make changes required with values retrived and passed
        $MailHeader = str_replace('#CompLogo#', $CompLogo, $MailHeader);
        $MailHeader = str_replace('#CompName#', $CompName, $MailHeader);
        $MailHeader = str_replace('#BranchName#', $BranchName, $MailHeader);

        if (isset($BranchAddress)) {
            $MailHeader = str_replace('#CompAddress#', $BranchAddress, $MailHeader);
        } else {
            $MailHeader = str_replace('#CompAddress#', $CompAddress, $MailHeader);
        }
        $MailHeader = str_replace('#CompTel#', $CompTel, $MailHeader);
        $MailHeader = str_replace('#CompEmail#', $CompEmail, $MailHeader);
        $MailHeader = str_replace('#CompRC#', $CompRC, $MailHeader);

        //$_SESSION["StkTck"."MailHeader"] = $MailHeader; //Set In SeSSION HEADER
        $Script = "Select MMsg from MailTemp where HashKey='FOOTER'";
        //$_SESSION["StkTck"."MailFooter"] = "</font>".ScriptRunner($Script,"MMsg");
        $MailFooter = "</font>" . ScriptRunner($Script, "MMsg");

        echo '<table width=100%><tr><td class="TinyTextTight">';
        echo $MailHeader . $PaySlip . $MailFooter;
        echo '</td></tr></table>';
        ?>

    </div>
    <div class="d-flex justify-content-end">

        <button type="button" class="btn btn-primary my-3 btn-sm " onclick="callDownload()">Download</button>
    </div>

    <script type="text/javascript">
        function callDownload() {
            const invoice = document.getElementById("invoice");

            html2pdf().from(invoice).save(`Payslip-<?= $paymonth_yr ?>`);

        }
    </script>
</body>
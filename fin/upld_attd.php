<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('Africa/Lagos');
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
// $Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';
if (ValidateURths("ATTENDANCE" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$GoValidate = true;
?>
<script>
    $(function() {
        //================================ REPORT DATES ==============================================
        $("#S_RptDate").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-60Y",
            maxDate: "+1Y",
            dateFormat: 'dd M yy'
        })
        $("#E_RptDate").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-60Y",
            maxDate: "+1Y",
            dateFormat: 'dd M yy'
        })
    });
</script>
<?php
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
if (!isset($EditID)) {
    $EditID = "";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($Del)) {
    $Del = 0;
}
if (!isset($Err)) {
    $Err = "";
}
if (!isset($ModMaster)) {
    $ModMaster = "";
}
if (!isset($ext)) {
    $ext = "";
}
if (!isset($SelID)) {
    $SelID = "";
}
if (!isset($RetVal)) {
    $RetVal = "";
} //Document Management return value for attached error screen shot.

$BasicPay = 0;
$dbOpen2 = "";
$TAmt = 0;
$TAmtLate = 0;
$PrefixID = $EmpID = $RecDate = $TimeIn = $TimeOut = $LateIn = $EarlyOut = $Normal = $OverTime = $Remark = ""; //Clear all variables
$_SESSION["file_size_dsp"] = "";
$_SESSION["file_name_dsp"] = "";

if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Add" && isset($_REQUEST["Doc"]) && $_REQUEST["Doc"] == "New") {
    echo '<html><head><title></title>';
    if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
        echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
    } else {
?>
        <link href="../css/style_main.css" rel="stylesheet" type="text/css">
    <?php }
    echo '</head>';
    ?>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <!-- Print function Lawrence -->
<?php
    echo '
                <body oncontextmenu="return false;">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6 text-center" style="margin-top: 70px">
                                    <form name="Upd" method="post" action="#" enctype="multipart/form-data">

            ';
    if (ValidateURths("ATTENDANCE" . "A") == true) {
        echo '
                    <p> Select a document file to upload</p>
                    <div class="form-group  row"  style="margin-bottom: 5px">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="box bg-danger" style="margin-bottom: 0px" >
                                <div class="box-body text-center" style="padding: 0.7rem">
                                    <div class="col-sm-12 input-group-sm">
                                        <input type="file" name="dmsupd" id="dmsupd" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                ';
    }
    echo '<div class="row"><div class="col-md-12 text-center">';
    if (ValidateURths("ATTENDANCE" . "A") == true) {
        echo '
                    <input type="submit" name="SubmitDoc" id="SubmitDoc" value="Upload" class="btn btn-danger btn-sm">
                ';
    }
    echo '
                <input type="submit" name="SubmitDoc" id="SubmitDoc" value="View Existing" class="btn btn-danger btn-sm">
                <input name="PgTy" id="PgTy" type="hidden" value="Loaded" />
                <input name="Doc" id="Doc" type="hidden" value="Loaded" />
                <input name="SearchType" id="SearchType" type="hidden" value="Unauthorized" />
                <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
                </div>
                </div>
                </form>
                <form name="Exp" id="Exp" action="../main/dwn_btchfl.php" method="post">
            ';
    if (ValidateURths("ATTENDANCE" . "A") == true) {
        echo '
                    <p style="margin-top: 1rem">Only CSV comma delimiter files allowed. Max file individual upload size is: 20Mb
                    </p>
                    <a href="../Sample Attendance Sheet format.csv">View sample upload file</a>
                ';
    }
    //"../public/sample_download.csv"
    $strExp = "************************************************************************************************************" . chr(13) . chr(10) . "ALL RECORDS MUST APPEAR AFTER THE BELOW LINE." . chr(13) . chr(10) . "************************************************************************************************************" . chr(13) . chr(10);
    //$strExp .="Prefix,Employee ID,Record Date,Time-In,Time-Out,Late-In,Early-Out,Normal,OverTime".chr(13).chr(10);

    $strExp .= "EmpID,RecDate,TimeIn,TimeOut,LateIn,EarlyOut,Normal,OverTime,Remark,Status,AddedeBy,AddedDate,HashKey,RecType" . chr(13) . chr(10);
    //$strExp .="6edcbaad1fbaa8eed32cdcefd199c3e3,23 May 2016,8:24:06,16:59:51,,,,,,A,6edcbaad1fbaa8eed32cdcefd199c3e3,00:00.0,6edcbaad1fbaa8eed32cdcefd199cdef,O";
    $strExp .= ",,10 May 2015,8:00,17:00,0,0,0,0";
    echo '
                <input type="hidden" value="" id="ActLnk" name="ActLnk">
                <input type="hidden" value="' . $strExp . '" id="ExpVal" name="ExpVal">
                <input type="hidden" value="Employee Attendance Upload Sample" id="RptName" name="RptName">
                <input type="hidden" value="csv" id="DType" name="DType">
            ';
    $strExp = "";
    echo '
                </form>
                </div>
                <div class="col-md-3"></div>
                </div>
                </div></div>
                </body></html>
            ';
    exit;
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    if (isset($_REQUEST["SubmitDoc"]) && $_REQUEST["SubmitDoc"] == "Upload" && isset($_REQUEST["Doc"]) && $_REQUEST["Doc"] == "Loaded") {
        if (ValidateURths("ATTENDANCE" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        /*
        These work for me, for 100MB uploads, lasting 2 hours:
        In apache-virtual-host:
        -----------------------------------------------------------
        <Directory /var/www/MyProgram>
        AllowOverride Options
        </Directory>
        -----------------------------------------------------------

        In .htaccess:
        -----------------------------------------------------------
        php_value session.gc_maxlifetime 10800
        php_value max_input_time         10800
        php_value max_execution_time     10800
        php_value upload_max_filesize    110M
        php_value post_max_size          120M
        -----------------------------------------------------------
         */
        if (isset($_FILES['dmsupd']['name'])) {
            $ext = findexts($_FILES['dmsupd']['name']);
        }

        if ($ext != "csv") {
            echo ("<script type='text/javascript'>{ parent.msgbox('Invalid file upload type. Please upload a valid document.', 'red'); }</script>");
            //Redirect to Upload Page
            exit;
        }
        //*****************
        $_SESSION["file_ext"] = $ext;
        $_SESSION["file_name_dsp"] = $_FILES["dmsupd"]["name"];
        $_SESSION["file_size"] = ($_FILES["dmsupd"]["size"]);
        $_SESSION["file_size_dsp"] = ($_FILES["dmsupd"]["size"] / 1024);
        $_SESSION["file_hash"] = md5($_FILES["dmsupd"]["name"]);

        if ($_SESSION["file_size_dsp"] > 1024) {
            $_SESSION["file_size_dsp"] = number_format(($_SESSION["file_size_dsp"] / 1024), 2, '.', ',');
            $_SESSION["file_size_dsp"] = $_SESSION["file_size_dsp"] . "Mb";
        } else {
            $_SESSION["file_size_dsp"] = number_format($_SESSION["file_size_dsp"], 2, '.', ',') . "Kb";
        }
        //Move file to application temp folder
        //Value: 0; There is no error, the file uploaded with success.
        //Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
        //Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
        //Value: 3; The uploaded file was only partially uploaded.
        //Value: 4; No file was uploaded.
        //No 5
        //Value: 6; Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.
        //Value: 7; Failed to write file to disk. Introduced in PHP 5.1.0.
        //Vaue: 8; A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded
        //extensions with phpinfo() may help. Introduced in PHP 5.2.0.
        if ($_FILES["dmsupd"]["error"] == 1 || $_FILES["dmsupd"]["error"] == 2) {
            header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File size exceeds maximum allowed<br>contact upload file.</b></font>");
            exit;
        } elseif ($_FILES["dmsupd"]["error"] == 3) {
            header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File upload failed.<br>Please try again.</b></font>");
            exit;
        } elseif ($_FILES["dmsupd"]["error"] > 3) {
            header("Location:m_validate_msg.php?WebBrwValue=<font color=red size=1 face=Arial><b><br>Error:<br>File upload failed<br></b></font>");
            exit;
        } else {
            // move_uploaded_file($_FILES["file"]["tmp_name"], "../players/m_songs/" . $kk . "." . $Ext[1]);
            $CntLn = 0;
            $Script = "Select * from Settings where Setting='AutoEmpID'";
            $IDType = ScriptRunner($Script, "SetValue2");
            $file = fopen($_FILES["dmsupd"]["tmp_name"], "r") or exit("Unable to open file!");
            //Output a line of the file until the end is reached
            while (!feof($file)) {
                //exit('stop here');
                $CntLn = $CntLn + 1; // Count what line we are on and jump first two lines of record. Begin posting from third
                //echo $CntLn;
                $Sender = str_replace('"', '', fgets($file)); // replace double quote with nothing. ASCII - 34 = "
                $Sender = str_replace("'", "", $Sender); // replace single quote with nothing. ASCII - 34 = "
                // if ($CntLn >0) // && isset($LnStr[7]))
                if ($CntLn > 0) {
                    //echo "here";
                    $GoValidate = true;
                    //Strip the values. Include routine to check if record already exist
                    $LnStr = explode(",", $Sender);
                    // var_dump($LnStr);
                    // Lawrence changed 3 to 4
                    if (isset($LnStr[4])) {
                        //if (strlen(trim($LnStr[0])) <1) {/*$GoValidate=false; */}//No Know Prefix entered
                        //else {$PrefixID = trim($LnStr[0]);} //Pass Value to $EmpID
                        //Ben, I noticed that the false must have been activated here
                        //0 is now the EmpID, no more prefix
                        if (strlen(trim($LnStr[0])) < 1) {
                            $GoValidate = false; //Invalid Employee Number
                        } else {
                            //0 is now the EmpID, no more prefix
                            $EmpID = trim($LnStr[0]);
                            if ($IDType == "Long") {
                                if (strlen($EmpID) == 1) {
                                    $EmpID = "000" . $EmpID;
                                } elseif (strlen($EmpID) == 2) {
                                    $EmpID = "00" . $EmpID;
                                } elseif (strlen($EmpID) == 3) {
                                    $EmpID = "0" . $EmpID;
                                }
                            }
                            $EmpID = $PrefixID . $EmpID;
                            //echo $EmpID;
                        }
                        //Pass Value to $EmpID
                        //if (strlen(trim($LnStr[1])) != 11) {
                        //    $GoValidate=false;
                        //}//Invalid Date Field Format
                        //else {
                        //exit('Ben: here it turns to falase, why?'. $GoValidate);
                        $RecDate = trim($LnStr[1]);
                        //exit('Ben: here it turns to falase, why?'. $RecDate);
                        //} //Pass Value to $RecDate
                        if (strlen(trim($LnStr[2])) < 3) {
                            //Invalid Time Field Format
                            $TimeIn = '12:00:00';
                        } else {
                            //Pass Value to $TimeIn
                            $TimeIn = trim($LnStr[2]);
                        }
                        if (strlen(trim($LnStr[3])) < 3) {
                            //Invalid Time Field Format
                            $TimeOut = '12:00:00';
                        } else {
                            //Pass Value to $TimeIn
                            $TimeOut = trim($LnStr[3]);
                        }
                        $RecType = "O";
                        // Exemption condition for Employess
                        $Exempt = 0;
                        if (strtolower(trim($LnStr[4])) == "yes") {
                            $Exempt = 1;
                        }
                        if (strtolower(trim($LnStr[4])) == "no") {
                            $Exempt = 0;
                        }
                        if (strtolower(trim($LnStr[4])) == "absent") {
                            $Exempt = 0;
                            $RecType = "A";
                        }
                        /*
                        if (!is_numeric(trim($LnStr[4]))) {$LateIn=0;}//Invalid numeric value
                        else {$LateIn = trim($LnStr[4]);} //Pass Value to $TimeIn

                        if (!is_numeric(trim($LnStr[5]))) {$EarlyOut=0;}//Invalid numeric value
                        else {$EarlyOut = trim($LnStr[5]);} //Pass Value to $TimeIn

                        if (!is_numeric(trim($LnStr[6]))) {$Normal=0;}//Invalid numeric value
                        else {$Normal = trim($LnStr[6]);} //Pass Value to $TimeIn

                        if (!is_numeric(trim($LnStr[7]))) {$OverTime=0;}//Invalid numeric value
                        else {$OverTime = trim($LnStr[7]);} //Pass Value to $TimeIn
                         */
                        if (strtolower(date("l", strtotime($RecDate))) == "saturday" || strtolower(date("l", strtotime($RecDate))) == "sunday") {
                            $RecType = "W";
                        }
                        $Remark = "";
                        // echo $EmpID;
                        // exit;
                        if ($GoValidate == true) {
                            //exit('Ben: here it turns to falase, why?'. $EmpID);
                            $Script = "Select HashKey from EmpTbl where EmpID='" . trim(ECh($EmpID)) . "' and EmpStatus='Active' and Status in ('A','U')"; //Check if employee exist else don't upload
                            $EmpID = ScriptRunner($Script, "HashKey");
                            //exit('Ben: here it turns to falase, why?'. $EmpID);
                            if (strlen(trim($EmpID)) != 32) {
                                //Employee does not exist
                                $EmpID = "";
                            } else {
                                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . $CntLn . "Attendance" . $_SESSION["StkTck" . "UName"];
                                $HashKey = md5($UniqueKey);
                                $Script = "Select Count(HashKey) Ct from [Fin_Attend] where EmpID='" . $EmpID . "' and RecDate='" . $RecDate . "' and Status<>'D'";
                                if (ScriptRunner($Script, "Ct") == 0) {
                                    // exit('stop here2');
                                    //$Script="INSERT into [Fin_Attend] ([EmpID],[RecDate],[TimeIn],[TimeOut],[LateIn],[EarlyOut],[Normal],[OverTime],[Remark],[RecType],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('".$EmpID."','".ECh($RecDate)."','".ECh($TimeIn)."','".ECh($TimeOut)."',".ECh($LateIn).",".ECh($EarlyOut).",".ECh($Normal).",".ECh($OverTime).",'".ECh($Remark)."','".$RecType."','N','".$_SESSION["StkTck"."HKey"]."',GetDate(),'".$HashKey."')";
                                    $Script = "INSERT into [Fin_Attend] ([EmpID],[RecDate],[TimeIn],[TimeOut],[RecType],[Status],[AddedBy],[AddedDate],[HashKey],[OverTimeAmount],[LatenessAmount], [Exempt]) VALUES ('" . $EmpID . "','" . ECh($RecDate) . "','" . ECh($TimeIn) . "','" . ECh($TimeOut) . "','" . $RecType . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "', '0.00', '0.00', '" . $Exempt . "')";
                                    //echo $Script;
                                    ScriptRunnerUD($Script, "Create Records"); //Records inserted. Pending autorization
                                }
                                //End Insert
                            }
                        }
                    }
                }
            }
            fclose($file);
            $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.AddedBy='" . ECh($_SESSION["StkTck" . "HKey"]) . "' and FA.AddedDate between(Convert(varchar(11),GetDate(),106)+' 00:00:00') and (Convert(varchar(11),GetDate(),106)+' 23:59:59')  and FA.Status='N'";
        }
    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        // var_dump("here");
        // die();
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //echo $EditID;
                $Script_Edit = "Select * from Fin_Attend where HashKey='" . $EditID . "'";
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("ATTENDANCE" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                if ($_REQUEST['PayCalcType'] === 'Overtime') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'A', [o_status] = 'A'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    $Script_m = "UPDATE [shm_allocate_shifts]
                    SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'A'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script_m, "Unauthorize");
                } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'A', [l_status] = 'A'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                }

                AuditLog("AUTHORIZE", "Overtime/weekend data athorized successfully.");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected overtime/weekend data authorized successfully.', 'green'); }</script>");
            }
        }
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Exempt Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {

        if (ValidateURths("ATTENDANCE" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                if ($_REQUEST['PayCalcType'] === 'Overtime') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Exempt]=1
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Exempt]= 0,
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                }

                AuditLog("Exemption", "Overtime/weekend data exempted successfully.");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected overtime/weekend data exempted successfully.', 'green'); }</script>");
            }
        }
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "UnExempt Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {

        if (ValidateURths("ATTENDANCE" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                if ($_REQUEST['PayCalcType'] === 'Overtime') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Exempt]=0
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {

                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Exempt]= 0,
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                }

                AuditLog("Unexemption", "Overtime/weekend data unexempted successfully.");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected overtime/weekend data unexempted successfully.', 'green'); }</script>");
            }
        }
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("ATTENDANCE" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                if ($_REQUEST['PayCalcType'] === 'Overtime') {
                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'U', [o_status] = 'U'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Unauthorize");

                    $Script_m = "UPDATE [shm_allocate_shifts]
                    SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'U'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script_m, "Unauthorize");
                } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {
                    $Script = "UPDATE [Fin_Attend]
                    SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "',[AuthDate] = GetDate(),[Status] = 'U', [l_status] = 'U'
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Unauthorize");
                }

                AuditLog("UNAUTHORIZE", "Overtime data athorized successfully.");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected overtime data unauthorized successfully.', 'green'); }</script>");
            }
        }
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("ATTENDANCE" . "D") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script = "SELECT [Status] FROM [Fin_Attend] WHERE HashKey ='" . $EditID . "'";
                if (ScriptRunner($Script, "Status") == 'A') {
                    echo ("<script type='text/javascript'>{ parent.msgbox('Cannot delete authorized records.', 'red'); }</script>");
                } else {
                    if ($_REQUEST['PayCalcType'] === 'Overtime') {

                        $Script = "UPDATE [Fin_Attend]
                        SET Status='D', [o_status] = 'D',DeletedBy='" . $_SESSION["StkTck" . "HKey"] . "',DeletedDate= GetDate()
                        where HashKey='" . $EditID . "'";
                        ScriptRunnerUD($Script, "Del");

                        $Script_m = "UPDATE [shm_allocate_shifts]
                        SET Status='D',DeletedBy='" . $_SESSION["StkTck" . "HKey"] . "',DeletedDate= GetDate()
                        where HashKey='" . $EditID . "'";
                        ScriptRunnerUD($Script_m, "Del");
                    } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {

                        $Script = "UPDATE [Fin_Attend]
                        SET Status='D', [l_status] = 'D',DeletedBy='" . $_SESSION["StkTck" . "HKey"] . "',DeletedDate= GetDate()
                        where HashKey='" . $EditID . "'";
                        ScriptRunnerUD($Script, "Del");
                    }

                    AuditLog("DELETE", "Overtime records deleted successfully");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected overtime records deleted successfully.', 'green'); }</script>");
                }
            }
        }
    }
}
?>
<script>
    $(function() {
        $("#DocRem").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "0D",
            maxDate: "+20Y",
            dateFormat: 'dd-mm-yy'
        })
        $("#DocDelDate").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "0D",
            maxDate: "+20Y",
            dateFormat: 'dd-mm-yy'
        })
    });
</script>
<?php
if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else {
?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css">
<?php }
?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;">
    <form action="#" method="post" name="Records" target="_self" id="Records">
        <div class="box">
            <div class="box-header with-border tdMenu_HeadBlock pt-2 pb-2">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p style="color: #000; margin-bottom: 4px;">
                            Attendance Management
                        </p>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- <div class="col-md-6"></div> -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" style="padding-right: 0px;">Type <span style="color: red">*</span>:</label>
                                    <div class="col-sm-9 input-group input-group-sm" style=" padding-right: 2px">
                                        <select name="PayCalcType" class="form-control" id="PayCalcType">
                                            <option value='Overtime' <?php echo (isset($_REQUEST['PayCalcType']) && $_REQUEST['PayCalcType'] === 'Overtime' ? 'selected' : '') ?>>Overtime</option>
                                            <option value='Lateness' <?php echo (isset($_REQUEST['PayCalcType']) && $_REQUEST['PayCalcType'] === 'Lateness' ? 'selected' : '') ?>>Lateness</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <div class="col-sm-12 input-group input-group-sm" style=" padding-right: 0px; padding-left: 0px;">
                                        <select name="SearchType" class="form-control" id="SearchType">
                                            <?php
                                            if ($_REQUEST['SearchType'] == "Unauthorized") {
                                                echo '
												<option value="Unauthorized" selected=selected">Unauthorized</option>
												<option value="Authorized">Authorized</option>
												';
                                            } elseif ($_REQUEST['SearchType'] == "Authorized") {
                                                echo '
												<option value="Unauthorized">Unauthorized</option>
												<option value="Authorized" selected=selected">Authorized</option>
												';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <div class="col-sm-12 input-group input-group-sm" style=" padding-right: 0px; padding-left: 0px;">
                                        <label class="col-4">From:</label>
                                        <?php
                                        if (isset($_REQUEST["S_RptDate"])) {
                                            echo '<input placeholder="Start Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
                                        } else {
                                            echo '<input placeholder="Start Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <div class="col-sm-12 input-group input-group-sm" style=" padding-right: 0px; padding-left: 0px;">
                                        <label class="col-4">To:</label>
                                        <?php
                                        if (isset($_REQUEST["E_RptDate"])) {
                                            echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control" value="' . ($_REQUEST["E_RptDate"]) . '" readonly/>';
                                        } else {
                                            echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control" value="" readonly/>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <!-- include '../rpts/rpt_header.php';  -->
                                <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Load" />
                            </div>
                        </div>
                    </div>
                </div>
                <?php //$Load_Date=false; $Load_Search=true; $Load_Auth=true; include '../main/report_opt.php'; 
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="ViewPatch">
                            <table id="table" class="tablesorter table-responsive table">
                                <?php
                                if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {
                                    $PayMthDt = " and RecDate between '" . ECh($_REQUEST["S_RptDate"]) . "' and '" . ECh($_REQUEST["E_RptDate"]) . "' ";
                                } else {
                                    $PayMthDt = '';
                                }
                                //$dbOpen2="Select Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),AddedDate,106) AD, Convert(varchar(5),TimeIn,114) TI, Convert(varchar(5),TimeOut,114) TO , * from Fin_Attend where AddedBy='".$_SESSION["StkTck"."UName"]."' and AddedDate='Convert(varchar(11),GetDate(),106)' and Status='N'";
                                $Del = 0;
                                if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized" && isset($PayMthDt) && $PayMthDt != '') {

                                    if ($_REQUEST['PayCalcType'] === 'Overtime') {
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
									Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.*
									from Fin_Attend FA inner join EmpTbl Et on FA.EmpID = Et.HashKey where FA.EmpID=Et.HashKey and ( [o_status] ='A' )" . "$PayMthDt";
                                    } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
									Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.*
									from Fin_Attend FA inner join EmpTbl Et on FA.EmpID = Et.HashKey where FA.EmpID=Et.HashKey and ( [l_status] ='A' )" . "$PayMthDt";
                                    }

                                    /*
$dbOpen2="Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.*
from Fin_Attend FA, EmpTbl Et, Fin_AttendSet FAS where FA.EmpID=Et.HashKey AND FAS.PayCalcType='Overtime' AND FAS.Status='A' AND FA.Status in ('A')";
 */
                                } elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized" && isset($PayMthDt) && $PayMthDt != '') {
                                    /*
    if($_REQUEST['PayCalcType']=="Lateness"){
    $type = "Lateness";
    }else{
    $type = "Overtime";
    }
     */
                                    if ($_REQUEST['PayCalcType'] === 'Overtime') {
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
                                    Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.*
                                    from Fin_Attend FA inner join EmpTbl Et on FA.EmpID = Et.HashKey where FA.EmpID=Et.HashKey and (FA.[o_status] is null or  [o_status] ='U' ) " . "$PayMthDt";
                                    } elseif ($_REQUEST['PayCalcType'] === 'Lateness') {
                                        $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD,
                                    Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.*
                                    from Fin_Attend FA inner join EmpTbl Et on FA.EmpID = Et.HashKey where FA.EmpID=Et.HashKey and  ( FA.[l_status] is null or  [l_status] ='U'  ) " . "$PayMthDt";
                                    }

                                    // print_r($dbOpen2);

                                    // var_dump();
                                }
                                if ($dbOpen2 == "") {
                                    $dbOpen2 = "Select (Et.SName+' '+Et.FName+' ['+Convert(varchar(24),Et.EmpID)+']') Nm, Et.HashKey HKey, Department, Convert(varchar(11),RecDate,106) RD, Convert(varchar(11),FA.AddedDate,106) AD, Convert(varchar(5),TimeIn,114) TIn, Convert(varchar(5),TimeOut,114) TOut, RecType, FA.* from Fin_Attend FA, EmpTbl Et where FA.EmpID=Et.HashKey and FA.AddedBy='" . ECh($_SESSION["StkTck" . "HKey"]) . "' and FA.AddedDate between(Convert(varchar(11),GetDate(),106)+' 00:00:00') and (Convert(varchar(11),GetDate(),106)+' 23:59:59')  and FA.Status='zzz'" . "$PayMthDt";
                                }
                                include '../login/dbOpen2.php';
                                /*
if((isset($_REQUEST['PayCalcType']))&&($_REQUEST['PayCalcType']=="Lateness")){
$type = "Lateness";
$Script_atdnset="Select Convert(varchar(5),CloseTime,114) RsmT, * from Fin_AttendSet where PayCalcType='Lateness' AND Status = 'A'";
$amt_unit = ScriptRunner($Script_atdnset,"OverTime");
//$resume_time = ScriptRunner($Script_atdnset,"CloseTime");
$resume_time = ScriptRunner($Script_atdnset,"RsmT");
}else{
$type = "Overtime";
$Script_atdnset="Select Convert(varchar(5),CloseTime,114) ClsT, * from Fin_AttendSet where PayCalcType='Overtime' AND Status = 'A' AND CloseTime <> ''";
$amt_unit = ScriptRunner($Script_atdnset,"OverTime");
//$close_time = ScriptRunner($Script_atdnset,"CloseTime");
$close_time = ScriptRunner($Script_atdnset,"ClsT");
}
 */
                                $Script_atdnset = "Select Convert(varchar(5),CloseTime,114) ClsT, Convert(varchar(5),TimeIn,114) RsmT, * from Fin_AttendSet where  Status = 'A'";
                                $amt_unit = ScriptRunner($Script_atdnset, "OverTime"); //amount set for this
                                $ActiveTime = ScriptRunner($Script_atdnset, "ActiveTime");
                                $close_time = ScriptRunner($Script_atdnset, "ClsT");
                                $resume_time = ScriptRunner($Script_atdnset, "RsmT");
                                //if(isset($resume_time)){
                                //if((isset($_REQUEST['PayCalcType']))&&($_REQUEST['PayCalcType']=="Lateness")){
                                if ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] == "Lateness")) {



                                    echo '
                                        <thead>
                                            <tr >
                                                <th data-sorter="false"><input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label>
                                                </th>
                                                <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                                                <th>Date</th>
                                                <th>Full Name</th>
                                                <th>Department</th>
                                                <th>Level</th>
                                                <th data-sorter="false">Resumption</th>
                                                <th data-sorter="false">Time-In</th>
                                                <th data-sorter="false">Time-Out</th>
                                                <th>Diffrence</th>
                                                <th>Amount incur</th>
                                                <th>Type</th>
                                                <th>Exemption</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    ';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        // if ($row2['RecType'] !== 'A') {
                                        // return ($row2['$rwfield']);
                                        //if(isset($close_time)&&(strtotime($close_time)) > strtotime($row2['TOut'])){
                                        //print "$Del) " . $resume_time ." < ".$row2['TIn'] ."<br/>";
                                        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        //  if(isset($resume_time)&&(strtotime($resume_time)) < strtotime($row2['TIn'])){

                                        $Script_atd = "Select  count(*) Ct from Fin_AttendSet where ActiveTime='TimeIn' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";

                                        $Script_atdnset = "Select Convert(varchar(5),CloseTime,114) ClsT, Convert(varchar(5),TimeIn,114) RsmT, * from Fin_AttendSet where ActiveTime='TimeIn' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";

                                        $amt_unit = ScriptRunner($Script_atdnset, "OverTime"); //amount set for this
                                        $ActiveTime = ScriptRunner($Script_atdnset, "ActiveTime");
                                        $close_time = ScriptRunner($Script_atdnset, "ClsT");
                                        $resume_time = ScriptRunner($Script_atdnset, "RsmT");

                                        $Del = $Del + 1;
                                        if (ScriptRunner($Script_atd, "Ct") > 0 && isset($resume_time)) : ?>
                                            <tr>
                                                <td><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
                                                <!--<td width="110" align="left" valign="middle" scope="col"><?php echo (trim($row2['AD'])); ?></td> //-->
                                                <td><?php echo (trim($row2['RD'])); ?></td>
                                                <td><?php echo trim($row2['Nm']); ?></td>
                                                <td><?php echo $row2['Department']; ?></td>
                                                <td>
                                                    <?php
                                                    $Script_set = "select * from Fin_AttendSet where ActiveTime='TimeIn' AND EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and Status in('A')";
                                                    $Fin_AttendSet_HashKey = ScriptRunner($Script_set, "HashKey");
                                                    $Script_ID = "select SetValue, SetValue2 from Settings where Setting='JobRoleLevel' and SetValue=(Select EID from Fin_AttendSet where [HashKey]='" . $Fin_AttendSet_HashKey . "')";
                                                    $level_name = ScriptRunner($Script_ID, "SetValue2");
                                                    print $level_name;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $resume_time; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo "<font color=\"#FF0000\"><b>" . $row2['TIn'] . "</b></font>";
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo "<font color=\"#FF0000\"><b>" . $row2['TOut'] . "</b></font>";
                                                    ?>
                                                </td>
                                                <td>

                                                    <?php
                                                    //Calculate lateness
                                                    $resumption_time = new DateTime($resume_time);
                                                    $time_in = new DateTime($row2['TIn']);
                                                    if ($time_in > $resumption_time) {
                                                        $time_diff = $resumption_time->diff(new DateTime($row2['TIn']));
                                                        if ($time_diff->h > 0) {
                                                            $time_diff = ($time_diff->h * 60) + $time_diff->i;
                                                            echo $time_diff . "mins";
                                                        } elseif ($time_diff->i > 0) {
                                                            // isset($resume_time)&&(strtotime($resume_time)) < strtotime($row2['TIn'])
                                                            $time_diff = $time_diff->i;
                                                            echo $time_diff . 'mins';
                                                        } else {
                                                            $time_diff = 0;
                                                            echo $time_diff . 'mins';
                                                        }
                                                    } else {
                                                        $time_diff = 0;
                                                        echo $time_diff . 'mins';
                                                    }
                                                    // var_dump($resumption_time);
                                                    // var_dump($row2['TIn']);
                                                    // $time_diff =$time_in->diff($resumption_time);
                                                    // var_dump($time_diff);

                                                    // if(isset($resume_time)&&(strtotime($resume_time)) < strtotime($row2['TIn']))
                                                    ?>
                                                </td>
                                                <td>

                                                    <?php
                                                    $type_arr = [];

                                                    $dbOpen3 = "select * from Fin_AttendSet where Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime='TimeIn'";

                                                    include '../login/dbOpen3.php';
                                                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                                        // var_dump($row3['c_type']);
                                                        if (isset($row3['c_type'])) {
                                                            $type_arr[] = $row3['c_type'];
                                                        }
                                                    }

                                                    include '../login/Close3.php';

                                                    if (in_array('workdays', $type_arr)) {
                                                        $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeIn'  and [c_type] = 'workdays'  ";
                                                        $ctty = 'workdays';
                                                    }

                                                    if (in_array('weekends', $type_arr)) {
                                                        // check if that day is a weekend
                                                        $weekend_arry = [];
                                                        $dbOpen3 = ("SELECT * from Masters where ItemName='Weekends' ");
                                                        include '../login/dbOpen3.php';
                                                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                                            $weekend_arry[] = strtolower($row3['Val1']);
                                                        }

                                                        include '../login/Close3.php';
                                                        $day_words = strtolower(date("l", strtotime(trim($row2['RD']))));
                                                        if (in_array($day_words, $weekend_arry)) {

                                                            $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeIn'  and [c_type] = 'weekends'  ";

                                                            $ctty = 'weekends';
                                                        }
                                                    }

                                                    if (in_array('events', $type_arr)) {
                                                        // check if that day is an events

                                                        $dbOpen3 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * from LvPHolsDetails where Status='A' and [HType] ='Event' and [OffOn] = 1 order by [SDate] desc, AuthDate desc");
                                                        include '../login/dbOpen3.php';
                                                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                                                            $paymentDate = date('Y-m-d', strtotime(trim($row2['RD'])));
                                                            //echo $paymentDate; // echos today!
                                                            $contractDateBegin = date('Y-m-d', strtotime(trim($row3['SDt'])));
                                                            $contractDateEnd = date('Y-m-d', strtotime(trim($row3['EDt'])));

                                                            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
                                                                $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeIn'  and [c_type] = 'events'  ";
                                                                $ctty = 'events';
                                                            }
                                                        }
                                                        include '../login/Close3.php';
                                                    }

                                                    if (in_array('holidays', $type_arr)) {
                                                        // check if that day is an holiday
                                                        $dbOpen3 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * from LvPHolsDetails where Status='A' and [HType] ='Holiday' and [OffOn] = 1 order by [SDate] desc, AuthDate desc");
                                                        include '../login/dbOpen3.php';
                                                        while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                                                            $paymentDate = date('Y-m-d', strtotime(trim($row2['RD'])));
                                                            //echo $paymentDate; // echos today!
                                                            $contractDateBegin = date('Y-m-d', strtotime(trim($row3['SDt'])));
                                                            $contractDateEnd = date('Y-m-d', strtotime(trim($row3['EDt'])));

                                                            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
                                                                $Script = "select * from Fin_AttendSet where  Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime ='TimeIn' and [c_type] = 'holidays'  ";

                                                                $ctty = 'holidays';
                                                            }
                                                        }
                                                        include '../login/Close3.php';
                                                    }

                                                    // $Script = "select * from Fin_AttendSet where Status in('A') and EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='" . $row2['HKey'] . "' order by ID desc) and ActiveTime='TimeIn'";
                                                    if (ScriptRunner($Script, "UseType") == "Percent") {
                                                        $Script_expt = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                                                        $exempt = ScriptRunner($Script_expt, "Exempt");
                                                        if ($exempt == '1') {
                                                            $amount_incurr_pay = 0;
                                                            echo number_format($amount_incurr_pay, 2);
                                                        } else {
                                                            $Amt = floatval(ScriptRunner($Script, "PCent")); // Gets the % of basic to be paid
                                                            $BasicPay = ScriptRunner("Select PayItem1 from Fin_PRCore where EmpID='" . $row2['HKey'] . "' and Status='A'", "PayItem1");
                                                            if ($BasicPay > 0) {
                                                                $BasicPay = $BasicPay / 12;
                                                            }
                                                            $minutes_pay = $BasicPay * ($Amt / 100);
                                                            $amount_incurr_pay = $minutes_pay * $time_diff;

                                                            if ($amount_incurr_pay < 0) {
                                                                $amount_incurr_pay = 0;
                                                            }
                                                            echo number_format($amount_incurr_pay, 2);
                                                        }

                                                        $Script = "UPDATE [Fin_Attend] SET [LatenessAmount] = CAST($amount_incurr_pay as decimal) WHERE [Status] NOT IN ('B','D') AND [HashKey] = '" . $row2['HashKey'] . "' ";
                                                        ScriptRunnerUD($Script, "Inst");

                                                        $TAmtLate = $TAmtLate + $amount_incurr_pay;
                                                    } elseif (ScriptRunner($Script, "UseType") == "Value") {

                                                        $Script_exmp = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                                                        $exempt = ScriptRunner($Script_exmp, "Exempt");
                                                        if ($exempt == '1') {
                                                            $amount_incurr_pay = 0;
                                                            echo number_format($amount_incurr_pay, 2);
                                                        } else {
                                                            $minutes_Amount = ScriptRunner($Script, "OverTime");
                                                            $amount_incurr_pay = $minutes_Amount * $time_diff;
                                                            echo number_format($amount_incurr_pay, 2);
                                                        }
                                                        //if($amount_incurr_pay <0){
                                                        //$amount_incurr_pay=0;
                                                        //}
                                                        //echo  number_format($amount_incurr_pay,2);
                                                        $Script = "UPDATE [Fin_Attend] SET [LatenessAmount] = CAST($amount_incurr_pay as decimal) WHERE [Status] NOT IN ('B','D') AND [HashKey] = '" . $row2['HashKey'] . "' ";
                                                        ScriptRunnerUD($Script, "Inst");
                                                        $TAmtLate = $TAmtLate + ($amount_incurr_pay);
                                                    } else {
                                                    }
                                                    ?>
                                                </td>
                                                <!--  <td width="108" align="right" valign="middle" scope="col"> -->
                                                <?php
                                                /*
        $Script="select * from Fin_AttendSet where EID=(select top(1) ItemLevel from EmpDtl where ItemCode='EMP' and StaffID='".$row2['HKey']."' order by ItemStart desc)";

        if (ScriptRunner($Script,"UseType")=="Percent")
        {
        $Amt = floatval(ScriptRunner($Script,"PCent")); // Gets the % of basic to be paid

        $BasicPay = ScriptRunner("Select PayItem1 from Fin_PRCore where EmpID='".$row2['HKey']."' and Status='A'","PayItem1");
        if ($BasicPay>0) {$BasicPay = $BasicPay/12;}

        $HourlyPay = ($BasicPay * $Amt) / 100;
        $OvPay = $HourlyPay * $row2['OverTime'];
        if($OvPay <0){
        $OvPay=0;
        }
        echo number_format($OvPay,2);
        $TAmt = $TAmt + $OvPay;
        }
        elseif (ScriptRunner($Script,"UseType")=="Value")
        {
        $Overtime_Hourly_Amount = ScriptRunner($Script_set,"OverTime");
        $amount_over_pay=$Overtime_Hourly_Amount * $overtime;
        if($amount_over_pay <0){
        $amount_over_pay=0;
        }
        echo  number_format($amount_over_pay,2);
        $TAmt = $TAmt + ($amount_over_pay);

        }
        else
        {}
         */
                                                ?>
                                                <!-- </td>-->
                                                <td>
                                                    <?php
                                                    if (isset($resume_time) && ($amount_incurr_pay == 0)) {
                                                        echo $ctty;
                                                    } elseif (isset($resume_time) && ($amount_incurr_pay > 0)) {
                                                        echo $ctty;
                                                    } else {

                                                        echo 'Overtime';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    // Exemption tab
                                                    $Script = "SELECT Exempt FROM Fin_Attend WHERE [HashKey]='" . $row2['HashKey'] . "'";
                                                    $exempt = ScriptRunner($Script, "Exempt");
                                                    if ($exempt == '1') {
                                                        echo 'YES';
                                                    } else {
                                                        echo 'NO';
                                                    }

                                                    ?>
                                                </td>
                                            </tr>
                                <?php endif;
                                        // }
                                        //include('Upld_Attd_inc.php');
                                        // }
                                    } //close while
                                }
                                include 'Upld_Attd_inc.php';
                                include '../login/dbClose2.php';
                                ?>
                                </tbody>
                                <?php
                                if ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] == "Overtime")) {
                                    print '
                                        <tfoot>
                                            <tr>
                                                <td data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
                                                <label for="DelChk_All"></label>
                                                </td>
                                                <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <tdstyle="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <th data-sorter="false">&nbsp;</th>
                                                <th data-sorter="false">&nbsp;</th>
                                                <th data-sorter="false">&nbsp;</th>
                                                <th data-sorter="false">&nbsp;</th>
                                                <td style="background: #EAEAEA; font-weight: bolder;"><span class="pull-right">' . number_format($TAmt, 2) . '</span></td>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    ';
                                } elseif ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] == "Lateness")) {
                                    print '
								        <tfoot>
								            <tr>
                                                <td style="background: #EAEAEA;" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
                                                <label for="DelChk_All"></label>
                                                </td>
                                                <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <tdstyle="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
                                                <td style="background: #EAEAEA; font-weight: bolder;" data-sorter="false"><span class="pull-right">' . number_format($TAmtLate, 2) . '</span></td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
								            </tr>
								        </tfoot>
                                    ';
                                } elseif ((isset($_REQUEST['PayCalcType'])) && ($_REQUEST['PayCalcType'] == "Absent")) {
                                    print '
								        <tfoot>
								            <tr>
                                                <td style="background: #EAEAEA;" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
                                                <label for="DelChk_All"></label>
                                                </td>
                                                <!--<th width="110" align="center" valign="middle" scope="col">Uploaded</th> //-->
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <tdstyle="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA;" >&nbsp;</td>
                                                <td style="background: #EAEAEA; font-weight: bolder;" data-sorter="false"><span class="pull-right">' . number_format($TAmt, 2) . '</span></td>
                                                <td style="background: #EAEAEA;" data-sorter="false">&nbsp;</td>
								            </tr>
								        </tfoot>
                                    ';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6 pt-5">
                        <?php include '../main/pagination.php'; ?>
                    </div>
                    <div class="col-md-6 pt-5">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <?php
                                echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
								<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
								<input name="FAction" id="FAction" type="hidden">';
                                //Check if any record was spolled at all before enabling buttons
                                if (ValidateURths("ATTENDANCE" . "D") == true) {
                                    //Check if user has Delete rights
                                    $but_HasRth = ("ATTENDANCE" . "D");
                                    $but_Type = 'Delete';
                                    include '../main/buttons.php';
                                }
                                if (ValidateURths("ATTENDANCE" . "A") == true) {
                                    //Check if user has Add/Edit rights
                                    $but_HasRth = ("ATTENDANCE**" . "A");
                                    $but_Type = 'View';
                                    include '../main/buttons.php';
                                }
                                //Check if user has Authorization rights
                                if (ValidateURths("ATTENDANCE" . "T") == true) {
                                    if ($_REQUEST['SearchType'] == "Unauthorized") {
                                        $but_HasRth = ("ATTENDANCE" . "T");
                                        $but_Type = 'Authorize';
                                        include '../main/buttons.php';

                                        $but_Type = 'Exempt';
                                        include '../main/buttons.php';

                                        $but_Type = 'Unexempt';
                                        include '../main/buttons.php';
                                    } else {
                                        $but_HasRth = ("ATTENDANCE" . "T");
                                        $but_Type = 'Unauthorize';
                                        include '../main/buttons.php';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        function printData() {
            var divToPrint = document.getElementById("table");
            newWin = window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        }
        $('button').on('click', function() {
            printData();
        })
    </script>
</body>
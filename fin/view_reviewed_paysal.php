<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
// require_once '../dompdf/dompdf_config.inc.php';
require_once('../mpdf/vendor/autoload.php');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("REVIEW SALARY" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
$CmdTyp = "";
$PaySlip = "";
$QueryStrCR = "";
$QueryStrDR = "";
$QueryStr = "";
$Net = $Pension = $PAYE = $Housing = $Transport = $Basic = 0;

$BrhID = "'A'";
if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
    $BrhID = "'" . ECh($_REQUEST["BranchID"]) . "'";
} elseif (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'all') {
    $BrhID = ECh($_REQUEST["BranchID"]);
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("AUTHORIZE SALARY" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        $i = 0;
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                //________________________>
                $Script_ID = "SELECT Em.HashKey HKey, (Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm, Em.EmpID as EmpID, Em.HashKey as EmpHKey, Em.Email, Em.Sec_Email, Fp.HashKey from EmpTbl Em, Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";
                //$EditID = ScriptRunner($Script_ID, "HashKey");
                $EmpID_ = ScriptRunner($Script_ID, "HKey");
                $EmpHKey = ScriptRunner($Script_ID, "EmpHKey");

                $Script_core = "Select * from Fin_PRCore where EmpID='$EmpHKey' and Status<>'D'";
                $Scheme_PRSetting_HashKey = ScriptRunner($Script_core, "Scheme");
                //________________________>

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [Fin_PRIndvPay] WHERE [HashKey]='" . $EditID . "'") == true) {
                    //Check if account is customer account and if SMS alert is set to fire
                    $Script = "UPDATE [Fin_PRIndvPay]
					SET [Status] = 'A'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    //AuditLog("AUTHORIZE","Employee payment authorized for ".ScriptRunner($Script_ID,"Nm"));
                    $Script = "Select Convert(varchar(11),PayMonth,106) PayMth from Fin_PRIndvPay WHERE [HashKey]='" . $EditID . "'";
                    $PayMonth = ScriptRunner($Script, "PayMth");

                    /*
                    $Script_Loan="Select HashKey from LnIndvOff where EID='".$EmpID_."' and Status='B' and
                    (Month(ExpPayDate) = MONTH('".$PayMonth."') and
                    YEAR(ExpPayDate) = YEAR('".$PayMonth."'))";
                     */
                    $Script_Loan = "Select HashKey from LnIndvOff where EID='" . $EmpID_ . "' and Status='B'";

                    $LoanHKey = ScriptRunner($Script_Loan, "HashKey");

                    if (strlen($LoanHKey) == 32) {
                        include 'loan_deduction_mgr.php';
                    }

                    if ((isset($_REQUEST['send_slip'])) && ($_REQUEST['send_slip'] == "1")) {

                        /*
                        if (strlen($LoanHKey) == 32)
                        {
                        $Script = "UPDATE [LnIndvOff]
                        SET [Status] = 'P'
                        ,[PaidBy] = '".$_SESSION["StkTck"."HKey"]."'
                        ,[PaidDate] = GetDate()
                        WHERE [HashKey]='".$LoanHKey."'";
                        ScriptRunnerUD ($Script, "Authorize");

                        //Get values for parent loan table
                        $Script="Select (Ld.RePayAmt - (Select Sum(Li.Paid) from LnIndvOff Li where Li.Status='P' and Ld.HashKey=Li.LID and Ld.EID='".$EmpID_."' and Ld.EID=Li.EID)) OutStd,
                        (Select Sum(Li.Paid) from LnIndvOff Li where Li.Status='P' and Ld.HashKey=Li.LID and Ld.EID='".$EmpID_."' and Ld.EID=Li.EID) PaidAmt from LnDetails Ld where Ld.status='A' and Ld.HashKey=(Select Top 1 LID from LnIndvOff where HashKey='".$LoanHKey."')";
                        $OutAmt = ScriptRunner ($Script, "OutStd");
                        $PaidAmt = ScriptRunner ($Script, "PaidAmt");

                        //echo $Script;

                        if ($OutAmt >= -1 && $OutAmt <= 1)
                        {
                        $Script="Update LnDetails set
                        OutSPay=".$OutAmt.",
                        Paid=".$PaidAmt.",
                        Status='P'
                        where HashKey=(Select Top 1 LID from LnIndvOff where HashKey='".$LoanHKey."')";
                        }
                        else
                        {
                        $Script="Update LnDetails set
                        OutSPay=".$OutAmt.",
                        Paid=".$PaidAmt."
                        where HashKey=(Select Top 1 LID from LnIndvOff where HashKey='".$LoanHKey."')";
                        }
                        ScriptRunnerUD ($Script, "Authorize");
                        }
                         */

                        $dbOpen2 = "SELECT Et.HashKey AS HKey, Et.SName + ' ' + Et.ONames + ' ' + Et.FName AS [Full Name], Et.HashKey, Et.Status, Et.EmpStatus,Et.EmpID,Et.EmpCategory,EmpDt,
                    Et.Email, Et.PENCommNo, Et.HInsurNo, Et.SalAcctNo, Et.SalBank, Et.Department, Et.SalSortCode, Et.TaxID, Et.PenCustID, Et.BranchID, FMc.PayItem1, FMc.PayItem2, FMc.Gross, (datename(M,FMc.PayMonth)+' '+datename(YYYY,FMc.PayMonth)) PMth,
                    FMc.PayItem3, FMc.PayItem4, FMc.PayItem5, FMc.PayItem6, FMc.PayItem7, FMc.PayItem8, FMc.PayItem9, FMc.PayItem10, FMc.PayItem11, FMc.PayItem12, FMc.PayItem13, FMc.PayItem14,
                    FMc.PayItem15, FMc.PayItem16, FMc.PayItem17, FMc.PayItem18, FMc.PayItem19, FMc.PayItem20, FMc.PayItem21, FMc.PayItem22, FMc.PayItem23, FMc.PayItem24,
                    FMc.PayItem25, FMs.PayItemNm1, FMs.PayItemOF1, FMs.PayItemTX1, FMs.PayItemCD1, FMs.PayItemPen1, FMs.PayItemNm2, FMs.PayItemOF2, FMs.PayItemTX2,
                    FMs.PayItemCD2, FMs.PayItemPen2, FMs.PayItemNm3, FMs.PayItemOF3, FMs.PayItemTX3, FMs.PayItemCD3, FMs.PayItemPen3, FMs.PayItemNm4,
                    FMs.PayItemOF4, FMs.PayItemTX4, FMs.PayItemCD4, FMs.PayItemPen4, FMs.PayItemNm5, FMs.PayItemOF5, FMs.PayItemTX5, FMs.PayItemCD5,
                    FMs.PayItemPen5, FMs.PayItemNm6, FMs.PayItemOF6, FMs.PayItemTX6, FMs.PayItemCD6, FMs.PayItemPen6, FMs.PayItemNm7, FMs.PayItemOF7,
                    FMs.PayItemTX7, FMs.PayItemCD7, FMs.PayItemPen7, FMs.PayItemNm8, FMs.PayItemOF8, FMs.PayItemTX8, FMs.PayItemCD8, FMs.PayItemPen8,
                    FMs.PayItemNm9, FMs.PayItemOF9, FMs.PayItemTX9, FMs.PayItemCD9, FMs.PayItemPen9, FMs.PayItemNm10, FMs.PayItemOF10, FMs.PayItemTX10,
                    FMs.PayItemCD10, FMs.PayItemPen10, FMs.PayItemNm11, FMs.PayItemOF11, FMs.PayItemTX11, FMs.PayItemCD11, FMs.PayItemPen11, FMs.PayItemNm12,
                    FMs.PayItemOF12, FMs.PayItemTX12, FMs.PayItemCD12, FMs.PayItemPen12, FMs.PayItemNm13, FMs.PayItemOF13, FMs.PayItemTX13, FMs.PayItemCD13,
                    FMs.PayItemPen13, FMs.PayItemNm14, FMs.PayItemOF14, FMs.PayItemTX14, FMs.PayItemCD14, FMs.PayItemPen14, FMs.PayItemNm15, FMs.PayItemOF15,
                    FMs.PayItemTX15, FMs.PayItemCD15, FMs.PayItemPen15, FMs.PayItemNm16, FMs.PayItemOF16, FMs.PayItemTX16, FMs.PayItemCD16, FMs.PayItemPen16,
                    FMs.PayItemNm17, FMs.PayItemOF17, FMs.PayItemTX17, FMs.PayItemCD17, FMs.PayItemPen17, FMs.PayItemNm18, FMs.PayItemOF18, FMs.PayItemTX18,
                    FMs.PayItemCD18, FMs.PayItemPen18, FMs.PayItemNm19, FMs.PayItemOF19, FMs.PayItemTX19, FMs.PayItemCD19, FMs.PayItemPen19, FMs.PayItemNm20,
                    FMs.PayItemOF20, FMs.PayItemTX20, FMs.PayItemCD20, FMs.PayItemPen20, FMs.PayItemNm21, FMs.PayItemOF21, FMs.PayItemTX21, FMs.PayItemCD21,
                    FMs.PayItemPen21, FMs.PayItemNm22, FMs.PayItemOF22, FMs.PayItemTX22, FMs.PayItemCD22, FMs.PayItemPen22, FMs.PayItemNm23, FMs.PayItemOF23,
                    FMs.PayItemTX23, FMs.PayItemCD23, FMs.PayItemPen23, FMs.PayItemNm24, FMs.PayItemOF24, FMs.PayItemTX24, FMs.PayItemCD24, FMs.PayItemPen24,
                    FMs.PayItemNm25, FMs.PayItemOF25, FMs.PayItemTX25, FMs.PayItemCD25, FMs.PayItemPen25, FMc.PensionEmployee, FMc.PensionEmployer,
                    FMc.LeaveAllowance, FMc.PAYE, FMc.TTaxable, FMc.NetPay, FMc.Scheme,
                    FMc.AnniversaryBonus, FMc.LatenessAmount, FMc.AbsentAmount,
                    FMc.OverTimeAmount,
                    FMc.loanAmount,
                    FMs.ShowPaySlip1,
                    FMs.ShowPaySlip2, FMs.ShowPaySlip3, FMs.ShowPaySlip4, FMs.ShowPaySlip5,  FMs.ShowPaySlip6 ,
                    FMs.ShowPaySlip7, FMs.ShowPaySlip8, FMs.ShowPaySlip9, FMs.ShowPaySlip10, FMs.ShowPaySlip11,  FMs.ShowPaySlip12,
                    FMs.ShowPaySlip13, FMs.ShowPaySlip14, FMs.ShowPaySlip15, FMs.ShowPaySlip16, FMs.ShowPaySlip17,  FMs.ShowPaySlip18,
                    FMs.ShowPaySlip19, FMs.ShowPaySlip20, FMs.ShowPaySlip21, FMs.ShowPaySlip22, FMs.ShowPaySlip23, FMs.ShowPaySlip24,
                    FMs.ShowPaySlip25,
                    (Select OName from BrhMasters where HashKey=Et.BranchID) BranchName
                    FROM dbo.EmpTbl AS Et,
                    dbo.Fin_PRIndvPay AS FMc,
                    dbo.Fin_PRSettings AS FMs
                    WHERE
                    Et.HashKey = FMc.EmpID AND
                    Et.EmpStatus = 'Active' AND
                    Et.Status IN ('A','U') AND
                    (FMc.Status = 'A') AND FMc.HashKey='" . $EditID . "' AND FMs.HashKey='" . $Scheme_PRSetting_HashKey . "'";

                        include '../login/dbOpen2.php';
                        $total_ded = 0;
                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                            $paymonth_yr = $row2['PMth'];

                            $Script = "Select MMsg, MSub from MailTemp where HashKey='36cc94aa012f06f92914645ae3e072d9'"; //MSub='TimeOff Approved'";
                            $PaySlip = ScriptRunner($Script, "MMsg");
                            $Subj = ScriptRunner($Script, "MSub"); //"Payslip for the month of ".$row2['PMth'];
                            $Subj = str_replace('#MonthPaid#', $row2['PMth'], $Subj);
                            $QueryStr = "";
                            $QueryStrDR = "";
                            $QueryStrCR = "";
                            $QueryStrRL  = "";

                            $total_fp = 0;
                            $ty = 0;
                            for ($io = 1; $io <= 25; $io++) {
                                if ($row2['ShowPaySlip' . $io] == "1") {
                                    if ($row2['PayItemCD' . $io] == "CR" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                        $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px' ><td width='150' align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px' >" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                                    }

                                    if ($row2['PayItemCD' . $io] == "DR" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                        $total_ded = $total_ded + $row2["PayItem" . $io];
                                        $QueryStrDR = $QueryStrDR . "<tr  style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px' >" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                                    }
                                    if ($row2['PayItemCD' . $io] == "RL" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                        $QueryStrRL = $QueryStrRL . "<tr  style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px' >" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                                    }
                                    // temporary hack to be fix later
                                    //****************************** */
                                    if (isset($row2['loanAmount']) && $row2['loanAmount'] > 0 && $ty == 0) {
                                        $total_ded = $total_ded + $row2['loanAmount'];
                                        $QueryStrDR = $QueryStrDR . "<tr style='font-size:11px'  ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >" . 'LOAN REPAYMENT' . ": </td><td style='font-size:11px'>" . number_format($row2['loanAmount'], 2) . "</td></tr>";
                                        $ty = 1;
                                    }
                                    //****************************** */

                                    if ($row2['PayItemCD' . $io] == "FP" && $row2['PayItemOF' . $io] == 1 && $row2['PayItem' . $io] > 0) {
                                        $total_fp += $row2["PayItem" . $io];
                                        $QueryStrFP = $QueryStrFP . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >" . strtoupper($row2["PayItemNm" . $io]) . ": </td><td style='font-size:11px' >" . number_format($row2["PayItem" . $io], 2) . "</td></tr>";
                                    }
                                }
                            }

                            //+++++++++++++++++++++++++ ENTENDED FUNCTION ++++++++++++++++++++++++++++++++++++
                            $Script = "SELECT * FROM Fin_PRIndvPay WHERE HashKey = '" . $EditID . "'";

                            $more_item_add_name1 = ScriptRunner($Script, "more_item_add_name1");
                            $more_item_add_value1 = ScriptRunner($Script, "more_item_add_value1");
                            $more_item_add_name2 = ScriptRunner($Script, "more_item_add_name2");
                            $more_item_add_value2 = ScriptRunner($Script, "more_item_add_value2");
                            $more_item_add_name3 = ScriptRunner($Script, "more_item_add_name3");
                            $more_item_add_value3 = ScriptRunner($Script, "more_item_add_value3");
                            $more_item_add_name4 = ScriptRunner($Script, "more_item_add_name4");
                            $more_item_add_value4 = ScriptRunner($Script, "more_item_add_value4");
                            $more_item_add_name5 = ScriptRunner($Script, "more_item_add_name5");
                            $more_item_add_value5 = ScriptRunner($Script, "more_item_add_value5");

                            $more_item_remove_name1 = ScriptRunner($Script, "more_item_remove_name1");
                            $more_item_remove_value1 = ScriptRunner($Script, "more_item_remove_value1");
                            $more_item_remove_name2 = ScriptRunner($Script, "more_item_remove_name2");
                            $more_item_remove_value2 = ScriptRunner($Script, "more_item_remove_value2");
                            $more_item_remove_name3 = ScriptRunner($Script, "more_item_remove_name3");
                            $more_item_remove_value3 = ScriptRunner($Script, "more_item_remove_value3");
                            $more_item_remove_name4 = ScriptRunner($Script, "more_item_remove_name4");
                            $more_item_remove_value4 = ScriptRunner($Script, "more_item_remove_value4");
                            $more_item_remove_name5 = ScriptRunner($Script, "more_item_remove_name5");
                            $more_item_remove_value5 = ScriptRunner($Script, "more_item_remove_value5");

                            //++++++++++++++++++++++++++++++ Individual specific Allowance, not tied to group +++++++++++++++++++++++++++++++
                            if (($more_item_add_name1 != "") || ($more_item_add_name2 != "") || ($more_item_add_name3 != "") || ($more_item_add_name4 != "") || ($more_item_add_name5 != "")) {
                                $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Allowances & Reliefs</td></tr>";
                            }

                            if ($more_item_add_name1 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name1 . "</td>
			<td>" . number_format($more_item_add_value1, 2) . "</td></tr>";
                            }
                            if ($more_item_add_name2 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name2 . "</td>
			<td>" . number_format($more_item_add_value2, 2) . "</td></tr>";
                            }
                            if ($more_item_add_name3 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name3 . "</td>
			<td>" . number_format($more_item_add_value3, 2) . "</td></tr>";
                            }
                            if ($more_item_add_name4 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name4 . "</td>
			<td>" . number_format($more_item_add_value4, 2) . "</td></tr>";
                            }
                            if ($more_item_add_name5 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_add_name5 . "</td>
			<td>" . number_format($more_item_add_value5, 2) . "</td></tr>";
                            }

                            //++++++++++++++++++++++++++++++ Individual specific Deductions, not tied to group ++++++++++++++++++++++++++++++++++++

                            if (($more_item_remove_name1 != "") || ($more_item_remove_name2 != "") || ($more_item_remove_name3 != "") || ($more_item_remove_name4 != "") || ($more_item_remove_name5 != "")) {
                                $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Deductions</td></tr>";
                            }

                            if ($more_item_remove_name1 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name1 . "</td>
			<td>" . number_format($more_item_remove_value1, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name2 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name2 . "</td>
			<td>" . number_format($more_item_remove_value2, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name3 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name3 . "</td>
			<td>" . number_format($more_item_remove_value3, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name4 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name4 . "</td>
			<td>" . number_format($more_item_remove_value4, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name5 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name5 . "</td>
			<td>" . number_format($more_item_remove_value5, 2) . "</td></tr>";
                            }

                            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                            //++++++++++++++++++++++++++++++ Individual specific Deductions, not tied to group ++++++++++++++++++++++++++++++++++++

                            if (($more_item_remove_name1 != "") || ($more_item_remove_name2 != "") || ($more_item_remove_name3 != "") || ($more_item_remove_name4 != "") || ($more_item_remove_name5 != "")) {
                                $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Employee specific monthly Deductions</td></tr>";
                            }

                            if ($more_item_remove_name1 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name1 . "</td>
			<td>" . number_format($more_item_remove_value1, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name2 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name2 . "</td>
			<td>" . number_format($more_item_remove_value2, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name3 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name3 . "</td>
			<td>" . number_format($more_item_remove_value3, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name4 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name4 . "</td>
			<td>" . number_format($more_item_remove_value4, 2) . "</td></tr>";
                            }
                            if ($more_item_remove_name5 != "") {
                                $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $more_item_remove_name5 . "</td>
			<td>" . number_format($more_item_remove_value5, 2) . "</td></tr>";
                            }

                            //Extension for LOAN DISPLAY

                            //         $dbOpen3 = ("SELECT Convert(Varchar(11),ExpPayDate,106) as ExpPD, (datename(M,ExpPayDate)+' '+datename(YYYY,ExpPayDate)) LnPayDate,
                            //         * from LnIndvOff  where EID='" . $EmpHKey . "' and Status = 'P' and Status <> 'D'
                            //         and Month(ExpPayDate) = MONTH(GETDATE())
                            // and YEAR(ExpPayDate) = YEAR(GETDATE())
                            //         Order by ExpPayDate ASC");

                            //         //if (ScriptRunner($dbOpen3,"Cnt") > 0){
                            //         //    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr></td></tr>";

                            //         include '../login/dbOpen3.php';
                            //         $row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH);
                            //         //while($row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH))        {

                            //         $ExpPay = $row3['ExpPay'];
                            //         $EID = $row3['EID'];
                            //         $LID = $row3['LID'];
                            //         $ExpPDate = $row3['ExpPD'];
                            //         $HashKey = $row3['HashKey'];
                            //         $LnPayDate = $row3['LnPayDate'];
                            //         //$LnPayDate="May 2017";

                            //         if (($LnPayDate == $paymonth_yr) && (($ExpPDate != "") || ($ExpPay != ""))) {
                            //             //($paymonth_yr)[$LnPayDate]
                            //             $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>Loan repayment</td></tr>";

                            //             $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $ExpPDate . "</td>
                            //         <td>" . $ExpPay . "</td></tr>";
                            //         }
                            //}
                            //}

                            $total_loan = 0;
                            $dbOpen3 = ("SELECT Convert(Varchar(11),AuthDate,106) as AuthPD,
		(datename(M,AuthDate)+' '+datename(YYYY,AuthDate)) LnAuthDate, * from LnDetails
		where EID='" . $EmpHKey . "'  and Status <> 'D'
		and Month(AuthDate) = MONTH('" . $row2['PMth'] . "')
		and YEAR(AuthDate) = YEAR('" . $row2['PMth'] . "')
        Order by AuthDate ASC");

                            // var_dump( $row2['PMth']);

                            include '../login/dbOpen3.php';
                            //if (ScriptRunner($dbOpen3,"Cnt") > 0){
                            //    $QueryStr=$QueryStr."<tr><td colspan='2' height='1'><hr></td></tr>";

                            // $row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH);
                            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                $total_loan += (float) $row3['LnAmt'];
                                $loan_amt = $row3['LnAmt'];
                                $AuthPD = ($row3['AuthDate'] == null) ? null : $row3['AuthDate']->format("F j Y");
                                //$LnPayDate="May 2017";

                                // date('M Y', strtotime(date('M Y')));

                                $Script = "SELECT SetValue2 from Settings where Setting ='Payroll Approver'";
                                $id_loan = ScriptRunner($Script, "SetValue2");

                                if ($id_loan == '0') {
                                    $total_loan = 0;
                                }

                                if (isset($loan_amt)  &&  $id_loan == '1') {

                                    $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr><br/>LOAN</td></tr>";

                                    $QueryStr = $QueryStr . "<tr><td align='right' class='tdMenu_HeadBlock_Light'>" . $AuthPD . "</td>
							<td>" . number_format($loan_amt, 2) . "</td></tr>";
                                }
                                //}
                            }
                            include '../login/dbClose3.php';

                            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            // Lawrence to add payslip
                            if ($row2['LeaveAllowance'] > 0) {
                                $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >LEAVE</td><td style='font-size:11px' >" . number_format($row2['LeaveAllowance'], 2) . "</td></tr>";
                            }

                            $QueryStr = $QueryStr . "<tr><td colspan='2' height='1'><hr></td></tr>";
                            $total_gross = $row2['Gross'] + $total_fp;
                            $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock' style='font-size:11px' >GROSS</td><td style='font-size:11px' >" . number_format($total_gross, 2) . "</td></tr>";

                            // ANNIVERSARY BONUS ADDED TO THE PAYSLIP
                            if ($row2['AnniversaryBonus'] > 0) {
                                $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >ANNIVERSARY BONUS</td><td style='font-size:11px' >" . number_format($row2['AnniversaryBonus'], 2) . "</td></tr>";
                            }

                            // OVERTIME BONUS
                            if ($row2['OverTimeAmount'] > 0) {
                                $QueryStrCR = $QueryStrCR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >Overtime</td><td style='font-size:11px' >" . number_format($row2['OverTimeAmount'], 2) . "</td></tr>";
                            }

                            // LATENESS BONUS
                            if ($row2['LatenessAmount'] > 0) {
                                $total_ded = $total_ded + $row2['LatenessAmount'];
                                $QueryStrDR = $QueryStrDR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >Lateness</td><td style='font-size:11px' >" . number_format($row2['LatenessAmount'], 2) . "</td></tr>";
                            }

                            // ABSENT
                            if ($row2['AbsentAmount'] > 0) {
                                $total_ded = $total_ded + $row2['AbsentAmount'];
                                $QueryStrDR = $QueryStrDR . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px'  >Absent</td><td style='font-size:11px' >" . number_format($row2['AbsentAmount'], 2) . "</td></tr>";
                            }

                            if ($row2['CurrencyType'] != "") {
                                $script_curr_name = "SELECT Val1 FROM Masters WHERE HashKey='" . trim($row2['CurrencyType']) . "'";
                                $curr_name = " (" . ScriptRunner($script_curr_name, "Val1") . ")";
                            } else {
                                $curr_name = " (Naira NGR)";
                            }

                            // $QueryStr = $QueryStr . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >PENSION</td><td style='font-size:11px' >" . number_format($row2['PensionEmployee'], 2) . "</td></tr>";
                            // $QueryStr = $QueryStr . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:11px' >PAYE</td><td style='font-size:11px'>" . number_format($row2['PAYE'], 2) . "</td></tr>";
                            $QueryStr = $QueryStr . "<tr style='font-size:11px' ><td height=1></td><td height=1><hr></td></tr>";
                            $QueryStr = $QueryStr . "<tr style='font-size:11px' ><td align='right' class='tdMenu_HeadBlock' style='font-size:11px' ><strong>NET PAY</strong></td><td style='font-size:11px' ><strong>" . number_format($row2['NetPay'] + (float) $total_loan, 2) . "$curr_name </strong></td></tr>";
                            $QueryStr = $QueryStr . "<tr style='font-size:11px' ><td height=1></td><td height=1><hr></td></tr>";
                            $empdt = $row2['EmpDt'] ? $row2['EmpDt']->format('F j, Y') : "N/A";
                            $empcat = $row2['EmpCategory'] ? $row2['EmpCategory'] : "N/A";
                            $PaySlip = str_replace('#MonthPaid#', $row2['PMth'], $PaySlip);
                            $PaySlip = str_replace('#Name#', $row2['Full Name'], $PaySlip);
                            $PaySlip = str_replace('#Idnumber#', $row2['EmpID'], $PaySlip);
                            $PaySlip = str_replace('#category#', $empcat, $PaySlip);
                            $PaySlip = str_replace('#Employmentdate#', $empdt, $PaySlip);
                            if (strlen($QueryStrCR) > 3) {
                                $QueryStrCR = "<tr  style='font-size:11px'><th colspan='2' class='tdMenu_HeadBlock' style='color:blue'  style='font-size:11px' >Earnings</th></tr>" . $QueryStrCR;
                            }


                            // if (strlen($QueryStrDR) > 3) {
                            $QueryStrDR = "<tr><th colspan='2'  class='tdMenu_HeadBlock' style='color:blue ; font-size:10px'>Deductions</th></tr>" . $QueryStrDR;
                            $total_ded = $total_ded + $row2['PensionEmployee'];
                            $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light' style='font-size:10px'>PENSION</td><td>" . number_format($row2['PensionEmployee'], 2) . "</td></tr>";
                            $total_ded = $total_ded + $row2['PAYE'];
                            $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock_Light'  style='font-size:10px'>PAYE</td><td>" . number_format($row2['PAYE'], 2) . "</td></tr>";
                            $QueryStrDR = $QueryStrDR . "<tr><td align='right' class='tdMenu_HeadBlock'  style='font-size:10px'>Total Deduction</td><td>" . number_format($total_ded, 2) . "</td></tr>";
                            // }
                            if (
                                strlen($QueryStrRL) > 3
                            ) {
                                $QueryStrRL = "<tr style='font-size:11px' ><th colspan='2' class='tdMenu_HeadBlock' style='font-size:11px' style='color:blue' >Reliefs</th></tr>" . $QueryStrRL;
                            }


                            if (
                                strlen($QueryStrFP) > 3
                            ) {
                                $QueryStrFP = "<tr style='font-size:11px' ><th colspan='2' class='tdMenu_HeadBlock' style='font-size:11px' style='color:blue' >Others</th></tr>" . $QueryStrFP;
                            }

                            $PaySlip = str_replace('#Values#', ('<TABLE>' . $QueryStrCR . $QueryStrDR . $QueryStrRL . $QueryStr . '</TABLE>'), $PaySlip);
                        }
                        include '../login/dbClose2.php';

                        //"=============================================================".$PaySlip."============================================================";
                        if ((isset($_REQUEST['send_slip'])) && ($_REQUEST['send_slip'] == "1")) {
                            $To = ScriptRunner($Script_ID, "Email");
                            $To2 = ScriptRunner($Script_ID, "Sec_Email");



                            $Bdy = $PaySlip;

                            // $dompdf = new DOMPDF();
                            $mpdf = new mPDF();

                            // Upload your HTML code
                            // $dompdf->load_html("$Bdy");
                            $pdf_body = str_replace('#BranchName#', $BranchName, $_SESSION["StkTck" . "MailHeader"]) . $Bdy . $_SESSION["StkTck" . "MailFooter"];
                            $mpdf->WriteHTML($pdf_body);



                            //Render html
                            // $dompdf->render();
                            $custId = $_SESSION["StkTck" . "CustID"];
                            $path = "../uploads_files/$custId/payslips";
                            $name = md5(uniqid(rand(), true)) . ".pdf";

                            if (!file_exists($path)) {
                                mkdir($path, 0777, true);
                            }
                            // //Create and output the pdf 
                            // Output to a file
                            // $ok = $dompdf->output();

                            $mpdf->Output("$path/$name", "F");

                            // file_put_contents("$path/$name", $ok);
                            $Atth = "$path/$name";

                            QueueMail($To, $Subj, $Bdy, $Atth, ''); //Sends the mail
                            if ($To2) {
                                QueueMail($To2, $Subj, $Bdy, $Atth, ''); //Sends the mail
                            }
                        }
                    }
                }
            }
        } // END FOR
        if ((isset($_REQUEST['send_slip'])) && ($_REQUEST['send_slip'] == "1")) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee payment(s) authorized successfully. Payslip sent', 'green'); }</script>");
        } else {
            echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee payment(s) authorized successfully.', 'green'); }</script>");
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("AUTHORIZE SALARY" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        // var_dump($_SESSION);
        if ($_SESSION['StkTckUName'] === 'Administrator' && $_SESSION['StkTckFName'] === 'Chief Administrator') {
            $i = 0;
            $HashVals = explode(";", $_REQUEST["ActLnk"]);
            $ArrayCount = count($HashVals);
            for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
                if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                    $EditID = ECh($HashVals[$i]);
                    $Script_ID = "SELECT Em.HashKey HKey, (Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm, Em.EmpID as EmpID, Em.HashKey as EmpHKey, Em.Email, Fp.HashKey from EmpTbl Em, Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";

                    $Script = "UPDATE [Fin_PRIndvPay]
					SET [Status] = 'U'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Unauthorize");
                    AuditLog("UNAUTHORIZE", "Employee payment unauthorized for " . ScriptRunner($Script_ID, "Nm"));
                    echo ("<script type='text/javascript'>{ parent.msgbox('Payroll records  unauthorized successfully', 'green'); }</script>");
                }
            }
        } else {
            echo ("<script type='text/javascript'>{ parent.msgbox('Payroll records cannot be unauthorized after final authorization', 'green'); }</script>");
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("AUTHORIZE SALARY" . "D") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT Em.HashKey HKey,
				(Em.FName+' '+Em.SName+' '+ONames+' ['+Convert(varchar(18),Em.EmpID)+']') Nm,
				Em.EmpID as EmpID, Em.Email, Fp.HashKey from EmpTbl Em,
				Fin_PRIndvPay Fp where Em.HashKey=Fp.EmpID and Fp.HashKey='" . $EditID . "'";
                //$EditID = ScriptRunner($Script_ID, "HashKey");
                $EmpID_ = ScriptRunner($Script_ID, "HKey");

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [Fin_PRIndvPay] WHERE [HashKey]='" . $EditID . "'") == true) {
                    // Check if account is customer account and if SMS alert is set to fire
                    $Script = "UPDATE [Fin_PRIndvPay]
                    SET [Status] = 'D'
                    ,[DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
                    ,[DeletedDate] = GetDate()
                    WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("AUTHORIZE", "Employee payment deleted for " . ScriptRunner($Script_ID, "Nm"));

                    $Script = "Select Convert(varchar(11),PayMonth,106) PayMth, [loanAmount] from Fin_PRIndvPay WHERE [HashKey]='" . $EditID . "'";
                    $loanAmount = ScriptRunner($Script, "loanAmount");
                    if (isset($loanAmount)) {
                        $Script_Loan = "Select * from LnIndvOff where EID='" . $EmpID_ . "' and [Status] in('P','B') order by AddedDate desc";
                        $status = ScriptRunner($Script_Loan, "Status");
                        $lid = ScriptRunner($Script_Loan, "LID");
                        $key = ScriptRunner($Script_Loan, "HashKey");
                        $kpaid = ScriptRunner($Script_Loan, "Paid");

                        // die;
                        if ($status === 'B') {
                            $Script = "UPDATE [LnIndvOff]
				            	SET [Status] = 'A' where [HashKey] ='" . ScriptRunner($Script_Loan, "HashKey") . "'";
                            ScriptRunnerUD($Script, "Authorize");
                        } elseif ($status === 'P') {
                            $newA = $kpaid - $loanAmount;
                            $Script = "UPDATE [LnIndvOff]
				            	SET [Status] = 'A' ,
                                [Paid] = '$newA'
                                where [HashKey] ='" . $key . "'";

                            ScriptRunnerUD($Script, "Authorize");


                            $Script_Loan = "Select * from [LnDetails] where HashKey ='" . $lid . "'";
                            $Paid = ScriptRunner($Script_Loan, "Paid");
                            $newPai = $Paid - $loanAmount;
                            $Script = "UPDATE [LnDetails]
				            	SET [Status] = 'A',
                                [Paid] = ' $newPai'
                                where [HashKey] ='" . ScriptRunner($Script_Loan, "HashKey") . "'";
                            ScriptRunnerUD($Script, "Authorize");
                        }
                    }


                    // I change Status='B' to Status='N'

                    // To update the attendance sheet date("Y-m", strtotime($PayMonth)) date_format(strtotime($PayMonth),"y-m");

                    // 1
                    $date_ = date("Ym", strtotime($PayMonth));
                    $Script = " UPDATE [Fin_Attend]
                    SET [Status] = 'N',
                    [UpdatedBy] = '" . $_SESSION["StkTck" . "HKey"] . "',
                    [UpdatedDate] = GetDate()
                    WHERE CONVERT(nvarchar(6), RecDate, 112) =  '" . $date_ . "' ";

                    ScriptRunnerUD($Script, "Authorize");
                    // end 1
                    //echo "<br>".$Script;
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
        include '../main/clr_val.php'; /* CLEARS $EditID */
    }
}
?>

<script type="text/javascript">
    function ChkConfirm($CtrName, $CtrAct) {
        $kkmsg = confirm("Do you really wish to " + $CtrAct + " the selected employee record(s)");

        if ($kkmsg == true) {
            if ($CtrAct == "Delete") {
                document.getElementById("FAction").value = "Delete Selected";
            }

            if ($CtrAct == "Authorize") {
                document.getElementById("FAction").value = "Authorize Selected";
            }

            if ($CtrAct == "Unauthorize") {
                document.getElementById("FAction").value = "Unauthorize Selected";
            }

            document.getElementById("DTrade").submit();
        }
    }
</script>

<script type="text/javascript">
    function ChkDel() {
        if (document.getElementById("DelChk_All").checked == true) {
            for (i = 1; i <= document.getElementById("DelMax").value; i++) {
                document.getElementById("DelBox" + i).checked = true;
                //		document.getElementById("DelBox" + i).checked="checked";
            }
        } else if (document.getElementById("DelChk_All").checked == false) {

            for (i = 1; i <= document.getElementById("DelMax").value; i++) {
                document.getElementById("DelBox" + i).checked = false;
                //		document.getElementById("DelBox" + i).checked="unchecked";
            }
        }
    }
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Calendar API -->

<script>
    $(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(function() {
        $(document).tooltip();
    });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body onLoad="doOnLoad(); GetMonth();">
    <form action="#" method="post" name="Records" target="_self" id="Records">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4">
                        <!-- [<a href="auth_paysal.php">Refresh Page</a>] -->
                    </div>
                    <div class="col-md-4 text-center ">
                        <h5>
                            View Review Employee Salary Payments
                        </h5>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" style="padding-right: 0px;">
                            </label>
                            <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
                                <?php
                                if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
                                    $SelID = ECh($_REQUEST["BranchID"]);
                                } elseif (isset($_REQUEST["BranchID"]) && $_REQUEST["BranchID"] == 'all') {
                                    $SelID = ECh($_REQUEST["BranchID"]);
                                }
                                echo '<select name="BranchID" class="form-control" id="BranchID" style="border-left:  3px solid red;">
									<option value="--" selected="selected">--</option>

									';

                                if (!isset($SelID)) {
                                    $SelID = "";
                                }

                                $dbOpen2 = ("SELECT OName, HashKey FROM BrhMasters where Status='A' ORDER BY OName");
                                include '../login/dbOpen2.php';
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    if ($SelID == $row2['HashKey']) {
                                        echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
                                    } else {
                                        echo '<option value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
                                    }
                                }
                                echo $SelID == 'all' ? '<option value="all" selected="selected">All Branches </option>' : '<option value="all" > All Branches </option>';
                                //include '../login/dbClose2.php';
                                echo '</select>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php $Load_Date = true;
                    $Load_Search = false;
                    $Load_Auth = false;
                    include '../main/report_opt.php'; ?>
                    <div>
                        <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="Load" /></td>
                    </div>
                </div>
                <hr>
                <!-- <div class="row">
                    <div class="col-md-12">
                        <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
                        <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                        if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {
                            $PayMthDt = " and PayMonth between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "' ";
                        } else {
                            $PayMthDt = '';
                        }
                        // if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                        if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "Load") && ($_REQUEST["BranchID"] != "--")):
                            //     print "<h4>UNAUTHORIZED</h4>";
                            // if ($BrhID == 'all') {
                            $dbOpen2 = ("SELECT (DateName(Month,Fm.PayMonth)+' '+convert(varchar(4),datename(Year,Fm.PayMonth))) as Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where  Em.HashKey=Fm.EmpID and (Fm.Status in ('N','U')) and (Em.Status in ('A','U') and EmpStatus='Active') " . $PayMthDt . " order by SName asc, UpdatedDate desc");
                            // } else {
                            //     $dbOpen2 = ("SELECT (DateName(Month,Fm.PayMonth)+' '+convert(varchar(4),datename(Year,Fm.PayMonth))) as Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm where Em.BranchID in (" . $BrhID . ") and Em.HashKey=Fm.EmpID and (Fm.Status in ('N','U','A')) and (Em.Status in ('A','U') and EmpStatus='Active') " . $PayMthDt . " order by SName asc, UpdatedDate desc");
                            // }
                            // } elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "AUTHORIZE")) {

                            //     print "<h4>AUTHORIZED</h4>";

                            // if ($BrhID == 'all') {
                            //     $dbOpen2 = ("SELECT (DateName(Month,Fm.PayMonth)+' '+convert(varchar(4),datename(Year,Fm.PayMonth))) as Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm
                            // 		where Em.HashKey=Fm.EmpID and (Fm.Status = 'A') and (Em.Status in ('A','U') and EmpStatus='Active')" . $PayMthDt . " order by SName asc, UpdatedDate desc");
                            // } else {
                            //     $dbOpen2 = ("SELECT (DateName(Month,Fm.PayMonth)+' '+convert(varchar(4),datename(Year,Fm.PayMonth))) as Dt, Fm.*, Fm.HashKey as HashKey, (Em.SName+' '+Em.FName+' ['+Em.EmpID+']') AS Nm from EmpTbl Em, Fin_PRIndvPay Fm
                            // 		where Em.BranchID in (" . $BrhID . ") and Em.HashKey=Fm.EmpID and (Fm.Status = 'A') and (Em.Status in ('A','U') and EmpStatus='Active')" . $PayMthDt . " order by SName asc, UpdatedDate desc");
                            // }
                            // } else {
                            // exit();
                            // }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive" id="table">
                            <thead>
                                <tr>
                                    <!-- <th data-sorter="false"><span class="TinyTextTight">
                                            <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                            <label for="DelChk_All"></label> -->
                                    </span></th>
                                    <th>Pay Period</th>
                                    <th>Employee Name</th>
                                    <th>Basic</th>
                                    <th>Housing</th>
                                    <th>Transport</th>
                                    <th>PAYE</th>
                                    <th>Pension</th>
                                    <th>Net Pay</th>
                                    <th>Payment Review</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Del = 0;
                                include '../login/dbOpen2.php';
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    $Del = $Del + 1;
                                ?>
                                    <tr>
                                        <!-- <td><input name="<? //php echo ("DelBox" . $Del); 
                                                                ?>" type="checkbox" id="<? //php echo ("DelBox" . $Del); 
                                                                                        ?>" value="<?php echo ($row2['HashKey']); ?>" />
                                            <label for="<? //php echo ("DelBox" . $Del); 
                                                        ?>"></label>
                                        </td> -->
                                        <!--<td width="133" align="center" valign="middle" class="TinyText" scope="col" ><?php echo (trim($row2['Dt'])); ?></td>-->
                                        <td>&nbsp;<?php echo $row2['Dt']; ?></td>
                                        <?php $hashkey = $row2['HashKey']; ?>
                                        <td><a href="#" style="text-decoration: underline;">
                                                <div class="TinyText" onClick="parent.ShowDisp('Employee Salary Payment','fin/VwEmpPayDtl_review.php?FAction=View Selected&DelMax=1&ActLnk=<?php echo $hashkey ?>',600,1120,'Yes')">
                                                    &nbsp;<?php echo (trim($row2['Nm'])); ?>
                                                </div>
                                            </a> </td>
                                        <td><?php
                                            $Basic = $Basic + $row2['PayItem1'];
                                            echo number_format($row2['PayItem1'], 2); ?></td>
                                        <td><?php
                                            $Housing = $Housing + $row2['PayItem2'];
                                            echo number_format($row2['PayItem2'], 2); ?></td>
                                        <td><?php
                                            $Transport = $Transport + $row2['PayItem3'];
                                            echo number_format($row2['PayItem3'], 2); ?></td>
                                        <td><?php
                                            $PAYE = $PAYE + $row2['PAYE'];
                                            echo number_format($row2['PAYE'], 2); ?></td>
                                        <td><?php
                                            $Pension = $Pension + $row2['PensionEmployee'];
                                            echo number_format($row2['PensionEmployee'], 2); ?></td>
                                        <td><?php
                                            $Net = $Net + $row2['NetPay'];
                                            echo number_format($row2['NetPay'], 2); ?></td>
                                        <td style="width:12%;">
                                            <select name="paymentReview[<?= $row2['HashKey'] ?>]" class="form-control paymentReview" id="paymentReview<?= $row2['HashKey'] ?>" disabled>
                                                <option value="satisfactory" <?php echo ($row2['paymentReview'] == 'satisfactory') ? 'selected' : ''; ?>>Satisfactory</option>
                                                <option value="unsatisfactory" <?php echo ($row2['paymentReview'] == 'unsatisfactory') ? 'selected' : ''; ?>>Unsatisfactory</option>
                                            </select>
                                        </td>
                                        <td>
                                            <textarea name="comment[<?= $row2['HashKey'] ?>]" id="comment<?= $row2['HashKey'] ?>" cols="30" rows="5" class="comment" disabled><?php echo $row2['comment'] ?></textarea>
                                        </td>
                                    </tr>
                                <?php }
                                include '../login/dbClose2.php'; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th valign="middle" class="smallText" scope="col" data-sorter="false">&nbsp;</th>
                                    <!--<th width="133" align="center" valign="middle" scope="col"><span class="TinyText"></span>Raised On<img src="../images/good_sign.gif" height="10" /></th> //-->
                                    <th width="160" align="center" valign="middle" class="smallText" scope="col">&nbsp;</th>
                                    <!-- <th width="424" align="center" valign="middle" scope="col">&nbsp;</th> -->
                                    <th width="102" align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Basic, 2); ?></th>
                                    <th width="97" align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Housing, 2); ?></th>
                                    <th width="132" align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Transport, 2); ?></th>
                                    <th width="106" align="right" valign="middle" scope="col"><span class="smallText"><?php echo number_format($PAYE, 2); ?></span></th>
                                    <th width="99" align="right" valign="middle" scope="col"><span class="smallText"><?php echo number_format($Pension, 2); ?></span></th>
                                    <th width="156" align="right" valign="middle" class="smallText" scope="col"><?php echo number_format($Net, 2); ?></th>
                                    <th>Payment Review</th>
                                    <th>Comment</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-6 pt-5">
                        <?php include '../main/pagination.php'; ?>
                    </div>
                    <div class="col-md-6 pt-5">
                        <div class="row">
                            <div class="col-md-6 pr-0 pl-0">
                                <?php if (ValidateURths("AUTHORIZE SALARY" . "T") == true) : ?>
                                    <?php if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) : ?>
                                        <div class="demo-checkbox">
                                            <input type="checkbox" id="send_slip_mod" name="send_slip" value="1" />
                                            <label for="send_slip_mod">
                                                <p title="" name="send_slip" id="send_slip_mod" style="line-height: 17px">Tick to send pay slip when you Authorize </p>
                                            </label>
                                        </div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="col-md-6">
                                <?php
                                echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
									<input name="FAction" id="FAction" type="hidden" />
									<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                //<input name="PgTy" id="PgTy" type="hidden" value="'.$_REQUEST["PgTy"].'" />
                                //Check if any record was spolled at all before enabling buttons
                                // if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {

                                //     if (ValidateURths("AUTHORIZE SALARY" . "D") == true) {
                                //         //Check if user has Delete rights
                                //         $but_HasRth = ("AUTHORIZE SALARY" . "D");
                                //         $but_Type = 'Delete';
                                //         include '../main/buttons.php';
                                //     }

                                //     if (ValidateURths("AUTHORIZE SALARY" . "T") == true) {
                                //         //Check if user has Authorization rights
                                //         $but_HasRth = ("AUTHORIZE SALARY" . "T");
                                //         $but_Type = 'Authorize';
                                //         include '../main/buttons.php';
                                //     }
                                // } else {
                                //     if (ValidateURths("AUTHORIZE SALARY" . "T") == true) {
                                //         //Check if user has Authorization rights
                                //         $but_HasRth = ("AUTHORIZE SALARY" . "T");
                                //         $but_Type = 'Unauthorize';
                                //         include '../main/buttons.php';
                                //     }
                                // if (ValidateURths("AUTHORIZE SALARY"."V")==true){
                                //     //Check if user has Add/Edit rights
                                //     $but_HasRth=("AUTHORIZE SALARY"."V"); $but_Type = 'View'; include '../main/buttons.php';
                                // }
                                // }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
    </form>
</body>
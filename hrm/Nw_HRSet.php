<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;include '../css/myscripts.php';

//$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; $Load_JQuery_Tags=true; include '../css/myscripts.php';
if (ValidateURths("HR SETTINGS" . "V") != true) {include '../main/NoAccess.php';exit;}

$GoValidate = true;
if (!isset($EditID)) {$EditID = "--";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($AllEmployee)) {$AllEmployee = "";}
if (!isset($EditID)) {$EditID = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($AuthRec)) {$AuthRec = "";}

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected") {
        if (ValidateURths("HR SETTINGS" . "A") != true) {include '../main/NoAccess.php';exit;}

        /*CHeck validation for updating an account */
        $GoValidate = true;
        if (trim($_REQUEST["ApprSNotify1"]) == trim($_REQUEST["ApprSNotify2"]) && strlen(trim($_REQUEST["ApprSNotify1"])) == "32") {
            echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the appraisal started(2).', 'red'); }</script>");
            $GoValidate = false;
        }

        if (trim($_REQUEST["ApprCNotify1"]) == trim($_REQUEST["ApprCNotify2"]) && strlen(trim($_REQUEST["ApprCNotify1"])) == "32") {
            echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the appraisal completed(2).', 'red'); }</script>");
            $GoValidate = false;
        }

        // LAWRENCE ADDED COLUMNS TO THE SETTINGS TABLE
        if ($GoValidate == true) {
            if (isset($_REQUEST["MMsg"]) && !empty($_REQUEST["MMsg"])) {
                $anniversaryMessage = sanitize($_REQUEST["MMsg"]);
            } else {
                $anniversaryMessage = 'NULL';
            }

            if (isset($_REQUEST["EConfirmNoticeNF1"])) {$EConfirmNoticeNF1_ = $_REQUEST["EConfirmNoticeNF1"];} else { $EConfirmNoticeNF1_ = '';}
            if (isset($_REQUEST["EConfirmNoticeNF2"])) {$EConfirmNoticeNF2_ = $_REQUEST["EConfirmNoticeNF2"];} else { $EConfirmNoticeNF2_ = '';}
            if (isset($_REQUEST["birthdayNotify3"])) {$allEmployee = 1;} else { $allEmployee = 0;}

            $Script = "Update [Settings] set [SetValue3]='" . ECh($_REQUEST["ApprSNotify1"]) . "', [SetValue4]='" . ECh($_REQUEST["ApprSNotify2"]) . "', [SetValue5]='" . ECh($_REQUEST["ApprCNotify1"]) . "',[SetValue6]='" . ECh($_REQUEST["ApprCNotify2"]) . "', [Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [Setting]= 'ApprNotify'";
            ScriptRunnerUD($Script, "Inst");

            $Script = "Update [Settings] set [SetValue]='" . ECh($_REQUEST["POutSig"]) . "',[SetValue2]='', [SetValue4]='" . ECh($_REQUEST["POutMobile"]) . "',			[SetValue5]='" . ECh($_REQUEST["POutEmail"]) . "',
				[SetValue7]='" . ECh($_REQUEST["CreatEmpNotify1"]) . "',
				[SetValue8]='" . ECh($_REQUEST["CreatEmpNotify2"]) . "',
				[SetValue9]='" . ECh($_REQUEST["UpdtEmpNotify1"]) . "',
				[SetValue10]='" . ECh($_REQUEST["UpdtEmpNotify2"]) . "',
				[SetValue11]='" . ECh($_REQUEST["birthdayNotifyEnable"]) . "',
				[SetValue15]='" . ECh($_REQUEST["birthdayNotify1"]) . "',
				[SetValue16]='" . ECh($_REQUEST["birthdayNotify2"]) . "',
				[SetValue17]='" . ECh($_REQUEST["HRNotify"]) . "',
				[SetValue18]='" . ECh($_REQUEST["birthdayMessage"]) . "',
				[SetValue19]='" . ECh($_REQUEST["empType_notif"]) . "',
				[SetValue20]='" . $anniversaryMessage . "',
				[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [Setting]= 'HRSignedDoc'";
            ScriptRunnerUD($Script, "Inst");

            $Script = "Update [Settings] set [SetValue]='" . ECh($_REQUEST["branch_downline"]) . "',[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [Setting]= 'branchDown'";
            ScriptRunnerUD($Script, "Inst");

            AuditLog("UPDATE", "Organisational HR settings updated");
            echo ("<script type='text/javascript'>{ parent.msgbox('Organisational HR setting updated successfully.', 'green'); }</script>");
            //Clear Selection//

            $Script_Edit = "Select *,Convert(Varchar(11),[SetStart],106) SDt, Convert(Varchar(11),[SetEnd],106) EDt from Settings where [Setting]= 'LeaveTyp'";

        }

        if ($GoValidate == false) {$EditID = "--";}
    } //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("HR SETTINGS" . "T") != true) {include '../main/NoAccess.php';exit;}

        $Script = "UPDATE [Settings]
			SET [Status] = 'A'
			,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
			,[AuthDate]=GetDate()
			WHERE [Setting] in ('EExitNotice','ApprNotify','ProspNotice','HRSignedDoc','branchDown')";
        ScriptRunnerUD($Script, "Authorize");
        AuditLog("AUTHORIZE", "Organisational leave setting authorized");

        echo ("<script type='text/javascript'>{ parent.msgbox('Organisational leave setting authorized successfully.', 'green'); }</script>");
        $EditID = "--";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("HR SETTINGS" . "T") != true) {include '../main/NoAccess.php';exit;}
        $Script = "UPDATE [Settings]
			SET [Status] = 'U'
			,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
			,[AuthDate]=GetDate()
			WHERE [Setting] in ('EExitNotice','ApprNotify','ProspNotice','HRSignedDoc')";
        ScriptRunnerUD($Script, "Authorize");
        AuditLog("UNAUTHORIZE", "Organisational leave setting unauthorized");

        echo ("<script type='text/javascript'>{ parent.msgbox('Organisational leave setting unauthorized successfully.', 'green'); }</script>");
        $EditID = "--";
    }
}
?>
<script>
  $(function()
  {
	$('#datepicker').datepicker({
    showButtonPanel: true,
    dateFormat: "mm/dd/yy",
//    beforeShow: function(){
});


	$("#StartDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-12M", maxDate: "+12M", dateFormat: 'dd M yy'})
	$("#EndDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-12M", maxDate: "+12M", dateFormat: 'dd M yy'})
  });
</script>

<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

<!-- Select 2-->
<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">


<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
		        <div class="col-md-12 text-center">
			        <h3>Organisational HR Setting</h3>
		        </div>
	      	</div>
		</div>
		<?php
$AllEmployee = "";
$dbOpen2 = ("SELECT * from EmpTbl where EmpStatus='Active' and Status in('A','U','N') order by SName Asc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    //echo '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [".$row2['EmpID']."]" . '</option>';
    $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
}
include '../login/dbClose2.php';
/*
$Script_Edit = "Select * from Settings where Setting='EConfirmNotice'";

echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;

$EditID = ScriptRunner($Script_Edit,"SetValue3");
if (strlen($EditID)==32)
{
$Script ="Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='".$EditID."'";
echo '<option selected="selected" value="'.$EditID.'">'.ScriptRunner($Script,"Nm").'</option>';
}
 */
?>
		<div class="box-body">
			<div class="col-sm-12" style="padding-left: 0px">
		        <h4> Appraisal Notification Completion</h4>
		    </div>
		    <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Start/End Appraisal (1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="ApprSNotify1" id="ApprSNotify1">
	        		    		<?php
$Script_Edit = "Select * from Settings where Setting='ApprNotify'";
$EditID = ScriptRunner($Script_Edit, "SetValue3");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Start/End Appraisal (2):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control"  name="ApprSNotify2" id="ApprSNotify2">
	        		    		<?php
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
$EditID = ScriptRunner($Script_Edit, "SetValue4");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Appraisal Settings Updated (1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="ApprCNotify1" id="ApprCNotify1">
	        		    		<?php
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
$EditID = ScriptRunner($Script_Edit, "SetValue5");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Appraisal Settings Updated (2):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="ApprCNotify2" id="ApprCNotify2">
	        		    		<?php
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
$EditID = ScriptRunner($Script_Edit, "SetValue6");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        	</div>
	        </div>
        	<div class="col-sm-12" style="padding-left: 0px" >
        	    <h4> Printout Signatories</h4>
        	</div>
        	<div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">PrintOut Signatory (1)</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="POutSig" id="POutSig">
	        		    		<?php
$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
$EditID = ScriptRunner($Script_Edit, "SetValue");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Mobile Correspondence(1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="POutMobile" id="POutMobile">
	        		    		<?php
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;

$EditID = ScriptRunner($Script_Edit, "SetValue4");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Email Correspondence (1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="POutEmail" id="POutEmail">
	        		    		 <?php
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
$EditID = ScriptRunner($Script_Edit, "SetValue5");
if (strlen($EditID) == 32) {
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        	</div>
        	</div>
        	<div class="col-sm-12" style="padding-left: 0px" >
        	    <h4>Update on Employee</h4>
        	</div>
        	<div class="row">
        		<div class="col-md-6">
        			<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">New Employee Created (1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="CreatEmpNotify1" id="CreatEmpNotify1">
	        		    		<?php
$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";

$EditID = ScriptRunner($Script_Edit, "SetValue7");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">New Employee Created (2):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="CreatEmpNotify2" id="CreatEmpNotify2">
	        		    		 <?php
$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
$EditID = ScriptRunner($Script_Edit, "SetValue8");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Employment Type Notification:</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="empType_notif" id="empType_notif">
								<option selected="selected" value="0">--</option>
								<?php
$Script_Edit = "Select SetValue19 from Settings where Setting='HRSignedDoc'";
$val = ScriptRunner($Script_Edit, "SetValue19");

for ($php_i = 0; $php_i <= 90; $php_i += 5) {
    if ($php_i == 0) {
        continue;
    }
    if ($val == $php_i) {
        echo '<option selected=selected value="' . $val . '">' . $val . ' days</option>';
    } else {
        echo '<option value="' . $php_i . '">' . $php_i . ' days</option>';
    }

}
;
?>
							</select>
	        		    </div>
	        		</div>
        		</div>
        		<div class="col-md-6">
        			<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Employee Bio-Data Updated (1):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="UpdtEmpNotify1" id="UpdtEmpNotify1">
	        		    		<?php
$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
$EditID = ScriptRunner($Script_Edit, "SetValue9");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Employee Bio-Data Updated (2):</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="UpdtEmpNotify2" id="UpdtEmpNotify2">
	        		    		<?php

$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
$EditID = ScriptRunner($Script_Edit, "SetValue10");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
		        		    </select>
	        		    </div>
	        		</div>
	        		<div class="form-group row ">
	        		    <label  class="col-sm-4 col-form-label">Branch access to downlines</label>
	        		    <div class="col-sm-8  input-group-sm" style=" padding-right: 35px">
	        		    	<select class="form-control" name="branch_downline" id="branch_downline">
	        		    		<?php

$Script_Edit = "Select * from Settings where Setting='branchDown'";
$EditID = ScriptRunner($Script_Edit, "SetValue");

?>
								<option value="">--</option>
								<option value="yes"  <?php echo $EditID === 'yes' ? 'selected' : '' ?> >Yes</option>
								<option value="no" <?php echo $EditID === 'no' ? 'selected' : '' ?> >No</option>

		        		    </select>
	        		    </div>
	        		</div>
        		</div>
        	</div>
			<div>
				<div class="col-sm-12" style="padding-left: 0px" >
					<h4>Other Notifications</h4>
				</div>
				<!--<div class="row"> -->
				<div class="form-group row">
					<label class="col-sm-2 col-form-label" style="padding-right: 0px;">Notification Before Birthday:</label>
					<div class="col-sm-4 input-group input-group-sm" style=" padding-right: 35px">
						<select class="form-control" name="birthdayNotify1" id="birthdayNotify1">
							<?php

$Script_Edit = "Select SetValue15 from Settings where Setting='HRSignedDoc'";
$birthdayID = ScriptRunner($Script_Edit, "SetValue15");

for ($php_i = 1; $php_i <= 31; $php_i++) {
    if ($birthdayID == $php_i) {
        echo '<option selected=selected value="' . $birthdayID . '">' . $birthdayID . ' days</option>';
    } else {
        echo '<option value="' . $php_i . '">' . $php_i . ' days</option>';
    }
}
;

?>
						</select>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" title="Birthday notification before the actual date of each employee's birthday. Example: 2days before 4th November" id="birthdayNotifyInfo">
							<i class="fa fa-info"></i>
							</button>
						</span>
					</div>

					<label class="col-sm-2 col-form-label" style="padding-right: 0px;">Intervals:</label>
					<div class="col-sm-4 input-group input-group-sm" style=" padding-right: 35px">
						<select class="form-control" name="birthdayNotify2" id="birthdayNotify2">
							<option selected="selected" value="0">--</option>
							<?php
$Script_Edit = "Select SetValue16 from Settings where Setting='HRSignedDoc'";
$birthdayID = ScriptRunner($Script_Edit, "SetValue16");

for ($php_i = 1; $php_i <= 10; $php_i++) {
    if ($birthdayID == $php_i) {
        echo '<option selected=selected value="' . $birthdayID . '">' . $birthdayID . ' days</option>';
    } else {
        echo '<option value="' . $php_i . '">' . $php_i . ' days</option>';
    }

}
;
?>
						</select>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" title="Email notification between notification start and birthday" id="birthdayNotifyInfo">
							<i class="fa fa-info"></i>
							</button>
						</span>
					</div>
				</div>

				<div class="form-group row">

				<label class="col-sm-2 col-form-label pt-5" style="padding-right: 0px;">Enable Birthday Notifications:</label>
					<div class="col-sm-4 input-group input-group-sm" style=" padding-right: 35px">
					<?php
$Script_Edit = "Select SetValue11 from Settings where Setting='HRSignedDoc'";
$ur = ScriptRunner($Script_Edit, 'SetValue11');
?>
						<select class="form-control" name="birthdayNotifyEnable" id="birthdayNotifyEnable">
							<option  <?php echo ($ur === "0" ? "selected" : "") ?> value="0">No</option>
							<option  <?php echo ($ur === "1" ? "selected" : "") ?>   value="1">Yes</option>

						</select>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" title="Determine if birthday email notification should work" id="birthdayNotifyInfo">
							<i class="fa fa-info"></i>
							</button>
						</span>
					</div>




					<label  class="col-sm-2 col-form-label">Human Resource Manager:</label>
        		    <div class="col-sm-4  input-group-sm" style=" padding-right: 35px">
        		    	<select class="form-control" name="HRNotify" id="HRNotify">
						<?php
$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
$EditID = ScriptRunner($Script_Edit, "SetValue17");
echo '<option value="--" selected="selected">--</option>';
echo $AllEmployee;
if (strlen($EditID) == 32) {
    //    $kk = ScriptRunner($Script_Edit,"SetValue3");
    $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
    echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
}
?>
	        		    </select>
        		    </div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Birthday Message:</label>
					<div class="col-sm-4  input-group-sm" style=" padding-right: 35px">
						<?php
$Script_Birthday = "SELECT SetValue18 FROM Settings WHERE Setting='HRSignedDoc'";
$message = ScriptRunner($Script_Birthday, "SetValue18");
if ($message != "") {echo '<textarea name="birthdayMessage" rows="4" cols="50"  id="birthdayMessage">' . $message . '</textarea>';} else {echo '<textarea name="birthdayMessage" rows="4" cols="50" id="birthdayMessage"></textarea>';}
?>
						<br>
						<p class="text-danger" ><strong style="font-weight: 600">#FName# [DON'T REMOVE]:</strong> #FName# picks up employee's name.</p>
						<br>
						<p class="text-danger"><strong style="font-weight: 600">#birthday# [DON'T REMOVE]:</strong> #birthday# picks up employee's date of birth.</p>
					</div>

					<label class="col-sm-2 col-form-label"> Work Anniversary Message:</label>
					<div class="col-sm-4 " style=" padding-right: 35px">
						<?php
$message = ScriptRunner("SELECT SetValue20 FROM Settings WHERE Setting='HRSignedDoc'", "SetValue20");
?>
						<?php if ($message): ?>
							<textarea name="MMsg" class="form-control MMsg"  id="MMsg"><?php echo $message ?></textarea>
						<?php else: ?>
							<textarea name="MMsg" class="form-control MMsg" rows="3" id="MMsg">
								<p>Hello #Name#,</p>
								<p>You have completed #Years# with our organization, and what a time it has been.</p>
								<p>We are thankful for your creative ideas that have helped us evolve as an organization.</p>
								<p>Wishing you a happy work anniversary and hoping you continue your awe-inspiring contribution.</p>
							</textarea>
						<?php endif?>
						<p class="text-danger"><strong style="font-weight: 600">To pick employee name use #Name# in text.</strong></p>
						<p class="text-danger"><strong style="font-weight: 600">To pick number of years employee has spent use #Years# in text</strong></p>
					</div>
				</div>

			</div>

			<div class="text-center">
				<?php
//Check if user has Add/Update User rights
if (ValidateURths("HR SETTINGS" . "A") == true) {

    if ($AuthRec != "A") {?>
			        <input name="SubmitTrans" type="button" class="btn btn-default btn-sm" id="SubmitTrans" value="Update" onclick="YesNo('Update', 'Update');"/>&nbsp;&nbsp;&nbsp;&nbsp;
			        <!-- <input name="SubmitTrans" type="button" class="btn btn-default btn-sm" id="SubmitTrans" value="Update" onclick="YesNo('Update', 'Update')"/>&nbsp;&nbsp;&nbsp;&nbsp; -->
			        <?php
} else {
        echo '<input name="SubmitTrans" type="submit" class="btn btn-default btn-sm" id="SubmitTrans" value="Update" disabled="disabled" />&nbsp;&nbsp;&nbsp;&nbsp;';
    }
}
//else
//{
//echo '<input name="SubmitTrans" type="submit" class="smallButton_disabled" id="SubmitTrans" value="Update" disabled="disabled" />';
//}
if (ValidateURths("HR SETTINGS" . "T") == true) {
    //if (ValidateURths("SET LEAVE POLICY"."T")==true)
    //{
    $Script_Menu = "Select Status from Settings where Setting='ApprNotify'";
    $AuthRec = ScriptRunner($Script_Menu, "Status");

    if ($AuthRec == "A") {?>
			        <input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Unauthorize All" onclick="YesNo('Unauthorize All', 'Unauthorize')" />
			        <?php
} else {?>
			        <input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Authorize All" onclick="YesNo('Authorize All', 'Authorize')" />
			        <?php
}
}
//else
//{ echo '<input name="SubmitTrans" id="SubmitTrans" type="button" class="smallButton_disabled" value="Authorize Selected" disabled="disabled" />'; }

//'<input name="UpdID" type="hidden" id="UpdID" value="' . $EditID . '" />
$Del = 1;
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
					<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
					<input name="FAction" id="FAction" type="hidden" />
					<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

?>
			</div>
       </td>
</tr>
  </table></td>
    </tr>
</table>
    </td>
  </tr>
	</form>
</table>
<script type="text/javascript" src="../jqry/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		tinyMCE.init({
			// General options
			mode : "specific_textareas",
			editor_selector : "MMsg",
			theme: "simple",
		    width: "100%",
		    height: "150",
		    // theme_advanced_disable : "sup,undo",
		});
	});
	// function checkT() {
	// 	const value = tinyMCE.get('MMsg').getContent({format: 'text'}).trim();
	// }
</script>
<!-- TinyMCE -->
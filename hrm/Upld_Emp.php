<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

if (ValidateURths("EMPLOYEE" . "A") != true) {include '../main/NoAccess.php';exit;}
$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
require_once 'emp_upload_func.php';
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    if (isset($_REQUEST["SubmitDoc"]) && $_REQUEST["SubmitDoc"] == "Upload") {
        if (ValidateURths("EMPLOYEE" . "A") != true) {include '../main/NoAccess.php';exit;}
        $success = 0;
        $error = 0;
        $errorRows = [];
        $msg = '';
        $shouldGenerateId = (isset($_REQUEST["generate"])) ? true : false;
        $file = $_FILES['dmsupd']['tmp_name'];
        if ($file) {
            $csv_mimetypes = ['text/csv', 'application/csv', 'text/comma-separated-values', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/octet-stream'];
            if (!in_array($_FILES['dmsupd']['type'], $csv_mimetypes)) {
                echo ("<script type='text/javascript'>{ parent.msgbox('File format not accepted.', 'red'); }</script>");
            } elseif (($_FILES['dmsupd']['size'] >= 20000000)) {
                echo ("<script type='text/javascript'>{ parent.msgbox('File too large. File must not be greater than 20MB.', 'red'); }</script>");
            } else {
                if (($handle = fopen($file, "r")) !== false) {
                    fgetcsv($handle);
                    fgetcsv($handle);
                    $headers = fgetcsv($handle);
                    // var_dump($headers);
                    $count = 4;
                    $totalEmp = ScriptRunnerParent("SELECT * FROM DbConnStr WHERE CustID = '" . $_SESSION["StkTck" . "CustID"] . "'")['Employees'];
                    while (($data = fgetcsv($handle, 10000, ",")) !== false) {
                        // var_dump($data);
                        // die();
                        if ($totalEmp) {
                            if ((int) ScriptRunner("SELECT COUNT(DISTINCT EmpID) as Active From EmpTbl WHERE EmpStatus = 'Active' AND EmpID <> '000000'", "Active") >= $totalEmp) {
                                $msg .= 'Error adding an active employee record.<br>You may have exhausted your licenses.<br>Please contact your vendor.<br>';
                                break;
                            }
                        }
                        if ($shouldGenerateId) {
                            array_shift($data);
                            $createData = createEmployeeWithID($data);
                            if ($createData['status']) {
                                $success++;
                            } else {
                                $error++;
                                $errorRows[$count] = $createData['msg'];
                            }
                        } else {
                            $createData = createEmployeeNoID($data);
                            if ($createData['status']) {
                                $success++;
                            } else {
                                $error++;
                                $errorRows[$count] = $createData['msg'];
                            }
                        }
                        $count++;
                    }
                    fclose($handle);
                    $empClass->updateData($shouldGenerateId);
                    $_SESSION['errorRows'] = (count($errorRows)) ? $errorRows : false;
                    echo ("<script type='text/javascript'>{parent.msgbox('{$msg}Completed<br>{$success} Records Uploded <br> {$error} Records Failed', 'red'); }</script>");
                } else {
                    echo ("<script type='text/javascript'>{ parent.msgbox('Something went wrong. Please Try Again', 'red'); }</script>");
                }
            }
        } else {
            echo ("<script type='text/javascript'>{ parent.msgbox('Select a file to upload', 'red'); }</script>");
        }
    }
}

?>
<html>
	<head>
		<title></title>
		<?php
if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css">
		<?php }?>
       	<!-- Bootstrap 4.0-->
       	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
       	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       	<!-- Bootstrap 4.0-->
       	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
       	<!-- Theme style -->
       	<link rel="stylesheet" href="../assets/css/master_style.css">
       	<link rel="stylesheet" href="../assets/css/responsive.css">
       	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    </head>
<body oncontextmenu="return false;">
	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 text-center" style="margin-top: 70px">
					<form name="Upd" method="post" action="#" enctype="multipart/form-data">
						<p> Select a document file to upload</p>
						<div class="form-group  row"  style="margin-bottom: 5px">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="box bg-danger" style="margin-bottom: 0px" >
									<div class="box-body text-center" style="padding: 0.7rem">
										<div class="col-sm-12 input-group-sm">
											<input type="file" name="dmsupd" id="dmsupd" accept=".csv" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2"></div>
						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="demo-checkbox col-md-6 input-group input-group-sm" style="margin-top: 1%;">
								<input type="checkbox" name="generate" id="generate" />
								<label for="generate">
									Auto Generate ID
								</label>
							</div>
							<div class="col-md-3"></div>
							<div class="col-md-12 text-center">
								<input type="submit" name="SubmitDoc" id="SubmitDoc" value="Upload" class="btn btn-danger btn-sm">
								<input name="PgDoS" id="PgDoS" type="hidden" value="<?php echo DoSFormToken() ?>">
								<p style="margin-top: 1rem">Only CSV comma delimiter files allowed. Max file individual upload size is: 20Mb</p>
		  						<a href="Sample Employee Upload Sheet format.csv">Download Template file</a>
							</div>
							<div class="col-md-12">
								<?php if (isset($_SESSION['errorRows'])): ?>
									<?php if ($_SESSION['errorRows']): ?>
										<?php $errors = $_SESSION['errorRows']?>
											<?php foreach ($errors as $key => $value): ?>
												<p><strong style="font-weight: 700">Row Number <?php echo $key ?> not inserted (<?php echo $value ?>)</strong></p>
											<?php endforeach?>
									<?php endif?>
									<?php unset($_SESSION['errorRows']);?>
								<?php endif?>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

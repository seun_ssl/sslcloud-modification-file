<?php
error_reporting(0);
@ini_set('display_errors', 0);

session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;include '../css/myscripts.php';

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

//Validate user viewing rights
if (ValidateURths("BRANCH CREATION" . "V") != true) {include '../main/NoAccess.php';exit;}
$ModMaster = ValidateURths("HR MODULE MASTERS" . "A");

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($AllEmployee)) {$AllEmployee = "";}
if (!isset($AllEmployee2)) {$AllEmployee2 = "";}
if (!isset($AllEmployee3)) {$AllEmployee3 = "";}
if (!isset($AllEmployee4)) {$AllEmployee4 = "";}

$GoValidate = true;

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) &&
    $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) &&
    strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
        $Script_Edit = "Select * from BrhMasters where HashKey='Clear'";
        $EditID = "Clear";
    }

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Office") {
        if (ValidateURths("BRANCH CREATION" . "A") != true) {include '../main/NoAccess.php';exit;}

        if (strlen(trim($_REQUEST["TaxState"])) === "" || trim($_REQUEST["TaxState"]) == '--') {
            echo ("<script type='text/javascript'>{parent.msgbox('Office state is required. Please review.', 'red');}</script>");
            $GoValidate = false;
        }
        if (strlen(trim($_REQUEST["OName"])) < 3) {
            echo ("<script type='text/javascript'>{parent.msgbox('Office name must be at least three(3) characters long. Please review.', 'red');}</script>");
            $GoValidate = false;
        }

        if ($GoValidate == true) {
            $Script = "Select * from BrhMasters where OName='" . ECh(trim($_REQUEST["OName"])) . "' and [TaxState] ='{$_REQUEST['TaxState']}' and Status<>'D'";
            if (strlen(ScriptRunner($Script, "OName")) > 0) {
                echo ("<script type='text/javascript'>{parent.msgbox('An office record with a duplicate name already exist. Please review.', 'red');}</script>");
                $GoValidate = false;
            }
        }

        if (strlen(trim($_REQUEST["OCode"])) < 2) {
            echo ("<script type='text/javascript'>{parent.msgbox('You must enter an office code of at least two(2) characters to proceed.', 'red');}</script>");
            $GoValidate = false;
        }

        if ($GoValidate == true) {
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Our Offices" . $_SESSION["StkTck" . "UName"];
            $HashKey = md5($UniqueKey);
            // var_dump('here');

            $Script = "INSERT INTO [BrhMasters] ([OName],[TaxState],[OCode],[OType],[OParent],[OContact],[OAddress],[OPhone],[Status],[AddedBy],[AddedDate],[HashKey],[OContact2],[OContact3],[OContact4]) VALUES ('" . ECh($_REQUEST["OName"]) . "','" . ECh($_REQUEST["TaxState"]) . "','" . ECh($_REQUEST["OCode"]) . "','" . ECh($_REQUEST["OType"]) . "','" . ECh($_REQUEST["OParent"]) . "','" . ECh($_REQUEST["OContact"]) . "','" . ECh($_REQUEST["OAddress"]) . "','" . $_REQUEST["OPhone"] . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . ECh($_REQUEST["OContact2"]) . "','" . ECh($_REQUEST["OContact3"]) . "','" . ECh($_REQUEST["OContact4"]) . "')";
            // var_dump($Script);
            ScriptRunnerUD($Script, "Inst");

            //Leave Audit Trail
            AuditLog("INSERT", "New office created for [" . ECh($_REQUEST["OName"]) . "]");

            /* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */
            echo ("<script type='text/javascript'>{ parent.msgbox('New office created successfully.', 'green'); }</script>");
            $Script_Edit = "Select * from BrhMasters where HashKey='zzz'";
            $EditID = "zzz";
        }
    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Office" && isset($_REQUEST["DelMax"])) {
        if (ValidateURths("BRANCH CREATION" . "A") != true) {include '../main/NoAccess.php';exit;}
        /* Set HashKey of Account to be Updated */

        if (isset($_REQUEST["UpdID"])) {
            $EditID = ECh($_REQUEST["UpdID"]);
            $HashKey = ECh($_REQUEST["UpdID"]);
        }

        /*Check validation for updating an account */

        if (strlen(trim($_REQUEST["TaxState"])) === "" || trim($_REQUEST["TaxState"]) == '--') {
            echo ("<script type='text/javascript'>{parent.msgbox('Office state is required. Please review.', 'red');}</script>");
            $GoValidate = false;
        }
        if (strlen(trim($_REQUEST["OName"])) < 3) {
            echo ("<script type='text/javascript'>{parent.msgbox('Office name must be at least three(3) characters long. Please review.', 'red');}</script>");
            $GoValidate = false;
        }

        if (strlen(trim($_REQUEST["OCode"])) < 2) {
            echo ("<script type='text/javascript'>{parent.msgbox('You must enter an office code of at least two(2) characters to proceed.', 'red');}</script>");
            $GoValidate = false;
        }
        // var_dump('here1');
        if ($GoValidate == true) {
            // var_dump('here2');
            if (ValidateUpdDel("SELECT Status from BrhMasters WHERE [HashKey] = '" . $HashKey . "'") == true) {
                // var_dump('here3');
                $Script = "Update [BrhMasters] set [OName]='" . ECh($_REQUEST["OName"]) . "',
                    [TaxState]='" . ECh($_REQUEST["TaxState"]) . "',
                    [OCode]='" . ECh($_REQUEST["OCode"]) . "',
                    [OType]='" . ECh($_REQUEST["OType"]) . "',
                    [OParent]='" . ECh($_REQUEST["OParent"]) . "',
                    [OContact]='" . ECh($_REQUEST["OContact"]) . "',
                    [OContact2]='" . ECh($_REQUEST["OContact2"]) . "',
                    [OContact3]='" . ECh($_REQUEST["OContact3"]) . "',
                    [OContact4]='" . ECh($_REQUEST["OContact4"]) . "',
                    [OAddress]='" . ECh($_REQUEST["OAddress"]) . "',[OPhone]='" . ECh($_REQUEST["OPhone"]) . "',[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $HashKey . "'";
                // print_r($Script);
                ScriptRunnerUD($Script, "Inst");

                AuditLog("INSERT", "Office details updated for [" . ECh($_REQUEST["OName"]) . "]");
                echo ("<script type='text/javascript'>{ parent.msgbox('Office details updated successfully.', 'green'); }</script>");
            }
        }
    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("BRANCH CREATION" . "V") != true) {include '../main/NoAccess.php';exit;}
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && $_REQUEST["DelBox" . $i] != "") {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_Edit = "select * from [BrhMasters] where HashKey='" . $EditID . "'";
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("BRANCH CREATION" . "T") != true) {include '../main/NoAccess.php';exit;}
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && $_REQUEST["DelBox" . $i] != "") {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_ID = "SELECT OName from BrhMasters where HashKey='" . $EditID . "'";

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from BrhMasters WHERE [HashKey] = '" . $EditID . "'") == true) {
                    //Mark the record as Authorized
                    $Script = "Update BrhMasters
                        SET [Status] = 'A'
                        ,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
                        ,[AuthDate] = GetDate()
                        WHERE [HashKey] = '" . ECh($_REQUEST["DelBox" . $i]) . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("AUTHORIZE", "Office details authorized for office " . ScriptRunner($Script_ID, "OName"));

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected office details authorized successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("BRANCH CREATION" . "T") != true) {include '../main/NoAccess.php';exit;}
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && $_REQUEST["DelBox" . $i] != "") {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_ID = "SELECT OName from BrhMasters where HashKey='" . $EditID . "'";

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from BrhMasters WHERE [HashKey] = '" . $EditID . "'") != true) {
                    //Mark the record as Authorized
                    $Script = "Update BrhMasters
                        SET [Status] = 'U'
                        ,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
                        ,[AuthDate] = GetDate()
                        WHERE [HashKey] = '" . ECh($_REQUEST["DelBox" . $i]) . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    AuditLog("UNAUTHORIZE", "Office details unauthorized for office " . ScriptRunner($Script_ID, "OName"));

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected office details unauthorized successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("BRANCH CREATION" . "D") != true) {include '../main/NoAccess.php';exit;}
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && $_REQUEST["DelBox" . $i] != "") {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $Script_ID = "SELECT OName from BrhMasters where HashKey='" . $EditID . "'";

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from BrhMasters WHERE [HashKey] = '" . $EditID . "'") == true) {
                    //Mark the record as Deleted
                    $Script = "Update BrhMasters
                        SET [Status] = 'D'
                        ,[DeletedBy] =  '" . $_SESSION["StkTck" . "UName"] . "'
                        ,[DeletedDate] = GetDate()
                        WHERE [HashKey] = '" . ECh($_REQUEST["DelBox" . $i]) . "'";
                    ScriptRunnerUD($Script, "Delete");
                    AuditLog("DELETE", "Branch account deleted for branch [" . ScriptRunner($Script_ID, "OName") . "]");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected branch account(s) deleted successfully.', 'green'); }</script>");
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select * from [BrhMasters] where HashKey='--'";
    }
}
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="../css/style_main.css" rel="stylesheet" type="text/css">
	<script>
	  $(function() {
	    $( document ).tooltip();
	  });
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
     <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
     <style>
            .select2-container--default .select2-selection--multiple .select2-selection__choice {
          background-color: #6dab69;
      }

     </style>
</head>

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
						<h4>Create/Edit  Offices</h4>

					</div>
					<div class="col-md-4">
						<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Clear" />
					</div>
				</div>
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;">Office Name:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php

if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    echo '<input type="text" name="OName" class="form-control" id="OName" value="' . $_REQUEST["OName"] . '"/>';
} elseif ($EditID != '' || $EditID != '--') {
    echo '<input type="text" name="OName" class="form-control" id="OName" value="' . ScriptRunner($Script_Edit, "OName") . '" />';

} elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {echo '<input type="text" name="OName" class="form-control" id="OName" value="" />';} else {if (isset($_REQUEST["OName"])) {
    echo '<input type="text" name="OName" class="form-control" id="OName" value="' . $_REQUEST["OName"] . '"/>';
} else {
    echo '<input type="text" name="OName" class="form-control" id="OName" value=""/>';
}
}
?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Office Type  <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OType" class="form-control" id="OType" >
						            <option value="--" selected="selected">--</option>
						            <?php
if (isset($_REQUEST['OType']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OType'];
} elseif ($EditID != "" && $EditID != "--") {$SelID = ScriptRunner($Script_Edit, "OType");} else { $SelID = "";}

$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='Office Type' and Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['Val1']) {echo '<option selected value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';} else {echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';}
}
include '../login/dbClose2.php';
?>
						        </select>
								<span class="input-group-btn">
									<?php
$imgID = "MastersNationality";
if ($ModMaster == true) {
    $imgPath = "fa fa-plus";
    $Titl = "Add a new office type eg. Head, Corporate, Regional";
    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Office+Type&LDb=Single&amp;Elmt=Office+Type',320,400,'No')";
} else {
    $imgPath = "fa fa-info";
    $Titl = "Select an office type";
    $OnClk = "";
}

echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
?>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Reporting Office:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OParent" class="form-control" id="OParent">
							        <?php
if (isset($_REQUEST['OParent']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OParent'];
} elseif ($EditID != '' && $EditID != '--') {$SelID = ScriptRunner($Script_Edit, "OParent");} else { $SelID = "";}

echo '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from BrhMasters where Status in ('A') order by OName Asc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['HashKey']) {echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';} else {echo '<option value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';}
}
echo $AllEmployee;
include '../login/dbClose2.php';
?>
							   	</select>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Contact Officer:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OContact" class="form-control" id="OContact">
							        <?php
if (isset($_REQUEST['OContact']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OContact'];
} elseif ($EditID != '' && $EditID != '--') {
    $SelID = ScriptRunner($Script_Edit, "OContact");
} else { $SelID = "";}

echo '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");
include '../login/dbOpen2.php';
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['HashKey']) {$AllEmployee = $AllEmployee . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';} else { $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';}
}
echo $AllEmployee;
include '../login/dbClose2.php';
?>
						      	</select>
							</div>
						</div>

            						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Contact Officer 2:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OContact2" class="form-control" id="OContact2">
							        <?php
if (isset($_REQUEST['OContact2']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OContact2'];
} elseif ($EditID != '' && $EditID != '--') {
    $SelID = ScriptRunner($Script_Edit, "OContact2");
} else { $SelID = "";}

echo '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");
include '../login/dbOpen2.php';
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['HashKey']) {$AllEmployee2 = $AllEmployee2 . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';} else { $AllEmployee2 = $AllEmployee2 . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';}
}
echo $AllEmployee2;
include '../login/dbClose2.php';
?>
						      	</select>
							</div>
						</div>

						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Contact Officer 3:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OContact3" class="form-control" id="OContact3">
							        <?php
if (isset($_REQUEST['OContact3']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OContact3'];
} elseif ($EditID != '' && $EditID != '--') {
    $SelID = ScriptRunner($Script_Edit, "OContact3");
} else { $SelID = "";}

echo '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");
include '../login/dbOpen2.php';
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['HashKey']) {$AllEmployee3 = $AllEmployee3 . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';} else { $AllEmployee3 = $AllEmployee3 . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';}
}
echo $AllEmployee3;
include '../login/dbClose2.php';
?>
						      	</select>
							</div>
						</div>

						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Contact Officer 4:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="OContact4" class="form-control" id="OContact4">
							        <?php
if (isset($_REQUEST['OContact4']) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    $SelID = $_REQUEST['OContact4'];
} elseif ($EditID != '' && $EditID != '--') {
    $SelID = ScriptRunner($Script_Edit, "OContact4");
} else { $SelID = "";}

echo '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");
include '../login/dbOpen2.php';
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row2['HashKey']) {$AllEmployee4 = $AllEmployee4 . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';} else { $AllEmployee4 = $AllEmployee4 . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';}
}
echo $AllEmployee4;
include '../login/dbClose2.php';
?>
						      	</select>
							</div>
						</div>







						<div class="form-group row">

										    <label  class="col-sm-4 col-form-label">Office State:</label>
											<!-- <div id="statediv2" name="statediv2"> -->
												<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {echo ' background: #e9e9e9; padding: 5px ';}?>">
													<select name="TaxState" id="TaxState" class="form-control">
														<option value="--" selected="selected">--</option>
														<?php
if (isset($_REQUEST["TaxState"])) {
    $SelID = $_REQUEST["TaxState"];
} else {
    $SelID = "";
}
if (isset($EditID) && strlen($EditID) == 32) {$SelID = ScriptRunner($Script_Edit, "TaxState");}
$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='State Of Origin' and Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';

while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if (($_REQUEST["SubmitTrans"] === 'Clear')) {

        echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';

    } else if ($SelID == $row2['Val1']) {
        echo '<option selected value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
    } else {
        echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
    }
}
include '../login/dbClose2.php';
?>
													</select>
													<span class="input-group-btn ">
														<?php
$imgID = "MastersSOO";
if ($ModMaster == true) {
    $imgPath = "fa fa-plus";
    $Titl = "Add a new employee tax state eg. Zamfara, Oyo, Enugu";
    $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?PRent=Nationality&GroupName=State%20Of%20Origin&LDb=Single&amp;Elmt=State%20Of%20Origin',320,400,'No')";
} else {
    $imgPath = "fa fa-info";
    $Titl = "Select an employee tax state";
    $OnClk = "";
}
echo '<button type="button" id="' . $imgID . '"  title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
?>
													</span>
												</div>
											<!-- </div> -->
										</div>










					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;">Office Code:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
if (isset($_REQUEST["OCode"]) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    echo '<input type="text" name="OCode" maxlength="6" class="form-control" id="OCode" value="' . $_REQUEST["OCode"] . '"/>';

} elseif ($EditID != '' || $EditID != '--') {
    echo '<input type="text" name="OCode" maxlength="6" class="form-control" id="OCode" value="' . ScriptRunner($Script_Edit, "OCode") . '" />';
} elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {echo '<input type="text" name="OCode" maxlength="6" class="form-control" id="OCode" value="" />';} else {if (isset($_REQUEST["OCode"])) {
    echo '<input type="text" name="OCode" maxlength="6" class="form-control" id="OCode" value="' . $_REQUEST["OCode"] . '"/>';
} else {
    echo '<input type="text" name="OCode" maxlength="6" class="form-control" id="OCode" value=""/>';
}
}
?>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Office Address:</label>
							<div class="col-sm-8" style=" padding-right: 35px">
								<?php
if (isset($_REQUEST["OAddress"]) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    echo '<textarea rows="4" name="OAddress" size="16" maxlength="150" class="form-control" id="OAddress" />' . $_REQUEST["OAddress"] . '</textarea>';

} elseif ($EditID != "" || $EditID != '--') {echo '<textarea rows="4" name="OAddress" size="16" maxlength="150" class="form-control" id="OAddress"/>' . ScriptRunner($Script_Edit, "OAddress") . '</textarea>';} elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {echo '<textarea rows="4" name="OAddress" size="16" maxlength="150" class="form-control" id="OAddress" /></textarea>';} else {if (isset($_REQUEST["OAddress"])) {
    echo '<textarea rows="4" name="OAddress" size="16" maxlength="150" class="form-control" id="OAddress" />' . $_REQUEST["OAddress"] . '</textarea>';
} else {
    echo '<textarea rows="4" name="OAddress" size="16" maxlength="150" class="form-control" id="OAddress" /></textarea>';
}
}
?>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;">Office Phone:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
if (isset($_REQUEST["OPhone"]) && $_REQUEST["SubmitTrans"] === 'Create Office') {
    echo '<input type="text" name="OPhone" maxlength="14" class="form-control" id="OPhone" value="' . $_REQUEST["OPhone"] . '"/>';

} elseif ($EditID != '' || $EditID != '--') {
    echo '<input type="text" name="OPhone" maxlength="14" class="form-control" id="OPhone" value="' . ScriptRunner($Script_Edit, "OPhone") . '" />';
} elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {echo '<input type="text" name="OPhone" maxlength="14" class="form-control" id="OPhone" value="" />';} else {if (isset($_REQUEST["OPhone"])) {
    echo '<input type="text" name="OPhone" maxlength="14" class="form-control" id="OPhone" value="' . $_REQUEST["OPhone"] . '"/>';
} else {
    echo '<input type="text" name="OPhone" maxlength="14" class="form-control" id="OPhone" value=""/>';
}
}
?>








							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">

						<div class="row">
							<div class="col-md-6"> </div>
							<div class="col-md-2">
							</div>
							<div class="col-md-4 pull-right">
								<?php
if (ValidateURths("BRANCH CREATION" . "A") == true) {
    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] === 'Create Office') {
        echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Office" />';

    } elseif ( /*$_REQUEST["PgTy"] == "AddIndTrans" &&*/($EditID == "" || $EditID == '--')) {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Office" />';} elseif ($EditID != "" || $EditID > 0) {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update Office" />';
        echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';} else {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Office" />';}
}
?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="col-md-12">
				    	<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE"/>
				    	<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE"/>
				    </div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    print "<h4>UNAUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from BrhMasters where Status in('N','U') order by OName asc");
} else {
    print "<h4>AUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from BrhMasters where Status = 'A' order by OName asc");
}
?>
					</div>
					<div class="col-md-12">
						<table width="100%" class="table table-responsive table-bordered table-striped">
							<thead>
								<tr>
									<th valign="middle" class="smallText" scope="col"><span class="TinyTextTight">
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
										<label for="DelChk_All"></label>
									</span></th>
									<th valign="middle"  scope="col"><span class="TinyTextTightBold">Created On</span></th>
									<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Office Name</span></th>
									<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Office Type</span></th>
									<th width="233" align="left" valign="middle"  scope="col">Reporting Office</th>
									<th width="195" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Contact Employee</span></th>
									<th width="195" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Contact Employee 2</span></th>
									<th width="195" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Contact Employee 3</span></th>
									<th width="195" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Contact Employee 4</span></th>
									<th width="99" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Phone</span></th>
									<th width="99" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Office State</span></th>
								</tr>
							</thead>
							<tbody>
								<?php
$Del = 0;
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    ?>
								<tr>
									<td width="21" height="27" valign="middle" scope="col" class="TinyTextTight">
										<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
										<label for="<?php echo ("DelBox" . $Del); ?>"></label>
									</td>
									<td width="100" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
									<td width="436" align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['OName'])); ?></td>
									<td width="175" align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['OType'])); ?></td>
									<td valign="middle" align="left" class="TinyText" scope="col">&nbsp;<?php
if (strlen($row2['OParent']) == 32) {
        $Script = "Select OName from [BrhMasters] where Status<>'D' and HashKey='" . $row2['OParent'] . "'";
        echo ScriptRunner($Script, "OName");
    }

    ?></td>
									<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php
if (strlen($row2['OContact']) == 32) {
        $Script = "Select (SName+' '+FName+' ['+Convert(varchar(24),EmpID)+']') Nm from [EmpTbl] where Status<>'D' and HashKey='" . $row2['OContact'] . "'";
        echo ScriptRunner($Script, "Nm");
    }
    ?></td>
									<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php
if (strlen($row2['OContact2']) == 32) {
        $Script = "Select (SName+' '+FName+' ['+Convert(varchar(24),EmpID)+']') Nm from [EmpTbl] where Status<>'D' and HashKey='" . $row2['OContact2'] . "'";
        echo ScriptRunner($Script, "Nm");
    }
    ?></td>
									<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php
if (strlen($row2['OContact3']) == 32) {
        $Script = "Select (SName+' '+FName+' ['+Convert(varchar(24),EmpID)+']') Nm from [EmpTbl] where Status<>'D' and HashKey='" . $row2['OContact3'] . "'";
        echo ScriptRunner($Script, "Nm");
    }
    ?></td>
									<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php
if (strlen($row2['OContact4']) == 32) {
        $Script = "Select (SName+' '+FName+' ['+Convert(varchar(24),EmpID)+']') Nm from [EmpTbl] where Status<>'D' and HashKey='" . $row2['OContact4'] . "'";
        echo ScriptRunner($Script, "Nm");
    }
    ?></td>
									<td width="99" align="left" valign="middle" scope="col" class="TinyText">&nbsp;<?php echo (trim($row2['OPhone'])); ?></td>
									<td width="99" align="left" valign="middle" scope="col" class="TinyText">&nbsp;<?php echo (trim($row2['TaxState'])); ?></td>
								</tr>
								<?php }
//include '../login/dbClose2.php';
?>
							</tbody>
					    </table>
					</div>
					<div class="col-md-4"></div>
					<div class="col-md-8 text-right">
						<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
							      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
								  <input name="FAction" id="FAction" type="hidden" />
								  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

//Check if any record was spolled at all before enabling buttons

if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    if (ValidateURths("BRANCH CREATION" . "D") == true) {
        //Check if user has Delete rights
        $but_HasRth = ("BRANCH CREATION" . "D");
        $but_Type = 'Delete';include '../main/buttons.php';
        //print ">".ValidateURths("BRANCH CREATION"."D");
    }

    if (ValidateURths("BRANCH CREATION" . "V") == true) {
        //Check if user has Add/Edit rights
        $but_HasRth = ("BRANCH CREATION" . "V");
        $but_Type = 'View';include '../main/buttons.php';
    }

    if (ValidateURths("BRANCH CREATION" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("BRANCH CREATION" . "T");
        $but_Type = 'Authorize';include '../main/buttons.php';
    }
} else {
    /*
    if (ValidateURths("BRANCH CREATION"."D")==true){
    //Check if user has Delete rights
    $but_HasRth=("BRANCH CREATION".""); $but_Type = 'Delete'; include '../main/buttons.php';
    }
    if (ValidateURths("BRANCH CREATION"."V")==true){
    //Check if user has Add/Edit rights
    $but_HasRth=("BRANCH CREATION".""); $but_Type = 'View'; include '../main/buttons.php';
    }
     */

    if (ValidateURths("BRANCH CREATION" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("BRANCH CREATION" . "T");
        $but_Type = 'Unauthorize';include '../main/buttons.php';
    }
}
?>
					</div>
				</div>
			</div>
		</div>
	</form>



</body>
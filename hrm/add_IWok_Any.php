<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

if (ValidateURths("EMPLOYEE" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$ModMaster = ValidateURths("HR MODULE MASTERS" . "A");

$GoValidate = true;
if (!isset($EditID)) {
    $EditID = "";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "";
}
if (!isset($RetVal)) {
    $RetVal = "";
} //Don't remove
if (!isset($Err)) {
    $Err = "";
} //Don't remove
if (!isset($AllEmployee)) {
    $AllEmployee = "";
} //Don't remove
if (!isset($SelID)) {
    $SelID = "";
}
$AuthRec = "";
$Script_Menu = "";

if (isset($_REQUEST["StaffID"]) && strlen(trim($_REQUEST["StaffID"])) != 32) {
    echo '<link href="../css/style_main.css" rel="stylesheet" type="text/css">
		<div align="center" class="TinyTextFail">You must first save employee details before adding work experience records</div>';
    $GoValidate == false;
    exit;
}

//Get Record HashKey for the specific row to be edited from the Employee form
if (isset($_REQUEST["HashID"]) && strlen(trim($_REQUEST["HashID"])) == 32) //HashID
{
    $EditID = $_REQUEST["HashID"];
}

if (isset($_REQUEST["HashKey"]) && strlen(trim($_REQUEST["HashKey"])) == 32) {
    $EditID = $_REQUEST["HashKey"];
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Clear") {
        $EditID = "--";
    }

    if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Add") {
        //Run Validation to see if form data is clear for Add/Update
        include 'add_IWok_val.php';

        //Set Remind Employee to true.
        if (isset($_REQUEST["RemindEmp"])) {
            $RemindEmp_ = 1;
        } else {
            $RemindEmp_ = 0;
        }

        if ($GoValidate == true) {
            $Script = "Select SetValue from Settings where Setting='CompName'";
            $ItemName = ScriptRunner($Script, "SetValue");

            /* If a document is attached to the updated record then call the file upload function */
            if ($_FILES["dmsupd"]["name"] != "") {
                include '../dms/stf_upload_file.php';
                $Script = "Select (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm from [EmpTbl] where HashKey='" . ECh($_REQUEST["StaffID"]) . "'";

                //Upload file to DMS
                $RetVal = UploadDoc("Add", "dmsupd", "", "", ScriptRunner($Script, "Nm") . " - " . $ItemName . " " . ECh($_REQUEST["ItemPos"]), "", "H", "");
                if (strlen($RetVal) == 32) {
                    $Err = "";
                } else {
                    $Err = "<font color=red> Unable to attach file.</font>";
                }
            }

            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "EmpWork" . $_SESSION["StkTck" . "UName"];
            $HashKey = md5($UniqueKey);

            if (strlen($ItemExpiry_V) < 11) {
                $Script = "INSERT INTO [EmpDtl]			([StaffID],[ItemCode],[ItemStatus],[ItemType],[ItemName],[RemindEmp],[ItemLevel],[ItemPos],[ItemMgr],[ItemStart],[ItemEnd],[ItemDoc],[ItemDocHID],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . ECh($_REQUEST["StaffID"]) . "','EMP','" . ECh($_REQUEST["ItemStatus"]) . "','" . ECh($_REQUEST["ItemType"]) . "','" . ECh($ItemName) . "'," . $RemindEmp_ . ",'" . ECh($_REQUEST["ItemDegree"]) . "','" . ECh($_REQUEST["ItemPos"]) . "','" . ECh($_REQUEST["ItemMgr"]) . "'," . ($ItemStart_V) . "," . ($ItemEnd_V) . ",'','" . $RetVal . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
                //echo $Script;
            } else {
                $Script = "INSERT INTO [EmpDtl]			([StaffID],[ItemCode],[ItemType],[ItemName],[RemindEmp],[ItemLevel],[ItemPos],[ItemMgr],[ItemStart],[ItemEnd],[ItemExpiry],[ItemDoc],[ItemDocHID],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . ECh($_REQUEST["StaffID"]) . "','EMP','" . ECh($_REQUEST["ItemType"]) . "','" . ECh($ItemName) . "'," . $RemindEmp_ . ",'" . ECh($_REQUEST["ItemDegree"]) . "','" . ECh($_REQUEST["ItemPos"]) . "','" . ECh($_REQUEST["ItemMgr"]) . "'," . ($ItemStart_V) . "," . ($ItemEnd_V) . "," . $ItemExpiry_V . ",'','" . $RetVal . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
                //echo $Script;
            }
            ScriptRunnerUD($Script, "New_Item");
            if (isset($_REQUEST['ItemType'])) {
                $Script_Edit_D = "Select * from [EmpDtl] where [StaffID]='{$_REQUEST["StaffID"]}' and [Status] in('N','U') order by [ID] desc";
                $itemty = ScriptRunner($Script_Edit_D, "ItemType");

                $Script_D = "Update EmpTbl set [EmpType]='{$itemty}' where HashKey='" . ECh($_REQUEST["StaffID"]) . "'";
                ScriptRunnerUD($Script_D, "New_Item");
            }

            echo ("<script type='text/javascript'>{ parent.parent.msgbox(Employment record added successfully', 'green'); }</script>");
            AuditLog("INSERT", "New employee employment record [" . ECh($_REQUEST["ItemType"]) . "] - " . ECh($ItemName) . " added successfully");
            echo ("<script type='text/javascript'>{	parent.refresh_frame('frame_employee_doc');	}</script>");
        }
    }

    if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Update") {
        //Run Validation to see if form data is clear for Add/Update
        include 'add_IWok_val.php';

        if ($GoValidate == true) {
            $Script = "Select SetValue from Settings where Setting='CompName'";
            $ItemName = ScriptRunner($Script, "SetValue");

            //Set Remind Employee to true.
            if (isset($_REQUEST["RemindEmp"])) {
                $RemindEmp_ = 1;
            } else {
                $RemindEmp_ = 0;
            }

            /* Upload file if there is any selected.
            If a document is attached to the updated record then call the file upload function */
            if ($_FILES["dmsupd"]["name"] != "") {
                include '../dms/stf_upload_file.php';

                //Delete file from DMS
                if (strlen($_REQUEST["DocHID"]) == 32) {
                    UploadDoc("Del", "", "", "", "", "", "", $_REQUEST["DocHID"]);
                } //Delete physical file, mark record as deleted

                $Script = "Select (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm from [EmpTbl] where HashKey='" . $_REQUEST["StaffID"] . "'"; //GEt Employee Name 4 DOC Title
                //Upload file to DMS
                $RetVal = UploadDoc("Add", "dmsupd", "", "", ScriptRunner($Script, "Nm") . " - " . $ItemName . " " . ECh($_REQUEST["ItemPos"]), "", "H", "");
                if (strlen($RetVal) == 32) {
                    $Err = "";
                } else {
                    $Err = "<font color=red> Unable to attach file.</font>";
                }
            }

            $Script = "Update EmpDtl set ItemName='" . ECh($ItemName) . "',ItemLevel='" . ECh($_REQUEST["ItemDegree"]) . "', ItemPos='" . ECh($_REQUEST["ItemPos"]) . "',ItemMgr='" . ECh($_REQUEST["ItemMgr"]) . "', ItemStatus='" . ECh($_REQUEST["ItemStatus"]) . "', ItemType='" . ECh($_REQUEST["ItemType"]) . "',RemindEmp=" . $RemindEmp_ . ", [ItemStart]=" . $ItemStart_V . ",[ItemEnd]=" . $ItemEnd_V . ",[ItemExpiry]=" . $ItemExpiry_V . ",[ItemDocHID]='" . $RetVal . "', UpdatedBy = '" . ECh($_SESSION["StkTck" . "UName"]) . "', UpdatedDate = GetDate(), Status='U' where [StaffID]='" . ECh(trim($_REQUEST["StaffID"])) . "' and HashKey = '" . ECh(trim($_REQUEST["HashKey"])) . "'";
            //echo $Script;
            ScriptRunnerUD($Script, "Update_EmpDtl");
            if (isset($_REQUEST['ItemType'])) {
                $Script_Edit_D = "Select * from [EmpDtl] where [StaffID]='{$_REQUEST["StaffID"]}' and [Status] in('N','U') order by [ID] desc";
                $itemty = ScriptRunner($Script_Edit_D, "ItemType");

                $Script_D = "Update EmpTbl set [EmpType]='{$itemty}' where HashKey='" . ECh($_REQUEST["StaffID"]) . "'";
                ScriptRunnerUD($Script_D, "New_Item");
            }

            AuditLog("UPDATE", "Employment record updated for [" . ECh($_REQUEST["ItemType"]) . "] - " . ECh($ItemName));
            echo ("<script type='text/javascript'>{ parent.parent.msgbox('Employee employment record updated successfully', 'green'); }</script>");
            echo ("<script type='text/javascript'>{	parent.refresh_frame('frame_employee_doc');	}</script>");

            $smsmsg = "Employee employment record updated successfully.";
        }
    }

    if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Delete" && isset($_REQUEST["HashKey"]) && isset($_REQUEST["StaffID"])) {
        $Script_Edit = "select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm from [EmpTbl] where HashKey='" . ECh(trim($_REQUEST["StaffID"])) . "'";
        if (ValidateUpdDel("SELECT Status from [EmpDtl] where [StaffID]='" . ECh(trim($_REQUEST["StaffID"])) . "' and HashKey = '" . ECh(trim($_REQUEST["HashKey"])) . "'") == true) {
            $Script = "Update EmpDtl set Status='D', DeletedBy = '" . ECh($_SESSION["StkTck" . "UName"]) . "', DeletedDate = GetDate() where[StaffID]='" . ECh(trim($_REQUEST["StaffID"])) . "' and HashKey = '" . ECh(trim($_REQUEST["HashKey"])) . "'";
            ScriptRunnerUD($Script, "Delete_EmpRec");

            // $Script_D = "Update EmpTbl set [EmpType]='--' where HashKey='" . ECh($_REQUEST["StaffID"]) . "'";
            // ScriptRunnerUD($Script_D, "New_Item");

            //Delete file from DMS
            if (strlen($_REQUEST["DocHID"]) == 32) {
                include 'stf_upload_file.php';
                UploadDoc("Del", "", "", "", "", "", "", ECh($_REQUEST["DocHID"]));
            } //Delete physical file, mark record as deleted

            AuditLog("DELETE", "Employee experience record " . ECh($_REQUEST["ItemName"]) . " deleted for employee " . ScriptRunner($Script_Edit, "Nm"));
            echo ("<script type='text/javascript'>{ parent.parent.msgbox('Employment record deleted successfully', 'green'); }</script>");
            echo ("<script type='text/javascript'>{	parent.refresh_frame('frame_employee_doc');	}</script>");
            $EditID = "--";
        }
    }
}
if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
    for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
        if (isset($_REQUEST["DelBox" . $i])) {
            $EditID = $_REQUEST["DelBox" . $i];
            $HashKey = $EditID;
            if (strlen(trim($EditID)) == 32) {
                $Script = "UPDATE [EmpDtl]
					SET [DeletedBy] =  '" . ECh($_SESSION["StkTck" . "UName"]) . "'
					  ,[DeletedDate] = GetDate()
					  ,[Status] = 'D'
					WHERE [HashKey] = '" . ECh($EditID) . "'";
                ScriptRunnerUD($Script, "Delete");

                //Delete file from DMS
                $Script = "Select * from EmpDtl where HashKey='" . ECh($EditID) . "'";
                $DelDocID = ScriptRunner($Script, "ItemDocHID");
                if (isset($DelDocID) && strlen($DelDocID) == 32) {
                    include '../dms/stf_upload_file.php';
                    UploadDoc("Del", "", "", "", "", "", "", $DelDocID); //Delete physical file, mark record as deleted
                }

                AuditLog("DELETE", "Employee educational record [" . ScriptRunner($Script, "ItemName") . "] deleted from employee " . ECh($_REQUEST["StaffID"]));
                echo ("<script type='text/javascript'>{parent.msgbox('Selected employee educational record(s) deleted successfully.','green'); }</script>");
                echo ("<script type='text/javascript'>{	parent.refresh_frame('frame_employee_doc');	}</script>");
                $EditID = "--";
            }
        }
    }
    $EditID = $HashKey;
}
?>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="../css/style_main.css" rel="stylesheet" type="text/css">
    <script>
        $(function() {
            $("#ItemStart").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1D",
                dateFormat: 'dd M yy',
                yearRange: "-75:+75"
            })
            $("#ItemEnd").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-80Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy',
                yearRange: "-75:+75"
            })
            $("#ItemExpiry").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-80Y",
                maxDate: "+2Y",
                dateFormat: 'dd M yy',
                yearRange: "-75:+75"
            })
        });
    </script>

    <script>
        $(function() {
            $(document).tooltip();
        });
    </script>


    <script>
        $(function() {
            //	alert (parent.Department.value);

            //	$("#SOO").change(function() {
            //		var Pt = $("#SOO").val();
            var Pt = parent.Department.value;
            var Replmt = Pt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            var Replmt = Replmt.replace(" ", "+");
            $("#ItemPos").load("../main/getCh.php?Choice=Designation&Parent=" + Replmt);
            //	});
        });
    </script>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

    <!-- Select 2-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">


    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
</head>

<body oncontextmenu="return false;">
    <!--onLoad="getChildren('statediv','../main/getCh.php?ChNm=ItemPos&ItmNm=Designation&Parent='+parent.Department.value)" -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
                    <?php
                    //WORKING PROFILE================================================
                    $Del = 0;
                    $Script = "SELECT count(*) as Mct from EmpDtl where ItemCode='EMP' and StaffID='" . ECh($_REQUEST["StaffID"]) . "' and Status <> 'D'";
                    if (ScriptRunner($Script, "Mct") > 0) {
                        echo '<div class="">';
                        echo '<table class="table table-bordered table-responsive">';
                        echo '<thead>';
                        echo '<tr>
								    		<th >Emp Type</th>
								    		<th >Emp Level</th>
								    		<th>Organisation</th>
								    		<th>Position</th>
								    		<th>Manager</th>
								    		<th>Start Date</th>
								    		<th>End Date</th>
									    </tr>
									</thead>';
                        $dbOpen2 = ("SELECT *, Convert(Varchar(11),ItemStart,106) as SDt, Convert(Varchar(11),ItemEnd,106) as EDt from EmpDtl where StaffID='" . $_REQUEST["StaffID"] . "' and ItemCode='EMP' and Status <> 'D' order by ID desc");

                        include '../login/dbOpen2.php';
                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                            $Del = $Del + 1;
                            $val = trim($row2['ItemLevel']);
                            $sctp = "SELECT SetValue2 FROM Settings where Setting='JobRoleLevel' and SetValue ='$val'";
                            $emp_level =  ScriptRunner($sctp, "SetValue2");
                            echo '
								        	<tr>
			            						<td>
			            							<div class="row">
			            								<div class="col-2">
			            									<div class="checkbox">
																<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '"); />
			            										<label for="' . ("DelBox" . $Del) . '"></label>
			            									</div>
			            								</div>
			            								<div class="col-2">'; ?>
                            <span title="Edit Record" name="EditWok<?php echo $Del; ?>" id="EditWok<?php echo $Del; ?>" style="font-size: 19px; margin-top: 4%; cursor: pointer;" class="fa fa-pencil" onClick=parent.redirect_frame("frame_employee_emp","add_IWok_Any.php?PgTy=Create&StaffID=<?php echo $_REQUEST["StaffID"] . '&HashID=' . trim($row2['HashKey']); ?>")></span>
                            <?php echo '
			            								</div>
			            								<div class="col-8">
			            								  ' . (trim($row2['ItemType'])) . '
			            								</div>
			            							</div>
			            						</td>
			            						<td>' .

                                $emp_level



                                . '</td>
			            						<td>' . (trim($row2['ItemName'])) . '</td>
			            						<td>' . (trim($row2['ItemPos'])) . '</td>
			            						<td>';
                            $Script = "Select count(*) Ct from EmpTbl where HashKey='" . $row2['ItemMgr'] . "'";
                            if (ScriptRunner($Script, "Ct") == 1) {
                                $Script = "Select (SName+' '+FName) Nm from EmpTbl where HashKey='" . $row2['ItemMgr'] . "'";
                                echo ScriptRunner($Script, "Nm");
                            }
                            echo '</td>
												<td>' . (trim($row2['SDt'])) . '</td>
												<td>
												  	<div class="row">
												    	<div  class="col-8">
												        	' . (trim($row2['EDt'])) . '
												      	</div>
												      	<div class="col-2">';
                            if (strlen(trim($row2['ItemDocHID'])) == 32) {
                                echo '<span style="font-size: 19px; margin-top: 4%; cursor: pointer;" title="File Attached" class="fa fa-paperclip"></span>';
                            }
                            echo '
												      	</div>
												  	</div>
												</td>
											</tr>
										';
                        }
                        include '../login/dbClose2.php';
                        echo '</table>';
                        //==================================================
                        if (ValidateURths("EMPLOYEE" . "D") == true) {
                            $Script = "Select * from EmpTbl where HashKey='" . ECh($_REQUEST["StaffID"]) . "'";
                            if (ScriptRunner($Script, "Status") == 'A') { ?>
                                <!-- <input name='SubmitTrans' id='SubmitTrans' type='button' class='smallButton_disabled' value='Delete Selected' onclick=ChkConfirm('SubmitTrans','Delete') />  -->
                                <input name='SubmitTrans' id='SubmitTrans' type='button' class='btn btn-danger btn-xs pull-right' value='Delete Selected' onClick="YesNo('Delete Record', 'Delete')" />
                                <?php
                            } else {
                                if ($Del == 0) { ?>
                                    <input name='SubmitTrans' disabled id='SubmitTrans' type='button' class='btn btn-danger btn-xs pull-right' value='Delete Selected' onclick=ChkConfirm('SubmitTrans','Delete') />
                                <?php
                                } else { ?>
                                    <input name='SubmitTrans' id='SubmitTrans' type='button' class='btn btn-danger btn-xs pull-right' value='Delete Selected' onClick="YesNo('Delete Record', 'Delete')" />
                    <?php
                                }
                            }
                        }
                        echo '</div>';
                    }
                    ?>
                    <input name="DelMax" id="DelMax" type="hidden" value="<?php echo $Del; ?>" />
                    <input name="PgTy" id="PgTy" type="hidden" value="<?php echo $_REQUEST["PgTy"]; ?>" />
                    <input name='FAction' id='FAction' type='hidden'>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-12">
                <form action="#" method="post" enctype="multipart/form-data" name="Degree" target="_self" id="Degree" autocomplete="off">
                    <div class="row">
                        <div class="col-md-5">
                            <?php
                            if (isset($EditID) && isset($_REQUEST["StaffID"]) && strlen($EditID) == 32) {
                                $Script_Edit = "Select *, Convert(Varchar(11),ItemStart,106) ItS, Convert(Varchar(11),ItemEnd,106) ItE, Convert(Varchar(11),ItemExpiry,106) ItX from EmpDtl where StaffID='" . ECh($_REQUEST["StaffID"]) . "' and HashKey='" . $EditID . "'";
                            } else {
                                //echo ("<script type='text/javascript'>{ alert (parent.Department.value); }<//script>");
                                $Script_Edit = "Select *, Convert(Varchar(11),ItemStart,106) ItS, Convert(Varchar(11),ItemEnd,106) ItE, Convert(Varchar(11),ItemExpiry,106) ItX from EmpDtl where HashKey='zzz'";
                            }
                            ?>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Start Date<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <?php
                                    if ($EditID != "" || $EditID > 0) {
                                        echo '<input name="ItemStart" id="ItemStart" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "ItS") . '" maxlength="12" readonly="readonly" />';
                                    } else {
                                        if (isset($_REQUEST["ItemStart"]) && $_REQUEST["SubmitContact"] != "Clear") {
                                            echo '<input name="ItemStart" id="ItemStart" type="text" class="form-control" value="" size="16" maxlength="12" readonly="readonly" value="' . $_REQUEST["ItemStart"] . '" />';
                                        } else {
                                            echo '<input name="ItemStart" id="ItemStart" type="text" class="form-control" value="" size="16" maxlength="12" readonly="readonly" value="" />';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Employement Type<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <select class="form-control" name="ItemType" id="ItemType">
                                        <option value="--" selected="selected">--</option>
                                        <?php
                                        if (isset($_REQUEST["ItemType"])) {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Type' and Val1<>'" . $_REQUEST["ItemType"] . "') ORDER BY Val1");
                                        } else {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Type') ORDER BY Val1");
                                        }
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                        }
                                        include '../login/dbClose2.php';
                                        if (strlen($EditID) == 32) {
                                            echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "ItemType") . '">' . ScriptRunner($Script_Edit, "ItemType") . '</option>';
                                        } else {
                                            if (isset($_REQUEST["ItemType"])) {
                                                echo '<option selected=selected value="' . $_REQUEST["ItemType"] . '">' . $_REQUEST["ItemType"] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <?php
                                        $imgID = "MastersEmployementType";
                                        if ($ModMaster == true) {
                                            $imgPath = "fa fa-plus";
                                            $Titl = "Add a new employment type eg. Full Time, Contract, Intern";
                                            $OnClk = "parent.parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Employee%20Type&LDb=Single&amp;Elmt=Employee%20Type',320,400,'No')";
                                        } else {
                                            $imgPath = "fa fa-info";
                                            $Titl = "Select an employee employment type";
                                            $OnClk = "";
                                        }

                                        echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Employement Level<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <select class="form-control" name="ItemDegree" id="ItemDegree">
                                        <option value="0" selected="selected">--</option>
                                        <?php
                                        if ($EditID != '') {
                                            //$SelID=ScriptRunner($Script_Edit,"ItemDegree");
                                            $SelID = ScriptRunner($Script_Edit, "ItemLevel");
                                        } else {
                                            $SelID = "";
                                        }

                                        $dbOpen2 = ("SELECT SetValue, SetValue2 FROM Settings where Setting='JobRoleLevel' ORDER BY SetValue");
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            if (trim($row2['SetValue2']) != "") {
                                                //if (($SelID==$row2['SetValue'])|| ((isset($_REQUEST["ItemDegree"]))&& ($_REQUEST["ItemDegree"]==$row2['SetValue'])))
                                                if ($SelID == $row2['SetValue']) {
                                                    echo '<option selected value="' . $row2['SetValue'] . '">' . $row2['SetValue2'] . '</option>';
                                                } /*elseif(isset($_REQUEST["ItemDegree"])&& ($_REQUEST["ItemDegree"]==$row2['SetValue'])){
        echo '<option selected value="'.$row2['SetValue'].'">'.$row2['SetValue2'].'</option>';
        }*/ else {
                                                    echo '<option value="' . $row2['SetValue'] . '">' . $row2['SetValue2'] . '</option>';
                                                }
                                            }
                                        }
                                        include '../login/dbClose2.php';
                                        /*
    if (isset($_REQUEST["ItemDegree"])){
    $dbOpen2 = ("SELECT SetValue, SetValue2 FROM Settings where (Setting='JobRoleLevel' and SetValue<>'".$_REQUEST["ItemDegree"]."') ORDER BY SetValue");

    }else{
    $dbOpen2 = ("SELECT SetValue, SetValue2 FROM Settings where Setting='JobRoleLevel' ORDER BY SetValue");
    }
    include '../login/dbOpen2.php';
    while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
    {
    echo '<option value="' . $row2['SetValue'] . '">' . $row2['SetValue2'] . '</option>';
    }
    include '../login/dbClose2.php';
    if (strlen($EditID)== 32)
    {echo '<option selected=selected value="'.ScriptRunner($Script_Edit,"ItemDegree").'">'.ScriptRunner($Script_Edit,"ItemDegree").'</option>';}
    else
    {
    if (isset($_REQUEST["ItemDegree"]))
    {echo '<option selected=selected value="'.$_REQUEST["ItemDegree"].'">'.$_REQUEST["ItemDegree"].'</option>';}
    }
     */
                                        ?>
                                    </select>
                                    <span class="input-group-btn ">
                                        <button type="button" id="MastersEmployeeLevel" class="btn btn-default" title="Select an employee level"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Designation<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <select class="form-control" name="ItemPos">
                                        <option value="--" selected="selected">--</option>
                                        <?php
                                        if (isset($_REQUEST["ItemPos"])) {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Designation' and Val1<>'" . $_REQUEST["ItemPos"] . "') and Status not in('D') ORDER BY Val1");
                                        } else {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Designation') and Status not in('D') ORDER BY Val1");
                                        }
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                        }
                                        include '../login/dbClose2.php';
                                        if ($EditID != "") {
                                            echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "ItemPos") . '">' . ScriptRunner($Script_Edit, "ItemPos") . '</option>';
                                        } else {
                                            if (isset($_REQUEST["ItemPos"])) {
                                                echo '<option selected=selected value="' . $_REQUEST["ItemPos"] . '">' . $_REQUEST["ItemPos"] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span class="input-group-btn ">
                                        <?php
                                        $imgID = "MastersDesignation";
                                        if ($ModMaster == true) {
                                            $imgPath = "fa fa-plus";
                                            $Titl = "Add a new designation eg. Senior HSE Officer, Support Engineer";
                                            $OnClk = "parent.parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?PRent=Department&GroupName=Designation&LDb=Single&amp;Elmt=Designation',320,400,'No')";
                                        } else {
                                            $imgPath = "fa fa-info";
                                            $Titl = "Select an employee designation";
                                            $OnClk = "";
                                        }
                                        echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dmsupd" class="col-sm-4 col-form-label">Attach Document:</label>
                                <div class="col-sm-8 input-group-sm">
                                    <input type="file" name="dmsupd" id="dmsupd">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Employment Status<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <select class="form-control" name="ItemStatus" id="ItemStatus">
                                        <option value="--" selected="selected">--</option>
                                        <?php
                                        if (isset($_REQUEST["ItemStatus"])) {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='JobRoleStatus' and Val1<>'" . $_REQUEST["ItemStatus"] . "') ORDER BY Val1");
                                        } else {
                                            $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='JobRoleStatus') ORDER BY Val1");
                                        }
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                        }
                                        include '../login/dbClose2.php';
                                        if (strlen($EditID) == 32) {
                                            echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "ItemStatus") . '">' . ScriptRunner($Script_Edit, "ItemStatus") . '</option>';
                                        } else {
                                            if (isset($_REQUEST["ItemStatus"])) {
                                                echo '<option selected=selected value="' . $_REQUEST["ItemStatus"] . '">' . $_REQUEST["ItemStatus"] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <?php
                                        $imgID = "MastersEmployementType";
                                        if ($ModMaster == true) {
                                            $imgPath = "fa fa-plus";
                                            $Titl = "Add a new job role status type eg. Confirmed, Probation, Trainee";
                                            $OnClk = "parent.parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=JobRoleStatus&LDb=Single&amp;Elmt=JobRoleStatus',320,400,'No')";
                                        } else {
                                            $imgPath = "fa fa-info";
                                            $Titl = "Select an employment role status";
                                            $OnClk = "";
                                        }
                                        echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default"><i class="' . $imgPath . '"></i></button>';
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Confirmation Date<span style="color: red">*</span>:</label>
                                <div class="input-group input-group-sm col-sm-8" style="padding-left: 0px; padding-right: 35px">
                                    <span class="input-group-addon ">
                                        <?php
                                        $kk = ScriptRunner($Script_Edit, "RemindEmp");
                                        ?>
                                        <?php if (trim($kk) == 'on'): ?>
                                            <input name="RemindEmp" id="RemindEmp" type="checkbox" checked="checked" />
                                        <?php else: ?>
                                            <input name="RemindEmp" id="RemindEmp" type="checkbox" />
                                        <?php endif ?>
                                        <label for="RemindEmp" style="padding-left: 20px;height: 13px;"></label>
                                    </span>
                                    <?php if ($EditID != "" || $EditID > 0) {
                                        echo '<input name="ItemExpiry" id="ItemExpiry" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "ItX") . '" maxlength="11" readonly="readonly" />';
                                    } else {
                                        if (isset($_REQUEST["ItemExpiry"])) {
                                            echo '<input name="ItemExpiry" id="ItemExpiry" type="text" class="form-control" maxlength="12" readonly="readonly" value="' . $_REQUEST["ItemExpiry"] . '" />';
                                        } else {
                                            echo '<input name="ItemExpiry" id="ItemExpiry" type="text" class="form-control" maxlength="12" readonly="readonly" value="" />';
                                        }
                                    }
                                    ?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" itle="Check if employee should also be sent reminder of confirmation"><i class="fa fa-info"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Employee Manager<span style="color: red">*</span>:</label>
                                <div class="col-sm-8 input-group input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <select class="form-control" name="ItemMgr" id="ItemMgr">
                                        <?php
                                        echo '<option value="--" selected="selected">--</option>';
                                        if ($EditID != '' && $EditID != '--') {
                                            $kk = ScriptRunner($Script_Edit, "ItemMgr");
                                        }

                                        $dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . ECh($_REQUEST["StaffID"]) . "' order by SName Asc");
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            if ($row2['HashKey'] == $kk) {
                                                $AllEmployee = $AllEmployee . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                            } else {
                                                $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                            }
                                        }
                                        echo $AllEmployee;
                                        include '../login/dbClose2.php';
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-sm-4 col-form-label">End Date:</label>
                                <div class="col-sm-8 input-group-sm" style="padding-left: 0px; padding-right: 35px">
                                    <?php if ($EditID != "" || $EditID > 0) {
                                        echo '<input name="ItemEnd" id="ItemEnd" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "ItE") . '" maxlength="12" readonly="readonly" />';
                                    } else {
                                        if (isset($_REQUEST["ItemEnd"])) {
                                            echo '<input name="ItemEnd" id="ItemEnd" type="text" class="form-control" value="" maxlength="12" readonly="readonly" value="' . $_REQUEST["ItemEnd"] . '" />';
                                        } else {
                                            echo '<input name="ItemEnd" id="ItemEnd" type="text" class="form-control" value="" maxlength="12" readonly="readonly" value="" />';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6">
                                    <br>
                                    <?php
                                    if (ValidateURths("EMPLOYEE" . "A") == true) {

                                        $Script_Edit = "Select * from EmpTbl where HashKey='" . $_REQUEST["StaffID"] . "'";
                                        if (ScriptRunner($Script_Edit, "Status") == 'A') {

                                            if (strlen(trim($EditID)) != 32 || (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Clear")) {
                                                echo '<input value="Add" name="SubmitContact" type="submit" class="btn btn-danger btn-sm" /> &nbsp;';
                                            } else {
                                                echo '<input value="Update" name="SubmitContact" type="submit" class="btn btn-danger btn-sm" /> &nbsp;';
                                            }
                                        } else {
                                            if (strlen(trim($EditID)) != 32 || (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Clear")) {
                                                echo '<input value="Add" name="SubmitContact" type="submit" class="btn btn-danger btn-sm"/> &nbsp;';
                                            } else {
                                                echo '<input value="Update" name="SubmitContact" type="submit" class="btn btn-danger btn-sm"/> &nbsp;';
                                            }
                                        }
                                        echo '<input type="submit" class="btn btn-danger btn-sm" name="SubmitContact" value="Clear" />
								            <input type="hidden" name="HashKey" id="HashKey" value="' . $EditID . '">';
                                    }

                                    $Script = "SELECT ItemDocHID from EmpDtl where HashKey='" . $EditID . "'";
                                    echo '<input type="hidden" name="DocHID" id="DocHID" value="' . ScriptRunner($Script, "ItemDocHID") . '">
								            <input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
											<input type="hidden" name="StaffID" id="StaffID" value="' . $_REQUEST["StaffID"] . '">
								            <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
											<input name="FAction" id="FAction" type="hidden">
											<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>
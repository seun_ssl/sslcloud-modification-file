<?php
$GoValidate = true;
if (trim($_REQUEST["EmpID"]) == "" || trim($_REQUEST["EmpID"]) == "--") {
    $GoValidate = false;
    echo ("<script type='text/javascript'>{ parent.msgbox('Invalid staff ID number entered', 'red'); }</script>");
}

/*    if ($_REQUEST["DealDate"] == "")
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid deal date entered', 'red'); }</script>");
}
if ($_REQUEST["Sec"] == "")
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid security name selected', 'red'); }</script>");
}
if ($_REQUEST["CAmt"] == "")
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid purchase/sale amount entered', 'red'); }</script>");
}
if ($_REQUEST["CAmt"] != "")
{
if (!is_numeric($_REQUEST["CAmt"]))
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid purchase/sale amount entered', 'red'); }</script>");
}
}
if ($_REQUEST["CUnits"] == "")
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid purchase/sale units entered', 'red'); }</script>");
}

if ($_REQUEST["CUnits"] != "")
{
if (!is_numeric($_REQUEST["CUnits"]))
{
$GoValidate = false;
echo ("<script type='text/javascript'>{ parent.msgbox('Invalid purchase/sale units entered', 'red'); }</script>");
}
}
 */

if ($_REQUEST["DOB"] == "" || strlen($_REQUEST["DOB"]) < 8) {
    $GoValidate = false;
    //echo strlen($_REQUEST["DOB"]);
    echo ("<script type='text/javascript'>{ parent.msgbox('Please select a valid employee date of birth', 'red'); }</script>");
}

if ($_REQUEST["EmpDt"] == "" || strlen($_REQUEST["EmpDt"]) < 8) {
    $GoValidate = false;
    echo ("<script type='text/javascript'>{ parent.msgbox('Please select a valid employee date of employment', 'red'); }</script>");
}

if ($_REQUEST["EmpStatus"] == "" || $_REQUEST["EmpStatus"] == "--") {
    $GoValidate = false;
    echo ("<script type='text/javascript'>{ parent.msgbox('Please select a valid employment status', 'red'); }</script>");
}

$ExtDt = "NULL";
if (isset($_REQUEST["EmpStatus"]) && strtolower($_REQUEST["EmpStatus"]) != "active") {

    if (isset($_REQUEST["ExDt22"]) && $_REQUEST["ExDt22"] !== "") {
        $ExtDt = "'" . ECh($_REQUEST["ExDt22"]) . "'";
    } else {
        if (isset($_REQUEST["ExDt"]) && strlen($_REQUEST["ExDt"]) != 11) {
            $GoValidate = false;
            echo ("<script type='text/javascript'>{ parent.msgbox('You must select an employee status action date', 'red'); }</script>");
            $msg = '<h4><font color="red">You must select an employee status action date</font></h4>';
        } else {
            $ExtDt = "'" . ECh($_REQUEST["ExDt"]) . "'";
        }
    }
}

if ($_REQUEST["Sex"] == "" || $_REQUEST["Sex"] == "--") {
    $GoValidate = false;
    echo ("<script type='text/javascript'>{ parent.msgbox('Please select a valid employee gender', 'red'); }</script>");
}

if ($GoValidate == true) {
    /*$dtIn_array = explode("-", $_REQUEST["DOB"]);
    if ($dtIn_array[1] == "01")
    {$dtIn_array[1] = "Jan";}
    elseif ($dtIn_array[1] == "02")
    {$dtIn_array[1] = "Feb";}
    elseif ($dtIn_array[1] == "03")
    {$dtIn_array[1] = "Mar";}
    elseif ($dtIn_array[1] == "04")
    {$dtIn_array[1] = "Apr";}
    elseif ($dtIn_array[1] == "05")
    {$dtIn_array[1] = "May";}
    elseif ($dtIn_array[1] == "06")
    {$dtIn_array[1] = "Jun";}
    elseif ($dtIn_array[1] == "07")
    {$dtIn_array[1] = "Jul";}
    elseif ($dtIn_array[1] == "08")
    {$dtIn_array[1] = "Aug";}
    elseif ($dtIn_array[1] == "09")
    {$dtIn_array[1] = "Sep";}
    elseif ($dtIn_array[1] == "10")
    {$dtIn_array[1] = "Oct";}
    elseif ($dtIn_array[1] == "11")
    {$dtIn_array[1] = "Nov";}
    elseif ($dtIn_array[1] == "12")
    {$dtIn_array[1] = "Dec";}
    $DOB = ($dtIn_array[2] . "-" . $dtIn_array[1] . "-" . $dtIn_array[0]); */
    $DOB = ECh($_REQUEST["DOB"]);

    /*$dtIn_array = explode("-", $_REQUEST["EmpDt"]);
    if ($dtIn_array[1] == "01")
    {$dtIn_array[1] = "Jan";}
    elseif ($dtIn_array[1] == "02")
    {$dtIn_array[1] = "Feb";}
    elseif ($dtIn_array[1] == "03")
    {$dtIn_array[1] = "Mar";}
    elseif ($dtIn_array[1] == "04")
    {$dtIn_array[1] = "Apr";}
    elseif ($dtIn_array[1] == "05")
    {$dtIn_array[1] = "May";}
    elseif ($dtIn_array[1] == "06")
    {$dtIn_array[1] = "Jun";}
    elseif ($dtIn_array[1] == "07")
    {$dtIn_array[1] = "Jul";}
    elseif ($dtIn_array[1] == "08")
    {$dtIn_array[1] = "Aug";}
    elseif ($dtIn_array[1] == "09")
    {$dtIn_array[1] = "Sep";}
    elseif ($dtIn_array[1] == "10")
    {$dtIn_array[1] = "Oct";}
    elseif ($dtIn_array[1] == "11")
    {$dtIn_array[1] = "Nov";}
    elseif ($dtIn_array[1] == "12")
    {$dtIn_array[1] = "Dec";}
    $EmpDt = ($dtIn_array[2] . "-" . $dtIn_array[1] . "-" . $dtIn_array[0]); */
    $EmpDt = ECh($_REQUEST["EmpDt"]);
}

<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("APPROVE FORM" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$level1_query = "Select * from [formrequest] where [request_name]='" . $_POST['bookselect1'] . "' and [level1_status] = 'P' and [level1] = '" . $_SESSION['StkTckHKey'] . "'";
	// var_dump($level1_query);
	$connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
	if ($conn === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$result = sqlsrv_query($conn, $level1_query);
	if ($result === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$level1_arry = [];
	while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
		$row['level_type'] = 1;
		array_push($level1_arry, $row);
	}
	// var_dump($level1_arry);
	$level2_query = "Select * from [formrequest] where [request_name]='" . $_POST['bookselect1'] . "' and [level1_status] = 'A' and [level2_status] ='P' and [level2] = '" . $_SESSION['StkTckHKey'] . "'";
	$connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
	if ($conn === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$result = sqlsrv_query($conn, $level2_query);
	if ($result === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$level2_arry = [];
	while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
		$row['level_type'] = 2;

		array_push($level2_arry, $row);
	}
	// var_dump($level2_arry);

	$level3_query = "Select * from [formrequest] where [request_name]='" . $_POST['bookselect1'] . "' and [level1_status] = 'A' and [level2_status] ='A' and [level3_status] ='P'  and [level3] = '" . $_SESSION['StkTckHKey'] . "'";
	$connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
	if ($conn === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$result = sqlsrv_query($conn, $level3_query);
	if ($result === false) {
		die(print_r(sqlsrv_errors(), true));
	}
	$level3_arry = [];
	while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
		$row['level_type'] = 3;

		array_push($level3_arry, $row);
	}

	$all_arry = array_merge($level1_arry, $level2_arry, $level3_arry);
	// var_dump($all_arry);
	if (count($all_arry) === 0) {
		echo ("<script type='text/javascript'>{parent.msgbox('You do not have any pending {$_POST['bookselect1']} request', 'red'); }</script>");
	}
}

// var_dump($allInfo);

function getSelect($id, $group)
{

	$op = "<select id=" . $id . " onchange='amp(" . $id . "); return;' >
				<option value='' >--select--</option>
		";

	$query = "SELECT * from [bkmitems] where [select_group] ='$group' ";

	// echo $query;

	$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
	if ($conn2 === false) {
		//Redirect this to a page that says there was an issue and it is being handled by the site administrator
		//Also send an automatic mail to me saying "CANNOT CONNECT TO DATABASE - DB SERVER DOWNTIME"
		echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
		//die( print_r( sqlsrv_errors(), true));
	}
	$res2 = sqlsrv_query($conn2, $query);

	while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_BOTH)) {

		$op .= "<option value=" . $row['id'] . " >" . $row['item_name'] . "</option>";
	}

	$op .= "</select>";

	return $op;
}

?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: px 5px;
			line-height: 20px;
		}
	</style>

</head>


<body>






	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Approve/Decline Form Request
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control " name="bookselect1" id="bookselect1">
										<option value="">--</option>
										<?php
										$query = "SELECT * from [formtemplate] where  id='1'";

										$getcheck = ScriptRunnercous($query);

										$groupsarry = json_decode($getcheck['template_name'], true);

										?>

										<?php foreach ($groupsarry as $eachgroup) : ?>


											<option value="<?php echo $eachgroup['templateName']; ?>"><?php echo $eachgroup['templateName']; ?></option>


										<?php endforeach; ?>



									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">


				<?php if (isset($all_arry) && !empty($all_arry)) :
					array_multisort(array_column($all_arry, 'id'), SORT_DESC, $all_arry);



				?>
					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<div class="nav-tabs-custom" style="min-height: 400px">
										<ul class="nav nav-tabs">
											<li>
												<a class="active" href="#book" data-toggle="tab"><?= ucfirst($all_arry[0]['request_name']) ?> </a>
											</li>



										</ul>
										<div class="tab-content">

											<div class="tab-pane active" id="book">


												<table id="example1" class="table  table-bordered table-striped table-responsive tablesorter mt-5 ">
													<thead>
														<tr>
															<th>Requested by </th>
															<th>Request Subject </th>
															<th>Department </th>
															<th>Requested Date</th>
															<th>My Status</th>
															<th>Approver Level</th>
															<th>View</th>

														</tr>
													</thead>
													<?php foreach ($all_arry as $arry) : ?>
														<tr>
															<th>
																<?php $details = getUserDetailsfromHash($arry['emp_hash']);
																echo "{$details['SName']} {$details['FName']} {$details['OName']}"; ?>
															</th>

															<th>

																<?php echo $arry['request_subject'] ? $arry['request_subject'] : "N/A"  ?>
															</th>
															<th>
																<?php $details = getUserDetailsfromHash($arry['emp_hash']);
																echo "{$details['Department']} "; ?>
															</th>
															<th>
																<?= $arry['request_date']->format('j F, Y, g:i a') ?>
															</th>
															<th>
																<?php

																$level_status = "Pending";
																?>
																Pending

															</th>
															<th>
																Level <?= $arry['level_type'] ?>
															</th>
															<th>

																<a href="request_single.php?current=<?= $arry['id'] ?>&level_type=<?= $arry['level_type'] ?>&level_status=<?= $level_status ?>">View</a>
															</th>
														</tr>
													<?php endforeach; ?>
													<tbody>


													</tbody>
												</table>
												<?php include '../main/pagination.php'; ?>


											</div>













											<!-- /.tab-content -->
										</div>
										<!-- /.nav-tabs-custom -->
									</div>
									<!-- /.col -->
								</div>
							</div>













						</div>











					</div>

				<?php endif; ?>






















			</div>
		</div>
		<!-- /.box-body -->




		</div>

















		<!-- /.row -->
	</section>
	<!-- /.content -->












	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->


	<script>
		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});













































		}

		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select a form', 'red');
				};

				return;

			} else {

				$('#theform1').submit();






			}



		});
	</script>
</body>

</html>
<?php session_start();
include '../login/scriptrunner.php';
function createPassword($value = 'secret')
{
    return strtoupper(md5($value));
}
function inValidatePassword($HashKey)
{
    $Script = "Update UsersiP set Status='H' WHERE EmpID='" . $HashKey . "'";
    ScriptRunnerUD($Script, "History");
}

function saveUserPassword($HashKey, $NewPassword)
{
    $Script = "Insert into UsersiP ([EmpID],[EmpPw],[Status]) VALUES
	('" . $HashKey . "','" . $NewPassword . "','L')";
    ScriptRunnerUD($Script, "");
}

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
//Include is brought in below
if (!isset($_REQUEST["FAction"])) {
    echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
}

if (ValidateURths("EMPLOYEE EXIT" . "V") != true) {include '../main/NoAccess.php';exit;}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        //if (ValidateURths("EMPLOYEE EXIT"."A")!=true){include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") != true) {include '../main/NoAuth.php';exit;} /*AuthEdit*/
                else {
                        $Script_CCode = "SELECT EmpID from EmpTbl where HashKey='" . $EditID . "'";
                        $Script = "UPDATE [EmpTbl]
																											SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
																											  ,[AuthDate] = GetDate()
																											  ,[Status] = 'A'
																												,[EmpStatus] = 'Active'
																												WHERE [HashKey] = '" . $EditID . "'";
                        ScriptRunnerUD($Script, "Authorize");
                        AuditLog("AUTHORIZE", "Authorization of Staff account [" . ScriptRunner($Script_CCode, "EmpID") . "].");

                        echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee account(s) authorized successfully.', 'green'); }</script>");
                    }
                }
            }
        } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        //if (ValidateURths("EMPLOYEE EXIT"."A")!=true){include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") == true) {include '../main/NoAuth.php';exit;} /*UnAuth*/else {
                    $Script_CCode = "SELECT EmpID from EmpTbl where HashKey='" . $EditID . "'";
                    $Script = "UPDATE [EmpTbl]
					SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'U'
						WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("UNAUTHORIZE", "Unauthorization of Staff account [" . ScriptRunner($Script_CCode, "EmpID") . "].", $EditID);

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected employee account(s) unauthorized successfully.','green');}</script>");
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0) {
        //if (ValidateURths("EMPLOYEE EXIT"."V")!=true){include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") != true) {include '../main/NoAuth.php';exit;} /*AuthEdit*/else {
                    $EditID = ECh($HashVals[$i]);
                    header("Location:cr_NwEmp?PgTy=Create&AcctNo=$EditID&rffbf6545bghjj89jyl8r&amp;pg=jhhyg54fgvgft65t6r53u&amp;mylogin=c&amp;ad=true");
                    exit;
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("EMPLOYEE" . "D") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") == true) {
                    $Script_CCode = "SELECT EmpID from EmpTbl where HashKey='" . $EditID . "'";
                    $Script = "UPDATE [EmpTbl]
					SET [DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[DeletedDate] = GetDate()
					  ,[Status] = 'D'
						WHERE [HashKey] = '" . $EditID . "'";
                    //                echo $Script;
                    ScriptRunnerUD($Script, "Delete");
                    AuditLog("DELETE", "Deletion of Staff account [" . ScriptRunner($Script_CCode, "EmpID") . "].");

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected employee account(s) deleted successfully.','green'); }</script>");
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Reactivate Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        //if (ValidateURths("EMPLOYEE EXIT"."A")!=true){include '../main/NoAccess.php';exit;}
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel1("SELECT EmpStatus from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") != true) {include '../main/NoAuth.php';exit;} /*AuthEdit*/
                else {
                        $Script_CCode = "SELECT EmpID from EmpTbl where HashKey='" . $EditID . "'";
                        $Script = "UPDATE [EmpTbl]
																											SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
																											  ,[AuthDate] = GetDate()
																											,[EmpStatus] = 'Active'
																												WHERE [HashKey] = '" . $EditID . "'";
                        //var_dump("good");
                        ScriptRunnerUD($Script, "Restore");
                        //var_dump("bad");
                        AuditLog("RESTORE", "Restoration of Staff account [" . ScriptRunner($Script_CCode, "EmpID") . "].");

                        echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee account(s) reactivated successfully.', 'green'); }</script>");
                    }
                }
            }
        } 
        
        elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] === "Reactivate with ID") {
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
      

        if ($ArrayCount > 2) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Kindly select one employee at a time.', 'green'); }</script>");

        } else {
            $EditID = ECh(end($HashVals));
            if (strlen($EditID) === 32) {
                $new_emp_id = ECh($_REQUEST['emp_id_hid']);
                $biodata = ECh($_REQUEST['biodata_hid']);
                $edu_history = ECh($_REQUEST['edu_history_hid']);
                $emp_history = ECh($_REQUEST['emp_history_hid']);
                $GpName = ECh($_REQUEST['GpName_hid']);
                $log_name = ECh($_REQUEST['log_id_hid']);

                // check for emplyee id
                if ($new_emp_id === "") {
                    echo ("<script type='text/javascript'>{parent.msgbox('Kindly enter a valid Employee ID', 'red'); }</script>");

                } else {

                    // get a new hashkey

                    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Employee" . $_SESSION["StkTck" . "UName"];
                    $HashKey = md5($UniqueKey);

                    // get previous info
                    $Script_C = "SELECT * from EmpTbl where HashKey='" . $EditID . "'";
                    $emp_info = ScriptRunnercous($Script_C);
                    $emp_empdt = isset($emp_info["EmpDt"]) ? trim(ECh($emp_info["EmpDt"]->format('Y-m-d H:i:s'))) : '';

                    $emp_dob = isset($emp_info["DOB"]) ? ECh(trim($emp_info["DOB"]->format('Y-m-d H:i:s'))) : '';
                    $emp_card_exp = isset($emp_info["card_exp"]) ? ECh($emp_info["card_exp"]->format('Y-m-d H:i:s')) : '';
                    $emp_exit = isset($emp_info["ExitDt"]) ? ECh($emp_info["ExitDt"]->format('Y-m-d H:i:s')) : '';

                    $Script = "INSERT INTO [EmpTbl] (
                  [EmpID],
                  [EmpMgr],
                  [EmpMgr_1],
                  [ProMgr],
                  [EmpStatus],
                  [EmpDt],
                  [SName],
                  [FName],
                  [ONames],
                  [DOB],
                  [Sex],
                  [Title],
                  [COO],
                  [SOO],
                  [LGA],
                  [Department],
                  [MStatus],
                  [Relg],
                  [EmpType],
                  [Email],
                  [Mobile],
                  [Phone],
                  [HmAdd],
                  [OffAdd],
                  [BloodGrp],
                  [GenoType],
                  [EmpDisabled],
                  [KnownIllment],
                  [NOK1Name],
                  [NOK2Name],
                  [NOK1Rel],
                  [NOK2Rel],
                  [NOK1Add],
                  [NOK2Add],
                  [NOK1Mobile],
                  [NOK2Mobile],
                  [SalBank],
                  [SalAcctNo],
                  [SalSortCode],
                  [PENCommNo],
                  [PenCustID],
                  [TaxID],
                  [TaxState],
                  [HInsurNo],
                  [name_spouse],
[spouse_residence],
[spouse_hmo_id]
,[child1_id]
,[child2_id]
,[child3_id]
,[child4_id]
,[card_exp],
[NIN],
[EmpCategory],
[spouse_address],
[spouse_phone],
[no_children],
[no_dependant],
[hmo_plan],
[hmo_id],
[primary_hospital],
[hospital_address],
[hospital_contact_1],
[hospital_contact_2],
[hmo_contact_1],
[hmo_contact_2],
	[MediCareNo],[StfImg],[BranchID],[ExitDt],[Expatriate],[Status],[created_from],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . trim(ECh($new_emp_id)) . "',
  '" . trim(ECh($emp_info["EmpMgr"])) . "',
  '" . trim(ECh($emp_info["EmpMgr_1"])) . "',
  '" . trim(ECh($emp_info["ProMgr"])) . "',
  'Active',
  '" . $emp_empdt . "',
  '" . ECh(strtoupper($emp_info["SName"])) . "',
  '" . ECh(trim($emp_info["FName"])) . "',
  '" . ECh(trim($emp_info["ONames"])) . "',
  '" . $emp_dob . "',
  '" . $emp_info["Sex"] . "',
  '" . ECh($emp_info["Title"]) . "',
  '" . ECh($emp_info["COO"]) . "',
  '" . ECh($emp_info["SOO"]) . "',
  '" . ECh($emp_info["LGA"]) . "',
  '" . ECh($emp_info["Department"]) . "',
  '" . ECh($emp_info["MStatus"]) . "',
  '" . ECh($emp_info["Relg"]) . "',
  '" . ECh($emp_info["EmpType"]) . "',
  '" . ECh(trim($emp_info["Email"])) . "',
  '" . ECh(trim($emp_info["Mobile"])) . "',
  '" . ECh(trim($emp_info["Phone"])) . "',
  '" . ECh($emp_info["HmAdd"]) . "',
  '" . ECh($emp_info["OffAdd"]) . "',
  '" . ECh($emp_info["BloodGrp"]) . "',
  '" . ECh($emp_info["GenoType"]) . "',
  '" . ECh($emp_info["EmpDisabled"]) . "',
  '" . ECh($emp_info["KnownIllment"]) . "',
  '" . ECh($emp_info["NOK1Name"]) . "',
  '" . ECh($emp_info["NOK2Name"]) . "',
  '" . ECh($emp_info["NOK1Rel"]) . "',
  '" . ECh($emp_info["NOK2Rel"]) . "',
  '" . ECh($emp_info["NOK1Add"]) . "',
  '" . ECh($emp_info["NOK2Add"]) . "',
  '" . ECh($emp_info["NOK1Mobile"]) . "',
  '" . ECh($emp_info["NOK2Mobile"]) . "',
  '" . ECh(trim($emp_info["SalBank"])) . "',
  '" . ECh(trim($emp_info["SalAcctNo"])) . "',
  '" . ECh($emp_info["SalSortCode"]) . "',
  '" . ECh($emp_info["PENCommNo"]) . "',
  '" . ECh($emp_info["PenCustID"]) . "',
  '" . ECh($emp_info["TaxID"]) . "',
  '" . ECh($emp_info["TaxState"]) . "',
	'" . ECh($emp_info["HInsurNo"]) . "',
	'" . ECh($emp_info["name_spouse"]) . "',
'" . ECh($emp_info["spouse_residence"]) . "',
'" . ECh($emp_info["spouse_hmo_id"]) . "',
'" . ECh($emp_info["child1_id"]) . "',
'" . ECh($emp_info["child2_id"]) . "',
'" . ECh($emp_info["child3_id"]) . "',
'" . ECh($emp_info["child4_id"]) . "',
'" . $emp_card_exp . "',
'" . ECh($emp_info["NIN"]) . "',
'" . ECh($emp_info["EmpCategory"]) . "',
'" . ECh($emp_info["spouse_address"]) . "',
'" . ECh($emp_info["spouse_phone"]) . "',
'" . ECh($emp_info["no_children"]) . "',
'" . ECh($emp_info["no_dependant"]) . "',
'" . ECh($emp_info["hmo_plan"]) . "',
'" . ECh($emp_info["hmo_id"]) . "',
'" . ECh($emp_info["primary_hospital"]) . "',
'" . ECh($emp_info["hospital_address"]) . "',
'" . ECh($emp_info["hospital_contact_1"]) . "',
'" . ECh($emp_info["hospital_contact_2"]) . "',
'" . ECh($emp_info["hmo_contact_1"]) . "',
'" . ECh($emp_info["hmo_contact_2"]) . "',
	'" . ECh($emp_info["MediCareNo"]) . "',
	'pfimg/Default.jpg',
	'" . ECh($emp_info["BranchID"]) . "',
	'" . $emp_exit . "',
	'" . ECh($emp_info["Expatriate"]) . "',
	'N',
	'" . $EditID . "',
	'" . ECh($_SESSION["StkTck" . "HKey"]) . "',
	GetDate(),
	'" . $HashKey . "')";
                    ScriptRunnerUD($Script, "Inst");
                    $Script = "Update Settings set SetValue2=(SetValue2 + 1) where Setting='EmpIDPSCnt'";
                    ScriptRunnerUD($Script, "Inst");
                    // get past educational history
                    if ($edu_history == 1) {
                        $dbOpen7 = ("SELECT * from EmpDtl where StaffID='" . $EditID . "' and ItemCode='EDU' and Status <> 'D' order by ItemEnd desc, ItemType asc");
                        // var_dump($dbOpen7);
                        include '../login/dbOpen7.php';
                        while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {
                            // var_dump($row7);
                            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "EmployeeQualification" . $_SESSION["StkTck" . "UName"];
                            $HashKey_edu = md5($UniqueKey);

                            $item_start = isset($row7["ItemStart"]) ? $row7["ItemStart"]->format('Y-m-d H:i:s') : '';
                            $item_end = isset($row7["ItemEnd"]) ? $row7["ItemEnd"]->format('Y-m-d H:i:s') : '';
                            $item_exp = isset($row7["ItemExpiry"]) ? $row7["ItemExpiry"]->format('Y-m-d H:i:s') : '';
                            $Script = "INSERT INTO [EmpDtl]			(
                              [StaffID],
                              [ItemCode],
                              [ItemType],
                              [ItemName],
                              [ItemPos],
                              [ItemDegree],
                              [ItemStart],
                              [ItemEnd],
                              [ItemExpiry],
                              [ItemDoc],
                              [ItemDocHID],
                              [Status],
                              [AddedBy],
                              [AddedDate],
                              [HashKey]
                              ) VALUES
                              (
                                '" . $HashKey . "',
                              'EDU','
                              " . $row7["ItemType"] . "',
                              '" . $row7["ItemName"] . "',
                              '" . $row7["ItemPos"] . "',
                              '" . $row7["ItemDegree"] . "',
                              '" . $item_start . "',
                              '" . $item_end . "',
                              '" . $item_exp . "',
                              '" . $row7["ItemDoc"] . "',
                              '" . $row7["ItemDocHID"] . "',
                              '" . $row7["Status"] . "',
                              '" . $_SESSION["StkTck" . "HKey"] . "',
                              GetDate(),
                              '" . $HashKey_edu . "')";

                            ScriptRunnerUD($Script, "Inst");
                        }
                        include '../login/dbClose7.php';

                    }
                    // get past employment history
                    if ($emp_history == 1) {
                        $dbOpen7 = ("SELECT * from EmpDtl where StaffID='" . $EditID . "' and ItemCode='EMP' and Status <> 'D' order by ItemEnd desc, ItemType asc");
                        // var_dump($dbOpen7);
                        include '../login/dbOpen7.php';
                        while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {
                            // var_dump($row7);
                            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "EmpWork" . $_SESSION["StkTck" . "UName"];
                            $HashKey_his = md5($UniqueKey);

                            $item_start = isset($row7["ItemStart"]) ? $row7["ItemStart"]->format('Y-m-d H:i:s') : '';
                            $item_end = isset($row7["ItemEnd"]) ? $row7["ItemEnd"]->format('Y-m-d H:i:s') : '';
                            $item_exp = isset($row7["ItemExpiry"]) ? $row7["ItemExpiry"]->format('Y-m-d H:i:s') : '';

                            $Script = "INSERT INTO [EmpDtl]			(
                          [StaffID],
                          [ItemCode],
                          [ItemType],
                          [ItemName],
                          [RemindEmp],
                          [ItemLevel],
                          [ItemPos],
                          [ItemMgr],
                          [ItemStart],
                          [ItemEnd],
                          [ItemExpiry],
                          [ItemDoc],
                          [ItemDocHID],
                          [Status],
                          [AddedBy],
                          [AddedDate],
                          [HashKey]
                          ) VALUES
	                     (
                         '" . $HashKey . "',
                       'EMP',
                       '" . $row7["ItemType"] . "',
                       '" . $row7["ItemName"] . "',
                       '" . $row7["RemindEmp"] . "',
                       '" . $row7["ItemLevel"] . "',
                       '" . $row7["ItemPos"] . "',
                       '" . $row7["ItemMgr"] . "',
                      '" . $item_start . "',
                      '" . $item_end . "',
                      '" . $item_exp . "',
                       '',
                      '" . $row7["ItemDocHID"] . "',
                      '" . $row7["Status"] . "',
                       '" . $_SESSION["StkTck" . "HKey"] . "',
                       GetDate(),
                       '" . $HashKey_his . "')";
                            ScriptRunnerUD($Script, "New_Item");

                        }
                        include '../login/dbClose7.php';

                    }

                    if ($log_name !== '' && $GpName !== '') {
                        $Script = "SELECT HashKey FROM [UGpRights] WHERE GpName='" . $GpName . "'";
                        $Script = "INSERT INTO [Users] ([LogName],[GpName],[AcctState],[PassDate],[RPwd],[ULock],[LogCnt],[LStatus],[EmpID],[Status],[AddedBy],[AddedDate],[HashKey],[CustID]) VALUES ('" . $log_name . "','" . ScriptRunner($Script, "HashKey") . "',1,'01-Jan-1900',1,0,0,'A','" . $new_emp_id . "','A','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "','" . $_SESSION["StkTck" . "CustID"] . "')";
                        ScriptRunnerUD($Script, "Inst");

                        //Create new user password
                        $NewPassword = createPassword($log_name);
                        //Render all old passwords invalid
                        inValidatePassword($HashKey);

                        //Save password to table
                        saveUserPassword($HashKey, $NewPassword);

                    }

                    AuditLog("INSERT", "New employee record reactivated for [" . ECh($emp_info["SName"] . " " . $emp_info["FName"] . " " . $emp_info["ONames"]) . "]");
                    echo ("<script type='text/javascript'>{ parent.msgbox('Employee Reactivated successfully.', 'green'); }</script>");

                }

            } else {
                echo ("<script type='text/javascript'>{ parent.msgbox('Kindly select an employee.', 'green'); }</script>");

            }

        }

    }
        elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] === "Update with ID") {
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
      

        if ($ArrayCount > 2) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Kindly select one employee at a time.', 'green'); }</script>");

        } else {
            $EditID = ECh(end($HashVals));
            if (strlen($EditID) === 32) {

                // check for emplyee id
                $new_emp_id = ECh($_REQUEST['emp_id_hid']);
                if ($new_emp_id === "") {
                    echo ("<script type='text/javascript'>{parent.msgbox('Kindly enter a valid Employee ID', 'red'); }</script>");

                } else {
                    $check_script = "Select * from [EmpTbl] where [EmpID] = '".$new_emp_id."' ";
                    $emp_info_fomer = ScriptRunnercous($check_script);


                  if(is_null(  $emp_info_fomer )){

                    $Script = "UPDATE [EmpTbl] set [EmpID]='" .  $new_emp_id . "',[EmpStatus] ='Active' ,[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]='" . $EditID. "'";
                    ScriptRunnerUD($Script, "Inst");

                    $check_script = "select * from [Users] where HashKey='".$EditID."' ";
                    $former_logname=ScriptRunner($check_script, "LogName");
                    $former_empId=ScriptRunner($check_script, "EmpID");

                    if($former_empId === $former_logname){
                      $script_up= "Update [Users] set  [EmpID]='" . $new_emp_id . "', [LogName]='" . $new_emp_id. "' where HashKey='".$EditID."'" ;

                    }else{
                      $script_up= "Update [Users] set  [EmpID]='" . $new_emp_id . "' where HashKey='".$EditID."'" ;
                    }
                   ScriptRunnerUD($script_up, "Inst");

                   $ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
                   $AutoID = ScriptRunner($ScriptEmpIDP, "SetValue");


                   if ($AutoID == "on") {

                     $Script = "Update Settings set SetValue2=(SetValue2 + 1) where Setting='EmpIDPSCnt'";
                     ScriptRunnerUD($Script, "Inst");

                   }


                    $Script_C = "SELECT * from EmpTbl where HashKey='" . $EditID . "'";
                    $emp_info = ScriptRunnercous($Script_C);

                    AuditLog("UPDATE", "Employee ID updated for [" . ECh($emp_info["SName"] . " " . $emp_info["FName"] . " " . $emp_info["ONames"]) . "]  from [".ECh($former_empId)."] to [".ECh($new_emp_id)."] ");
                    echo ("<script type='text/javascript'>{ parent.msgbox('Employee ID updated successfully.', 'green'); }</script>");
                  }else{

                    echo ("<script type='text/javascript'>{ parent.msgbox('Employee ID already exist in the system. kindly try with another Employee ID', 'red'); }</script>");

                  }
                }

            } else {
                echo ("<script type='text/javascript'>{ parent.msgbox('Kindly select an employee.', 'green'); }</script>");

            }

        }

    }
}

include '../css/myscripts.php';
?>

				<script>
				  $(function() {
				    $( document ).tooltip();
				  });
				</script>


				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
				<link href="../css/style_main.css" rel="stylesheet" type="text/css">
				<!-- Bootstrap 4.0-->
				<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
				 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
				<!-- Bootstrap 4.0-->
				<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

				<!-- Select 2-->
				<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
				<!-- Theme style -->
				<link rel="stylesheet" href="../assets/css/master_style.css">
				<link rel="stylesheet" href="../assets/css/responsive.css">


				<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
				<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

				<body oncontextmenu="return false;"  topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
				<table  width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table table-responsive">
				<form action="#" method="post" name="Records" target="_self" id="Records">
				  <tr>
				    <td height="10" colspan="5" align="center"  class="tdMenu_HeadBlock">Authorize/Unauthorize Employee Accounts</td>
				  </tr>
				<tr class="tdMenu_HeadBlock_Light">
				<td colspan="2" align="left"><span class="TinyText">



				  <?php //$Load_Date=true; $Load_Search=false; $Load_Auth=false; include '../main/report_opt.php'; ?>
				  <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE"/>
				      <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE"/>
							<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="INACTIVE"/>
				</span></td>
				</tr>
				  <tr>
				    <td height="100%" colspan="5" align="center" class="ViewPatch">
				    <table width="100%" align="center" id="table" class="tablesorter">
				      <thead>
				        <tr >
				          <th width="20"valign="middle" scope="col" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label></th>
				          <th width="85" align="center" valign="middle" scope="col">Employed</th>
				          <th width="184" align="center" valign="middle" scope="col">Employee ID</th>
				          <th width="459" align="center" valign="middle" scope="col">Full Name</th>
				          <th width="309" align="center" valign="middle" scope="col">Department</th>
				          <th width="121" align="center" valign="middle" scope="col" data-sorter="false">Mobile</th>
				          <th width="46" align="center" valign="middle" scope="col" data-sorter="false">Email</th>
				          <th width="60" valign="middle" align="center" scope="col" data-sorter="false">Status</th>
					<?php if (isset($_POST['SubmitTrans']) && $_POST['SubmitTrans'] == "INACTIVE"): ?>
				          <th width="60" valign="middle" align="center" scope="col" data-sorter="false">Exit Reasons</th>
				  <?php endif;?>
        </tr>
      </thead>
      <tbody>
        <?php
if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {$PayMthDt = " and EmpDt between '" . ECh($_REQUEST["S_RptDate"]) . "' and '" . ECh($_REQUEST["E_RptDate"]) . "' ";} else { $PayMthDt = '';}

$Del = 0;

if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    print "<font size=\"3\">UNAUTHORIZED</font>";

    /*
    $dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, * from EmpTbl where EmpStatus='Active' and (EmpID like '%".$SearchVal."%' or FName like '%".$SearchVal."%' or SName like '%".$SearchVal."%' or ONames like '%".$SearchVal."%') ".$PayMthDt." order by SName asc, EmpID desc");
     */
    $StatusCheck = "('N','U','PC','PA')";
    $dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, * from EmpTbl where Status in " . $StatusCheck . " and EmpStatus='Active' order by SName asc, EmpID desc");

} elseif (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "AUTHORIZE")) {
    print "<font size=\"3\">AUTHORIZED</font>";

    $StatusCheck = "('A')";
    $dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, * from EmpTbl where Status in " . $StatusCheck . " order by SName asc, EmpID desc");

    /*
$dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, * from EmpTbl where   (EmpID like '%".$SearchVal."%' or FName like '%".$SearchVal."%' or SName like '%".$SearchVal."%' or ONames like '%".$SearchVal."%') ".$PayMthDt." order by SName asc, EmpID desc");
 */
} elseif (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "INACTIVE")) {
    print "<font size=\"3\">INACTIVE</font>";

    $dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, *
	from EmpTbl where EmpStatus != 'Active' order by SName asc, EmpID desc");

    /*
$dbOpen2 = ("SELECT Convert(Varchar(11),EmpDt,106) as Dt, (SName + ' ' + FName + ' ' + ONames) as Nm, * from EmpTbl where   (EmpID like '%".$SearchVal."%' or FName like '%".$SearchVal."%' or SName like '%".$SearchVal."%' or ONames like '%".$SearchVal."%') ".$PayMthDt." order by SName asc, EmpID desc");
 */
}

include '../login/dbOpen2.php';
//mssql_num_rows
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
//        return ($row2['$rwfield']);
    $Del = $Del + 1;
    //if ($Del==1) {$PrevID=$row2['ID'];} else {$NextID=$row2['ID'];}
    ?>
        <tr>
          <td width="20" height="20" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
          <td width="85" align="left" valign="middle" scope="col"><?php echo (trim($row2['Dt'])); ?></td>
          <td width="184" align="left" valign="middle" scope="col"><?php echo (trim($row2['EmpID'])); ?></td>
          <td align="left" valign="middle" scope="col"><?php echo trim($row2['Nm']); ?></td>
          <td width="309" align="left" valign="middle" scope="col"><?php echo $row2['Department']; ?></td>
          <td width="121" align="left" valign="middle" scope="col"><?php echo $row2['Mobile']; ?></td>
          <td align="center" valign="middle" scope="col"><?php
if (strlen(trim($row2['Email'])) > 10) {echo ('<img src="../images/green.png" height="18" title="Email:' . $row2['Email'] . '">');} else {echo ('<img src="../images/red.png" height="18" title="No registered Email">');}?>
          </td>
          <td width="60" align="center" valign="middle" scope="col">
		  <?php
if ($_POST['SubmitTrans'] == "UNAUTHORIZE") {
        echo 'Unauthorize';
    } else if ($_POST['SubmitTrans'] == "AUTHORIZE") {
        echo 'Authorize';
    } else if ($_POST['SubmitTrans'] == "INACTIVE") {

        echo $row2['EmpStatus'];
    }

    ?></td>
	<?php if ($_POST['SubmitTrans'] == "INACTIVE"): ?>
	<td>
	 <?php echo $row2['exitreasons'];
    ?>
	</td>
  <?php endif;?>
        </tr>
        <?php }
include '../login/dbClose2.php';?>
      </tbody>
    </table></td>
    </tr>
  <tr class="tdMenu_HeadBlock_Light">
    <td width="50%" align="left"><?php include '../main/pagination.php';?></td>
    <td width="50%" align="right">
<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
	  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
	  <input name="FAction" id="FAction" type="hidden">
	  <input name="emp_id_hid" id="emp_id_hid" type="hidden">
	  <input name="biodata_hid" id="biodata_hid" type="hidden">
	  <input name="edu_history_hid" id="edu_history_hid" type="hidden">
	  <input name="emp_history_hid" id="emp_history_hid" type="hidden">
	  <input name="GpName_hid" id="GpName_hid" type="hidden">
	  <input name="log_id_hid" id="log_id_hid" type="hidden">

      '
;

//Check if any record was spolled at all before enabling buttons
//Check if user has Delete rights
if (ValidateURths("EMPLOYEE EXIT" . "D") == true && ($_POST['SubmitTrans'] == "AUTHORIZE") || ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "D");
    $but_Type = 'Delete';include '../main/buttons.php';
}
//Check if user has Add/Edit rights
if (ValidateURths("EMPLOYEE EXIT" . "V") == true && ($_POST['SubmitTrans'] == "AUTHORIZE") || ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "V");
    $but_Type = 'View';include '../main/buttons.php';
}
//Check if user has Authorization rights
if (ValidateURths("EMPLOYEE EXIT" . "T") == true && ($_POST['SubmitTrans'] == "AUTHORIZE") || ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "T");
    $but_Type = 'Authorize';include '../main/buttons.php';
}

if (ValidateURths("EMPLOYEE EXIT" . "T") == true && ($_POST['SubmitTrans'] == "INACTIVE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "T");
    $but_Type = 'Reactivate';include '../main/buttons.php';
}
if (ValidateURths("EMPLOYEE EXIT" . "T") == true && ($_POST['SubmitTrans'] == "INACTIVE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "T");
    $but_Type = 'Reactivate with new ID';include '../main/buttons.php';
}
if (ValidateURths("EMPLOYEE EXIT" . "T") == true && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
    $but_HasRth = ("EMPLOYEE EXIT" . "T");
    $but_Type = 'Update with new ID';include '../main/buttons.php';
}
?>
   </tr>
	</form>
</table>

<script>
 function deleteFun(){


    	document.getElementById("ActLnk").value="";
        $Mx=document.getElementById("DelMax").value;

	for ($i=1; $i<=$Mx; $i++)
	{
		if (  document.getElementById("DelBox" + $i) && document.getElementById("DelBox" + $i).checked)
		{

			document.getElementById("ActLnk").value=document.getElementById("ActLnk").value+';'+document.getElementById("DelBox"+$i).value;
		}
	}

    // return;



    //    document.getElementById("SubmitTrans").value="Update Record";


		$('<div></div>').appendTo('body')
        .html('<div><class=TextBoxText>Do you wish to <strong>Reactive Employee with new ID</strong>?</h5></div>')
        .dialog({
          modal: true, title: "Reactive Employee with new ID", zIndex: 10000, autoOpen: true,
          width: 'auto', resizable: true,
          height: 'auto', resizable: true,
          buttons: {
            Yes: function () {

              <?php

$ScriptEmpIDP = "Select * from Settings where Setting='EmpIDPSCnt'"; // and Status='A'
$SetVal = ScriptRunner($ScriptEmpIDP, "SetValue");
$SetVal2 = ScriptRunner($ScriptEmpIDP, "SetValue2") + 1;
$SetVal3 = ScriptRunner($ScriptEmpIDP, "SetValue3");
$SetVal4 = ScriptRunner($ScriptEmpIDP, "SetValue4");
$DigitsDsp_ = ScriptRunner("Select * from Settings where Setting='AutoEmpID'", "SetValue2");
if ($DigitsDsp_ == "Long") {
    if (strlen($SetVal2) == 1) {$SetVal2 = "000" . $SetVal2;} elseif (strlen($SetVal2) == 2) {$SetVal2 = "00" . $SetVal2;} elseif (strlen($SetVal2) == 3) {$SetVal2 = "0" . $SetVal2;}
}
if (trim($SetVal3) != "") {$SetVal5 = $SetVal4;} else { $SetVal5 = "";}

$LoginDet = $SetVal . $SetVal4 . $SetVal2 . $SetVal5 . $SetVal3;
$LoginDet = str_replace('#YYYY#', date('Y'), $LoginDet);
$LoginDet = str_replace('#YY#', date('y'), $LoginDet);
$LoginDet = str_replace('#MM#', date('m'), $LoginDet);
//$LoginDet=str_replace('#YYYY#',date('Y'),$LoginDet);

$ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
$AutoID = ScriptRunner($ScriptEmpIDP, "SetValue");
$input_emp = '';
if ($AutoID == "on") {
    $input_emp = '<input type="text" readonly name="emp_id"  class="form-control" id="emp_id" value="' . trim($LoginDet) . '"    placeholder="New employee ID" />';
} else {
    $input_emp = '<input type="text" name="emp_id"  class="form-control" id="emp_id" value="' . trim($LoginDet) . '"   placeholder="New employee ID"  />';
}

?>


                      $('<div></div>').appendTo('body')
                        .html(`
                      <?=$input_emp?>
                       <br>
                       <div id="reactivateTemp">

                       <input type="checkbox" id="biodata" name="biodata" value="1"  checked onclick="return false;" >
                       <label for="biodata">Biodata Information</label><br>
                       <input type="checkbox" id="emp_history" name="emp_history" value="1">
                       <label for="emp_history">Employment History</label><br>
                       <input type="checkbox" id="edu_history" name="edu_history" value="1">
                       <label for="edu_history">Education History</label><br>
                       <input type="checkbox" id="as_user" name="as_user" value="1"  onclick="addTemplate()" >
                       <label for="as_user">Create as user</label><br>

                       </div>

                        `).dialog({
                        modal: true,title: " Reactive employee with ", zIndex: 10000, autoOpen: true,
                        height: 370 + 6,
                        width: 500 + 20,
                        buttons: {

                        Reactivate: function () {


                        if (document.getElementById("emp_id").value =='') {

                           { parent.msgbox('Please provide new employee ID.', 'red'); };

                           return;

                        }

                            if($('#as_user').is(":checked")){

                            if (document.getElementById("log_id").value =='' ) {

                           { parent.msgbox('Please provide a valid login ID.', 'red'); };

                           return;

                        }
                            if (document.getElementById("GpName").value =='' ) {

                           { parent.msgbox('Please provide a valid group name.', 'red'); };

                           return;

                        }

                            }

                    if($('#as_user').is(":checked")){
                	document.getElementById("log_id_hid").value=document.getElementById("log_id").value;
                	document.getElementById("GpName_hid").value=document.getElementById("GpName").value;



                     }




							document.getElementById("emp_id_hid").value=document.getElementById("emp_id").value;

							document.getElementById("biodata_hid").value=document.getElementById("biodata").checked === true ? document.getElementById("biodata").value : '' ;
							document.getElementById("edu_history_hid").value=document.getElementById("edu_history").checked === true ? document.getElementById("edu_history").value : '' ;
							document.getElementById("emp_history_hid").value=document.getElementById("emp_history").checked === true ? document.getElementById("emp_history").value : '' ;


							// document.getElementById("exitreasons").value=document.getElementById("sd").value;
							// console.log(document.getElementById("ExDt").value);
							// return;
							// console.log(document.getElementById("SubmitTrans").value);
                            document.getElementById("FAction").value="Reactivate with ID";
                            document.getElementById("Records").submit();




                          $(this).dialog("close");

                        }
                      }

                      });





                    $(this).dialog("close");
            },
            No: function () {

              $(this).dialog("close");
            }
          },
          close: function (event, ui) {
            $(this).remove();
          }
        });








  }



  function deleteFun2(){


document.getElementById("ActLnk").value="";
$Mx=document.getElementById("DelMax").value;

for ($i=1; $i<=$Mx; $i++)
{
if (  document.getElementById("DelBox" + $i) && document.getElementById("DelBox" + $i).checked)
{

    document.getElementById("ActLnk").value=document.getElementById("ActLnk").value+';'+document.getElementById("DelBox"+$i).value;
}
}

// return;



//    document.getElementById("SubmitTrans").value="Update Record";


$('<div></div>').appendTo('body')
.html('<div><class=TextBoxText>Do you wish to <strong>Update Employee with new ID</strong>?</h5></div>')
.dialog({
  modal: true, title: "Update Employee with new ID", zIndex: 10000, autoOpen: true,
  width: 'auto', resizable: true,
  height: 'auto', resizable: true,
  buttons: {
    Yes: function () {

      <?php

$ScriptEmpIDP = "Select * from Settings where Setting='EmpIDPSCnt'"; // and Status='A'
$SetVal = ScriptRunner($ScriptEmpIDP, "SetValue");
$SetVal2 = ScriptRunner($ScriptEmpIDP, "SetValue2") + 1;
$SetVal3 = ScriptRunner($ScriptEmpIDP, "SetValue3");
$SetVal4 = ScriptRunner($ScriptEmpIDP, "SetValue4");
$DigitsDsp_ = ScriptRunner("Select * from Settings where Setting='AutoEmpID'", "SetValue2");
if ($DigitsDsp_ == "Long") {
if (strlen($SetVal2) == 1) {$SetVal2 = "000" . $SetVal2;} elseif (strlen($SetVal2) == 2) {$SetVal2 = "00" . $SetVal2;} elseif (strlen($SetVal2) == 3) {$SetVal2 = "0" . $SetVal2;}
}
if (trim($SetVal3) != "") {$SetVal5 = $SetVal4;} else { $SetVal5 = "";}

$LoginDet = $SetVal . $SetVal4 . $SetVal2 . $SetVal5 . $SetVal3;
$LoginDet = str_replace('#YYYY#', date('Y'), $LoginDet);
$LoginDet = str_replace('#YY#', date('y'), $LoginDet);
$LoginDet = str_replace('#MM#', date('m'), $LoginDet);
//$LoginDet=str_replace('#YYYY#',date('Y'),$LoginDet);

$ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
$AutoID = ScriptRunner($ScriptEmpIDP, "SetValue");
$input_emp = '';
if ($AutoID == "on") {
$input_emp = '<input type="text" readonly name="emp_id"  class="form-control" id="emp_id" value="' . trim($LoginDet) . '"    placeholder="New employee ID" />';
} else {
$input_emp = '<input type="text" name="emp_id"  class="form-control" id="emp_id" value="' . trim($LoginDet) . '"   placeholder="New employee ID"  />';
}

?>


              $('<div></div>').appendTo('body')
                .html(`
              <?=$input_emp?>
             
                `).dialog({
                modal: true,title: " Update employee with ", zIndex: 10000, autoOpen: true,
                height: 370 + 6,
                width: 500 + 20,
                buttons: {

                Update: function () {


                if (document.getElementById("emp_id").value =='') {

                   { parent.msgbox('Please provide new employee ID.', 'red'); };

                   return;

                }

                  

           




                    document.getElementById("emp_id_hid").value=document.getElementById("emp_id").value;

                    // document.getElementById("biodata_hid").value=document.getElementById("biodata").checked === true ? document.getElementById("biodata").value : '' ;
                    // document.getElementById("edu_history_hid").value=document.getElementById("edu_history").checked === true ? document.getElementById("edu_history").value : '' ;
                    // document.getElementById("emp_history_hid").value=document.getElementById("emp_history").checked === true ? document.getElementById("emp_history").value : '' ;


                    // document.getElementById("exitreasons").value=document.getElementById("sd").value;
                    // console.log(document.getElementById("ExDt").value);
                    // return;
                    // console.log(document.getElementById("SubmitTrans").value);
                    document.getElementById("FAction").value="Update with ID";
                    document.getElementById("Records").submit();




                  $(this).dialog("close");

                }
              }

              });





            $(this).dialog("close");
    },
    No: function () {

      $(this).dialog("close");
    }
  },
  close: function (event, ui) {
    $(this).remove();
  }
});








}
  function addTemplate(){
    if($('#as_user').is(":checked")){
        $("#as_temp").remove();
      $("#reactivateTemp").append(`
      <div id="as_temp">
          <div class="row">
              <div class="col-6" >
              <label for="inputEmail4" class="form-label">Login</label><span style="color: red">*</span>
                     <input type='text' class="form-control form-control-sm" placeholder='Login ID'  id="log_id" name="log_id" />
              </div>
              <div class="col-6" >
              <label for="inputEmail4" class="form-label">User group</label><span style="color: red">*</span>
                       <select name="GpName" id="GpName" class="form-control  form-control-sm " >
           <option value="" selected="selected">--</option>
          <?php
$dbOpen7 = ("select Distinct(GpName) as GpName from UGpRights where GpType='P' and Status ='A' ORDER BY GpName");
include '../login/dbOpen7.php';
while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {
    echo '<option value="' . ErCh($row7['GpName']) . '">' . ErCh($row7['GpName']) . '</option>';
}
include '../login/dbClose7.php';

?>
             </select>
              </div>
          </div>



      </div>
      `);

    }else{
        $("#as_temp").remove();
    }
  }


</script>
</body>
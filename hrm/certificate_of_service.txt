1. Insert into Sys_LnkMenu

ID	LnkDeptID	LnkSortID	LnkName1	LnkName2	LnkAdd1	LnkAdd2	LnkParam1	LnkParam2	LnkParam3	Status	AddedBy	AddedDate	UpdatedBy	UpdatedDate	DeletedBy	DeletedDate	AuthBy	AuthDate	HashKey	LnkIcon	LnkRth1	LnkRth2	CustID	ModuleActive	OverAllGrpAccess
50307	2	16	Certificate of Service	Certificate of Service	hrm/staff_resignation	NULL	pgTy=Add		NULL	A	Admin	2020-02-25 13:50:34.000	NULL	NULL	NULL	NULL	NULL	NULL	e0f02125c66c0a61a5cd5958ed1351c7	NULL	RESIGNATION LETTER	V	bd6dbd063f5ab7394879e3c08781cd72	1	HRM


ID	LnkDeptID	LnkSortID	LnkName1	LnkName2	LnkAdd1	LnkAdd2	LnkParam1	LnkParam2	LnkParam3	Status	AddedBy	AddedDate	UpdatedBy	UpdatedDate	DeletedBy	DeletedDate	AuthBy	AuthDate	HashKey	LnkIcon	LnkRth1	LnkRth2	CustID	ModuleActive	OverAllGrpAccess
50309	2	16	Edit Certificate of Service	Certificate of Service	hrm/edit_staff_resignation	NULL	pgTy=Add		NULL	A	Admin	2020-02-25 13:50:34.000	NULL	NULL	NULL	NULL	NULL	NULL	e0f02125c66c0a61a5cd5958ed1351c7	NULL	EDIT RESIGNATION LETTER	V	bd6dbd063f5ab7394879e3c08781cd72	1	HRM


2. Insert into Settings

Setting	DateIO	SetStaff	SetValue	SetStart	SetEnd	SetValue2	LongVal	AddedBy	AddedDate	UpdatedBy	UpdatedDate	DeletedBy	DeletedDate	AuthBy	AuthDate	LStatus	Status	HashKey	SetValue3	CustID	SetValue4	SetValue5	SetValue6	SetValue7	SetValue8	SetValue9	SetValue10	SetValue15	SetValue16	SetValue17	SetValue18	SetValue19	SetValue20	SetValue21	SetValue11
CertificateOfService	NULL	NULL	371a0c07fc45c238dca08d951c6e169f	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	A	NULL	NULL	SSL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	<!-- <div style="text-align: center;"> --><!-- <div style="text-align: center;"> --><!-- <div style="text-align: center;"> --><!-- <div style="text-align: center;"> --><!-- <div style="text-align: center;"> --><!-- <div style="text-align: center;"> -->
<p style="text-align:center"><strong>CERTIFICATE OF SERVICE</strong></p>

<p style="color:red; text-align:center">#Name#</p>

<p style="text-align:center">With Staff ID</p>

<p style="color:red; text-align:center">#StaffID#</p>

<p style="text-align:center"><strong>TO WHOM IT MAY CONCERN</strong></p>

<p style="text-align:center">The above-mentioned name was employed by NTIC as follows:</p>

<p style="text-align:center"><strong>DATE OF ENGAGEMENT:</strong></p>

<p style="color:red; text-align:center">#DateOfEngagement#</p>

<p style="text-align:center"><strong>DATE OF DISENGAGEMENT:</strong></p>

<p style="color:red; text-align:center">#DateOfDisengagement#</p>

<p style="text-align:center"><strong>REASON FOR LEAVING:</strong></p>

<p style="color:red; text-align:center">#ReasonsForLeaving#</p>

<p style="text-align:center"><strong>LAST POSITION HELD:</strong></p>

<p style="color:red; text-align:center">#PositionHeld#</p>

<p>This is the only Certificate issued by NTIC.</p>

<p>HR Department</p>
<!-- </div> --><!-- </div> --><!-- </div> --><!-- </div> --><!-- </div> --><!-- </div> -->	NULL	NULL


3. Paste staff_resignation.php, edit_staff_resignation, rt_hrm.php and cr_NwGrp_rt.php in hrm folder
4. Replace styles.js into ckeditor folder
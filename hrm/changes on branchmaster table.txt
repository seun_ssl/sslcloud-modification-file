USE [SSLCloud_0009]
GO

/****** Object:  Table [dbo].[BrhMasters]    Script Date: 5/5/2021 4:47:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BrhMasters](
	[OName] [varchar](150) NOT NULL,
	[OCode] [varchar](6) NULL,
	[OType] [varchar](15) NULL,
	[OParent] [varchar](32) NULL,
	[OContact] [varchar](32) NULL,
	[OAddress] [text] NULL,
	[OMobile] [varchar](15) NULL,
	[OPhone] [varchar](15) NULL,
	[Status] [char](1) NOT NULL,
	[AddedBy] [varchar](250) NULL,
	[AddedDate] [datetime] NULL,
	[UpdatedBy] [varchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [varchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[AuthBy] [varchar](250) NULL,
	[AuthDate] [datetime] NULL,
	[HashKey] [varchar](52) NOT NULL,
	[TaxState] [varchar](250) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[BrhMasters] ADD  CONSTRAINT [DF_BrhMasters_Status]  DEFAULT ('N') FOR [Status]
GO



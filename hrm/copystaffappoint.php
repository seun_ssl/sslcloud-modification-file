<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("EMPLOYEE" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);
$emp_details = null;
if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
	// var_dump($emp_detailsHr['sig_img']);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
	$rc_details = ScriptRunnercous($Script_Edit_RC);
	$branchId = $emp_details['BranchID'];
	$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
	$bran_details = ScriptRunnercous($Script_Edit_Br);

	// var_dump($bran_details);

}



?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: 5px;
			line-height: 20px;
		}

		.box-body ul li {
			line-height: 18px !important;
		}

		.card-img {
			width: 76%;
		}

		table,
		th,
		td {
			/* padding: 10px 5px; */
			/* padding-top:5px;
	padding-left: 15px;
	padding-bottom: 5px; */
			/* padding:5px 0px 5px 15px; */
			border: 1px solid black;
			border-collapse: collapse;
		}

		.nav-tabs-custom>.tab-content {
			color: black !important;
			text-align: justify;
		}
	</style>

</head>


<body>






	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Generate Employee Confirmation Letter
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control " name="bookselect1" id="bookselect1">
										<!-- <select name="level1" id="level1"  class="form-control" > -->
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>



									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">



				<?php if (!is_null($emp_details) && !empty($emp_details)): ?>
					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<div class="nav-tabs-custom" style="min-height: 400px">
										<ul class="nav nav-tabs">
											<li>
												<a class="active" href="#book" data-toggle="tab">
													NON-ACADEMIC STAFF
												</a>
											</li>

											<li>
												<a href="#book2" data-toggle="tab">
													ACADEMIC STAFF
												</a>
											</li>



										</ul>
										<div class="tab-content">

											<div class="tab-pane active " id="book">
												<div id="invoice" class="text-dark ">
													<div class="row">
														<div class="col-sm-10  mx-auto">
															<div class="row text-center">
																<div class="col-sm-2">
																	<!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
																</div>

															</div>
														</div>
													</div>
													<!-- <div class="row justify-content-center mt-1 "> -->

													<p><?php echo date("Y-m-d") ?></p>
													<p>Dear <?php echo ucwords("{$emp_details['FName']} {$emp_details['SName']}") ?> </p>
													<h6 style="font-weight: 600">CONFIRMATION LETTER</h6>

													<p>Consequence to the review of your performance during the three (3) months probation period
														from <?php echo $emp_details['EmpDt']->format('j M Y') ?>, we have the pleasure of informing you that your employment is being confirmed
														as <?php

															$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] not in('D') ORDER BY [ItemStart] DESC ";

															$sqR = ScriptRunnercous($sq);

															echo ucwords($sqR['ItemPos']) ?> with effect from <?php
																												$sq = "SELECT *, CONVERT(varchar, ItemExpiry, 107) ItemExpiry FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] not in('D') ORDER BY [ItemStart] DESC ";

																												$sqR = ScriptRunnercous($sq);
																												echo $sqR['ItemExpiry'] ?>.</p>



													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">1. RENUMERATION</p>
													<p style="margin-bottom: 10px;">Your gross monthly pay is stated below and the sum includes consolidated allowances as part your basic salary.</p>



													<table style="width:100%;margin-bottom:10px;">
														<tr>
															<th> Gross Salary </th>
															<th> I. NHF Contribution </th>
															<th> II.Pension Contribution </th>
															<th> III. PAYE(TAX) </th>
															<th> NET Salary </th>
														</tr>

														<tr>
															<td>0.00</td>
															<td>0.00</td>
															<td>0.00</td>
															<td>0.00</td>
															<td>0.00</td>
														</tr>
													</table>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">2. EMPLOYMENT DETAILS</p>
													<p style="margin-bottom: 0px;margin-left: 10px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</p>
													<p style="margin-bottom: 0px;">This is a full-time position. Your regular weekly schedule will be Monday- Friday
														(8am-5pm) and Saturday (9m- 4pm) with one (1) hour lunch break.
													<p style="margin-bottom: 0px;">Your schedule of duties remains the same and/ or may be subject to review solely at School
														Managements directives. If need be, you will be
														asked to work outside the normal hours and be paid for accordingly. </p>
													<p style="margin-bottom: 0px;">The organization will make available information on Public Holiday(s) as declared by the
														Federal Government. However, if you are expected to be on duty on Public Holiday(s), you
														will be informed accordingly by your Supervisor/ Manager.</p>


													<p style="margin-bottom: 0px;margin-left: 10px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">i. <span style="margin-left: 12px;"> Confirmed Employee is entitled to
															any form of Leave.</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">ii. <span style="margin-left: 10px;"> Annual Leave, Maternity Leave, Casual Leave, Compassionate
															Leave is granted but after a period of twelve (12) months in the organization’s employ</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">iii. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

													<ul style="margin-left: 20px;">
														<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
														<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
														<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
														<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
														<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
													</ul>

													<p style="margin-bottom: 0px;margin-left: 10px;">iv. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by Management. </p>


													<p style="margin-bottom: 0px;margin-left: 12px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

													<ul style="margin-left: 20px;">
														<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
														<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
														<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
														<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
													</ul>

													<p style="margin-bottom: 0px;margin-left: 12px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 10px;">3. CONFLICT OF INTEREST POLICY</p>
													<p style="margin-left: 15px;margin-bottom: 30px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest. By signing this Confirmation Agreement, you confirm that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ.</p>


													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
													<p style="margin-left: 15px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
													<p style="margin-left: 15px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by virtue of your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, your duty is to safeguard and not disclose Confidential Information will survive the expiration or termination of this Agreement and/or your employment with the organization</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
													<p style="margin-bottom: 0px;margin-left: 20px;">I. <span style="margin-left: 10px;"> Upon confirmation, the Employees appointment may be terminated if found guilty of serious or gross misconduct or inefficiency. </span> </p>

													<p style="margin-left: 15px;margin-bottom: 0px;">The notice will be given as follows:</p>
													<ul>
														<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
														<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
														<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
														<li>One (1) month where the contract has continued for five (5) years or more.</li>
														<li>Any notice for a period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>

													</ul>


													<p style="margin-bottom: 0px;margin-left: 20px;">II. <span style="margin-left: 10px;"> RESIGNATION </span></p>

													<ul>
														<li>For an Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
														<li>For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in lieu of notice. </li>
														<li>Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
													</ul>

													<p>Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>
													<p>We look forward to your valuable contributions and wish you the very best for a rewarding career with our organisation.</p>
													<p>Please sign the duplicate copy of this letter as a token of acceptance.</p>
													<p>Congratulations! </p>


													<!-- </div> -->

													<div class="row mt-4">
														<div class="col-sm-6 text-right">
															<p>...............................................</p>
															<p style="font-weight: 600">EMPLOYER & DATE</p>
														</div>
														<div class="col-sm-4 ml-4">
															<p>...............................................</p>
															<p style="font-weight: 600">EMPLOYEE & DATE</p>
														</div>
													</div>









													<!-- /.tab-content -->
												</div>
												<!-- /.nav-tabs-custom -->

												<button class="btn btn-sm btn-danger" id="download">Download</button>

											</div>
											<!-- /.col -->



											<div class="tab-pane" id="book2">
												<div id="invoice2" class="text-dark ">

													<!-- <div class="row justify-content-center mt-1 "> -->

													<p><?php echo date("Y-m-d") ?></p>
													<p>Dear <?php echo ucwords("{$emp_details['FName']} {$emp_details['SName']}") ?> </p>
													<h6 style="font-weight: 600">CONFIRMATION LETTER</h6>

													<p>Consequence to the review of your performance during the three (3) months probation
														period from <?php echo $emp_details['EmpDt']->format('j M Y') ?>, we have the pleasure of informing you that your employment
														is being confirmed as a <?php

																				$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] not in('D') ORDER BY [ItemStart] DESC ";

																				$sqR = ScriptRunnercous($sq);

																				echo ucwords($sqR['ItemPos']) ?> with effect from <?php
																																	$sq = "SELECT *, CONVERT(varchar, ItemExpiry, 107) ItemExpiry FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] not in('D') ORDER BY [ItemStart] DESC ";

																																	$sqR = ScriptRunnercous($sq);
																																	echo $sqR['ItemExpiry'] ?>.</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">1. RENUMERATION</p>
													<p style="margin-bottom: 0px;">Your gross monthly pay is stated below and the sum includes consolidated allowances as part your basic salary.</p>

													<div class="row">
														<div class="col-sm-2">
															<p style="margin-bottom: 0px; font-weight: 600">Gross Salary </p>
															<p>0.00</p>
														</div>
														<div class="col-sm-2">
															<p style="margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
															<p>0.00</p>
														</div>
														<div class="col-sm-2">
															<p style="margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
															<p>0.00</p>
														</div>
														<div class="col-sm-2">
															<p style="margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
															<p>0.00</p>
														</div>
														<div class="col-sm-2">
															<p style="margin-bottom: 0px; font-weight: 600"> NET Salary </p>
															<p>0.00</p>
														</div>
													</div>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">2. EMPLOYMENT DETAILS</p>
													<p style="margin-bottom: 0px;margin-left: 10px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</p>
													<p style="margin-bottom: 0px;">This is a full-time position. Your regular weekly schedule will be Monday- Friday
														(7:30am-4pm) and Saturday (8:30am- 2pm) with one (1) hour lunch break.
													<p style="margin-bottom: 0px;">Your schedule of duties remains the same and/ or may be subject to review solely at School
														Principal’s directives. If need be, you will be
														asked to work outside the normal hours and be paid for accordingly. </p>
													<p style="margin-bottom: 0px;">The organization will make available information on Public Holiday(s) as declared by the
														Federal Government. However, if you are expected to be on duty on Public Holiday(s), you
														will be informed accordingly by your School Principal.</p>

													<p style="margin-bottom: 0px;margin-left: 10px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">i. <span style="margin-left: 12px;"> Confirmed Employee is entitled to
															any form of Leave.</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">ii. <span style="margin-left: 10px;"> Annual Leave, Maternity Leave, Casual Leave, Compassionate
															Leave is granted but after a period of twelve (12) months in the organization’s employ</p>
													<p style="margin-bottom: 0px;margin-left: 12px;">iii. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

													<ul style="margin-left: 20px;">
														<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
														<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
														<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
														<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
														<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
													</ul>

													<p style="margin-bottom: 0px;margin-left: 10px;">iv. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Principal’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
													<p style="margin-bottom: 0px;margin-left: 12px;">v. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>

													<p style="margin-bottom: 0px;margin-left: 12px;">vi. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

													<ul style="margin-left: 20px;">
														<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
														<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
														<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
														<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
														<li>Employer (Teacher) who agrees to work during his/ her Annual Leave will be paid the amount equivalent to his/ her daily salary for each day. </li>
														<li>Extended Leave duration without any agreement with the School Principal before embarking on the Leave is regarded as Leave without pay.</li>
													</ul>

													<p style="margin-bottom: 0px;margin-left: 12px;">vii. <span style="margin-left: 10px;font-weight: 600;"> Excused situations/ absences </span> Excused situations/ absences are scheduled or unscheduled time off from work that occurs when an Employee is not present at work during a scheduled work period. Any absence without permission from School Principal attracts warning and monetary penalty.</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
													<p style="margin-left: 15px;margin-bottom: 30px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest. By signing this Confirmation Agreement, you confirm that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ.</p>


													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
													<p style="margin-left: 15px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
													<p style="margin-left: 15px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by virtue of your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, your duty is to safeguard and not disclose Confidential Information will survive the expiration or termination of this Agreement and/or your employment with the organization</p>

													<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
													<p style="margin-bottom: 0px;margin-left: 20px;">I. <span style="margin-left: 10px;"> Upon confirmation, the Employees appointment may be terminated if found guilty of serious or gross misconduct or inefficiency. </span> </p>

													<p style="margin-left: 15px;margin-bottom: 0px;">The notice will be given as follows:</p>
													<ul>
														<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
														<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
														<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
														<li>One (1) month where the contract has continued for five (5) years or more.</li>
														<li>Any notice for a period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>

													</ul>


													<p style="margin-bottom: 0px;margin-left: 20px;">II. <span style="margin-left: 10px;"> RESIGNATION </span></p>

													<ul>
														<li>For an Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
														<li>For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in lieu of notice. </li>
														<li>Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
													</ul>

													<p>Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>
													<p>We look forward to your valuable contributions and wish you the very best for a rewarding career with our organisation.</p>
													<p>Please sign the duplicate copy of this letter as a token of acceptance.</p>
													<p>Congratulations! </p>


													<!-- </div> -->

													<div class="row mt-4">
														<div class="col-sm-6 text-right">
															<p>...............................................</p>
															<p style="font-weight: 600">EMPLOYER & DATE</p>
														</div>
														<div class="col-sm-4 ml-4">
															<p>...............................................</p>
															<p style="font-weight: 600">EMPLOYEE & DATE</p>
														</div>
													</div>






													<!-- /.tab-content -->
												</div>
												<!-- /.nav-tabs-custom -->
												<button class="btn btn-sm btn-danger" id="download2">Download</button>

											</div>
											<!-- /.col -->

											<!-- <div class="tab-content"> -->










										</div>
									</div>













								</div>











							</div>

						<?php endif; ?>






















						</div>
					</div>
					<!-- /.box-body -->




			</div>

















			<!-- /.row -->
	</section>
	<!-- /.content -->












	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="html2pdf/dist/html2pdf.bundle.js"></script>


	<script>
		window.onload = function() {
			document.getElementById("download").addEventListener("click", () => {
				const invoice = document.getElementById("invoice");
				const opt = {
					margin: [.75, .75, .75, .75],
					image: {
						type: 'jpeg',
						quality: 1
					},
					jsPDF: {
						unit: 'cm',
						orientation: 'p'
					}
				};

				// html2pdf().from(invoice).set(opt).save();

				html2pdf().from(invoice).set(opt).toPdf().get('pdf').then(function(pdf) {
					// Open the PDF in a new tab
					const pdfBlob = pdf.output('blob');
					const pdfURL = URL.createObjectURL(pdfBlob);
					window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

					// Alternatively, trigger the browser print dialog directly
					window.open(pdfURL).print();
				});
			})

			document.getElementById("download2").addEventListener("click", () => {
				const invoice2 = document.getElementById("invoice2");
				const opt = {
					margin: [.5, .5, .5, .5],
					image: {
						type: 'jpeg',
						quality: 1
					},
					jsPDF: {
						unit: 'cm',
						orientation: 'p'
					}
				};

				// html2pdf().from(invoice2).set(opt).save();

				html2pdf().from(invoice2).set(opt).toPdf().get('pdf').then(function(pdf) {
					// Open the PDF in a new tab
					const pdfBlob = pdf.output('blob');
					const pdfURL = URL.createObjectURL(pdfBlob);
					window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

					// Alternatively, trigger the browser print dialog directly
					window.open(pdfURL).print();
				});
			})

		}
	</script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->


	<script>
		$('#example1').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example6').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example7').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		setInterval(function() {
			$(".tablesorter-filter-row").remove();
		}, 2000);

		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});













































		}

		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select an employee', 'red');
				};

				return;

			} else {

				$('#theform1').submit();






			}



		});
	</script>
</body>

</html>
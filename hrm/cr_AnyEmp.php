<?php
error_reporting(0);
@ini_set('display_errors', 0);

session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = false;
include '../css/myscripts.php';


//Validate user viewing rights
//if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="ModifySelf")
//{
//    if (ValidateURths("PERSONAL"."V")!=true){include '../main/NoAccess.php';exit;}
//}
//else
//{
//if (ValidateURths("EMPLOYEE"."V")!=true){include '../main/NoAccess.php';exit;}
//}


if (ValidateURths("EMPLOYEE" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}
$ModMaster = ValidateURths("HR MODULE MASTERS" . "A");
//echo $ModMaster;
//exit;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
if (!isset($_REQUEST["SndBk_Acct"])) {
	$_REQUEST["SndBk_Acct"] = "";
}
if (!isset($_SESSION['DoS' . $_REQUEST["PgDoS"]])) {
	$_SESSION['DoS' . $_REQUEST["PgDoS"]] = "";
}
if (!isset($EditID)) {
	$EditID = "";
}
if (!isset($AllEmployee)) {
	$AllEmployee = "";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "";
}
if (!isset($_REQUEST["AcctNo"])) {
	$_REQUEST["AcctNo"] = "";
}
if (!isset($_REQUEST["PgTy"])) {
	$_REQUEST["PgTy"] = "";
}
$SelID = "";
// var_dump($_REQUEST);
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Employee") {
		if (ValidateURths("EMPLOYEE" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		include 'add_emp_validation.php';
		/*    echo ("<script type='text/javascript'>{ parent.msgbox('Demo version. New users cannot be created', 'green'); }</script>");
        exit; */

		if ($GoValidate == true) {
			$ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
			$AutoID = strtolower(ScriptRunner($ScriptEmpIDP, "SetValue"));
			// if ( $AutoID != "on")
			// {
			$Script = "Select * from EmpTbl where EmpID='" . ECh($_REQUEST["EmpID"]) . "'";
			if (strlen(ScriptRunner($Script, "EmpID")) > 0 || strlen(ScriptRunner($Script, "PENCommNo") > 0) || strlen(ScriptRunner($Script, "Email") > 0)) {
				echo ("<script type='text/javascript'>{parent.msgbox('A record with some duplicates already exist. Please review.', 'red'); }</script>");
				$GoValidate = false;
			}
			// }
		}
		$totalEmp = ScriptRunnerParent("SELECT * FROM DbConnStr WHERE CustID = '" . $_SESSION["StkTck" . "CustID"] . "'")['Employees'];
		if ($totalEmp) {
			if ((int) ScriptRunner("SELECT COUNT(EmpID) as Active From EmpTbl WHERE EmpStatus = 'Active' AND EmpID <> '000000' and  Status not in('D') ", "Active") >= $totalEmp) {
				echo ("<script type='text/javascript'>{parent.msgbox('Error adding an active employee record.<br>You may have exhausted your licenses.<br>Please contact your vendor.', 'red'); }</script>");
				$GoValidate = false;
			}
		}

		if ($GoValidate == true) {
			/* Create a HashKey of the Row ID  as identifier for each unique staff record*/

			$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Employee" . $_SESSION["StkTck" . "UName"];
			$HashKey = md5($UniqueKey);
			if (isset($_REQUEST["Expatriate"]) && $_REQUEST["Expatriate"] == 'on') {
				$Exprt = 1;
			} else {
				$Exprt = 0;
			}

			/* Create New Employee ID by selecting last employee ID if set to Automatic */
			$ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
			if ($AutoID == "on") {
				$ScriptEmpIDP = "Select * from Settings where Setting='EmpIDPSCnt'"; // and Status='A'
				$SetVal = ScriptRunner($ScriptEmpIDP, "SetValue");
				$SetVal2 = ScriptRunner($ScriptEmpIDP, "SetValue2") + 1;
				$SetVal3 = ScriptRunner($ScriptEmpIDP, "SetValue3");
				$SetVal4 = ScriptRunner($ScriptEmpIDP, "SetValue4");
				$DigitsDsp_ = ScriptRunner("Select * from Settings where Setting='AutoEmpID'", "SetValue2");

				if ($DigitsDsp_ == "Long") {
					if (strlen($SetVal2) == 1) {
						$SetVal2 = "000" . $SetVal2;
					} elseif (strlen($SetVal2) == 2) {
						$SetVal2 = "00" . $SetVal2;
					} elseif (strlen($SetVal2) == 3) {
						$SetVal2 = "0" . $SetVal2;
					}
				}

				if (trim($SetVal3) != "") {
					$SetVal5 = $SetVal4;
				} else {
					$SetVal5 = "";
				}

				$LoginDet = $SetVal . $SetVal4 . $SetVal2 . $SetVal5 . $SetVal3;
				$LoginDet = str_replace('#YYYY#', date('Y'), $LoginDet);
				$LoginDet = str_replace('#YY#', date('y'), $LoginDet);
				$LoginDet = str_replace('#MM#', date('m'), $LoginDet);
				$LoginDet = str_replace('#SD#', date('Y'), $LoginDet); //State or District of Origin - CODE
				$LoginDet = str_replace('#CY#', date('Y'), $LoginDet); //Country of Origin - CODE
				$LoginDet = str_replace('#DP#', date('Y'), $LoginDet); //Employee Department - CODE
			} else {
				$LoginDet = $_REQUEST["EmpID"];
			}

			if (isset($_REQUEST["EmpDisabled"])) {
				$EmpDis = $_REQUEST["EmpDisabled"];
			} else {
				$EmpDis = "";
			}
			//Set employee Brance ID
			if (!isset($_REQUEST["BranchID"]) || (isset($_REQUEST["BranchID"]) && (trim($_REQUEST["BranchID"]) == "--" || trim($_REQUEST["BranchID"]) == ""))) {
				$EmpBranchID = ScriptRunner("Select BranchID from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'", "BranchID");
			} else {
				$EmpBranchID = ECh(trim($_REQUEST["BranchID"]));
			}
			$Script = "INSERT INTO [EmpTbl] ([EmpID],[EmpMgr],[EmpMgr_1],[ProMgr],[EmpStatus],[EmpDt],[SName],[FName],[ONames],[DOB],[Sex],[Title],[COO],[SOO],[LGA],[Department],[MStatus],[Relg],[EmpType],[Email],[Mobile],[Phone],[HmAdd],[OffAdd],[BloodGrp],[GenoType],[EmpDisabled],[KnownIllment],[NOK1Name],[NOK2Name],[NOK1Rel],[NOK2Rel],[NOK1Add],[NOK2Add],[NOK1Mobile],[NOK2Mobile],[SalBank],[SalAcctNo],[SalSortCode],[PENCommNo],[PenCustID],[TaxID],[TaxState],[HInsurNo],[name_spouse],
[spouse_residence],
[Sec_Email],
[bank_code],
[pfa_code],
[spouse_hmo_id]
,[child1_id]
,[child2_id]
,[child3_id]
,[child4_id]
,[card_exp],
[NIN],
[EmpCategory],
[spouse_address],
[spouse_phone],
[no_children],
[no_dependant],
[hmo_plan],
[hmo_id],
[primary_hospital],
[hospital_address],
[hospital_contact_1],
[hospital_contact_2],
[hmo_contact_1],
[hmo_contact_2],
	[MediCareNo],[StfImg],[BranchID],[ExitDt],[Expatriate],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . trim(ECh($LoginDet)) . "','" . trim(ECh($_REQUEST["EmpMgr"])) . "','" . trim(ECh($_REQUEST["EmpMgr_1"])) . "','" . trim(ECh($_REQUEST["ProMgr"])) . "','" . trim(ECh($_REQUEST["EmpStatus"])) . "','" . $EmpDt . "','" . ECh(strtoupper($_REQUEST["SName"])) . "','" . ECh(trim($_REQUEST["FName"])) . "','" . ECh(trim($_REQUEST["ONames"])) . "','" . $DOB . "','" . $_REQUEST["Sex"] . "','" . ECh($_REQUEST["Title"]) . "','" . ECh($_REQUEST["COO"]) . "','" . ECh($_REQUEST["SOO"]) . "','" . ECh($_REQUEST["LGA"]) . "','" . ECh($_REQUEST["Department"]) . "','" . ECh($_REQUEST["MStatus"]) . "','" . ECh($_REQUEST["Relg"]) . "','" . ECh($_REQUEST["EmpType"]) . "','" . ECh(trim($_REQUEST["Email"])) . "','" . ECh(trim($_REQUEST["Mobile"])) . "','" . ECh(trim($_REQUEST["Phone"])) . "','" . ECh($_REQUEST["HmAdd"]) . "','" . ECh($_REQUEST["OffAdd"]) . "','" . ECh($_REQUEST["BloodGrp"]) . "','" . ECh($_REQUEST["GenoType"]) . "','" . $EmpDis . "','" . ECh($_REQUEST["KnownIllment"]) . "','" . ECh($_REQUEST["NOK1Name"]) . "','" . ECh($_REQUEST["NOK2Name"]) . "','" . ECh($_REQUEST["NOK1Rel"]) . "','" . ECh($_REQUEST["NOK2Rel"]) . "','" . ECh($_REQUEST["NOK1Add"]) . "','" . ECh($_REQUEST["NOK2Add"]) . "','" . ECh($_REQUEST["NOK1Mobile"]) . "','" . ECh($_REQUEST["NOK2Mobile"]) . "','" . ECh(trim($_REQUEST["SalBank"])) . "','" . ECh(trim($_REQUEST["SalAcctNo"])) . "','" . ECh($_REQUEST["SalSortCode"]) . "','" . ECh($_REQUEST["PENCommNo"]) . "','" . ECh($_REQUEST["PenCustID"]) . "','" . ECh($_REQUEST["TaxID"]) . "','" . ECh($_REQUEST["TaxState"]) . "',
	'" . ECh($_REQUEST["HInsurNo"]) . "',
	'" . ECh($_REQUEST["name_spouse"]) . "',
'" . ECh($_REQUEST["spouse_residence"]) . "',
'" . ECh($_REQUEST["Sec_Email"]) . "',
'" . ECh($_REQUEST["bank_code"]) . "',
'" . ECh($_REQUEST["pfa_code"]) . "',
'" . ECh($_REQUEST["spouse_hmo_id"]) . "',
'" . ECh($_REQUEST["child1_id"]) . "',
'" . ECh($_REQUEST["child2_id"]) . "',
'" . ECh($_REQUEST["child3_id"]) . "',
'" . ECh($_REQUEST["child4_id"]) . "',
'" . ECh($_REQUEST["card_exp"]) . "',
'" . ECh($_REQUEST["NIN"]) . "',
'" . ECh($_REQUEST["EmpCategory"]) . "',
'" . ECh($_REQUEST["spouse_address"]) . "',
'" . ECh($_REQUEST["spouse_phone"]) . "',
'" . ECh($_REQUEST["no_children"]) . "',
'" . ECh($_REQUEST["no_dependant"]) . "',
'" . ECh($_REQUEST["hmo_plan"]) . "',
'" . ECh($_REQUEST["hmo_id"]) . "',
'" . ECh($_REQUEST["primary_hospital"]) . "',
'" . ECh($_REQUEST["hospital_address"]) . "',
'" . ECh($_REQUEST["hospital_contact_1"]) . "',
'" . ECh($_REQUEST["hospital_contact_2"]) . "',
'" . ECh($_REQUEST["hmo_contact_1"]) . "',
'" . ECh($_REQUEST["hmo_contact_2"]) . "',
	'" . ECh($_REQUEST["MediCareNo"]) . "',
	'pfimg/Default.jpg',
	'" . ECh($EmpBranchID) . "',
	" . $ExtDt . "," . $Exprt . ",
	'N',
	'" . ECh($_SESSION["StkTck" . "HKey"]) . "',
	GetDate(),
	'" . $HashKey . "')";
			ScriptRunnerUD($Script, "Inst");
			$Script = "Update Settings set SetValue2=(SetValue2 + 1) where Setting='EmpIDPSCnt'";
			ScriptRunnerUD($Script, "Inst");

			AuditLog("INSERT", "New employee record created for [" . ECh($_REQUEST["SName"] . " " . $_REQUEST["FName"] . " " . $_REQUEST["ONames"]) . "]");

			//if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
			//{
			//Formally sending, decativated on 22/07/2016
			//MailTrail("EMPLOYEE","T",'','','',($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
			//}
			include 'mail_trail_send_kpi.php';

			echo ("<script type='text/javascript'>{ parent.msgbox('New employee record created successfully.', 'green'); }</script>");
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************

	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "ReActivate") {
		include 'add_emp_validation.php';
		$HashKey = ECh($_REQUEST["UpdID"]);
		$EditID = $HashKey;

		$Script = "UPDATE [EmpTbl] set [EmpStatus]='Active',[exitreasons]='',[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]='" . $HashKey . "'";
		ScriptRunnerUD($Script, "Inst");
		//echo $Script;
		AuditLog("UPDATE", "Employee record re-activated for [" . ECh($_REQUEST["SName"] . " " . $_REQUEST["FName"] . " " . $_REQUEST["ONames"]) . "]");

		//if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
		//{
		//MailTrail("EMPLOYEE ACTIVATION","T",'','','',ECh($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
		//MailTrail("EMPLOYEE","T",'','','',ECh($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
		//}
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Record") // && $_REQUEST["DelMax"] > 0)
	{
		if (ValidateURths("EMPLOYEE" . "B") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$is_new_emp_id = $_REQUEST['EmpID'];
		$Script_id = "Select EmpID from [EmpTbl] where HashKey='" . ECh($_REQUEST["UpdID"]) . "'";
		$former_emp_id = ScriptRunner($Script_id, "EmpID");
		$emp_id_changed = $former_emp_id === $is_new_emp_id;



		include 'add_emp_validation.php';
		$HashKey = ECh($_REQUEST["UpdID"]);
		$EditID = $HashKey;

		if (isset($_REQUEST["Expatriate"]) && $_REQUEST["Expatriate"] == 'on') {
			$Exprt = 1;
		} else {
			$Exprt = 0;
		}

		/*    echo ("<script type='text/javascript'>{ parent.msgbox('Demo version. New users cannot be created', 'green'); }</script>");
        exit; */

		/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
		if ($GoValidate == true) {
			$HashKey = ECh($_REQUEST["UpdID"]);
			$EditID = $HashKey;
			if (isset($_REQUEST["EmpDisabled"])) {
				$EmpDis = $_REQUEST["EmpDisabled"];
			} else {
				$EmpDis = "";
			}

			//Set employee Brance ID
			if (isset($_REQUEST["BranchID"]) && trim($_REQUEST["BranchID"]) == "--") {
				$EmpBranchID = ScriptRunner("Select BranchID from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'", "BranchID");
			} else {
				$EmpBranchID = ECh(trim($_REQUEST["BranchID"]));
			}
			$Script = "UPDATE [EmpTbl] set [EmpID]='" . ECh($_REQUEST["EmpID"]) . "',[EmpMgr]='" . ECh($_REQUEST["EmpMgr"]) . "',[EmpMgr_1]='" . ECh($_REQUEST["EmpMgr_1"]) . "',
	[ProMgr]='" . ECh($_REQUEST["ProMgr"]) . "',
	[name_spouse]='" . ECh($_REQUEST["name_spouse"]) . "',
	[spouse_residence]='" . ECh($_REQUEST["spouse_residence"]) . "',
	[spouse_hmo_id]='" . ECh($_REQUEST["spouse_hmo_id"]) . "',
	[child1_id]='" . ECh($_REQUEST["child1_id"]) . "',
	[child2_id]='" . ECh($_REQUEST["child2_id"]) . "',
	[child3_id]='" . ECh($_REQUEST["child3_id"]) . "',
	[child4_id]='" . ECh($_REQUEST["child4_id"]) . "',
	[card_exp]='" . ECh($_REQUEST["card_exp"]) . "',
	[NIN]='" . ECh($_REQUEST["NIN"]) . "',
	[EmpCategory]='" . ECh($_REQUEST["EmpCategory"]) . "',
	[spouse_address]='" . ECh($_REQUEST["spouse_address"]) . "',
	[spouse_phone]='" . ECh($_REQUEST["spouse_phone"]) . "',
	[no_children]='" . ECh($_REQUEST["no_children"]) . "',
	[no_dependant]='" . ECh($_REQUEST["no_dependant"]) . "',
	[hmo_plan]='" . ECh($_REQUEST["hmo_plan"]) . "',
	[hmo_id]='" . ECh($_REQUEST["hmo_id"]) . "',
	[Sec_Email]='" . ECh($_REQUEST["Sec_Email"]) . "',
	[bank_code]='" . ECh($_REQUEST["bank_code"]) . "',
	[pfa_code]='" . ECh($_REQUEST["pfa_code"]) . "',
	[primary_hospital]='" . ECh($_REQUEST["primary_hospital"]) . "',
	[hospital_address]='" . ECh($_REQUEST["hospital_address"]) . "',
	[hospital_contact_1]='" . ECh($_REQUEST["hospital_contact_1"]) . "',
	[hospital_contact_2]='" . ECh($_REQUEST["hospital_contact_2"]) . "',
	[hmo_contact_1]='" . ECh($_REQUEST["hmo_contact_1"]) . "',
	[hmo_contact_2]='" . ECh($_REQUEST["hmo_contact_2"]) . "',
	[exitreasons]='" . ECh($_REQUEST["exitreasons"]) . "',
	[EmpStatus]='" . ECh($_REQUEST["EmpStatus"]) . "',[EmpDt]='" . $EmpDt . "',[SName]='" . ECh(trim($_REQUEST["SName"])) . "',[FName]='" . ECh($_REQUEST["FName"]) . "',[ONames]='" . ECh($_REQUEST["ONames"]) . "',[DOB]='" . $DOB . "',[Sex]='" . ECh($_REQUEST["Sex"]) . "',[Title]='" . ECh($_REQUEST["Title"]) . "',[COO]='" . ECh($_REQUEST["COO"]) . "',[SOO]='" . ECh($_REQUEST["SOO"]) . "',[LGA]='" . ECh($_REQUEST["LGA"]) . "',[Department]='" . ECh($_REQUEST["Department"]) . "',[MStatus]='" . ECh($_REQUEST["MStatus"]) . "',[Relg]='" . ECh($_REQUEST["Relg"]) . "',[EmpType]='" . ECh($_REQUEST["EmpType"]) . "',[Email]='" . ECh(trim($_REQUEST["Email"])) . "',[Mobile]='" . ECh(trim($_REQUEST["Mobile"])) . "',[Phone]='" . ECh($_REQUEST["Phone"]) . "',[HmAdd]='" . ECh(trim($_REQUEST["HmAdd"])) . "',[OffAdd]='" . ECh($_REQUEST["OffAdd"]) . "',[BloodGrp]='" . ECh($_REQUEST["BloodGrp"]) . "',[GenoType]='" . ECh($_REQUEST["GenoType"]) . "',[EmpDisabled]='" . $EmpDis . "',[KnownIllment]='" . ECh($_REQUEST["KnownIllment"]) . "',[NOK1Name]='" . ECh($_REQUEST["NOK1Name"]) . "',[NOK2Name]='" . ECh($_REQUEST["NOK2Name"]) . "',[NOK1Rel]='" . ECh($_REQUEST["NOK1Rel"]) . "',[NOK2Rel]='" . ECh(trim($_REQUEST["NOK2Rel"])) . "',[NOK1Add]='" . ECh($_REQUEST["NOK1Add"]) . "',[NOK2Add]='" . ECh(trim($_REQUEST["NOK2Add"])) . "',[NOK1Mobile]='" . ECh(trim($_REQUEST["NOK1Mobile"])) . "',[NOK2Mobile]='" . ECh(trim($_REQUEST["NOK2Mobile"])) . "',[SalBank]='" . ECh(trim($_REQUEST["SalBank"])) . "',[SalAcctNo]='" . ECh(trim($_REQUEST["SalAcctNo"])) . "',[SalSortCode]='" . ECh(trim($_REQUEST["SalSortCode"])) . "',[PENCommNo]='" . ECh(trim($_REQUEST["PENCommNo"])) . "',[PenCustID]='" . ECh($_REQUEST["PenCustID"]) . "',[TaxID]='" . ECh(trim($_REQUEST["TaxID"])) . "',[TaxState]='" . ECh(trim($_REQUEST["TaxState"])) . "',[HInsurNo]='" . ECh(trim($_REQUEST["HInsurNo"])) . "',[BranchID]='" . ECh(trim($EmpBranchID)) . "',[MediCareNo]='" . ECh(trim($_REQUEST["MediCareNo"])) . "',[ExitDt]=" . ($ExtDt) . ",[Expatriate]=" . $Exprt . ",[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]='" . $HashKey . "'";
			ScriptRunnerUD($Script, "Inst");

			// if(!$emp_id_changed){

			// 	$script_up= "Update [Users] set  [EmpID]='" . ECh($_REQUEST["EmpID"]) . "', [LogName]='" . ECh($_REQUEST["EmpID"]). "' where HashKey='".ECh($_REQUEST["UpdID"])."'" ;
			// 	ScriptRunnerUD($script_up, "Inst");
			// }
			//echo $Script;

			$Script_up = "Select * from Settings where Setting='HRSignedDoc'";
			$up_details_1 = ScriptRunner($Script_up, "SetValue9");
			$up_details_2 = ScriptRunner($Script_up, "SetValue10");
			if (isset($up_details_1) && strlen($up_details_1) === 32) {
				//    send mail to notifier 1
				$notifier_details = getUserDetailsfromHash($up_details_1);
				$loggin_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

				$msg = "Dear #Recipent_New_Employee_Updated1#, <br/> A new Employee Profile(#updated_profile#) has been updated by #Logged_In_User_Updated_Record#. <br/><br/><br/> For more information, contact your HR. <br/> Thank you.";
				$msg = str_replace('#Recipent_New_Employee_Updated1#', "{$notifier_details['SName']} {$notifier_details['FName']} {$notifier_details['OName']}", $msg);
				$msg = str_replace('#Logged_In_User_Updated_Record#', "{$loggin_details['SName']} {$loggin_details['FName']} {$loggin_details['OName']}", $msg);
				$msg = str_replace('#updated_profile#', "{$_REQUEST['SName']} {$_REQUEST['FName']} {$_REQUEST['OName']}", $msg);
				$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
				$HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
				$Atth = "";
				$Tm = "GetDate()";
				$To = $notifier_details['Email'];
				$Subj = "Employee Profile Updated";

				$Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
				// var_dump($Script_Mail);
				ScriptRunnerUD($Script_Mail, "QueueMail");
			}
			if (isset($up_details_2) && strlen($up_details_2) === 32) {
				//    send mail to notifier 2
				$notifier_details = getUserDetailsfromHash($up_details_2);
				$loggin_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

				$msg = "Dear #Recipent_New_Employee_Updated1#, <br/> A new Employee Profile(#updated_profile#) has been updated by #Logged_In_User_Updated_Record#. <br/><br/><br/> For more information, contact your HR. <br/> Thank you.";
				$msg = str_replace('#Recipent_New_Employee_Updated1#', "{$notifier_details['SName']} {$notifier_details['FName']} {$notifier_details['OName']}", $msg);
				$msg = str_replace('#Logged_In_User_Updated_Record#', "{$loggin_details['SName']} {$loggin_details['FName']} {$loggin_details['OName']}", $msg);
				$msg = str_replace('#updated_profile#', "{$_REQUEST['SName']} {$_REQUEST['FName']} {$_REQUEST['OName']}", $msg);
				$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
				$HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
				$Atth = "";
				$Tm = "GetDate()";
				$To = $notifier_details['Email'];
				$Subj = "Employee Profile Updated";

				$Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
				// var_dump($Script_Mail);
				ScriptRunnerUD($Script_Mail, "QueueMail");
			}

			//Added by SEUN AND SHOLA FOR TRAINING MODULE
			$query = ("SELECT * from EmpTbl where HashKey = '" . ECh($_REQUEST["EmpMgr"]) . "'");
			$managerDetails = ScriptRunnercous($query);
			$Script = "UPDATE PreEva SET [Manager] = '" . $managerDetails['EmpID'] . "' WHERE [Status2] ='OG' AND [EmpId]='" . ECh($_REQUEST["EmpID"]) . "'";
			ScriptRunnerUD($Script, "Inst");
			//END

			if (isset($_REQUEST["EmpStatus"]) && $_REQUEST["EmpStatus"] != "Active") {
				"UPDATE [EmpTbl] set Status='I' where [HashKey]='" . $HashKey . "'";
				ScriptRunnerUD($Script, "Inst");
				AuditLog("UPDATE", "Employee record updated for [" . ECh($_REQUEST["SName"] . " " . $_REQUEST["FName"] . " " . $_REQUEST["ONames"]) . "]");

				//if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
				//{
				//Formally sending, decativated on 22/07/2016
				//MailTrail("EMPLOYEE","T",'','','',ECh($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
				//}
			} else {
				AuditLog("UPDATE", "Employee record updated for [" . ECh($_REQUEST["SName"] . " " . $_REQUEST["FName"] . " " . $_REQUEST["ONames"]) . "]");
				//if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
				//{
				//MailTrail("EMPLOYEE","T",'','',''); //Sends a mail notifications
				//Formally sending, decativated on 22/07/2016
				//MailTrail("EMPLOYEE","T",'','','',ECh($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
				//}
			}
			include 'mail_trail_send_kpi.php';

			echo ("<script type='text/javascript'>{ parent.msgbox('Employee record was updated successfully.', 'green'); }</script>");
		}
	}
	//===============================================================================
	//===============================================================================
	//SPECIAL FOR EMPLOYEE'S TO UPDATE THEIR RECORDS DIRECTLY
	//===============================================================================
	//===============================================================================

	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update My Record" && isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
		if (ValidateURths("PERSONAL" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}
		// var_dump('here');
		// die();

		include 'add_emp_validation.php';
		$HashKey = ECh($_REQUEST["UpdID"]);
		$EditID = $HashKey;

		/*    echo ("<script type='text/javascript'>{ parent.msgbox('Demo version. New users cannot be created', 'green'); }</script>");
        exit; */

		/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
		if ($GoValidate == true) {
			$HashKey = ECh($_REQUEST["UpdID"]);
			$EditID = $HashKey;

			if (isset($_REQUEST["EmpDisabled"])) {
				$EmpDis = $_REQUEST["EmpDisabled"];
			} else {
				$EmpDis = "";
			}

			//Set employee Brance ID
			if (isset($_REQUEST["BranchID"]) && trim($_REQUEST["BranchID"]) == "--") {
				$EmpBranchID = ScriptRunner("Select BranchID from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'", "BranchID");
			} else {
				$EmpBranchID = ECh(trim($_REQUEST["BranchID"]));
			}
			$Script = "UPDATE [EmpTbl] set [EmpID]='" . ECh($_REQUEST["EmpID"]) . "',[EmpMgr]='" . ECh($_REQUEST["EmpMgr"]) . "',[EmpMgr_1]='" . ECh($_REQUEST["EmpMgr_1"]) . "',
	[ProMgr]='" . ECh($_REQUEST["ProMgr"]) . "',
	[name_spouse]='" . ECh($_REQUEST["name_spouse"]) . "',
	[spouse_residence]='" . ECh($_REQUEST["spouse_residence"]) . "',
	[spouse_hmo_id]='" . ECh($_REQUEST["spouse_hmo_id"]) . "',
	[child1_id]='" . ECh($_REQUEST["child1_id"]) . "',
	[child2_id]='" . ECh($_REQUEST["child2_id"]) . "',
	[child3_id]='" . ECh($_REQUEST["child3_id"]) . "',
	[child4_id]='" . ECh($_REQUEST["child4_id"]) . "',
	[card_exp]='" . ECh($_REQUEST["card_exp"]) . "',
	[NIN]='" . ECh($_REQUEST["NIN"]) . "',
	[EmpCategory]='" . ECh($_REQUEST["EmpCategory"]) . "',
	[spouse_address]='" . ECh($_REQUEST["spouse_address"]) . "',
	[spouse_phone]='" . ECh($_REQUEST["spouse_phone"]) . "',
	[no_children]='" . ECh($_REQUEST["no_children"]) . "',
	[no_dependant]='" . ECh($_REQUEST["no_dependant"]) . "',
	[hmo_plan]='" . ECh($_REQUEST["hmo_plan"]) . "',
	[hmo_id]='" . ECh($_REQUEST["hmo_id"]) . "',
	[primary_hospital]='" . ECh($_REQUEST["primary_hospital"]) . "',
	[Sec_Email]='" . ECh($_REQUEST["Sec_Email"]) . "',
	[bank_code]='" . ECh($_REQUEST["bank_code"]) . "',
	[pfa_code]='" . ECh($_REQUEST["pfa_code"]) . "',
	[hospital_address]='" . ECh($_REQUEST["hospital_address"]) . "',
	[hospital_contact_1]='" . ECh($_REQUEST["hospital_contact_1"]) . "',
	[hospital_contact_2]='" . ECh($_REQUEST["hospital_contact_2"]) . "',
	[hmo_contact_1]='" . ECh($_REQUEST["hmo_contact_1"]) . "',
	[hmo_contact_2]='" . ECh($_REQUEST["hmo_contact_2"]) . "',
	[exitreasons]='" . ECh($_REQUEST["exitreasons"]) . "',
	[EmpStatus]='" . ECh($_REQUEST["EmpStatus"]) . "',[EmpDt]='" . $EmpDt . "',[SName]='" . ECh(trim($_REQUEST["SName"])) . "',[FName]='" . ECh($_REQUEST["FName"]) . "',[ONames]='" . ECh($_REQUEST["ONames"]) . "',[DOB]='" . $DOB . "',[Sex]='" . ECh($_REQUEST["Sex"]) . "',[Title]='" . ECh($_REQUEST["Title"]) . "',[COO]='" . ECh($_REQUEST["COO"]) . "',[SOO]='" . ECh($_REQUEST["SOO"]) . "',[LGA]='" . ECh($_REQUEST["LGA"]) . "',[Department]='" . ECh($_REQUEST["Department"]) . "',[MStatus]='" . ECh($_REQUEST["MStatus"]) . "',[Relg]='" . ECh($_REQUEST["Relg"]) . "',[EmpType]='" . ECh($_REQUEST["EmpType"]) . "',[Email]='" . ECh(trim($_REQUEST["Email"])) . "',[Mobile]='" . ECh(trim($_REQUEST["Mobile"])) . "',[Phone]='" . ECh($_REQUEST["Phone"]) . "',[HmAdd]='" . ECh(trim($_REQUEST["HmAdd"])) . "',[OffAdd]='" . ECh($_REQUEST["OffAdd"]) . "',[BloodGrp]='" . ECh($_REQUEST["BloodGrp"]) . "',[GenoType]='" . ECh($_REQUEST["GenoType"]) . "',[EmpDisabled]='" . $EmpDis . "',[KnownIllment]='" . ECh($_REQUEST["KnownIllment"]) . "',[NOK1Name]='" . ECh($_REQUEST["NOK1Name"]) . "',[NOK2Name]='" . ECh($_REQUEST["NOK2Name"]) . "',[NOK1Rel]='" . ECh($_REQUEST["NOK1Rel"]) . "',[NOK2Rel]='" . ECh(trim($_REQUEST["NOK2Rel"])) . "',[NOK1Add]='" . ECh($_REQUEST["NOK1Add"]) . "',[NOK2Add]='" . ECh(trim($_REQUEST["NOK2Add"])) . "',[NOK1Mobile]='" . ECh(trim($_REQUEST["NOK1Mobile"])) . "',[NOK2Mobile]='" . ECh(trim($_REQUEST["NOK2Mobile"])) . "',[SalBank]='" . ECh(trim($_REQUEST["SalBank"])) . "',[SalAcctNo]='" . ECh(trim($_REQUEST["SalAcctNo"])) . "',[SalSortCode]='" . ECh(trim($_REQUEST["SalSortCode"])) . "',[PENCommNo]='" . ECh(trim($_REQUEST["PENCommNo"])) . "',[PenCustID]='" . ECh($_REQUEST["PenCustID"]) . "',[TaxID]='" . ECh(trim($_REQUEST["TaxID"])) . "',[TaxState]='" . ECh(trim($_REQUEST["TaxState"])) . "',[HInsurNo]='" . ECh(trim($_REQUEST["HInsurNo"])) . "',[BranchID]='" . ECh(trim($EmpBranchID)) . "',[MediCareNo]='" . ECh(trim($_REQUEST["MediCareNo"])) . "',[ExitDt]=" . ($ExtDt) . ",[Expatriate]=" . $Exprt . ",[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]='" . $HashKey . "'";
			ScriptRunnerUD($Script, "Inst");
			//Set Update Request==False
			$Script = "Update [EmpUpdReq] set Status='D', [UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where EmpID='" . $HashKey . "' and Status<>'D'";
			ScriptRunnerUD($Script, "Inst");

			AuditLog("UPDATE", "Employee self record updated for [" . ECh($_REQUEST["SName"] . " " . $_REQUEST["FName"] . " " . $_REQUEST["ONames"]) . "]");
			//Formally sending, decativated on 22/07/2016
			// MailTrail("EMPLOYEE","T",'','','',ECh($_REQUEST["SName"]." ".$_REQUEST["FName"]." ".$_REQUEST["ONames"])); //Sends a mail notifications
			include 'mail_trail_send_kpi.php';

			echo ("<script type='text/javascript'>{ parent.msgbox('Employee record was updated successfully.', 'green'); }</script>");
		}
	} elseif ($_REQUEST["SubmitTrans"] == "Open") { //echo $_REQUEST["AcctNo"]."sfdsdjksjdksdsd";
		if (strlen($_REQUEST["AcctNo"]) > 2) {
			$EditID = $_REQUEST["AcctNo"];
		} else {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select an employee record to open', 'red'); }</script>");
			$EditID = '';
		}
	}
}
//For send back accounts from the search list
/*
if ($_REQUEST["SndBk_Acct"] != "")
{
if (strlen($_REQUEST["SndBk_Acct"])>2)
{
$EditID = ECh($_REQUEST["SndBk_Acct"]);
}
else
{
$EditID = '--';
}
}
 */
if (isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32 && strlen($EditID) != 32) {
	if (strlen($_REQUEST["AcctNo"]) > 2) {
		$EditID = ECh($_REQUEST["AcctNo"]);
	} else {
		$EditID = '--';
	}
}
?>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
			<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
			} else { ?>
				$("#ExDt").datepicker({
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					minDate: "-1Y",
					maxDate: "+1D",
					dateFormat: 'dd M yy',
					yearRange: "-75:+75"
				})
				$("#EmpDt").datepicker({
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					minDate: "-80Y",
					maxDate: "+1M",
					dateFormat: 'dd M yy',
					yearRange: "-75:+75"
				})
				$("#card_exp").datepicker({
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					minDate: "-80Y",
					maxDate: "+80Y",
					dateFormat: 'dd M yy',
					yearRange: "-75:+75"
				})

				$("#ClearDate").click(function() {
					document.getElementById("ExDt").value = '';
				});
			<?php } ?>
			$("#DOB").datepicker({
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: "-80Y",
				maxDate: "+80Y",
				dateFormat: 'dd M yy',
				yearRange: "-75:+75"
			})
		});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
</head>

<body>

	<form action="#" method="post" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-4">
						<?php if ($_REQUEST["PgTy"] == "ModifySelf") { ?>
							<div><span style="font-size: 15px; display: inline-block; margin: 10px;">Employee:</span>
								<span style="font-size: 11px;">&nbsp;
									<?php
									if ($_REQUEST["PgTy"] == "ModifySelf") {
										$Script = "SELECT HashKey,(Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where Et.EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
										$EmpID = ScriptRunner($Script, "HashKey");
										$EditID = $EmpID;
										echo '<input name="AcctNo" id="AcctNo" type="hidden" value="' . $EmpID . '" />';
										echo ScriptRunner($Script, "Nm");

										$Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
									} else {
										if ($EditID != '' && $EditID != '--') {
											$Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
											$empnm = ScriptRunner($Script_Edit, "Nm");
											echo '<option selected="selected" value="' . $EditID . '">' . $empnm . '</option>';
										}

										$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and HashKey<>'" . $EditID . "' order by SName Asc");
										include '../login/dbOpen2.php';
										while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
											$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
										}
										echo $AllEmployee;
										include '../login/dbClose2.php';
									} ?>
								</span>
							</div>
						<?php
						} ?>
						<h3>
							<?php
							//echo $Script_Edit . $_REQUEST["PgTy"];

							if ($_REQUEST["PgTy"] == "ModifySelf") {
							} elseif ($_REQUEST["PgTy"] == "Create") {
								echo "Create/Edit Employee Records";
							} elseif ($_REQUEST["PgTy"] == "Inactive") {
								echo "Inactive Employee Records";
							}
							?>
						</h3>
					</div>
					<?php
					$totalLicense = ScriptRunnerParent("SELECT * FROM DbConnStr WHERE CustID = '" . $_SESSION["StkTck" . "CustID"] . "'")['Employees'];
					$getActive = (int) ScriptRunner("SELECT COUNT(EmpID) as Active From EmpTbl WHERE EmpStatus = 'Active' AND EmpID <> '000000' and  Status not in('D')", "Active");
					if (!$totalLicense) {
						$max = 100;
						$bg = 'danger';
					} else {
						$data = round($getActive / $totalLicense * 100);
						if ($data >= 0 && $data <= 25) {
							$max = $data;
							$bg = 'primary';
						} else if ($data >= 26 && $data <= 50) {
							$max = $data;
							$bg = 'success';
						} else if ($data >= 51 && $data <= 75) {
							$max = $data;
							$bg = 'warning';
						} else if ($data >= 76) {
							$max = $data;
							$bg = 'danger';
						}
					}
					?>
					<div class="col-md-4">
						<div class="flexbox mb-1">
							<span>
								<i class="ion-information font-size-26"></i><br>
								Employees / Licenses
							</span>
							<span class="text-<?php echo $bg ?> font-size-30"><?php echo $getActive ?> / <?php echo ($totalLicense) ? $totalLicense : 0 ?><span>
						</div>
						<div class="progress progress-xxs mt-10 mb-0">
							<div class="progress-bar bg-<?php echo $bg ?>" role="progressbar" style="width: <?php echo $max ?>%; height: 4px;" aria-valuenow="<?php echo $getActive ?>" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-2"></div>
							<?php if ($_REQUEST["PgTy"] == "Create" || $_REQUEST["PgTy"] == "Inactive") { ?>
								<div class="col-1" style="margin-top:2%;">
									<a href="javascript: void(0)" class="btn btn-default btn-sm" id="FindanEmployee" onClick="parent.ShowDisp('Search&nbsp;Employee','hrm/srh_Emp.php?PgTy=<?php echo ($_REQUEST["PgTy"]); ?>&RetPg=hrm/cr_AnyEmp.php&GroupName=Nationality&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')"><i class="fa fa-search"></i></a>
								</div>
								<div class="col-1" style="margin-top:2%;">
									<a href="javascript: void(0)" name="EditDashboard" id="EditDashboard" onclick="removeElement('DbName_TR');" class="btn btn-default btn-sm"><i class="fa  fa-address-card"></i></a>
								</div>
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%;">
									<select name="AcctNo" class="form-control select2" id="AcctNo">
										<?php
										echo '<option value="--" selected="selected">--</option>';
										if ($_REQUEST["PgTy"] == "Create") {
											$Script_Edi = "Select * from Settings where Setting='branchDown'";
											$brn_ = ScriptRunner($Script_Edi, "SetValue");

											if ($brn_ === 'yes') {
												$getBrDet = isUserBranchContact($_SESSION["StkTck" . "HKey"]);

												if (is_array($getBrDet) && !empty($getBrDet)) {
													// $dbOpen2 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' ";
													if (count($getBrDet) > 1) {
														$dbOpen2 = '';
														for ($i = 0, $len = count($getBrDet); $i < $len; $i++) {
															if ($i === 0) {

																$dbOpen2 = "(SELECT * from EmpTbl where (BranchID ='{$getBrDet[$i]['HashKey']}' and EmpStatus='Active' and Status in ('A','U','N')) ";
																continue;

																// $dbOpen2 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' or (BranchID ='{$getBrDet[$i]['HashKey']}' and EmpStatus='Active' and Status in ('A','U','N')) ";

															}
															$dbOpen2 .= "  or (BranchID ='{$getBrDet[$i]['HashKey']}' and EmpStatus='Active' and Status in ('A','U','N'))";
														}
														if ($EditID == '') {
															//  $dbOpen2 .=")";
														} else {
															$dbOpen2 .= " and HashKey<>'" . $EditID . "'";
														}
														$dbOpen2 .= ") order by SName Asc";
													} else {
														$dbOpen2 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' and BranchID ='{$getBrDet[0]['HashKey']}'";

														$dbOpen2 .= " order by SName Asc";
													}

													// foreach ($getBrDet as $one_det) {
													//     $dbOpen2 .= " UNION SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' and BranchID ='{$one_det['HashKey']}'";

													// }
												} else {
													$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' order by SName Asc");
												}
											} else {
												$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' order by SName Asc");
											}
										} elseif ($_REQUEST["PgTy"] == "Inactive") {
											$Script_Edi = "Select * from Settings where Setting='branchDown'";
											$brn_ = ScriptRunner($Script_Edi, "SetValue");
											if ($brn_ === 'yes') {

												$getBrDet = isUserBranchContact($_SESSION["StkTck" . "HKey"]);
												if (is_array($getBrDet) && !empty($getBrDet)) {
													// $dbOpen2 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' ";
													if (count($getBrDet) > 1) {
														$dbOpen2 = '';
														for ($i = 0, $len = count($getBrDet); $i < $len; $i++) {
															if ($i === 0) {
																$dbOpen2 = "(SELECT * from EmpTbl where (BranchID ='{$getBrDet[$i]['HashKey']}' and (EmpStatus is null or EmpStatus<>'Active') and Status in ('A','U','N')) ";
																continue;
															}
															$dbOpen2 .= "  or (BranchID ='{$getBrDet[$i]['HashKey']}' and (EmpStatus is null or EmpStatus<>'Active') and Status in ('A','U','N'))";
														}
														if ($EditID == '') {
															//  $dbOpen2 .=")";
														} else {
															$dbOpen2 .= " and HashKey<>'" . $EditID . "'";
														}
														$dbOpen2 .= ") order by SName Asc";
													} else {
														$dbOpen2 = "SELECT * from EmpTbl where Status in ('A','U','N') and (EmpStatus is null or EmpStatus<>'Active') and HashKey<>'" . $EditID . "' and BranchID ='{$getBrDet[0]['HashKey']}'";
														$dbOpen2 .= " order by SName Asc";
													}

													// foreach ($getBrDet as $one_det) {
													//     $dbOpen2 .= " UNION SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' and BranchID ='{$one_det['HashKey']}'";

													// }
												} else {

													$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and (EmpStatus is null or EmpStatus<>'Active') and HashKey<>'" . $EditID . "' order by SName Asc");
												}
											} else {
												$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and (EmpStatus is null or EmpStatus<>'Active') and HashKey<>'" . $EditID . "' order by SName Asc");
											}
										}
										include '../login/dbOpen2.php';
										//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
										while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
											//echo '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [".$row2['EmpID']."]" . '</option>';
											$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
										}
										echo $AllEmployee;
										include '../login/dbClose2.php';
										//if (isset($EditID) && $EditID != '' && $EditID != '--')
										if ($EditID != '' && $EditID != '--') {
											$Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
											$empnm = ScriptRunner($Script_Edit, "Nm");
											echo '<option selected="selected" value="' . $EditID . '">' . $empnm . '</option>';
										}
										?>
									</select>
									<span class="input-group-btn">
										<button type="submit" class="btn btn-danger" name="SubmitTrans" id="SubmitTrans">Open</button>
									</span>
								</div>
								<div class="col-1">
									<?php
									if ($EditID != "" || $EditID > 0) {
										$Script_Edit = "select *, Convert(Varchar(11),EmpDt,106) as EmpDt_, Convert(Varchar(11),DOB,106) as DOB_ from [EmpTbl] where HashKey='" . $EditID . "'";
									}
									?>
								</div>
							<?php } ?>
						</div>
						<div id="DbName_TR" name="DbName_TR" class=" input-group-sm col-8 pull-right" style="margin-top: 2%">
							<?php
							echo ("<script type='text/javascript'>{ removeElement('DbName_TR'); }</script>");
							echo '<select name="BranchID" class="form-control" id="BranchID">
										<option value="--" selected="selected">--</option>';
							if ($EditID != "" && $EditID != "--") {
								$SelID = ScriptRunner($Script_Edit, "BranchID");
							} else {
								if (isset($_REQUEST["BranchID"]) && strlen($_REQUEST["BranchID"]) == 32) {
									$SelID = $_REQUEST["BranchID"];
								} else {
									$SelID = "";
								}
							}
							if (!isset($SelID)) {
								$SelID = "";
							}
							$dbOpen2 = ("SELECT OName, HashKey FROM BrhMasters where Status='A' ORDER BY OName");
							include '../login/dbOpen2.php';
							while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
								if ($SelID == $row2['HashKey']) {
									echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
								} else {
									echo '<option value="' . $row2['HashKey'] . '">' . $row2['OName'] . '</option>';
								}
							}
							include '../login/dbClose2.php';
							echo '</select>';
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group row ">
							<label class="col-sm-4 col-form-label">Surname <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group-sm">
								<?php
								if (strlen($EditID) == 32) {
									echo '<input type="text" name="SName" class="form-control" id="SName" value="' . ScriptRunner($Script_Edit, "SName") . '" />';
								}
								//elseif (isset($_REQUEST["MSub"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear")))
								else {
									if (isset($_REQUEST["SName"])) {
										echo '<input type="text" name="SName" class="form-control" id="SName" value="' . $_REQUEST["SName"] . '" />';
									} else {
										echo '<input type="text" name="SName" class="form-control" id="SName" value="" />';
									}
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Employee No:</label>
							<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																			echo ' background: #e9e9e9; padding: 5px; ';
																		} ?>">
								<?php
								if ($EditID != '' && $EditID != '--') {
									$Script_Edit = "select * from EmpTbl where HashKey='" . $EditID . "'";
									echo '<input type="text"  name="EmpID" class="form-control" id="EmpID" value="' . trim(ScriptRunner($Script_Edit, "EmpID")) . '" />';
								} else {
									if (isset($_REQUEST["EmpID"])) {
										echo '<input type="text" readonly name="EmpID" class="form-control" id="EmpID" value="' . $_REQUEST["EmpID"] . '" />';
									} else {
										$ScriptEmpIDP = "Select * from Settings where Setting='EmpIDPSCnt'"; // and Status='A'
										$SetVal = ScriptRunner($ScriptEmpIDP, "SetValue");
										$SetVal2 = ScriptRunner($ScriptEmpIDP, "SetValue2") + 1;
										$SetVal3 = ScriptRunner($ScriptEmpIDP, "SetValue3");
										$SetVal4 = ScriptRunner($ScriptEmpIDP, "SetValue4");
										$DigitsDsp_ = ScriptRunner("Select * from Settings where Setting='AutoEmpID'", "SetValue2");
										if ($DigitsDsp_ == "Long") {
											if (strlen($SetVal2) == 1) {
												$SetVal2 = "000" . $SetVal2;
											} elseif (strlen($SetVal2) == 2) {
												$SetVal2 = "00" . $SetVal2;
											} elseif (strlen($SetVal2) == 3) {
												$SetVal2 = "0" . $SetVal2;
											}
										}
										if (trim($SetVal3) != "") {
											$SetVal5 = $SetVal4;
										} else {
											$SetVal5 = "";
										}

										$LoginDet = $SetVal . $SetVal4 . $SetVal2 . $SetVal5 . $SetVal3;
										$LoginDet = str_replace('#YYYY#', date('Y'), $LoginDet);
										$LoginDet = str_replace('#YY#', date('y'), $LoginDet);
										$LoginDet = str_replace('#MM#', date('m'), $LoginDet);
										//$LoginDet=str_replace('#YYYY#',date('Y'),$LoginDet);

										$ScriptEmpIDP = "Select * from Settings where Setting='AutoEmpID'"; // and Status='A'
										$AutoID = ScriptRunner($ScriptEmpIDP, "SetValue");
										if ($AutoID == "on") {
											echo '<input type="text" readonly name="EmpID"  class="form-control" id="EmpID" value="' . trim($LoginDet) . '" />';
										} else {
											echo '<input type="text" name="EmpID"  class="form-control" id="EmpID" value="' . trim($LoginDet) . '" />';
										}
									}
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Department:</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<select name="Department" id="Department" class="form-control">
									<option value="--" selected="selected">--</option>

									<?php
									if (isset($_REQUEST["Department"])) {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Department' and Val1<>'" . $_REQUEST["Department"] . "') ORDER BY Val1");
									} else {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Department') and status <> 'D'  ORDER BY Val1");
									}
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
									}
									include '../login/dbClose2.php';
									if ($EditID != "" && $EditID != "--") {
										echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "Department") . '">' . ScriptRunner($Script_Edit, "Department") . '</option>';
									} else {
										if (isset($_REQUEST["Department"])) {
											echo '<option selected=selected value="' . $_REQUEST["Department"] . '">' . $_REQUEST["Department"] . '</option>';
										} else {
										} //Nothing needed
									}
									?>
								</select>
								<span class="input-group-btn ">
									<?php
									$imgID = "MastersDepartment";
									if ($ModMaster == true) {
										$imgPath = "fa fa-plus";
										$Titl = "Add a new department eg. HR, Finance";
										$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Department&LDb=Single&amp;Elmt=Department',320,400,'No')";
									} else {
										$imgPath = "fa fa-info";
										$Titl = "Select an employee department";
										$OnClk = "";
									}
									echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
									?>
								</span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Project Team Lead:</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<select class="form-control select2" name="ProMgr" id="ProMgr">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;
									if ($EditID != '' && $EditID != '--') {
										$kk = ScriptRunner($Script_Edit, "ProMgr");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									} else
if (isset($_REQUEST['ProMgr']) && $_REQUEST['ProMgr'] !== '--') {

										$kk = $_REQUEST['ProMgr'];
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
								<span class="input-group-btn ">
									<?php
									$imgID = "MastersEmpStatus";
									$Titl = "Select for second level leave authorization";
									$OnClk = "";
									echo '<button type="button" onClick="' . $OnClk . '" title="' . $Titl . '" id="' . $imgID . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="fa fa-info"></i></button>';

									?>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group row ">
							<label class="col-sm-4 col-form-label">First Name <span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group-sm">
								<?php
								if ($EditID != "" || $EditID > 0) {
									echo '<input type="text" name="FName" class="form-control" id="FName" value="' . ScriptRunner($Script_Edit, "FName") . '" />';
								} else {
									if (isset($_REQUEST["FName"])) {
										echo '<input type="text" name="FName" class="form-control" id="FName" value="' . $_REQUEST["FName"] . '" />';
									} else {
										echo '<input type="text" name="FName" class="form-control" id="FName" value="" />';
									}
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Employed On<span style="color: red">*</span>:</label>
							<div class="input-group input-group-sm col-sm-8" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<span class="input-group-addon ">
									<?php
									$kk = ScriptRunner($Script_Edit, "Expatriate");
									if (trim($kk) == 1) {
										echo '<input name="Expatriate" id="Expatriate" type="checkbox" checked="checked" />';
									} else {
										echo '<input name="Expatriate" id="Expatriate" type="checkbox" />';
									}
									?>
									<label for="Expatriate" style="padding-left: 20px;height: 13px;"></label>
								</span>
								<?php if ($EditID != "" && $EditID != "--") {
									$Script = "SELECT CONVERT(char(11),EmpDt,106) as EmpDt from EmpTbl where HashKey='" . $EditID . "'";
									echo '<input name="EmpDt" id="EmpDt" type="text" class="form-control" value="' . ScriptRunner($Script, "EmpDt") . '"  readonly="readonly" /></span>';
								} else {
									if (isset($_REQUEST["EmpDt"])) {
										echo '<input name="EmpDt" id="EmpDt" type="text" class="form-control"  readonly="readonly" value="' . $_REQUEST["EmpDt"] . '" /></span>';
									} else {
										echo '<input name="EmpDt" id="EmpDt" type="text" class="form-control" value=""  readonly="readonly" value="" /></span>';
									}
								}
								?>
								<span class="input-group-btn ">
									<?php
									$imgID = "MastersEmpStatus";
									$Titl = "Check if employee is an expatriate";
									$OnClk = "";
									echo '<button type="button" onClick="' . $OnClk . '" title="' . $Titl . '" id="' . $imgID . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="fa fa-info"></i></button>';
									?>
								</span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Primary Manager:</label>
							<div class="col-sm-8  input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																				echo ' background: #e9e9e9; padding: 5px ';
																			} ?>">
								<select class="form-control select2" name="EmpMgr" id="EmpMgr">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;
									if ($EditID != '' && $EditID != '--') {
										$kk = ScriptRunner($Script_Edit, "EmpMgr");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									} else if (isset($_REQUEST['EmpMgr']) && $_REQUEST['EmpMgr'] !== '--') {

										$kk = $_REQUEST['EmpMgr'];
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}

									?>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Secondary Manager:</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<select class="form-control select2" name="EmpMgr_1" id="EmpMgr_1">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;
									if ($EditID != '' && $EditID != '--') {
										$kk = ScriptRunner($Script_Edit, "EmpMgr_1");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									} else
if (isset($_REQUEST['EmpMgr_1']) && $_REQUEST['EmpMgr_1'] !== '--') {

										$kk = $_REQUEST['EmpMgr_1'];
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
										echo '<option selected="selected" value="' . $kk . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
								<span class="input-group-btn ">
									<?php
									$imgID = "MastersEmpStatus";
									$Titl = "Select only if employee has two managers. Note that the Primary Manager is considered the actual Manager  ";
									$OnClk = "";
									echo '<button type="button" onClick="' . $OnClk . '" title="' . $Titl . '" id="' . $imgID . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="fa fa-info"></i></button>';

									?>
								</span>
							</div>
						</div>

					</div>
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Other Names:</label>
							<div class="col-sm-8 input-group-sm">
								<?php
								if ($EditID != "" || $EditID > 0) {
									echo '<input type="text" name="ONames" class="form-control" id="ONames" value="' . ScriptRunner($Script_Edit, "ONames") . '" />';
								} else {
									if (isset($_REQUEST["ONames"])) {
										echo '<input type="text" name="ONames" class="form-control" id="ONames" value="' . $_REQUEST["ONames"] . '" />';
									} else {
										echo '<input type="text" name="ONames" class="form-control" id="ONames" value="" />';
									}
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Employment Type:</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<?php
								if ($EditID != "" || $EditID > 0) {
									$Script_Edit_D = "Select * from [EmpDtl] where [StaffID]='{$EditID}' and [Status] in('N','U') order by [ID] desc";

									//    var_dump($Script_Edit_D);
									echo '<input type="text" name="" class="form-control"  readonly id="" value="' . ScriptRunner($Script_Edit_D, "ItemType") . '" />';
								} else {
									echo '<input type="text" name="" class="form-control" id="" value="" readonly />';
								}

								?>


							</div>
						</div>
						<div class="form-group row" id="autoappend">
							<!-- <input type="text" value="Update Record"  name="SubmitTrans"  id="toremove" /> -->
							<label class="col-sm-4 col-form-label" style="font-size: 11px;">Employee Status:</label>
							<div class=" col-sm-8  input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																							echo ' background: #e9e9e9; padding: 5px ';
																						} ?>">
								<select class="form-control" name="EmpStatus" id="EmpStatus" onchange="deleteFun(); return;">
									<option value="--" selected="selected">--</option>
									<?php
									if (isset($_REQUEST["EmpStatus"])) {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='EmployeeStatus' and Val1<>'" . $_REQUEST["EmpStatus"] . "') ORDER BY Val1");
									} else {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='EmployeeStatus') and status <> 'D' ORDER BY Val1");
									}
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
									}
									include '../login/dbClose2.php';
									if ($EditID != "" && $EditID != "--") {
										echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "EmpStatus") . '">' . ScriptRunner($Script_Edit, "EmpStatus") . '</option>';
									} else {
										if (isset($_REQUEST["EmpStatus"])) {
											echo '<option selected=selected value="' . $_REQUEST["EmpStatus"] . '">' . $_REQUEST["EmpStatus"] . '</option>';
										} else {
										} //Nothing needed
									}
									?>
								</select>
								<span class="input-group-btn ">
									<?php
									$imgID = "MastersEmpStatus";
									if ($ModMaster == true) {
										$imgPath = "fa fa-plus";
										$Titl = "Add a new employee status eg. Active, Retired, Terminated";
										$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=EmployeeStatus&LDb=Single&amp;Elmt=EmployeeStatus',320,400,'No')";
									} else {
										$imgPath = "fa fa-info";
										$Titl = "Select an employee status";
										$OnClk = "";
									}
									echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
									?>
								</span>
								<!-- <span class="input-group-btn ">
										<button type="button" class="btn btn-danger btn-sm" style="line-height: 17px"  name="ExitDt" id="ExitDt"  onclick="removeElement('TDExitDt');"><i class="fa fa-pencil"></i></button>
									</span> -->
							</div>

						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Designation:</label>
							<div class="col-sm-8 input-group-sm">
								<?php
								if ($EditID != "" || $EditID > 0) {
									$Script_Edit_D = "Select * from [EmpDtl] where [StaffID]='{$EditID}'  and [Status] in('N','U') order by [ID] desc";
									//    var_dump($Script_Edit_D);
									echo '<input type="text" name="" class="form-control"  readonly id="" value="' . ScriptRunner($Script_Edit_D, "ItemPos") . '" />';
								} else {
									echo '<input type="text" name="" class="form-control" id="" value="" readonly />';
								}

								?>
							</div>
						</div>





						<div class="form-group row">
							<label class="col-sm-4 col-form-label">Employee Category</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
								<select name="EmpCategory" id="EmpCategory" class="form-control">
									<option value="--" selected="selected">--</option>

									<?php
									if (isset($_REQUEST["EmpCategory"])) {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Category' and Val1<>'" . $_REQUEST["EmpCategory"] . "') ORDER BY Val1");
									} else {
										$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Category') and status <> 'D'  ORDER BY Val1");
									}
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
									}
									include '../login/dbClose2.php';
									if ($EditID != "" && $EditID != "--") {
										echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "EmpCategory") . '">' . ScriptRunner($Script_Edit, "EmpCategory") . '</option>';
									} else {
										if (isset($_REQUEST["EmpCategory"])) {
											echo '<option selected=selected value="' . $_REQUEST["EmpCategory"] . '">' . $_REQUEST["EmpCategory"] . '</option>';
										} else {
										} //Nothing needed
									}
									?>
								</select>
								<span class="input-group-btn ">
									<?php
									$imgID = "Masters_Employee_Category";
									if ($ModMaster == true) {
										$imgPath = "fa fa-plus";
										$Titl = "Add a new employee category eg. Admin, Academic";
										$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Employee Category&LDb=Single&amp;Elmt=Employee Category',320,400,'No')";
									} else {
										$imgPath = "fa fa-info";
										$Titl = "Select employee category ";
										$OnClk = "";
									}
									echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
									?>
								</span>
							</div>
						</div>
						<div id="TDExitDt" name="TDExitDt">
							<div class=" col-sm-8  input-group input-group-sm  pull-right">
								<?php
								echo ("<script type='text/javascript'>{ removeElement('TDExitDt'); }</script>");
								if ($EditID != "" && $EditID != "--") {
									$Script = "SELECT CONVERT(char(11),ExitDt,106) as ExDt from EmpTbl where HashKey='" . $EditID . "'";
									echo '<input type="text" class="form-control" name="ExDt" id="ExDt" value="' . ScriptRunner($Script, "ExDt") . '"  maxlength="12" readonly="readonly" /></span>';
								} else {
									echo '<input type="text" class="form-control" name="ExDt" id="ExDt"  value="" maxlength="12" readonly="readonly" /></span>';
								}
								?>
								<span class="input-group-btn">
									<!-- <td align="left"><img src="../images/bi_butn/infoBW.jpg" Title="Click to clear date fields" name="ClearDate" width="15" height="15" border="0" id="ClearDate" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ClearDate','','../images/bi_butn/info.jpg',1)" /></td> -->
									<button type="button" id="ClearDate" Title="Click to clear date fields" name="ClearDate" class="btn btn-default"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<div class="row">
			<div class="col-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li><a class="active" href="#bioData" data-toggle="tab">BioData</a></li>
						<li><a href="#educational" data-toggle="tab">Educational</a></li>
						<li><a href="#history" data-toggle="tab">Employment History</a></li>
						<li><a href="#references" data-toggle="tab">References</a></li>
						<li><a href="#others" data-toggle="tab">Others</a></li>
						<li><a href="#comp_property" data-toggle="tab">Company Property</a></li>
						<li><a href="#appraisal" data-toggle="tab">Appraisal</a></li>
						<li><a href="#leave" data-toggle="tab">Leave</a></li>
						<li><a href="#salary" data-toggle="tab">Salary History</a></li>
						<li><a href="#docs" data-toggle="tab">Attached Docs</a></li>
						<li><a href="#line_report" data-toggle="tab">Line Report</a></li>
						<li><a href="#my_query" data-toggle="tab">Query</a></li>
						<li><a href="#my_request" data-toggle="tab">Request</a></li>
						<!--<li><a href="<?php //echo '../dms/inv_doc_search.php?PgTy=Search&HHeader=True&SubmitDoc=Go&Search='.trim(ScriptRunner($Script_Edit,"EmpID"));
											?>" class="TinyText">Attached Documents</a></li> //-->
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="bioData">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Birth Date <span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {

												$Script = "SELECT CONVERT(char(11),DOB,106) as DOB from EmpTbl where HashKey='" . $EditID . "'";
												$EmpDt_ = ScriptRunner($Script, "DOB");
												echo '<input name="DOB" id="DOB" type="text" class="form-control" value="' . $EmpDt_ . '" maxlength="12" readonly="readonly" />';
											} else {
												if (isset($_REQUEST["DOB"])) {
													echo '<input name="DOB" id="DOB" type="text" class="form-control"  maxlength="12" readonly="readonly" value="' . $_REQUEST["DOB"] . '" />';
												} else {
													echo '<input name="DOB" id="DOB"  type="text" class="form-control" value="" maxlength="12" readonly="readonly" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Card Expiry Date :</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {

												$Script = "SELECT CONVERT(char(11),card_exp,106) as card_exp from EmpTbl where HashKey='" . $EditID . "'";
												$cardDt_ = ScriptRunner($Script, "card_exp");
												echo '<input name="card_exp" id="card_exp" type="text" class="form-control" value="' . $cardDt_ . '" maxlength="12" readonly="readonly" />';
											} else {
												if (isset($_REQUEST["card_exp"])) {
													echo '<input name="card_exp" id="card_exp" type="text" class="form-control"  maxlength="12" readonly="readonly" value="' . $_REQUEST["card_exp"] . '" />';
												} else {
													echo '<input name="card_exp" id="card_exp"  type="text" class="form-control" value="" maxlength="12" readonly="readonly" value="" />';
												}
											}
											?>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">NIN:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="NIN" maxlength="11" class="form-control" id="NIN" value="' . ScriptRunner($Script_Edit, "NIN") . '" />';
											} elseif (isset($_REQUEST["NIN"])) {
												echo '<input type="text" name="NIN" maxlength="11" class="form-control" id="NIN" value="' . $_REQUEST["NIN"] . '" />';
											} else {
												echo '<input type="text" name="NIN" maxlength="11" class="form-control" id="NIN" value="" />';
											}
											?>
										</div>
									</div>


									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Title:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select class="form-control" name="Title" id="Title">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["Title"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Title' and Val1<>'" . $_REQUEST["Title"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Title') ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "Title") . '">' . ScriptRunner($Script_Edit, "Title") . '</option>';
												} else {
													if (isset($_REQUEST["Title"])) {
														echo '<option selected=selected value="' . $_REQUEST["Title"] . '">' . $_REQUEST["Title"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersEmpStatus";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new employee status eg. Active, Retired, Terminated";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Title&LDb=Single&amp;Elmt=Title',320,400,'No')";
												} else {
													$imgPath = ".fa fa-info";
													$Titl = "Select an employee status";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Gender<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group-sm input-group-sm ">
											<select class="form-control" name="Sex" id="Sex">
												<option value="--">--</option>
												<option value="Male">Male</option>
												<option value="Female">Female</option>
												<?php if ($EditID != "" || $EditID > 0) {
													echo '<option value="' . ScriptRunner($Script_Edit, "Sex") . '" selected="selected">' . ScriptRunner($Script_Edit, "Sex") . '</option>';
												} else {
													if (isset($_REQUEST["Sex"])) {
														echo '<option selected=selected value="' . $_REQUEST["Sex"] . '">' . $_REQUEST["Sex"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Religion:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select name="Relg" id="Relg" class="form-control">
												<option value="--" selected="selected">--</option>

												<?php
												if (isset($_REQUEST["Relg"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Religion' and Val1<>'" . $_REQUEST["Relg"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Religion') ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "Relg") . '">' . ScriptRunner($Script_Edit, "Relg") . '</option>';
												} else {
													if (isset($_REQUEST["Relg"])) {
														echo '<option selected=selected value="' . $_REQUEST["Relg"] . '">' . $_REQUEST["Relg"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersReligion";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new employee religion eg. Traditionalist, Buddist, Atheist";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Religion&LDb=Single&amp;Elmt=Religion',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee religion";
													$OnClk = "";
												}
												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Nationality <span style="color: red">*</span> :</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select class="form-control" name="COO" id="COO">
												<option value="--" selected="selected">--</option>
												<?php
												//if (isset($_REQUEST["COO"]))
												$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='Nationality' and status <> 'D'  ORDER BY Val1");
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "COO") . '">' . ScriptRunner($Script_Edit, "COO") . '</option>';
												} elseif (!isset($_REQUEST["COO"])) {
												} else {
													if (isset($_REQUEST["COO"])) {
														echo '<option selected=selected value="' . $_REQUEST["COO"] . '">' . $_REQUEST["COO"] . '</option>';
													}
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersNationality";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new nationality eg. Ghana, Kenya, Liberia";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Nationality&LDb=Single&amp;Elmt=Nationality',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee nationality";
													$OnClk = "";
												}
												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>'; ?>
											</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">State of Origin <span style="color: red">*</span> :</label>
										<!-- <div id="statediv" name="statediv"> -->
										<div class="col-sm-8 input-group input-group-sm">
											<select name="SOO" class="form-control  select2" id="SOO">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["SOO"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='State Of Origin' and Val1<>'" . $_REQUEST["SOO"] . "' ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='State Of Origin' and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "SOO") . '">' . ScriptRunner($Script_Edit, "SOO") . '</option>';
												} else {
													if (isset($_REQUEST["SOO"])) {
														echo '<option selected=selected value="' . $_REQUEST["SOO"] . '">' . $_REQUEST["SOO"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersSOO";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new employee state of origin eg. Zamfara, Oyo, Enugu";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?PRent=Nationality&GroupName=State%20Of%20Origin&LDb=Single&amp;Elmt=State%20Of%20Origin',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee state of origin";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
										<!-- </div> -->
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Province/LGA <span style="color: red">*</span> :</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select class="form-control select2" name="LGA" id="LGA">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["LGA"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='LGA' and Val1<>'" . $_REQUEST["LGA"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='LGA') and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "LGA") . '">' . ScriptRunner($Script_Edit, "LGA") . '</option>';
												} else {
													if (isset($_REQUEST["LGA"])) {
														echo '<option selected=selected value="' . $_REQUEST["LGA"] . '">' . $_REQUEST["LGA"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersLGA";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new local government area";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=LGA&LDb=Single&amp;Elmt=LGA',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee local government area";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">M.Status <span style="color: red">*</span> :</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select name="MStatus" id="MStatus" class="form-control">
												<option value="--" selected="selected">--</option>

												<?php
												if (isset($_REQUEST["MStatus"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Marital Status' and Val1<>'" . $_REQUEST["MStatus"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Marital Status') and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "MStatus") . '">' . ScriptRunner($Script_Edit, "MStatus") . '</option>';
												} else {
													if (isset($_REQUEST["MStatus"])) {
														echo '<option selected=selected value="' . $_REQUEST["MStatus"] . '">' . $_REQUEST["MStatus"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersMariatalStatus";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new employee marital status eg. Single, Married, Divorced";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Marital%20Status&LDb=Single&amp;Elmt=Marital%20Status',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee's marital status";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div id="PptTD" class="text-center">
										<?php
										if ($EditID == '' || $EditID == '--') {
											$imgsrc = "pfimg/Default.jpg";
										} else {
											$imgsrc = ScriptRunner($Script_Edit, "StfImg");
											if (strlen($imgsrc) < 4) {
												$imgsrc = "pfimg/Default.jpg";
											}
										}
										if (!isset($empnm)) {
											$empnm = "0";
										}
										echo '<iframe id="frame_employee_Ppt" name="frame_employee_Ppt" src="stf_vw_img.php?imgsrc=' . $imgsrc . '&empnm=' . $empnm . '&Siz=Mini&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="92" height="110" frameborder="0" scrolling="No" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>'; ?>

										<!---<img id="Ppt" name="Ppt" src="../<?php echo $imgsrc; ?>" height="110" border="1" title="Add a new employee passport" onClick="parent.ShowDisp('Add employee passport','hrm/stf_vw_img.php?imgsrc=../<?php echo $imgsrc . "&empnm=" . $empnm; ?>',400,290,'No')" /> //-->

									</div>
									<div class="text-center" style="margin-top: 2%;"><img src="../images/plus_.jpg" width="15" title="Add a new employee passport" onClick="parent.ShowDisp('Add&nbsp;employee&nbsp;passport','hrm/stf_upload_img.php?StaffID=<?php echo $EditID . "&DocHID=" . ScriptRunner($Script_Edit, "DocHID"); ?>&GroupName=Employee%20Type&amp;LDb=Single&amp;Elmt=Employee%20Type',150,400,'No')" />
									</div>


									<div id="PptTD_sig" class="text-center mt-2">
										<?php
										if ($EditID == '' || $EditID == '--') {
											$imgsrc_sig = "pfimg/Default.jpg";
										} else {
											$imgsrc_sig = ScriptRunner($Script_Edit, "sig_img");
											if (strlen($imgsrc_sig) < 4) {
												$imgsrc_sig = "pfimg/Default.jpg";
											}
										}
										if (!isset($empnm)) {
											$empnm = "0";
										}
										echo '<iframe id="frame_employee_Ppt_sig" name="frame_employee_Ppt_sig" src="stf_vw_sig.php?imgsrc=' . $imgsrc_sig . '&empnm=' . $empnm . '&Siz=Mini&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="92" height="92" frameborder="0" scrolling="No" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>'; ?>



									</div>
									<div class="text-center mt-2"><img src="../images/plus_.jpg" width="15" title="upload employee signature" onClick="parent.ShowDisp('Upload&nbsp;employee&nbsp;signature','hrm/stf_upload_sig.php?StaffID=<?php echo $EditID . "&DocHID=" . ScriptRunner($Script_Edit, "DocHID"); ?>&GroupName=Employee%20Type&amp;LDb=Single&amp;Elmt=Employee%20Type',150,400,'No')" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-header">Contact Details</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Email:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "") {
												echo '<input type="text" name="Email" maxlength="250" class="form-control" id="Email" value="' . ScriptRunner($Script_Edit, "Email") . '" />';
											} elseif (isset($_REQUEST["Email"])) {
												echo '<input type="text" name="Email" maxlength="250" class="form-control" id="Email" value="' . $_REQUEST["Email"] . '" />';
											} else {
												echo '<input type="text" name="Email" maxlength="250" class="form-control" id="Email" value="" />';
											}
											?>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Home Address:</label>
										<div class="col-sm-8">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<textarea type="text" rows="3" name="HmAdd" maxlength="200" class="form-control" id="HmAdd"/>' . ScriptRunner($Script_Edit, "HmAdd") . '</textarea>';
											} elseif (isset($_REQUEST["HmAdd"])) {
												echo '<textarea type="text" rows="3" name="HmAdd" maxlength="200" class="form-control" id="HmAdd" />' . $_REQUEST["HmAdd"] . '</textarea>';
											} else {
												echo '<textarea type="text" rows="3" name="HmAdd" maxlength="200" class="form-control" id="HmAdd" /></textarea>';
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Mobile:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="Mobile"  maxlength="14" class="form-control" id="Mobile" value="' . ScriptRunner($Script_Edit, "Mobile") . '" />';
											} elseif (isset($_REQUEST["Mobile"])) {
												echo '<input type="text" name="Mobile"  maxlength="14" class="form-control" id="Mobile" value="' . $_REQUEST["Mobile"] . '" />';
											} else {
												echo '<input type="text" name="Mobile"  maxlength="14" class="form-control" id="Mobile" value="" />';
											}
											?>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Office Address:</label>
										<div class="col-sm-8">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<textarea type="text"  rows="3" name="OffAdd"  maxlength="200" class="form-control" id="OffAdd"/>' . ScriptRunner($Script_Edit, "OffAdd") . '</textarea>';
											} elseif (isset($_REQUEST["OffAdd"])) {
												echo '<textarea type="text"  rows="3" name="OffAdd"  maxlength="200" class="form-control" id="OffAdd" />' . $_REQUEST["OffAdd"] . '</textarea>';
											} else {
												echo '<textarea type="text"  rows="3" name="OffAdd"  maxlength="200" class="form-control" id="OffAdd" /></textarea>';
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Phone No:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="Phone" maxlength="15" class="form-control" id="Phone" value="' . ScriptRunner($Script_Edit, "Phone") . '" />';
											} elseif (isset($_REQUEST["Phone"])) {
												echo '<input type="text" name="Phone" maxlength="15" class="form-control" id="Phone" value="' . $_REQUEST["Phone"] . '" />';
											} else {
												echo '<input type="text" name="Phone" maxlength="15" class="form-control" id="Phone" value="" />';
											}
											?>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Secondary Email:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "") {
												echo '<input type="text" name="Sec_Email" maxlength="250" class="form-control" id="Sec_Email" value="' . ScriptRunner($Script_Edit, "Sec_Email") . '" />';
											} elseif (isset($_REQUEST["Sec_Email"])) {
												echo '<input type="text" name="Sec_Email" maxlength="250" class="form-control" id="Sec_Email" value="' . $_REQUEST["Sec_Email"] . '" />';
											} else {
												echo '<input type="text" name="Sec_Email" maxlength="250" class="form-control" id="Sec_Email" value="" />';
											}
											?>
										</div>
									</div>
									<div>
										<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
											echo ' <div class="TinyTextFail" style="font-size: 12px;"><strong>Note:</strong><br>Shaded fields will not be updated regardless of changes. Contact your HR Team in this regard </div>';
										} ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-header">Next of Kin & Emergency Details</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Blood Group:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select name="BloodGrp" id="BloodGrp" class="form-control">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["BloodGrp"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName ='Blood Group' and Val1<>'" . $_REQUEST["BloodGrp"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName ='Blood Group') and status <> 'D' ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "BloodGrp") . '">' . ScriptRunner($Script_Edit, "BloodGrp") . '</option>';
												} else {
													if (isset($_REQUEST["BloodGrp"])) {
														echo '<option selected=selected value="' . $_REQUEST["BloodGrp"] . '">' . $_REQUEST["BloodGrp"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "BloodGrp";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new next individual type blood group";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Blood Group&LDb=Single&amp;Elmt=Blood&nbsp;Group',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee blood group";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Genotype:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select name="GenoType" id="GenoType" class="form-control">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["GenoType"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='GenoType' and Val1<>'" . $_REQUEST["GenoType"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='GenoType') and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "GenoType") . '">' . ScriptRunner($Script_Edit, "GenoType") . '</option>';
												} else {
													if (isset($_REQUEST["GenoType"])) {
														echo '<option selected=selected value="' . $_REQUEST["GenoType"] . '">' . $_REQUEST["GenoType"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "GenoType";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new individual type genotype";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=GenoType&LDb=Single&amp;Elmt=GenoType',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee genotype";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Known Illment:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="KnownIllment"  maxlength="100" class="form-control" id="KnownIllment" value="' . ScriptRunner($Script_Edit, "KnownIllment") . '" />';
											} else {
												if (isset($_REQUEST["KnownIllment"])) {
													echo '<input type="text" name="KnownIllment"  maxlength="100" class="form-control" id="KnownIllment" value="' . $_REQUEST["KnownIllment"] . '" />';
												} else {
													echo '<input type="text" name="KnownIllment"  maxlength="100" class="form-control" id="KnownIllment" value="" />';
												}
											}
											?>
										</div>
									</div>
									<!-- <div class="form-group row"> -->
									<div class="demo-checkbox">
										<?php
										if ($EditID != "" || $EditID > 0) {
											$kk = ScriptRunner($Script_Edit, "EmpDisabled");
										}
										if (trim($kk) == 'on') {
											echo '<input name="EmpDisabled" id="EmpDisabled" type="checkbox" checked="checked" />';
										} else {
											echo '<input name="EmpDisabled" id="EmpDisabled" type="checkbox" />';
										}
										?>
										<label for="EmpDisabled">Disabled</label>
									</div>
									<!-- </div> -->
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Full Name<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="NOK1Name" maxlength="30" class="form-control" id="NOK1Name" value="' . ScriptRunner($Script_Edit, "NOK1Name") . '" />';
											} else {
												if (isset($_REQUEST["NOK1Name"])) {
													echo '<input type="text" name="NOK1Name" maxlength="30" class="form-control" id="NOK1Name" value="' . $_REQUEST["NOK1Name"] . '" />';
												} else {
													echo '<input type="text" name="NOK1Name" maxlength="30" class="form-control" id="NOK1Name" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Relationship<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select name="NOK1Rel" id="NOK1Rel" class="form-control">
												<option value="--" selected="selected">--</option>

												<?php
												if (isset($_REQUEST["NOK1Rel"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Relationship Status' and Val1<>'" . $_REQUEST["NOK1Rel"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Relationship Status') and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "NOK1Rel") . '">' . ScriptRunner($Script_Edit, "NOK1Rel") . '</option>';
												} else {
													if (isset($_REQUEST["NOK1Rel"])) {
														echo '<option selected=selected value="' . $_REQUEST["NOK1Rel"] . '">' . $_REQUEST["NOK1Rel"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "RelationshipStatus1";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new next of kin relationship type eg. Brother, Sister, Spouse";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Relationship%20Status&LDb=Single&amp;Elmt=Relationship%20Status',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select a next of kin relationship type";
													$OnClk = "";
												}

												echo '<button type="button" id="' . $imgID . '"  title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Mobile Number<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="NOK1Mobile" maxlength="15" class="form-control" id="NOK1Mobile" value="' . ScriptRunner($Script_Edit, "NOK1Mobile") . '" />';
											} else {
												if (isset($_REQUEST["NOK1Mobile"])) {
													echo '<input type="text" name="NOK1Mobile" maxlength="15" class="form-control" id="NOK1Mobile" value="' . $_REQUEST["NOK1Mobile"] . '" />';
												} else {
													echo '<input type="text" name="NOK1Mobile" maxlength="15" class="form-control" id="NOK1Mobile" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Home Address<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<textarea type="text" rows="3" name="NOK1Add" maxlength="200" class="form-control" id="NOK1Add"/>' . ScriptRunner($Script_Edit, "NOK1Add") . '</textarea>';
											} else {
												if (isset($_REQUEST["NOK1Add"])) {
													echo '<textarea type="text" rows="3" name="NOK1Add" maxlength="200" class="form-control" id="NOK1Add" />' . $_REQUEST["NOK1Add"] . '</textarea>';
												} else {
													echo '<textarea type="text" rows="3" name="NOK1Add" maxlength="200" class="form-control" id="NOK1Add" /></textarea>';
												}
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Full Name:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="NOK2Name" maxlength="60" class="form-control" id="NOK2Name" value="' . ScriptRunner($Script_Edit, "NOK2Name") . '" />';
											} else {
												if (isset($_REQUEST["NOK2Name"])) {
													echo '<input type="text" name="NOK2Name" maxlength="60" class="form-control" id="NOK2Name" value="' . $_REQUEST["NOK2Name"] . '" />';
												} else {
													echo '<input type="text" name="NOK2Name" maxlength="60" class="form-control" id="NOK2Name" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Relationship:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select class="form-control" name="NOK2Rel" id="NOK2Rel">
												<option value="--" selected="selected">--</option>

												<?php
												if (isset($_REQUEST["NOK2Rel"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Relationship Status' and Val1<>'" . $_REQUEST["NOK2Rel"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Relationship Status') and status <> 'D'  ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "NOK2Rel") . '">' . ScriptRunner($Script_Edit, "NOK2Rel") . '</option>';
												} else {
													if (isset($_REQUEST["NOK2Rel"])) {
														echo '<option selected=selected value="' . $_REQUEST["NOK2Rel"] . '">' . $_REQUEST["NOK2Rel"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "RelationshipStatus2";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new next of kin relationship type eg. Brother, Sister, Spouse";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Relationship%20Status&LDb=Single&amp;Elmt=Relationship%20Status',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select a next of kin relationship type";
													$OnClk = "";
												}
												echo '<button type="button" class="btn btn-default btn-sm" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Mobile Number:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="NOK2Mobile" maxlength="15" class="form-control" id="NOK2Mobile" value="' . ScriptRunner($Script_Edit, "NOK2Mobile") . '" />';
											} else {
												if (isset($_REQUEST["NOK2Mobile"])) {
													echo '<input type="text" name="NOK2Mobile" maxlength="15" class="form-control" id="NOK2Mobile" value="' . $_REQUEST["NOK2Mobile"] . '" />';
												} else {
													echo '<input type="text" name="NOK2Mobile" maxlength="15" class="form-control" id="NOK2Mobile" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Home Address:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<textarea type="text" rows="3" name="NOK2Add" maxlength="200" class="form-control" id="NOK2Add"/>' . ScriptRunner($Script_Edit, "NOK2Add") . '</textarea>';
											} else {
												if (isset($_REQUEST["NOK2Add"])) {
													echo '<textarea type="text" rows="3" name="NOK2Add" maxlength="200" class="form-control" id="NOK2Add" />' . $_REQUEST["NOK2Add"] . '</textarea>';
												} else {
													echo '<textarea type="text" rows="3" name="NOK2Add" maxlength="200" class="form-control" id="NOK2Add" /></textarea>';
												}
											}
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-header"> Financial Details</h3>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Salary Bank:</label>
										<div class="col-sm-8 input-group input-group-sm">
											<select class="form-control select2 " name="SalBank" id="SalBank" onchange="updateCode(this)">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["SalBank"])) {
													$dbOpen2 = ("SELECT Val1, Val2,ItemCode FROM Masters where (ItemName='Salary Bank' and Val1<>'" . $_REQUEST["SalBank"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2, ItemCode FROM Masters where (ItemName='Salary Bank') and status <> 'D' ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option   data-code="' . $row2['ItemCode'] . '"  value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "SalBank") . '">' . ScriptRunner($Script_Edit, "SalBank") . '</option>';
												} else {
													if (isset($_REQUEST["SalBank"])) {
														echo '<option selected=selected value="' . $_REQUEST["SalBank"] . '">' . $_REQUEST["SalBank"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersSalaryBank";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new bank for employee salaries eg. EcoBank, GTBank";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Salary%20Bank&LDb=Single&amp;Elmt=Salary%20Bank',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee salary bank name";
													$OnClk = "";
												}
												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Account No:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="hidden" name="SalSortCode" maxlength="25" class="form-control" id="SalSortCode" value="' . ScriptRunner($Script_Edit, "SalSortCode") . '" />';
											} else {
												if (isset($_REQUEST["SalSortCode"])) {
													echo '<input type="hidden" name="SalSortCode" maxlength="25" class="form-control" id="SalSortCode" value="' . $_REQUEST["SalSortCode"] . '" />';
												} else {
													echo '<input type="hidden" name="SalSortCode" maxlength="25" class="form-control" id="SalSortCode" value="" />';
												}
											}
											?>
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="SalAcctNo" maxlength="10" class="form-control" id="SalAcctNo" value="' . ScriptRunner($Script_Edit, "SalAcctNo") . '" />';
											} else {
												if (isset($_REQUEST["SalAcctNo"])) {
													echo '<input type="text" name="SalAcctNo" maxlength="10" class="form-control" id="SalAcctNo" value="' . $_REQUEST["SalAcctNo"] . '" />';
												} else {
													echo '<input type="text" name="SalAcctNo" maxlength="10" class="form-control" id="SalAcctNo" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Tax ID:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="TaxID" maxlength="10" class="form-control" id="TaxID" value="' . ScriptRunner($Script_Edit, "TaxID") . '" />';
											} else {
												if (isset($_REQUEST["TaxID"])) {
													echo '<input type="text" name="TaxID" maxlength="10" class="form-control" id="TaxID" value="' . $_REQUEST["TaxID"] . '" />';
												} else {
													echo '<input type="text" name="TaxID" maxlength="10" class="form-control" id="TaxID" value="" />';
												}
											}
											?>
										</div>
									</div>
								</div>

								<div class="col-md-4">


									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Bank Code:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input  readonly="readonly"  type="text" name="bank_code" maxlength="25" class="form-control" id="bank_code" value="' . ScriptRunner($Script_Edit, "bank_code") . '" />';
											} else {
												if (isset($_REQUEST["bank_code"])) {
													echo '<input type="text"  readonly="readonly"  name="bank_code" maxlength="25" class="form-control" id="bank_code" value="' . $_REQUEST["bank_code"] . '" />';
												} else {
													echo '<input type="text"  readonly="readonly"  name="bank_code" maxlength="25" class="form-control" id="bank_code" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">PFA code:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="pfa_code" maxlength="25" class="form-control" id="pfa_code" value="' . ScriptRunner($Script_Edit, "pfa_code") . '" />';
											} else {
												if (isset($_REQUEST["pfa_code"])) {
													echo '<input type="text" name="pfa_code" maxlength="25" class="form-control" id="pfa_code" value="' . $_REQUEST["pfa_code"] . '" />';
												} else {
													echo '<input type="text" name="pfa_code" maxlength="25" class="form-control" id="pfa_code" value="" />';
												}
											}
											?>
										</div>
									</div>




									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Medical No:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="' . ScriptRunner($Script_Edit, "MediCareNo") . '" />';
											} else {
												if (isset($_REQUEST["MediCareNo"])) {
													echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="' . $_REQUEST["MediCareNo"] . '" />';
												} else {
													echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="demo-checkbox" style="margin-top: 2%">
										<?php if (strlen($EditID) == 33) { ?>
											<img src="../images/bi_butn/PrintBW.jpg" title="Print employee details" name="PrintEmpRec" height="18" border="0" id="PrintEmpRec" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('Print&nbsp;Employee&nbsp;Details','hrm/Vw_EmpDtl.php?HID=<?php echo $row2['HashKey']; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('PrintEmpRec','','../images/bi_butn/Print.jpg',1); ImgBorderOn('PrintEmpRec', 0);" />
										<?php } ?>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">PFA Custodian:</label>
										<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																									echo ' background: #e9e9e9; padding: 5px ';
																								} ?>">
											<select name="PenCustID" id="PenCustID" class="form-control">
												<option value="--" selected="selected">--</option>

												<?php
												if (isset($_REQUEST["PenCustID"])) {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Pension Custodian' and Val1<>'" . $_REQUEST["PenCustID"] . "') ORDER BY Val1");
												} else {
													$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Pension Custodian') and status <> 'D' ORDER BY Val1");
												}
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
												}
												include '../login/dbClose2.php';
												if ($EditID != "" && $EditID != "--") {
													echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "PenCustID") . '">' . ScriptRunner($Script_Edit, "PenCustID") . '</option>';
												} else {
													if (isset($_REQUEST["PenCustID"])) {
														echo '<option selected=selected value="' . $_REQUEST["PenCustID"] . '">' . $_REQUEST["PenCustID"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "PensionCustodian";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new pension fund administrator type eg. Allied PFA, Sterling PFA";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Pension%20Custodian&LDb=Single&amp;Elmt=Pension%20Custodian',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select a pension fund administrator for this employee account";
													$OnClk = "";
												}
												echo '<button type="button" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">PFA ID:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="PENCommNo" maxlength="15" class="form-control" id="PENCommNo" value="' . ScriptRunner($Script_Edit, "PENCommNo") . '" />';
											} else {
												if (isset($_REQUEST["PENCommNo"])) {
													echo '<input type="text" name="PENCommNo" maxlength="15" class="form-control" id="PENCommNo" value="' . $_REQUEST["PENCommNo"] . '" />';
												} else {
													echo '<input type="text" name="PENCommNo" maxlength="15" class="form-control" id="PENCommNo" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Tax State:</label>
										<!-- <div id="statediv2" name="statediv2"> -->
										<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																									echo ' background: #e9e9e9; padding: 5px ';
																								} ?>">
											<select name="TaxState" id="TaxState" class="form-control">
												<option value="--" selected="selected">--</option>
												<?php
												if (isset($_REQUEST["TaxState"])) {
													$SelID = $_REQUEST["TaxState"];
												} else {
													$SelID = "";
												}
												if (isset($EditID) && strlen($EditID) == 32) {
													$SelID = ScriptRunner($Script_Edit, "TaxState");
												}
												$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where ItemName='State Of Origin' and Status<>'D' ORDER BY Val1");
												include '../login/dbOpen2.php';
												while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
													if ($SelID == $row2['Val1']) {
														echo '<option selected value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
													} else {
														echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
													}
												}
												include '../login/dbClose2.php';
												?>
											</select>
											<span class="input-group-btn ">
												<?php
												$imgID = "MastersSOO";
												if ($ModMaster == true) {
													$imgPath = "fa fa-plus";
													$Titl = "Add a new employee tax state eg. Zamfara, Oyo, Enugu";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?PRent=Nationality&GroupName=State%20Of%20Origin&LDb=Single&amp;Elmt=State%20Of%20Origin',320,400,'No')";
												} else {
													$imgPath = "fa fa-info";
													$Titl = "Select an employee tax state";
													$OnClk = "";
												}
												echo '<button type="button" id="' . $imgID . '"  title="' . $Titl . '" onClick="' . $OnClk . '" class="btn btn-default btn-sm" style="line-height: 17px"><i class="' . $imgPath . '"></i></button>';
												?>
											</span>
										</div>
										<!-- </div> -->
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Housing Fund ID:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="HInsurNo" maxlength="10" class="form-control" id="HInsurNo" value="' . ScriptRunner($Script_Edit, "HInsurNo") . '" />';
											} else {
												if (isset($_REQUEST["HInsurNo"])) {
													echo '<input type="text" name="HInsurNo" maxlength="10" class="form-control" id="HInsurNo" value="' . $_REQUEST["HInsurNo"] . '" />';
												} else {
													echo '<input type="text" name="HInsurNo" maxlength="10" class="form-control" id="HInsurNo" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Medical No:</label>
										<div class="col-sm-8 input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
																						echo ' background: #e9e9e9; padding: 5px ';
																					} ?>">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="' . ScriptRunner($Script_Edit, "MediCareNo") . '" />';
											} else {
												if (isset($_REQUEST["MediCareNo"])) {
													echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="' . $_REQUEST["MediCareNo"] . '" />';
												} else {
													echo '<input type="text" name="MediCareNo" maxlength="25" class="form-control" id="MediCareNo" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="demo-checkbox" style="margin-top: 2%">
										<?php if (strlen($EditID) == 33) { ?>
											<img src="../images/bi_butn/PrintBW.jpg" title="Print employee details" name="PrintEmpRec" height="18" border="0" id="PrintEmpRec" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('Print&nbsp;Employee&nbsp;Details','hrm/Vw_EmpDtl.php?HID=<?php echo $row2['HashKey']; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('PrintEmpRec','','../images/bi_butn/Print.jpg',1); ImgBorderOn('PrintEmpRec', 0);" />
										<?php } ?>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-header"> Medical Details</h3>
								</div>
								<div class="col-md-4">

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Name of Spouse:</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="name_spouse" maxlength="25" class="form-control" id="name_spouse" value="' . ScriptRunner($Script_Edit, "name_spouse") . '" />';
											} else {
												if (isset($_REQUEST["name_spouse"])) {
													echo '<input type="text" name="name_spouse" maxlength="25" class="form-control" id="name_spouse" value="' . $_REQUEST["name_spouse"] . '" />';
												} else {
													echo '<input type="text" name="name_spouse" maxlength="25" class="form-control" id="name_spouse" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Spouse Resident State</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="spouse_residence" maxlength="25" class="form-control" id="spouse_residence" value="' . ScriptRunner($Script_Edit, "spouse_residence") . '" />';
											} else {
												if (isset($_REQUEST["spouse_residence"])) {
													echo '<input type="text" name="spouse_residence" maxlength="25" class="form-control" id="spouse_residence" value="' . $_REQUEST["spouse_residence"] . '" />';
												} else {
													echo '<input type="text" name="spouse_residence" maxlength="25" class="form-control" id="spouse_residence" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Spouse Address</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="spouse_address" maxlength="25" class="form-control" id="spouse_address" value="' . ScriptRunner($Script_Edit, "spouse_address") . '" />';
											} else {
												if (isset($_REQUEST["spouse_address"])) {
													echo '<input type="text" name="spouse_address" maxlength="25" class="form-control" id="spouse_address" value="' . $_REQUEST["spouse_address"] . '" />';
												} else {
													echo '<input type="text" name="spouse_address" maxlength="25" class="form-control" id="spouse_address" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Spouse Mobile No</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="spouse_phone" maxlength="25" class="form-control" id="spouse_phone" value="' . ScriptRunner($Script_Edit, "spouse_phone") . '" />';
											} else {
												if (isset($_REQUEST["spouse_phone"])) {
													echo '<input type="text" name="spouse_phone" maxlength="25" class="form-control" id="spouse_phone" value="' . $_REQUEST["spouse_phone"] . '" />';
												} else {
													echo '<input type="text" name="spouse_phone" maxlength="25" class="form-control" id="spouse_phone" value="" />';
												}
											}
											?>
										</div>
									</div>

									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">No of Children:</label>
										<div class="col-sm-8 input-group-sm">
											<select class="form-control" name="no_children" id="no_children">
												<option value="--">--</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<?php if ($EditID != "" || $EditID > 0) {
													echo '<option value="' . ScriptRunner($Script_Edit, "no_children") . '" selected="selected">' . ScriptRunner($Script_Edit, "no_children") . '</option>';
												} else {
													if (isset($_REQUEST["no_children"])) {
														echo '<option selected=selected value="' . $_REQUEST["no_children"] . '">' . $_REQUEST["no_children"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Child 1 HMO ID</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="child1_id" maxlength="25" class="form-control" id="child1_id" value="' . ScriptRunner($Script_Edit, "child1_id") . '" />';
											} else {
												if (isset($_REQUEST["child1_id"])) {
													echo '<input type="text" name="child1_id" maxlength="25" class="form-control" id="child1_id" value="' . $_REQUEST["child1_id"] . '" />';
												} else {
													echo '<input type="text" name="child1_id" maxlength="25" class="form-control" id="child1_id" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Child 4 HMO ID</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="child4_id" maxlength="25" class="form-control" id="child4_id" value="' . ScriptRunner($Script_Edit, "child4_id") . '" />';
											} else {
												if (isset($_REQUEST["child4_id"])) {
													echo '<input type="text" name="child4_id" maxlength="25" class="form-control" id="child4_id" value="' . $_REQUEST["child4_id"] . '" />';
												} else {
													echo '<input type="text" name="child4_id" maxlength="25" class="form-control" id="child4_id" value="" />';
												}
											}
											?>
										</div>
									</div>



								</div>






								<div class="col-md-4">
									<div class="form-group row ">
										<label class="col-sm-4 col-form-label">Total Number of dependant:</label>
										<div class="col-sm-8 input-group-sm">
											<select class="form-control" name="no_dependant" id="no_dependant">
												<option value="--">--</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<?php if ($EditID != "" || $EditID > 0) {
													echo '<option value="' . ScriptRunner($Script_Edit, "no_dependant") . '" selected="selected">' . ScriptRunner($Script_Edit, "no_dependant") . '</option>';
												} else {
													if (isset($_REQUEST["no_dependant"])) {
														echo '<option selected=selected value="' . $_REQUEST["no_dependant"] . '">' . $_REQUEST["no_dependant"] . '</option>';
													} else {
													} //Nothing needed
												}
												?>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">HMO Plan</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hmo_plan" maxlength="25" class="form-control" id="hmo_plan" value="' . ScriptRunner($Script_Edit, "hmo_plan") . '" />';
											} else {
												if (isset($_REQUEST["hmo_plan"])) {
													echo '<input type="text" name="hmo_plan" maxlength="25" class="form-control" id="hmo_plan" value="' . $_REQUEST["hmo_plan"] . '" />';
												} else {
													echo '<input type="text" name="hmo_plan" maxlength="25" class="form-control" id="hmo_plan" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">HMO ID No</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hmo_id" maxlength="25" class="form-control" id="hmo_id" value="' . ScriptRunner($Script_Edit, "hmo_id") . '" />';
											} else {
												if (isset($_REQUEST["hmo_id"])) {
													echo '<input type="text" name="hmo_id" maxlength="25" class="form-control" id="hmo_id" value="' . $_REQUEST["hmo_id"] . '" />';
												} else {
													echo '<input type="text" name="hmo_id" maxlength="25" class="form-control" id="hmo_id" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Primary Hospital</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="primary_hospital" maxlength="25" class="form-control" id="primary_hospital" value="' . ScriptRunner($Script_Edit, "primary_hospital") . '" />';
											} else {
												if (isset($_REQUEST["primary_hospital"])) {
													echo '<input type="text" name="primary_hospital" maxlength="25" class="form-control" id="primary_hospital" value="' . $_REQUEST["primary_hospital"] . '" />';
												} else {
													echo '<input type="text" name="primary_hospital" maxlength="25" class="form-control" id="primary_hospital" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Hospital Address</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hospital_address" maxlength="25" class="form-control" id="hospital_address" value="' . ScriptRunner($Script_Edit, "hospital_address") . '" />';
											} else {
												if (isset($_REQUEST["hospital_address"])) {
													echo '<input type="text" name="hospital_address" maxlength="25" class="form-control" id="hospital_address" value="' . $_REQUEST["hospital_address"] . '" />';
												} else {
													echo '<input type="text" name="hospital_address" maxlength="25" class="form-control" id="hospital_address" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Child 2 HMO ID</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="child2_id" maxlength="25" class="form-control" id="child2_id" value="' . ScriptRunner($Script_Edit, "child2_id") . '" />';
											} else {
												if (isset($_REQUEST["child2_id"])) {
													echo '<input type="text" name="child2_id" maxlength="25" class="form-control" id="child2_id" value="' . $_REQUEST["child2_id"] . '" />';
												} else {
													echo '<input type="text" name="child2_id" maxlength="25" class="form-control" id="child2_id" value="" />';
												}
											}
											?>
										</div>
									</div>


								</div>






								<div class="col-md-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Hospital Contact No1</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hospital_contact_1" maxlength="25" class="form-control" id="hospital_contact_1" value="' . ScriptRunner($Script_Edit, "hospital_contact_1") . '" />';
											} else {
												if (isset($_REQUEST["hospital_contact_1"])) {
													echo '<input type="text" name="hospital_contact_1" maxlength="25" class="form-control" id="hospital_contact_1" value="' . $_REQUEST["hospital_contact_1"] . '" />';
												} else {
													echo '<input type="text" name="hospital_contact_1" maxlength="25" class="form-control" id="hospital_contact_1" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Hospital Contact No 2</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hospital_contact_2" maxlength="25" class="form-control" id="hospital_contact_2" value="' . ScriptRunner($Script_Edit, "hospital_contact_2") . '" />';
											} else {
												if (isset($_REQUEST["hospital_contact_2"])) {
													echo '<input type="text" name="hospital_contact_2" maxlength="25" class="form-control" id="hospital_contact_2" value="' . $_REQUEST["hospital_contact_2"] . '" />';
												} else {
													echo '<input type="text" name="hospital_contact_2" maxlength="25" class="form-control" id="hospital_contact_2" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">HMO Contact No 1</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hmo_contact_1" maxlength="25" class="form-control" id="hmo_contact_1" value="' . ScriptRunner($Script_Edit, "hmo_contact_1") . '" />';
											} else {
												if (isset($_REQUEST["hmo_contact_1"])) {
													echo '<input type="text" name="hmo_contact_1" maxlength="25" class="form-control" id="hmo_contact_1" value="' . $_REQUEST["hmo_contact_1"] . '" />';
												} else {
													echo '<input type="text" name="hmo_contact_1" maxlength="25" class="form-control" id="hmo_contact_1" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">HMO Contact No 2</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="hmo_contact_2" maxlength="25" class="form-control" id="hmo_contact_2" value="' . ScriptRunner($Script_Edit, "hmo_contact_2") . '" />';
											} else {
												if (isset($_REQUEST["hmo_contact_2"])) {
													echo '<input type="text" name="hmo_contact_2" maxlength="25" class="form-control" id="hmo_contact_2" value="' . $_REQUEST["hmo_contact_2"] . '" />';
												} else {
													echo '<input type="text" name="hmo_contact_2" maxlength="25" class="form-control" id="hmo_contact_2" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Spouse HMO ID</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="spouse_hmo_id" maxlength="25" class="form-control" id="spouse_hmo_id" value="' . ScriptRunner($Script_Edit, "spouse_hmo_id") . '" />';
											} else {
												if (isset($_REQUEST["spouse_hmo_id"])) {
													echo '<input type="text" name="spouse_hmo_id" maxlength="25" class="form-control" id="spouse_hmo_id" value="' . $_REQUEST["spouse_hmo_id"] . '" />';
												} else {
													echo '<input type="text" name="spouse_hmo_id" maxlength="25" class="form-control" id="spouse_hmo_id" value="" />';
												}
											}
											?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Child 3 HMO ID</label>
										<div class="col-sm-8 input-group-sm">
											<?php
											if ($EditID != "" || $EditID > 0) {
												echo '<input type="text" name="child3_id" maxlength="25" class="form-control" id="child3_id" value="' . ScriptRunner($Script_Edit, "child3_id") . '" />';
											} else {
												if (isset($_REQUEST["child3_id"])) {
													echo '<input type="text" name="child3_id" maxlength="25" class="form-control" id="child3_id" value="' . $_REQUEST["child3_id"] . '" />';
												} else {
													echo '<input type="text" name="child3_id" maxlength="25" class="form-control" id="child3_id" value="" />';
												}
											}
											?>
										</div>
									</div>


								</div>
								<div class="col-sm-12">
									<?php
									if (ValidateURths("EMPLOYEE" . "A") == true || ValidateURths("EMPLOYEE" . "B") == true) {
										if ($_REQUEST["PgTy"] == "AddIndTrans" && ($EditID == "" || $EditID == '--')) {
											if (ValidateURths("EMPLOYEE" . "A") == true) {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right" id="SubmitTrans" value="Create Employee" />';
											}
										} elseif ($EditID != "" && $EditID != '--') {
											$AuthStat = "";
											/*
        if (ScriptRunner($Script_Edit,"Status") =='A')
        {
        $AuthStat = "A";
        echo '<input name="SubmitTrans" disabled="disabled" type="submit" class="smallButton_disabled" id="SubmitTrans" value="Update Record"/>';
        }
        else
        {
         */
											$EmpStatus = ScriptRunner($Script_Edit, "EmpStatus");
											if ($EmpStatus == "Active") //if (isset($_REQUEST["EmpStatus"]))
											{
												if ($_REQUEST["PgTy"] == "ModifySelf") {
													// if (ValidateURths("EMPLOYEE" . "B") == true) {
													echo '<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right" id="SubmitTrans" value="Update My Record" />';
													echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
													// }
												} else {
													if (ValidateURths("EMPLOYEE" . "B") == true) {
														echo '<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right" id="SubmitTrans" value="Update Record" />';
														echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
													}
												}
											} else {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right" id="SubmitTrans" value="ReActivate" />';
												echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
												//echo '<input name="" disabled="disabled" type="submit" class="smallButton_disabled" id="" value="Update Record" />';
											}
											//}
										} else {
											if (ValidateURths("EMPLOYEE" . "A") == true) {
												echo '<input name="SubmitTrans" type="submit" class="btn btn-danger pull-right" id="SubmitTrans" value="Create Employee" />';
											}
										}
									}
									?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="educational">
							<?php echo '<iframe id="frame_employee_edu" name="frame_employee_edu" src="add_IQual_Any.php?PgTy=Create&PgTy=Create&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>';
							?>
						</div>
						<div class="tab-pane" id="history">
							<?php
							echo '<iframe id="frame_employee_emp" name="frame_employee_emp" src="add_IWok_Any.php?PgTy=Create&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>'; ?>
						</div>
						<div class="tab-pane" id="references">
							<?php echo '<iframe id="frame_employee_ref" name="frame_employee_ref" src="add_IRef_Any.php?PgTy=Create&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								          </iframe>';
							?>
						</div>
						<div class="tab-pane" id="others">
							<?php
							echo '<iframe id="frame_other_doc" name="frame_other_doc" src="add_IMgt.php?PgTy=Search&HHeader=True&SubmitDoc=Go&Search=' . $EditID . '&StaffID=' . $EditID . '"" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								</iframe>'; ?>
						</div>
						<div class="tab-pane" id="comp_property">
							<?php
							echo '<iframe id="frame_employee_assets" name="frame_employee_assets" src="add_ItmAss.php?PgTy=' . $_REQUEST["PgTy"] . '&HHeader=True&SubmitDoc=Go&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="600" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								    </iframe>'; ?>
						</div>
						<div class="tab-pane" id="appraisal">
							<?php
							echo '<iframe id="frame_employee_appraisal" name="frame_employee_appraisal" src="add_IAppr.php?PgTy=Search&HHeader=True&SubmitDoc=Go&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								    </iframe>'; ?>
						</div>
						<div class="tab-pane" id="leave">
							<?php
							echo '<iframe id="frame_employee_leave" name="frame_employee_leave" src="../hrm/add_ILv_Any.php?PgTy=Search&HHeader=True&SubmitDoc=Go&Search=' . $EditID . '&StaffID=' . $EditID . '"" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								    </iframe>'; ?>
						</div>
						<div class="tab-pane" id="salary">
							<?php
							echo '<iframe id="frame_employee_sal" name="frame_employee_sal" src="../hrm/add_PayR_Any.php?PgTy=Search&HHeader=True&SubmitDoc=Go&HSearch=' . $EditID . '&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								    </iframe>'; ?>
						</div>
						<div class="tab-pane" id="docs">
							<?php
							echo '<iframe id="frame_employee_doc" name="frame_employee_doc" src="../dms/inv_doc_search_Any.php?PgTy=Search&HHeader=True&SubmitDoc=Go&HSearch=' . $EditID . '&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								    </iframe>'; ?>
						</div>
						<div class="tab-pane" id="line_report">
							<?php
							echo '<iframe id="frame_employee_line" name="frame_employee_line" src="../hrm/line_report.php?PgTy=Search&HHeader=True&SubmitDoc=Go&HSearch=' . $EditID . '&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								</iframe>'; ?>
						</div>
						<div class="tab-pane" id="my_query">
							<?php
							echo '<iframe id="frame_employee_query" name="frame_employee_query" src="../hrm/employee_query.php?PgTy=Search&HHeader=True&SubmitDoc=Go&HSearch=' . $EditID . '&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								</iframe>'; ?>
						</div>

						<div class="tab-pane" id="my_request">
							<?php
							echo '<iframe id="frame_employee_query" name="frame_employee_query" src="../hrm/employee_request.php?PgTy=Search&HHeader=True&SubmitDoc=Go&HSearch=' . $EditID . '&StaffID=' . $EditID . '" marginheight="0" marginwidth="0" width="100%" height="460" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p>
								</iframe>'; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php //echo '<input name="DelMax" id="DelMax" type="hidden" value="'.$Del.'" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
		echo '<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
				<input name="FAction" id="FAction" type="hidden" />
				<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
		?>
	</form>
	<!-- jQuery 3 -->
	<!-- <script src="assets/assets/vendor_components/jquery/dist/jquery.min.js"></script> -->



	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>


	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<script>
		$('.select2').select2();
	</script>
</body>

</html>
<script>
	function updateCode(selectElement) {
		// Get the selected option
		var selectedOption = selectElement.options[selectElement.selectedIndex];

		// Get the data-code attribute value
		var dataCode = selectedOption.getAttribute('data-code');
		document.getElementById('bank_code').value = dataCode;

		console.log({
			dataCode
		})

	}

	function deleteFun() {
		const EmpStatus = $("#EmpStatus").val();
		if (EmpStatus !== "Active") {

			//    document.getElementById("SubmitTrans").value="Update Record";


			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText>Do you wish to <strong>change employee status</strong> to ' + EmpStatus + '?</h5></div>')
				.dialog({
					modal: true,
					title: "Change Employee Status",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							$("#autoappend").append('<div id="toremove"><input type="hidden" value="Update Record"  name="SubmitTrans"/><input type="hidden" value=""  name="ExDt22"  id="ExDt22" /><input type="hidden" value=""  name="exitreasons"  id="exitreasons" /></div>');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea><br><input type="date" class="form-control" name="ExDt5" id="ExDt5" value=""  /></span>').dialog({

									modal: true,
									title: " Exit Reasons with date ",
									zIndex: 10000,
									autoOpen: true,
									height: 250 + 6,
									width: 500 + 20,
									buttons: {

										Update: function() {

											const msgC = document.getElementById("sd").value;
											const ExDt5 = document.getElementById("ExDt5").value;
											if (document.getElementById("sd").value == '' || document.getElementById("ExDt5").value == '') {

												{
													parent.msgbox('Please provide a Exit reason/date.', 'red');
												};

												return;

											}
											//  $("#autoappend").append('<input type="text" value="Update My Record"  name="Trans"  id="toremove" />');


											// document.getElementById('Tr').value ='Update My Record';

											document.getElementById("ExDt22").value = document.getElementById("ExDt5").value;
											document.getElementById("exitreasons").value = document.getElementById("sd").value;
											// console.log(document.getElementById("ExDt").value);
											// return;
											// console.log(document.getElementById("SubmitTrans").value);
											document.getElementById('Records').submit();



											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {
							const EmpStatus = $("#EmpStatus").val();
							if (EmpStatus === "Active") {
								const parent = document.getElementById("autoappend");
								const child = document.getElementById("toremove");
								parent.removeChild(child);
							}
							//  $("#EmpStatus").val('Active');
							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						const EmpStatus = $("#EmpStatus").val();
						if (EmpStatus === "Active") {
							const parent = document.getElementById("autoappend");
							const child = document.getElementById("toremove");
							parent.removeChild(child);
						}
						//   $("#EmpStatus").val('Active');
						$(this).remove();
					}
				});

		}






	}
</script>
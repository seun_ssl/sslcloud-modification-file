<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("GROUPS" . "V") != true) {include '../main/NoAccess.php';exit;}

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
//echo $_REQUEST["FAction"].$_REQUEST["DelMax"];

if (!isset($EditID)) {$EditID = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($ChkName)) {$ChkName = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}

if (!isset($DelBox)) {$DelBox = 0;}
if (!isset($if_authorized_for_view)) {$if_authorized_for_view = "0";}

$GoValidate = true;

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
//echo "here".$_REQUEST["FAction"].$_REQUEST["DelMax"];
    //exit;
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Group Selected" && strlen($_REQUEST["UpdID"]) == 32) {
        if (ValidateURths("GROUPS" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set HashKey of Account to be Updated */
        $EditID = ECh($_REQUEST["UpdID"]);

        if ($GoValidate == true) {
            //UPDATING MODULES BEGINS HERE
            //=******************************************************************************************
            //=**************************************** PREFERENCES *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='PREF' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_pref.php';
            }

            //=******************************************************************************************
            //=**************************************** PREFERENCES *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='TAM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_train.php';
            }

            //=******************************************************************************************
            //=******************************************** AMS ******************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='AMS' and CID='" . ECh($_SESSION["StkTck" . "CustID"]) . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_ams.php';
            }

            //=******************************************************************************************
            //=******************************************** HR ******************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='HRM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_hrm.php';
            }

            //UPDATING MODULES BEGINS HERE
            //=******************************************************************************************
            //=**************************************** PREFERENCES *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='PREF' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_pref.php';
            }
            //=******************************************************************************************
            //=************************************** PAYROLL *******************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='PAY' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_proll.php';
            }

            //=******************************************************************************************
            //=************************************** KPI MODULE **************************************** APPRAISAL
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='KPI' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_kpi.php';
                include 'rt_kpi_confirm.php';
            }

            //=******************************************************************************************
            //=********************************** LEAVE MANAGEMENT SYSTEM *******************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='LMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_lms.php';
            }

            //=******************************************************************************************
            //=***************************** DOCUMENT MANAGEMENT SYSTEM *********************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='DMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_dms.php';
            }

            //=******************************************************************************************
            //=************************************ ACCOUNTING **************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='ACCT' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_acct.php';
            }

            //=******************************************************************************************
            //=************************** PARISH INFORMATION MANAGEMENT *********************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='PIM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_pim.php';
            }

            //=******************************************************************************************
            //=************************************ HOSPITAL MANAGEMENT *********************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='HMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_hms.php';
            }

            //=******************************************************************************************
            //=************************************ CONTACT MANAGEMENT **********************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='CCT' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_ctms.php';
            }

            //=******************************************************************************************
            //=**************************************** BOOKING MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='BKM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_bkm.php';
            }
            //******************************************************************************************
            //=************************************ TODO LIST *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='TDL' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_tdl.php';
            }

            //=******************************************************************************************
            //=**************************************** REQUEST MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='RQM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_hrm.php';
            }

            //=******************************************************************************************
            //=**************************************** DISCIPLINARY MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='DSM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_dsm.php';
            }

            //=******************************************************************************************
            //=**************************************** RECRUITMENT MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='REC' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_rec.php';
            }

			 //=******************************************************************************************
            //=**************************************** INVENTORY MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='INV' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_inv.php';
            }

			 //=******************************************************************************************
            //=**************************************** SHIFT MANAGEMENT *************************************
            //=******************************************************************************************
            $Script = "Select * from CLicense where CModule='SHM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
            if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
                include 'rt_shm.php';
            }

            //=******************************************************************************************
            //=************************* ALL RIGHTS AND PREVILEDGES END HERE ****************************
            //=******************************************************************************************

            //=******************************************************************************************
            //=************************* ALL RIGHTS AND PREVILEDGES END HERE ****************************
            //=******************************************************************************************

            AuditLog("UPDATE", "Group account updated for [" . Ech($_REQUEST["LogName"]) . "]", $HashKey);
            echo ("<script type='text/javascript'>{ parent.msgbox('Group account updated successfully.', 'green'); }</script>");

            //=CHeck validation for updating an account */
            //    if ($GoValidate == true)
            //    {
            //        $Script = "Update [UGpRights] set [LogPassword]='".$Pwd."',[FullName]='".$_REQUEST["FullName"]."',[GpName]='".$_REQUEST["GpName"]."',[AcctState]='".$AcctState."',[PassDate]='',[PassExpire]='01-Jan-1900',[ULock]='',[LogCnt]='',[email]='".$_REQUEST["Email"]."',[mobile]='".$_REQUEST["Mobile"]."',[LStatus]='".$LStatus."',[Status]='U',[UpdatedBy]='".$_SESSION["StkTck"."HKey"]."',[UpdatedDate]=GetDate() where [HashKey]= '".$HashKey."'";
            //        ScriptRunnerUD($Script,"Inst");
            //    }
        }
    } //End Group update

    /* elseif ($_REQUEST["SubmitTrans"] == "Open")
    {
    if (strlen($_REQUEST["GroupName"])>2)
    {
    $EditID = $_REQUEST["GroupName"];
    $Script_Edit ="select * from [UGpRights] where GPType='P' and HashKey='" . $EditID . "'";
    }
    else
    {
    $EditID = '--';
    $Script_Edit ="select * from [Groups] where HashKey='--'";
    }
    } */

    elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                //exit('stop');
                $EditID = ECh($HashVals[$i]);
                $Script_Edit = "select * from [UGpRights] where GPType='P' and HashKey='" . $EditID . "'";

                $Script_ID = "SELECT GpName, Status, HashKey from UGpRights where HashKey='" . $EditID . "'";
                $kk = ScriptRunner($Script_ID, "Status");
                //if ($kk == 'A' || $kk == 'D'){
                if ($kk == 'D') {
                    $EditID = '';
                }
                if ($kk == 'A') {
                    $if_authorized_for_view = 1;
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {

        if (ValidateURths("GROUPS" . "T") != true) {include '../main/NoAccess.php';exit;}
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT GpName, HashKey from UGpRights where HashKey='" . $EditID . "'";

                if (ScriptRunner($Script_ID, "HashKey") == $EditID) {$HashKey = ScriptRunner($Script_ID, "HashKey");
                    //=************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                    //Check if account is customer account and if SMS alert is set to fire

                    $Script = "UPDATE [UGpRights]
					SET [Status] = 'A'
					,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
					,[AuthDate]=GetDate()
					WHERE ([GpName] = '" . $HashKey . "' or [HashKey] = '" . $HashKey . "')";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("AUTHORIZE", "User group rights authorized for group [" . ScriptRunner($Script_ID, "GpName") . "]");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected user group(s) authorized successfully.', 'green'); }</script>");
                }
            }
        }
        //Should fire wether validate is true or false
        //Clear Selection//
        $Script_Edit = "Select * from UGpRights where HashKey='z'";
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("GROUPS" . "T") != true) {include '../main/NoAccess.php';exit;}
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script_ID = "SELECT HashKey from [UGpRights] where HashKey='" . $EditID . "'";
                $AuthID = ScriptRunner($Script_ID, "HashKey");

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                //Check if account is customer account and if SMS alert is set to fire
                $Script_ID = "Select GpName from [UGpRights] where HashKey='" . $AuthID . "'";

                $Script = "UPDATE [UGpRights]
					SET [Status] = 'U'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $AuthID . "'";
                ScriptRunnerUD($Script, "Unauthorize");
                AuditLog("UNAUTHORIZE", "User group unauthorized for group name [" . ScriptRunner($Script_ID, "GpName") . "]");

                echo ("<script type='text/javascript'>{ parent.msgbox('Selected group unauthorized successfully.', 'green'); }</script>");

                //Clear Selection//
                $Script_Edit = "Select *, convert(varchar(5),GpLogInT,114) GpLogInT, convert(varchar(5),GpLogOutT,114) GpLogOutT  from UGpRights where HashKey='" . $EditID . "'";
                $EditID = "--";
            }
        }
        //Should fire wether validate is true or false
        //Clear Selection//
        $Script_Edit = "Select *, convert(varchar(5),GpLogInT,114) GpLogInT, convert(varchar(5),GpLogOutT,114) GpLogOutT  from UGpRights where HashKey='" . $EditID . "'";
        $EditID = "";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("GROUPS" . "D") != true) {include '../main/NoAccess.php';exit;}
        $no_of_iter = (int) $_REQUEST["DelMax"];
        for ($i = 1; $i <= $no_of_iter; $i++) {
            $EditID = $_REQUEST["DelBox" . $i];
            if (count($EditID) > 0) {
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [UGpRights] WHERE [HashKey] = '" . $EditID . "'") == true) {

                    $Script_ID = "SELECT HashKey from [UGpRights] where HashKey='" . $EditID . "'";
                    $AuthID = ScriptRunner($Script_ID, "HashKey");

                    $Script = "UPDATE [UGpRights]
					SET [Status] = 'D'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $AuthID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("DELETE", "User group deleted for group name [" . ScriptRunner($Script_ID, "GpName") . "]", $AuthID);

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected group deleted successfully.', 'green'); }</script>");

                    //Clear Selection//
                    $Script_Edit = "Select *, convert(varchar(5),GpLogInT,114) GpLogInT, convert(varchar(5),GpLogOutT,114) GpLogOutT  from UGpRights where HashKey='" . $EditID . "'";
                    $EditID = "--";
                }
            }
        }
    }
}
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="../css/style_main.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <style type="text/css">
        .auto-style1 {
            font-size: 9px;
        }

        .with-border {
            cursor: pointer;
        }

    </style>
</head>

<body oncontextmenu="return false;">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records"
        autocomplete="off">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12 text-center ">
                        <h5>
                            User Group Rights
                            <span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php
if ($EditID != "" && $EditID != "--") {
    $Script_Edit = "select * from [UGpRights] where GPType='P' and HashKey='" . $EditID . "'";
    $kk = ScriptRunner($Script_Edit, "GpName");
    echo $kk;
    echo "<input type='hidden' name='LogName' id='LogName' value='" . $kk . "'>";
} else {}
?>
                            </span>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <?php
$Script = "Select * from CLicense where CModule='PREF' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                <div class="box box-slided-up ">
                    <div class="box-header with-border">

                        <h4 class="box-title"> PREFERENCES (Security & Audit)</h4>
                        <ul class="box-controls pull-right">
                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox1" class="test" value="" />
                            <label for="DelBoox1"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ADD/EDIT OFFICES</h6>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BRANCH CREATION" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH CREATION" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BRANCH CREATION" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH CREATION" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BRANCH CREATION" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH CREATION" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BRANCH CREATION" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH CREATION" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ORG. PREFERENCES</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES ORG" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES ORG" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES ORG" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES ORG" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES ORG" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES ORG" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">SECURITY PREFERENCES</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES SEC" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES SEC" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES SEC" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES SEC" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES SEC" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES SEC" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">NOTIFICATION TEMP.</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("NOTIFICATION" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "NOTIFICATION" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("NOTIFICATION" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "NOTIFICATION" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">CREATE/EDIT USER</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("USERS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "USERS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("USERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "USERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("USERS" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "USERS" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("USERS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "USERS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">CREATE/EDIT GROUP</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS_" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS_" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">GROUP RIGHTS</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS RT" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS RT" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS RT" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS RT" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("GROUPS RT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "GROUPS RT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">MAIN MENU SETTINGS</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("NAVIGATOR BAR" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "NAVIGATOR BAR" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("NAVIGATOR BAR" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "NAVIGATOR BAR" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("NAVIGATOR BAR" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "NAVIGATOR BAR" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">SORT LINK MENUS</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SORT USER MENUS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SORT USER MENUS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SORT USER MENUS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SORT USER MENUS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">SORT LINK REPORTS</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SORT REPORT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SORT REPORT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SORT REPORT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SORT REPORT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">PERSONAL SETTINGS</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PERSONAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PERSONAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View My
                                                Records</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PERSONAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PERSONAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Change My
                                                Password</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PERSONAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PERSONAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Employee
                                                Director</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PERSONAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PERSONAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit My
                                                Bio-Data</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SUGGESTION FORM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SUGGESTION FORM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Create
                                                Suggestions</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("OVERTIME FORM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "OVERTIME FORM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Create
                                                Overtime</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">PREF. REPORT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PREFERENCES REPORT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PREFERENCES REPORT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <?php
}

// TRAINING MODULE
// __________________________________________________________________________________________________________________________________________
$Script = "Select * from CLicense where CModule='TAM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                <div class="box box-slided-up">
                    <div class="box-header with-border">
                        <h4 class="box-title">TRAINING MODULE</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox2" class="test" value="" />
                            <label for="DelBoox2"></label>
                        </div>
                    </div>

                    <div class="box-content">

                        <div class="box-body">

                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">PRE EVALUATION ASSESSMENT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PRE EVA" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PRE EVA" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PRE EVA" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PRE EVA" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PRE EVA" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PRE EVA" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PRE EVA" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PRE EVA" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">POST EVALUATION ASSESSMENT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("POST EVA" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "POST EVA" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("POST EVA" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "POST EVA" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ACTION PLAN(Employee)</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APP" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APP" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APP" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APP" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ACTION PLAN(HOD)</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APH" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APH" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APH" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APH" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ACTION PLAN(Manager)</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APM" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APM" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">DASHBOARD</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TR DASH" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TR DASH" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TR DASH" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TR DASH" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TR DASH" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TR DASH" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ACTION PLAN MEASUREMENT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMP" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMP" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">APM MANAGER</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">APM HOD</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMH" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMH" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APMH" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APMH" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">TRAINING SETTING</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRAIN SETTING" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRAIN SETTING" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRAIN SETTING" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRAIN SETTING" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRAIN SETTING" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRAIN SETTING" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">VIEW MY ASSESSMENT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW ASSESSMENT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW ASSESSMENT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ASSESSMENT COMPARISON</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ASSESSMENT COMPARISON" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ASSESSMENT COMPARISON" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">TRAINING REPORT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRAIN REPORT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRAIN REPORT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                </div>
                                <div class="box">
                                </div>
                                <div class="box">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <?php
}

// }
//*************************** TODO LIST MODULE *******************************************
$Script = "Select * from CLicense where CModule='TDL' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1)
// {
{
    ;
}
?>
                <div class="box box-slided-up ">
                    <div class="box-header with-border">
                        <h4 class="box-title">TASK MANAGER</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox13" class="test" value="" />
                            <label for="DelBoox13"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">Add Task</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD TASK" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD TASK" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD TASK" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD TASK" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD TASK" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD TASK" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD TASK" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD TASK" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">View Task</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TASKS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TASKS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TASKS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TASKS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TASKS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TASKS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TASKS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TASKS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">Team Members</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("TEAM MEMBERS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM MEMBERS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("TEAM MEMBERS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM MEMBERS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("TEAM MEMBERS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM MEMBERS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("TEAM MEMBERS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM MEMBERS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">Branch Members</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("BRANCH MEMBERS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM MEMBERS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("BRANCH MEMBERS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH MEMBERS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("BRANCH MEMBERS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH MEMBERS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("BRANCH MEMBERS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BRANCH MEMBERS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">All Employees</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALL EMPLOYEES" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALL EMPLOYEES" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALL EMPLOYEES" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALL EMPLOYEES" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALL EMPLOYEES" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALL EMPLOYEES" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALL EMPLOYEES" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALL EMPLOYEES" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">View Team Tasks</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TEAM TASKS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TEAM TASKS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TEAM TASKS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TEAM TASKS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TEAM TASKS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TEAM TASKS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW TEAM TASKS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW TEAM TASKS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>

                <?php

// BOOKING MANAGEMENT MODULE
// __________________________________________________________________________________________________________________________________________
$Script = "Select * from CLicense where CModule='BKM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                <div class="box box-slided-up">
                    <div class="box-header with-border">
                        <h4 class="box-title">BOOKING MANAGEMENT MODULE</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox3" class="test" value="" />
                            <label for="DelBoox3"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> ADD BOOKING ITEM </h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD BOOKING ITEM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BOOKING ITEM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD BOOKING ITEM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BOOKING ITEM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit</label>
                                        </div>


                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> BOOK AN ITEM</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BOOK ITEM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BOOK ITEM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>


                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BOOK ITEM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BOOK ITEM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit</label>
                                        </div>


                                    </div>
                                </div>


                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> MANAGE BOOKED ITEM</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MANAGE BOOK ITEM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MANAGE BOOK ITEM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>


                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MANAGE BOOK ITEM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MANAGE BOOK ITEM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit</label>
                                        </div>

                                    </div>
                                </div>



                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> ADD/EDIT ITEM GROUP</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD EDIT ITEM GROUP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD EDIT ITEM GROUP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD EDIT ITEM GROUP" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD EDIT ITEM GROUP" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit</label>
                                        </div>


                                    </div>
                                </div>



                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> BOOKINGS HISTORY</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BOOKINGS HISTORY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BOOKINGS HISTORY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>

                                        <!-- <div class="demo-checkbox">
											<?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BOOKINGS HISTORY" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BOOKINGS HISTORY" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
											<label for="<?=$ChkName?>" style="margin-bottom: 5px">Edit</label>
										</div> -->


                                    </div>
                                </div>




                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> AUTHORIZE TEAM REQUEST</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE TEAM REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE TEAM REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>




                                    </div>
                                </div>



                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> AUTHORIZE ALL REQUEST</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE ALL REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE ALL REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>




                                    </div>
                                </div>









                            </div>

                        </div>
                    </div>




                </div>


                <?php
}

//***************************REQUEST MGT MODULE*******************************************

$Script = "Select * from CLicense where CModule='RQM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                <div class="box box-slided-up">
                    <div class="box-header with-border">
                        <h4 class="box-title">REQUEST MANAGEMENT</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox11" class="test" value="" />
                            <label for="DelBoox11"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">CREATE ANY FORM </h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE FORM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE FORM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Create any
                                                Form</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("REQUEST FORM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FORM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Request
                                                Form</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE FORM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE FORM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Approve
                                                Form</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("REQUEST FORM HISTORY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FORM HISTORY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Form Request
                                                History</label>
                                        </div>

                                    </div>
                                </div>






                            </div>







                        </div>

                    </div>

                </div>


                <?php
}

//***************************DISCIPLINARY MGT MODULE******************************

$Script = "Select * from CLicense where CModule='DSM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                <div class="box box-slided-up">
                    <div class="box-header with-border">
                        <h4 class="box-title">DISCIPLINARY MANAGEMENT</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox12" class="test" value="" />
                            <label for="DelBoox12"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center"> </h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE QUERY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE QUERY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View Create any
                                                Query</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("QUERY EMPLOYEE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "QUERY EMPLOYEE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Query any Employee
                                            </label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("QUERY BRANCH" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "QUERY BRANCH" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Query Branch Employee
                                            </label>
                                        </div>


                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("QUERY TEAM MEMBER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "QUERY TEAM MEMBER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Query team members
                                            </label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE QUERY REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE QUERY REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Approve query request
                                            </label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ANSWER QUERY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ANSWER QUERY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Answer a query</label>
                                        </div>

                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MANAGE QUERY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MANAGE QUERY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Manage Query</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("QUERY HISTORY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "QUERY HISTORY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Query Request
                                                History</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("QUERY HISTORY ALL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "QUERY HISTORY ALL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">All Query Request
                                                History</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BLANK QUERY TEMPLATE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BLANK QUERY TEMPLATE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Blank Query
                                                Template</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <?php
}

//***************************SHIFT MGT MODULE******************************

$Script = "Select * from CLicense where CModule='SHM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                <div class="box box-slided-up">
                    <div class="box-header with-border">
                        <h4 class="box-title">SHIFT MANAGEMENT</h4>
                        <ul class="box-controls pull-right">

                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a></li>

                        </ul>
                        <div class="box-controls pull-right">
                            <input type="checkbox" id="DelBoox15" class="test" value="" />
                            <label for="DelBoox15"></label>
                        </div>
                    </div>
                    <div class="box-content">

                        <div class="box-body">
                            <div class="box-group bg-lightest">
                                <div class="box">
                                    <div class="box-body">
                                        <h6 class="box-title b-0 px-0 text-center">ADD SHIFT</h6>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD SHIFT" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD SHIFT" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Add/Update</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD SHIFT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD SHIFT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD SHIFT" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD SHIFT" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                        </div>
                                        <div class="demo-checkbox">
                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD SHIFT" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD SHIFT" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Un/Authorize</label>
                                        </div>
                                    </div>
                                </div>

                                        <div class="box">
                                            <div class="box-body">
                                                <h6 class="box-title b-0 px-0 text-center">VIEW SHIFTS</h6>
                                                <div class="demo-checkbox">
                                                    <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFTS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFTS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                    <label for="<?=$ChkName?>"
                                                        style="margin-bottom: 5px">Add/Update</label>
                                                </div>
                                                <div class="demo-checkbox">
                                                    <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFTS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFTS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                    <label for="<?=$ChkName?>" style="margin-bottom: 5px">View</label>
                                                </div>
                                                <div class="demo-checkbox">
                                                    <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFTS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFTS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                    <label for="<?=$ChkName?>" style="margin-bottom: 5px">Delete</label>
                                                </div>
                                                <div class="demo-checkbox">
                                                    <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFTS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFTS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                    <label for="<?=$ChkName?>"
                                                        style="margin-bottom: 5px">Un/Authorize</label>
                                                </div>
                                            </div>
                                        </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MY ALLOCATIONS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("MY ALLOCATIONS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MY ALLOCATIONS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("MY ALLOCATIONS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MY ALLOCATIONS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("MY ALLOCATIONS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MY ALLOCATIONS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("MY ALLOCATIONS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MY ALLOCATIONS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--  -->


                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ALLOCATE SHIFT</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALLOCATE SHIFT" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALLOCATE SHIFT" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALLOCATE SHIFT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALLOCATE SHIFT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALLOCATE SHIFT" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALLOCATE SHIFT" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ALLOCATE SHIFT" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALLOCATE SHIFT" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--  -->

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ADD AUTHORIZERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD AUTHORIZERS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD AUTHORIZERS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD AUTHORIZERS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD AUTHORIZERS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD AUTHORIZERS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD AUTHORIZERS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD AUTHORIZERS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD AUTHORIZERS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--  -->

                                                <!-- DISPLAY RIGHTS ON A LINK -->
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">SWAP REQUEST</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("SWAP REQUEST" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SWAP REQUEST" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("SWAP REQUEST" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SWAP REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("SWAP REQUEST" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SWAP REQUEST" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("SWAP REQUEST" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SWAP REQUEST" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
<!-- END -->
                                                                                                <!--  -->
                                            <!-- DISPLAY RIGHTS ON A LINK -->
                                            <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW MY SHIFT STATISTICS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY SHIFT STATISTICS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY SHIFT STATISTICS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY SHIFT STATISTICS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY SHIFT STATISTICS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY SHIFT STATISTICS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY SHIFT STATISTICS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY SHIFT STATISTICS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY SHIFT STATISTICS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
<!-- END -->

                                                <!--  -->
                                            <!-- DISPLAY RIGHTS ON A LINK -->
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW SHIFT STATISTICS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFT STATISTICS" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFT STATISTICS" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFT STATISTICS" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFT STATISTICS" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFT STATISTICS" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFT STATISTICS" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW SHIFT STATISTICS" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW SHIFT STATISTICS" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
<!-- END -->


<!--  -->

<div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW MY PAYROLL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY PAYROLL" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY PAYROLL" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY PAYROLL" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY PAYROLL" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY PAYROLL" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY PAYROLL" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW MY PAYROLL" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW MY PAYROLL" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>


<!--  -->

<!--  -->

<div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW PAYROLL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW PAYROLL" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW PAYROLL" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW PAYROLL" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW PAYROLL" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW PAYROLL" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW PAYROLL" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("VIEW PAYROLL" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW PAYROLL" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>


<!--  -->

                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <?php
}

//***************************INVENTORY MGT MODULE******************************

$Script = "Select * from CLicense where CModule='INV' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">INVENTORY MANAGEMENT</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox14" class="test" value="" />
                                            <label for="DelBoox14"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center"> CATEGORY </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD CATEGORY" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CATEGORY" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CATEGORY" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CATEGORY" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD CATEGORY" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CATEGORY" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CATEGORY" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CATEGORY" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CATEGORY" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CATEGORY" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center"> BRAND </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD BRAND" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BRAND" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD BRAND" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BRAND" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD BRAND" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BRAND" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD BRAND" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BRAND" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD BRAND" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD BRAND" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center"> ORDER </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD ORDER" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD ORDER" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD ORDER" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD ORDER" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD ORDER" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD ORDER" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD ORDER" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD ORDER" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD ORDER" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD ORDER" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center"> PRODUCT </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD PRODUCT" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD PRODUCT" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD PRODUCT" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD PRODUCT" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD PRODUCT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD PRODUCT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD PRODUCT" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD PRODUCT" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD PRODUCT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD PRODUCT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <?php
}


//***************************HR MODULE*******************************************
$Script = "Select * from CLicense where CModule='HRM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">HUMAN RESOURCES</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox4" class="test" value="" />
                                            <label for="DelBoox4"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">CREATE EMPLOYEE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROSPECT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROSPECT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROSPECT" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROSPECT" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROSPECT" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROSPECT" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROSPECT" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROSPECT" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROSPECT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROSPECT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">EDIT ANY EMPLOYEE
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Download/Share File</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">JOB ROLE LEVEL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DICIPLINARY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DICIPLINARY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DICIPLINARY" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DICIPLINARY" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DICIPLINARY" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DICIPLINARY" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">AUTHORIZE EMPLOYEE
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE EXIT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE EXIT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE EXIT" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE EXIT" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE EXIT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE EXIT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">HR REPORTS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HR REPORTS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HR REPORTS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <label for="" style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <label for="" style="margin-bottom: 5px">Export</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MODULE MASTERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HR MODULE MASTERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HR MODULE MASTERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">EDIT HR SETTINGS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HR SETTINGS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HR SETTINGS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HR SETTINGS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HR SETTINGS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HR SETTINGS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HR SETTINGS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">UPDATE REQUEST</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("UPDATE REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "UPDATE REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("UPDATE REQUEST" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "UPDATE REQUEST" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Request</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("UPDATE REQUEST" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "UPDATE REQUEST" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Cancel</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- LAWRENCE -->
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">Events Manager</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ALL EVENTS MANAGER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ALL EVENTS MANAGER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EVENTS MANAGER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EVENTS MANAGER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Create</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">SUGGESTION REQUESTS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SUGGESTION VIEW" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SUGGESTION VIEW" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">OVERTIME REQUESTS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VERIFY OVERTIME REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VERIFY OVERTIME REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Veiw
                                                                Verify Request</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE OVERTIME REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE OVERTIME REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Approve Request</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("HISTORY OVERTIME REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "HISTORY OVERTIME REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Request History </label>
                                                        </div>

                                                    </div>
                                                </div>



                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">OTHERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ID CARD" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ID CARD" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Veiw
                                                                Employee ID Card</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RENEWAL AGREEMENT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RENEWAL AGREEMENT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Renewal Agreement </label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EDIT RENEWAL AGREEMENT" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EDIT RENEWAL AGREEMENT" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update
                                                                Renewal Agreement </label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CONFIRMATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CONFIRMATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Confirmation Letter</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PROBATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PROBATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Probation Letter </label>
                                                        </div>
                                                        <?php if($_SESSION['StkTckCustID'] !== "092f904d7a11f9ad2c827afe889d7cda"){?>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESIGNATION ACCEPTANCE LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESIGNATION ACCEPTANCE LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Resignation Acceptance Letter </label>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if($_SESSION['StkTckCustID'] !== "092f904d7a11f9ad2c827afe889d7cda"){?>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYMENT TERMINATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYMENT TERMINATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Employment Termination Letter </label>
                                                        </div>
                                                        <?php } ?>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYOFF RESIGNATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYOFF RESIGNATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Payoff Resignation Letter </label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYOFF TERMINATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYOFF TERMINATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                            Payoff Termination Letter </label>
                                                        </div>
                                                        <?php if($_SESSION['StkTckCustID'] === "092f904d7a11f9ad2c827afe889d7cda"){?>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LATE ENTITLEMENT LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LATE ENTITLLEMENT LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                            Late Entitlement Letter </label>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if($_SESSION['StkTckCustID'] !== "092f904d7a11f9ad2c827afe889d7cda"){?>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESIGNATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESIGNATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Resignation Letter </label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EDIT RESIGNATION LETTER" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EDIT RESIGNATION LETTER" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Update
                                                                Resignation Letter </label>
                                                        </div>
<?php } ?>

                                                    </div>
                                                </div>


                                            </div>


                                        </div>

                                    </div>

                                </div>


                                <?php
}
//***************************KPI MODULE*******************************************
$Script = "Select * from CLicense where CModule='KPI' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">APPRAISAL MODULE</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox5" class="test" value="" />
                                            <label for="DelBoox5"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">

                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">CREATE EMPLOYEE
                                                            APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE APPRAISAL" . "A"); // A is for View
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE APPRAISAL" . "V"); // V is for Authorized
        $Script = "Select GpVal, GpName, GHashRt from [UGpRights] where HashKey='" . md5($EditID . "CREATE APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';}

        //'.ScriptRunner($Script,'GpVal').'-'.ScriptRunner($Script,'GHashRt').'-'.ScriptRunner($Script,'GpName');}
        else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ADD/EDIT DELIVERABLES
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MANAGE TEAM APPRAISAL
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL MGR" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL MGR" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL MGR" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL MGR" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Appraise</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">REPLICATE
                                                            DELIVERABLES</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MEDIATION" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MEDIATION" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">All</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MEDIATION" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MEDIATION" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Team</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">START &amp; END
                                                            APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ADD CALIBRATED SCORE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CALIBRATED SCORE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CALIBRATED SCORE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CALIBRATED SCORE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CALIBRATED SCORE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CALIBRATED SCORE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CALIBRATED SCORE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ADD CALIBRATED SCORE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD CALIBRATED SCORE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">APPRAISAL SETTINGS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL SETTING" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL SETTING" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL SETTING" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL SETTING" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">KPI TEMPLATE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KPI TEMPLATE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KPI TEMPLATE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KPI TEMPLATE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KPI TEMPLATE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KPI TEMPLATE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KPI TEMPLATE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KPI TEMPLATE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KPI TEMPLATE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">APPRAISAL REPORTS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL REPORTS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL REPORTS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <label for="" style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <label for="" style="margin-bottom: 5px">Export</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">EMPLOYEE APPRAISAL
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMP APPRAISAL" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMP APPRAISAL" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Begin
                                                                Appraisal</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMP APPRAISAL" . "M");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMP APPRAISAL" . "M") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Manage
                                                                My Deliverables</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMP APPRAISAL" . "S");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMP APPRAISAL" . "S") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">My
                                                                Submitted Appraisal</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">All
                                                                Submitted Appraisal</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW APPRAISAL" . "N");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW APPRAISAL" . "N") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Next
                                                                Level Appraiser</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Team
                                                                Submitted Appraisal</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MODULE MASTERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL MODULE MASTERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL MODULE MASTERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update/Delete</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">KRA / KPI TAG</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KRA" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KRA" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Define
                                                                Key Result Areas</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("KPI" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "KPI" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Define
                                                                KPI Tags</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">TEAM DELIVERABLES
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">TRACK APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRACK APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRACK APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                </div>
                                                <div class="box">
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>


                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">CONFIRMATION APPRAISAL MODULE</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox6" class="test" value="" />
                                            <label for="DelBoox6"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">CREATE EMPLOYEE
                                                            APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE CONFIRM APPRAISAL" . "A"); // A is for View
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE CONFIRM APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE CONFIRM APPRAISAL" . "V"); // V is for Authorized
        $Script = "Select GpVal, GpName, GHashRt from [UGpRights] where HashKey='" . md5($EditID . "CREATE CONFIRM APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';}

        //'.ScriptRunner($Script,'GpVal').'-'.ScriptRunner($Script,'GHashRt').'-'.ScriptRunner($Script,'GpName');}
        else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE CONFIRM APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE CONFIRM APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CREATE CONFIRM APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE CONFIRM APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ADD/EDIT DELIVERABLES
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MANAGE TEAM APPRAISAL
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL MGR" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL MGR" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL MGR" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL MGR" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Appraise</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">REPLICATE
                                                            DELIVERABLES</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MEDIATION CONFIRM" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MEDIATION CONFIRM" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">All</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">START &amp; END
                                                            APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END CONFIRM APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END CONFIRM APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END CONFIRM APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END CONFIRM APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END CONFIRM APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END CONFIRM APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("START END CONFIRM APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "START END CONFIRM APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">APPRAISAL SETTINGS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM SETTING" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM SETTING" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPRAISAL CONFIRM SETTING" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPRAISAL CONFIRM SETTING" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">EMPLOYEE APPRAISAL
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMP CONFIRM APPRAISAL" . "B");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMP CONFIRM APPRAISAL" . "B") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Begin
                                                                Appraisal</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMP CONFIRM APPRAISAL" . "M");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMP CONFIRM APPRAISAL" . "M") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Manage
                                                                My Deliverables</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VIEW APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW CONFIRM APPRAISAL" . "N");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW CONFIRM APPRAISAL" . "N") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Next
                                                                Level Appraiser</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VIEW CONFIRM APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VIEW CONFIRM APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Team
                                                                Submitted Appraisal</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">TEAM DELIVERABLES
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TEAM CONFIRM APPRAISAL" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TEAM CONFIRM APPRAISAL" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">TRACK APPRAISAL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TRACK CONFIRM APPRAISAL" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TRACK CONFIRM APPRAISAL" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                </div>
                                                <div class="box">
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>

                                <?php
}
$Script = "Select * from CLicense where CModule='CCT' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                <?php
}
//***************************ACCOUNTING*******************************************
$Script = "Select * from CLicense where CModule='ACCT' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                <?php
}
//***************************ACCOUNTING*******************************************
$Script = "Select * from CLicense where CModule='PAY' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>

                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">PAYROLL</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox7" class="test" value="" />
                                            <label for="DelBoox7"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAYROLL GROUP</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL GROUP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL GROUP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL GROUP" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL GROUP" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL GROUP" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL GROUP" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL GROUP" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL GROUP" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAY MONTHLY SALARY
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAY SALARY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAY SALARY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAY SALARY" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAY SALARY" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Generate</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAY SALARY" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAY SALARY" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ADD/EDIT LOAN</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE LOAN" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE LOAN" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE LOAN" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE LOAN" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE LOAN" . "R");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE LOAN" . "R") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Reschedule</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("EMPLOYEE LOAN" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "EMPLOYEE LOAN" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Terminate</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">WAGE ITEM SETTING
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL SETTING" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL SETTING" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL SETTING" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL SETTING" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">LOAN SCHEME</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LOAN SCHEME" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LOAN SCHEME" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LOAN SCHEME" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LOAN SCHEME" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LOAN SCHEME" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LOAN SCHEME" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LOAN SCHEME" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LOAN SCHEME" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAYROLL REPORTS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL REPORTS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL REPORTS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MODULE MASTERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL MODULE MASTERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL MODULE MASTERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ATTENDANCE UPLOAD
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ATTENDANCE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ATTENDANCE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ATTENDANCE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ATTENDANCE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Upload</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ATTENDANCE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ATTENDANCE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("ATTENDANCE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ATTENDANCE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center"> PERSONAL LOAN</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("MY LOAN REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "MY LOAN REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">My
                                                                Loan Request</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">OVERTIME SETTING</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("OVERTIME SETTING" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "OVERTIME SETTING" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("OVERTIME SETTING" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "OVERTIME SETTING" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("OVERTIME SETTING" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "OVERTIME SETTING" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("OVERTIME SETTING" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "OVERTIME SETTING" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- </div> -->
                                                <?php }
$Script = "Select * from CLicense where CModule='BI' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                                <!-- <tr>
						<td align="left"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						</table></td>
					</tr> -->
                                                <?php }
$Script = "Select * from CLicense where CModule='HMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                                <?php
}
$Script = "Select * from CLicense where CModule='ROM' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                                <?php
}
$Script = "Select * from CLicense where CModule='LMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {
    ?>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">DEDUCTION MASTERS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DEDUCTION MASTERS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DEDUCTION MASTERS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DEDUCTION MASTERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DEDUCTION MASTERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DEDUCTION MASTERS" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DEDUCTION MASTERS" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DEDUCTION MASTERS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DEDUCTION MASTERS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAYROLL SETTINGS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYROLL SETTINGS VIEW" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYROLL SETTINGS VIEW" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAYMENT TEMPLATE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYMENT TEMPLATE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYMENT TEMPLATE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYMENT TEMPLATE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYMENT TEMPLATE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYMENT TEMPLATE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYMENT TEMPLATE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php

    if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAYMENT TEMPLATE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAYMENT TEMPLATE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}

    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">RESEND PAYSLIP</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PAYSLIP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PAYSLIP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PAYSLIP" . "P");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PAYSLIP" . "P") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PAYSLIP" . "S");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PAYSLIP" . "S") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Send</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">BATCH PAY</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BATCH PAY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BATCH PAY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("BATCH PAY" . "C");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "BATCH PAY" . "C") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Create</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">RESEND PERSONAL PAYSLIP</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PERSONAL PAYSLIP" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PERSONAL PAYSLIP" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PERSONAL PAYSLIP" . "P");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PERSONAL PAYSLIP" . "P") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("RESEND PERSONAL PAYSLIP" . "S");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "RESEND PERSONAL PAYSLIP" . "S") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Send</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">PAID LEAVE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAID LEAVE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAID LEAVE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAID LEAVE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAID LEAVE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("PAID LEAVE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "PAID LEAVE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">UN/AUTHORIZE LOANS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE LOANS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE LOANS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE LOANS" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE LOANS" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE LOANS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE LOANS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Un/Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">AUTHORIZE SALARY</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE SALARY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE SALARY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE SALARY" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE SALARY" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("AUTHORIZE SALARY" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "AUTHORIZE SALARY" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">REVIEW SALARY</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("REVIEW SALARY" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REVIEW SALARY" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Edit</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("REVIEW SALARY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REVIEW SALARY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="box box-slided-up">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">LEAVE MANAGEMENT</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox8" class="test" value="" />
                                            <label for="DelBoox8"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">


                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">NEW LEAVE REQUEST
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TIMEOFF REQUEST" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TIMEOFF REQUEST" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TIMEOFF REQUEST" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TIMEOFF REQUEST" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TIMEOFF REQUEST" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TIMEOFF REQUEST" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Cancel
                                                                (This should make Cancel Leave Page to be
                                                                visible)</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">EDIT LEAVE SETTINGS
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET LEAVE POLICY" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET LEAVE POLICY" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET LEAVE POLICY" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET LEAVE POLICY" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET LEAVE POLICY" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET LEAVE POLICY" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">SET HOLIDAYS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET HOLIDAYS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET HOLIDAYS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET HOLIDAYS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET HOLIDAYS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET HOLIDAYS" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET HOLIDAYS" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("SET HOLIDAYS" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "SET HOLIDAYS" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">APPROVE ALL LEAVE
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE ALL LEAVE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE ALL LEAVE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE ALL LEAVE" . "P");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE ALL LEAVE" . "P") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE ALL LEAVE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE ALL LEAVE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize/Reject/Save
                                                                Comment</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">CALENDAR</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CALENDAR" . "VT");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CALENDAR" . "VT") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Team Calendar</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CALENDAR" . "VG");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CALENDAR" . "VG") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Branch Calendar</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CALENDAR" . "VO");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CALENDAR" . "VO") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View
                                                                Organisational Calendar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">MODULE MASTERS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("LEAVE MODULE MASTERS" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE MODULE MASTERS" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">TIMEOFF REPORTS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("TIMEOFF REPORTS" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "TIMEOFF REPORTS" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">VERIFY LEAVE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VERIFY TIMEOFF" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VERIFY TIMEOFF" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VERIFY TIMEOFF" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VERIFY TIMEOFF" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Cancel</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("VERIFY TIMEOFF" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "VERIFY TIMEOFF" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Verify</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">APPROVE TEAM LEAVE
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE TEAM LEAVE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE TEAM LEAVE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE TEAM LEAVE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE TEAM LEAVE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Print</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("APPROVE TEAM LEAVE" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPROVE TEAM LEAVE" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize/Reject</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <td  align="left" valign="top"><table width="170" border="0" align="left" cellpadding="1" cellspacing="1" id="non-gradient-style">
						<tr>
							<td colspan="2" align="center" class="tdMenu_HeadBlock_Light" style="height: 24px"></td>
						</tr>
						<tr>
							<td width="17" align="right">&nbsp;</td>
							<td width="95" class="TinyText">&nbsp;</td>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td class="TinyText">&nbsp;</td>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td class="TinyText">&nbsp;</td>
						</tr>
						<tr>
							<td align="right" style="height: 23px"></td>
							<td class="TinyText" style="height: 23px"></td>
						</tr>
					</table></td>
					</tr>
					</table></td>
					</tr> -->
                                                <?php
}?>
                                                <!--  </table></td>
      				</tr> -->
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">LEAVE GROUP</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD LEAVE GROUP" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD LEAVE GROUP" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD LEAVE GROUP" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD LEAVE GROUP" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD LEAVE GROUP" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD LEAVE GROUP" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ADD LEAVE GROUP" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ADD LEAVE GROUP" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">LEAVE COMPONENTS</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("LEAVE GROUP COMPONENT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE GROUP COMPONENT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("LEAVE GROUP COMPONENT" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE GROUP COMPONENT" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("LEAVE GROUP COMPONENT" . "D");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE GROUP COMPONENT" . "D") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("LEAVE GROUP COMPONENT" . "T");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE GROUP COMPONENT" . "T") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">LEAVE TYPES</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("LEAVE TYPES" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "LEAVE TYPES" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">View /
                                                                Add/Update</span> / Delete</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">REQUEST FOR ALL</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REQUEST FOR ALL" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FOR ALL" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REQUEST FOR ALL" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FOR ALL" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Apply</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">REQUEST FOR TEAM</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REQUEST FOR TEAM" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FOR TEAM" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REQUEST FOR TEAM" . "A");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REQUEST FOR TEAM" . "A") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Apply</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box"></div>
                                                <div class="box"></div>
                                            </div>

                                        </diV>
                                    </diV>
                                </diV>







                                <!-- RECRUITMENT SETTING BEGINS -->
                                <div class="box box-slided-up ">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">RECRUITMENT MANAGEMENT</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox9" class="test" value="" />
                                            <label for="DelBoox9"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">JOB POST</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("CREATE JOB" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE JOB" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Create
                                                                Job Post</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("APPLICANT LIST" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "APPLICANT LIST" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Applicant List</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">JOB APPLICATION FORM
                                                        </h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("CREATE APPLICATION TEMPLATE" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE APPLICATION TEMPLATE" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Create
                                                                Job Application Template</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REARRANGE APPLICATION TEMPLATE" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REARRANGE APPLICATION TEMPLATE" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Rearrange Job
                                                                Template</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">ASSESSMENT</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("ASSESSMENT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "ASSESSMENT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Assessment</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">INTERVIEW</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("CREATE INTERVIEW" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CREATE INTERVIEW" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Create
                                                                Interview Template</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("REARRANGE INTERVIEW" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "REARRANGE INTERVIEW" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Re-arrange Interview
                                                                Template</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("INTERVIEW" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "INTERVIEW" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Interview</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("INTERVIEW DATA" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "INTERVIEW DATA" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Interview Data</label>
                                                        </div>

                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("INVITE INTERVIEW" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "INVITE INTERVIEW" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
?>
                                                            <label for="<?=$ChkName?>" style="margin-bottom: 5px">Invite
                                                                Interview</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="box"></div>
      				<div class="box"></div> -->
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <!-- END OF RECRUITMENT SETTINGS  -->

                                <?php
$Script = "Select * from CLicense where CModule='DMS' and CID='" . $_SESSION["StkTck" . "CustID"] . "'";
if (ScriptRunner_dflt($Script, "ModStatus") == 1) {?>
                                <div class="box box-slided-up ">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">DOCUMENT MANAGEMENT</h4>
                                        <ul class="box-controls pull-right">

                                            <li><a class="box-btn-slides" href="#"><i class="fa fa-arrow-down"></i></a>
                                            </li>

                                        </ul>
                                        <div class="box-controls pull-right">
                                            <input type="checkbox" id="DelBoox10" class="test" value="" />
                                            <label for="DelBoox10"></label>
                                        </div>
                                    </div>
                                    <div class="box-content">

                                        <div class="box-body">
                                            <div class="box-group bg-lightest">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">DOCUMENT</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
    $ChkName = md5("DOCUMENT" . "V");
    $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DOCUMENT" . "V") . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DOCUMENT" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DOCUMENT" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DOCUMENT" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DOCUMENT" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("DOCUMENT" . "T");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "DOCUMENT" . "T") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Authorize/Unauthorize</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h6 class="box-title b-0 px-0 text-center">CATALOGUE</h6>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CATALOGUE" . "V");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CATALOGUE" . "V") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">View</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CATALOGUE" . "A");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CATALOGUE" . "A") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Add/Update</label>
                                                        </div>
                                                        <div class="demo-checkbox">
                                                            <?php
if ($EditID != "" && $EditID != "--") {
        $ChkName = md5("CATALOGUE" . "D");
        $Script = "Select GpVal from [UGpRights] where HashKey='" . md5($EditID . "CATALOGUE" . "D") . "' and GPType='C' and Status<>'D'";
        if (ScriptRunner($Script, 'GpVal') == 1) {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" checked="checked"/>';} else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    } else {echo '<input name="' . $ChkName . '" id="' . $ChkName . '" type="checkbox" />';}
    ?>
                                                            <label for="<?=$ChkName?>"
                                                                style="margin-bottom: 5px">Delete</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
}
?>














                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6"> </div>
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-3 pull-right">
                                                <?php
if ( /*$_REQUEST["PgTy"] == "AddIndTrans" &&*/($EditID == "" || $EditID == '--')) {
    //$but_Caption="Apply Now"; $but_HasRth=("GROUPS"."A"); $but_Type='CustomNoSelect'; include '../main/buttons.php';
    //echo '<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Create Group" />';
} elseif (($EditID != "" && $EditID != "--")) {
    if ($if_authorized_for_view == "0") {
        if (ValidateURths("GROUPS" . "A") == true) {
            $but_Caption = "Update Group";
            $but_HasRth = ("GROUPS" . "A");
            $but_Type = 'CustomNoSelect';include '../main/buttons.php';
            //echo '<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Update Group" />';
            echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
        }
    }
} else { //echo '<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Create Group" />';
}
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input name="SubmitTrans" id="SubmitTrans" type="submit"
                                            class="btn btn-sm btn-danger" value="AUTHORIZE" />
                                        <input name="SubmitTrans" id="SubmitTrans" type="submit"
                                            class="btn btn-sm btn-danger" value="UNAUTHORIZE" />
                                    </div>
                                </div>
                                <hr style="margin-top: 1.0rem;margin-bottom: .5rem">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <?php
if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    print "<h4>UNAUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(varchar(8),GpLogOutT) GTO, * from UGpRights where Status <> 'A' and Status <> 'D' and GpType='P' order by GpName asc");
} else {
    print "<h4>AUTHORIZED</h4>";
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(varchar(8),GpLogOutT) GTO, * from UGpRights where Status = 'A' and GpType='P' order by GpName asc, AuthDate desc");
}
//-------------------
?>
                                    </div>
                                    <div class="col-md-12">
                                        <table width="100%" border="1" align="left" cellpadding="1" cellspacing="1"
                                            id="table">
                                            <thead>
                                                <tr>
                                                    <th valign="middle" class="smallText" scope="col"
                                                        data-sorter="false"><span class="TinyTextTight">
                                                            <input type="checkbox" id="DelChk_All"
                                                                onClick="ChkDel();" />
                                                            <label for="DelChk_All"></label>
                                                        </span></th>
                                                    <th width="141" valign="middle" scope="col">Created On</th>
                                                    <th width="447" align="center" valign="middle" scope="col">Group
                                                        Name</th>
                                                    <th width="129" valign="middle" align="center">No. Of Users</th>
                                                    <th width="247" align="center" valign="middle" scope="col">Group
                                                        Leave</th>
                                                    <th width="157" align="center" valign="middle" scope="col">Group
                                                        Time Out</th>
                                                    <th width="110" align="center" valign="middle" scope="col">P.Expiry
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$Del = 0;
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    ?>
                                                <tr>
                                                    <td width="20" height="27" valign="middle" scope="col"
                                                        class="TinyTextTight"><input
                                                            name="<?php echo ("DelBox" . $Del); ?>" type="checkbox"
                                                            id="<?php echo ("DelBox" . $Del); ?>"
                                                            value="<?php echo ($row2['HashKey']); ?>" />
                                                        <label for="<?php echo ("DelBox" . $Del); ?>"></label>
                                                        <?php
if (($row2['HashKey']) == $EditID) {$bgColor = 'bgcolor="#cccccc"';} else { $bgColor = 'bgcolor="#ffffff"';}
    ?></td>
                                                    <td width="141" valign="middle" class="TinyText" scope="col">
                                                        &nbsp;<?php echo (trim($row2['Dt'])); ?></td>
                                                    <td align="center" valign="middle" class="TinyText">
                                                        &nbsp;<?php echo (trim($row2['GpName'])); ?></td>
                                                    <td valign="middle" align="center" class="TinyText" scope="col">
                                                        &nbsp;
                                                        <?php
$Script = "select Count(*) as Ct from Users where GpName='" . $row2['HashKey'] . "' and Status ='A'";
    echo ScriptRunner($Script, "Ct");?></td>
                                                    <td align="center" valign="middle" class="TinyText" scope="col">
                                                        &nbsp;
                                                        <?php
$Script = "select LID from LvSettings where HashKey='" . trim($row2['GpLeave']) . "' and Status<>'D'";
    echo ScriptRunner($Script, "LID");?></td>
                                                    <td width="157" align="center" valign="middle" class="TinyText"
                                                        scope="col">&nbsp;<?php echo (trim($row2['GTO'])) . " hrs"; ?>
                                                    </td>
                                                    <td width="110" align="center" valign="middle" class="TinyText"
                                                        scope="col"><?php echo (trim($row2['GpPwRst'])) . " days"; ?>
                                                    </td>
                                                </tr>
                                                <?php }
include '../login/dbClose2.php';?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 pt-5">
                                        <?php include '../main/pagination.php';?>
                                    </div>
                                    <div class="col-md-6 pt-5">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-9">
                                                <?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
									  <input name="FAction" id="FAction" type="hidden">
								  	  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

//Check if any record was spolled at all before enabling buttons

if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    if (ValidateURths("GROUPS RT" . "D") == true) {
        //Check if user has Delete rights
        $but_HasRth = ("GROUPS RT" . "D");
        $but_Type = 'Delete';include '../main/buttons.php';
    }

    if (ValidateURths("GROUPS" . "V") == true) {
        //Check if user has Add/Edit rights
        $but_HasRth = ("GROUPS" . "V");
        $but_Type = 'View';include '../main/buttons.php';
    }

    if (ValidateURths("GROUPS RT" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("GROUPS RT" . "T");
        $but_Type = 'Authorize';include '../main/buttons.php';
    }
} else {

    //Check if user has Delete rights
    //$but_HasRth=("GROUPS".""); $but_Type = 'Delete'; include '../main/buttons.php';

    if (ValidateURths("GROUPS" . "V") == true) {
        //Check if user has Add/Edit rights
        $but_HasRth = ("GROUPS" . "V");
        $but_Type = 'View';include '../main/buttons.php';
    }

    if (ValidateURths("GROUPS RT" . "T") == true) {
        //Check if user has Authorization rights
        $but_HasRth = ("GROUPS RT" . "T");
        $but_Type = 'Unauthorize';include '../main/buttons.php';
    }
}
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    </form>

    <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>


    <!-- FastClick -->
    <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

    <!-- MinimalLite Admin App -->
    <script src="../assets/js/template.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <script>
        $(document).on('click', '.box-btn-slides', function (e) {
            e.stopPropagation();
            $(this).toggleClass('rotate-180')
                .parent().parent().parent().parent().find('.box-content').slideToggle();
            //   console.log( $(this).toggleClass('rotate-180')
            //   .parent().parent().parent().parent());
            //   ;
        });
        $(document).on('click', '.with-border', function (e) {
            e.stopPropagation();
            if (e.target.tagName === 'LABEL') return;
            if (e.target.tagName === 'INPUT') return;
            $(this).find('.box-btn-slides').toggleClass('rotate-180');
            $(this).parent().find('.box-content').slideToggle();
        });
        $(document).on('change', '.test', function (e) {
            e.stopPropagation();
            const status = $(this).prop("checked");
            const all = $(this).parent().parent().parent().find('input[type="checkbox"]');
            const newAry = Array.from(all);
            const filt = newAry.filter(eac => {
                return !eac.classList.contains('test');
            });
            if (status) {
                filt.forEach(input => input.checked = true);
            } else {
                filt.forEach(input => input.checked = false);
            }
        });
        $(document).on('change', '.box-slided-up input[type="checkbox"]', function (e) {
            e.stopPropagation();
            if (e.target.classList.contains('test')) return;
            $(this).parent().parent().parent().parent().parent().parent().parent().find('input[class="test"]')
                .prop("checked", false);

        });

    </script>
</body>

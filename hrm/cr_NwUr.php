<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;
include '../css/myscripts.php';

if (ValidateURths("USERS" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
	$EditID = "";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($Del)) {
	$Del = 0;
}
if (!isset($ULock)) {
	$ULock = "";
}
if (!isset($SelID)) {
	$SelID = "";
}



if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unlock Account Selected") {
		if (ValidateURths("USERS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$EditID = ECh($_REQUEST["UpdID"]);
		/*=====================================================================================================*/
		/*=================================== UPDATE EMPLOYEE USER ACCOUNT ====================================*/
		/*=====================================================================================================*/
		$Script = "Update Users set ULock=0, LogCnt=0 where HashKey='" . $EditID . "'";
		ScriptRunnerUD($Script, "LeaveUpd");

		$Script = "Select Ur.LogName, Et.Email from Users Ur, EmpTbl Et where Et.EmpID=Ur.EmpID and Ur.HashKey='" . $EditID . "'";

		$Script_mail = "Select * from Settings where Setting='ULockNotfEm'";
		$send_email = ScriptRunner($Script_mail, "SetValue");

		//Leave Audit Trail
		AuditLog("INSERT", "User account unlocked for [" . ScriptRunner($Script, "LogName") . "]");

		//if ($_REQUEST["AuthNotifier"] == "on")
		//{
		//MailTrail("USER","UNLOCKED",'','',ScriptRunner($Script,"Email"),''); //User Account Unlocked -- d35e802dde7342814e8bbd8d5afdde8b
		MailTrail("USER", "UNLOCKED", "", "", $send_email, ScriptRunner($Script, "LogName"));
		//}
		$EditID = "zzz";
		echo ("<script type='text/javascript'>{ parent.msgbox('Employee account unlocked successfully.', 'green'); }</script>");
	}

	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create User") {
		if (ValidateURths("USERS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		include 'add_user_validation.php';

		$Script = "Select * from Users where LogName='" . ECh(trim($_REQUEST["LogName"])) . "' and Status<>'D'";

		if (strlen(ScriptRunner($Script, "LogName")) > 0) {
			echo ("<script type='text/javascript'>{parent.msgbox('A user record with a duplicate name already exist. Please review.', 'red');}</script>");
			$GoValidate = false;
		}

		$Script = "select EmpID from Users where EmpID='" . ECh(trim($_REQUEST["EmpID"])) . "' and Status='A'";
		if (strlen(ScriptRunner($Script, "EmpID")) > 0) {
			echo ("<script type='text/javascript'>{parent.msgbox('The selected employee has been tied to a user account already. Please review.', 'red');}</script>");
			$GoValidate = false;
		}

		/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
		$Script = "Select HashKey from EmpTbl where EmpID='" . ECh($_REQUEST["EmpID"]) . "' and EmpStatus='Active'";
		$HashKey = ScriptRunner($Script, "HashKey");
		if ($HashKey == "") {
			echo ("<script type='text/javascript'>{ parent.msgbox('Invalid active employee selected. Please review.', 'red'); }</script>");
			$GoValidate = false;
		}

		if ($GoValidate == true) {
			if (isset($_REQUEST["RSet"]) && $_REQUEST["RSet"] == "on") {
				$RSet = 1;
			} else {
				$RSet = 0;
			}

			$Script = "Select HashKey from [UGpRights] where GpName='" . ECh($_REQUEST["GpName"]) . "'";
			$Script = "INSERT INTO [Users] ([LogName],[GpName],[AcctState],[PassDate],[RPwd],[ULock],[LogCnt],[LStatus],[EmpID],[Status],[AddedBy],[AddedDate],[HashKey],[CustID]) VALUES ('" . ECh($_REQUEST["LogName"]) . "','" . ScriptRunner($Script, "HashKey") . "','" . $AcctState . "','" . $RsPwd . "'," . $RSet . ",0,0,'" . $LStatus . "','" . $_REQUEST["EmpID"] . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . $_SESSION["StkTck" . "CustID"] . "')";
			//		echo $Script;
			ScriptRunnerUD($Script, "Inst");

			/*=====================================================================================================*/
			/*=================================== UPDATE EMPLOYEE LEAVE GROUP =====================================*/
			/*=====================================================================================================*/
			$Script = "Update EmpTbl set EmpLeaveGp=(Select GpLeave from [UGpRights] where GpName='" . ECh($_REQUEST["GpName"]) . "') where EmpID='" . ECh($_REQUEST["EmpID"]) . "'";
			ScriptRunnerUD($Script, "LeaveUpd");
			/*=====================================================================================================*/
			/*=====================================================================================================*/
			/*=====================================================================================================*/

			//Set Users New Password for Login
			//Render all old passwords invalid
			$Script = "Update UsersiP set Status='H' where EmpID='" . $HashKey . "'";
			ScriptRunnerUD($Script, "History");

			//Set New Password
			$Script = "Insert into UsersiP ([EmpID],[EmpPw],[Status]) VALUES
			('" . $HashKey . "','" . $NewPassword . "','L')";
			ScriptRunnerUD($Script, "");

			//User Audit Trail
			MailTrail("USERS", "T", '', '', '', ''); //Sends a mail notifications
			AuditLog("INSERT", "New user account created for [" . ECh($_REQUEST["LogName"]) . "]");

			//**********************************************************************************
			$Script_Edit = "Select (SName+' '+FName) Nm, Email from EmpTbl where EmpID='" . ECh(trim($_REQUEST["EmpID"])) . "'";
			//Send mail of the user login details

			$Script = "Select MMsg, MSub from MailTemp where HashKey='b0b4f9e5cb933606109105f3d3bf1f97'"; //MSub='User Account Login'";
			$LoginDet = ScriptRunner($Script, "MMsg");
			$LoginDet = str_replace('#CompanyID#', $_SESSION["StkTck" . "Cust"], $LoginDet);
			$LoginDet = str_replace('#LogName#', $_REQUEST["LogName"], $LoginDet);
			$LoginDet = str_replace('#Password#', $_REQUEST["LogPassword"], $LoginDet);
			$LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);

			/* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */

			$To = ScriptRunner($Script_Edit, "Email");
			$Bdy = $LoginDet;
			$Subj = ScriptRunner($Script, "MSub");
			$Atth = "";

			QueueMail($To, $Subj, $Bdy, $Atth, '');
			//**************************************************************

			echo ("<script type='text/javascript'>{ parent.msgbox('New user account created successfully.', 'green'); }</script>");
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************

	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update User" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("USERS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set HashKey of Account to be Updated */
		if (isset($_REQUEST["UpdID"])) {
			$EditID = ECh($_REQUEST["UpdID"]);
			$HashKey = ECh($_REQUEST["UpdID"]);
		}

		/*Check validation for updating an account */
		include 'add_user_validation.php';

		if ($GoValidate == true) {
			if (isset($_REQUEST["RSet"]) && $_REQUEST["RSet"] == "on") {
				$RSet = 1;
			} else {
				$RSet = 0;
			}

			if (ValidateUpdDel("SELECT Status from Users WHERE [HashKey] = '" . $HashKey . "'") == true) {
				$Script = "Select HashKey from [UGpRights] where GpName='" . ECh($_REQUEST["GpName"]) . "'";
				$Script = "Update [Users] set [GpName]='" . ScriptRunner($Script, "HashKey") . "',[AcctState]='" . $AcctState . "',EmpID='" . ECh($_REQUEST["EmpID"]) . "'" . $RsPwd . ",[RPwd]='" . $RSet . "',[LStatus]='" . $LStatus . "',[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $HashKey . "'";
				ScriptRunnerUD($Script, "Inst");

				/*=====================================================================================================*/
				/*=================================== UPDATE EMPLOYEE LEAVE GROUP =====================================*/
				/*=====================================================================================================*/
				$Script = "Update EmpTbl set EmpLeaveGp=(Select GpLeave from [UGpRights] where GpName='" . ECh($_REQUEST["GpName"]) . "') where EmpID='" . ECh($_REQUEST["EmpID"]) . "'";
				ScriptRunnerUD($Script, "LeaveUpd");
				/*=====================================================================================================*/
				/*=====================================================================================================*/
				/*=====================================================================================================*/

				if (strlen($_REQUEST["LogPassword"]) != 32) {
					//Set Users New Password for Login
					//Render all old passwords invalid
					$Script = "Update UsersiP set Status='H' where EmpID='" . $HashKey . "'";
					ScriptRunnerUD($Script, "History");

					//Set New Password
					$Script = "Insert into UsersiP ([EmpID],[EmpPw],[Status]) VALUES
				('" . $HashKey . "','" . $NewPassword . "','L')";
					ScriptRunnerUD($Script, "");

					$Script_Edit = "Select (SName+' '+FName) Nm, Email from EmpTbl where EmpID='" . ECh(trim($_REQUEST["EmpID"])) . "'";
					//Send mail of the user login details

					$Script = "Select MMsg, MSub from MailTemp where HashKey='b0b4f9e5cb933606GTHYD5f3d3bf1f97'"; //MSub='User Account Login'";
					$LoginDet = ScriptRunner($Script, "MMsg");
					$LoginDet = str_replace('#CompanyID#', $_SESSION["StkTck" . "Cust"], $LoginDet);
					$LoginDet = str_replace('#LogName#', $_REQUEST["LogName"], $LoginDet);
					$LoginDet = str_replace('#Password#', $_REQUEST["LogPassword"], $LoginDet);
					$LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);

					/* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */

					$To = ScriptRunner($Script_Edit, "Email");
					$Bdy = $LoginDet;
					$Subj = ScriptRunner($Script, "MSub");
					$Atth = "";

					QueueMail($To, $Subj, $Bdy, $Atth, '');
					//**************************************************************
				}
				MailTrail("USERS", "T", '', '', '', ''); //Sends a mail notifications
				AuditLog("INSERT", "User account updated for [" . ECh($_REQUEST["LogName"]) . "]");
				echo ("<script type='text/javascript'>{ parent.msgbox('User account updated successfully.', 'green'); }</script>");
			}
		}
		$Script_Edit = "select * from [Users] where HashKey='" . $EditID . "'";
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
		if (isset($_REQUEST["UserName"]) && strlen($_REQUEST["UserName"]) > 2) {
			$EditID = ECh($_REQUEST["UserName"]);
			$Script_Edit = "select * from [Users] where HashKey='" . $EditID . "'";
		} else {
			$EditID = '--';
			$Script_Edit = "select * from [Users] where HashKey='--'";
		}
	} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("USERS" . "V") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_Edit = "select * from [Users] where HashKey='" . $EditID . "'";
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("USERS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT LogName from Users where HashKey='" . $EditID . "'";

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from Users WHERE [HashKey] = '" . $EditID . "'") == true) {
					//Mark the record as Authorized
					$Script = "UPDATE Users
					SET [Status] = 'A'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("AUTHORIZE", "User account authorized for account " . ScriptRunner($Script_ID, "LogName"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected user account(s) authorized successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("USERS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT LogName from Users where HashKey='" . $EditID . "'";

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from Users WHERE [HashKey] = '" . $EditID . "'") != true) {
					//Mark the record as Authorized
					$Script = "UPDATE Users
					SET [Status] = 'U'
					,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Authorize");
					AuditLog("UNAUTHORIZE", "User account unauthorized for account " . ScriptRunner($Script_ID, "LogName"));

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected user account(s) unauthorized successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("USERS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		// var_dump("here");

		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				$Script_ID = "SELECT LogName from Users where HashKey='" . $EditID . "' order by AddedBy";


				// $q = "SELECT Status from Users WHERE [HashKey] = '" . $EditID . "' order by AddedBy";
				// var_dump($q, $Script_ID);

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from Users WHERE [HashKey] = '" . $EditID . "' order by AddedBy ") == true) {
					//Mark the record as Deleted
					$Script = "UPDATE Users
					SET [Status] = 'D'
                    ,[EmpID] =  NULL
					,[DeletedBy] =  '" . $_SESSION["StkTck" . "UName"] . "'
					,[DeletedDate] = GetDate()
					WHERE [HashKey] = '" . $EditID . "'";
					ScriptRunnerUD($Script, "Delete");
					AuditLog("DELETE", "User account deleted for user [" . ScriptRunner($Script_ID, "LogName") . "]");

					echo ("<script type='text/javascript'>{ parent.msgbox('Selected user account(s) deleted successfully.', 'green'); }</script>");
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID = '--';
		$Script_Edit = "select * from [Users] where HashKey='--'";
	}
}
?>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="../css/style_main.css" rel="stylesheet" type="text/css">
	<script>
		$(function() {
			$(document).tooltip();
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
</head>

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-4">
					</div>
					<div class="col-md-4  text-center">
						<h4 style="margin-top: 1%;">
							Create/Edit User Priviledges
						</h4>
					</div>
					<div class="col-md-4">
						<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger pull-right" id="SubmitTrans" value="Clear" style="margin-top: 1%;" />
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Login ID</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								if ($EditID != "" && $EditID != "--") {
									echo '<input type="text" name="LogName" size="50" maxlength="60" class="form-control" id="LogName" value="' . ScriptRunner($Script_Edit, "LogName") . '" />';
								} elseif (isset($_REQUEST["LogName"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<input type="text" name="LogName" size="50" maxlength="60" class="form-control" id="LogName" value="' . $_REQUEST["LogName"] . '" />';
								} else {
									echo '<input type="text" name="LogName" size="50" maxlength="60" class="form-control" id="LogName" value="" />';
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Password
								<span style="color: red">*</span>:
							</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								if ($EditID != "" || $EditID > 0) {
									$Script = "Select * from UsersiP where EmpID='" . ScriptRunner($Script_Edit, "HashKey") . "' and Status='L'";
									//echo $Script;
									echo '<input type="password" name="LogPassword" size="25" maxlength="30" class="form-control" id="LogPassword" value="' . ScriptRunner($Script, "EmpPw") . '" />';
								} elseif (isset($_REQUEST["LogPassword"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
									echo '<input type="password" name="LogPassword" size="25" maxlength="30" class="form-control" id="LogPassword" value="' . $_REQUEST["LogPassword"] . '" />';
								} else {
									echo '<input type="password" name="LogPassword" size="25" maxlength="30" class="form-control" id="LogPassword" value="" />';
								}
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Password Reset
								:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="PassExpire" class="form-control" id="PassExpire" disabled="disabled">
									<option selected="selected" value="0">Inherit</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">User Group:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="GpName" class="form-control" id="GpName">
									<option value="--" selected="selected">--</option>
									<?php
									if (isset($_REQUEST["GpName"])) {
										$dbOpen2 = ("select Distinct(GpName) as GpName from UGpRights where GpType='P' and Status ='A' and HashKey<>'" . ECh($_REQUEST["GpName"]) . "' ORDER BY GpName");
									} else {
										$dbOpen2 = ("select Distinct(GpName) as GpName from UGpRights where GpType='P' and Status ='A' ORDER BY GpName");
									}
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										echo '<option value="' . $row2['GpName'] . '">' . $row2['GpName'] . '</option>';
									}
									include '../login/dbClose2.php';

									if ($EditID == "--") {
									} elseif ($EditID != "" && $EditID != "--") {
										$Script = "Select GpName from [UGpRights] where HashKey='" . ScriptRunner($Script_Edit, "GpName") . "'";
										echo '<option selected=selected value="' . ScriptRunner($Script, "GpName") . '">' . ScriptRunner($Script, "GpName") . '</option>';
									} elseif (isset($_REQUEST["GpName"])) {
										echo '<option selected=selected value="' . $_REQUEST["GpName"] . '">' . $_REQUEST["GpName"] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Account State :
							</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="AcctState" class="form-control" id="AcctState">
									<option selected="selected" value="--">--</option>
									<?php
									if ($EditID != "") {
										if (ScriptRunner($Script_Edit, "AcctState") == 1) {
											echo '<option selected=selected value="Activated">Activated</option>';
											echo '<option value="Deactivated">Deactivated</option>';
										} else {
											echo '<option value="Activated">Activated</option>';
											echo '<option value="Deactivated">Deactivated</option>';
										}
									} else {
										echo '<option value="Activated">Activated</option>';
										echo '<option value="Deactivated">Deactivated</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="demo-checkbox">
							<label>Reset On Login:</label>
							<input type="checkbox" name="RSet" id="RSet" />
							<label for="RSet">
								<button type="button" class="btn btn-default btn-xs " style="line-height: 17px" id="MastersDepartment" title="Check for user to reset password on next login">
									<i class="fa fa-info"></i>
								</button>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<h4 class="page-header">Personal Details</h4>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Staff ID :
							</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="EmpID" class="form-control select2  " id="EmpID">
									<option value="--" selected="selected">--</option>
									<?php
									if ($EditID != "" && $EditID != "--") {
										$SelID = ScriptRunner($Script_Edit, "EmpID");
									} else {
										$SelID = "";
									}


									$dbOpen2 = ("SELECT (SName+' '+FName+' '+ONames +' ['+Convert(Varchar(24),EmpID)+']') as Nm, EmpID FROM EmpTbl where EmpStatus='Active' AND Status <>'D'  ORDER BY EmpID"); //where status ='A'

									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//---------------------------------------------
										if ((isset($SelID)) && ($SelID == $row2['EmpID'])) {
											$Script = ("SELECT LogName FROM Users where ([LStatus] <>'D' OR [LStatus] <>'A') AND EmpID = '" . $SelID . "'");
											echo '<option value="' . $row2['EmpID'] . '" selected>' . $row2['Nm'] . '</option>';
										} else {
											$Script = ("SELECT LogName FROM Users where ([LStatus] <>'D' OR [LStatus] <>'A') AND EmpID = '" . $row2['EmpID'] . "'");

											$val = ScriptRunner($Script, "LogName");
											if (!isset($val)) {
												echo '<option value="' . $row2['EmpID'] . '">' . $row2['Nm'] . '</option>';
											}
										}

										//---------------------------------------------

									}
									include '../login/dbClose2.php';
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Enable Login :
							</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select name="LStatus" class="form-control" id="LStatus">
									<option selected="selected" value="--">--</option>
									<?php
									if ($EditID != "") {
										if (ScriptRunner($Script_Edit, "LStatus") == 'A') {
											echo '<option selected=selected value="Active">Active</option>';
											echo '<option value="Inactive">Inactive</option>';
										} else {
											echo '<option value="Active">Active</option>';
											echo '<option value="Inactive">Inactive</option>';
										}
									} else {
										echo '<option value="Active">Active</option>';
										echo '<option value="Inactive">Inactive</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6"> </div>
							<div class="col-md-2">
							</div>
							<div class="col-md-4 pull-right">
								<?php
								/* CHeck If User Account Is Lock From Logging In */
								if ($EditID != "" && $EditID != "--") {
									$ULock = ScriptRunner($Script_Edit, "ULock");
									if ($ULock != 0) {
										//echo '<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Unlock Account" />';
										$but_Caption = "Unlock Account";
										$but_HasRth = ("USERS" . "A");
										$but_Type = 'CustomNoSelect';
										include '../main/buttons.php';
									}
								}
								?>
								<?php
								if (ValidateURths("USERS" . "A") == true) {
									if (/*$_REQUEST["PgTy"] == "AddIndTrans" &&*/($EditID == "" || $EditID == '--')) {
										echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create User" />';
									} elseif ($EditID != "" || $EditID > 0) {
										echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Update User" />';
										echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
									} else {
										echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create User" />';
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="UNAUTHORIZE" />
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						/*
		    	    		if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized")
		    	    		{
		    	    		$dbOpen2 = ("SELECT Convert(Varchar(11),Ur.AddedDate,106) as Dt,Ur.*, Em.mobile Mob, Em.email Eml, (Em.SName+' '+Em.FName+' '+Em.ONames) as Nm from Users Ur, EmpTbl Em where Ur.Status = 'A' and (Ur.LogName like '%".$SearchVal."%' or Em.SName like '%".$SearchVal."%' or Em.FName like '%".$SearchVal."%'or Em.ONames like '%".$SearchVal."%' or Em.Email like '%".$SearchVal."%') and Ur.EmpID=Em.EmpID order by Ur.LogName asc");
		    	    		}
		    	    		elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized")
		    	    		{
		    	    		$dbOpen2 = ("SELECT Convert(Varchar(11),Ur.AddedDate,106) as Dt,Ur.*, Em.mobile Mob, Em.email Eml, (Em.SName+' '+Em.FName+' '+Em.ONames) as Nm from Users Ur, EmpTbl Em where (Ur.Status not in ('A','D')) and (Ur.LogName like '%".$SearchVal."%' or Em.SName like '%".$SearchVal."%' or Em.FName like '%".$SearchVal."%'or Em.ONames like '%".$SearchVal."%' or Em.Email like '%".$SearchVal."%') and Ur.EmpID=Em.EmpID order by Ur.LogName asc");
		    	    		}
		    	    		else
		    	    		{
		    	    		$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from Users where Status = 'Z' order by LogName asc");
		    	    		}
		    	    		*/
						//-------------------
						if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),Ur.AddedDate,106) as Dt,Ur.*, Em.mobile Mob, Em.email Eml, (Em.SName+' '+Em.FName+' '+Em.ONames) as Nm from Users Ur, EmpTbl Em where (Ur.Status not in ('A','D')) AND Em.Status <> 'D' and Ur.EmpID=Em.EmpID order by Ur.LogName asc");
						} else {
							print "<h4>AUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),Ur.AddedDate,106) as Dt,Ur.*, Em.mobile Mob, Em.email Eml, (Em.SName+' '+Em.FName+' '+Em.ONames) as Nm from Users Ur, EmpTbl Em where Ur.Status = 'A'  AND Em.Status <> 'D' and Ur.EmpID=Em.EmpID order by Ur.LogName asc");
						}
						//-------------------
						?>
					</div>
					<div class="col-md-12">
						<table width="100%" id="table" class="tablesorter table-responsive table">
							<thead>
								<tr>
									<th valign="middle" class="smallText" scope="col" data-sorter="false">
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
										<label for="DelChk_All"></label>
									</th>
									<th valign="middle" scope="col"><span class="TinyTextTightBold">Created On</span></th>
									<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Login ID</span></th>
									<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Employee Name</span></th>
									<th width="259" align="left" valign="middle" scope="col">Email</th>
									<th width="128" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Mobile</span></th>
									<th width="187" align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Group</span></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$Del = 0;
								include '../login/dbOpen2.php';
								while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
									$Del = $Del + 1;
								?>
									<tr>
										<td width="20" height="23" valign="middle" scope="col" class="TinyTextTight">
											<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
											<label for="<?php echo ("DelBox" . $Del); ?>"></label>
										</td>
										<?php $Script = "Select * from [EmpTbl] where EmpID='" . $row2['EmpID'] . "'"; // Gets staff full details from the Emp Table
										?>
										<td width="137" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
										<td width="255" align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['LogName'])); ?></td>
										<td width="275" align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['Nm'])); ?></td>
										<td valign="middle" align="left" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Eml'])); ?></td>
										<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Mob'])); ?></td>
										<td width="187" align="left" valign="middle" scope="col" class="TinyText">&nbsp;<?php
																														if (strlen($row2['GpName']) == 32) {
																															$Script = "Select GpName from [UGpRights] where HashKey='" . $row2['GpName'] . "'";
																															echo ScriptRunner($Script, "GpName");
																														} ?></td>
									</tr>
								<?php }
								//include '../login/dbClose2.php';
								?>
							</tbody>
						</table>
					</div>
					<div class="col-md-6 pt-5">
						<?php include '../main/pagination.php'; ?>
					</div>
					<div class="col-md-6 pt-5">
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-9">
								<?php
								echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
								      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
									  <input name="FAction" id="FAction" type="hidden" />
									  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';

								//Check if any record was spolled at all before enabling buttons

								if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
									if (ValidateURths("USERS" . "D") == true) {
										//Check if user has Delete rights
										$but_HasRth = ("USERS" . "D");
										$but_Type = 'Delete';
										include '../main/buttons.php';
									}

									if (ValidateURths("USERS" . "V") == true) {
										//Check if user has Add/Edit rights
										$but_HasRth = ("USERS" . "V");
										$but_Type = 'View';
										include '../main/buttons.php';
									}

									if (ValidateURths("USERS" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("USERS" . "T");
										$but_Type = 'Authorize';
										include '../main/buttons.php';
									}
								} else {
									/*
											//Check if user has Delete rights
											$but_HasRth=("USERS".""); $but_Type = 'Delete'; include '../main/buttons.php';
											//Check if user has Add/Edit rights
											$but_HasRth=("USERS".""); $but_Type = 'View'; include '../main/buttons.php';
											*/

									if (ValidateURths("USERS" . "T") == true) {
										//Check if user has Authorization rights
										$but_HasRth = ("USERS" . "T");
										$but_Type = 'Unauthorize';
										include '../main/buttons.php';
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>


	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>

	<script>
		$('.select2').select2();
	</script>
</body>
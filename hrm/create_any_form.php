<?php

$html = '
<h1><a name="top"></a>mPDF</h1>
<h2 style="color:red">Basic HTML Example</h2>
This file demonstrates most of the HTML elements.
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>
<p>P: Nulla felis erat, imperdiet eu, ullamcorper non, nonummy quis, elit. Suspendisse potenti. Ut a eros at ligula vehicula pretium. Maecenas feugiat pede vel risus. Nulla et lectus. Fusce eleifend neque sit amet erat. Integer consectetuer nulla non orci. Morbi feugiat pulvinar dolor. Cras odio. Donec mattis, nisi id euismod auctor, neque metus pellentesque risus, at eleifend lacus sapien et risus. Phasellus metus. Phasellus feugiat, lectus ac aliquam molestie, leo lacus tincidunt turpis, vel aliquam quam odio et sapien. Mauris ante pede, auctor ac, suscipit quis, malesuada sed, nulla. Integer sit amet odio sit amet lectus luctus euismod. Donec et nulla. Sed quis orci. </p>

<hr />
';


//==============================================================
//==============================================================
//==============================================================

// require_once('../mpdf/vendor/autoload.php');
// $custId = "";
// $path = "../uploads_files/ok_test";
// $name = md5(uniqid(rand(), true)) . ".pdf";
// $mpdf = new mPDF();

// $mpdf->SetWatermarkText('Approved'); // Will cope with UTF-8 encoded text
// $mpdf->watermarkTextAlpha = 0.1;
// $mpdf->showWatermarkText = true;
// // $mpdf->SetWatermarkText(new WatermarkText('DRAFT'));

// // $mpdf->SetWatermarkText('Approved');
// // $mpdf->Watermark('orheh');


// $mpdf->WriteHTML($html);
// // if (!file_exists($path)) {
// //   mkdir($path, 0777, true);
// // }
// // $mpdf->Output("$path/$name", "F");
// // $mpdf->Output("$path/$name");
// $mpdf->Output('', "I");
// // $mpdf->OutputHttpDownload('download.pdf');


session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

$template = tem();







//Validate user viewing rights
if (ValidateURths("CREATE FORM" . "V") != true) {
  include '../main/NoAccess.php';
  exit;
}

$dbOpen4 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc";

?>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
  } else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
  <script>
    $(function() {
          <?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
          } else { ?>
            $("#ExDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "-1Y",
              maxDate: "+1D",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })
            $("#EmpDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "-80Y",
              maxDate: "+1M",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

          <?php } ?>
          $("#DOB").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-80Y",
            maxDate: "<?php
                      $kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
                      if (number_format($kk, 0, '', '') == 0 || $kk == "") {
                        echo "-18Y";
                      } else {
                        echo "-" . $kk . "Y";
                      } ?>
            ", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
          });
  </script>
  <script>
    $(function() {
      $("#tabs").tabs();
    });
    $(function() {
      $(document).tooltip();
    });
    $(function() {
      $("#ClearDate").click(function() {
        document.getElementById("ExDt").value = '';
      });
    });
  </script>
  <script>
    $(function() {
      $("#COO").change(function() {
        var Pt = $("#COO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
      });
      $("#SOO").change(function() {
        var Pt = $("#SOO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
      });
    });
  </script>
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

  <!-- Select 2-->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/master_style.css">
  <link rel="stylesheet" href="../assets/css/responsive.css">


  <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
  <!-- <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script> -->
  <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">

  <style>
    .font-weight-bolder {
      font-weight: 600;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
      background-color: #6dab69;
    }

    [type=checkbox]+label {
      padding-left: 20px;
    }

    hr {
      border: 0;
      clear: both;
      display: block;
      width: 96%;
      background-color: #dee7ee;
      height: 1px;
      margin-top: 10px;
    }
  </style>


</head>


<body>






  <!-- Main content -->
  <section class="content" style="min-height: 1000px">

    <div class="box">
      <div class="box-header with-border">
        <div class="row">

          <div class="col-md-6  text-md-left text-center">
            <h3 id="header">
              Create Form Template
            </h3>

          </div>
          <div class="col-md-6">
            <div class="row">

              <div class="col-4"></div>
              <!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
              <!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->




              <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                <select class="form-control " name="eachGroup" id="eachGroup">
                  <option value="">--</option>
                  <?php
                  $query = "SELECT * from [formtemplate] where  id='1'";

                  $getcheck = ScriptRunnercous($query);

                  $groupsarry = json_decode($getcheck['template_name'], true);

                  ?>

                  <?php foreach ($groupsarry as $eachgroup) : ?>


                    <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>


                  <?php endforeach; ?>



                </select>
                <span class="input-group-btn">
                  <button type="button" class="btn btn-danger" onclick="getGroupArry()">Open</button>
                </span>
              </div>




            </div>



          </div>
        </div>
      </div>
      <div class="box-body">



        <div class="row">


          <div class="col-8 mx-auto">
            <form name="groupForm" id="groupForm" method="POST">




              <div class="form-group row ">
                <label class="col-sm-4 col-form-label">Template Name<span style="color: red">*</span>:</label>
                <div class="col-sm-8 input-group-sm">
                  <input class="form-control" type="text" name="group" id="groupName" value="">
                </div>
              </div>






              <!-- <section  style="postion:relative" > -->

              <div id="selemp">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Short Text Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forShortanswer">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>

                    </div>
                    <div class="box-body" id="dynanic_field">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="shortanswer[]" id="getshort">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>


                      <div class="checkbox">
                        <input type="checkbox" checked name="shortanswerequired[]" id="shortanswerequired_0">
                        <label for="shortanswerequired_0">Make field compulsory</label>
                      </div>


                    </div>


                  </div>
                </div>


              </div>
              <div id="selemp_com">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Comment Text Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forShortanswer_com">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>

                    </div>
                    <div class="box-body" id="dynanic_field_com">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="shortanswer_com[]" id="getshort">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add_com"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>


                      <div class="checkbox">
                        <input type="checkbox" checked name="shortanswerequired_com[]" id="shortanswerequired_com_0">
                        <label for="shortanswerequired_com_0">Make field compulsory</label>
                      </div>


                    </div>


                  </div>
                </div>


              </div>


              <div id="selemp7">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Url Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forUrl">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>

                    </div>
                    <div class="box-body" id="dynanic_field7">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="url[]" id="geturl">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add7"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>


                      <div class="checkbox">
                        <input type="checkbox" checked name="urlrequired[]" id="urlrequired_0">
                        <label for="urlrequired_0">Make field compulsory</label>
                      </div>


                    </div>


                  </div>
                </div>


              </div>



              <div id="selemp1">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class=" box-title text-dark">Long Text Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forParagraph">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field1">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="paragraph[]" id="getpara">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add1"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>


                      <div class="checkbox">
                        <input type="checkbox" checked name="paragraphrequired[]" id="paragraphrequired_0">
                        <label for="paragraphrequired_0">Make field compulsory</label>
                      </div>
                    </div>

                  </div>
                </div>


              </div>


              <div id="selemp2">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Dropdown Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forDropdown">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field2">
                      <div>
                        <div class="form-group row " style="background-color:#bce0f8">
                          <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="dropdown0[]">

                            <span class="input-group-btn ">
                              <button type="button" class="btn btn-default " id="add2"><i title="Add More Question"></i>Add More</button>
                            </span>
                          </div>
                        </div>

                        <div class="checkbox">
                          <input type="checkbox" checked name="dropdownrequired[]" id="dropdownrequired_0">
                          <label for="dropdownrequired_0">Make field compulsory</label>
                        </div>



                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="dropdown0[]">



                          </div>
                        </div>
                        <div id="dyn_field0">
                          <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                            <div class="col-sm-8 input-group input-group-sm">
                              <input class="form-control" type="text" value="" name="dropdown0[]">

                              <span class="input-group-btn ">
                                <button type="button" class=" add3 btn btn-default" onclick="letstry()"><i></i>Add More</button>
                              </span>

                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>


              </div>






              <div id="selemp5">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Checkbox Field</h5>


                      <button type="button" class="close" aria-label="Close" id="forCheckbox">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field5">
                      <div>
                        <div class="form-group row " style="background-color:#bce0f8">
                          <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="checkbox0[]">

                            <span class="input-group-btn ">
                              <button type="button" class="btn btn-default " id="add25"><i title="Add More Question"></i>Add More</button>
                            </span>
                          </div>
                        </div>

                        <div class="checkbox">
                          <input type="checkbox" checked name="checkboxrequired[]" id="checkboxrequired_0">
                          <label for="checkboxrequired_0">Make field compulsory</label>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="checkbox0[]">



                          </div>
                        </div>
                        <div id="dyn_fied10">
                          <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                            <div class="col-sm-8 input-group input-group-sm">
                              <input class="form-control" type="text" value="" name="checkbox0[]">

                              <span class="input-group-btn ">
                                <button type="button" class=" add3 btn btn-default" onclick="letstry1()"><i></i>Add More</button>
                              </span>

                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>


              </div>





              <div id="selemp8">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class="box-title text-dark">Radio Field</h5>


                      <button type="button" class="close" aria-label="Close" id="forRadio">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field8">
                      <div>
                        <div class="form-group row " style="background-color:#bce0f8">
                          <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="radio0[]">

                            <span class="input-group-btn ">
                              <button type="button" class="btn btn-default " id="add8"><i title="Add More Question"></i>Add More</button>
                            </span>
                          </div>
                        </div>

                        <div class="checkbox">
                          <input type="checkbox" checked name="radiorequired[]" id="radiorequired_0">
                          <label for="radiorequired_0">Make field compulsory</label>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                          <div class="col-sm-8 input-group input-group-sm">
                            <input class="form-control" type="text" value="" name="radio0[]">



                          </div>
                        </div>
                        <div id="dyn_fied10ra">
                          <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">option<span style="color: red">*</span></label>

                            <div class="col-sm-8 input-group input-group-sm">
                              <input class="form-control" type="text" value="" name="radio0[]">

                              <span class="input-group-btn ">
                                <button type="button" class=" btn btn-default" onclick="letstry1ra()"><i></i>Add More</button>
                              </span>

                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>


              </div>













              <div id="selemp3">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class=" box-title text-dark">Numeric Field</h5>

                      <button type="button" class="close" aria-label="Close" id="forNumeric">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field3">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="numeric[]" id="getnumeric">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add3"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>


                      <div class="checkbox">
                        <input type="checkbox" checked name="numericrequired[]" id="numericrequired_0">
                        <label for="numericrequired_0">Make field compulsory</label>
                      </div>
                    </div>

                  </div>
                </div>


              </div>

              <div id="selemp4">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class=" box-title text-dark">Date Field</h5>
                      <button type="button" class="close" aria-label="Close" id="forDate">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field4">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="date[]" id="getdate">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add4"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>

                      <div class="checkbox">
                        <input type="checkbox" checked name="daterequired[]" id="daterequired_0">
                        <label for="daterequired_0">Make field compulsory</label>
                      </div>
                    </div>

                  </div>
                </div>


              </div>
              <div id="selemp9">

                <div class="form-group row " style="display: none;">
                  <div class="box box-solid bg-pale-secondary">
                    <div class="box-header text-center">
                      <h5 class=" box-title text-dark">File Field</h5>
                      <button type="button" class="close" aria-label="Close" id="forFile">

                        <span aria-hidden="true" style="color: red">&times;</span>

                      </button>
                    </div>
                    <div class="box-body" id="dynanic_field9">
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label>
                        <div class="col-sm-8 input-group input-group-sm">
                          <input class="form-control" type="text" value="" name="file[]" id="getfile">

                          <span class="input-group-btn ">
                            <button type="button" class="btn btn-default " id="add9"><i title="Add More Question"></i>Add More</button>
                          </span>
                        </div>
                      </div>

                      <div class="checkbox">
                        <input type="checkbox" checked name="filerequired[]" id="filerequired_0">
                        <label for="filerequired_0">Make field compulsory</label>
                      </div>
                    </div>

                  </div>
                </div>


              </div>




              <!-- </section> -->

              <div class="row justify-content-center">
                <h6 class="text-center">Add Authorization Levels</h6>

              </div>
              <div class="form-group row ">
                <label class="col-sm-4 col-form-label">Level 1<span style="color: red">*</span>:</label>
                <div class="col-sm-8 input-group-sm">
                  <select name="level1" id="level1" class="form-control">
                    <option value="--">--</option>
                    <option value="first_sup"> 1st Level Supervisor</option>
                    <option value="second_sup"> 2nd Level Supervisor</option>
                    <option value="third_sup"> 3rd Level Supervisor</option>
                    <option value="branch_cont"> Branch Contact</option>
                    <option value="branch_cont2"> Branch Contact 2</option>
                    <option value="branch_cont3"> Branch Contact 3</option>
                    <option value="branch_cont4"> Branch Contact 4</option>
                    <?php
                    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                    if ($conn2 === false) {
                      echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                    }
                    $dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

                    $result2 = sqlsrv_query($conn2, $dbOpen4);
                    if ($result2 === false) {
                      die(print_r(sqlsrv_errors(), true));
                    }
                    while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                      <option value="<?php echo $row3['HashKey']; ?>">
                        <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
                      </option>
                    <?php endwhile;
                    sqlsrv_free_stmt($result2);

                    ?>


                  </select>
                </div>
              </div>
              <div class="form-group row ">
                <label class="col-sm-4 col-form-label">Level 2:</label>
                <div class="col-sm-8 input-group-sm">
                  <select name="level2" id="level2" class="form-control">
                    <option value="--">--</option>
                    <option value="first_sup"> 1st Level Supervisor</option>
                    <option value="second_sup"> 2nd Level Supervisor</option>
                    <option value="third_sup"> 3rd Level Supervisor</option>
                    <option value="branch_cont"> Branch Contact</option>
                    <option value="branch_cont2"> Branch Contact 2</option>
                    <option value="branch_cont3"> Branch Contact 3</option>
                    <option value="branch_cont4"> Branch Contact 4</option>
                    <?php
                    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                    if ($conn2 === false) {
                      echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                    }
                    $dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

                    $result2 = sqlsrv_query($conn2, $dbOpen4);
                    if ($result2 === false) {
                      die(print_r(sqlsrv_errors(), true));
                    }
                    while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                      <option value="<?php echo $row3['HashKey']; ?>">
                        <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
                      </option>
                    <?php endwhile;
                    sqlsrv_free_stmt($result2);
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group row ">
                <label class="col-sm-4 col-form-label">Level 3:</label>
                <div class="col-sm-8 input-group-sm">
                  <select name="level3" id="level3" class="form-control">
                    <option value="--">--</option>
                    <option value="first_sup"> 1st Level Supervisor</option>
                    <option value="second_sup"> 2nd Level Supervisor</option>
                    <option value="third_sup"> 3rd Level Supervisor</option>
                    <option value="branch_cont"> Branch Contact</option>
                    <option value="branch_cont2"> Branch Contact 2</option>
                    <option value="branch_cont3"> Branch Contact 3</option>
                    <option value="branch_cont4"> Branch Contact 4</option>


                    <?php
                    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                    if ($conn2 === false) {
                      echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                    }
                    $dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

                    $result2 = sqlsrv_query($conn2, $dbOpen4);
                    if ($result2 === false) {
                      die(print_r(sqlsrv_errors(), true));
                    }
                    while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                      <option value="<?php echo $row3['HashKey']; ?>">
                        <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
                      </option>
                    <?php endwhile;
                    sqlsrv_free_stmt($result2);

                    ?>

                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-6">Select request correspondence :</label>
                <select name="select-LD[]" class="form-control select2 col-sm-12" multiple="multiple" id="select_ld">
                  <option value="first_sup"> 1st Level Supervisor</option>
                  <option value="second_sup"> 2nd Level Supervisor</option>
                  <option value="third_sup"> 3rd Level Supervisor</option>
                  <option value="branch_cont"> Branch Contact</option>
                  <option value="branch_cont2"> Branch Contact 2</option>
                  <option value="branch_cont3"> Branch Contact 3</option>
                  <option value="branch_cont4"> Branch Contact 4</option>
                  <?php
                  include '../login/dbOpen4.php';

                  while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>

                    <option value="<?php echo $row3['HashKey']; ?>">
                      <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?></option>
                  <?php endwhile;
                  include '../login/dbClose4.php';
                  ?>
                </select>
              </div>



              <div class="form-group row bg-light pt-2 ">
                <label class="col-sm-10 col-form-label">Select from predefined templates:</label>
                <div class="checkbox col-sm-2  mt-5">
                  <input type="checkbox" name="defined_temp" id="defined_temp" class="filled-in chk-col-navy">
                  <label for="defined_temp"></label>
                </div>
              </div>
              <div id="predefined_tem">
                <div class="row form-group" style="display: none;">
                  <label class="col-sm-4 col-form-label">Predefined templates:</label>
                  <div class="col-sm-8 input-group-sm">
                    <select name="frm_template" id="frm_template" class="form-control">
                      <option value="--">--</option>
                      <?php foreach ($template as $key => $val) : ?>
                        <option value="<?php echo $key; ?>"><?php echo strtoupper($key); ?></option>
                      <?php endforeach; ?>

                    </select>

                  </div>

                </div>
              </div>
              <div id="template_fields">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Add Custom Field:</label>
                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="myshortanswer" id="shortanswer">
                      <label for="shortanswer">Short Text</label>
                    </div>
                  </div>


                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="myparagraph" id="paragraph">
                      <label for="paragraph">Long Text</label>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="mydropdown" id="dropdown">
                      <label for="dropdown">Dropdown</label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="numeric" id="numeric">
                      <label for="numeric">Numeric</label>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="date" id="date">
                      <label for="date">Date</label>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="checkbox" id="checkbox">
                      <label for="checkbox">Check Box</label>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="url" id="url">
                      <label for="url">Url</label>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="radio" id="radio">
                      <label for="radio">Radio</label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="myshortanswer_com" id="shortanswer_com">
                      <label for="shortanswer_com">Comment</label>
                    </div>
                  </div>



                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label"></label>

                  <div class="col-sm-3">
                    <div class="checkbox">
                      <input type="checkbox" name="file" id="file">
                      <label for="file">File</label>
                    </div>
                  </div>

                </div>
              </div>






              <div class="row  d-flex justify-content-center">



                <button type=submit name="add" id="sdfer" class="btn btn-danger btn-sm mr-2 "> Submit </button>



                <button id="editsdfer" class="btn btn-danger btn-sm mr-2 " style="display: none">Update</button>
                <button id="saveassdfer" class="btn btn-danger btn-sm mr-2 " style="display: none">Save As</button>
                <button id="deletesdfer" class="btn btn-danger btn-sm mr-2 " style="display: none">Delete </button>




                <button class="btn btn-default btn-sm mr-2 " onclick="location.reload()">Cancel </button>
              </div>
              <div class="row text-center" style="margin-top:10px">

                <div class="col-md-12 mt-2">
                  <a href="rearrange_form_template.php" class="text-center">Click here to rearrange Form templates</a>
                </div>
              </div>

            </form>

          </div>








        </div>









      </div>




















    </div>
    </div>
    <!-- /.box-body -->




    </div>

















    <!-- /.row -->
  </section>
  <!-- /.content -->












  <!-- Bootstrap 4.0-->
  <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <script>
    $("#defined_temp").change(function() {
      if (this.checked) {
        $("#shortanswer").prop("checked", false);
        $("#selemp > div").fadeOut(10);

        $("#shortanswer_com").prop("checked", false);
        $("#selemp_com > div").fadeOut(10);

        $("#url").prop("checked", false);
        $("#selemp7 > div").fadeOut(10);

        $("#paragraph").prop("checked", false);
        $("#selemp1 > div").fadeOut(10);

        $("#dropdown").prop("checked", false);
        $("#selemp2 > div").fadeOut(10);

        $("#checkbox").prop("checked", false);
        $("#selemp5 > div").fadeOut(10);

        $("#radio").prop("checked", false);
        $("#selemp8 > div").fadeOut(10);

        $("#numeric").prop("checked", false);
        $("#selemp3 > div").fadeOut(10);

        $("#date").prop("checked", false);
        $("#selemp4 > div").fadeOut(10);


        $("#file").prop("checked", false);
        $("#selemp9 > div").fadeOut(10);






        $("#predefined_tem > div").fadeIn();
        $("#template_fields").fadeOut(10);


      } else {
        $("#predefined_tem > div").fadeOut(10);
        $("#template_fields").fadeIn();
        // alert('unchecked');
      }
    });



    var lw = 1;
    $('#aadd').click(function() {
      lw++;
      $('#ttable').append('<tr id="ro' + lw + '"><td><input type="text" name="fullnames[]"></td><td><input type="text" name="phonenos[]"  placeholder="08012345678" ></td><td><input type="text" name="emails[]"></td><td><span class="badge bg-red bt-rem" id="' + lw + '" style="cursor: pointer;">remove</span></td></tr>');

    });

    $(document).on('click', '.bt-rem', function() {
      var butt_id = $(this).attr("id");
      $('#ro' + butt_id).remove();
    });


    $("#basic").change(function() {
      if (this.checked) {
        $("#basicCon").fadeIn();
      } else {
        $("#basicCon").fadeOut(10);
      }
    });
  </script>


  <script type="text/javascript">
    $("#typeG").change(function() {
      if ($(this).val() === "assign_to_employee") {
        $("#selempty > div").fadeIn();
      } else {
        $("#selempty > div").fadeOut();
      };


    });
  </script>

  <script>
    var c = 1;
    var i = 1;
    var j = 1;
    var z = 1;
    var n = 1;
    var d = 1;
    var dd = 1;
    var u = 1;
    var r = 1;
    var f = 1;
    var ra = 1;
    var raf = 1;
    var eachgroup = null;
    var we = 0;
    var co = 0;




    $(document).ready(function() {
      $('#add').click(function() {
        if (i <= 100) {

          $('#dynanic_field').append('<div class="form-group row the' + i + '"> <hr/> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + i + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="shortanswerequired[]" id="shortanswerequired_' + i + '"><label for="shortanswerequired_' + i + '">Make field compulsory</label></div></div></div>');
          i++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove', function() {
        // alert(i);
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        i--;
        // alert(i);
      });

      $('#add_com').click(function() {
        if (co <= 100) {

          $('#dynanic_field_com').append('<div class="form-group row the' + co + '"> <hr/> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + co + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="shortanswerequired_com[]" id="shortanswerequired_com_' + co + '"><label for="shortanswerequired_com_' + co + '">Make field compulsory</label></div></div></div>');
          co++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove', function() {
        // alert(i);
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        co--;
        // alert(i);
      });












      $('#add7').click(function() {
        if (i <= 100) {

          $('#dynanic_field7').append('<div class="form-group row the' + u + '"> <hr/> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="url[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + u + '"class="btn btn-default btn_remove7 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="urlrequired[]" id="urlrequired_' + i + '"><label for="urlrequired_' + u + '">Make field compulsory</label></div></div></div>');
          u++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove7', function() {
        // alert(i);
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        u--;
        // alert(i);
      });

      $('#add1').click(function() {
        if (j <= 100) {

          $('#dynanic_field1').append('<div class="form-group row thep' + j + ' "><hr/><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="paragraph[]" ><span class="input-group-btn "><button type="button" name="remove" id="p' + j + '" class="btn btn-default btn_remove1 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="paragraphrequired[]" id="paragraphrequired_' + i + '"><label for="paragraphrequired_' + i + '">Make field compulsory</label></div> </div></div>');
          j++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove1', function() {
        var button_id = $(this).attr("id");
        // alert(button_id);
        $('.the' + button_id + '').remove();
        j--;
      });

      $('#add2').click(function() {
        if (z <= 100) {

          $('#dynanic_field2').append('<div class="thed' + c + '"><hr/><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remove2 " id="d' + c + '" style="color:red" ><i></i>Remove</button></span></div></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="dropdownrequired[]" id="dropdownrequired_' + c + '"><label for="dropdownrequired_' + c + '">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ></div></div><div id="dyn_field' + c + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction(' + c + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
          z++;
          c++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove2', function() {
        var button_id = $(this).attr("id");
        // alert(button_id);
        $('.the' + button_id + '').remove();
        z--;
      });




      $('#add25').click(function() {
        if (r <= 100) {

          $('#dynanic_field5').append('<div class="thec' + f + '"><hr/><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2 " id="c' + f + '" style="color:red" ><i></i>Remove</button></span></div></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="checkboxrequired[]" id="checkboxrequired_' + f + '"><label for="checkboxrequired_' + f + '">Make field compulsory</label></div> <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ></div></div><div id="dyn_fied1' + f + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction1(' + f + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
          r++;
          f++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });




      $(document).on('click', '.btn_remov2', function() {
        var button_id = $(this).attr("id");
        // alert(button_id);
        $('.the' + button_id + '').remove();
        r--;
      });


      $('#add8').click(function() {
        if (ra <= 100) {

          $('#dynanic_field8').append('<div class="thera' + raf + '"><hr/><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2ra " id="ra' + raf + '" style="color:red" ><i></i>Remove</button></span></div></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="radiorequired[]" id="radiorequired_' + raf + '"><label for="radiorequired_' + raf + '">Make field compulsory</label></div> <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ></div></div><div id="dyn_fied1ra' + raf + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction1ra(' + raf + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
          ra++;
          raf++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remov2ra', function() {
        // alert('dd');
        var button_id = $(this).attr("id");

        $('.the' + button_id + '').remove();
        ra--;
      });




      $('#add3').click(function() {
        if (n <= 100) {

          $('#dynanic_field3').append('<div class="form-group row then' + n + ' "><hr/><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="numeric[]" ><span class="input-group-btn "><button type="button" name="remove" id="n' + n + '" class="btn btn-default btn_remove3 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="numericrequired[]" id="numericrequired_' + n + '"><label for="numericrequired_' + n + '">Make field compulsory</label></div> </div></div>');
          n++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove3', function() {
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        n--;
      });


      $('#add4').click(function() {
        if (d <= 100) {

          $('#dynanic_field4').append('<div class="form-group row thed' + d + ' "><hr/><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="date[]" ><span class="input-group-btn "><button type="button" name="remove" id="d' + d + '" class="btn btn-default btn_remove4 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="daterequired[]" id="daterequired_' + d + '"><label for="daterequired_' + d + '">Make field compulsory</label></div> </div></div>');
          d++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove4', function() {
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        d--;
      });







      $('#add9').click(function() {
        if (d <= 100) {

          $('#dynanic_field9').append('<div class="form-group row thedf' + dd + ' "><hr/><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="file[]" ><span class="input-group-btn "><button type="button" name="remove" id="df' + dd + '" class="btn btn-default btn_remove9 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox"  checked name="filerequired[]" id="filerequired_' + dd + '"><label for="filerequired_' + dd + '">Make field compulsory</label></div> </div></div>');
          dd++;

        } else {
          {
            parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
          }
        }
      });

      $(document).on('click', '.btn_remove9', function() {
        var button_id = $(this).attr("id");
        $('.the' + button_id + '').remove();
        dd--;
      });



      $("#shortanswer").change(function() {
        if (this.checked) {
          $("#selemp > div").fadeIn();
          document.querySelector('#selemp').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp > div").fadeOut(10);
        }
      });


      $("#forShortanswer").click(function() {

        $("#shortanswer").prop("checked", false);

        $("#selemp > div").fadeOut(10);

      });


      $("#shortanswer_com").change(function() {
        if (this.checked) {
          $("#selemp_com > div").fadeIn();
          document.querySelector('#selemp_com').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp_com > div").fadeOut(10);
        }
      });


      $("#forShortanswer_com").click(function() {

        $("#shortanswer_com").prop("checked", false);

        $("#selemp_com > div").fadeOut(10);

      });





      $("#url").change(function() {
        if (this.checked) {
          $("#selemp7 > div").fadeIn();
          document.querySelector('#selemp7').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp7 > div").fadeOut(10);
        }
      });

      $("#forUrl").click(function() {

        $("#url").prop("checked", false);

        $("#selemp7 > div").fadeOut(10);

      });





      $("#paragraph").change(function() {
        if (this.checked) {
          $("#selemp1 > div").fadeIn();
          document.querySelector('#selemp1').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp1 > div").fadeOut(10);
        }
      });

      $("#forParagraph").click(function() {

        $("#paragraph").prop("checked", false);

        $("#selemp1 > div").fadeOut(10);

      });




      $("#dropdown").change(function() {
        if (this.checked) {
          $("#selemp2 > div").fadeIn();
          document.querySelector('#selemp2').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp2 > div").fadeOut(10);
        }
      });



      $("#forDropdown").click(function() {

        $("#dropdown").prop("checked", false);

        $("#selemp2 > div").fadeOut(10);

      });


      $("#checkbox").change(function() {
        if (this.checked) {
          $("#selemp5 > div").fadeIn();
          document.querySelector('#selemp5').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp5 > div").fadeOut(10);
        }
      });


      $("#forCheckbox").click(function() {

        $("#checkbox").prop("checked", false);

        $("#selemp5 > div").fadeOut(10);

      });


      $("#radio").change(function() {
        if (this.checked) {
          $("#selemp8 > div").fadeIn();
          document.querySelector('#selemp8').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp8 > div").fadeOut(10);
        }
      });


      $("#forRadio").click(function() {

        $("#radio").prop("checked", false);

        $("#selemp8 > div").fadeOut(10);

      });







      $("#numeric").change(function() {
        if (this.checked) {
          $("#selemp3 > div").fadeIn();
          document.querySelector('#selemp3').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp3 > div").fadeOut(10);
        }
      });


      $("#forNumeric").click(function() {

        $("#numeric").prop("checked", false);

        $("#selemp3 > div").fadeOut(10);

      });


      $("#date").change(function() {
        if (this.checked) {
          $("#selemp4 > div").fadeIn();
          document.querySelector('#selemp4').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp4 > div").fadeOut(10);
        }
      });


      $("#forDate").click(function() {

        $("#date").prop("checked", false);

        $("#selemp4 > div").fadeOut(10);

      });

      $("#file").change(function() {
        if (this.checked) {
          $("#selemp9 > div").fadeIn();
          document.querySelector('#selemp9').scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        } else {
          $("#selemp9 > div").fadeOut(10);
        }
      });


      $("#forFile").click(function() {

        $("#file").prop("checked", false);

        $("#selemp9 > div").fadeOut(10);

      });



    })
  </script>



  <!-- adding process -->

  <script type="text/javascript">
    $("#sdfer").click(function(event) {
      event.preventDefault();

      function uniqBy(a, key) {
        var seen = {};
        return a.filter(function(item) {
          var k = key(item);
          return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        })
      }


      function sanitarize(string) {
        const map = {
          '&': '&amp;',
          '<': '&lt;',
          '>': '&gt;',
          '"': '&quot;',
          "'": '&#x27;',
          "/": '&#x2F;',
        };
        const reg = /[&<>"'/]/ig;
        return string.replace(reg, (match) => (map[match]));
      }

      var groupName = $('#groupName').val();
      const correspondence = $('#select_ld').val() || [];



      const level1 = $('#level1').val();
      const level2 = $('#level2').val();
      const level3 = $('#level3').val();
      var grouptype = $('#typeG').val();
      let employee = $("#employee").val();

      if (groupName === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      } else {
        groupName = sanitarize(groupName);
      }
      // console.log(level1);
      // console.log(level2);
      // console.log(level3);
      // return;
      if (level1 === '--') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      }
      if (level3 !== '--' && level2 === "--") {

        {
          parent.msgbox('Kindly fill level 2 or remove level 3 authorization level', 'red');
        };
        return;
      }


      if (grouptype === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;

      }

      if ($('#defined_temp').is(":checked")) {
        // alert('here');
        if ($('#frm_template').val() === "--" || $('#frm_template').val() === " ") {

          {
            parent.msgbox('Kindly select a predefined template ', 'red');
          };
          return;

        }

      }




      if ($('#basic').is(":checked")) {

        // var allFullName=[];
        // var allPhoneNo=[];
        // var allEmail=[];

        // alert("ok1");

        if ($('input[name="fullnames[]"]').length) {

          var FullNames = $('input[name="fullnames[]"]').map(function() {
            return $(this).val();
          }).get();
          var PhoneNos = $('input[name="phonenos[]"]').map(function() {
            return $(this).val();
          }).get();
          var Emails = $('input[name="emails[]"]').map(function() {
            return $(this).val();
          }).get();

          for (let b = 0; b < FullNames.length; b++) {

            if (FullNames[b] === "" || PhoneNos[b] === "" || Emails[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            }

            if (/^\d{11}$/.test(PhoneNos[b]) === false) {

              {
                parent.msgbox('Enter a valid phone number ', 'red');
              };
              return;
            }

            if (/\S+@\S+\.\S+/.test(Emails[b]) === false) {

              {
                parent.msgbox('Enter a valid email address ', 'red');
              };
              return;
            }

          }

          var thirdParty = {
            "fullnames": FullNames,
            "phonenos": PhoneNos,
            "emails": Emails,
            "sendSMS": $('#sendSMS').is(":checked") ? true : null,
            "sendEmail": true
          }


          // console.log(thirdParty);
        }
      }



      if ($('#shortanswer').is(":checked")) {

        var allshortanswer = [];
        //get all shortanswer fields
        if ($('input[name="shortanswer[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer = $('input[name="shortanswer[]"]').map(function() {
            return $(this).val();
          }).get();

          // console.log(shortanswer);
          var shortanswer1 = document.getElementsByName('shortanswerequired[]');
          // console.log(shortanswer1);

          for (let b = 0; b < shortanswer.length; b++) {
            // console.log(shortanswer[b]);

            if (shortanswer[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(shortanswer[b]),
                required: (shortanswer1[b].checked) ? shortanswer1[b].value : "null"

              }
              allshortanswer.push(eachppy);


            }
          }

        }
      }


      if ($('#shortanswer_com').is(":checked")) {

        var allshortanswer_com = [];
        //get all shortanswer fields
        if ($('input[name="shortanswer_com[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer_com = $('input[name="shortanswer_com[]"]').map(function() {
            return $(this).val();
          }).get();

          // console.log(shortanswer);
          var shortanswer1_com = document.getElementsByName('shortanswerequired_com[]');
          // console.log(shortanswer1);

          for (let b = 0; b < shortanswer_com.length; b++) {
            // console.log(shortanswer[b]);

            if (shortanswer_com[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy_com = {

                fieldName: sanitarize(shortanswer_com[b]),
                required: (shortanswer1_com[b].checked) ? shortanswer1_com[b].value : "null"

              }
              allshortanswer_com.push(eachppy_com);


            }
          }

        }
      }









      if ($('#url').is(":checked")) {

        var allurl = [];
        //get all shortanswer fields
        if ($('input[name="url[]"]').length) {


          //get all shortanswerequired fields
          var url = $('input[name="url[]"]').map(function() {
            return $(this).val();
          }).get();

          // console.log(shortanswer);
          var url1 = document.getElementsByName('urlrequired[]');
          // console.log(shortanswer1);

          for (let b = 0; b < url.length; b++) {
            // console.log(shortanswer[b]);

            if (url[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(url[b]),
                required: (url1[b].checked) ? url1[b].value : "null"

              }
              allurl.push(eachppy);


            }
          }

        }
      }






      if ($('#numeric').is(":checked")) {

        var allnumeric = [];

        if ($('input[name="numeric[]"]').length) {
          var numeric = $('input[name="numeric[]"]').map(function() {
            return $(this).val();
          }).get();

          var numeric1 = document.getElementsByName('numericrequired[]');

          for (let b = 0; b < numeric.length; b++) {
            // console.log(shortanswer[b]);

            if (numeric[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {


              let eachnumppy = {

                fieldName: sanitarize(numeric[b]),
                required: (numeric1[b].checked) ? numeric1[b].value : "null"

              }
              allnumeric.push(eachnumppy);

            }
          }

        }

        // console.log(allnumeric);
        // return;
      }


      if ($('#date').is(":checked")) {

        var alldate = [];

        if ($('input[name="date[]"]').length) {
          var date = $('input[name="date[]"]').map(function() {
            return $(this).val();
          }).get();
          var date1 = document.getElementsByName('daterequired[]');

          for (let b = 0; b < date.length; b++) {


            if (date[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {

              let eachdateppy = {

                fieldName: sanitarize(date[b]),
                required: (date1[b].checked) ? date1[b].value : "null"

              }
              alldate.push(eachdateppy);
            }
          }

        }

      }


      if ($('#paragraph').is(":checked")) {
        var allparagraph = [];

        if ($('input[name="paragraph[]"]').length) {
          var paragraph = $('input[name="paragraph[]"]').map(function() {
            return $(this).val();
          }).get();
          var paragraph1 = document.getElementsByName('paragraphrequired[]');

          for (let b = 0; b < paragraph.length; b++) {
            // console.log(paragraph[b]);

            if (paragraph[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {

              let eachparappy = {

                fieldName: sanitarize(paragraph[b]),
                required: (paragraph1[b].checked) ? paragraph1[b].value : "null"

              }

              allparagraph.push(eachparappy);
            }
          }

        }
      }


      if ($('#dropdown').is(":checked")) {

        var alldropdown = [];
        var dropdownpper = document.getElementsByName('dropdownrequired[]');
        var refactorArry = [];

        for (var pp = c - 1; pp >= 0; pp--) {
          if ($('input[name="dropdown' + pp + '[]"]').length) {

            var dropdownpp = $('input[name="dropdown' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < dropdownpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (dropdownpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!alldropdown.includes(dropdownpp)) {
                  const dropd = dropdownpp.map(function(elem) {
                    return sanitarize(elem);
                  });


                  // console.log(dropd,dropdownpp1)

                  alldropdown.push(dropd);


                  alldropdown = uniqBy(alldropdown, JSON.stringify);

                }
              }
            }



          }
        }
        var rearrangedArry = alldropdown.reverse();

        for (let iz = 0; iz < rearrangedArry.length; iz++) {

          let eachdropdownppy = {

            fieldName: sanitarize(rearrangedArry[iz][0]),
            options: rearrangedArry[iz].slice(1),
            required: (dropdownpper[iz].checked) ? dropdownpper[iz].value : "null"

          }

          refactorArry.push(eachdropdownppy);

        }

        // console.log(refactorArry);
        // return;
      }



      if ($('#checkbox').is(":checked")) {



        var allcheckbox = [];
        var checkboxpper = document.getElementsByName('checkboxrequired[]');
        var refactorArrychkbox = [];


        for (let pp = f - 1; pp >= 0; pp--) {
          if ($('input[name="checkbox' + pp + '[]"]').length) {

            var checkboxpp = $('input[name="checkbox' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < checkboxpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (checkboxpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allcheckbox.includes(checkboxpp)) {
                  const checkd = checkboxpp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allcheckbox.push(checkd);

                  allcheckbox = uniqBy(allcheckbox, JSON.stringify);

                }
              }
            }



          }
        }



        var rearrangedArrychk = allcheckbox.reverse();

        for (let iz = 0; iz < rearrangedArrychk.length; iz++) {

          let eachcheckboxppy = {

            fieldName: sanitarize(rearrangedArrychk[iz][0]),
            options: rearrangedArrychk[iz].slice(1),
            required: (checkboxpper[iz].checked) ? checkboxpper[iz].value : "null"

          }

          refactorArrychkbox.push(eachcheckboxppy);


          // console.log(refactorArrychkbox);
          // return;
        }

      }







      if ($('#radio').is(":checked")) {



        var allradio = [];
        var radiopper = document.getElementsByName('radiorequired[]');
        var refactorArryradio = [];


        for (let pp = raf - 1; pp >= 0; pp--) {
          if ($('input[name="radio' + pp + '[]"]').length) {

            var radiopp = $('input[name="radio' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            // console.log(radiopp);

            for (let b = 0; b < radiopp.length; b++) {
              // console.log(dropdownpp[b]);

              if (radiopp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allradio.includes(radiopp)) {
                  const checkd = radiopp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allradio.push(checkd);

                  allradio = uniqBy(allradio, JSON.stringify);

                }
              }
            }



          }
        }



        var rearrangedArryradio = allradio.reverse();

        for (let iz = 0; iz < rearrangedArryradio.length; iz++) {

          let eachradioppy = {

            fieldName: sanitarize(rearrangedArryradio[iz][0]),
            options: rearrangedArryradio[iz].slice(1),
            required: (radiopper[iz].checked) ? radiopper[iz].value : "null"

          }

          refactorArryradio.push(eachradioppy);


          // console.log(refactorArryradio);
          // return;

        }














      }

      if ($('#file').is(":checked")) {

        var allfile = [];

        if ($('input[name="file[]"]').length) {
          var file = $('input[name="file[]"]').map(function() {
            return $(this).val();
          }).get();
          var file1 = document.getElementsByName('filerequired[]');

          for (let b = 0; b < file.length; b++) {


            if (file[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {

              let eachfileppy = {

                fieldName: sanitarize(file[b]),
                required: (file1[b].checked) ? file1[b].value : "null"

              }
              allfile.push(eachfileppy);
            }
          }

        }

      }

      var arry = [];
      var groupobj = {
        id: Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36),
        templateName: groupName,
        shortanswer: $('#shortanswer').is(":checked") ? allshortanswer : null,
        shortanswer_com: $('#shortanswer_com').is(":checked") ? allshortanswer_com : null,
        url: $('#url').is(":checked") ? allurl : null,
        paragraph: $('#paragraph').is(":checked") ? allparagraph : null,
        dropdown: $('#dropdown').is(":checked") ? refactorArry : null,
        checkbox: $('#checkbox').is(":checked") ? refactorArrychkbox : null,
        radio: $('#radio').is(":checked") ? refactorArryradio : null,
        numeric: $('#numeric').is(":checked") ? allnumeric : null,
        file: $('#file').is(":checked") ? allfile : null,
        def_temp_name: $('#defined_temp').is(":checked") ? $('#frm_template').val() : null,
        date: $('#date').is(":checked") ? alldate : null,
        level1: level1 === "--" ? null : level1,
        level2: level2 === "--" ? null : level2,
        level3: level3 === "--" ? null : level3,
        correspondence: correspondence,




      }

      arry.push(groupobj);

      // alert("heer")
      // console.log(arry);
      // return;
      var total = JSON.stringify(arry);

      $.ajax({
        url: "form_process.php",
        method: "POST",
        data: {
          total: total,
          type: "add"
        },
        success: function(response) {
          if (response === 'OK') {
            {
              parent.msgbox('Template Name successfully created ', 'green');
            };
            location.reload();


          } else if (response === "Template Name already exist") {

            {
              parent.msgbox('Template Name already exist ', 'red');
            };
          }
        },
        error: function() {
          alert('Something went wrong');
        }
      });

      return;
    });
  </script>




























  <!-- update process -->

  <script type="text/javascript">
    $("#editsdfer").click(function(event) {
      event.preventDefault();

      // alert("here");
      // return;
      function uniqBy(a, key) {
        var seen = {};
        return a.filter(function(item) {
          var k = key(item);
          return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        })
      }


      function sanitarize(string) {
        const map = {
          '&': '&amp;',
          '<': '&lt;',
          '>': '&gt;',
          '"': '``',
          "'": '`',
          "/": '&#x2F;',
        };
        const reg = /[&<>"'/]/ig;
        return string.replace(reg, (match) => (map[match]));
      }

      var groupName = $('#groupName').val();
      const level1 = $('#level1').val();
      const level2 = $('#level2').val();
      const level3 = $('#level3').val();
      var grouptype = $('#typeG').val();
      let employee = $("#employee").val();
      const correspondence = $('#select_ld').val() || [];




      if (groupName === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      } else {
        groupName = sanitarize(groupName);
      }
      if (level1 === '--') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      }
      if (level3 !== '--' && level2 === "--") {

        {
          parent.msgbox('Kindly fill level 2 or remove level 3 authorization level', 'red');
        };
        return;
      }


      if (grouptype === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;

      }

      if ($('#defined_temp').is(":checked")) {
        // alert('here');
        if ($('#frm_template').val() === "--" || $('#frm_template').val() === " ") {

          {
            parent.msgbox('Kindly select a predefined template ', 'red');
          };
          return;

        }

      }








      if ($('#shortanswer').is(":checked")) {

        var allshortanswer = [];

        //get all shortanswer fields
        if ($('input[name="shortanswer[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer = $('input[name="shortanswer[]"]').map(function() {
            return $(this).val();
          }).get();
          var shortanswer1 = document.getElementsByName('shortanswerequired[]');

          for (let b = 0; b < shortanswer.length; b++) {
            // console.log(shortanswer[b]);

            if (shortanswer[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(shortanswer[b]),
                required: (shortanswer1[b].checked) ? shortanswer1[b].value : "null"

              }
              allshortanswer.push(eachppy);


            }
          }

        }


        // console.log(allshortanswer);
      }
      if ($('#shortanswer_com').is(":checked")) {

        var allshortanswer_com = [];

        //get all shortanswer fields
        if ($('input[name="shortanswer_com[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer_com = $('input[name="shortanswer_com[]"]').map(function() {
            return $(this).val();
          }).get();
          var shortanswer1_com = document.getElementsByName('shortanswerequired_com[]');

          for (let b = 0; b < shortanswer_com.length; b++) {
            console.log(shortanswer_com[b]);

            if (shortanswer_com[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy_com = {

                fieldName: sanitarize(shortanswer_com[b]),
                required: (shortanswer1_com[b].checked) ? shortanswer1_com[b].value : "null"

              }
              allshortanswer_com.push(eachppy_com);


            }
          }

        }


        // console.log(allshortanswer);
      }


      if ($('#url').is(":checked")) {

        var allurl = [];
        //get all shortanswer fields
        if ($('input[name="url[]"]').length) {


          //get all shortanswerequired fields
          var url = $('input[name="url[]"]').map(function() {
            return $(this).val();
          }).get();

          // console.log(shortanswer);
          var url1 = document.getElementsByName('urlrequired[]');
          // console.log(shortanswer1);

          for (let b = 0; b < url.length; b++) {
            // console.log(shortanswer[b]);

            if (url[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(url[b]),
                required: (url1[b].checked) ? url1[b].value : "null"

              }
              allurl.push(eachppy);


            }
          }

        }
      }






      if ($('#numeric').is(":checked")) {

        var allnumeric = [];

        if ($('input[name="numeric[]"]').length) {
          var numeric = $('input[name="numeric[]"]').map(function() {
            return $(this).val();
          }).get();
          var numeric1 = document.getElementsByName('numericrequired[]');

          for (let b = 0; b < numeric.length; b++) {
            // console.log(shortanswer[b]);

            if (numeric[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {

              let eachppynum = {

                fieldName: sanitarize(numeric[b]),
                required: (numeric1[b].checked) ? numeric1[b].value : "null"

              }



              allnumeric.push(eachppynum);

            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }


      if ($('#date').is(":checked")) {

        var alldate = [];

        if ($('input[name="date[]"]').length) {
          var date = $('input[name="date[]"]').map(function() {
            return $(this).val();
          }).get();
          var date1 = document.getElementsByName('daterequired[]');

          for (let b = 0; b < date.length; b++) {
            // console.log(shortanswer[b]);

            if (date[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {
              let eachppydate = {

                fieldName: sanitarize(date[b]),
                required: (date1[b].checked) ? date1[b].value : "null"

              }

              alldate.push(eachppydate);
            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }

      if ($('#file').is(":checked")) {

        var allfile = [];

        if ($('input[name="file[]"]').length) {
          var file = $('input[name="file[]"]').map(function() {
            return $(this).val();
          }).get();
          var file1 = document.getElementsByName('filerequired[]');

          for (let b = 0; b < file.length; b++) {
            // console.log(shortanswer[b]);

            if (file[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {
              let eachppyfile = {

                fieldName: sanitarize(file[b]),
                required: (file1[b].checked) ? file1[b].value : "null"

              }

              allfile.push(eachppyfile);
            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }

      if ($('#paragraph').is(":checked")) {
        var allparagraph = [];


        if ($('input[name="paragraph[]"]').length) {
          var paragraph = $('input[name="paragraph[]"]').map(function() {
            return $(this).val();
          }).get();
          var paragraph1 = document.getElementsByName('paragraphrequired[]');

          for (let b = 0; b < paragraph.length; b++) {
            // console.log(paragraph[b]);

            if (paragraph[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {

              let eachppyparagraph = {

                fieldName: sanitarize(paragraph[b]),
                required: (paragraph1[b].checked) ? paragraph1[b].value : "null"

              }

              allparagraph.push(eachppyparagraph);
            }
          }

          // console.log(allparagraph);
          // return;

        }
      }


      if ($('#dropdown').is(":checked")) {

        var alldropdown = [];
        var dropdownpper = document.getElementsByName('dropdownrequired[]');
        var refactorArry = [];

        for (var pp = c - 1; pp >= 0; pp--) {
          if ($('input[name="dropdown' + pp + '[]"]').length) {

            var dropdownpp = $('input[name="dropdown' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < dropdownpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (dropdownpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!alldropdown.includes(dropdownpp)) {
                  const dropd = dropdownpp.map(function(elem) {
                    return sanitarize(elem);
                  });


                  // console.log(dropd,dropdownpp1)

                  alldropdown.push(dropd);


                  alldropdown = uniqBy(alldropdown, JSON.stringify);

                }
              }
            }



          }
        }
        var rearrangedArry = alldropdown.reverse();

        for (let iz = 0; iz < rearrangedArry.length; iz++) {

          let eachdropdownppy = {

            fieldName: sanitarize(rearrangedArry[iz][0]),
            options: rearrangedArry[iz].slice(1),
            required: (dropdownpper[iz].checked) ? dropdownpper[iz].value : "null"

          }

          refactorArry.push(eachdropdownppy);

        }









      }




      if ($('#checkbox').is(":checked")) {

        var allcheckbox = [];
        var checkboxpper = document.getElementsByName('checkboxrequired[]');
        var refactorArrychkbox = [];

        // console.log(f);
        // return;

        for (let pp = f - 1; pp >= 0; pp--) {
          if ($('input[name="checkbox' + pp + '[]"]').length) {

            var checkboxpp = $('input[name="checkbox' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < checkboxpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (checkboxpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allcheckbox.includes(checkboxpp)) {
                  const checkd = checkboxpp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allcheckbox.push(checkd);

                  allcheckbox = uniqBy(allcheckbox, JSON.stringify);

                }
              }
            }



          }
        }

        var rearrangedArrychk = allcheckbox.reverse();

        for (let iz = 0; iz < rearrangedArrychk.length; iz++) {
          console.log(checkboxpper[iz].checked, iz);

          let eachcheckboxppy = {

            fieldName: sanitarize(rearrangedArrychk[iz][0]),
            options: rearrangedArrychk[iz].slice(1),
            required: (checkboxpper[iz].checked) ? checkboxpper[iz].value : "null"

          }

          refactorArrychkbox.push(eachcheckboxppy);

        }






      }




      if ($('#radio').is(":checked")) {



        var allradio = [];
        var radiopper = document.getElementsByName('radiorequired[]');
        var refactorArryradio = [];


        for (let pp = raf - 1; pp >= 0; pp--) {
          if ($('input[name="radio' + pp + '[]"]').length) {

            var radiopp = $('input[name="radio' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            // console.log(radiopp);

            for (let b = 0; b < radiopp.length; b++) {
              // console.log(dropdownpp[b]);

              if (radiopp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allradio.includes(radiopp)) {
                  const checkd = radiopp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allradio.push(checkd);

                  allradio = uniqBy(allradio, JSON.stringify);

                }
              }
            }



          }
        }



        var rearrangedArryradio = allradio.reverse();

        for (let iz = 0; iz < rearrangedArryradio.length; iz++) {

          let eachradioppy = {

            fieldName: sanitarize(rearrangedArryradio[iz][0]),
            options: rearrangedArryradio[iz].slice(1),
            required: (radiopper[iz].checked) ? radiopper[iz].value : "null"

          }

          refactorArryradio.push(eachradioppy);


          // console.log(refactorArryradio);
          // return;

        }



      }

      var arry = [];
      var groupobj = {
        id: eachgroup,
        templateName: groupName,
        shortanswer: $('#shortanswer').is(":checked") ? allshortanswer : null,
        shortanswer_com: $('#shortanswer_com').is(":checked") ? allshortanswer_com : null,
        url: $('#url').is(":checked") ? allurl : null,
        paragraph: $('#paragraph').is(":checked") ? allparagraph : null,
        dropdown: $('#dropdown').is(":checked") ? refactorArry : null,
        checkbox: $('#checkbox').is(":checked") ? refactorArrychkbox : null,
        radio: $('#radio').is(":checked") ? refactorArryradio : null,
        numeric: $('#numeric').is(":checked") ? allnumeric : null,
        def_temp_name: $('#defined_temp').is(":checked") ? $('#frm_template').val() : null,
        date: $('#date').is(":checked") ? alldate : null,
        file: $('#file').is(":checked") ? allfile : null,
        level1: level1 === "--" ? null : level1,
        level2: level2 === "--" ? null : level2,
        level3: level3 === "--" ? null : level3,
        correspondence,


      }
      arry.push(groupobj);
      var total = JSON.stringify(arry);

      $.ajax({
        url: "form_process.php",
        method: "POST",
        data: {
          total: total,
          type: "edit"
        },
        success: function(response) {
          if (response === "OK") {
            // alert('made it');
            {
              parent.msgbox('Template successfully updated ', 'green');
            };
            location.reload();


          } else if (response === "Template Name already exist") {

            {
              parent.msgbox('Template Name already exist ', 'red');
            };
          }
        },
        error: function() {
          alert('Something went wrong');
        }
      });

      return;

      // alert('about to submit');

      // $('#groupForm').submit();
      // document.groupForm.submit();
    });
  </script>














  <!-- save as process -->

  <script type="text/javascript">
    $("#saveassdfer").click(function(event) {
      event.preventDefault();


      function uniqBy(a, key) {
        var seen = {};
        return a.filter(function(item) {
          var k = key(item);
          return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        })
      }


      function sanitarize(string) {
        const map = {
          '&': '&amp;',
          '<': '&lt;',
          '>': '&gt;',
          '"': '&quot;',
          "'": '&#x27;',
          "/": '&#x2F;',
        };
        const reg = /[&<>"'/]/ig;
        return string.replace(reg, (match) => (map[match]));
      }

      var groupName = $('#groupName').val();
      var grouptype = $('#typeG').val();
      const level1 = $('#level1').val();
      const level2 = $('#level2').val();
      const level3 = $('#level3').val();
      let employee = $("#employee").val();
      const correspondence = $('#select_ld').val() || [];



      if (groupName === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      } else {
        groupName = sanitarize(groupName);
      }
      if (level1 === '--') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;
      }
      if (level3 !== '--' && level2 === "--") {

        {
          parent.msgbox('Kindly fill level 2 or remove level 3 authorization level', 'red');
        };
        return;
      }

      if (grouptype === '') {

        {
          parent.msgbox('All fields are are compulsory ', 'red');
        };
        return;

      }

      if ($('#defined_temp').is(":checked")) {
        // alert('here');
        if ($('#frm_template').val() === "--" || $('#frm_template').val() === " ") {

          {
            parent.msgbox('Kindly select a predefined template ', 'red');
          };
          return;

        }

      }








      if ($('#shortanswer').is(":checked")) {

        var allshortanswer = [];

        //get all shortanswer fields
        if ($('input[name="shortanswer[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer = $('input[name="shortanswer[]"]').map(function() {
            return $(this).val();
          }).get();
          var shortanswer1 = document.getElementsByName('shortanswerequired[]');

          for (let b = 0; b < shortanswer.length; b++) {
            // console.log(shortanswer[b]);

            if (shortanswer[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(shortanswer[b]),
                required: (shortanswer1[b].checked) ? shortanswer1[b].value : "null"

              }
              allshortanswer.push(eachppy);


            }
          }

        }










        // console.log(allshortanswer);
      }
      if ($('#shortanswer_com').is(":checked")) {

        var allshortanswer_com = [];

        //get all shortanswer fields
        if ($('input[name="shortanswer_com[]"]').length) {


          //get all shortanswerequired fields
          var shortanswer_com = $('input[name="shortanswer_com[]"]').map(function() {
            return $(this).val();
          }).get();
          var shortanswer1_com = document.getElementsByName('shortanswerequired_com[]');

          for (let b = 0; b < shortanswer_com.length; b++) {
            // console.log(shortanswer[b]);

            if (shortanswer_com[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy_com = {

                fieldName: sanitarize(shortanswer_com[b]),
                required: (shortanswer1_com[b].checked) ? shortanswer1_com[b].value : "null"

              }
              allshortanswer_com.push(eachppy_com);


            }
          }

        }










        // console.log(allshortanswer);
      }

      if ($('#url').is(":checked")) {

        var allurl = [];
        //get all shortanswer fields
        if ($('input[name="url[]"]').length) {


          //get all shortanswerequired fields
          var url = $('input[name="url[]"]').map(function() {
            return $(this).val();
          }).get();

          // console.log(shortanswer);
          var url1 = document.getElementsByName('urlrequired[]');
          // console.log(shortanswer1);

          for (let b = 0; b < url.length; b++) {
            // console.log(shortanswer[b]);

            if (url[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {
              // create each field object hasOwnProperty

              let eachppy = {

                fieldName: sanitarize(url[b]),
                required: (url1[b].checked) ? url1[b].value : "null"

              }
              allurl.push(eachppy);


            }
          }

        }
      }

      if ($('#numeric').is(":checked")) {

        var allnumeric = [];

        if ($('input[name="numeric[]"]').length) {
          var numeric = $('input[name="numeric[]"]').map(function() {
            return $(this).val();
          }).get();
          var numeric1 = document.getElementsByName('numericrequired[]');

          for (let b = 0; b < numeric.length; b++) {
            // console.log(shortanswer[b]);

            if (numeric[b] === "") {
              {
                parent.msgbox('All fields are are compulsory ', 'red');
              };
              return;
            } else {

              let eachppynum = {

                fieldName: sanitarize(numeric[b]),
                required: (numeric1[b].checked) ? numeric1[b].value : "null"

              }



              allnumeric.push(eachppynum);

            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }


      if ($('#date').is(":checked")) {

        var alldate = [];

        if ($('input[name="date[]"]').length) {
          var date = $('input[name="date[]"]').map(function() {
            return $(this).val();
          }).get();
          var date1 = document.getElementsByName('daterequired[]');

          for (let b = 0; b < date.length; b++) {
            // console.log(shortanswer[b]);

            if (date[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {
              let eachppydate = {

                fieldName: sanitarize(date[b]),
                required: (date1[b].checked) ? date1[b].value : "null"

              }

              alldate.push(eachppydate);
            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }
      if ($('#file').is(":checked")) {

        var allfile = [];

        if ($('input[name="file[]"]').length) {
          var file = $('input[name="file[]"]').map(function() {
            return $(this).val();
          }).get();
          var file1 = document.getElementsByName('filerequired[]');

          for (let b = 0; b < file.length; b++) {
            // console.log(shortanswer[b]);

            if (file[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {
              let eachppyfile = {

                fieldName: sanitarize(file[b]),
                required: (file1[b].checked) ? file1[b].value : "null"

              }

              allfile.push(eachppyfile);
            }
          }

        }
        // return;
        // console.log(allshortanswer);
      }

      if ($('#paragraph').is(":checked")) {
        var allparagraph = [];


        if ($('input[name="paragraph[]"]').length) {
          var paragraph = $('input[name="paragraph[]"]').map(function() {
            return $(this).val();
          }).get();
          var paragraph1 = document.getElementsByName('paragraphrequired[]');

          for (let b = 0; b < paragraph.length; b++) {
            // console.log(paragraph[b]);

            if (paragraph[b] === "") {
              {
                parent.msgbox('All fields are  compulsory ', 'red');
              };
              return;
            } else {

              let eachppyparagraph = {

                fieldName: sanitarize(paragraph[b]),
                required: (paragraph1[b].checked) ? paragraph1[b].value : "null"

              }

              allparagraph.push(eachppyparagraph);
            }
          }

          // console.log(allparagraph);
          // return;

        }
      }


      if ($('#dropdown').is(":checked")) {

        var alldropdown = [];
        var dropdownpper = document.getElementsByName('dropdownrequired[]');
        var refactorArry = [];

        for (var pp = c - 1; pp >= 0; pp--) {
          if ($('input[name="dropdown' + pp + '[]"]').length) {

            var dropdownpp = $('input[name="dropdown' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < dropdownpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (dropdownpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!alldropdown.includes(dropdownpp)) {
                  const dropd = dropdownpp.map(function(elem) {
                    return sanitarize(elem);
                  });


                  // console.log(dropd,dropdownpp1)

                  alldropdown.push(dropd);


                  alldropdown = uniqBy(alldropdown, JSON.stringify);

                }
              }
            }



          }
        }
        var rearrangedArry = alldropdown.reverse();

        for (let iz = 0; iz < rearrangedArry.length; iz++) {

          let eachdropdownppy = {

            fieldName: sanitarize(rearrangedArry[iz][0]),
            options: rearrangedArry[iz].slice(1),
            required: (dropdownpper[iz].checked) ? dropdownpper[iz].value : "null"

          }

          refactorArry.push(eachdropdownppy);

        }









      }




      if ($('#checkbox').is(":checked")) {

        var allcheckbox = [];
        var checkboxpper = document.getElementsByName('checkboxrequired[]');
        var refactorArrychkbox = [];

        // console.log(f);
        // return;

        for (let pp = f - 1; pp >= 0; pp--) {
          if ($('input[name="checkbox' + pp + '[]"]').length) {

            var checkboxpp = $('input[name="checkbox' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            for (let b = 0; b < checkboxpp.length; b++) {
              // console.log(dropdownpp[b]);

              if (checkboxpp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allcheckbox.includes(checkboxpp)) {
                  const checkd = checkboxpp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allcheckbox.push(checkd);

                  allcheckbox = uniqBy(allcheckbox, JSON.stringify);

                }
              }
            }



          }
        }

        var rearrangedArrychk = allcheckbox.reverse();

        for (let iz = 0; iz < rearrangedArrychk.length; iz++) {
          // console.log(checkboxpper[iz].checked, iz);

          let eachcheckboxppy = {

            fieldName: sanitarize(rearrangedArrychk[iz][0]),
            options: rearrangedArrychk[iz].slice(1),
            required: (checkboxpper[iz].checked) ? checkboxpper[iz].value : "null"

          }

          refactorArrychkbox.push(eachcheckboxppy);

        }






      }




      if ($('#radio').is(":checked")) {



        var allradio = [];
        var radiopper = document.getElementsByName('radiorequired[]');
        var refactorArryradio = [];


        for (let pp = raf - 1; pp >= 0; pp--) {
          if ($('input[name="radio' + pp + '[]"]').length) {

            var radiopp = $('input[name="radio' + pp + '[]"]').map(function() {
              return $(this).val();
            }).get();

            // console.log(radiopp);

            for (let b = 0; b < radiopp.length; b++) {
              // console.log(dropdownpp[b]);

              if (radiopp[b] === "") {
                {
                  parent.msgbox('All fields are are compulsory ', 'red');
                };
                return;
              } else {

                if (!allradio.includes(radiopp)) {
                  const checkd = radiopp.map(function(elem) {
                    return sanitarize(elem);
                  });
                  allradio.push(checkd);

                  allradio = uniqBy(allradio, JSON.stringify);

                }
              }
            }



          }
        }



        var rearrangedArryradio = allradio.reverse();

        for (let iz = 0; iz < rearrangedArryradio.length; iz++) {

          let eachradioppy = {

            fieldName: sanitarize(rearrangedArryradio[iz][0]),
            options: rearrangedArryradio[iz].slice(1),
            required: (radiopper[iz].checked) ? radiopper[iz].value : "null"

          }

          refactorArryradio.push(eachradioppy);


          // console.log(refactorArryradio);
          // return;

        }



      }


      $('<div></div>').appendTo('body')
        .html('<div><class=TextBoxText>Do you wish to same this template as a new one?</h5></div>')
        .dialog({
          modal: true,
          title: "Delete Booking Items",
          zIndex: 10000,
          autoOpen: true,
          width: 'auto',
          resizable: true,
          height: 'auto',
          resizable: true,
          buttons: {
            Yes: function() {


              $('<div></div>').appendTo('body')
                .html('<input name="mesg" class="form-control" placeholder="Template name"  id="sd"/>').dialog({

                  modal: true,
                  title: " Template Name ",
                  zIndex: 10000,
                  autoOpen: true,
                  height: 200 + 6,
                  width: 500 + 20,
                  buttons: {

                    Send: function() {

                      var msgC = document.getElementById("sd").value;



                      if (msgC == '') {

                        {
                          parent.msgbox('Please provide template name.', 'red');
                        };

                        return;

                      }


                      var arry = [];
                      var groupobj = {
                        id: Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36),
                        templateName: msgC,
                        shortanswer: $('#shortanswer').is(":checked") ? allshortanswer : null,
                        shortanswer_com: $('#shortanswer_com').is(":checked") ? allshortanswer_com : null,
                        url: $('#url').is(":checked") ? allurl : null,
                        paragraph: $('#paragraph').is(":checked") ? allparagraph : null,
                        dropdown: $('#dropdown').is(":checked") ? refactorArry : null,
                        checkbox: $('#checkbox').is(":checked") ? refactorArrychkbox : null,
                        radio: $('#radio').is(":checked") ? refactorArryradio : null,
                        numeric: $('#numeric').is(":checked") ? allnumeric : null,
                        def_temp_name: $('#defined_temp').is(":checked") ? $('#frm_template').val() : null,
                        date: $('#date').is(":checked") ? alldate : null,
                        file: $('#file').is(":checked") ? allfile : null,
                        def_temp_name: $('#defined_temp').is(":checked") ? $('#frm_template').val() : null,
                        level1: level1 === "--" ? null : level1,
                        level2: level2 === "--" ? null : level2,
                        level3: level3 === "--" ? null : level3,
                        correspondence,

                      }
                      arry.push(groupobj);
                      var total = JSON.stringify(arry);

                      $.ajax({
                        url: "form_process.php",
                        method: "POST",
                        data: {
                          total: total,
                          type: "saveAs"
                        },
                        success: function(response) {
                          if (response === "OK") {
                            // alert('made it');
                            {
                              parent.msgbox('Template successfully updated ', 'green');
                            };
                            location.reload();


                          } else if (response === "Template Name already exist") {

                            {
                              parent.msgbox('Template Name already exist ', 'red');
                            };
                            $(this).dialog('destroy');
                            location.reload();
                          }
                        },
                        error: function() {
                          alert('Something went wrong');
                          location.reload();
                        }
                      });



                      $(this).dialog("close");

                    }
                  }

                });





              $(this).dialog("close");
            },
            No: function() {
              $(this).dialog("close");
            }
          },
          close: function(event, ui) {
            $(this).remove();
          }
        });









      return;

      // alert('about to submit');

      // $('#groupForm').submit();
      // document.groupForm.submit();
    });
  </script>




  <!-- delete process -->
  <script type="text/javascript">
    $("#deletesdfer").click(function(event) {
      event.preventDefault();

      $('<div></div>').appendTo('body')
        .html('<div><class=TextBoxText>Do you wish to <strong>delete</strong> the selected record(s)?</h5></div>')
        .dialog({
          modal: true,
          title: "Delete Group",
          zIndex: 10000,
          autoOpen: true,
          width: 'auto',
          resizable: true,
          height: 'auto',
          resizable: true,
          buttons: {
            Yes: function() {

              function uniqBy(a, key) {
                var seen = {};
                return a.filter(function(item) {
                  var k = key(item);
                  return seen.hasOwnProperty(k) ? false : (seen[k] = true);
                })
              }


              function sanitarize(string) {
                const map = {
                  '&': '&amp;',
                  '<': '&lt;',
                  '>': '&gt;',
                  '"': '&quot;',
                  "'": '&#x27;',
                  "/": '&#x2F;',
                };
                const reg = /[&<>"'/]/ig;
                return string.replace(reg, (match) => (map[match]));
              }

              var groupName = $('#groupName').val();



              if (groupName === '') {

                {
                  parent.msgbox('All fields are  compulsory ', 'red');
                };
                return;
              } else {
                groupName = sanitarize(groupName);
              }



              var arry = [];
              var groupobj = {
                id: eachgroup,

              }
              arry.push(groupobj);
              var total = JSON.stringify(arry);

              $.ajax({
                url: "form_process.php",
                method: "POST",
                data: {
                  total: total,
                  type: "delete"
                },
                success: function(response) {
                  if (response === "OK") {
                    // alert('made it');
                    {
                      parent.msgbox('Group successfully deleted ', 'green');
                    };
                    location.reload();


                  } else if (response === "Group Name already exist") {

                    {
                      parent.msgbox('Group Name already exist ', 'red');
                    };
                  } else if (response === "TIED") {

                    {
                      parent.msgbox('Group cannot  be deleted,because it is tied to a booking item.', 'red');
                    };


                  }
                },
                error: function() {
                  alert('Something went wrong');
                }
              });





              $(this).dialog("close");
            },
            No: function() {
              $(this).dialog("close");
            }
          },
          close: function(event, ui) {
            $(this).remove();
          }
        });


      // return;

      // alert("here");
      // return;

      return;

      // alert('about to submit');

      // $('#groupForm').submit();
      // document.groupForm.submit();
    });
  </script>








  <!-- get field group info   show info-->

  <script>
    var q = 1;
    var q1 = 1;
    var q1ra = 1;

    function forShortanswer() {

      $("#shortanswer").prop("checked", false);

      $("#selemp > div").fadeOut(10);
    }

    function forShortanswer_com() {

      $("#shortanswer_com").prop("checked", false);

      $("#selemp_com > div").fadeOut(10);
    }

    function forUrl() {

      $("#url").prop("checked", false);

      $("#selemp7 > div").fadeOut(10);
    }

    function basic() {

      $("#basic").change(function() {
        if (this.checked) {
          $("#basicCon").fadeIn();
        } else {
          $("#basicCon").fadeOut(10);
        }
      });
    }

    function aadd() {
      var lw = Date.now();

      $('#ttable').append('<tr id="ro' + lw + '"><td><input type="text" name="fullnames[]"></td><td><input type="text" name="phonenos[]"  placeholder="08012345678" ></td><td><input type="text" name="emails[]"></td><td><span class="badge bg-red bt-rem" id="' + lw + '" style="cursor: pointer;">remove</span></td></tr>');


    }

    function forParagraph() {

      $("#paragraph").prop("checked", false);

      $("#selemp1 > div").fadeOut(10);
    }

    function forCheckbox() {

      $("#checkbox").prop("checked", false);

      $("#selemp5 > div").fadeOut(10);

    }

    function forRadio() {

      $("#radio").prop("checked", false);

      $("#selemp8 > div").fadeOut(10);

    }

    function forDate() {

      $("#date").prop("checked", false);

      $("#selemp4 > div").fadeOut(10);

    }

    function forFile() {

      $("#file").prop("checked", false);

      $("#selemp9 > div").fadeOut(10);

    }

    function forNumeric() {

      $("#numeric").prop("checked", false);

      $("#selemp3 > div").fadeOut(10);

    }

    function forDropdown() {
      $("#dropdown").prop("checked", false);

      $("#selemp2 > div").fadeOut(10);

    }


    function adde() {
      // alert(i);
      // alert(we);

      if (i <= 100) {

        $('#dynanic_field').append('<div class="form-group row the' + i + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + i + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="shortanswerequired[]" id="shortanswerequired_' + i + '" ><label for="shortanswerequired_' + i + '">Make field compulsory</label></div> </div></div>');
        i++;

        // alert(i);

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }
    }

    function adde_com() {
      // alert(i);
      // alert(we);

      if (co <= 100) {

        $('#dynanic_field_com').append('<div class="form-group row the' + co + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer_com[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + co + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="shortanswerequired_com[]" id="shortanswerequired_com_' + co + '" ><label for="shortanswerequired_com_' + co + '">Make field compulsory</label></div> </div></div>');
        co++;

        // alert(i);

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }
    }

    function adde7() {
      // alert(i);
      // alert(we);

      if (u <= 100) {

        $('#dynanic_field7').append('<div class="form-group row the' + u + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="url[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + u + '"class="btn btn-default btn_remove7 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="urlrequired[]" id="urlrequired_' + u + '" ><label for="urlrequired_' + u + '">Make field compulsory</label></div> </div></div>');
        u++;

        // alert(i);

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }
    }


    function adde3() {

      if (n <= 100) {

        $('#dynanic_field3').append('<div class="form-group row then' + n + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="numeric[]" ><span class="input-group-btn "><button type="button" name="remove" id="n' + n + '" class="btn btn-default btn_remove3 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px"><input type="checkbox" checked name="numericrequired[]" id="numericrequired_' + n + '" ><label for="numericrequired_' + n + '">Make field compulsory</label></div> </div></div>');
        n++;

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100)  Questions', 'green');
        }
      }
    }

    function adde4() {

      if (d <= 100) {

        $('#dynanic_field4').append('<div class="form-group row thed' + d + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="date[]" ><span class="input-group-btn "><button type="button" name="remove" id="d' + d + '" class="btn btn-default btn_remove4 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px"><input type="checkbox" checked name="daterequired[]" id="daterequired_' + d + '" ><label for="daterequired_' + d + '">Make field compulsory</label></div> </div></div>');
        d++;

      } else {
        {
          parent.msgbox('You Cannot add more than five(5) Questions', 'green');
        }
      }
    }


    function adde9() {

      if (dd <= 100) {

        $('#dynanic_field9').append('<div class="form-group row thedf' + dd + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="file[]" ><span class="input-group-btn "><button type="button" name="remove" id="df' + dd + '" class="btn btn-default btn_remove9 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px"><input type="checkbox" checked name="filerequired[]" id="filerequired_' + dd + '" ><label for="filerequired_' + dd + '">Make field compulsory</label></div> </div></div>');
        dd++;

      } else {
        {
          parent.msgbox('You Cannot add more than five(5) Questions', 'green');
        }
      }
    }








    function adde1() {

      if (j <= 100) {

        $('#dynanic_field1').append('<div class="form-group row thep' + j + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="paragraph[]" ><span class="input-group-btn "><button type="button" name="remove" id="p' + j + '"class="btn btn-default btn_remove1 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="paragraphrequired[]" id="paragraphrequired_' + j + '" ><label for="paragraphrequired_' + j + '">Make field compulsory</label></div> </div></div>');
        j++;

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }

    }

    function adde2() {

      if (z <= 100) {

        $('#dynanic_field2').append('<div class="thed' + c + '"><hr><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remove2 " id="d' + c + '" style="color:red" ><i></i>Remove</button></span></div></div>   <div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="dropdownrequired[]" id="dropdownrequired_' + c + '" ><label for="dropdownrequired_' + c + '">Make field compulsory</label></div><div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ></div></div><div id="dyn_field' + c + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction(' + c + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
        z++;
        c++;

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }

    }

    function adde25() {

      if (r <= 100) {

        $('#dynanic_field5').append('<div class="thec' + f + '"><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2 " id="c' + f + '" style="color:red" ><i></i>Remove</button></span></div></div>  <div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="checkboxrequired[]" id="checkboxrequired_' + f + '" ><label for="checkboxrequired_' + f + '">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ></div></div><div id="dyn_fied1' + f + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction1(' + f + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
        r++;
        f++;

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }

    }

    function adde8() {

      if (ra <= 100) {

        $('#dynanic_field8').append('<div class="thera' + raf + '"><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2ra " id="ra' + f + '" style="color:red" ><i></i>Remove</button></span></div></div>  <div class="checkbox" style="margin-left:11px"><input type="checkbox" checked name="radiorequired[]" id="radiorequired_' + raf + '" ><label for="radiorequired_' + raf + '">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ></div></div><div id="dyn_fied1ra' + raf + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class="  btn btn-default " onclick="myFunction1ra(' + raf + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');
        ra++;
        raf++;

      } else {
        {
          parent.msgbox('You Cannot add more than hundred(100) Questions', 'green');
        }
      }

    }


    function convert(str) {

      str = str.replace("&amp;", "&");
      str = str.replace("&lt;", "<");
      str = str.replace("&gt;", ">");
      str = str.replace("&quot;", '"');
      str = str.replace("&#x27;", "'");
      str = str.replace("&#x2F;", '/');
      return str;

    }


    $("#basic").change(function() {
      if (this.checked) {
        $("#basicCon").fadeIn();
      } else {
        $("#basicCon").fadeOut(10);
      }
    });

    function getGroupArry() {
      $('#header').text('Edit Form Template');
      $('#sdfer').fadeOut(10);
      $('#editsdfer').fadeOut(10);
      $('#saveassdfer').fadeOut(10);
      $('#deletesdfer').fadeOut(10);
      c = 1;
      co = 1;
      i = 1;
      j = 1;
      z = 1;
      n = 1;
      d = 1;
      dd = 1;
      f = 1;
      r = 1;
      ra = 1;
      raf = 1;
      u = 1;
      eachgroup = $("#eachGroup").val();
      // console.log(eachgroup);
      if (eachgroup == '') {
        {
          parent.msgbox('Please select a group name ', 'red');
        };
        return;
      }

      $('#editsdfer').fadeIn();
      $('#saveassdfer').fadeIn();
      $('#deletesdfer').fadeIn();
      // alert('here');
      var data = <?php echo json_encode($groupsarry); ?>;
      // console.log(data);

      var result = data.filter(obj => {
        return obj.id === eachgroup;
      });

      if (result) {

        // console.log(result);

        var groupName = convert(result[0].templateName);
        var level1 = result[0].level1;
        var level2 = result[0].level2;
        var level3 = result[0].level3;
        var correspondence = result[0].correspondence;
        var def_temp = result[0].def_temp_name || null;

        //  console.log(def_temp);


        //  console . log(correspondence);


        var shortanswer = result[0].shortanswer;
        var shortanswer_com = result[0].shortanswer_com;
        var paragraph = result[0].paragraph;
        var dropdown = result[0].dropdown;
        var checkbox = result[0].checkbox;
        var radio = result[0].radio;
        var numeric = result[0].numeric;
        var date = result[0].date;
        var file = result[0].file;
        var url = result[0].url;



        // console.log(emp_sup);
        // console.log(admin_sup);
        if (def_temp !== null) {
          $("#defined_temp").prop('checked', true);
          $("#predefined_tem > div").fadeIn();
          $("#template_fields").fadeOut(10);
          $('#frm_template').val(def_temp);

        } else {
          $("#defined_temp").prop('checked', false);
          $("#predefined_tem > div").fadeOut(10);
          $("#template_fields").fadeIn();
          $('#frm_template').val("--");
        }


        $('#groupName').val(groupName);
        $('#level1').val(level1);
        $('#level2').val(level2);
        $('#level3').val(level3);
        $('#select_ld').val(correspondence);
        //  $('#select_ld').val(correspondence);
        $('.select2').select2();

        document.querySelector('#groupName').setAttribute('readonly', true);

        if (shortanswer) {
          $('#shortanswer').prop('checked', true);
          $("#selemp > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get shortanswer array first item;
          var shtary = shortanswer[0];

          // get the required property

          var chk = (shtary.required == 'on') ? 'checked' : '';

          $('#selemp').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Short Text Field</h5> <button type="button" class="close" aria-label="Close"  onclick="forShortanswer()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer[]"  id="getshort" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" ' + chk + ' name="shortanswerequired[]" id="shortanswerequired_0" ><label for="shortanswerequired_0">Make field compulsory</label></div></div></div></div>');



          $("#selemp > div").fadeIn();

          // console.log(shtary);

          // var res = str.split(" ");

          $("#getshort").val(convert(shtary.fieldName));


          if (shortanswer.length > 1) {
            for (let b = 1; b < shortanswer.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);

              // loop array
              let looarry = shortanswer[b];
              let chksht = (looarry.required == 'on') ? 'checked' : '';

              // console.log(i);


              if (i <= 99) {

                $('#dynanic_field').append('<div class="form-group row the' + i + ' "> <hr> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarry.fieldName) + '" name="shortanswer[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + i + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" ' + chksht + ' name="shortanswerequired[]" id="shortanswerequired_' + i + '"><label for="shortanswerequired_' + i + '">Make field compulsory</label></div> </div></div>');
                i++;

              }

            }
            // let we= i;
            window['i'] = i;
            // alert(i);
          }
        } else {
          $("#selemp > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Short Answer Field</h5> </div><div class="box-body"  id="dynanic_field"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer[]"  id="getshort" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="shortanswerequired[]" id="shortanswerequired_0" ><label for="shortanswerequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#shortanswer').is(":checked")) {

            // alert("dfdf");
            $('#shortanswer').prop('checked', false);
          }

        }


        if (shortanswer_com) {
          $('#shortanswer_com').prop('checked', true);
          $("#selemp_com > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get shortanswer array first item;
          var shtary_com = shortanswer_com[0];

          // get the required property

          var chk = (shtary_com.required == 'on') ? 'checked' : '';

          $('#selemp_com').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Comment Field</h5> <button type="button" class="close" aria-label="Close"  onclick="forShortanswer_com()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field_com"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer_com[]"  id="getshort_com" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde_com()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" ' + chk + ' name="shortanswerequired_com[]" id="shortanswerequired_com_0" ><label for="shortanswerequired_com_0">Make field compulsory</label></div></div></div></div>');



          $("#selemp_com > div").fadeIn();

          // console.log(shtary);

          // var res = str.split(" ");

          $("#getshort_com").val(convert(shtary_com.fieldName));


          if (shortanswer_com.length > 1) {
            for (let b = 1; b < shortanswer_com.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);

              // loop array
              let looarry = shortanswer_com[b];
              let chksht = (looarry.required == 'on') ? 'checked' : '';

              // console.log(i);


              if (co <= 99) {

                $('#dynanic_field_com').append('<div class="form-group row the' + co + ' "> <hr> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarry.fieldName) + '" name="shortanswer_com[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + co + '"class="btn btn-default btn_remove "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" ' + chksht + ' name="shortanswerequired_com[]" id="shortanswerequired_com_' + co + '"><label for="shortanswerequired_com_' + co + '">Make field compulsory</label></div> </div></div>');
                co++;

              }

            }
            // let we= i;
            window['co'] = co;
            // alert(i);
          }
        } else {
          $("#selemp_com > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp_com').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Comment Field</h5> </div><div class="box-body"  id="dynanic_field_com"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="shortanswer_com[]"  id="getshort_com" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde_com()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="shortanswerequired_com[]" id="shortanswerequired_com_0" ><label for="shortanswerequired_com_0">Make field compulsory</label></div></div></div></div>');

          if ($('#shortanswer_com').is(":checked")) {

            // alert("dfdf");
            $('#shortanswer_com').prop('checked', false);
          }

        }









        if (url) {
          $('#url').prop('checked', true);
          $("#selemp7 > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get shortanswer array first item;
          var ulary = url[0];

          // get the required property

          var chk = (ulary.required == 'on') ? 'checked' : '';

          $('#selemp7').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Url Field</h5> <button type="button" class="close" aria-label="Close"  onclick="forUrl()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field7"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="url[]"  id="geturl" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde7()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" ' + chk + ' name="urlrequired[]" id="urlrequired_0" ><label for="urlrequired_0">Make field compulsory</label></div></div></div></div>');



          $("#selemp7 > div").fadeIn();

          // console.log(shtary);

          // var res = str.split(" ");

          $("#geturl").val(convert(ulary.fieldName));


          if (url.length > 1) {
            for (let b = 1; b < url.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',url.length);

              // loop array
              let looarryurl = url[b];
              let chkul = (looarryurl.required == 'on') ? 'checked' : '';

              // console.log(i);


              if (u <= 99) {

                $('#dynanic_field7').append('<div class="form-group row the' + u + ' "> <hr> <label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarryurl.fieldName) + '" name="url[]" ><span class="input-group-btn "><button type="button" name="remove" id="' + i + '"class="btn btn-default btn_remove7 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox" style="margin-left:11px"><input type="checkbox" ' + chkul + ' name="urlrequired[]" id="urlrequired_' + u + '"><label for="urlrequired_' + u + '">Make field compulsory</label></div> </div></div>');
                i++;

              }

            }
            // let we= i;
            window['u'] = u;
            // alert(i);
          }
        } else {
          $("#selemp7 > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp7').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Url Field</h5> </div><div class="box-body"  id="dynanic_field7"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="url[]"  id="geturl" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde7()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="urlrequired[]" id="urlrequired_0" ><label for="urlrequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#url').is(":checked")) {

            // alert("dfdf");
            $('#url').prop('checked', false);
          }

        }




        if (numeric) {
          $('#numeric').prop('checked', true);
          $("#selemp3 > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get numeric array first item;
          var numary = numeric[0];

          // get the required property

          var chknum = (numary.required == 'on') ? 'checked' : '';

          $('#selemp3').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Numeric Field</h5><button type="button" class="close" aria-label="Close"  onclick="forNumeric()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field3"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="numeric[]"  id="getnumeric" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde3()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" ' + chknum + ' name="numericrequired[]" id="numericrequired_0" ><label for="numericrequired_0">Make field compulsory</label></div></div></div></div>');

          $("#selemp3 > div").fadeIn();

          $("#getnumeric").val(convert(numary.fieldName));

          if (numeric.length > 1) {
            for (let b = 1; b < numeric.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);


              // loop array
              let looarrynum = numeric[b];
              let chknum = (looarrynum.required == 'on') ? 'checked' : '';


              if (n <= 99) {

                $('#dynanic_field3').append('<div class="form-group row then' + n + ' "> <hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarrynum.fieldName) + '" name="numeric[]" ><span class="input-group-btn "><button type="button" name="remove"  id="n' + n + '"  class="btn btn-default btn_remove3 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox" ' + chknum + ' name="numericrequired[]" id="numericrequired_' + n + '"><label for="numericrequired_' + n + '">Make field compulsory</label></div> </div></div>');
                n++;

              }

            }
            window['n'] = n;
          }
        } else {
          $("#selemp3 > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp3').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Numeric Field</h5> </div><div class="box-body"  id="dynanic_field3"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="numeric[]"  id="getnumeric" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde3()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" checked name="numericrequired[]" id="numericrequired_0" ><label for="numericrequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#numeric').is(":checked")) {

            // alert("dfdf");
            $('#numeric').prop('checked', false);
          }

        }





        if (date) {
          $('#date').prop('checked', true);
          $("#selemp4 > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get numeric array first item;
          var dateary = date[0];

          // get the required property

          var chkdate = (dateary.required == 'on') ? 'checked' : '';
          $('#selemp4').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Date Field</h5><button type="button" class="close" aria-label="Close"  onclick="forDate()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field4"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="date[]"  id="getdate" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde4()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" ' + chkdate + ' name="daterequired[]" id="daterequired_0" ><label for="daterequired_0">Make field compulsory</label></div></div></div></div>');

          $("#selemp4 > div").fadeIn();



          $("#getdate").val(convert(dateary.fieldName));

          if (date.length > 1) {
            for (let b = 1; b < date.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);

              // loop array
              let looarrydate = date[b];
              let chkdate = (looarrydate.required == 'on') ? 'checked' : '';


              if (d <= 99) {

                $('#dynanic_field4').append('<div class="form-group row thed' + d + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarrydate.fieldName) + '" name="date[]" ><span class="input-group-btn "><button type="button" name="remove"  id="d' + d + '"  class="btn btn-default btn_remove4 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox" ' + chkdate + ' name="daterequired[]" id="daterequired_' + d + '"><label for="daterequired_' + d + '">Make field compulsory</label></div> </div></div>');
                d++;

              }

            }
            window['d'] = d;
          }
        } else {
          $("#selemp4 > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp4').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >Date Field</h5> </div><div class="box-body"  id="dynanic_field4"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="date[]"  id="getdate" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde4()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" checked name="daterequired[]" id="daterequired_0" ><label for="daterequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#date').is(":checked")) {

            // alert("dfdf");
            $('#date').prop('checked', false);
          }

        }
        if (file) {
          $('#file').prop('checked', true);
          $("#selemp9 > div").remove();
          // console.log('arraylength',shortanswer.length);

          // get numeric array first item;
          var fileary = file[0];

          // get the required property

          var chkfile = (fileary.required == 'on') ? 'checked' : '';
          $('#selemp9').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >File Field</h5><button type="button" class="close" aria-label="Close"  onclick="forFile()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field9"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="file[]"  id="getfile" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde9()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" ' + chkfile + ' name="filerequired[]" id="filerequired_0" ><label for="filerequired_0">Make field compulsory</label></div></div></div></div>');

          $("#selemp9 > div").fadeIn();



          $("#getfile").val(convert(fileary.fieldName));

          if (file.length > 1) {
            for (let b = 1; b < file.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);

              // loop array
              let looarryfile = file[b];
              let chkfile = (looarryfile.required == 'on') ? 'checked' : '';


              if (dd <= 99) {

                $('#dynanic_field9').append('<div class="form-group row thedf' + dd + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarryfile.fieldName) + '" name="file[]" ><span class="input-group-btn "><button type="button" name="remove"  id="df' + dd + '"  class="btn btn-default btn_remove9 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox" ' + chkfile + ' name="filerequired[]" id="filerequired_' + dd + '"><label for="filerequired_' + dd + '">Make field compulsory</label></div> </div></div>');
                dd++;

              }

            }
            window['dd'] = dd;
          }
        } else {
          $("#selemp9 > div").remove();
          // console.log('arraylength',shortanswer.length);
          $('#selemp9').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary "><div class="box-header text-center"><h5 class="box-title text-dark" >File Field</h5> </div><div class="box-body"  id="dynanic_field9"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="file[]"  id="getfile" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde9()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" checked name="filerequired[]" id="filerequired_0" ><label for="filerequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#file').is(":checked")) {

            // alert("dfdf");
            $('#file').prop('checked', false);
          }

        }







        if (paragraph) {

          // console.log(paragraph);

          $('#paragraph').prop('checked', true);
          $("#selemp1 > div").remove();

          // get numeric array first item;
          var paragraphary = paragraph[0];

          // get the required property

          var chkpara = (paragraphary.required == 'on') ? 'checked' : '';

          $('#selemp1').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5  class=" box-title text-dark" >Paragraph Field</h5> <button type="button" class="close" aria-label="Close"  onclick="forParagraph()"><span aria-hidden="true" style="color: red">&times;</span></button> </div><div class="box-body"  id="dynanic_field1"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="paragraph[]" id="getpara" ><span class="input-group-btn "><button type="button" class="btn btn-default " onclick="adde1()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" ' + chkpara + ' name="paragraphrequired[]" id="paragraphrequired_0" ><label for="paragraphrequired_0">Make field compulsory</label></div></div></div></div>');


          $("#selemp1 > div").fadeIn();

          $("#getpara").val(convert(paragraphary.fieldName));


          if (paragraph.length > 1) {
            for (let b = 1; b < paragraph.length; b++) {
              // console.log('sa');
              // console.log('the b no',b,'arraylength',shortanswer.length);

              // loop array
              let looarrypara = paragraph[b];
              let chkpara = (looarrypara.required == 'on') ? 'checked' : '';


              if (j <= 99) {


                $('#dynanic_field1').append('<div class="form-group row thep' + j + ' "><hr><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="' + convert(looarrypara.fieldName) + '" name="paragraph[]" ><span class="input-group-btn "><button type="button" name="remove" id="p' + j + ' "class="btn btn-default btn_remove1 "  style="color:red" ><i></i> Remove</button></span></div><div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox" ' + chkpara + ' name="paragraphrequired[]" id="paragraphrequired_' + j + '"><label for="paragraphrequired_' + j + '">Make field compulsory</label></div> </div></div>');
                j++;


              }

            }
            window['j'] = j;
          }



        } else {
          $("#selemp1 > div").remove();

          $('#selemp1').append('<div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5  class=" box-title text-dark" >Paragraph Field</h5> </div><div class="box-body"  id="dynanic_field1"><div class="form-group row "><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="paragraph[]" id="getpara" ><span class="input-group-btn "><button type="button" class="btn btn-default " onclick="adde1()" ><i title="Add More Question" ></i>Add More</button></span></div></div><div class="checkbox"><input type="checkbox" checked name="paragraphrequired[]" id="paragraphrequired_0" ><label for="paragraphrequired_0">Make field compulsory</label></div></div></div></div>');

          if ($('#paragraph').is(":checked")) {

            // alert("dfdf");
            $('#paragraph').prop('checked', false);
          }


        }



        if (dropdown) {

          var reverseArry = dropdown;
          // console.log(dropdown);

          $('#dropdown').prop('checked', true);

          $("#selemp2 > div").remove();

          // for (let i = 0; i < dropdown.length; i++) {
          // var len1=$('input[name="dropdown0[]"]');
          var chkdropdown1 = (reverseArry[0].required == 'on') ? 'checked' : '';

          // console.log('fsfs',chkdropdown1);

          $('#selemp2').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Dropdown Field</h5><button type="button" class="close" aria-label="Close"  onclick="forDropdown()"><span aria-hidden="true" style="color: red">&times;</span></button></div><div class="box-body"  id="dynanic_field2"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde2()" ><i title="Add More Question" ></i>Add More</button></span></div> </div>  <div class="checkbox"><input type="checkbox"  ' + chkdropdown1 + '   name="dropdownrequired[]" id="dropdownrequired_0" ><label for="dropdownrequired_0">Make field compulsory</label></div><div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ></div></div><div id="dyn_field0"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          // }







          // alert((reverseArry[0].options.length));

          if (reverseArry[0].options.length > 2) {

            for (var i = 0; i < reverseArry[0].options.length - 2; i++) {

              $('#dyn_field0').append('<div class="form-group row " id="yy' + q + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry(' + q + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
              q++;


            }


          }

          var len = $('input[name="dropdown0[]"]');

          len[0].value = convert(reverseArry[0].fieldName);


          for (let l = 0; l < reverseArry[0].options.length; l++) {

            len[l + 1].value = convert(reverseArry[0].options[l]);

          }

          $("#selemp2 > div").fadeIn();

          if (dropdown.length > 1) {

            for (let i = 1; i < dropdown.length; i++) {
              if (z <= 99) {

                let chkdropdown = (dropdown[i].required == 'on') ? 'checked' : '';
                // var chkdropdown1=(reverseArry[0].required =='on')?'checked':'';

                // console.log('recde',chkdropdownss,i);

                $('#dynanic_field2').append('<div class="thed' + c + '"> <hr><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remove2 " id="d' + c + '" style="color:red" ><i></i>Remove</button></span></div></div>  <div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox"  ' + chkdropdown + ' name="dropdownrequired[]" id="dropdownrequired_' + c + '"><label for="dropdownrequired_' + c + '">Make field compulsory</label></div> <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ></div></div><div id="dyn_field' + c + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction(' + c + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');


                if (reverseArry[i].options.length > 2) {

                  for (var iu = 0; iu < reverseArry[i].options.length - 2; iu++) {

                    $('#dyn_field' + c).append('<div class="form-group row " id="yy' + q + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + c + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry(' + q + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
                    q++;


                  }


                }

                var lent = $('input[name="dropdown' + i + '[]"]');

                lent[0].value = convert(dropdown[i].fieldName);

                for (let k = 0; k < dropdown[i].options.length; k++) {

                  lent[k + 1].value = convert(dropdown[i].options[k]);

                }

                z++;
                c++;

              }



            }


            window['z'] = z;

          }



          // console.log(len[1].value='bis1');


        } else {


          $("#selemp2 > div").remove();

          $('#selemp2').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Dropdown Field</h5></div><div class="box-body"  id="dynanic_field2"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde2()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="dropdownrequired[]" id="dropdownrequired_0" ><label for="dropdownrequired_0">Make field compulsory</label></div>   <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ></div></div><div id="dyn_field0"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          if ($('#dropdown').is(":checked")) {

            // alert("dfdf");
            $('#dropdown').prop('checked', false);
          }



        }










        if (checkbox) {

          // console.log(dropdown.reverse());

          $('#checkbox').prop('checked', true);

          $("#selemp5 > div").remove();

          // for (let i = 0; i < dropdown.length; i++) {




          var reverseArrychk = checkbox;
          // console.log(checkbox);


          $('#checkbox').prop('checked', true);

          $("#selemp5 > div").remove();

          // for (let i = 0; i < dropdown.length; i++) {
          // var len1=$('input[name="dropdown0[]"]');
          var chkcheckbox1 = (reverseArrychk[0].required == 'on') ? 'checked' : '';

          $('#selemp5').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Checkbox Field</h5><button type="button" class="close" aria-label="Close"  onclick="forCheckbox()"><span aria-hidden="true" style="color: red">&times;</span></button></div><div class="box-body"  id="dynanic_field5"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde25()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox"  ' + chkcheckbox1 + '   name="checkboxrequired[]" id="checkboxrequired_0" ><label for="checkboxrequired_0">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ></div></div><div id="dyn_fied10"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry1()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          // }



          // var reverseArry1= checkbox.reverse();



          // alert((reverseArry[0].length));

          if (reverseArrychk[0].options.length > 2) {

            for (var i = 0; i < reverseArrychk[0].options.length - 2; i++) {

              $('#dyn_fied10').append('<div class="form-group row " id="xy1' + q1 + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry1(' + q1 + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
              q1++;


            }


          }

          let len = $('input[name="checkbox0[]"]');

          len[0].value = convert(reverseArrychk[0].fieldName);


          for (let l = 0; l < reverseArrychk[0].options.length; l++) {

            len[l + 1].value = convert(reverseArrychk[0].options[l]);

          }


          $("#selemp5 > div").fadeIn();





          if (checkbox.length > 1) {

            for (let i = 1; i < checkbox.length; i++) {
              if (r <= 99) {


                let chkcheckbox = (checkbox[i].required == 'on') ? 'checked' : '';

                $('#dynanic_field5').append('<div class="thec' + f + '"><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2 " id="c' + f + '" style="color:red" ><i></i>Remove</button></span></div></div> <div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox"  ' + chkcheckbox + ' name="checkboxrequired[]" id="checkboxrequired_' + f + '"><label for="checkboxrequired_' + f + '">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ></div></div><div id="dyn_fied1' + f + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction1(' + f + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');


                if (reverseArrychk[i].options.length > 2) {

                  for (let iu = 0; iu < reverseArrychk[i].options.length - 2; iu++) {

                    $('#dyn_fied1' + f).append('<div class="form-group row " id="yy' + q1 + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + f + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry(' + q1 + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
                    q1++;


                  }


                }

                let lent = $('input[name="checkbox' + i + '[]"]');
                lent[0].value = convert(checkbox[i].fieldName);
                // console.log(checkbox[i].options.length);
                for (let k = 0; k < checkbox[i].options.length; k++) {
                  // console.log(checkbox[i].options[k]);

                  lent[k + 1].value = checkbox[i].options[k];
                  // lent[k+1].value=convert(dropdown[i].options[k]);

                }



                r++;
                f++;

              }



            }

            window['r'] = r;

          }



          // console.log(len[1].value='bis1');


        } else {


          $("#selemp5 > div").remove();

          $('#selemp5').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Checkbox Field</h5></div><div class="box-body"  id="dynanic_field5"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde25()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="checkboxrequired[]" id="checkboxrequired_0" ><label for="checkboxrequired_0">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ></div></div><div id="dyn_fied10"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry1()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          if ($('#checkbox').is(":checked")) {

            // alert("dfdf");
            $('#checkbox').prop('checked', false);
          }



        }


        if (radio) {

          // console.log(dropdown.reverse());

          $('#radio').prop('checked', true);

          $("#selemp8 > div").remove();

          // for (let i = 0; i < dropdown.length; i++) {

          console.log(radio.length);


          var reverseArryradio = radio;
          // console.log(checkbox);


          $('#radio').prop('checked', true);

          $("#selemp8 > div").remove();

          // for (let i = 0; i < dropdown.length; i++) {
          // var len1=$('input[name="dropdown0[]"]');
          var chkradio1 = (reverseArryradio[0].required == 'on') ? 'checked' : '';

          $('#selemp8').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Radio Field</h5><button type="button" class="close" aria-label="Close"  onclick="forRadio()"><span aria-hidden="true" style="color: red">&times;</span></button></div><div class="box-body"  id="dynanic_field8"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde8()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox"  ' + chkradio1 + '   name="radiorequired[]" id="radiorequired_0" ><label for="radiorequired_0">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ></div></div><div id="dyn_fied10ra"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry1ra()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          // }



          // var reverseArry1= checkbox.reverse();



          // alert((reverseArry[0].length));

          if (reverseArryradio[0].options.length > 2) {

            for (var i = 0; i < reverseArryradio[0].options.length - 2; i++) {

              $('#dyn_fied10ra').append('<div class="form-group row " id="xy1ra' + q1ra + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry1ra(' + q1ra + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
              q1ra++;


            }


          }

          let len = $('input[name="radio0[]"]');

          len[0].value = convert(reverseArryradio[0].fieldName);


          for (let l = 0; l < reverseArryradio[0].options.length; l++) {

            len[l + 1].value = convert(reverseArryradio[0].options[l]);

          }


          $("#selemp8 > div").fadeIn();





          if (reverseArryradio.length > 1) {

            for (let i = 1; i < reverseArryradio.length; i++) {
              if (ra <= 99) {


                let chkradio = (reverseArryradio[i].required == 'on') ? 'checked' : '';

                $('#dynanic_field8').append('<div class="thera' + raf + '"><div class="form-group row " style="background-color:#bce0f8"><label class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class="btn btn-default  btn_remov2ra " id="ra' + raf + '" style="color:red" ><i></i>Remove</button></span></div></div> <div class="checkbox"  style="margin-left:11px" style="margin-left:11px"><input type="checkbox"  ' + chkradio + ' name="radiorequired[]" id="radiorequired_' + raf + '"><label for="radiorequired_' + raf + '">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ></div></div><div id="dyn_fied1ra' + raf + '" ><div class="form-group row" ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default " onclick="myFunction1ra(' + raf + ')"  ><i ></i>Add More</button></span></div> </div></div></div>');


                if (reverseArryradio[i].options.length > 2) {

                  for (let iu = 0; iu < reverseArryradio[i].options.length - 2; iu++) {

                    $('#dyn_fied1ra' + raf).append('<div class="form-group row " id="yyra' + q1ra + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + raf + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstryra(' + q1ra + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
                    q1ra++;


                  }


                }

                let lent = $('input[name="radio' + i + '[]"]');
                lent[0].value = convert(radio[i].fieldName);
                // console.log(checkbox[i].options.length);
                for (let k = 0; k < radio[i].options.length; k++) {
                  // console.log(checkbox[i].options[k]);

                  lent[k + 1].value = radio[i].options[k];
                  // lent[k+1].value=convert(dropdown[i].options[k]);

                }



                ra++;
                raf++;

              }



            }

            window['ra'] = ra;

          }



          // console.log(len[1].value='bis1');


        } else {


          $("#selemp8 > div").remove();

          $('#selemp8').append(' <div class="form-group row " style="display: none;" ><div class="box box-solid bg-pale-secondary"><div class="box-header text-center"><h5 class="box-title text-dark">Radio Field</h5></div><div class="box-body"  id="dynanic_field8"><div><div class="form-group row " style="background-color:#bce0f8"><label  class="col-sm-4 col-form-label">Question<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "> <button type="button" class="btn btn-default " onclick="adde8()" ><i title="Add More Question" ></i>Add More</button></span></div></div> <div class="checkbox"><input type="checkbox" checked name="radiorequired[]" id="radiorequired_0" ><label for="radiorequired_0">Make field compulsory</label></div>  <div class="form-group row"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ></div></div><div id="dyn_fied10ra"><div class="form-group row " ><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="letstry1ra()" ><i></i>Add More</button></span></div></div></div></div></div></div></div>');

          if ($('#radio').is(":checked")) {

            // alert("dfdf");
            $('#radio').prop('checked', false);
          }



        }










      }

    }












    // console.log(se);
















    function myFunction(num) {
      var uuid = Math.random().toString(36).substring(2) +
        (new Date()).getTime().toString(36);

      $('#dyn_field' + num + '').append('<div class="form-group row fee' + uuid + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown' + num + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default btn_remove27 "  id="' + uuid + '" style="color:red">Remove</button></span> </div></div>');
    }


    function myFunction1(num) {
      var uuid = Math.random().toString(36).substring(2) +
        (new Date()).getTime().toString(36);

      $('#dyn_fied1' + num + '').append('<div class="form-group row fees1' + uuid + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox' + num + '[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default btn_remove271 "  id="' + uuid + '" style="color:red">Remove</button></span> </div></div>');
    }

    function myFunction1ra(num) {
      var uuid = Math.random().toString(36).substring(2) +
        (new Date()).getTime().toString(36);

      $('#dyn_fied1ra' + num + '').append('<div class="form-group row fees1ra' + uuid + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio' + num + '[]" ><span class="input-group-btn "><button type="button" class="  btn btn-default btn_remove271ra "  id="' + uuid + '" style="color:red">Remove</button></span> </div></div>');
    }

    function letstry() {
      $('#dyn_field0').append('<div class="form-group row " id="yy' + q + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="dropdown0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry(' + q + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
      q++;

    }

    function letstry1() {
      $('#dyn_fied10').append('<div class="form-group row " id="xy1' + q1 + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="checkbox0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry1(' + q1 + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
      q1++;

    }

    function letstry1ra() {
      $('#dyn_fied10ra').append('<div class="form-group row " id="xy1ra' + q1ra + '"><label  class="col-sm-4 col-form-label">option<span style="color: red">*</span></label><div class="col-sm-8 input-group input-group-sm"><input class="form-control" type="text" value="" name="radio0[]" ><span class="input-group-btn "><button type="button" class=" add3 btn btn-default" onclick="rletstry1ra(' + q1ra + ')" style="color:red"  ><i></i>Remove</button></span></div></div>');
      q1ra++;

    }

    function rletstry(q) {
      // alert ($(this));
      $('#yy' + q + '').remove();
    }

    function rletstry1(q1) {
      // alert ($(this));
      $('#xy1' + q1 + '').remove();
    }

    function rletstryra(q1ra) {
      // alert ($(this));
      $('#yyra' + q1ra + '').remove();
    }



    $(document).on('click', '.btn_remove27', function() {

      var button_id = $(this).attr("id");
      // alert(button_id);
      $('.fee' + button_id + '').remove();

    });
    $(document).on('click', '.btn_remove271', function() {

      var button_id = $(this).attr("id");
      // alert(button_id);
      $('.fees1' + button_id + '').remove();

    });

    $(document).on('click', '.btn_remove271ra', function() {

      var button_id = $(this).attr("id");
      // alert(button_id);
      $('.fees1ra' + button_id + '').remove();

    });
  </script>



  <!-- SlimScroll -->
  <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>


  <!-- FastClick -->
  <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

  <!-- MinimalLite Admin App -->
  <script src="../assets/js/template.js"></script>

  <!-- MinimalLite Admin for demo purposes -->
  <script src="../assets/js/demo.js"></script>

  <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
  <script>
    $(function() {
      "use strict";

      //Initialize Select2 Elements
      $('.select2').select2();


    });
  </script>

</body>

</html>
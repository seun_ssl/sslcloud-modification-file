<?php
session_start();

include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = false;include '../css/myscripts.php';
if (ValidateURths("EVENTS MANAGER" . "V") != true) {include '../main/NoAccess.php';exit;}
$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

if (isset($_POST['Send'])) {
    if (isset($_POST['title']) && !empty($_POST['title'])
        && isset($_POST['message']) && !empty($_POST['message'])
        && isset($_POST['sendDate']) && !empty($_POST['sendDate'])
        && (@count($_POST['select-LD']) != 0 || !empty($_POST['sendToAll']))
    ) {
        $title = ECh(trim($_POST['title']));
        $sendTo = @$_POST['select-LD'];
        $arraySendTo = json_encode($sendTo);
        $message = ECh(trim($_POST['message']));
        $sendDate = date('Y-m-d', strtotime(trim($_POST['sendDate'])));
        $eventDate = date('Y-m-d', strtotime(trim($_POST['eventDate'])));
        $interval = trim($_POST['interval']);
        if (isset($_POST['sendToAll'])) {$sendToAll = "on";} else { $sendToAll = "";}

        // Date conversion format
        $date = date('Y-m-d', strtotime(date('y-m-d')));
        $sendDate = date('Y-m-d', strtotime($sendDate));

        // To send if today's date is equal to send date
        if ($date == $sendDate) {

            // To all Employees
            if ($sendToAll == 'on') {

                include '../login/dbOpen4.php';
                while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                    $To = $row3['Email'];
                    QueueMail($To, $title, $message, "", "");
                }
                include '../login/dbClose4.php';
                // Status S means mail sent

                $status = 'S';
                $updated_date = isset($interval) && !empty($interval) ? date("Y-m-d", strtotime("{$interval} days", strtotime("$sendDate"))) : "GetDate()";
                $Script = "INSERT INTO [dbo].[Notify] (title, body, sendTo, sendDate, eventDate, interval, [status], updatedDate, sendToAll)
            VALUES ('" . $title . "', '" . $message . "','" . $arraySendTo . "','" . $sendDate . "', '" . $eventDate . "',
            '" . $interval . "', '" . $status . "', '" . $updated_date . "', '" . $sendToAll . "')";
                ScriptRunnerUD($Script, "Inst");

                // To selected employees
            } else {
                // Status S means mail sent
                $status = 'S';
                $updated_date = isset($interval) && !empty($interval) ? date("Y-m-d", strtotime("{$interval} days", strtotime("$sendDate"))) : "GetDate()";
                // Send mail
                MyQueueMail($arraySendTo, $title, $message, "", "");

                $Script = "INSERT INTO [dbo].[Notify] (title, body, sendTo, sendDate, eventDate, interval, [status], updatedDate, sendToAll)
            VALUES ('" . $title . "', '" . $message . "','" . $arraySendTo . "','" . $sendDate . "', '" . $eventDate . "',
            '" . $interval . "', '" . $status . "', '" . $updated_date . "', '" . $sendToAll . "')";
                ScriptRunnerUD($Script, "Inst");
            }

        } else {
            $status = 'N';
            $updated_date = isset($interval) && !empty($interval) ? date("Y-m-d", strtotime("{$interval} days", strtotime("$sendDate"))) : "GetDate()";
            $Script = "INSERT INTO [dbo].[Notify] (title, body, sendTo, sendDate, eventDate, interval, [status], updatedDate, sendToAll)
        VALUES ('" . $title . "', '" . $message . "','" . $arraySendTo . "','" . $sendDate . "', '" . $eventDate . "',
        '" . $interval . "', '" . $status . "', '" . $updated_date . "', '" . $sendToAll . "')";
            ScriptRunnerUD($Script, "Inst");
        }

        echo ("<script type='text/javascript'>{ parent.msgbox('Notification successfully created', 'green'); }</script>");
    } else {
        $_SESSION['error'] = "<script type='text/javascript'>{ parent.msgbox('All Fields are required', 'green'); }</script>";
    }
} elseif (isset($_GET['id'])) {
    $id = $_GET['id'];
    $scriptView = "SELECT title, body, sendTo, Convert(varchar, sendDate, 23) sendDate,
    Convert(varchar, eventDate, 23) eventDate, interval, sendToAll FROM Notify WHERE ID = '" . $id . "' ";

    $title = ScriptRunner($scriptView, "title");
    $body = ScriptRunner($scriptView, "body");

    $sendTo = ScriptRunner($scriptView, "sendTo");
    $sendTos = json_decode($sendTo);

    $sendDate = date('m/d/Y', strtotime(ScriptRunner($scriptView, "sendDate")));
    $eventDate = date('m/d/Y', strtotime(ScriptRunner($scriptView, "eventDate")));
    $sendToAll = ScriptRunner($scriptView, "sendToAll");
    $interval = (int) ScriptRunner($scriptView, "interval");

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="../train/select2/dist/css/select2.min.css">
    <link href="../train/css/app.css" rel="stylesheet">
    <!-- Bootstrap core CSS-->
    <link href="../train/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../train/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="../train/css/sb-admin.css" rel="stylesheet">
    <!-- <script src="../train/vendor/jquery/jquery.min.js"></script> -->
    <!-- Select2 -->

     <script type="text/javascript" src="../jqry/tiny_mce/tiny_mce.js"></script>
	    <script type="text/javascript">
            tinyMCE.init({
                // General options
                mode : "textareas",
                theme : "simple",
                plugins : "advhr,advimage,emotions,iespell,visualchars,wordcount",
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,charmap,emotions,iespell,media,advhr,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,pastetext,pasteword,|,undo,redo",
                theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,outdent,indent,blockquote,|,bullist,numlist,",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : false,

                // CSS (should be your site CSS)
                content_css : "../css/sonafem.css"
            });
	    </script>

    <style>
      .select2-container .select2-selection--single {
        height: 38px;
      }
      .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da;
        border-radius: .25rem;
      }
      .select2-container--default .select2-selection--multiple {
        border: 1px solid #ced4da;
        border-radius: .25rem;
      }
      .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 35px;
        color: #495057;
        font-size: 10px;
        padding-left: 12px;
      }
      .select2-container--default .select2-selection--multiple .select2-selection__rendered {
        line-height: 28px;
        color: #495057;
        font-size: 10px;
        /*padding-left: 12px;*/
      }
      .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 36px;
      }
      .select2-container--default .select2-selection--single .select2-selection__arrow b {
        border-color: #495057 transparent transparent transparent;
      }
      .select2-container--default.select2-container--focus .select2-selection--multiple {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0,123,255,.25);
      }
    </style>
  </head>
  <body class="fixed-nav sticky-footer" id="page-top" style="background-color: #fff;">
    <div class="content-wrapper">
      <div class="container-fluid">
        <div class="card mb-3">
          <div class="card-header">
            <center>
            <p><b>Events Manager</b></p>
            </center>
          </div>
          <div class="card-body">
                  <?php if (isset($_SESSION['error'])): ?>
                  <?php echo $_SESSION['error']; ?>
                  <?php endif;
unset($_SESSION['error']);
?>

            <form action="create_events.php" method="post" id="memo_form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label class="text-left">Title</label>
                  <?php if (isset($_GET['id']) && isset($title)) {
    echo '<input type="text" name="title" id="title" class="form-control" value="' . $title . '">';
} else {
    echo ' <input type="text" name="title" id="title" class="form-control">';
}?>
                </div>

                <div class="form-group col-md-4">

                  <label class="text-left">Send To (Select Employee):</label>
                    <select name="select-LD[]" class="form-control select2" multiple="multiple" id="select_ld">
                        <?php
include '../login/dbOpen4.php';

while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>

                        <option value="<?php echo $row3['Email']; ?>"
                        <?php
if (isset($sendTos)) {
    foreach ($sendTos as $sendTo) {
        if ($sendTo == $row3['Email']) {
            echo "selected";
        }
    }
}
?>
                        >  <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] ?></option>
                        <?php endwhile;
include '../login/dbClose4.php';
?>
                    </select>
                </div>

                <div class="form-group col-md-2">
                    <?php if (isset($_GET['id']) && $sendToAll == 'on') {
    echo '<input type="checkbox" name="sendToAll" id="sendToAll" checked>';
} else {
    echo '<input type="checkbox" name="sendToAll" id="sendToAll">';
}?>
                    <label>Send to all</label>
                </div>

              </div>

              <div class="row">
                <div class="form-group col-md-6">
                  <label class="text-left">Compose Message</label>
                  <?php
if (isset($_GET['id']) && isset($body)) {
    echo '<textarea name="message" id="message" cols="155" rows="5" class="form-control">' . $body . '</textarea>';
} else {
    echo '<textarea name="message" id="message" cols="155" rows="5" class="form-control"></textarea>';
}
?>
                </div>

                <div class="form-group col-md-4">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-left">Send Date</label>
                            <?php
if (isset($_GET['id']) && isset($sendDate)) {
    echo '<input type="text" name="sendDate" id="sendDate" class="form-control" value=' . $sendDate . '>';?>
                                <script>
                                $("#sendDate").datepicker({
                                        minDate : 0,
                                        showButtonPanel: true,
                                        dateFormat: "mm/dd/yy",
                                    })
                                </script>
                              <?php } else {
    echo '<input type="text" name="sendDate" id="sendDate" class="form-control">';?>
                                <script>
                                $("#sendDate").datepicker({
                                        minDate : 0,
                                        showButtonPanel: true,
                                        dateFormat: "mm/dd/yy",
                                    })
                                </script>
                              <?php }?>

                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-left">Event Date</label>
                            <?php
if (isset($_GET['id']) && isset($eventDate)) {
    echo '<input type="text" name="eventDate" id="eventDate" class="form-control" value="' . $eventDate . '">';?>
                                <script>
                                $("#eventDate").datepicker({
                                        minDate : 0,
                                        showButtonPanel: true,
                                        dateFormat: "mm/dd/yy",
                                    })
                                </script>
                              <?php } else {
    echo '<input type="text" name="eventDate" id="eventDate" class="form-control">';?>
                                <script>
                                $("#eventDate").datepicker({
                                        minDate : 0,
                                        showButtonPanel: true,
                                        dateFormat: "mm/dd/yy",
                                    })
                                </script>
                              <?php }?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-left">Interval</label>
                            <select class="form-control" name="interval" id="interval">
                                <option value="" selected>--</option>
                                <?php
for ($php_i = 1; $php_i <= 10; $php_i++) {
    if ($interval == $php_i) {
        echo '<option selected=selected value="' . $interval . '">' . $interval . ' days</option>';
    } else {
        echo '<option value="' . $php_i . '">' . $php_i . ' days</option>';
    }
}
;
?>
                            </select>
                        </div>
                    </div>
                </div>

              </div>

              <input type="submit" name="<?php echo (isset($btn_name)) ? "$btn_name" : 'Send'; ?>" value='<?php echo (isset($btn_name)) ? "$btn_name" : 'Send'; ?>' class="btn btn-success btn-lg pull-right" onclick=" save(); return false; ">
            </form>

          </div>
        </div>
      </div>
      <!-- Scripts -->
      <script src="../train/js/app.js"></script>
      <!-- Bootstrap core JavaScript-->
      <script src="../train/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="../train/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="../train/select2/dist/js/select2.full.min.js"></script>
      <!-- Custom scripts for all pages-->
      <!-- Select2 -->
      <script src="../train/js/sb-admin.min.js"></script>
      <script src="../train/js/duration.js"></script>
      <script>
        $(function () {
          //Initialize Select2 Elements
          $('.select2').select2()
        });
        function save() {
          var title = $('#title').val()
          var sendDate = $('#sendDate').val()
          var eventDate = $('#eventDate').val()
          var interval = $('#interval').val();
          var message = $('#message').val();
          var select_ld = $('#select_ld').val();

          if (sendTo == "" || sendToAll == null) {

            parent.msgbox('All Fields are Compulsory', 'green');
          }else{
            document.memo_form.submit();
          }
        }
      </script>
    </div>
  </body>
</html>




            <!-- Main content -->
            <section class="content">

                <div class="box">
              <div class="box-header with-border">
                    <div class="row">

                              <div class="col-md-6  text-md-left text-center">
                                <h3>
								Edit Employee Renewal Letter
                                </h3>

                              </div>

                            </div>
              </div>
              <div class="box-body">

                  <div class="row">


                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-12">
                                  <div class="nav-tabs-custom" style="min-height: 400px">
                                      <ul class="nav nav-tabs">
                                          <li >
                                              <a class="active" href="#book"  data-toggle="tab">
                                                   NON-MEDICAL STAFF
                                              </a>
                                          </li>

										  <li>
                                              <a href="#book2" data-toggle="tab">
                                                    MEDICAL STAFF
                                              </a>
                                          </li>

										  <li>
                                              <a href="#book5"  data-toggle="tab">
                                              EXPATRIATE STAFF
                                              </a>
                                          </li>

                                      </ul>
                                      <div class="tab-content">
										  <div class="tab-pane active " id="book">
										
											  <div id="invoice" class="text-dark ">

									<div class="editorContainer">
										<form action=" " method="POST">
										<textarea id="editor" name="editor" cols="80" rows="10" >
										<?php

										$sqlE = "SELECT * FROM Settings where Setting = 'RNWNonAcademic'";
										$resultE = sqlsrv_query($conn2,$sqlE);

										$rowE = sqlsrv_fetch_array($resultE);
											echo $rowE['SetValue21'];
										
									?>

										</textarea>
										<div class="editorContainer">
										<input type="submit" name="editor_submit" value="SUBMIT">
											  </div>
										</form>
											  </div>
									
											   <!-- </div> -->



                                          <!-- /.tab-content -->
                                      </div>
                                      <!-- /.nav-tabs-custom -->


							

                                  </div>
                                  <!-- /.col -->



                                          <div class="tab-pane" id="book2">
										 
										  <div id="invoice2" class="text-dark ">
											   

										  <div class="editorContainer">
										<form action=" " method="POST">
										<textarea id="editorAcademic" name="editorAcademic" cols="80" rows="10" >
											  
										<?php

										$sqlA = "SELECT * FROM Settings where Setting = 'RNWAcademic'";
										$resultA = sqlsrv_query($conn2,$sqlA);

										$rowA = sqlsrv_fetch_array($resultA);
											echo $rowA['SetValue21'];
										
									?>

											</textarea>	
										<div class="editorContainer">
										<input type="submit" name="academic_submit" value="SUBMIT">
											  </div>
										</form>
											  </div>

											   <!-- </div> -->



                                          <!-- /.tab-content -->
                                      </div>
                                      <!-- /.nav-tabs-custom -->
									  <!-- <button class="btn btn-sm btn-danger" id="download2"  onclick="downloadInvoice2()"  >Download</button> -->
								 
                                  </div>
                                  <!-- /.col -->

								  <!-- <div class="tab-content"> -->


								  <div class="tab-pane" id="book3">
    						
										  <div id="invoice3" class="text-dark ">


										  <div class="editorContainer">
										<form action="" method="POST">
										<textarea id="editorAdmin" name="editorAdmin" cols="80" rows="10" >

										<?php

										$sqlS = "SELECT * FROM Settings where Setting = 'RNWAdmin'";
										$resultS = sqlsrv_query($conn2,$sqlS);

										$rowS = sqlsrv_fetch_array($resultS);
											echo $rowS['SetValue21'];

										?>
													</textarea>	
										<div class="editorContainer">
										<input type="submit" name="admin_submit" value="SUBMIT">
											  </div>
										</form>
											  </div>


                                          <!-- /.tab-content -->
                                      </div>
                                      <!-- /.nav-tabs-custom -->




                                  </div>

								  <div class="tab-pane" id="book5">
										 
										 <div id="invoice5" class="text-dark ">
											  

										 <div class="editorContainer">
									   <form action=" " method="POST">
									   <textarea id="editorSuperAdmin" name="editorSuperAdmin" cols="80" rows="10" >
											 
									   <?php

									   $sqlT = "SELECT * FROM Settings where Setting = 'RNWExpatriate'";
									   $resultT = sqlsrv_query($conn2,$sqlT);

									   $resultT = sqlsrv_fetch_array($resultT);
										   echo $resultT['SetValue21'];
									   
								   ?>

										   </textarea>	
									   <div class="editorContainer">
									   <input type="submit" name="super_admin_submit" value="SUBMIT">
											 </div>
									   </form>
											 </div>

											  <!-- </div> -->



										 <!-- /.tab-content -->
									 </div>
									 <!-- /.nav-tabs-custom -->
								  <!-- <button class="btn btn-sm btn-danger" id="download3"  onclick="downloadInvoice3()"  >Download</button> -->
								







                              </div>



                          </div>

                      </div>

                  </div>






              </div>
          </div>
          <!-- /.box-body -->




        </div>

        <!-- /.row -->
        </section>
        <!-- /.content -->
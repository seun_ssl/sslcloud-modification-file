<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';

date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

$connectionInfo2 = array( "Database"=>$_SESSION["StkTck"."myDB"],"UID"=>$_SESSION["StkTck"."myUser"], "PWD"=>$_SESSION["StkTck"."myPass"], "ReturnDatesAsStrings"=>true);
$conn2 = sqlsrv_connect($_SESSION["StkTck"."myServer"], $connectionInfo2);

//Validate user viewing rights
if (ValidateURths("EDIT RESIGNATION LETTER". "V") != true) {include '../main/NoAccess.php';exit;}


if(isset($_POST['editor_submit'])){

    $editorE = $_POST['editor'];
       
        $sqlEdit = "UPDATE Settings SET SetValue20='$editorE' WHERE Setting = 'CertificateOfService'";
        
        $resultEdit = sqlsrv_query($conn2, $sqlEdit);

		if( $resultEdit ){

			echo ("<script type='text/javascript'>{parent.msgbox('Letter has been updated successfully  ', 'red'); }</script>");
			}else{
				echo ("<script type='text/javascript'>{parent.msgbox('Unexpected error occured.Try again  ', 'red'); }</script>");
			}

        
	}

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
		<script>
		$(function()
		{
		<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {} else {?>
		$("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+1D", dateFormat: 'dd M yy',yearRange: "-75:+75"})
		$("#EmpDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "+1M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		<?php }?>
		$("#DOB").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "<?php
$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
if (number_format($kk, 0, '', '') == 0 || $kk == "") {echo "-18Y";} else {echo "-" . $kk . "Y";}?>
		", dateFormat: 'dd M yy', yearRange: "-75:+75"})
		});
		</script>
		<script>
		$(function() {$( "#tabs" ).tabs();});
		$(function() {
			$( document ).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value='';
			});
		});
		</script>
		<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent="+Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent="+Replmt);
			});
		});
		</script>
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
			


		<style>

*{
	font-family: "Times New Roman", Times, serif !important;

	color: black !important;
}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 11px !important;
  /* font-family: "Times New Roman", Times, serif; */
}

body{
	font-size: 16px !important;
}
		.table>tbody>tr>td {
				padding-left: 7px !important;
			}

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: 5px;
    line-height: 20px;
}
.box-body ul li {
    line-height: 18px !important;
}
.card-img {
    width: 76%;
 }

 .nav-tabs-custom>.tab-content {
  color: black !important;
    text-align: justify;
}

.rectangle {
  border: 1px solid black ;
  /* padding: 10px; */
}

img {
  /* width: 100%;
  height: 160px;
  margin-left: 550px; */
}

.editorContainer{
	padding: 20px;
	/*background-color: red !important;*/
}


		</style>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/styles.js"></script>


	</head>


	<body>



            <!-- Main content -->
            <section class="content">

                <div class="box">
              <div class="box-header with-border">
                    <div class="row">

                              <div class="col-md-6  text-md-left text-center">
                                <h3>
								Edit Certificate of Service
                                </h3>

                              </div>

                            </div>
              </div>
              <div class="box-body">

                  <div class="row">


                    <div class="col-md-12">
                        <div class="row">
                          <div class="col-12">
							  						<div id="invoice" class="text-dark ">
																<div class="editorContainer">
																		<form action=" " method="POST">
																				<textarea cols="80" rows="10" name="editor" id="editor">
																				<!-- <div style="text-align: center;"> -->
																					<?php
																					$sqlE = "SELECT * FROM Settings where Setting = 'CertificateOfService'";
																					$resultE = sqlsrv_query($conn2,$sqlE);

																					$rowE = sqlsrv_fetch_array($resultE);
																						echo $rowE['SetValue20'];
																					?>
																				<!-- </div> -->

																			</textarea>
																			
																			<div class="editorContainer">
																				<input type="submit" name="editor_submit" value="SUBMIT">
																			</div>
																		</form>
								                </div>
																  <!-- <button class="btn btn-sm btn-danger" id="download3"  onclick="downloadInvoice3()"  >Download</button> -->
								            </div>
                 		 		</div>

                      </div>

                  </div>


              </div>
          </div>
          <!-- /.box-body -->




        </div>

        <!-- /.row -->
        </section>
        <!-- /.content -->


		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="html2pdf/dist/html2pdf.bundle.js"></script>


		<script>
			CKEDITOR.replace('editor');
			// CKEDITOR.replace('editorAcademic');
			// CKEDITOR.replace('editorAdmin');
		</script>




<script>
    // DecoupledEditor
    //     .create( document.querySelector( '#editor' ) )
    //     .then( editor => {
    //         const toolbarContainer = document.querySelector( '#toolbar-container' );

    //         toolbarContainer.appendChild( editor.ui.view.toolbar.element );
    //     } )
    //     .catch( error => {
    //         console.error( error );
    //     } );

DecoupledEditor
    .create( document.querySelector( '.document-editor__editable' ), {
        cloudServices: {
            // A configuration of CKEditor Cloud Services.
            // ...
        }
    } )
    .then( editor => {
        const toolbarContainer = document.querySelector( '.document-editor__toolbar' );

        toolbarContainer.appendChild( editor.ui.view.toolbar.element );

        window.editor = editor;
    } )
    .catch( err => {
        console.error( err );
    } );
</script>

<script>

// alert(editor);
if(<?=json_encode($_POST['editor_submit'])?>  ){
	// alert('here1');
	activaTab('book');	
}

function activaTab(tab){
	// alert("hereact");
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
</script>



	</body>

	</html>
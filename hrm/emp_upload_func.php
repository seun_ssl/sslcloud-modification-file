<?php
function EChCustom($OldStrVal)
{
    $NewStrVal = trim(str_replace("'", "`", $OldStrVal));
    return $NewStrVal;
}

class Employee
{
    public $arr = [];
    public function setDataIntoArray($emp_id, $mgr)
    {
        $this->arr[$emp_id] = $mgr;
    }
    public function ViewData()
    {
        return $this->arr;
    }
    public function updateData($status)
    {
        if (count($this->arr)) {
            foreach ($this->arr as $key => $value) {
                if (!empty($value) && $value !== '--') {
                    $Script = ($status) ? "SELECT HashKey FROM EmpTbl WHERE Email='" . EChCustom($value) . "'" : "SELECT HashKey FROM EmpTbl WHERE EmpID='" . EChCustom($value) . "'";
                    if (strlen(ScriptRunner($Script, "HashKey")) > 0) {
                        $Script = "UPDATE EmpTbl SET EmpMgr='" . EChCustom(ScriptRunner($Script, "HashKey")) . "' WHERE HashKey='" . EChCustom($key) . "'";
                        ScriptRunnerUD($Script, "Inst");
                        $EmptDtlManager = "UPDATE EmpDtl SET ItemMgr='" . EChCustom(ScriptRunner($Script, "HashKey")) . "' WHERE StaffID='" . EChCustom($key) . "'";
                        ScriptRunnerUD($EmptDtlManager, "Inst");
                    }
                }
            }
        }
    }
}

function getBranch($branch = null)
{
    $Script = "SELECT HashKey FROM [BrhMasters] WHERE OName='" . EChCustom($branch) . "'";
    return EChCustom(ScriptRunner($Script, "HashKey"));
}

function getFormattedDate($date, $value = 'Y-m-d')
{
    if ($date) {
        try {
            $time = strtotime($date);
            $newformat = date($value, $time);
            // $temp = DateTime::createFromFormat('d/m/Y', $date);
            // return $temp->format($value);
            return $newformat;
        } catch (Exception $e) {
            return false;
        }
    }
    return false;
}

function updateEmpCount()
{
    $Script = "UPDATE Settings SET SetValue2=(SetValue2 + 1) WHERE Setting='EmpIDPSCnt'";
    ScriptRunnerUD($Script, "Inst");
}

function createPassword($value = 'secret')
{
    return strtoupper(md5($value));
}

function inValidatePassword($HashKey)
{
    $Script = "Update UsersiP set Status='H' WHERE EmpID='" . $HashKey . "'";
    ScriptRunnerUD($Script, "History");
}

$empClass = new Employee;

function saveUserPassword($HashKey, $NewPassword)
{
    $Script = "Insert into UsersiP ([EmpID],[EmpPw],[Status]) VALUES
	('" . $HashKey . "','" . $NewPassword . "','L')";
    ScriptRunnerUD($Script, "");
}

/**
 * check if records exists in db. Empty array returns true
 * @param Associative array with column as key and data as value
 * @return Boolean or String
 */
function createEmployeeID()
{
    $ScriptEmpIDP = "SELECT * FROM Settings WHERE Setting='AutoEmpID'"; // and Status='A'
    $AutoID = strtolower(ScriptRunner($ScriptEmpIDP, "SetValue"));
    if ($AutoID == "on") {
        $ScriptEmpIDP = "SELECT * FROM Settings WHERE Setting='EmpIDPSCnt'"; // and Status='A'
        $SetVal = ScriptRunner($ScriptEmpIDP, "SetValue");
        $SetVal2 = ScriptRunner($ScriptEmpIDP, "SetValue2") + 1;
        $SetVal3 = ScriptRunner($ScriptEmpIDP, "SetValue3");
        $SetVal4 = ScriptRunner($ScriptEmpIDP, "SetValue4");
        $DigitsDsp_ = ScriptRunner("SELECT * FROM Settings WHERE Setting='AutoEmpID'", "SetValue2");
        if ($DigitsDsp_ == "Long") {
            if (strlen($SetVal2) == 1) {
                $SetVal2 = "000" . $SetVal2;
            } elseif (strlen($SetVal2) == 2) {
                $SetVal2 = "00" . $SetVal2;
            } elseif (strlen($SetVal2) == 3) {
                $SetVal2 = "0" . $SetVal2;
            }
        }
        if (trim($SetVal3) != "") {
            $SetVal5 = $SetVal4;
        } else {
            $SetVal5 = "";
        }
        $LoginDet = $SetVal . $SetVal4 . $SetVal2 . $SetVal5 . $SetVal3;
        $LoginDet = str_replace('#YYYY#', date('Y'), $LoginDet);
        $LoginDet = str_replace('#YY#', date('y'), $LoginDet);
        $LoginDet = str_replace('#MM#', date('m'), $LoginDet);
        $LoginDet = str_replace('#SD#', date('Y'), $LoginDet); //State or District of Origin - CODE
        $LoginDet = str_replace('#CY#', date('Y'), $LoginDet); //Country of Origin - CODE
        $LoginDet = str_replace('#DP#', date('Y'), $LoginDet);
        return $LoginDet;
    } else {
        return false;
    }
}

function createEmployeeWithID($data)
{
    // if (count($data) !== 43) {
    //     return false;
    // }
    global $empClass;
    list($lname, $fname, $oname, $emp_date, $emp_type, $branch, $dept, $manager, $manager_email, $emp_status, $probation, $designation, $lvl, $confirm_status, $title, $gender, $dob, $nationality, $state, $lga, $religion, $marital_status, $email, $mobile, $home_address, $office_address, $blood_grp, $genotype, $ill_health, $disabled, $nok_name, $nok_relationship, $nok_mobile, $nok_address, $bank, $acct_no, $pfa_custodian, $pfa_id, $nhf_id, $hmo_id, $hmo_no, $tax_state, $tax_no, $user_id, $user_grp, $exit_date, $exit_reason, $spouse_hmo_id, $child1_id, $child2_id, $child3_id, $child4_id, $nin, $emp_category) = $data;
    /* Create New Employee ID by selecting last employee ID if set to Automatic */
    $emp_id = createEmployeeID();
    $user_id = preg_replace('/\s+/', '', $user_id);
    if (empty($user_id) || strtoupper($user_id) === 'N/A' || strtoupper($user_id) === 'NIL') {
        $user_id = $emp_id;
    }
    //Check if fields are empty
    if (!$emp_id) {
        return ['status' => false, 'msg' => 'Employee ID could not be created'];
    }

    if (!$emp_id || empty($dob) || empty($email) || empty($emp_date) || empty($emp_status) || empty($gender)) {
        return ['status' => false, 'msg' => 'Date Of Birth, Email, Employed on Date, Employee Status or Gender might be empty.'];
    }

    //Format Date into format, If it can be formatted return false
    if (!$dob = getFormattedDate($dob)) {
        return ['status' => false, 'msg' => 'Invalid Date Of Birth Format'];
    }

    if (!$emp_date = getFormattedDate($emp_date)) {
        return ['status' => false, 'msg' => 'Invalid Employed On Date Format'];
    }

    $checkDept = ScriptRunner("SELECT Val1 FROM Masters where ItemName='Department'  and Status<>'D' and Val1='" . EChCustom($dept) . "'", "Val1");
    if (!$checkDept) {
        return ['status' => false, 'msg' => 'Department Not Found'];
    }

    $dept = $checkDept;
    $checkStatus = ScriptRunner("SELECT Val1 FROM Masters WHERE ItemName='EmployeeStatus' AND Val1='" . EChCustom($emp_status) . "' AND Status<>'D'", "Val1");
    if (!$checkStatus) {
        return ['status' => false, 'msg' => 'Employment Status Not Found'];
    }

    $emp_status = $checkStatus;
    if (!empty($probation)) {
        if (!$probation = getFormattedDate($probation)) {
            return ['status' => false, 'msg' => 'Invalid Probation Date Format'];
        }

        $probation = "'" . EChCustom($probation) . "'";
    } else {
        $probation = 'NULL';
    }
    // Check Exit date
    if (!$exit_date = getFormattedDate($exit_date)) {
        $exit_date = 'NULL';
    } else {
        $exit_date = "'" . EChCustom($exit_date) . "'";
    }
    //Check if EmpID or Email exits
    $Script = "SELECT * FROM EmpTbl WHERE EmpID='" . EChCustom($emp_id) . "' OR Email='" . EChCustom($email) . "'";
    if (strlen(ScriptRunner($Script, "EmpID")) > 0 || strlen(ScriptRunner($Script, "PENCommNo") > 0) || strlen(ScriptRunner($Script, "Email") > 0)) {
        return ['status' => false, 'msg' => 'ID or Email Exists'];
    }

    $EmpDis = (EChCustom($disabled) == 'Yes' || EChCustom($disabled) == 'Y') ? 'on' : "";
    //Check if user name exists
    $Script = "SELECT * FROM Users WHERE LogName='" . $user_id . "' and Status<>'D'";
    if (strlen(ScriptRunner($Script, "LogName")) > 0) {
        return ['status' => false, 'msg' => 'Username Exists'];
    }

    //Check if employee id is already in users table
    $Script = "SELECT EmpID FROM Users WHERE EmpID='" . EChCustom(trim($emp_id)) . "' and Status='A'";
    if (strlen(ScriptRunner($Script, "EmpID")) > 0) {
        return ['status' => false, 'msg' => 'Employee ID assinged to another user'];
    }

    //Check if variable is empty
    $branch = (empty($branch) || strtoupper($branch) === 'N/A') ? '--' : $branch;
    $emp_type = (empty($emp_type) || strtoupper($emp_type) === 'N/A') ? '--' : $emp_type;
    $title = (empty($title) || strtoupper($title) === 'N/A') ? '--' : $title;
    $manager = '--';
    $dept = (empty($dept) || strtoupper($dept) === 'N/A') ? '--' : $dept;
    $nationality = (empty($nationality) || strtoupper($nationality) === 'N/A') ? '--' : $nationality;
    $state = (empty($state) || strtoupper($state) === 'N/A') ? '--' : $state;
    $lga = (empty($lga) || strtoupper($lga) === 'N/A') ? '--' : $lga;
    $marital_status = (empty($marital_status) || strtoupper($marital_status) === 'N/A') ? '--' : $marital_status;
    $bank = (empty($bank) || strtoupper($bank) === 'N/A') ? '--' : $bank;
    $nok_relationship = (empty($nok_relationship) || strtoupper($nok_relationship) === 'N/A') ? '--' : $nok_relationship;
    $religion = (empty($religion) || strtoupper($religion) === 'N/A') ? '--' : $religion;
    $genotype = (empty($genotype) || strtoupper($genotype) === 'N/A') ? '--' : $genotype;
    $blood_grp = (empty($blood_grp) || strtoupper($blood_grp) === 'N/A') ? '--' : $blood_grp;
    $pfa_custodian = (empty($pfa_custodian) || strtoupper($pfa_custodian) === 'N/A') ? '--' : $pfa_custodian;
    $tax_state = (empty($tax_state) || strtoupper($tax_state) === 'N/A') ? '--' : $tax_state;
    $spouse_hmo_id = (empty($spouse_hmo_id) || strtoupper($spouse_hmo_id) === 'N/A') ? '--' : $spouse_hmo_id;
    $child1_id = (empty($child1_id) || strtoupper($child1_id) === 'N/A') ? '--' : $child1_id;
    $child2_id = (empty($child2_id) || strtoupper($child2_id) === 'N/A') ? '--' : $child2_id;
    $child3_id = (empty($child3_id) || strtoupper($child3_id) === 'N/A') ? '--' : $child3_id;
    $child4_id = (empty($child4_id) || strtoupper($child4_id) === 'N/A') ? '--' : $child4_id;
    $nin = (empty($nin) || strtoupper($nin) === 'N/A') ? '--' : $nin;
    $emp_category = (empty($emp_category) || strtoupper($emp_category) === 'N/A') ? '--' : $emp_category;
    //Create employee hashkey
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Employee" . $_SESSION["StkTck" . "UName"] . $email . $emp_id;
    $HashKey = md5($UniqueKey);
    //Save data to employee table
    $Script = "INSERT INTO [EmpTbl] ([EmpID],[EmpMgr],[EmpStatus],[EmpDt],[SName],[FName],[ONames],[DOB],[Sex],[Title],[COO],[SOO],[LGA],[Department],[MStatus],[Relg],[EmpType],[Email],[Mobile],[HmAdd],[OffAdd],[BloodGrp],[GenoType],[EmpDisabled],[KnownIllment],[NOK1Name],[NOK1Rel],[NOK1Add],[NOK1Mobile],[SalBank],[SalAcctNo],[PENCommNo],[PenCustID],[TaxID],[TaxState],[HInsurNo],[MediCareNo],[StfImg],[Status],[AddedBy],[AddedDate],[HashKey],[BranchID],[ExitDt],[EmpIReason],[spouse_hmo_id],[child1_id],[child2_id],[child3_id],[child4_id],[NIN],[EmpCategory]) VALUES ('" . trim(EChCustom($emp_id)) . "','" . trim(EChCustom($manager)) . "','" . trim(EChCustom($emp_status)) . "','" . $emp_date . "','" . EChCustom(strtoupper($lname)) . "','" . EChCustom(trim($fname)) . "','" . EChCustom(trim($oname)) . "','" . $dob . "','" . EchCustom($gender) . "','" . EChCustom($title) . "','" . EChCustom($nationality) . "','" . EChCustom($state) . "','" . EChCustom($lga) . "','" . EChCustom($dept) . "','" . EChCustom($marital_status) . "','" . EChCustom($religion) . "','" . EChCustom($emp_type) . "','" . EChCustom(trim($email)) . "','" . EChCustom(trim($mobile)) . "','" . EChCustom($home_address) . "','" . EChCustom($office_address) . "','" . EChCustom($blood_grp) . "','" . EChCustom($genotype) . "','" . $EmpDis . "','" . EChCustom($ill_health) . "','" . EChCustom($nok_name) . "','" . EChCustom($nok_relationship) . "','" . EChCustom($nok_address) . "','" . EChCustom($nok_mobile) . "','" . EChCustom(trim($bank)) . "','" . EChCustom(trim($acct_no)) . "','" . EChCustom($pfa_id) . "','" . EChCustom($pfa_custodian) . "','" . EChCustom($tax_no) . "','" . EChCustom($tax_state) . "','" . EChCustom($nhf_id) . "','" . EChCustom($hmo_no) . "','pfimg/Default.jpg','N','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . getBranch($branch) . "'," . $exit_date . ",
	'" . EChCustom($exit_reason) . "',
	'" . EChCustom($spouse_hmo_id) . "',
	'" . EChCustom($child1_id) . "',
	'" . EChCustom($child2_id) . "',
	'" . EChCustom($child3_id) . "',
	'" . EChCustom($child4_id) . "',
	'" . EChCustom($nin) . "',
	'" . EChCustom($emp_category) . "'

	)";
    ScriptRunnerUD($Script, "Inst");
    //Update Employee Count
    updateEmpCount();
    //Save to users table
    $Script = "SELECT HashKey FROM [UGpRights] WHERE GpName='" . EChCustom(trim($user_grp)) . "'";
    $Script = "INSERT INTO [Users] ([LogName],[GpName],[AcctState],[PassDate],[RPwd],[ULock],[LogCnt],[LStatus],[EmpID],[Status],[AddedBy],[AddedDate],[HashKey],[CustID]) VALUES ('" . EChCustom($user_id) . "','" . ScriptRunner($Script, "HashKey") . "',1,'01-Jan-1900',1,0,0,'A','" . $emp_id . "','A','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . $_SESSION["StkTck" . "CustID"] . "')";
    ScriptRunnerUD($Script, "Inst");
    //Create new user password
    $NewPassword = createPassword($user_id);
    //Render all old passwords invalid
    inValidatePassword($HashKey);
    //Save password to table
    saveUserPassword($HashKey, $NewPassword);
    //Save Employee History
    $Script = "SELECT SetValue FROM Settings WHERE Setting='CompName'";
    $ItemName = ScriptRunner($Script, "SetValue");
    $hash = md5(date('l jS \of F Y h:i:s A') . gettimeofday(true) . "EmpWork" . $_SESSION["StkTck" . "UName"]);
    $Script = "INSERT INTO [EmpDtl] ([StaffID],[ItemCode],[ItemType],[ItemName],[RemindEmp],[ItemLevel],[ItemPos],[ItemStart],[ItemExpiry],[Status],[AddedBy],[AddedDate],[HashKey],[ItemStatus]) VALUES ('" . EChCustom($HashKey) . "','EMP','" . EChCustom($emp_type) . "','" . EChCustom($ItemName) . "',0,'" . EChCustom($lvl) . "','" . EChCustom($designation) . "','" . $emp_date . "'," . $probation . ",'N','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $hash . "','" . $confirm_status . "')";

    ScriptRunnerUD($Script, "New_Item");
    $empClass->setDataIntoArray($HashKey, $manager_email);
    return ['status' => true, 'msg' => 'Success'];
}

function createEmployeeNoID($data)
{
    // if (count($data) !== 44) {
    //     return false;
    // }
    global $empClass;
    list($emp_code, $lname, $fname, $oname, $emp_date, $emp_type, $branch, $dept, $manager, $manager_email, $emp_status, $probation, $designation, $lvl, $confirm_status, $title, $gender, $dob, $nationality, $state, $lga, $religion, $marital_status, $email, $mobile, $home_address, $office_address, $blood_grp, $genotype, $ill_health, $disabled, $nok_name, $nok_relationship, $nok_mobile, $nok_address, $bank, $acct_no, $pfa_custodian, $pfa_id, $nhf_id, $hmo_id, $hmo_no, $tax_state, $tax_no, $user_id, $user_grp, $exit_date, $exit_reason, $spouse_hmo_id, $child1_id, $child2_id, $child3_id, $child4_id, $nin, $emp_category) = $data;
    if (empty($emp_code)) {
        return ['status' => false, 'msg' => 'Employee ID is required'];
    }
    $emp_id = EChCustom(preg_replace('/\s+/', '', $emp_code));
    $user_id = preg_replace('/\s+/', '', $user_id);
    if (empty($user_id) || strtoupper($user_id) === 'N/A' || strtoupper($user_id) === 'NIL') {
        $user_id = $emp_id;
    }
    //Check if fields are empty
    if (!$emp_id || empty($dob) || empty($emp_date) || empty($emp_status) || empty($gender)) {
        return ['status' => false, 'msg' => 'Date Of Birth, Employed on Date, Employee Status or Gender might be empty.'];
    }

    //Format Date into format, If it can be formatted return false
    if (!$dob = getFormattedDate($dob)) {
        return ['status' => false, 'msg' => 'Invalid Date Of Birth Format'];
    }

    if (!$emp_date = getFormattedDate($emp_date)) {
        return ['status' => false, 'msg' => 'Invalid Employed On Date Format'];
    }

    $checkDept = ScriptRunner("SELECT Val1 FROM Masters where ItemName='Department'  and Status<>'D' and Val1='" . EChCustom($dept) . "'", "Val1");
    if (!$checkDept) {
        return ['status' => false, 'msg' => 'Department Not Found'];
    }

    $manager = (empty($manager)) ? '--' : $manager;
    $dept = $checkDept;
    $checkStatus = ScriptRunner("SELECT Val1 FROM Masters WHERE ItemName='EmployeeStatus' AND Val1='" . EChCustom($emp_status) . "' AND Status<>'D'", "Val1");
    if (!$checkStatus) {
        return ['status' => false, 'msg' => 'Employment Status Not Found'];
    }

    $emp_status = $checkStatus;
    // die;
    if (!empty($probation)) {
        if (!$probation = getFormattedDate($probation)) {
            return ['status' => false, 'msg' => 'Invalid Probation Date Format'];
        }

        $probation = "'" . EChCustom($probation) . "'";
    } else {
        $probation = 'NULL';
    }
    // Check Exit date
    if (!$exit_date = getFormattedDate($exit_date)) {
        $exit_date = 'NULL';
    } else {
        $exit_date = "'" . EChCustom($exit_date) . "'";
    }
    //Check if EmpID or email exits
    $Script = "SELECT * FROM EmpTbl WHERE EmpID='" . EChCustom($emp_id) . "'";
    if (strlen(ScriptRunner($Script, "EmpID")) > 0) {
        return ['status' => false, 'msg' => 'ID Exists'];
    }

    if (!empty($email)) {
        $Script = "SELECT * FROM EmpTbl WHERE Email='" . EChCustom($email) . "'";
        if (strlen(ScriptRunner($Script, "EmpID")) > 0) {
            return ['status' => false, 'msg' => 'Email Exists'];
        }

    }
    $EmpDis = (EChCustom($disabled) == 'Yes' || EChCustom($disabled) == 'Y') ? 'on' : "";
    //Check if user name exists
    $Script = "SELECT * FROM Users WHERE LogName='" . $user_id . "' and Status<>'D'";
    if (strlen(ScriptRunner($Script, "LogName")) > 0) {
        return ['status' => false, 'msg' => 'Username Exists'];
    }

    //Check if employee id is already in users table
    $Script = "SELECT EmpID FROM Users WHERE EmpID='" . EChCustom(trim($emp_id)) . "' and Status='A'";
    if (strlen(ScriptRunner($Script, "EmpID")) > 0) {
        return ['status' => false, 'msg' => 'Employee ID assinged to another user'];
    }

    //Check if variable is empty
    $branch = (empty($branch) || strtoupper($branch) === 'N/A') ? '--' : $branch;
    $emp_type = (empty($emp_type) || strtoupper($emp_type) === 'N/A') ? '--' : $emp_type;
    $title = (empty($title) || strtoupper($title) === 'N/A') ? '--' : $title;
    $dept = (empty($dept) || strtoupper($dept) === 'N/A') ? '--' : $dept;
    $nationality = (empty($nationality) || strtoupper($nationality) === 'N/A') ? '--' : $nationality;
    $state = (empty($state) || strtoupper($state) === 'N/A') ? '--' : $state;
    $lga = (empty($lga) || strtoupper($lga) === 'N/A') ? '--' : $lga;
    $marital_status = (empty($marital_status) || strtoupper($marital_status) === 'N/A') ? '--' : $marital_status;
    $bank = (empty($bank) || strtoupper($bank) === 'N/A') ? '--' : $bank;
    $nok_relationship = (empty($nok_relationship) || strtoupper($nok_relationship) === 'N/A') ? '--' : $nok_relationship;
    $religion = (empty($religion) || strtoupper($religion) === 'N/A') ? '--' : $religion;
    $genotype = (empty($genotype) || strtoupper($genotype) === 'N/A') ? '--' : $genotype;
    $blood_grp = (empty($blood_grp) || strtoupper($blood_grp) === 'N/A') ? '--' : $blood_grp;
    $pfa_custodian = (empty($pfa_custodian) || strtoupper($pfa_custodian) === 'N/A') ? '--' : $pfa_custodian;
    $tax_state = (empty($tax_state) || strtoupper($tax_state) === 'N/A') ? '--' : $tax_state;
    $spouse_hmo_id = (empty($spouse_hmo_id) || strtoupper($spouse_hmo_id) === 'N/A') ? '--' : $spouse_hmo_id;
    $child1_id = (empty($child1_id) || strtoupper($child1_id) === 'N/A') ? '--' : $child1_id;
    $child2_id = (empty($child2_id) || strtoupper($child2_id) === 'N/A') ? '--' : $child2_id;
    $child3_id = (empty($child3_id) || strtoupper($child3_id) === 'N/A') ? '--' : $child3_id;
    $child4_id = (empty($child4_id) || strtoupper($child4_id) === 'N/A') ? '--' : $child4_id;
    $nin = (empty($nin) || strtoupper($nin) === 'N/A') ? '--' : $nin;

    //Create employee hashkey
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Employee" . $_SESSION["StkTck" . "UName"] . $email . $emp_id;
    $HashKey = md5($UniqueKey);
    //Save data to employee table
    $Script = "INSERT INTO [EmpTbl] ([EmpID],[EmpMgr],[EmpStatus],[EmpDt],[SName],[FName],[ONames],[DOB],[Sex],[Title],[COO],[SOO],[LGA],[Department],[MStatus],[Relg],[EmpType],[Email],[Mobile],[HmAdd],[OffAdd],[BloodGrp],[GenoType],[EmpDisabled],[KnownIllment],[NOK1Name],[NOK1Rel],[NOK1Add],[NOK1Mobile],[SalBank],[SalAcctNo],[PENCommNo],[PenCustID],[TaxID],[TaxState],[HInsurNo],[MediCareNo],[StfImg],[Status],[AddedBy],[AddedDate],[HashKey],[BranchID],[ExitDt],[EmpIReason],[spouse_hmo_id],[child1_id],[child2_id],[child3_id],[child4_id],[NIN],[EmpCategory]) VALUES ('" . trim(EChCustom($emp_id)) . "','" . trim(EChCustom($manager)) . "','" . trim(EChCustom($emp_status)) . "','" . $emp_date . "','" . EChCustom(strtoupper($lname)) . "','" . EChCustom(trim($fname)) . "','" . EChCustom(trim($oname)) . "','" . $dob . "','" . EChCustom($gender) . "','" . EChCustom($title) . "','" . EChCustom($nationality) . "','" . EChCustom($state) . "','" . EChCustom($lga) . "','" . EChCustom($dept) . "','" . EChCustom($marital_status) . "','" . EChCustom($religion) . "','" . EChCustom($emp_type) . "','" . EChCustom(trim($email)) . "','" . EChCustom(trim($mobile)) . "','" . EChCustom($home_address) . "','" . EChCustom($office_address) . "','" . EChCustom($blood_grp) . "','" . EChCustom($genotype) . "','" . $EmpDis . "','" . EChCustom($ill_health) . "','" . EChCustom($nok_name) . "','" . EChCustom($nok_relationship) . "','" . EChCustom($nok_address) . "','" . EChCustom($nok_mobile) . "','" . EChCustom(trim($bank)) . "','" . EChCustom(trim($acct_no)) . "','" . EChCustom($pfa_id) . "','" . EChCustom($pfa_custodian) . "','" . EChCustom($tax_no) . "','" . EChCustom($tax_state) . "','" . EChCustom($nhf_id) . "','" . EChCustom($hmo_no) . "','pfimg/Default.jpg','N','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . getBranch($branch) . "'," . $exit_date . ",
	'" . EChCustom($exit_reason) . "',
	'" . EChCustom($spouse_hmo_id) . "',
	'" . EChCustom($child1_id) . "',
	'" . EChCustom($child2_id) . "',
	'" . EChCustom($child3_id) . "',
	'" . EChCustom($child4_id) . "',
	'" . EChCustom($nin) . "',
	'" . EChCustom($emp_category) . "'

	)";
    ScriptRunnerUD($Script, "Inst");
    //Update Employee Count
    updateEmpCount();
    //Save to users table
    $Script = "SELECT HashKey FROM [UGpRights] WHERE GpName='" . EChCustom(trim($user_grp)) . "'";
    $Script = "INSERT INTO [Users] ([LogName],[GpName],[AcctState],[PassDate],[RPwd],[ULock],[LogCnt],[LStatus],[EmpID],[Status],[AddedBy],[AddedDate],[HashKey],[CustID]) VALUES ('" . EChCustom($user_id) . "','" . ScriptRunner($Script, "HashKey") . "',1,'01-Jan-1900',1,0,0,'A','" . $emp_id . "','A','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . $_SESSION["StkTck" . "CustID"] . "')";
    ScriptRunnerUD($Script, "Inst");
    //Create new user password
    $NewPassword = createPassword($user_id);
    //Render all old passwords invalid
    inValidatePassword($HashKey);
    //Save password to table
    saveUserPassword($HashKey, $NewPassword);
    //Save Employee History
    $Script = "SELECT SetValue FROM Settings WHERE Setting='CompName'";
    $ItemName = ScriptRunner($Script, "SetValue");
    $hash = md5(date('l jS \of F Y h:i:s A') . gettimeofday(true) . "EmpWork" . $_SESSION["StkTck" . "UName"] . $HashKey);
    $Script = "INSERT INTO [EmpDtl] ([StaffID],[ItemCode],[ItemType],[ItemName],[RemindEmp],[ItemLevel],[ItemPos],[ItemStart],[ItemExpiry],[Status],[AddedBy],[AddedDate],[HashKey],[ItemStatus]) VALUES ('" . EChCustom($HashKey) . "','EMP','" . EChCustom($emp_type) . "','" . EChCustom($ItemName) . "',0,'" . EChCustom($lvl) . "','" . EChCustom($designation) . "','" . $emp_date . "'," . $probation . ",'N','" . EChCustom($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $hash . "','" . $confirm_status . "')";
    ScriptRunnerUD($Script, "New_Item");
    $empClass->setDataIntoArray($HashKey, $manager);
    return ['status' => true, 'msg' => 'Success'];
}

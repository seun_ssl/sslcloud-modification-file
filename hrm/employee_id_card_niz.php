<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("ID CARD" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);
$emp_details = null;
if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";

	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
	// var_dump($emp_details['sig_img']);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	// var_dump($comp_details);

}

?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: 5px;
			line-height: 20px;
		}

		.box-body ul li {
			line-height: 18px !important;
		}

		.list-group-item {
			border: thin solid #fff;
			/* color: #002e69; */
			color: #001a3c;
			font-weight: 700;
			margin-bottom: 1px;
			/* border-radius: 5px !important; */
			border-bottom: 1px solid #001a3c !important;
		}


		element.style {}

		.list-group-flush:first-child .list-group-item:first-child {
			border-top: 0;
		}

		.list-group-flush .list-group-item {
			border-right: 0;
			border-left: 0;
			border-radius: 0;
			/* border-bottom: 1px solid #001a3c */
			;
		}

		/* .list-group-item:first-child {
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
} */
		.box-body ul li {
			line-height: 18px !important;
		}

		.box-body ul li {
			line-height: 24px;
		}

		/* .list-group-item {
    border: 1px solid rgb(0 35 102);
    color: #000;
    font-weight: 700;
} */
		.py-0 {
			padding-top: 2px !important;
			padding-bottom: 2px !important;
		}

		.card-img {
			border-radius: 8px;
			border: 1px solid #000;
		}
	</style>

</head>


<body>






	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Generate Employee ID Card
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control select2" name="bookselect1" id="bookselect1">
										<!-- <select name="level1" id="level1"  class="form-control" > -->
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>


									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">



				<?php if (!is_null($emp_details) && !empty($emp_details)) : ?>
					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<div class="nav-tabs-custom" style="min-height: 400px">
										<ul class="nav nav-tabs">
											<li>
												<a class="active" href="#book" data-toggle="tab">
													<?php
													// var_dump($emp_details);
													echo ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}") ?>
												</a>
											</li>



										</ul>
										<div class="tab-content">

											<div class="tab-pane active " id="book">
												<div id="invoice" style="display: flex; flex-direction: column; gap: 5px">
													<div class="" style="
          max-width: 302.36px;
          max-height: 188.97px;
          width: 302.36px;
          height: 188.97px;
          font-weight: 550;
          font-size: 9.5px;
          border-radius: 0px;
          border: 1px solid #2b4b98;
          display: flex;
          gap: 0px;
          flex-direction: column;
          margin: auto;
        ">
														<!-- section one wrapper -->
														<div class="" style="
            display: flex;
            height: 65%;
            color: #2b4b98;
            padding-top: 6px;
            padding-right: 6px;
          ">
															<!-- section one left -->
															<div style="
              display: flex;
              flex-direction: column;
              height: 100%;
              width: 70%;
            ">
																<!-- logo and id -->
																<div style="
                display: flex;
                justify-content: space-around;
                align-items: center;
                width: 100%;
                height: 30%;
              ">
																	<!-- logo -->
																	<div style="
                  width: 90px;
                  height: 100%;
                  display: flex;
                  align-items: center;
                ">
																		<!-- <img src="../pfimg/nizamiyelogo.png" alt="" style="width: 100%" /> -->
																		<img src="<?= $ComLogo ?>" alt="" style="width: 84px;height: 41px;" />
																	</div>
																	<!-- id -->
																	<p style="margin: 0; font-size: 13px; height: fit-content; color: #2b4b98">
																		<?= $emp_details['EmpID']; ?>
																	</p>
																</div>
																<!-- <img src="../pfimg/niz_wave.jpeg" alt="" style="width: 100%" /> -->
																<!-- pattern -->
																<div style="height: 45%; display:flex;"><img src="../pfimg/niz_wave.jpeg" alt="" style="width: 100%" /></div>

																<!-- department -->
																<p style="
                height: 25%;
                font-size: 12px;
                background-color: #5eba49;
                color: white;
                display: flex;
                align-items: center;
                padding-left: 5px;
                margin: 0;
              ">
																	<svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" viewBox="0 0 24 24">
																		<path fill-rule="evenodd" d="M10.271 5.575C8.967 4.501 7 5.43 7 7.12v9.762c0 1.69 1.967 2.618 3.271 1.544l5.927-4.881a2 2 0 0 0 0-3.088l-5.927-4.88Z" clip-rule="evenodd" />
																	</svg>

																	<span style="font-weight: 200"><?php echo strtoupper("{$emp_details['Department']}") ?></span>
																</p>
															</div>

															<!-- avatar -->

															<div style="
              width: 30%;
              height: 100%;
              background-color: #2b4b98;
              border-top-left-radius: 8px;
              border-top-right-radius: 8px;
              color: white;
              padding: 4px;
              display: flex;
              flex-direction: column;
              align-items: center;
              gap: 3px;
            ">
																<p style="margin: 0">STAFF ID CARD</p>
																<div style="width: 90px;">
																	<img src="../<?= $emp_details['StfImg'] ?>" alt="" style="width: 100%; height: 100%; object-fit: cover;max-height:95px;" />
																</div>
															</div>
														</div>

														<!-- section two wrapper -->
														<div style="
            color: white;
            height: 35%;
            display: flex;
            flex-direction: column;
          ">
															<!-- name and position -->
															<p style="
              margin: 0;
              height: 50%;
              background: rgb(47, 79, 161);
              background: linear-gradient(
                90deg,
                rgba(47, 79, 161, 1) 0%,
                rgba(43, 75, 152, 1) 86%
              );
              display: flex;
              align-items: center;
              padding-left: 10px;
              font-size: 12px;
              font-weight: 700;
            ">
																<?php $sumName = "{$emp_details['Title']}. {$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']} ";
																?>
																<?php echo strtoupper($sumName) ?>
															</p>
															<p style="
              margin: 0;
              color: #5eba49;
              height: 50%;
              display: flex;
              align-items: center;
              padding-left: 10px;
              font-size: 12px;
            ">
																<?php

																$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

																$sqR = ScriptRunnercous($sq);

																$slen = strlen($sqR['ItemPos']);

																?>

																<?php echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');
																?>
																</span></p>
														</div>
													</div>


													<!-- ID Card back -->
													<div style="
														max-width: 302.36px;
														max-height: 188.97px;
														width: 302.36px;
														height: 178.97px;
														margin: auto;
														font-size: 10px;
														display: flex;
														flex-direction: column;
														gap: 0px;
														">
														<!-- first half -->
														<div style="height: 94.485px">
															<div style="
																display: flex;
																flex-direction: column;
																gap: 5px;
																padding: 5px;
																color: #2b4b98;
																">
																<p style="margin: 0; text-align: center; font-size: 10px">
																	This is to certify that the bearer whose name, signature and
																	photograph appear overleaf is a Staff in this Hospital:
																</p>
																<p style="margin: 0; text-align: center; font-size: 11px">
																	<?php
																	// Get the current date
																	$currentDate = new DateTime();

																	// Add 2 years to the current date
																	$currentDate->add(new DateInterval('P2Y'));

																	// Format the date as per your requirement (optional)
																	$expiryDate = $currentDate->format('j F,Y');
																	?>
																	<strong style="font-weight: bold;"> Nizamiye Hospital LTD -Valid till: </strong><?= $expiryDate ?>
																</p>
															</div>
															<div style="
																height: 35px;
																display: flex;
																justify-content: center;
																align-items: center;
																text-align: center;
																padding-left: 5px;
																padding-right: 5px;
																background: rgb(94, 186, 73);
																background: linear-gradient(
																	90deg,
																	rgba(94, 186, 73, 1) 0%,
																	rgba(43, 75, 152, 1) 86%
																);
																color: white;
																font-size: 10px;
																padding: 5px;
																">
																If found, please return to the address below or to the nearest
																police station
															</div>
														</div>

														<!-- second half -->
														<div style="
																height: 94.485px;
																color: #2b4b98;
																display: flex;
																flex-direction: column;
																gap: 5px;
															">
															<!-- authorized signature -->
															<div style="
																display: flex;
																justify-content: center;
																align-items: center;
																gap: 10px;
																height: fit-content;
																overflow: hidden;
																padding-top: 5px;
																">
																<p style="
																	margin: 0;
																	text-align: center;
																	font-weight: bold;
																	font-size: 11px;
																">
																	Authorized <br />
																	Signature:
																</p>
																<p style="margin: 0; height: 40px; width: auto; overflow: hidden;">
																	<img src="../pfimg/auth_sig_niz.png" alt="" style="height: 100%; width: 100%" />
																</p>
															</div>

															<!-- address -->
															<div style="display: flex; flex-direction: column; gap: 3px; font-size: 9px;">
																<p style="margin: 0; text-align: center">
																	<strong style="font-weight: bold;">Address:</strong> Plot 113, Sector S, Cadastral Zone Life
																	Camp Abuja
																</p>
																<p style="margin: 0; text-align: center">
																	<strong style="font-weight: bold;">Call:</strong> +234 816 666 60 23 / +234 805 633 94 44 /
																	www.nizamiye.ng
																</p>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
										<button class="btn btn-sm btn-danger" id="download">Download</button>













										<!-- /.tab-content -->
									</div>
									<!-- /.nav-tabs-custom -->
								</div>
								<!-- /.col -->
							</div>
						</div>













					</div>











			</div>

		<?php endif; ?>






















		</div>
		</div>
		<!-- /.box-body -->




		</div>

















		<!-- /.row -->
	</section>
	<!-- /.content -->












	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="html2pdf/dist/html2pdf.bundle.js"></script>


	<script>
		window.onload = function() {
			document.getElementById("download").addEventListener("click", () => {
				const invoice = document.getElementById("invoice");
				const opt = {
					html2canvas: {
						scale: 4
					},
					margin: [0, 0, 0, 0],
					image: {
						type: 'jpeg',
						quality: 1
					},
					jsPDF: {
						unit: 'cm',
						format: [8, 5],
						orientation: 'l'
					}
				};

				html2pdf().from(invoice).set(opt).save();
			})
		}
	</script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<script>
		$('.select2').select2();
	</script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->


	<script>
		$('#example1').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example6').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example7').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		setInterval(function() {
			$(".tablesorter-filter-row").remove();
		}, 2000);

		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});













































		}

		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select an employee', 'red');
				};

				return;

			} else {

				$('#theform1').submit();






			}



		});
	</script>
</body>

</html>
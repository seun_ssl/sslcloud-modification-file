<?php
session_start();
include '../login/scriptrunner.php';
$template_def = tem();
$applist = function ($value, $key, $te) {
	if (array_key_exists($value, $te)) {
		$value = (is_array($te[$value])) ? implode('   ', $te[$value]) : $te[$value];
		echo "<td align='left' valign='middle' scope='col'>$value</td>";
	} else {
		echo "<td align='left' valign='middle' scope='col'></td>";
	}
};

$repalce = function ($a) {
	$a = str_replace(" ", "_", $a);
	return $a;
};

if (isset($_POST['selected']) && $_POST['type'] === 'decline') {
	$jobId = sanitize($_POST['jobId']);
	$fieldArry = $_POST['fieldArry'];
	$allId = $_POST['selected'];
	$allId = json_decode($allId, true);
	$allId = array_map(function ($a) {
		return (int) $a;
	}, $allId);
	foreach ($allId as $key => $value) {
		$quey = "SELECT * FROM [recjobs] WHERE [id] = '" . $value . "'";
		$getApplicant = ScriptRunnercous($quey);
		$applicantJobId = $getApplicant['job_id'];
		$applicantName = $getApplicant['last_name'] . " " . $getApplicant['first_name'] . " " . $getApplicant['middle_name'];
		$applicantEmail = $getApplicant['email'];
		$quey1 = "SELECT * FROM [jobs] WHERE [id] = '" . $applicantJobId . "'";
		$getApplicant1 = ScriptRunnercous($quey1);
		$applicantJobTitle = $getApplicant1['title'];

		// send mail
		DeclineMailToApplicant('DELCINE', 'APPLICANT', $applicantName, $applicantEmail, $applicantJobTitle);
		$Script = "UPDATE [recjobs] SET [status] = 'D' WHERE [id] ='$value'";

		ScriptRunnerUD($Script, "Inst");
	}

	$query = "SELECT * FROM recjobs Where [job_id]='" . $jobId . "' AND [status] is NUll ";
	$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
	if ($conn2 === false) {

		echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
	}
	$res2 = sqlsrv_query($conn2, $query);
	$Del = 0;
	while ($row2 = sqlsrv_fetch_array($res2, SQLSRV_FETCH_BOTH)) :
		$Del = $Del + 1;
		ob_start();
?>
		<tr>
			<td width="20" height="20" valign="middle" scope="col" class="TinyTextTight">
				<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['id']); ?>" />
				<label for="<?php echo ("DelBox" . $Del); ?>"></label>
			</td>
			<td align="left" valign="middle" scope="col"><?php echo (trim($row2['first_name'])); ?> <?php echo (trim($row2['middle_name'])); ?> <?php echo (trim($row2['last_name'])); ?></td>

			<td align="left" valign="middle" scope="col"><?php echo trim($row2['email']); ?></td>
			<td align="left" valign="middle" scope="col"> <a href="../public/<?php echo trim($row2['cv']); ?>" target="_blank">view</a> </td>

			<?php
			$te = json_decode($row2['other_info'], true);
			if (array_key_exists('job_id', $te)) {
				unset($te['job_id']);
			}

			if (array_key_exists('cust_id', $te)) {
				unset($te['cust_id']);
			}

			if (isset($te) && !empty($te)) {
				$fieldArry = array_map('strtolower', $fieldArry);
				$fieldArry = array_map($repalce, $fieldArry);

				array_walk($fieldArry, $applist, $te);
			}

			?>


		</tr>

	<?php endwhile;
	echo ob_get_clean();
	return false;
}

if ($_POST['type'] === 'resend') {

	$selected = json_decode($_POST['selected'], true);

	// $value= $_POST['id'];
	$custId = $_SESSION['StkTckCustID'];

	foreach ($selected as $value) {
		# code...

		$quey = "SELECT * FROM [recjobs] WHERE [id] = '" . $value . "'";
		$getApplicant = ScriptRunnercous($quey);
		$applicantJobId = $getApplicant['job_id'];
		$applicantName = $getApplicant['last_name'] . " " . $getApplicant['first_name'] . " " . $getApplicant['middle_name'];
		$applicantEmail = $getApplicant['email'];
		$assessId = $getApplicant['assess_id'];
		$stDt = $getApplicant['start_date']->format('Y-m-d');
		$enDt = $getApplicant['end_date']->format('Y-m-d') ? $getApplicant['end_date']->format('Y-m-d') : $stDt;
		$quey1 = "SELECT * FROM [jobs] WHERE [id] = '" . $applicantJobId . "'";
		$getApplicant1 = ScriptRunnercous($quey1);
		$applicantJobTitle = $getApplicant1['title'];
		$link = "https://" . $_SERVER['HTTP_HOST'] . "/public/exam.php?customer=$custId&exam_id=$assessId&email=$applicantEmail";

		// send mail
		AssessmentMailToApplicant('ASSESS', 'APPLICANT', $applicantName, $applicantEmail, $applicantJobTitle, $stDt, $enDt, $link);
	}

	echo "OK";
}

if (isset($_POST['selected']) && $_POST['type'] === 'invite') {

	$jobId = sanitize($_POST['jobId']);
	$fieldArry = $_POST['fieldArry'];
	$stDt = sanitize($_POST['stDt']);
	$enDt = sanitize($_POST['enDt']);
	$assessId = sanitize($_POST['assessId']);
	$custId = $_SESSION['StkTckCustID'];
	$allId = $_POST['selected'];
	$allId = json_decode($allId, true);
	$allId = array_map(function ($a) {
		return (int) $a;
	}, $allId);
	foreach ($allId as $key => $value) {
		$quey = "SELECT * FROM [recjobs] WHERE [id] = '" . $value . "'";
		$getApplicant = ScriptRunnercous($quey);
		$applicantJobId = $getApplicant['job_id'];
		$applicantName = $getApplicant['last_name'] . " " . $getApplicant['first_name'] . " " . $getApplicant['middle_name'];
		$applicantEmail = $getApplicant['email'];
		$quey1 = "SELECT * FROM [jobs] WHERE [id] = '" . $applicantJobId . "'";
		$getApplicant1 = ScriptRunnercous($quey1);
		$applicantJobTitle = $getApplicant1['title'];
		$link = "https://" . $_SERVER['HTTP_HOST'] . "/public/exam.php?customer=$custId&exam_id=$assessId&email=$applicantEmail&job_id=$jobId";

		// send mail
		AssessmentMailToApplicant('ASSESS', 'APPLICANT', $applicantName, $applicantEmail, $applicantJobTitle, $stDt, $enDt, $link);
		$Script = "UPDATE [recjobs] SET [assess_id] = '$assessId', [start_date] ='$stDt',[end_date]='$enDt', [status] = 'A' WHERE [id] ='$value'";

		ScriptRunnerUD($Script, "Inst");
	}
	$query = "SELECT * FROM recjobs Where [job_id]='" . $jobId . "' AND [status] is NUll ";
	$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
	$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
	if ($conn2 === false) {

		echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
	}
	$res2 = sqlsrv_query($conn2, $query);
	$Del = 0;
	while ($row2 = sqlsrv_fetch_array($res2, SQLSRV_FETCH_BOTH)) :
		$Del = $Del + 1;
		ob_start();
	?>
		<tr>
			<td width="20" height="20" valign="middle" scope="col" class="TinyTextTight">
				<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['id']); ?>" />
				<label for="<?php echo ("DelBox" . $Del); ?>"></label>
			</td>
			<td align="left" valign="middle" scope="col"><?php echo (trim($row2['first_name'])); ?> <?php echo (trim($row2['middle_name'])); ?> <?php echo (trim($row2['last_name'])); ?></td>

			<td align="left" valign="middle" scope="col"><?php echo trim($row2['email']); ?></td>
			<td align="left" valign="middle" scope="col"> <a href="../public/<?php echo trim($row2['cv']); ?>" target="_blank">view</a> </td>

			<?php
			$te = json_decode($row2['other_info'], true);
			if (array_key_exists('job_id', $te)) {
				unset($te['job_id']);
			}

			if (array_key_exists('cust_id', $te)) {
				unset($te['cust_id']);
			}

			if (isset($te) && !empty($te)) {
				$fieldArry = array_map('strtolower', $fieldArry);
				$fieldArry = array_map($repalce, $fieldArry);

				array_walk($fieldArry, $applist, $te);
			}

			?>


		</tr>

	<?php endwhile;
	echo ob_get_clean();
	return false;
}

if (isset($_POST['total']) && !empty($_POST['total']) && $_POST['type'] === 'add') {

	$query = "SELECT * from [formtemplate] where  id='1'";

	$getcheck = ScriptRunnercous($query);


	if ($getcheck) {

		$groupsarry = json_decode($getcheck['template_name'], true);

		$incomingarry = json_decode($_POST['total'], true);

		foreach ($groupsarry as $arrt) {

			if ($arrt['templateName'] == $incomingarry[0]['templateName']) {

				echo "Template Name already exist";
				return false;
			}
		}

		$allInfo = $incomingarry[0];


		// var_dump($allInfo, $groupsarry);
		// die();

		ob_start();

	?>

		<?php if ($allInfo['shortanswer'] != null) : ?>

			<?php foreach ($allInfo['shortanswer'] as $shortanswer) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($shortanswer['fieldName']) ?>" value="" class="form-control" <?php echo ($shortanswer['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>
		<?php if ($allInfo['shortanswer_com'] != null) : ?>

			<?php foreach ($allInfo['shortanswer_com'] as $shortanswer_com) : ?>
				<div class="col-md-12">
					<div class="form-group row font-weight-bold font-italic mb-0 ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer_com['fieldName']) ?> <?= ($shortanswer_com['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> </label>

					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>


		<?php if ($allInfo['url'] != null) : ?>

			<?php foreach ($allInfo['url'] as $url) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($url['fieldName']) ?> <?= ($url['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($url['fieldName']) ?>" placeholder="Type URL here.." value="" class="form-control url" <?php echo ($url['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['numeric'] != null) : ?>

			<?php foreach ($allInfo['numeric'] as $numeric) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($numeric['fieldName']) ?> <?= ($numeric['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="number" name="<?= strtolower($numeric['fieldName']) ?>""  class=" form-control" placeholder="0.00" pattern="^\d*(\.\d{0,4})?$" <?= ($numeric['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['date'] != null) : ?>

			<?php foreach ($allInfo['date'] as $date) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($date['fieldName']) ?><?= ($date['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="date" name="<?= strtolower($date['fieldName']) ?>" class="form-control" <?= ($date['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['file'] != null) : ?>

			<?php foreach ($allInfo['file'] as $file) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($file['fieldName']) ?><?= ($file['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="file" name="<?= strtolower($file['fieldName']) ?>" id="<?= strtolower($file['fieldName']) ?>" class=" form-control" <?= ($file['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['paragraph'] != null) : ?>

			<?php foreach ($allInfo['paragraph'] as $paragraph) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($paragraph['fieldName']) ?> <?= ($paragraph['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="2" name="<?= strtolower($paragraph['fieldName']) ?>" <?= ($paragraph['required'] == 'on') ? 'required' : ''; ?>></textarea>
						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['dropdown'] != null) : ?>

			<?php foreach ($allInfo['dropdown'] as $dropdown) : ?>
				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($dropdown['fieldName']) ?> <?= ($dropdown['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>

						<div class="input-group input-group-sm col-sm-12 ">
							<select class="form-control" name="<?= strtolower($dropdown['fieldName']) ?>" <?= ($dropdown['required'] == 'on') ? 'required' : ''; ?>>
								<option value="">--Select--</option>
								<?php for ($i = 0; $i < count($dropdown['options']); $i++) : ?>

									<option value="<?= strtolower($dropdown['options'][$i]) ?>"><?= $dropdown['options'][$i] ?></option>

								<?php endfor; ?>
							</select>

						</div>

					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['checkbox'] != null) : ?>



			<?php foreach ($allInfo['checkbox'] as $checkbox) : ?>


				<div class="col-md-12">


					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($checkbox['fieldName']) ?> <?= ($checkbox['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($checkbox['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="checkbox">
											<input type="checkbox" class="form-control" value="<?= strtolower($checkbox['options'][$i]) ?>" name="<?= strtolower($checkbox['fieldName']) ?>[]" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($checkbox['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>




					</div>

				</div>






			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['radio'] != null) : ?>



			<?php foreach ($allInfo['radio'] as $radio) : ?>


				<div class="col-md-12">


					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($radio['fieldName']) ?> <?= ($radio['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($radio['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="radio">
											<input type="radio" class="form-control" value="<?= strtolower($radio['options'][$i]) ?>" name="<?= strtolower($radio['fieldName']) ?>" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($radio['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>




					</div>

				</div>






			<?php endforeach; ?>



		<?php endif; ?>






















		<?php

		if (isset($allInfo['def_temp_name'])) {
			$content = ob_get_clean();

			$content = $template_def[$allInfo['def_temp_name']];
		} else {
			$content = ob_get_clean();
		}

		$allInfo['template'] = $content;

		$groupsarry[] = $allInfo;
		// var_dump($groupsarry);
		$str = json_encode($groupsarry);

		$Script = "UPDATE [formtemplate] SET [template_name] = '" . $str . "' WHERE [id] ='1'";
		ScriptRunnerUD($Script, "Inst");
		echo "OK";
	} else {

		// var_dump($_POST['total']);
		// die();

		$incomingarry = json_decode($_POST['total'], true);

		$allInfo = $incomingarry[0];

		ob_start();

		?>

		<?php if ($allInfo['shortanswer'] != null) : ?>

			<?php foreach ($allInfo['shortanswer'] as $shortanswer) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($shortanswer['fieldName']) ?>" value="" class="form-control" <?php echo ($shortanswer['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>
		<?php if ($allInfo['shortanswer_com'] != null) : ?>

			<?php foreach ($allInfo['shortanswer_com'] as $shortanswer) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer_com['fieldName']) ?> <?= ($shortanswer_com['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['url'] != null) : ?>

			<?php foreach ($allInfo['url'] as $url) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($url['fieldName']) ?> <?= ($url['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($url['fieldName']) ?>" placeholder="Type URL here.." value="" class="form-control url" <?php echo ($url['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['numeric'] != null) : ?>

			<?php foreach ($allInfo['numeric'] as $numeric) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($numeric['fieldName']) ?> <?= ($numeric['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="number" name="<?= strtolower($numeric['fieldName']) ?>""  class=" form-control" placeholder="0.00" pattern="^\d*(\.\d{0,4})?$" <?= ($numeric['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['date'] != null) : ?>

			<?php foreach ($allInfo['date'] as $date) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($date['fieldName']) ?><?= ($date['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="date" name="<?= strtolower($date['fieldName']) ?>""  class=" form-control" <?= ($date['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>


		<?php if ($allInfo['file'] != null) : ?>

			<?php foreach ($allInfo['file'] as $file) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($file['fieldName']) ?><?= ($file['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="file" name="<?= strtolower($file['fieldName']) ?>" id=" <?= strtolower($file['fieldName']) ?>" class=" form-control" <?= ($file['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>






		<?php if ($allInfo['paragraph'] != null) : ?>

			<?php foreach ($allInfo['paragraph'] as $paragraph) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($paragraph['fieldName']) ?> <?= ($paragraph['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="2" name="<?= strtolower($paragraph['fieldName']) ?>" <?= ($paragraph['required'] == 'on') ? 'required' : ''; ?>></textarea>
						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['dropdown'] != null) : ?>

			<?php foreach ($allInfo['dropdown'] as $dropdown) : ?>


				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($dropdown['fieldName']) ?> <?= ($dropdown['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>

						<div class="input-group input-group-sm col-sm-12 " style=" padding-right: 35px">
							<select class="form-control" name="<?= strtolower($dropdown['fieldName']) ?>" <?= ($dropdown['required'] == 'on') ? 'required' : ''; ?>>
								<option value="">--Select--</option>
								<?php for ($i = 0; $i < count($dropdown['options']); $i++) : ?>

									<option value="<?= strtolower($dropdown['options'][$i]) ?>"><?= $dropdown['options'][$i] ?></option>

								<?php endfor; ?>
							</select>

						</div>

					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['checkbox'] != null) : ?>



			<?php foreach ($allInfo['checkbox'] as $checkbox) : ?>



				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($checkbox['fieldName']) ?> <?= ($checkbox['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>


						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($checkbox['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="checkbox">
											<input type="checkbox" class="form-control" value="<?= strtolower($checkbox['options'][$i]) ?>" name="<?= strtolower($checkbox['fieldName']) ?>[]" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($checkbox['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>



					</div>
				</div>






			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['radio'] != null) : ?>



			<?php foreach ($allInfo['radio'] as $radio) : ?>


				<div class="col-md-12">


					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($radio['fieldName']) ?> <?= ($radio['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($radio['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="radio">
											<input type="radio" class="form-control" value="<?= strtolower($radio['options'][$i]) ?>" name="<?= strtolower($radio['fieldName']) ?>" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($radio['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>




					</div>

				</div>






			<?php endforeach; ?>



		<?php endif; ?>








		<?php
		if (isset($allInfo['def_temp_name'])) {
			$content = ob_get_clean();

			$content = $template_def[$allInfo['def_temp_name']];
		} else {
			$content = ob_get_clean();
		}

		$allInfo['template'] = $content;

		$groupsarry[] = $allInfo;
		// var_dump($groupsarry);
		$str = json_encode($groupsarry);

		$Script = "INSERT INTO [formtemplate]([template_name]) VALUES ('" . $str . "')";
		$qrr = ScriptRunnerUD($Script, "Inst");

		echo "OK";
	}
} else if (isset($_POST['total']) && !empty($_POST['total']) && $_POST['type'] === 'saveAs') {

	$theindex = null;

	$query = "SELECT * from [formtemplate] where  id='1'";

	$getcheck = ScriptRunnercous($query);
	// var_dump($getcheck);
	// die();

	if (count($getcheck)) {

		$groupsarry = json_decode($getcheck['template_name'], true);
		// $group=$groupsarry[0];
		$incomingarry = json_decode($_POST['total'], true);

		foreach ($groupsarry as $arrt) {

			if ($arrt['templateName'] == $incomingarry[0]['templateName']) {

				echo "Template Name already exist";
				return false;
			}
		}

		$allInfo = $incomingarry[0];

		ob_start();

		?>

		<?php if ($allInfo['shortanswer'] != null) : ?>

			<?php foreach ($allInfo['shortanswer'] as $shortanswer) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($shortanswer['fieldName']) ?>" value="" class="form-control" <?php echo ($shortanswer['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>
		<?php if ($allInfo['shortanswer_com'] != null) : ?>

			<?php foreach ($allInfo['shortanswer_com'] as $shortanswer) : ?>

				<div class="col-md-12">
					<div class="form-group row  font-weight-bold font-italic mb-0">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> </label>

					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['url'] != null) : ?>

			<?php foreach ($allInfo['url'] as $url) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($url['fieldName']) ?> <?= ($url['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($url['fieldName']) ?>" placeholder="Type URL here.." value="" class="form-control url" <?php echo ($url['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['numeric'] != null) : ?>

			<?php foreach ($allInfo['numeric'] as $numeric) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($numeric['fieldName']) ?> <?= ($numeric['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="number" name="<?= strtolower($numeric['fieldName']) ?>""  class=" form-control" placeholder="0.00" pattern="^\d*(\.\d{0,4})?$" <?= ($numeric['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['date'] != null) : ?>

			<?php foreach ($allInfo['date'] as $date) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($date['fieldName']) ?><?= ($date['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="date" name="<?= strtolower($date['fieldName']) ?>""  class=" form-control" <?= ($date['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['file'] != null) : ?>

			<?php foreach ($allInfo['file'] as $file) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($file['fieldName']) ?><?= ($file['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="file" name="<?= strtolower($file['fieldName']) ?>" id=" <?= strtolower($file['fieldName']) ?>" class=" form-control" <?= ($file['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>






		<?php if ($allInfo['paragraph'] != null) : ?>

			<?php foreach ($allInfo['paragraph'] as $paragraph) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($paragraph['fieldName']) ?> <?= ($paragraph['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="2" name="<?= strtolower($paragraph['fieldName']) ?>" <?= ($paragraph['required'] == 'on') ? 'required' : ''; ?>></textarea>
						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['dropdown'] != null) : ?>

			<?php foreach ($allInfo['dropdown'] as $dropdown) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($dropdown['fieldName']) ?> <?= ($dropdown['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>

						<div class="input-group input-group-sm col-sm-12 ">
							<select class="form-control" name="<?= strtolower($dropdown['fieldName']) ?>" <?= ($dropdown['required'] == 'on') ? 'required' : ''; ?>>
								<option value="">--Select--</option>
								<?php for ($i = 0; $i < count($dropdown['options']); $i++) : ?>

									<option value="<?= strtolower($dropdown['options'][$i]) ?>"><?= $dropdown['options'][$i] ?></option>

								<?php endfor; ?>
							</select>

						</div>

					</div>
				</div>
			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['checkbox'] != null) : ?>



			<?php foreach ($allInfo['checkbox'] as $checkbox) : ?>




				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($checkbox['fieldName']) ?> <?= ($checkbox['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>


						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($checkbox['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="checkbox">
											<input type="checkbox" class="form-control" value="<?= strtolower($checkbox['options'][$i]) ?>" name="<?= strtolower($checkbox['fieldName']) ?>[]" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($checkbox['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>



					</div>

				</div>




			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['radio'] != null) : ?>



			<?php foreach ($allInfo['radio'] as $radio) : ?>


				<div class="col-md-12">


					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($radio['fieldName']) ?> <?= ($radio['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($radio['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="radio">
											<input type="radio" class="form-control" value="<?= strtolower($radio['options'][$i]) ?>" name="<?= strtolower($radio['fieldName']) ?>" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($radio['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>




					</div>

				</div>






			<?php endforeach; ?>



		<?php endif; ?>








		<?php

		if (isset($allInfo['def_temp_name'])) {
			$content = ob_get_clean();

			$content = $template_def[$allInfo['def_temp_name']];
		} else {
			$content = ob_get_clean();
		}

		$allInfo['template'] = $content;

		$groupsarry[] = $allInfo;

		$str = json_encode($groupsarry);

		$Script = "UPDATE [formtemplate] SET [template_name] = '" . $str . "' WHERE [id] ='1'";
		ScriptRunnerUD($Script, "Inst");

		echo "OK";
	}
} else if (isset($_POST['total']) && !empty($_POST['total']) && $_POST['type'] === 'edit') {

	$theindex = null;

	$query = "SELECT * from [formtemplate] where  id='1'";

	$getcheck = ScriptRunnercous($query);
	// var_dump($getcheck);
	// die();

	if (count($getcheck)) {

		$groupsarry = json_decode($getcheck['template_name'], true);
		// $group=$groupsarry[0];
		$incomingarry = json_decode($_POST['total'], true);

		$allInfo = $incomingarry[0];

		ob_start();

		?>

		<?php if ($allInfo['shortanswer'] != null) : ?>

			<?php foreach ($allInfo['shortanswer'] as $shortanswer) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($shortanswer['fieldName']) ?>" value="" class="form-control" <?php echo ($shortanswer['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['shortanswer_com'] != null) : ?>

			<?php foreach ($allInfo['shortanswer_com'] as $shortanswer) : ?>

				<div class="col-md-12">
					<div class="form-group row font-weight-bold font-italic mb-0">
						<label class="col-sm-12 col-form-label"><?= ucwords($shortanswer['fieldName']) ?> <?= ($shortanswer['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> </label>

					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['url'] != null) : ?>

			<?php foreach ($allInfo['url'] as $url) : ?>
				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($url['fieldName']) ?> <?= ($url['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="text" name="<?= strtolower($url['fieldName']) ?>" placeholder="Type URL here.." value="" class="form-control url" <?php echo ($url['required'] == 'on') ? 'required' : '' ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['numeric'] != null) : ?>

			<?php foreach ($allInfo['numeric'] as $numeric) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($numeric['fieldName']) ?> <?= ($numeric['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="number" name="<?= strtolower($numeric['fieldName']) ?>""  class=" form-control" placeholder="0.00" pattern="^\d*(\.\d{0,4})?$" <?= ($numeric['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['date'] != null) : ?>

			<?php foreach ($allInfo['date'] as $date) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($date['fieldName']) ?><?= ($date['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="date" name="<?= strtolower($date['fieldName']) ?>""  class=" form-control" <?= ($date['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>


		<?php if ($allInfo['file'] != null) : ?>

			<?php foreach ($allInfo['file'] as $file) : ?>

				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($file['fieldName']) ?><?= ($file['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12 input-group input-group-sm">
							<input type="file" name="<?= strtolower($file['fieldName']) ?>" id=" <?= strtolower($file['fieldName']) ?>" class=" form-control" <?= ($file['required'] == 'on') ? 'required' : ''; ?>>

						</div>
					</div>

				</div>

			<?php endforeach; ?>



		<?php endif; ?>




		<?php if ($allInfo['paragraph'] != null) : ?>

			<?php foreach ($allInfo['paragraph'] as $paragraph) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($paragraph['fieldName']) ?> <?= ($paragraph['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="2" name="<?= strtolower($paragraph['fieldName']) ?>" <?= ($paragraph['required'] == 'on') ? 'required' : ''; ?>></textarea>
						</div>
					</div>
				</div>

			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['dropdown'] != null) : ?>

			<?php foreach ($allInfo['dropdown'] as $dropdown) : ?>

				<div class="col-md-12">
					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($dropdown['fieldName']) ?> <?= ($dropdown['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?>:</label>

						<div class="input-group input-group-sm col-sm-12 ">
							<select class="form-control" name="<?= strtolower($dropdown['fieldName']) ?>" <?= ($dropdown['required'] == 'on') ? 'required' : ''; ?>>
								<option value="">--Select--</option>
								<?php for ($i = 0; $i < count($dropdown['options']); $i++) : ?>

									<option value="<?= strtolower($dropdown['options'][$i]) ?>"><?= $dropdown['options'][$i] ?></option>

								<?php endfor; ?>
							</select>

						</div>

					</div>
				</div>
			<?php endforeach; ?>



		<?php endif; ?>





		<?php if ($allInfo['checkbox'] != null) : ?>



			<?php foreach ($allInfo['checkbox'] as $checkbox) : ?>




				<div class="col-md-12">

					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($checkbox['fieldName']) ?> <?= ($checkbox['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>


						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($checkbox['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="checkbox">
											<input type="checkbox" class="form-control" value="<?= strtolower($checkbox['options'][$i]) ?>" name="<?= strtolower($checkbox['fieldName']) ?>[]" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($checkbox['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>



					</div>

				</div>




			<?php endforeach; ?>



		<?php endif; ?>



		<?php if ($allInfo['radio'] != null) : ?>



			<?php foreach ($allInfo['radio'] as $radio) : ?>


				<div class="col-md-12">


					<div class="form-group row ">
						<label class="col-sm-12 col-form-label"><?= ucwords($radio['fieldName']) ?> <?= ($radio['required'] == 'on') ? '<span style="color: red">*</span>' : '' ?> :</label>

						<div class="col-sm-12">

							<div class="row">

								<?php for ($i = 0; $i < count($radio['options']); $i++) :
									$uni = md5(uniqid(rand(), true)); ?>
									<div class="col-sm-4">
										<div class="radio">
											<input type="radio" class="form-control" value="<?= strtolower($radio['options'][$i]) ?>" name="<?= strtolower($radio['fieldName']) ?>" id="<?= $uni ?>">
											<label for="<?= $uni ?>"><?= ucwords($radio['options'][$i]) ?></label>
										</div>
									</div>

								<?php endfor; ?>

							</div>

						</div>




					</div>

				</div>






			<?php endforeach; ?>



		<?php endif; ?>








<?php

		if (isset($allInfo['def_temp_name'])) {
			$content = ob_get_clean();

			$content = $template_def[$allInfo['def_temp_name']];
		} else {
			$content = ob_get_clean();
		}

		$allInfo['template'] = $content;

		foreach ($groupsarry as $key => $value) {

			if ($value['id'] == $allInfo['id']) {

				// var_dump('fsfsfsf');
				// die();

				$theindex = $key;

				break;
			}
		}
		// var_dump($groupsarry[$theindex]);
		// var_dump($allInfo['id']);

		// $tr=explode('<div class="form-group row ">', preg_replace('/\s+/', ' ',$groupsarry[$theindex]['template']));
		// $trnew=explode('<div class="form-group row ">', preg_replace('/\s+/', ' ',$allInfo['template']));

		// var_dump($tr);
		// var_dump($trnew);
		// var_dump(trim($allInfo['template']));

		// die;

		$groupsarry[$theindex] = $allInfo;

		$str = json_encode($groupsarry);

		$Script = "UPDATE [formtemplate] SET [template_name] = '" . $str . "' WHERE [id] ='1'";
		ScriptRunnerUD($Script, "Inst");

		echo "OK";
	}
} else if (isset($_POST['total']) && !empty($_POST['total']) && $_POST['type'] === 'delete') {
	$theindex = null;

	$incom = json_decode($_POST['total'], true);
	$checkid = $incom[0]['id'];

	// $query="SELECT * from [bkmitems] where [select_group] ='$checkid'";

	// // var_dump($query);

	// $getcheck=ScriptRunnercous($query);

	// if(count($getcheck)){

	//     echo "TIED";

	//     return false;

	// }

	$query = "SELECT * from [formtemplate] where  id='1'";

	$getcheck = ScriptRunnercous($query);
	// var_dump($getcheck);
	// die();

	if (count($getcheck)) {

		$groupsarry = json_decode($getcheck['template_name'], true);
		// $group=$groupsarry[0];
		$incomingarry = json_decode($_POST['total'], true);

		// var_dump($groupsarry,$incomingarry);
		// die();

		foreach ($groupsarry as $key => $value) {

			if ($value['id'] == $incomingarry[0]['id']) {

				// var_dump('fsfsfsf');
				// die();

				$theindex = $key;

				break;
			}
		}
		// var_dump($groupsarry);
		// $groupsarry[$theindex]= $incomingarry[0];
		// var_dump($groupsarry);
		// unset($groupsarry[$theindex]);

		array_splice($groupsarry, $theindex, 1);

		// var_dump($groupsarry);

		$str = json_encode($groupsarry);

		// var_dump($str);
		// die();

		$Script = "UPDATE [formtemplate] SET [template_name] = '" . $str . "' WHERE [id] ='1'";
		ScriptRunnerUD($Script, "Inst");

		echo "OK";
	}
} elseif (isset($_POST['allnewcontent']) && !empty($_POST['allnewcontent']) && $_POST['type'] === 'rearrange') {

	$str = $_POST['allnewcontent'];

	$Script = "UPDATE [formtemplate] SET [template_name] = '" . $str . "' WHERE [id] ='1'";
	ScriptRunnerUD($Script, "Inst");

	echo "OK";
}

<?php
////////////////////////////////-=============================================////////////////////////////////
// var_dump($_REQUEST);
if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Employee") {

    $Script_Emp1 = "Select *, (FName+' '+SName) Nm from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
    $EmpName = ScriptRunner($Script_Emp1, "Nm");
    $ToEmp = ScriptRunner($Script_Emp1, "Email");

    //--------------------------- CREATE 1
    $Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
    $creat_user1_id = ScriptRunner($Script_Edit, "SetValue7");
    if (isset($creat_user1_id) && strlen($creat_user1_id) === 32) {
        $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $creat_user1_id . "'";
        $creat_user1_name = ScriptRunner($Script, "Nm");
        $creat_user1_email = ScriptRunner($Script, "Email");

        $Script_MailTemp = "Select MMsg, MSub from MailTemp where MSub='New Employee Created (1)'";

        $LoginDet = ScriptRunner($Script_MailTemp, "MMsg");
        $LoginDet = str_replace('#Recipent_New_Employee_Created1#', $creat_user1_name, $LoginDet);
        $LoginDet = str_replace('#Logged_In_User_Created_Record#', $EmpName, $LoginDet);

        $Atth = "";
        $Bdy = $LoginDet;
        $Subj = ScriptRunner($Script_MailTemp, "MSub");

        //Send Mail
        $Script_Mail1 = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $creat_user1_email . "','" . $Bdy . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
        ScriptRunnerUD($Script_Mail1, "SendLogin");

    }

    //--------------------------- CREATE 2    -----------------------
    $Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
    $creat_user2_id = ScriptRunner($Script_Edit, "SetValue8");
    if (isset($creat_user2_id) && strlen($creat_user2_id) === 32) {
        $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $creat_user2_id . "'";
        $creat_user2_name = ScriptRunner($Script, "Nm");
        $creat_user2_email = ScriptRunner($Script, "Email");

        $Script_MailTemp = "Select MMsg, MSub from MailTemp where MSub='New Employee Created (2)'";

        $LoginDet = ScriptRunner($Script_MailTemp, "MMsg");
        $LoginDet = str_replace('#Recipent_New_Employee_Created2#', $creat_user2_name, $LoginDet);
        $LoginDet = str_replace('#Logged_In_User_Created_Record#', $EmpName, $LoginDet);

        $Atth = "";
        $Bdy = $LoginDet;
        $Subj = ScriptRunner($Script_MailTemp, "MSub");

        //Send Mail
        $Script_Mail1 = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $creat_user2_email . "','" . $Bdy . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
        ScriptRunnerUD($Script_Mail1, "SendLogin");

    }

} elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Record") {

    $Script_Emp1 = "Select *, (FName+' '+SName) Nm from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
    $EmpName = ScriptRunner($Script_Emp1, "Nm");
    $ToEmp = ScriptRunner($Script_Emp1, "Email");

    //--------------------------- UDATE 1
    $Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
    $update_user1_id = ScriptRunner($Script_Edit, "SetValue9");
    if (isset($update_user1_id) && strlen($update_user1_id) === 32) {
        $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames) as Nm, * from [EmpTbl] where HashKey='" . $update_user1_id . "'";
        $update_user1_name = ScriptRunner($Script, "Nm");
        $update_user1_email = ScriptRunner($Script, "Email");

        $Script_MailTemp = "Select MMsg, MSub from MailTemp where MSub='Employee Biodata Updated (1)'";

        $LoginDet = ScriptRunner($Script_MailTemp, "MMsg");
        $LoginDet = str_replace('#Recipent_New_Employee_Updated1#', $update_user1_name, $LoginDet);
        $LoginDet = str_replace('#Logged_In_User_Updated_Record#', $EmpName, $LoginDet);

        $Atth = "";
        $Bdy = $LoginDet;
        $Subj = ScriptRunner($Script_MailTemp, "MSub");

        //Send Mail
        $Script_Mail1 = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $update_user1_email . "','" . $Bdy . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
        ScriptRunnerUD($Script_Mail1, "SendLogin");

    }

    //--------------------------- UPDATE 2    -----------------------
    $Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
    $update_user2_id = ScriptRunner($Script_Edit, "SetValue10");
    if (isset($update_user2_id) && strlen($update_user2_id) === 32) {
        $Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames) as Nm, * from [EmpTbl] where HashKey='" . $update_user2_id . "'";
        $update_user2_name = ScriptRunner($Script, "Nm");
        $update_user2_email = ScriptRunner($Script, "Email");

        $Script_MailTemp = "Select MMsg, MSub from MailTemp where MSub='Employee Biodata Updated (2)'";

        $LoginDet = ScriptRunner($Script_MailTemp, "MMsg");
        $LoginDet = str_replace('#Recipent_New_Employee_Updated2#', $update_user2_name, $LoginDet);
        $LoginDet = str_replace('#Logged_In_User_Updated_Record#', $EmpName, $LoginDet);

        $Atth = "";
        $Bdy = $LoginDet;
        $Subj = ScriptRunner($Script_MailTemp, "MSub");

        //Send Mail
        $Script_Mail1 = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $update_user2_email . "','" . $Bdy . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
        ScriptRunnerUD($Script_Mail1, "SendLogin");
    }

}

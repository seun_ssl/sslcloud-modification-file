<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("REQUEST FORM" . "V") != true) {
  include '../main/NoAccess.php';
  exit;
}

$dbOpen4 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc";

if (isset($_POST) && isset($_POST['request_name'])) {
  $request_subject = ECh($_POST['request_subject']);
  // var_dump($_POST);
  // var_dump($_FILES);

  // die;
  // process file
  $all_file = null;
  if (count($_FILES)) {
    // var_dump("here");
    $all_file = [];

    foreach ($_FILES as $index => $file) {

      // var_dump($index, $file);
      if ($file['name'] !== "") {
        $cust_id = $_SESSION['StkTckCustID'];
        $path = "uploads_files/expense/$cust_id";
        if (!file_exists($path)) {
          mkdir($path, 0777, true);
        }
        $filename = $file["tmp_name"];
        // $name = md5(uniqid(rand(), true)) . "." . $file['type'];
        $ext = end(explode(".", $file['name']));
        $name = md5(uniqid(rand(), true)) . ".$ext";
        // var_dump($name);
        move_uploaded_file($filename, "$path/$name");
        array_push($all_file, [$index => "$path/$name"]);
      }
    }

    $all_file = json_encode($all_file);
  }

  // var_dump($all_file);
  // die;


  $file_1 = '';
  $file_2 = '';
  $file_3 = '';
  $file_4 = '';
  $file_5 = '';
  if ($_FILES['file_1'] &&   $_FILES['file_1']['name'] !== '') {



    $cust_id = $_SESSION['StkTckCustID'];
    $path = "uploads_files/expense/$cust_id";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $filename = $_FILES["file_1"]["tmp_name"];
    $ext = end(explode(".", $_FILES["file_1"]['name']));
    $name = md5(uniqid(rand(), true)) . ".$ext";
    move_uploaded_file($filename, "$path/$name");
    $file_1 = "$path/$name";
  }




  if ($_FILES['file_2'] && $_FILES['file_2']['name'] !== "") {
    $cust_id = $_SESSION['StkTckCustID'];
    $path = "uploads_files/expense/$cust_id";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $filename = $_FILES["file_2"]["tmp_name"];
    $ext = end(explode(".", $_FILES["file_1"]['name']));
    $name = md5(uniqid(rand(), true)) . ".$ext";
    move_uploaded_file($filename, "$path/$name");
    $file_2 = "$path/$name";
  }
  if ($_FILES['file_3'] && $_FILES['file_3']['name'] !== "") {
    $cust_id = $_SESSION['StkTckCustID'];
    $path = "uploads_files/expense/$cust_id";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $filename = $_FILES["file_3"]["tmp_name"];
    $ext = end(explode(".", $_FILES["file_1"]['name']));
    $name = md5(uniqid(rand(), true)) . ".$ext";
    move_uploaded_file($filename, "$path/$name");
    $file_3 = "$path/$name";
  }
  if ($_FILES['file_4'] && $_FILES['file_4']['name'] !== "") {
    $cust_id = $_SESSION['StkTckCustID'];
    $path = "uploads_files/expense/$cust_id";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $filename = $_FILES["file_4"]["tmp_name"];
    $ext = end(explode(".", $_FILES["file_1"]['name']));
    $name = md5(uniqid(rand(), true)) . ".$ext";
    move_uploaded_file($filename, "$path/$name");
    $file_4 = "$path/$name";
  }
  if ($_FILES['file_5'] && $_FILES['file_5']['name'] !== "") {
    $cust_id = $_SESSION['StkTckCustID'];
    $path = "uploads_files/expense/$cust_id";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $filename = $_FILES["file_5"]["tmp_name"];
    $ext = end(explode(".", $_FILES["file_1"]['name']));
    $name = md5(uniqid(rand(), true)) . ".$ext";
    move_uploaded_file($filename, "$path/$name");
    $file_5 = "$path/$name";
  }




  // var_dump($_SESSION);

  $form_dat = ECh($_POST['form_dat']);
  $page_template = ECh($_POST['page_template']);
  $to_backup = ECh($_POST['to_backup']);
  $corres = $_POST['correspondence'];
  $correspondences = json_decode($_POST['correspondence'], true);
  $level1 = "";
  $level2 = "";
  $level3 = "";
  // get first level line manager
  if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'first_sup') {
    $level1 = getLevel1($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'second_sup') {

    $level1 = getLevel2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'third_sup') {
    $level1 = getLevel3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'branch_cont') {
    $level1 = getBranchContact($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'branch_cont2') {
    $level1 = getBranchContact2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'branch_cont3') {
    $level1 = getBranchContact3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level1']) && $_POST['level1'] !== '' && strlen($_POST['level1']) !== 32 && $_POST['level1'] === 'branch_cont4') {
    $level1 = getBranchContact4($_SESSION['StkTckHKey']);
  } else {
    $level1 = $_POST['level1'];
  }

  // get second level line manager
  if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'first_sup') {
    $level2 = getLevel1($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'second_sup') {

    $level2 = getLevel2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'third_sup') {
    $level2 = getLevel3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'branch_cont') {
    $level2 = getBranchContact($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'branch_cont2') {
    $level2 = getBranchContact2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'branch_cont3') {
    $level2 = getBranchContact3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level2']) && $_POST['level2'] !== '' && strlen($_POST['level2']) !== 32 && $_POST['level2'] === 'branch_cont4') {
    $level2 = getBranchContact4($_SESSION['StkTckHKey']);
  } else {
    $level2 = $_POST['level2'];
  }
  // get third level line manager
  if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'first_sup') {
    $level3 = getLevel1($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'second_sup') {

    $level3 = getLevel2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'third_sup') {
    $level3 = getLevel3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'branch_cont') {
    $level3 = getBranchContact($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'branch_cont2') {
    $level3 = getBranchContact2($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'branch_cont3') {
    $level3 = getBranchContact3($_SESSION['StkTckHKey']);
  } else if (isset($_POST['level3']) && $_POST['level3'] !== '' && strlen($_POST['level3']) !== 32 && $_POST['level3'] === 'branch_cont4') {
    $level3 = getBranchContact4($_SESSION['StkTckHKey']);
  } else {
    $level3 = $_POST['level3'];
  }

  $rest = [];
  foreach ($_POST as $key => $value) {
    if ($_POST['level1'] === $_POST[$key] || $_POST['level2'] === $_POST[$key] || $_POST['level3'] === $_POST[$key]) {
      continue;
    }

    $rest[ECh($key)] = ECh($_POST[$key]);
  }
  if (array_key_exists('request', $rest)) {
    unset($rest['request']);
  }
  if (array_key_exists('request_name', $rest)) {
    unset($rest['request_name']);
  }
  if (array_key_exists('correspondence', $rest)) {
    unset($rest['correspondence']);
  }
  if (array_key_exists('form_dat', $rest)) {
    unset($rest['form_dat']);
  }
  if (array_key_exists('page_template', $rest)) {
    unset($rest['page_template']);
  }
  if (array_key_exists('to_backup', $rest)) {
    unset($rest['to_backup']);
  }
  if (array_key_exists('request_subject', $rest)) {
    unset($rest['request_subject']);
  }

  // var_dump($rest);
  $rest = json_encode($rest);

  if (strlen($level1) !== 32 || is_null($level1)) {
    echo ("<script type='text/javascript'>{parent.msgbox('You do not have a line manager.Kindly contact the HR team', 'red'); }</script>");
    die();
  }
  //for level1
  if (strlen($level1) === 32 && (strlen($level2) !== 32 || is_null($level2)) && (strlen($level3) !== 32 || is_null($level3))) {
    // var_dump("here0");
    // die;
    $Script = "INSERT INTO [formrequest]([emp_hash]
      ,[request_name]
      ,[request]
      ,[level1]
      ,[level1_status]
      ,[level2]
      ,[level2_status]
      ,[level3]
      ,[level3_status]
      ,[request_date]
      ,[corres]
      ,[form_dat]
      ,[page_template]
      ,[to_backup]
      ,[file_1]
      ,[file_2]
      ,[file_3]
      ,[file_4]
      ,[file_5]
      ,[all_file]
      ,[request_subject]

      ) VALUES ('" . $_SESSION['StkTckHKey'] . "','" . $_POST['request_name'] . "','" . $rest . "','$level1','P',NULL,'',NULL,'',GetDate(), '$corres','$form_dat','$page_template','$to_backup','$file_1','$file_2','$file_3','$file_4'
      ,'$file_5'
      ,'$all_file'
      ,'$request_subject'
      
      
      )";

    // var_dump("$Script");
    $level1_details = getUserDetailsfromHash($level1);
    $sender_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);

    $qrr = ScriptRunnerUD($Script, "Inst");
    AuditLog("INSERT-Request", "New " . ECh($_POST['request_name']) - $request_subject . " Request Record Created by [" . ECh($sender_details["SName"] . " " . $sender_details["FName"] . " " . $sender_details["ONames"]) . "]");

    // send mail to level1

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has just submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']}-$request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");
    echo ("<script type='text/javascript'>{parent.msgbox('Request submitted successfully', 'red'); }</script>");

    $msg = "Dear #Name#,<br /> Your request(#nameofrequest#) is being processed. You will get another mail approving/cancelling your request soon";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    // $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']} - $request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    foreach ($correspondences as $correspondence) {
      // var_dump('hhh');
      $hash = '';
      if ($correspondence === 'first_sup') {
        $hash = getLevel1($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'second_sup') {
        $hash = getLevel2($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'third_sup') {
        $hash = getLevel3($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'branch_cont') {
        $hash = getBranchContact($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'branch_cont2') {
        $hash = getBranchContact2($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'branch_cont3') {
        $hash = getBranchContact3($_SESSION['StkTckHKey']);
      } else if ($correspondence === 'branch_cont4') {
        $hash = getBranchContact4($_SESSION['StkTckHKey']);
      } else {
        $hash = $correspondence;
      }
      $correspond = getUserDetailsfromHash($hash);

      $msg = "Dear #Name#,<br /> This is to inform you that a request({$_POST['request_name']}- $request_subject) has been made by employee #EmpName#. #Approver# has been notify to either approve or reject the request. ";
      $Subj = "{$_POST['request_name']} - $request_subject Request Notification ";
      $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
      $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#Approver#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);

      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $correspond['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      // var_dump($Script_Mail);
      ScriptRunnerUD($Script_Mail, "QueueMail");
    }
  } else if (strlen($level1) === 32 && strlen($level2) === 32 && (strlen($level3) !== 32 || is_null($level3))) {
    // var_dump("here1");
    // die;
    $Script = "INSERT INTO [formrequest]([emp_hash]
      ,[request_name]
      ,[request]
      ,[level1]
      ,[level1_status]
      ,[level2]
      ,[level2_status]
      ,[level3]
      ,[level3_status]
      ,[request_date]
      ,[corres]
      ,[form_dat]
      ,[page_template]
      ,[to_backup]
      ,[file_1]
      ,[file_2]
      ,[file_3]
      ,[file_4]
      ,[file_5]
      ,[all_file]
      ,[request_subject]
      ) VALUES ('" . $_SESSION['StkTckHKey'] . "','" . $_POST['request_name'] . "','" . $rest . "','$level1','P','$level2','',NULL,'',GetDate(),
      '$corres','$form_dat','$page_template','$to_backup','$file_1','$file_2','$file_3','$file_4'
      ,'$file_5'
      ,'$all_file'
      ,'$request_subject'
      
      )";

    $qrr = ScriptRunnerUD($Script, "Inst");

    // send mail to level1
    $level1_details = getUserDetailsfromHash($level1);
    $sender_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);
    // audit trail
    AuditLog("INSERT-Request", "New " . ECh($_POST['request_name']) - $request_subject . " Request Record Created by [" . ECh($sender_details["SName"] . " " . $sender_details["FName"] . " " . $sender_details["ONames"]) . "]");

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has just submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']} - $request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";

    ScriptRunnerUD($Script_Mail, "QueueMail");
    echo ("<script type='text/javascript'>{parent.msgbox('Request submitted successfully', 'red'); }</script>");
    $msg = "Dear #Name#,<br /> Your request(#nameofrequest#) is being processed. You will another mail approving/cancelling your request soon";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    // $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']}-$request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    ScriptRunnerUD($Script_Mail, "QueueMail");

    foreach ($correspondences as $correspondence) {
      $correspond = getUserDetailsfromHash($correspondence);

      $msg = "Dear #Name#,<br /> This is to inform you that a request({$_POST['request_name']}) has been made by employee #EmpName#. #Approver# has been notify to either approve or reject the request. ";
      $Subj = "{$_POST['request_name']}-$request_subject Request Notification ";
      $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
      $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#Approver#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);

      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $correspond['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      // var_dump($Script_Mail);
      ScriptRunnerUD($Script_Mail, "QueueMail");
    }
  } else if (strlen($level1) === 32 && strlen($level2) === 32 && strlen($level3) === 32) {
    // var_dump("here2");

    $Script = "INSERT INTO [formrequest]([emp_hash]
      ,[request_name]
      ,[request]
      ,[level1]
      ,[level1_status]
      ,[level2]
      ,[level2_status]
      ,[level3]
      ,[level3_status]
      ,[request_date]
      ,[corres]
       ,[form_dat]
      ,[page_template]
      ,[to_backup]
       ,[file_1]
      ,[file_2]
      ,[file_3]
      ,[file_4]
      ,[file_5]
      ,[all_file]
      ,[request_subject]
      ) VALUES ('" . $_SESSION['StkTckHKey'] . "','" . $_POST['request_name'] . "','" . $rest . "','$level1','P','$level2','','$level3','',GetDate(),'$corres','$form_dat','$page_template','$to_backup','$file_1','$file_2','$file_3','$file_4'
      ,'$file_5'
      ,'$all_file'
      ,'$request_subject'
      
      )";
    $qrr = ScriptRunnerUD($Script, "Inst");

    // var_dump("here2_1");

    // send mail to level1
    $level1_details = getUserDetailsfromHash($level1);
    $sender_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);
    // audit trail
    AuditLog("INSERT-Request", "New " . ECh($_POST['request_name']) - $request_subject . " Request Record Created by [" . ECh($sender_details["SName"] . " " . $sender_details["FName"] . " " . $sender_details["ONames"]) . "]");

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has just submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']}-$request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];
    // var_dump("here2_2");
    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    ScriptRunnerUD($Script_Mail, "QueueMail");
    echo ("<script type='text/javascript'>{parent.msgbox('Request submitted successfully', 'red'); }</script>");

    $msg = "Dear #Name#,<br /> Your request(#nameofrequest#) is being processed. You will another mail approving/cancelling your request soon";
    $Subj = "Request Notification - {$_POST['request_name']} ($request_subject)";
    // $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', "{$_POST['request_name']} - $request_subject", $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    // var_dump("here2_3");

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";

    // print_r("$Script_Mail");

    ScriptRunnerUD($Script_Mail, "QueueMail");

    // var_dump("here2_4");

    // die();

  } else if (strlen($level1) === 32 && strlen($level2) !== 32 && strlen($level3) === 32) {

    echo ("<script type='text/javascript'>{parent.msgbox('You need a second level approver to request this form. Kindly contact the administrator', 'red'); }</script>");
  }
}

?>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
  } else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
  <script>
    $(function() {
          <?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
          } else { ?>
            $("#ExDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "-1Y",
              maxDate: "+1D",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })
            $("#EmpDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "-80Y",
              maxDate: "+1M",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

          <?php } ?>
          $("#DOB").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-80Y",
            maxDate: "<?php
                      $kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
                      if (number_format($kk, 0, '', '') == 0 || $kk == "") {
                        echo "-18Y";
                      } else {
                        echo "-" . $kk . "Y";
                      } ?>
            ", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
          });
  </script>
  <script>
    $(function() {
      $("#tabs").tabs();
    });
    $(function() {
      $(document).tooltip();
    });
    $(function() {
      $("#ClearDate").click(function() {
        document.getElementById("ExDt").value = '';
      });
    });
  </script>
  <script>
    $(function() {
      $("#COO").change(function() {
        var Pt = $("#COO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
      });
      $("#SOO").change(function() {
        var Pt = $("#SOO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
      });
    });
  </script>
  <link href="../css/style_main.css" rel="stylesheet" type="text/css">
  <script language="JavaScript" src="../calendar/calendar_us.js"></script>
  <link rel="stylesheet" href="../calendar/calendar.css">
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/master_style.css">
  <link rel="stylesheet" href="../assets/css/responsive.css">
  <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
  <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
  <link rel="stylesheet" href="../assets/css/jquery-ui.min.css">
</head>
<style>
  .ui-state-highlight {
    border-color: #0a8261 !important;
    border-width: 3px;
  }
</style>

<style>
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #6dab69;
  }

  [type=checkbox]+label {
    padding-left: 20px;
  }

  hr {
    border: 0;
    clear: both;
    display: block;
    width: 96%;
    background-color: #dee7ee;
    height: 1px;
    margin-top: 10px;
  }
</style>


</head>


<body>






  <!-- Main content -->
  <section class="content">

    <div class="box mx-auto" style="width:85%">
      <div class="box-header with-border">
        <div class="row">

          <div class="col-md-6  text-md-left text-center">
            <h3 id="header">
              Request Form
            </h3>

          </div>
          <div class="col-md-6">
            <div class="row">

              <div class="col-4"></div>
              <!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
              <!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->



              <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                <select class="form-control " name="eachGroup" id="eachGroup">
                  <option value="">--</option>
                  <?php
                  $query = "SELECT * from [formtemplate] where  id='1'";

                  $getcheck = ScriptRunnercous($query);

                  $groupsarry = json_decode($getcheck['template_name'], true);

                  ?>

                  <?php foreach ($groupsarry as $eachgroup) : ?>


                    <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>


                  <?php endforeach; ?>



                </select>
                <span class="input-group-btn">
                  <button type="button" class="btn btn-danger" onclick="getGroupArry()">Open</button>
                </span>
              </div>



            </div>



          </div>
        </div>
      </div>



      <div class="box-body" id="box-body">

        <form method="post" id="needs-validation" enctype="multipart/form-data">
          <div class="row" id="sortable">




          </div>

          <input type="hidden" name="level1" id="level1" value="">
          <input type="hidden" name="level2" id="level2" value="">
          <input type="hidden" name="level3" id="level3" value="">
          <input type="hidden" name="request_name" id="request_name" value="">
          <input type="hidden" name="correspondence" id="correspondence" value="">
          <input type="hidden" name="form_dat" id="form_dat" value="">
          <input type="hidden" name="to_backup" id="to_backup" value="">
          <input type="hidden" name="page_template" id="page_template" value="">

          <button class="btn btn-danger btn-sm text-center " id="sub" type="submit" name="request" disabled onclick="getFormTemplate(event); return false">Submit</button>
        </form>


        <!-- <div  class="row">
                        <div class="row">

                            <div class="col-md-12">


                                <div class="text-center ">
                                      <br>
                                      <button class="btn btn-danger btn-sm text-center " id="rearrange" disabled  >Update Template</button> <br>
                                      <small><i>Right click and drag the field label to rearrange</i></small>

                                </div>
                                <div class="row text-center" style="margin-top:10px">
                                       <div class="col-md-12 mt-2">
                                         <a href="create_edit_job.php" class="text-center">Click here to create/edit job templates</a>
                                       </div>
                                </div>

                           </div>
                        </div> -->












      </div>




















    </div>
    </div>
    <!-- /.box-body -->




    </div>

















    <!-- /.row -->
  </section>
  <!-- /.content -->












  <!-- Bootstrap 4.0-->
  <script src="../assets/js/jquery-ui.min.js"></script>



  <script type="text/javascript">
    function getTotal() {
      let amt_1 = $('#amt_1').val() || 0;
      let amt_2 = $('#amt_2').val() || 0;
      let amt_3 = $('#amt_3').val() || 0;
      let amt_4 = $('#amt_4').val() || 0;
      let amt_5 = $('#amt_5').val() || 0;
      let tot = (parseFloat(amt_1) + parseFloat(amt_2) + parseFloat(amt_3) + parseFloat(amt_4) + parseFloat(amt_5));
      $('#tot_expense').val(tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

      if (tot) {
        let tot_l = $('#advance_collected').val() || 0;
        $('#balance').val((tot_l - tot).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
      }


    }

    function getBalance() {
      let amt_1 = $('#balance').val() || 0;

      let tot = (parseFloat(amt_1));
      $('#balance').val(tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

    }

    function getAdvance() {
      let amt_1 = $('#advance_collected').val() || 0;

      let tot = (parseFloat(amt_1));
      $('#advance_collected').val(tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));


      if (tot) {

        let amt_1 = $('#amt_1').val() || 0;
        let amt_2 = $('#amt_2').val() || 0;
        let amt_3 = $('#amt_3').val() || 0;
        let amt_4 = $('#amt_4').val() || 0;
        let amt_5 = $('#amt_5').val() || 0;
        let tot_l = (parseFloat(amt_1) + parseFloat(amt_2) + parseFloat(amt_3) + parseFloat(amt_4) + parseFloat(amt_5));
        $('#balance').val((tot - tot_l).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
      }


    }



    function convert(str) {
      //  /[\s\S]*?/g
      str = str.replace(/&amp;/g, "&");
      str = str.replace(/&lt;/g, "<");
      str = str.replace(/&gt;/g, ">");
      str = str.replace(/&quot;/g, '"');
      str = str.replace(/&#x27;/g, "'");
      str = str.replace(/&#x2F;/g, '/');
      return str;

    }




    function getGroupArry() {


      let eachgroup = $("#eachGroup").val();
      // console.log(eachgroup);
      if (eachgroup == '') {
        {
          parent.msgbox('Please select a template name ', 'red');
        };
        return;
      }
      $('#sub').prop('disabled', false);
      let data = <?php echo json_encode($groupsarry); ?>;
      let result = data.filter(obj => {
        return obj.id === eachgroup;
      });


      //  console.log();
      let pageContent = convert(result[0].template);

      $('#sortable').empty();
      $('#sortable').html(
        `
         <div class="col-md-12">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request subject <span style="color: red">*</span> :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="" class="form-control" required>

                </div>
              </div>
            </div>
        ${pageContent}
        `
      );
      $('#level1').val(result[0].level1?.trim());
      $('#level2').val(result[0].level2?.trim());
      $('#level3').val(result[0].level3?.trim());
      $('#request_name').val(result[0].templateName?.trim());
      $('#correspondence').val(JSON.stringify(result[0].correspondence));
      $('#page_template').val(JSON.stringify(pageContent));









      // console.log(result);



    }

    function getFormTemplate(event) {

      event.preventDefault();

      $('<div></div>').appendTo('body')
        .html('<div><class=TextBoxText> Do you wish to <strong>submit this form request</strong>?</h5></div>')
        .dialog({
          modal: true,
          title: "Submit Form Request",
          zIndex: 10000,
          autoOpen: true,
          width: 'auto',
          resizable: true,
          height: 'auto',
          resizable: true,
          buttons: {
            Yes: function() {

              // for files


              let file_1 = $('#file_1').prop('files') ? $('#file_1').prop('files')[0] : null;
              let file_2 = $('#file_2').prop('files') ? $('#file_2').prop('files')[0] : null;
              let file_3 = $('#file_3').prop('files') ? $('#file_3').prop('files')[0] : null;
              let file_4 = $('#file_4').prop('files') ? $('#file_4').prop('files')[0] : null;
              let file_5 = $('#file_5').prop('files') ? $('#file_5').prop('files')[0] : null;

              let imagefile_1 = file_1 ? file_1.type : null;
              let imagefile_2 = file_2 ? file_2.type : null;
              let imagefile_3 = file_3 ? file_3.type : null;
              let imagefile_4 = file_4 ? file_4.type : null;
              let imagefile_5 = file_5 ? file_5.type : null;
              // console.log(imagefile_1);
              // console.log(imagefile_2);
              let match = ['image/gif', 'image/jpeg', 'image/png', 'application/pdf', 'csv', 'xlsx'];
              // let maxsize= 5000000;
              let maxsize = 50000000;
              if (imagefile_1 !== null) {
                if (match.includes(imagefile_1)) {
                  let imagesize = file_1.size;
                  if (imagesize > maxsize) {
                    {
                      parent.msgbox('File size cannot be greater than 5Mb.', 'red');
                    };
                    return;
                  }

                }


                // else {
                //   $(this).dialog("close");
                //   {
                //     parent.msgbox(`Please select a valid file type (${match.join(",")}) `, 'red');
                //   };
                //   return;

                // }
              }
              if (imagefile_2 !== null) {
                if (match.includes(imagefile_2)) {
                  let imagesize = file_2.size;
                  if (imagesize > maxsize) {
                    {
                      parent.msgbox('File size cannot be greater than 5Mb.', 'red');
                    };
                    return;
                  }

                }


                // else {
                //   $(this).dialog("close");
                //   {
                //     parent.msgbox(`Please select a valid file type (${match.join(",")}) `, 'red');
                //   };
                //   return;

                // }
              }

              if (imagefile_3 !== null) {
                if (match.includes(imagefile_3)) {
                  let imagesize = file_3.size;
                  if (imagesize > maxsize) {
                    {
                      parent.msgbox('File size cannot be greater than 5Mb.', 'red');
                    };
                    return;
                  }

                }


                // else {
                //   $(this).dialog("close");
                //   {
                //     parent.msgbox(`Please select a valid file type (${match.join(",")}) `, 'red');
                //   };
                //   return;

                // }
              }

              if (imagefile_4 !== null) {
                if (match.includes(imagefile_4)) {
                  let imagesize = file_4.size;
                  if (imagesize > maxsize) {
                    {
                      parent.msgbox('File size cannot be greater than 5Mb.', 'red');
                    };
                    return;
                  }

                }


                // else {
                //   $(this).dialog("close");
                //   {
                //     parent.msgbox(`Please select a valid file type (${match.join(",")}) `, 'red');
                //   };
                //   return;

                // }
              }
              if (imagefile_5 !== null) {
                if (match.includes(imagefile_5)) {
                  let imagesize = file_5.size;
                  if (imagesize > maxsize) {
                    {
                      parent.msgbox('File size cannot be greater than 1Mb.', 'red');
                    };
                    return;
                  }

                }


                // else {
                //   $(this).dialog("close");
                //   {
                //     parent.msgbox(`Please select a valid file type (${match.join(",")}) `, 'red');
                //   };
                //   return;

                // }
              }





              const formdata = $('#needs-validation').serializeArray();
              const backup = [];

              // const real_data=[];
              const new_formdata = formdata
                .filter((obj) => obj.name !== 'level1')
                .filter((obj) => obj.name !== 'level2')
                .filter((obj) => obj.name !== 'level3')
                .filter((obj) => obj.name !== 'request_name')
                .filter((obj) => obj.name !== 'correspondence')
                .filter((obj) => obj.name !== 'form_dat')
                .filter((obj) => obj.name !== 'to_backup')
                .filter((obj) => obj.name !== 'page_template');
              $.each(new_formdata, function(i, obj) {
                if (obj.value !== "") {
                  backup.push(obj);
                }
              });

              const to_str = JSON.stringify(new_formdata);
              const to_backup = JSON.stringify(backup);


              $('#form_dat').val(to_str);
              $('#to_backup').val(to_backup);

              // check if all required field are filled
              if ($("#needs-validation")[0].checkValidity()) {
                $('#needs-validation').submit();
              } else {
                $("#needs-validation")[0].reportValidity()
              }

            },
            No: function() {
              $(this).dialog("close");
            }

          },
          close: function(event, ui) {
            $(this).remove();
          }
        });






















    }
  </script>








</body>

</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$td = date("Y-m-d");
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("APPROVE FORM" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}

function getSelect($id, $group)
{

    $item = $GLOBALS['item'];

    $assItem = $item['assigned_item'];

    // var_dump( $item['assigned_item']);

    $op = "<select id=" . $id . " onchange='amp(" . $id . "); return;' class='form-control' >
        <option value='' >--select--</option>
    ";

    $query = "SELECT * from [bkmitems] where [select_group] ='$group' ";

    // echo $query;

    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
    if ($conn2 === false) {
        //Redirect this to a page that says there was an issue and it is being handled by the site administrator
        //Also send an automatic mail to me saying "CANNOT CONNECT TO DATABASE - DB SERVER DOWNTIME"
        echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
        //die( print_r( sqlsrv_errors(), true));
    }
    $res2 = sqlsrv_query($conn2, $query);

    while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_BOTH)) {
        $assname = "";
        if ($assItem === $row['item_name']) {
            $assname = "selected";
        }

        $op .= "<option value=" . $row['id'] . " $assname >" . $row['item_name'] . "</option>";
    }

    $op .= "</select>";

    return $op;
}

$dis = false;

if (isset($_POST['Decline'])) {
    // var_dump($_POST);
    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);

    $req_name = ucfirst($data['request_name']);
    $request_subject = $data['request_subject'];

    $corres = $data['corres'];
    if (!is_null($corres)) {
        $correspondences = json_decode($corres, true);
    } else {
        $correspondences = [];
    }

    // send mail employee
    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $sender_details = getUserDetailsfromHash($data['emp_hash']);

    $msg = "Dear #Name#,<br /> This is to inform you that your request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "Request Notification Declined - $req_name ({$request_subject})";
    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'] - ($request_subject), $msg);
    $msg = str_replace('#declineReason#', ECh($_POST['decline_reason']), $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // update
    if ($level_type === 1) {
        $sql = "Update [formrequest] set [level1_status] = 'C' , [level1_action_date] =GetDate() WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        AuditLog("L1-Req-Declined", " {$req_name}- {$request_subject}  Request Record declined by [" . ECh($decliner_details["SName"] . " " . $decliner_details["FName"] . " " . $decliner_details["ONames"]) . "]");
    } else if ($level_type === 2) {
        $sql = "Update [formrequest] set [level2_status] = 'C', [level2_action_date] =GetDate() WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        AuditLog("L2-Req-Declined", " {$req_name} - {$request_subject}   Request Record declined by [" . ECh($decliner_details["SName"] . " " . $decliner_details["FName"] . " " . $decliner_details["ONames"]) . "]");
    } else if ($level_type === 3) {
        $sql = "Update [formrequest] set [level3_status] = 'C', [level3_action_date] =GetDate() WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        AuditLog("L3-Req-Declined", " {$req_name} - {$request_subject}   Request Record declined by [" . ECh($decliner_details["SName"] . " " . $decliner_details["FName"] . " " . $decliner_details["ONames"]) . "]");
    }

    foreach ($correspondences as $correspondence) {
        $hash = '';
        if ($correspondence === 'first_sup') {
            $hash = getLevel1($data['emp_hash']);
        } else if ($correspondence === 'second_sup') {
            $hash = getLevel2($data['emp_hash']);
        } else if ($correspondence === 'third_sup') {
            $hash = getLevel3($data['emp_hash']);
        } else if ($correspondence === 'branch_cont') {
            $hash = getBranchContact($data['emp_hash']);
        } else if ($correspondence === 'branch_cont2') {
            $hash = getBranchContact2($data['emp_hash']);
        } else if ($correspondence === 'branch_cont3') {
            $hash = getBranchContact3($data['emp_hash']);
        } else if ($correspondence === 'branch_cont4') {
            $hash = getBranchContact4($data['emp_hash']);
        } else {
            $hash = $correspondence;
        }

        // var_dump('hhh');
        $correspond = getUserDetailsfromHash($hash);

        $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has declined the request({ $req_name }- {$request_subject} ) made by employee #EmpName#. Find more details below.<br />#declineReason#";
        $Subj = "{ $req_name }- ({$request_subject})  Request Notification Declined";
        $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
        $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
        $msg = str_replace('#Approver#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
        $msg = str_replace('#declineReason#', ECh($_POST['decline_reason']), $msg);

        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $correspond['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        // var_dump($Script_Mail);
        ScriptRunnerUD($Script_Mail, "QueueMail");
    }

    echo ("<script type='text/javascript'>{parent.msgbox('Request Declined successfully', 'red'); }</script>");
    $dis = true;
}

if (isset($_POST['Approve'])) {

    $level_type = (int) $_POST['level_type'];
    $id = (int) $_POST['request_id'];
    $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
    $data = ScriptRunnercous($sql);
    // var_dump($sql);
    // var_dump($data);
    $req_name = ucfirst($data['request_name']);
    $request_subject = $data['request_subject'];

    $corres = $data['corres'];
    if (!is_null($corres)) {
        $correspondences = json_decode($corres, true);
    } else {
        $correspondences = [];
    }
    // var_dump($correspondences);
    // die();

    if ($level_type === 1) {
        $next_approver = $data['level2'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level

            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);
            $approver_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Request Notification  - $req_name ({$request_subject})";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'] - ($request_subject), $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            // mail to correspondence

            foreach ($correspondences as $correspondence) {
                // var_dump('hhh');
                $hash = '';
                if ($correspondence === 'first_sup') {
                    $hash = getLevel1($data['emp_hash']);
                } else if ($correspondence === 'second_sup') {
                    $hash = getLevel2($data['emp_hash']);
                } else if ($correspondence === 'third_sup') {
                    $hash = getLevel3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont') {
                    $hash = getBranchContact($data['emp_hash']);
                } else if ($correspondence === 'branch_cont2') {
                    $hash = getBranchContact2($data['emp_hash']);
                } else if ($correspondence === 'branch_cont3') {
                    $hash = getBranchContact3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont4') {
                    $hash = getBranchContact4($data['emp_hash']);
                } else {
                    $hash = $correspondence;
                }

                // var_dump('hhh');
                $correspond = getUserDetailsfromHash($hash);

                $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has approved the request({ $req_name }- {$request_subject}) made by employee #EmpName#. ";
                $Subj = "{ $req_name }- ({$request_subject}) Request Notification Approved";
                $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
                $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
                $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
                $Atth = "";
                $Tm = "GetDate()";
                $To = $correspond['Email'];

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
                // var_dump($Script_Mail);
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }

            // update level1_status = A and level2_status = P
            $sql = "Update [formrequest] set [level1_status] = 'A', [level2_status] = 'P', [level1_action_date] =GetDate() WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            AuditLog("L1-Req-Approved", " {$req_name}-{$request_subject} Request Record approved by [" . ECh($approver_details["SName"] . " " . $approver_details["FName"] . " " . $approver_details["ONames"]) . "]");

            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        } else {
            // send approver mail to employee

            // $next_approver_details = getUserDetailsfromHash($next_approver1);

            $sender_details = getUserDetailsfromHash($data['emp_hash']);
            $approver_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

            // var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Request Notification { $req_name }- ({$request_subject})";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'] - $request_subject, $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // mail to corress
            foreach ($correspondences as $correspondence) {
                // var_dump('hhh');
                $hash = '';
                if ($correspondence === 'first_sup') {
                    $hash = getLevel1($data['emp_hash']);
                } else if ($correspondence === 'second_sup') {
                    $hash = getLevel2($data['emp_hash']);
                } else if ($correspondence === 'third_sup') {
                    $hash = getLevel3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont') {
                    $hash = getBranchContact($data['emp_hash']);
                } else if ($correspondence === 'branch_cont2') {
                    $hash = getBranchContact2($data['emp_hash']);
                } else if ($correspondence === 'branch_cont3') {
                    $hash = getBranchContact3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont4') {
                    $hash = getBranchContact4($data['emp_hash']);
                } else {
                    $hash = $correspondence;
                }

                // var_dump('hhh');
                $correspond = getUserDetailsfromHash($hash);

                $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has approved the request({ $req_name }- $request_subject) made by employee #EmpName#. ";
                $Subj = "{ $req_name }- $request_subject Request Notification Approved";
                $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
                $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
                $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
                $Atth = "";
                $Tm = "GetDate()";
                $To = $correspond['Email'];

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
                // var_dump($Script_Mail);
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }

            // update level1_status = A

            $sql = "Update [formrequest] set [level1_status] = 'A', [level1_action_date] = GetDate()  WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            AuditLog("L1-Req-Approved", " {$req_name}  Request Record approved by [" . ECh($approver_details["SName"] . " " . $approver_details["FName"] . " " . $approver_details["ONames"]) . "]");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        }
    } else if ($level_type === 2) {
        $next_approver = $data['level3'];
        if ($next_approver && strlen($next_approver) === 32) {
            // send mail to next level
            $next_approver_details = getUserDetailsfromHash($next_approver);
            $sender_details = getUserDetailsfromHash($data['emp_hash']);
            $approver_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

            $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
            $Subj = "Request Notification - $req_name ({$request_subject})";
            $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'] - $request_subject, $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $next_approver_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
            // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

            // mail to corress
            foreach ($correspondences as $correspondence) {

                $hash = '';
                if ($correspondence === 'first_sup') {
                    $hash = getLevel1($data['emp_hash']);
                } else if ($correspondence === 'second_sup') {
                    $hash = getLevel2($data['emp_hash']);
                } else if ($correspondence === 'third_sup') {
                    $hash = getLevel3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont') {
                    $hash = getBranchContact($data['emp_hash']);
                } else if ($correspondence === 'branch_cont2') {
                    $hash = getBranchContact2($data['emp_hash']);
                } else if ($correspondence === 'branch_cont3') {
                    $hash = getBranchContact3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont4') {
                    $hash = getBranchContact4($data['emp_hash']);
                } else {
                    $hash = $correspondence;
                }

                // var_dump('hhh');
                $correspond = getUserDetailsfromHash($hash);

                $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has approved the request({ $req_name }-$request_subject) made by employee #EmpName#. ";
                $Subj = "Request Notification Approved { $req_name }- { $req_subject }";
                $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
                $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
                $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
                $Atth = "";
                $Tm = "GetDate()";
                $To = $correspond['Email'];

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
                // var_dump($Script_Mail);
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }

            // update level2_status = A and level3_status = P

            $sql = "Update [formrequest] set [level2_status] = 'A', [level3_status] = 'P', [level2_action_date] =GetDate() WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            AuditLog("L2-Req-Approved", " {$req_name}  Request Record approved by [" . ECh($approver_details["SName"] . " " . $approver_details["FName"] . " " . $approver_details["ONames"]) . "]");
            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        } else {
            // send approver mail to employee

            $sender_details = getUserDetailsfromHash($data['emp_hash']);
            $approver_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

            // var_dump($sender_details);

            $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
            $Subj = "Request Notification - $req_name ({$request_subject})";

            $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#nameofrequest#', $data['request_name'] - $req_subject, $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $sender_details['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");

            // mail to corress
            foreach ($correspondences as $correspondence) {
                // var_dump('hhh');
                $hash = '';
                if ($correspondence === 'first_sup') {
                    $hash = getLevel1($data['emp_hash']);
                } else if ($correspondence === 'second_sup') {
                    $hash = getLevel2($data['emp_hash']);
                } else if ($correspondence === 'third_sup') {
                    $hash = getLevel3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont') {
                    $hash = getBranchContact($data['emp_hash']);
                } else if ($correspondence === 'branch_cont2') {
                    $hash = getBranchContact2($data['emp_hash']);
                } else if ($correspondence === 'branch_cont3') {
                    $hash = getBranchContact3($data['emp_hash']);
                } else if ($correspondence === 'branch_cont4') {
                    $hash = getBranchContact4($data['emp_hash']);
                } else {
                    $hash = $correspondence;
                }

                // var_dump('hhh');
                $correspond = getUserDetailsfromHash($hash);

                $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has approved the request({ $req_name }-$request_subject) made by employee #EmpName#. ";
                $Subj = "Request Notification Approved - $req_name ({$request_subject})";
                $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
                $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
                $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
                $Atth = "";
                $Tm = "GetDate()";
                $To = $correspond['Email'];

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
                // var_dump($Script_Mail);
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }

            // update level2_status = A

            $sql = "Update [formrequest] set [level2_status] = 'A', [level2_action_date] =GetDate() WHERE id=$id";
            ScriptRunnerUD($sql, "Que");
            AuditLog("L2-Req-Approved", " {$req_name}  Request Record approved by [" . ECh($approver_details["SName"] . " " . $approver_details["FName"] . " " . $approver_details["ONames"]) . "]");

            echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
            $dis = true;
        }
    } else if ($level_type === 3) {
        // send approver mail to employee

        $sender_details = getUserDetailsfromHash($data['emp_hash']);
        $approver_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);

        // var_dump($sender_details);

        $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
        $Subj = "Request Notification - $req_name ({$request_subject})";

        $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $data['request_name'] - $request_subject, $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $sender_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");
        foreach ($correspondences as $correspondence) {
            // var_dump('hhh');
            $hash = '';
            if ($correspondence === 'first_sup') {
                $hash = getLevel1($data['emp_hash']);
            } else if ($correspondence === 'second_sup') {
                $hash = getLevel2($data['emp_hash']);
            } else if ($correspondence === 'third_sup') {
                $hash = getLevel3($data['emp_hash']);
            } else if ($correspondence === 'branch_cont') {
                $hash = getBranchContact($data['emp_hash']);
            } else if ($correspondence === 'branch_cont2') {
                $hash = getBranchContact2($data['emp_hash']);
            } else if ($correspondence === 'branch_cont3') {
                $hash = getBranchContact3($data['emp_hash']);
            } else if ($correspondence === 'branch_cont4') {
                $hash = getBranchContact4($data['emp_hash']);
            } else {
                $hash = $correspondence;
            }

            // var_dump('hhh');
            $correspond = getUserDetailsfromHash($hash);

            $msg = "Dear #Name#,<br /> This is to inform you that #Approver# has approved the request({ $req_name }-$request_subject) made by employee #EmpName#. ";
            $Subj = " Request Notification Approved - $req_name ({$request_subject})";
            $msg = str_replace('#Name#', "{$correspond['SName']} {$correspond['FName']} {$correspond['OName']}", $msg);
            $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
            $msg = str_replace('#Approver#', "{$approver_details['SName']} {$approver_details['FName']} {$approver_details['OName']}", $msg);

            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
            $Atth = "";
            $Tm = "GetDate()";
            $To = $correspond['Email'];

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            // var_dump($Script_Mail);
            ScriptRunnerUD($Script_Mail, "QueueMail");
        }

        // update level3_status = A

        $sql = "Update [formrequest] set [level3_status] = 'A', [level3_action_date] =GetDate() WHERE id=$id";
        ScriptRunnerUD($sql, "Que");
        AuditLog("L3-Req-Approved", " {$req_name}  Request Record approved by [" . ECh($approver_details["SName"] . " " . $approver_details["FName"] . " " . $approver_details["ONames"]) . "]");
        echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
        $dis = true;
    }
}

if (isset($_GET['current'])) {
    // unset($_SESSION)

    $level_type = $_GET['level_type'];
    // var_dump($level_type);

    //   $next=$_POST['next'];
    //   $prev=$_POST['prev'];
    $id = (int) sanitize($_GET['current']);
    $sql = "SELECT * FROM [formrequest] WHERE id=$id";
    $item = ScriptRunnercous($sql);

    //   var_dump($item);
    //       die();

    // var_dump($id,$prev,$next);

}

?>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
        echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
    } else { ?>
        <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
    <script>
        $(function() {
                    <?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
                    } else { ?>
                        $("#ExDt").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            minDate: "+0D",
                            maxDate: "+12M",
                            dateFormat: 'dd M yy',
                            yearRange: "-75:+75"
                        })

                        $("#ExDt1").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            minDate: "+0D",
                            maxDate: "+12M",
                            dateFormat: 'dd M yy',
                            yearRange: "-75:+75"
                        })

                        $("#ExDt2").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            minDate: "+0D",
                            maxDate: "+12M",
                            dateFormat: 'dd M yy',
                            yearRange: "-75:+75"
                        })

                        $("#EmpDt").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            minDate: "-80Y",
                            maxDate: "+10Y",
                            dateFormat: 'dd M yy',
                            yearRange: "-75:+75"
                        })

                    <?php } ?>
                    $("#DOB").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        minDate: "-80Y",
                        maxDate: "<?php
                                    $kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
                                    if (number_format($kk, 0, '', '') == 0 || $kk == "") {
                                        echo "-18Y";
                                    } else {
                                        echo "-" . $kk . "Y";
                                    } ?>
                        ", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
                    });
    </script>
    <script>
        $(function() {
            $("#tabs").tabs();
        });
        $(function() {
            $(document).tooltip();
        });
        $(function() {
            $("#ClearDate").click(function() {
                document.getElementById("ExDt").value = '';
            });
        });
    </script>
    <script>
        $(function() {
            $("#COO").change(function() {
                var Pt = $("#COO").val();
                var Replmt = Pt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                $("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
            });
            $("#SOO").change(function() {
                var Pt = $("#SOO").val();
                var Replmt = Pt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                var Replmt = Replmt.replace(" ", "+");
                $("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
            });
        });
    </script>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

    <!-- Select 2-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">


    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">

    <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

    <style type="text/css">
        .btn-group-sm>.btn,
        .btn-sm {
            font-size: 10px;
            padding: px 5px;
            line-height: 20px;
        }

        .form-control {
            display: block;
            width: 100%;
            padding: .5rem .75rem;
            font-size: 0.8rem;
            line-height: 1.25;
            color: #495057;
            background-color: #fff;
            background-image: none;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: .25rem;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        .box .box-group>.box {
            margin-bottom: 20px;
        }
    </style>




</head>


<body>






    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="row">

                    <div class="col-md-12  ">
                        <div class="d-flex justify-content-between">

                            <h3>
                                Manage <?= ucfirst($item['request_name']) ?>
                            </h3>


                            <h3>
                                Status: <?= $_GET['level_status'] ?>
                            </h3>

                        </div>



                    </div>


                </div>
            </div>
            <div class="box-body">


                <?php if (is_null($item['page_template'])) : ?>

                    <div class="row">


                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h6 class="box-title"><strong>Requested By</strong></h6>
                                </div>
                                <div class="box-body">
                                    <p><?php $details = getUserDetailsfromHash($item['emp_hash']);
                                        echo "{$details['SName']} {$details['FName']} {$details['OName']}"; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h6 class="box-title"><strong>Requested Date</strong></h6>
                                </div>
                                <div class="box-body">
                                    <p><?= $item['request_date']->format('j F, Y') ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h6 class="box-title"><strong>Department</strong></h6>
                                </div>
                                <div class="box-body">
                                    <p><?php $details = getUserDetailsfromHash($item['emp_hash']);
                                        echo "{$details['Department']} "; ?></p>
                                </div>
                            </div>
                        </div>

























                        <?php

                        $groupsarry = json_decode($item['request'], true);
                        foreach ($groupsarry as $key => $value) : ?>



                            <div class="col-md-12">

                                <div class="box">
                                    <div class="box-header with-border">
                                        <h6 class="box-title"><strong><?= ucfirst(str_replace("_", " ", $key)) ?></strong></h6>
                                    </div>
                                    <div class="box-body">
                                        <?php if (is_array($value)) : ?>
                                            <?php foreach ($value as $val) : ?>
                                                <p><?= $val ?></p>
                                            <?php endforeach; ?>

                                        <?php else : ?>

                                            <p><?= $value ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>


                        <?php endforeach; ?>
















































                    </div>
                <?php else : ?>
                    <div class="row p-3">
                        <form method="post" id="needs-valid" class="form-group" style="width: 100%">
                            <div id="sortable" class="row px-5">



                            </div>
                        </form>


                    </div>




                <?php endif; ?>


                <div class="row">
                    <?php
                    // var_dump($item);
                    ?>
                    <div class="col-1">
                        <form method="post" action="approve_request_form.php">

                            <input type="submit" value="Back" class="btn btn-danger btn-sm">
                            <input type="hidden" name="bookselect1" value="<?= $item['request_name'] ?>">
                            <input type="hidden" name="request_id" value="<?= $item['id'] ?>">

                        </form>

                    </div>

                    <div class="col-1">
                        <form method="post" action="">

                            <input type="submit" id="Approve" <?= $dis == true ? "disabled" : "" ?> name="Approve" value="Approve" class="btn btn-danger btn-sm">
                            <input type="hidden" name="level_type" value="<?= $level_type ?>">
                            <input type="hidden" name="request_id" value="<?= $item['id'] ?>">

                        </form>
                    </div>

                    <div class="col-1">
                        <form method="post" action="" id="decline_form">
                            <input type="hidden" name="level_type" value="<?= $level_type ?>">
                            <input type="hidden" name="request_id" value="<?= $item['id'] ?>">
                            <input type="hidden" name="decline_reason" id="decline_reason" value="">
                            <input type="hidden" name="Decline" value="Decline">

                        </form>
                        <button type="submit" id="Decline" <?= $dis == true ? "disabled" : "" ?> class="btn btn-danger btn-sm" onclick="decline(event); return false;">Decline </button>
                    </div>

                    <div class="col-7"></div>

















                </div>






            </div>
        </div>
        <!-- /.box-body -->




        </div>

















        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->












    <!-- Bootstrap 4.0-->
    <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



    <!-- SlimScroll -->
    <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

    <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- FastClick -->
    <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

    <!-- MinimalLite Admin App -->
    <script src="../assets/js/template.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>


    <script>
        function amp(id) {


            var val = $('#' + id).val();
            // console.log(val);

            if (val == '') {
                $('#com' + id).prop('disabled', true);
                window['assignedId'] = val;

                {
                    parent.msgbox('Please select an item', 'green');
                };

            } else {
                $('#com' + id).prop('disabled', false);
                window['assignedId'] = val;
            }

        }

        function confirm(id, email) {

            $('<div></div>').appendTo('body')
                .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
                .dialog({
                    modal: true,
                    title: "Delete Booking Items",
                    zIndex: 10000,
                    autoOpen: true,
                    width: 'auto',
                    resizable: true,
                    height: 'auto',
                    resizable: true,
                    buttons: {
                        Yes: function() {
                            let name = '<?= $employee_name ?>';
                            let thirdParty = '<?= json_encode($thirdParty) ?>';

                            // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                            $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                                    modal: true,
                                    title: "Enter Custom Message",
                                    zIndex: 10000,
                                    autoOpen: true,
                                    height: 200 + 6,
                                    width: 500 + 20,
                                    buttons: {

                                        Send: function() {

                                            var msgC = document.getElementById("sd").value;



                                            let tid = (document.getElementById("sd2")) ? document.getElementById("sd2").value : '0';



                                            if (msgC == '') {

                                                {
                                                    parent.msgbox('Enter custom message', 'red');
                                                };

                                                return;

                                            }

                                            // console.log(tid);
                                            // return;

                                            $.ajax({
                                                url: "add_bookitem.php",
                                                method: "POST",
                                                data: {
                                                    id: id,
                                                    email: email,
                                                    name: name,
                                                    type: "confirmed",
                                                    val: assignedId,
                                                    msgC: msgC,
                                                    thirdParty: thirdParty,
                                                    tid: tid
                                                },
                                                success: function(response) {

                                                    if (response === 'OK') {

                                                        // console.log(allInfo);
                                                        {
                                                            parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
                                                        };
                                                        location.reload();




                                                    } else if (response === "Wrong") {

                                                        {
                                                            parent.msgbox('Something went wrong', 'red');
                                                        };

                                                    }
                                                },
                                                error: function() {
                                                    alert('Something went wrong');
                                                }
                                            });




                                            $(this).dialog("close");

                                        }
                                    }

                                });





                            $(this).dialog("close");
                        },
                        No: function() {

                            $('<div></div>').appendTo('body')
                                .html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
                                .dialog({
                                    modal: true,
                                    title: "Confirm Custom Message",
                                    zIndex: 10000,
                                    autoOpen: true,
                                    width: 'auto',
                                    resizable: true,
                                    height: 'auto',
                                    resizable: true,
                                    buttons: {
                                        Yes: function() {


                                            let name = '<?= $employee_name ?>';
                                            let tid = (document.getElementById("sd2")) ? document.getElementById("sd2").value : '0';
                                            let thirdParty = '<?= json_encode($thirdParty) ?>';
                                            $.ajax({
                                                url: "add_bookitem.php",
                                                method: "POST",
                                                data: {
                                                    id: id,
                                                    email: email,
                                                    name: name,
                                                    type: "confirmed",
                                                    val: assignedId,
                                                    thirdParty: thirdParty,
                                                    tid: tid
                                                },
                                                success: function(response) {
                                                    if (response === 'OK') {

                                                        // console.log(allInfo);
                                                        {
                                                            parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
                                                        };
                                                        location.reload();




                                                    } else if (response === "Wrong") {

                                                        {
                                                            parent.msgbox('Something went wrong', 'red');
                                                        };

                                                    }
                                                },
                                                error: function() {
                                                    alert('Something went wrong');
                                                }
                                            });






                                            $(this).dialog("close");
                                        },
                                        No: function() {

                                            $(this).dialog("close");
                                        }
                                    },
                                    close: function(event, ui) {
                                        $(this).remove();
                                    }
                                });





                            $(this).dialog("close");
                        }
                    },
                    close: function(event, ui) {
                        $(this).remove();
                    }
                });
        }

        function decline(e) {
            e.preventDefault()

            $('<div></div>').appendTo('body')
                .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
                .dialog({
                    modal: true,
                    title: "Delicine Custom Message",
                    zIndex: 10000,
                    autoOpen: true,
                    width: 'auto',
                    resizable: true,
                    height: 'auto',
                    resizable: true,
                    buttons: {
                        Yes: function() {


                            // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

                            $('<div></div>').appendTo('body')
                                .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

                                    modal: true,
                                    title: "Enter Custom Message",
                                    zIndex: 10000,
                                    autoOpen: true,
                                    height: 200 + 6,
                                    width: 500 + 20,
                                    buttons: {

                                        Send: function() {

                                            var msgC = document.getElementById("sd").value;
                                            var tid = (document.getElementById("sd2")) ? document.getElementById("sd2").value : '0';
                                            document.getElementById("decline_reason").value = msgC;



                                            if (msgC == '') {

                                                {
                                                    parent.msgbox('Enter custom message', 'red');
                                                };

                                                return;

                                            }
                                            //submit form here
                                            document.getElementById("decline_form").submit();

                                            $(this).dialog("close");

                                        }
                                    }

                                });





                            $(this).dialog("close");
                        },
                        No: function() {

                            // submit form here;
                            document.getElementById("decline_form").submit();

                            $(this).dialog("close");
                        }
                    },
                    close: function(event, ui) {
                        $(this).remove();
                    }
                });













































        }

        $("#getall1").click(function() {

            let bookSelected = $('#bookselect1').val();


            if (bookSelected === "") {



                {
                    parent.msgbox('Please select an item', 'red');
                };

                return;

            } else {

                $('#theform1').submit();






            }



        });


        function convert(str) {
            //  /[\s\S]*?/g
            str = str.replace(/&amp;/g, "&");
            str = str.replace(/&lt;/g, "<");
            str = str.replace(/&gt;/g, ">");
            str = str.replace(/&quot;/g, '"');
            str = str.replace(/&#x27;/g, "'");
            str = str.replace(/&#x2F;/g, '/');
            return str;

        }

        function ErChjs(inputString) {
            if (inputString) {

                let escapedString = inputString.replace(/`/g, "'");

                return escapedString;
            }

            return "";
        }

        // get form template
        if (<?php echo isset($item['page_template']); ?>) {

            let form_tempalet = <?php echo ErCh($item['page_template']); ?>;

            let all_data = <?php echo json_encode(($item)); ?>;
            let form_ans = <?php echo ErCh($item['form_dat']); ?>;
            let user_details = <?php echo json_encode(getUserDetailsfromHash($item['emp_hash']));  ?>;

            let all_file = all_data['all_file'].length ? ErChjs(all_data['all_file']) : "[]";








            $('#sortable').empty();
            $('#sortable').html(
                `
                 <div class="col-md-8">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request subject:</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="${ErChjs(all_data['request_subject'])}" class="form-control" required readonly>

                </div>
              </div>
            </div>
             <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Requested by :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_by" value="${user_details['FName']} ${user_details['SName']}-${user_details['EmpID']}  " class="form-control" required readonly >

                </div>
              </div>
            </div>
                ${convert(form_tempalet)}
                
                `

            );
            // let oo = all_file[0]['upload'];
            // console.log({
            //     form_tempalet,
            //     form_ans,
            //     all_file: all_file[0]['upload'],
            //     oo

            // });



            function popUp(src) {



                $('<div></div>').appendTo('body')
                    .html(`<div class="d-flex flex-column" >
                 <a href="${src}" download >download</a>
                <iframe
                src="${src}"
                frameBorder="0"
                scrolling="auto"
                width="800px"
                 height="2100px"
                ></iframe>
                    </div>`)
                    .dialog({
                        modal: true,
                        title: "Attached File",
                        zIndex: 10000,
                        autoOpen: true,
                        width: '700px',
                        resizable: true,
                        height: '600',
                        resizable: true,
                        close: function(event, ui) {
                            $(this).remove();
                        }
                    });
            }
            $.each(form_ans, function(i, field) {
                if (field.name.endsWith("[]")) {
                    var form_field = document.querySelector(`#needs-valid [value="${field.value}"]`);
                } else {
                    var form_field = document.querySelector(`#needs-valid [name="${field.name}"]`);
                    if (form_field?.type === 'radio') {
                        var form_field = document.querySelector(`#needs-valid [value="${field.value}"][type="radio"]`);
                        // console.log(form_field);
                    }

                }

                if (form_field) {
                    if (form_field.type === 'checkbox' || form_field.type === 'radio') {
                        form_field.checked = true;
                    } else {
                        form_field.value = `${field.value}`;

                    }
                }
            });


            $.each(JSON.parse(all_file), function(i, field) {
                let key = Object.keys(field)[0];
                const value = Object.values(field)[0];
                key = key.replaceAll("_", " ");


                $(`input[name="${key}"]`).parent().empty().html(`
                    <span class="smallText"  onclick="popUp('${value}')"  style="cursor:pointer">
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

            });





        }



        if ((<?php echo json_encode($item['file_1'] !== '') ?>)) {

            let all_data = <?php echo json_encode(($item)); ?>;
            let all_file = JSON.parse((all_data['all_file']));
            let file1 = all_file[0]?.file_1;

            console.log({
                all_file,
                file1
            })


            $("#file_1").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);
        } else {
            $("#file_1").parent().empty();

        }
        if ((<?php echo json_encode($item['file_2'] !== '') ?>)) {

            let all_data = <?php echo json_encode(($item)); ?>;

            let all_file = JSON.parse((all_data['all_file']));
            let file1 = all_file[1]?.file_2;

            $("#file_2").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

        } else {
            $("#file_2").parent().empty();

        }
        if ((<?php echo json_encode($item['file_3'] !== '') ?>)) {

            let all_data = <?php echo json_encode(($item)); ?>;

            let all_file = JSON.parse((all_data['all_file']));
            let file1 = all_file[2]?.file_3;

            $("#file_3").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

        } else {
            $("#file_3").parent().empty();

        }
        if ((<?php echo json_encode($item['file_4'] !== '') ?>)) {
            let all_data = <?php echo json_encode(($item)); ?>;
            let all_file = JSON.parse((all_data['all_file']));
            let file1 = all_file[3]?.file_4;

            $("#file_4").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

        } else {
            $("#file_4").parent().empty();

        }
        if ((<?php echo json_encode($item['file_5'] !== '') ?>)) {
            let all_data = <?php echo json_encode(($item)); ?>;

            let all_file = JSON.parse((all_data['all_file']));

            let file1 = all_file[4]?.file_5;

            $("#file_5").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

        } else {
            $("#file_5").parent().empty();

        }
    </script>

</body>

</html>
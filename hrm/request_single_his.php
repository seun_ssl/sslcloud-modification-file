<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$td = date("Y-m-d");
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights

if (isset($_REQUEST['view'])) {
} else {

  if (ValidateURths("REQUEST FORM HISTORY" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
  }
}

function getSelect($id, $group)
{

  $item = $GLOBALS['item'];

  $assItem = $item['assigned_item'];

  // var_dump( $item['assigned_item']);

  $op = "<select id=" . $id . " onchange='amp(" . $id . "); return;' class='form-control' >
        <option value='' >--select--</option>
    ";

  $query = "SELECT * from [bkmitems] where [select_group] ='$group' ";

  // echo $query;

  $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
  $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
  if ($conn2 === false) {
    //Redirect this to a page that says there was an issue and it is being handled by the site administrator
    //Also send an automatic mail to me saying "CANNOT CONNECT TO DATABASE - DB SERVER DOWNTIME"
    echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
    //die( print_r( sqlsrv_errors(), true));
  }
  $res2 = sqlsrv_query($conn2, $query);

  while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_BOTH)) {
    $assname = "";
    if ($assItem === $row['item_name']) {
      $assname = "selected";
    }

    $op .= "<option value=" . $row['id'] . " $assname >" . $row['item_name'] . "</option>";
  }

  $op .= "</select>";

  return $op;
}

$dis = false;

if (isset($_POST['Decline'])) {
  // var_dump($_POST);
  $level_type = (int) $_POST['level_type'];
  $id = (int) $_POST['request_id'];
  $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
  $data = ScriptRunnercous($sql);
  $level1 = $data['level1'];
  $level2 = $data['level2'];
  // $level3 = $data['level3'];
  // send mail employee
  $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
  $sender_details = getUserDetailsfromHash($data['emp_hash']);

  $msg = "Dear #Name#,<br /> This is to inform you that your request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
  $Subj = "Form Request Notification";
  $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
  $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
  $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
  $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
  $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
  $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
  $Atth = "";
  $Tm = "GetDate()";
  $To = $sender_details['Email'];

  $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
  // var_dump($Script_Mail);
  ScriptRunnerUD($Script_Mail, "QueueMail");

  // update
  if ($level_type === 1) {
    $sql = "Update [formrequest] set [level1_status] = 'C' WHERE id=$id";
    ScriptRunnerUD($sql, "Que");
  } else if ($level_type === 2) {
    $sql = "Update [formrequest] set [level2_status] = 'C' WHERE id=$id";
    ScriptRunnerUD($sql, "Que");
    // send mail to level1

    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $level1_details = getUserDetailsfromHash($level1);
    $req_name = $data['request_name'];
    $msg = "Dear #Name#,<br /> This is to inform you that the request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "{$req_name} Form Request Notification Cancelled";
    $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");
  } else if ($level_type === 3) {
    $sql = "Update [formrequest] set [level3_status] = 'C' WHERE id=$id";
    ScriptRunnerUD($sql, "Que");
    // send mail to level1 and level2
    // send mail to level1

    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $level1_details = getUserDetailsfromHash($level1);
    $req_name = $data['request_name'];
    $msg = "Dear #Name#,<br /> This is to inform you that the request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "{$req_name} Form Request Notification Cancelled";
    $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // send mail to level2

    $decliner_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
    $level2_details = getUserDetailsfromHash($level2);
    $req_name = $data['request_name'];
    $msg = "Dear #Name#,<br /> This is to inform you that the request(#nameofrequest#) has been rejected by employee #EmpName#. Find more details below.<br />#declineReason#";
    $Subj = "{$req_name} Form Request Notification Cancelled";
    $msg = str_replace('#Name#', "{$level2_details['SName']} {$level2_details['FName']} {$level2_details['OName']}", $msg);
    $msg = str_replace('#EmpName#', "{$decliner_details['SName']} {$decliner_details['FName']} {$decliner_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $msg = str_replace('#declineReason#', $_POST['decline_reason'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $level1_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    // var_dump($Script_Mail);
    ScriptRunnerUD($Script_Mail, "QueueMail");
  }
  echo ("<script type='text/javascript'>{parent.msgbox('Request Declined successfully', 'red'); }</script>");
  $dis = true;
}

if (isset($_POST['Approve'])) {

  $level_type = (int) $_POST['level_type'];
  $id = (int) $_POST['request_id'];
  $sql = "SELECT * FROM [formrequest] WHERE id='" . $id . "'";
  $data = ScriptRunnercous($sql);
  // var_dump($sql);
  // var_dump($data);
  if ($level_type === 1) {
    $next_approver = $data['level2'];
    if ($next_approver && strlen($next_approver) === 32) {
      // send mail to next level

      $next_approver_details = getUserDetailsfromHash($next_approver);
      $sender_details = getUserDetailsfromHash($data['emp_hash']);

      $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
      $Subj = "Form Request Notification";
      $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
      $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $next_approver_details['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      // var_dump($Script_Mail);
      ScriptRunnerUD($Script_Mail, "QueueMail");
      // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

      // update level1_status = A and level2_status = P
      $sql = "Update [formrequest] set [level1_status] = 'A', [level2_status] = 'P' WHERE id=$id";
      ScriptRunnerUD($sql, "Que");
      echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
      $dis = true;
    } else {
      // send approver mail to employee

      // $next_approver_details = getUserDetailsfromHash($next_approver1);
      $sender_details = getUserDetailsfromHash($data['emp_hash']);

      // var_dump($sender_details);

      $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
      $Subj = "Form Request Notification";

      $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $sender_details['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      ScriptRunnerUD($Script_Mail, "QueueMail");

      // update level1_status = A
      $sql = "Update [formrequest] set [level1_status] = 'A' WHERE id=$id";
      ScriptRunnerUD($sql, "Que");
      echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
      $dis = true;
    }
  } else if ($level_type === 2) {
    $next_approver = $data['level3'];
    if ($next_approver && strlen($next_approver) === 32) {
      // send mail to next level
      $next_approver_details = getUserDetailsfromHash($next_approver);
      $sender_details = getUserDetailsfromHash($data['emp_hash']);

      $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has submitted a request(#nameofrequest#).Kindly login and either approve or cancel his/her request.";
      $Subj = "Form Request Notification";
      $msg = str_replace('#Name#', "{$next_approver_details['SName']} {$next_approver_details['FName']} {$next_approver_details['OName']}", $msg);
      $msg = str_replace('#EmpName#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $next_approver_details['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      // var_dump($Script_Mail);
      ScriptRunnerUD($Script_Mail, "QueueMail");
      // echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");

      // update level2_status = A and level3_status = P

      $sql = "Update [formrequest] set [level2_status] = 'A', [level3_status] = 'P' WHERE id=$id";
      ScriptRunnerUD($sql, "Que");
      echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
      $dis = true;
    } else {
      // send approver mail to employee

      $sender_details = getUserDetailsfromHash($data['emp_hash']);

      // var_dump($sender_details);

      $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
      $Subj = "Form Request Notification";

      $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
      $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
      $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
      $Atth = "";
      $Tm = "GetDate()";
      $To = $sender_details['Email'];

      $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
      ScriptRunnerUD($Script_Mail, "QueueMail");

      // update level2_status = A

      $sql = "Update [formrequest] set [level2_status] = 'A' WHERE id=$id";
      ScriptRunnerUD($sql, "Que");
      echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
      $dis = true;
    }
  } else if ($level_type === 3) {
    // send approver mail to employee

    $sender_details = getUserDetailsfromHash($data['emp_hash']);

    // var_dump($sender_details);

    $msg = "Dear #Name#,<br /> This is to inform you that request(#nameofrequest#) has been approved";
    $Subj = "Form Request Notification";

    $msg = str_replace('#Name#', "{$sender_details['SName']} {$sender_details['FName']} {$sender_details['OName']}", $msg);
    $msg = str_replace('#nameofrequest#', $data['request_name'], $msg);
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";
    $To = $sender_details['Email'];

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    ScriptRunnerUD($Script_Mail, "QueueMail");

    // update level3_status = A

    $sql = "Update [formrequest] set [level3_status] = 'A' WHERE id=$id";
    ScriptRunnerUD($sql, "Que");
    echo ("<script type='text/javascript'>{parent.msgbox('Request Approved successfully', 'red'); }</script>");
    $dis = true;
  }
}

if (isset($_GET['current'])) {

  $level_type = $_GET['level_type'];

  $id = (int) sanitize($_GET['current']);
  $sql = "SELECT * FROM [formrequest] WHERE id=$id";
  $item = ScriptRunnercous($sql);
  if (ValidateURths("UPDATE REQUEST FORM" . "V")) {
    if (isset($_POST['form_dat'])) {
      $to_diff = $_POST['to_diff'];
      $decoded_diff = json_decode($to_diff, true);
      $diff_value = '';
      foreach ($decoded_diff as $key => $value) {
        $a = ucwords(rtrim($value['name'], '[]'));
        $b = ucwords($value['value']);
        $diff_value .= "<p>{$a}</p><p>{$b}</p><hr/>";
      }
      $diff_value = rtrim($diff_value, '<hr/>');

      $form_dat = ECh($_POST['form_dat']);
      $sql = "Update [formrequest] set [form_dat] = '$form_dat',[updated_by] ='" . $_SESSION["StkTck" . "HKey"] . "', [updated_date] = GetDate()  WHERE id=$id";
      ScriptRunnerUD($sql, "Que");
      $sender_details = getUserDetailsfromHash($_SESSION['StkTckHKey']);
      $res_name = $item['request_name'];
      AuditLog("UPDATE-Request", "$res_name Request Record updated by [" . ECh($sender_details["SName"] . " " . $sender_details["FName"] . " " . $sender_details["ONames"]) . "]");
      $level1 = $item['level1'];
      $level2 = $item['level2'];
      $level3 = $item['level3'];
      if (isset($level1)) {
        // send mail to level 1 authorizer
        $level1_details = getUserDetailsfromHash($level1);
        $editors_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
        $request_name = $item['request_name'];
        $request_date = $item['request_date'];
        $emp_details = getUserDetailsfromHash($item['emp_hash']);
        // var_dump($sender_details);

        $msg = "Dear #Name#,<br />
              This is to inform you that request(#nameofrequest#) template made by #nameofemp# on #requesteddate#  has been updated with the value below <br/>
              #diff_value#
              ";
        $Subj = "Form Request Notification(Template update)";

        $msg = str_replace('#Name#', "{$level1_details['SName']} {$level1_details['FName']} {$level1_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $request_name, $msg);
        $msg = str_replace('#nameofemp#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
        $msg = str_replace('#requesteddate#', $request_date->format("F j, Y, g:i a"), $msg);
        $msg = str_replace('#diff_value#', $diff_value, $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $level1_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                              VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");
      }
      if (isset($level2)) {
        // send mail to level 1 authorizer
        $level2_details = getUserDetailsfromHash($level2);
        $editors_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
        $request_name = $item['request_name'];
        $request_date = $item['request_date'];
        $emp_details = getUserDetailsfromHash($item['emp_hash']);
        // var_dump($sender_details);

        $msg = "Dear #Name#,<br />
              This is to inform you that request(#nameofrequest#) template made by #nameofemp# on #requesteddate#  has been updated with the value below <br/>
              #diff_value#
              ";
        $Subj = "Form Request Notification(Template update)";

        $msg = str_replace('#Name#', "{$level2_details['SName']} {$level2_details['FName']} {$level2_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $request_name, $msg);
        $msg = str_replace('#nameofemp#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
        $msg = str_replace('#requesteddate#', $request_date->format("F j, Y, g:i a"), $msg);
        $msg = str_replace('#diff_value#', $diff_value, $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $level2_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                              VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");
      }

      if (isset($level3)) {
        // send mail to level 1 authorizer
        $level3_details = getUserDetailsfromHash($level3);
        $editors_details = getUserDetailsfromHash($_SESSION["StkTck" . "HKey"]);
        $request_name = $item['request_name'];
        $request_date = $item['request_date'];
        $emp_details = getUserDetailsfromHash($item['emp_hash']);
        // var_dump($sender_details);

        $msg = "Dear #Name#,<br />
              This is to inform you that request(#nameofrequest#) template made by #nameofemp# on #requesteddate# has been updated with the value below <br/>
              #diff_value#
              ";
        $Subj = "Form Request Notification(Template update)";

        $msg = str_replace('#Name#', "{$level3_details['SName']} {$level3_details['FName']} {$level3_details['OName']}", $msg);
        $msg = str_replace('#nameofrequest#', $request_name, $msg);
        $msg = str_replace('#nameofemp#', "{$emp_details['SName']} {$emp_details['FName']} {$emp_details['OName']}", $msg);
        $msg = str_replace('#requesteddate#', $request_date->format("F j, Y, g:i a"), $msg);
        $msg = str_replace('#diff_value#', $diff_value, $msg);
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
        $Atth = "";
        $Tm = "GetDate()";
        $To = $level3_details['Email'];

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                              VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
        ScriptRunnerUD($Script_Mail, "QueueMail");
      }

      echo ("<script type='text/javascript'>{parent.msgbox('Form Request updated successfully', 'red'); }</script>");
    }
  }
  $sql = "SELECT * FROM [formrequest] WHERE id=$id";
  $item = ScriptRunnercous($sql);

  //   var_dump($item);
  //   die();

  // var_dump($id,$prev,$next);

}

?>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
  } else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
  <script>
    $(function() {
          <?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
          } else { ?>
            $("#ExDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "+0D",
              maxDate: "+12M",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

            $("#ExDt1").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "+0D",
              maxDate: "+12M",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

            $("#ExDt2").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "+0D",
              maxDate: "+12M",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

            $("#EmpDt").datepicker({
              changeMonth: true,
              changeYear: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              minDate: "-80Y",
              maxDate: "+10Y",
              dateFormat: 'dd M yy',
              yearRange: "-75:+75"
            })

          <?php } ?>
          $("#DOB").datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "-80Y",
            maxDate: "<?php
                      $kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
                      if (number_format($kk, 0, '', '') == 0 || $kk == "") {
                        echo "-18Y";
                      } else {
                        echo "-" . $kk . "Y";
                      } ?>
            ", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
          });
  </script>
  <script>
    $(function() {
      $("#tabs").tabs();
    });
    $(function() {
      $(document).tooltip();
    });
    $(function() {
      $("#ClearDate").click(function() {
        document.getElementById("ExDt").value = '';
      });
    });
  </script>
  <script>
    $(function() {
      $("#COO").change(function() {
        var Pt = $("#COO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
      });
      $("#SOO").change(function() {
        var Pt = $("#SOO").val();
        var Replmt = Pt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        var Replmt = Replmt.replace(" ", "+");
        $("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
      });
    });
  </script>
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

  <!-- Select 2-->
  <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/master_style.css">
  <link rel="stylesheet" href="../assets/css/responsive.css">


  <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
  <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">

  <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

  <style type="text/css">
    .btn-group-sm>.btn,
    .btn-sm {
      font-size: 10px;
      padding: px 5px;
      line-height: 20px;
    }

    .form-control {
      display: block;
      width: 100%;
      padding: .5rem .75rem;
      font-size: 0.8rem;
      line-height: 1.25;
      color: #495057;
      background-color: #fff;
      background-image: none;
      background-clip: padding-box;
      border: 1px solid rgba(0, 0, 0, .15);
      border-radius: .25rem;
      transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }

    .box .box-group>.box {
      margin-bottom: 20px;
    }
  </style>




</head>


<body>






  <!-- Main content -->
  <section class="content" id="invoice">

    <div class="box">
      <div class="box-header with-border">
        <div class="row">

          <div class="col-md-12  ">
            <div class="d-flex justify-content-between">

              <h3>
                <?= ucfirst($item['request_name']) ?>
              </h3>


              <h3>
                Status: <?= $_GET['level_status'] ?>
              </h3>

            </div>



          </div>

        </div>
      </div>
      <div class="box-body">


        <?php if (is_null($item['page_template'])) : ?>

          <div class="row">


            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h6 class="box-title"><strong>Requested By</strong></h6>
                </div>
                <div class="box-body">
                  <p><?php $details = getUserDetailsfromHash($item['emp_hash']);
                      echo "{$details['SName']} {$details['FName']} {$details['OName']}"; ?></p>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h6 class="box-title"><strong>Requested Date</strong></h6>
                </div>
                <div class="box-body">
                  <p><?= $item['request_date']->format('j F, Y') ?></p>
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h6 class="box-title"><strong>Department</strong></h6>
                </div>
                <div class="box-body">
                  <p><?php $details = getUserDetailsfromHash($item['emp_hash']);
                      echo "{$details['Department']} "; ?></p>
                </div>
              </div>
            </div>

























            <?php

            $groupsarry = json_decode($item['request'], true);
            foreach ($groupsarry as $key => $value) : ?>
              <!-- <div class="col-md-3">
                                <div class="col-md-12"  style="padding-left: 0px; padding-right: 0px;" >
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary" >
                                      <div class="box-header">
                                         <p class="b-0 px-0" style="font-weight: bold"><?= $key ?></p>
                                      </div>
                                      <div class="box-body">
                                                <p>
                                                   <textarea class="form-control" rows="2" readonly > <?= $value ?>
                                                    </textarea>
                                                </p>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> -->


              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h6 class="box-title"><strong><?= ucfirst(str_replace("_", " ", $key)) ?></strong></h6>
                  </div>
                  <div class="box-body">
                    <?php if (is_array($value)) : ?>
                      <?php foreach ($value as $val) : ?>
                        <p><?= $val ?></p>
                      <?php endforeach; ?>

                    <?php else : ?>

                      <p><?= $value ?></p>
                    <?php endif; ?>
                  </div>
                </div>
              </div>


            <?php endforeach; ?>
















































          </div>
        <?php else : ?>
          <div class="row p-3">
            <form method="post" id="needs-valid" class="form-group" style="width: 100%">
              <div id="sortable" class="row px-5">



              </div>
            </form>


          </div>




        <?php endif; ?>









      </div>
    </div>
    <!-- /.box-body -->




    </div>

















    <!-- /.row -->
  </section>
  <div class="row ml-5">





    <div class="col-1">

      <?php if (!isset($_REQUEST['view'])) : ?>
        <form method="post" action="request_form_history.php">
        <?php else : ?>
          <form method="post" action="employee_request.php">
            <input type="hidden" name="StaffID" value="<?= $_REQUEST['StaffID'] ?>">
          <?php endif; ?>
          <input type="submit" value="Back" class="btn btn-danger btn-sm">
          <input type="hidden" name="bookselect1" value="<?= $item['request_name'] ?>">
          <input type="hidden" name="request_id" value="<?= $item['id'] ?>">

          </form>


    </div>





    <div class="col-7">
      <?php if (!isset($_REQUEST['view'])) : ?>
        <button class="btn btn-info btn-sm" id="download" onclick="callDownload()">Download form</button>
      <?php endif; ?>
      <?php if (ValidateURths("UPDATE REQUEST FORM" . "V") && isset($item['page_template'])) : ?>
        <button class="btn btn-danger btn-sm" id="update_form" onclick="updateForm(<?= $item['id'] ?>);">Update Form</button>
      <?php endif; ?>
    </div>

















  </div>
  <!-- /.content -->
  <!-- /.content -->












  <!-- Bootstrap 4.0-->
  <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



  <!-- SlimScroll -->
  <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

  <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>

  <!-- FastClick -->
  <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

  <!-- MinimalLite Admin App -->
  <script src="../assets/js/template.js"></script>

  <!-- MinimalLite Admin for demo purposes -->
  <script src="../assets/js/demo.js"></script>
  <script src="html2pdf/dist/html2pdf.bundle.js"></script>


  <script>
    function getTotal() {
      let amt_1 = $('#amt_1').val() || 0;
      let amt_2 = $('#amt_2').val() || 0;
      let amt_3 = $('#amt_3').val() || 0;
      let amt_4 = $('#amt_4').val() || 0;
      let amt_5 = $('#amt_5').val() || 0;
      let tot = (parseFloat(amt_1) + parseFloat(amt_2) + parseFloat(amt_3) + parseFloat(amt_4) + parseFloat(amt_5));
      $('#tot_expense').val(tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));





    }

    function ErChjs(inputString) {
      if (inputString) {

        let escapedString = inputString.replace(/`/g, "'");

        return escapedString;
      }

      return "";
    }

    function callDownload() {

      function popUp(src) {



        $('<div></div>').appendTo('body')
          .html(`<div class="d-flex flex-column" >
                 <a href="${src}" download >download</a>
                <iframe
                src="${src}"
                frameBorder="0"
                scrolling="auto"
                width="800px"
                 height="2100px"
                ></iframe>
                    </div>`)
          .dialog({
            modal: true,
            title: "Attached File",
            zIndex: 10000,
            autoOpen: true,
            width: '700px',
            resizable: true,
            height: '600',
            resizable: true,
            close: function(event, ui) {
              $(this).remove();
            }
          });
      }
      let form_tempalet = <?php echo ErCh($item['page_template']); ?>;
      let all_data = <?php echo json_encode(($item)); ?>;


      let name = '<?php echo ErCh($item['request_name']); ?>';
      let res_date = '<?php echo ErCh($item['request_date']->format('j F, Y, g:i a')); ?>';
      let res_sub = '<?php echo $item['request_subject']; ?>';

      let form_ans = <?php echo ErCh($item['form_dat']); ?>;
      let user_details = <?php echo json_encode(getUserDetailsfromHash($item['emp_hash']));  ?>;

      let all_file = all_data['all_file'].length ? ErChjs(all_data['all_file']) : "[]";
      let request_date = all_data['request_date']?.date;
      var options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
      request_date = new Date(request_date);
      $('#sortable').empty();
      $('#sortable').html(`
       <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request subject  :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="${ErChjs(all_data['request_subject'])}" class="form-control" required readonly>

                </div>
              </div>
        </div>
         <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request date  :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="${request_date.toLocaleDateString("en-US", options)}" class="form-control" required readonly>

                </div>
              </div>
         </div>
             <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Requested by  :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_by" value="${user_details['FName']} ${user_details['SName']}-${user_details['EmpID']}  " class="form-control" required readonly >

                </div>
              </div>
            </div>
                ${convert(form_tempalet)}
                
                `);
      //  console.log(form_ans);
      $.each(form_ans, function(i, field) {
        if (field.name.endsWith("[]")) {
          var form_field = document.querySelector(`#needs-valid [value="${field.value}"]`);
        } else {
          var form_field = document.querySelector(`#needs-valid [name="${field.name}"]`);
          if (form_field?.type === 'radio') {
            var form_field = document.querySelector(`#needs-valid [value="${field.value}"][type="radio"]`);
            // console.log(form_field);
          }

        }

        if (form_field) {
          if (form_field.type === 'checkbox' || form_field.type === 'radio') {
            form_field.checked = true;
          } else {
            form_field.value = `${field.value}`;

          }
        }
      });



      $.each(JSON.parse(all_file), function(i, field) {
        let key = Object.keys(field)[0];
        const value = Object.values(field)[0];
        key = key.replaceAll("_", " ");


        $(`input[name="${key}"]`).parent().empty().html(`
                    <span class="smallText"  onclick="popUp('${value}')"  style="cursor:pointer">
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      });

      if ((<?php echo json_encode($item['file_1'] !== '') ?>)) {


        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[0]?.file_1;


        $("#file_1").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);
      } else {
        $("#file_1").parent().empty();

      }
      if ((<?php echo json_encode($item['file_2'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[1]?.file_2;

        $("#file_2").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_2").parent().empty();

      }
      if ((<?php echo json_encode($item['file_3'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[2]?.file_3;

        $("#file_3").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_3").parent().empty();

      }
      if ((<?php echo json_encode($item['file_4'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[3]?.file_4;

        $("#file_4").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_4").parent().empty();

      }
      if ((<?php echo json_encode($item['file_5'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));

        let file1 = all_file[4]?.file_5;

        $("#file_5").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_5").parent().empty();

      }

      const invoice = document.getElementById("invoice");


      html2pdf().from(invoice).save(`${name}-${res_sub}`);

    }

    function convert(str) {
      //  /[\s\S]*?/g
      str = str.replace(/&amp;/g, "&");
      str = str.replace(/&lt;/g, "<");
      str = str.replace(/&gt;/g, ">");
      str = str.replace(/&quot;/g, '"');
      str = str.replace(/&#x27;/g, "'");
      str = str.replace(/&#x2F;/g, '/');
      return str;

    }
    // get form template
    if (<?php echo isset($item['page_template']); ?>) {
      let all_data = <?php echo json_encode(($item)); ?>;
      let form_tempalet = <?php echo ErCh($item['page_template']); ?>;
      let form_ans = <?php echo ErCh($item['form_dat']); ?>;
      let user_details = <?php echo json_encode(getUserDetailsfromHash($item['emp_hash']));  ?>;
      let all_file = all_data['all_file'].length ? ErChjs(all_data['all_file']) : "[]";
      let request_date = all_data['request_date']?.date;
      var options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
      request_date = new Date(request_date);
      // console.log(request_date?.date);

      $('#sortable').empty();
      $('#sortable').html(`
             <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request subject <span style="color: red">*</span> :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="${ErChjs(all_data['request_subject'])}" class="form-control" required readonly>

                </div>
              </div>
            </div>
               <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Request date  :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_subject" value="${request_date.toLocaleDateString("en-US", options)}" class="form-control" required readonly>

                </div>
              </div>
         </div>
             <div class="col-md-4">
              <div class="form-group row ">
                <label class="col-sm-12 col-form-label">Requested by <span style="color: red">*</span> :</label>
                <div class="col-sm-12 input-group input-group-sm">
                  <input type="text" name="request_by" value="${user_details['FName']} ${user_details['SName']}-${user_details['EmpID']}  " class="form-control" required readonly >

                </div>
              </div>
            </div>
                ${convert(form_tempalet)}
                
                `);
      //  console.log(form_ans);

      function popUp(src) {



        $('<div></div>').appendTo('body')
          .html(`<div class="d-flex flex-column" >
                 <a href="${src}" download >download</a>
                <iframe
                src="${src}"
                frameBorder="0"
                scrolling="auto"
                width="800px"
                 height="2100px"
                ></iframe>
                    </div>`)
          .dialog({
            modal: true,
            title: "Attached File",
            zIndex: 10000,
            autoOpen: true,
            width: '700px',
            resizable: true,
            height: '600',
            resizable: true,
            close: function(event, ui) {
              $(this).remove();
            }
          });
      }
      $.each(form_ans, function(i, field) {
        if (field.name.endsWith("[]")) {
          var form_field = document.querySelector(`#needs-valid [value="${field.value}"]`);
        } else {
          var form_field = document.querySelector(`#needs-valid [name="${field.name}"]`);
          if (form_field?.type === 'radio') {
            var form_field = document.querySelector(`#needs-valid [value="${field.value}"][type="radio"]`);
            // console.log(form_field);
          }

        }

        if (form_field) {
          if (form_field.type === 'checkbox' || form_field.type === 'radio') {
            form_field.checked = true;
          } else {
            form_field.value = `${field.value}`;

          }
        }
      });


      $.each(JSON.parse(all_file), function(i, field) {
        let key = Object.keys(field)[0];
        const value = Object.values(field)[0];
        key = key.replaceAll("_", " ");


        $(`input[name="${key}"]`).parent().empty().html(`
                    <span class="smallText"  onclick="popUp('${value}')"   style="cursor:pointer">
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);
        console.log({
          field: key
        });
      });



      // for file

      if ((<?php echo json_encode($item['file_1'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[0].file_1;



        $("#file_1").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);
      } else {
        $("#file_1").parent().empty();

      }
      if ((<?php echo json_encode($item['file_2'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[1].file_2;

        $("#file_2").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_2").parent().empty();

      }
      if ((<?php echo json_encode($item['file_3'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[2].file_3;

        $("#file_3").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_3").parent().empty();

      }
      if ((<?php echo json_encode($item['file_4'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[3].file_4;

        $("#file_4").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_4").parent().empty();

      }
      if ((<?php echo json_encode($item['file_5'] !== '') ?>)) {

        let all_file = JSON.parse((all_data['all_file']));
        let file1 = all_file[4].file_5;

        $("#file_5").parent().empty().html(`
             
                    <span class="smallText"  onclick="popUp('${file1}')"  style="cursor:pointer"  >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);

      } else {
        $("#file_5").parent().empty();

      }






    }



    function updateForm(id) {

      $('<div></div>').appendTo('body')
        .html('<div><class=TextBoxText> Do you wish to <strong>update this form request</strong>?</h5></div>')
        .dialog({
          modal: true,
          title: "Update Form Request",
          zIndex: 10000,
          autoOpen: true,
          width: 'auto',
          resizable: true,
          height: 'auto',
          resizable: true,
          buttons: {
            Yes: function() {
              function popUp(src) {



                $('<div></div>').appendTo('body')
                  .html(`<div class="d-flex flex-column" >
                 <a href="${src}" download >download</a>
                <iframe
                src="${src}"
                frameBorder="0"
                scrolling="auto"
                width="800px"
                 height="2100px"
                ></iframe>
                    </div>`)
                  .dialog({
                    modal: true,
                    title: "Attached File",
                    zIndex: 10000,
                    autoOpen: true,
                    width: '700px',
                    resizable: true,
                    height: '600',
                    resizable: true,
                    close: function(event, ui) {
                      $(this).remove();
                    }
                  });
              }
              let form_ans = <?php echo ErCh($item['to_backup']); ?>;
              let all_data = <?php echo json_encode(($item)); ?>;
              let all_file = ErChjs(all_data['all_file']);

              let go = true;
              // check if employee input was changed
              form_ans.forEach(function(field) {
                if (field.name.endsWith("[]")) {
                  var form_field = document.querySelector(`#needs-valid [value="${field.value}"]`);
                } else {
                  var form_field = document.querySelector(`#needs-valid [name="${field.name}"]`);
                  if (form_field?.type === 'radio') {
                    var form_field = document.querySelector(`#needs-valid [value="${field.value}"][type="radio"]`);
                  }

                }

                if (form_field) {
                  if (form_field.type === 'checkbox' || form_field.type === 'radio') {
                    // form_field.checked = true;
                    if (form_field.checked === false) {
                      go = false;
                    }
                  } else {
                    // form_field.value=`${field.value}`;
                    if (form_field.value !== field.value) {
                      go = false;

                    }

                  }

                }
              });
              // if employee data was not changed
              // console.log(go);

              $.each(JSON.parse(all_file), function(i, field) {
                let key = Object.keys(field)[0];
                const value = Object.values(field)[0];
                key = key.replace("_", " ");


                $(`input[name="${key}"]`).parent().empty().html(`
                    <span class="smallText"  onclick="popUp('${value}')"  style="cursor:pointer" >
                     
                          <img border="0" src="../images/attachment.png" width="32" height="32">
                  
                  </span>
                `);
                console.log({
                  field: key
                });
              });
              if (go) {
                const formdata = $('#needs-valid').serializeArray();
                const to_str = JSON.stringify(formdata);
                $('<input>').attr({
                  type: 'hidden',
                  id: 'form_dat',
                  name: 'form_dat',
                  value: `${to_str}`
                }).appendTo('#needs-valid');

                const diff = [];
                // console.log(formdata);
                formdata.forEach(function(field) {
                  if (field.name.endsWith("[]")) {
                    const itm = form_ans.find((item) => (item.value === field.value && item.name === field.name));
                    if (!itm) {
                      diff.push(field);
                    }
                  } else {
                    const itm = form_ans.find((item) => (item.name === field.name && item.value === field.value));
                    if (!itm && field.value.trim() !== "") {
                      diff.push(field);
                    }

                  }
                });
                // console.log(diff);
                if (diff.length) {
                  const to_diff = JSON.stringify(diff);
                  $('<input>').attr({
                    type: 'hidden',
                    id: 'to_diff',
                    name: 'to_diff',
                    value: `${to_diff}`
                  }).appendTo('#needs-valid');
                }

                $('#needs-valid').submit();

              } else {
                {
                  parent.msgbox('You are not allowed to alter what was entered before the request was submitted. Kindly review and re-update ', 'red');
                }
              }
              $(this).dialog("close");
            },
            No: function() {
              $(this).dialog("close");
            }

          },
          close: function(event, ui) {
            $(this).remove();
          }
        });






    }
  </script>

</body>

</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("RENEWAL AGREEMENT" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}
$emp_details = null;
$other = false;

if (isset($_POST['scheduleForm'])) {
	if (empty($_POST['workSchedule'])) {

		echo ("<script type='text/javascript'>{parent.msgbox('Enter Work Schedule', 'red'); }</script>");
	} else {


		$other = true;


		$emp = $_POST['other'];

		// var_dump($emp);
		// die();
		$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

		$emp_details = ScriptRunnercous($Script_Menu);

		// var_dump($emp_details);
		$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
		$EditID = ScriptRunner($Script_Edit, "SetValue17");
		$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
		$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
		// var_dump($emp_detailsHr['sig_img']);

		$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
		$comp_details = ScriptRunnercous($Script_Edit_com);
		$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
		$email_details = ScriptRunnercous($Script_Edit_email);
		$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
		$addr_details = ScriptRunnercous($Script_Edit_addr);
		$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
		$rc_details = ScriptRunnercous($Script_Edit_RC);
		$branchId = $emp_details['BranchID'];
		$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
		$bran_details = ScriptRunnercous($Script_Edit_Br);
		$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
		$fin_details = ScriptRunnercous($Script_Edit_Fin);
		$fin_hash = $fin_details['Scheme'];
		$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
		$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

		// var_dump($fin_set_details);
		$nm = null;
		for ($i = 1; $i <= 25; $i++) {
			if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
				$nm = $i;
				break;
			}
		}
	}
}



$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
	// var_dump($emp_detailsHr['sig_img']);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
	$rc_details = ScriptRunnercous($Script_Edit_RC);
	$branchId = $emp_details['BranchID'];
	$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
	$bran_details = ScriptRunnercous($Script_Edit_Br);
	$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
	$fin_details = ScriptRunnercous($Script_Edit_Fin);
	$fin_hash = $fin_details['Scheme'];
	$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
	$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

	// var_dump($fin_set_details);
	$nm = null;
	for ($i = 1; $i <= 25; $i++) {
		if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
			$nm = $i;
			break;
		}
	}

	// var_dump($bran_details);

}

?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		* {
			font-family: "Times New Roman", Times, serif !important;

			color: black !important;
		}

		table,
		th,
		td {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px !important;
			/* font-family: "Times New Roman", Times, serif; */
		}

		body {
			font-size: 16px !important;
		}

		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: 5px;
			line-height: 20px;
		}

		.box-body ul li {
			line-height: 18px !important;
		}

		.card-img {
			width: 76%;
		}

		.nav-tabs-custom>.tab-content {
			color: black !important;
			text-align: justify;
		}

		.rectangle {
			border: 1px solid black;
			/* padding: 10px; */
		}

		img {
			/* width: 100%;
  height: 160px;
  margin-left: 550px; */
		}
	</style>

</head>


<body>



	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Generate Employee Renewal Letter
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control " name="bookselect1" id="bookselect1">
										<!-- <select name="level1" id="level1"  class="form-control" > -->
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>



									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">



				<?php if (!is_null($emp_details) && !empty($emp_details) || isset($_POST['scheduleForm'])): ?>
					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<div class="nav-tabs-custom" style="min-height: 400px">
										<ul class="nav nav-tabs">
											<li>
												<a href="#book" data-toggle="tab">
													NON-ACADEMIC STAFF
												</a>
											</li>

											<li>
												<a href="#book2" data-toggle="tab">
													ACADEMIC STAFF
												</a>
											</li>

											<li>
												<a href="#book3" data-toggle="tab">
													ADMIN STAFF
												</a>
											</li>

											<li>
												<a href="#book4" data-toggle="tab">
													OTHERS
												</a>
											</li>

										</ul>
										<div class="tab-content">
											<div class="tab-pane  " id="book">
												<?php if (strtoupper($emp_details['EmpCategory']) === 'NON-ACADEMIC'): ?>
													<div id="invoice" class="text-dark ">
														<div class="row">
															<div class="col-sm-10  mx-auto">
																<div class="row text-center">
																	<div class="col-sm-2">
																		<!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
																	</div>

																</div>
															</div>
														</div>
														<!-- <div class="row justify-content-center mt-1 "> -->
														<div style="margin-bottom:5px;">
															<img src="../pfimg/non_academic.jpg" alt="" width="100%" height="100">
														</div>

														<p style="margin-bottom: 1px; font-weight: 600; margin-left: 75%; ">DATE: <?php echo date("d F Y") ?></p>


														<div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
															<div class="row">
																<div class="col-md-10">

																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Branch of school:</span>
																		<?php

																		$sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

																		$sqgR = ScriptRunnercous($sqg);

																		echo ucwords(strtolower($sqgR['OName']));

																		?>

																	</p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?></p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Designation:</span>
																		<?php

																		$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

																		$sqR = ScriptRunnercous($sq);

																		echo (isset($sqR) ? ucwords(strtolower($sqR['ItemPos'])) : '');

																		?>
																	</p>
																</div>
																<div class="col-md-2 text-right">

																	<!-- <p>Branch of school: </p> -->
																	<img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
																</div>
															</div>

														</div>
														<p style="margin-bottom: 30px;margin-left: 25px;">The Management wishes to inform you that your appointment has been reviewed and is valid from
															September 01, 2021 to July 31, 2022 (being one academic session). The new terms and conditions
															are as follows: </p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">1. RENUMERATION</p>
														<p style="margin-bottom: 15px;margin-left: 25px;">Your gross monthly payment shall be stated below and the sum includes consolidated allowances as
															part of your basic salary. The below table represents all the lawful deductions by the existing laws.</p>

														<div class="row">
															<!-- <div class="col-sm-2"> -->
															<table style="width:90%;margin-left:40px; margin-bottom: 15px;">
																<tr>

																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600">Gross Salary </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> NET Salary </p>
																	</th>
																	<!-- </div> -->
																</tr>
																<!-- <div class="col-sm-2"> -->
																<tr>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details['Gross'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details["PayItem{$nm}"] / 12, 2) ?>
																	</td>
																	<td style="padding:5px;">
																		<p> <?= number_format($fin_details['PensionEmployee'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
																	</td>
																</tr>

															</table>
														</div>


														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;font-weight: 600;">2. EMPLOYMENT DETAILS</p>
														<p style="margin-bottom: 15px;margin-left: 30px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</span></p>
														<ul style="margin-left: 10px;margin-bottom: 0px;">
															<li>This is a full-time position. Your regular weekly schedule will be Monday- Friday (7.30 am to 4.30 pm) and Saturday (8:00 am- 4:00 pm) with one (1) hour lunch break. The closing time may be determined by the exigency of work each day and would be advised daily by the School Principal.</li>
															<li>Your schedule of duties remains the same and/ or may be subject to review solely at Management’s directives. If need be, you will be asked to work outside the normal hours and be paid for accordingly.</li>
															<li>During the break, lunch is served at the dining hall in the school premises and the monthly rates are NGN 2500 for Academic staff and NGN 2000 for Non-Academic staff respectively. These rates are subject to change and the changes will be communicated accordingly.
																<p style="margin-bottom:0px;">NOTE: Taking lunch from the school dining hall is not mandatory hence, the fee is only applicable to those who wish to do so. These rates are subject to change and the changes will be communicated accordingly.</p>
															</li>
															<li>The management will activate its policy of Leave without pay or terminate the valid contract during the period of a pandemic or any situation that disrupts the effective running of the school and/or academic activities or in any situation where an employee is not able to perform his duties on account of ill health or any other reasons.</li>
															<li>The amount payable for the hours and days of work done within the period above shall be decided and communicated to the employee by Management in such circumstance(s).</li>
															<li>The organization will make available information on Public Holiday(s) as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday(s), you will be informed accordingly by your School Principal/ Reporting Manager. </li>
														</ul>
														<div class="html2pdf__page-break"></div>

														<p style="margin-bottom: 0px;margin-left: 30px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</span></p>
														<p style="margin-bottom: 0px;margin-left: 60px;"> Confirmed Employee is entitled to any form of Leave. Annual Leave, Maternity Leave, Casual Leave, Compassionate Leave is granted but after twelve (12) months in the organization’s employ
														</p>
														<p style="margin-bottom: 0px;margin-left: 30px;">i. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
															<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
															<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
															<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
															<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
														</ul>


														<p style="margin-bottom: 2px;margin-left: 30px;">ii. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
														<!-- <div class="html2pdf__page-break"></div> -->

														<p style="margin-bottom: 0px;margin-left: 30px;">iii. <span style="margin-left: 10px;font-weight: 600;">Sick Leave</span> </p>
														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>With regards to Sick Leave, according to the Workmen's Compensation Act, an employee shall be entitled to paid wages up to twelve (12) working days in any one calendar year during absence from work caused by temporary illness certified by a medical practitioner or government-certified hospital. The employer reserves the right to require employees to provide a note from the doctor verifying that an absence was caused by a medical situation.</li>
															<li>To encourage good attendance standards, the employer reserves the right to require documentation from the doctor authorizing the employee’s leave extension and when fit to return to work. </li>
														</ul>

														<p style="margin-bottom: 0px;margin-left: 30px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>
														<p style="margin-bottom: 0px;margin-left: 30px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

														<ul style="margin-left: 40px;">
															<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
															<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
															<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
															<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
														</ul>


														<div class="html2pdf__page-break"></div>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest.</p>


														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary, and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information that will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
														<p style="margin-bottom: 0px;margin-left: 30px;">TERMINATION</p>
														<p style="margin-bottom: 0px;margin-left: 40px;"> Upon confirmation, the Employee appointment may be terminated if found guilty of serious or gross misconduct or inefficiency and lack of commitment. Furthermore, at the expiration of your contract, the management are at liberty to end the contract for any other reason or for no reason. </p>

														<p style="margin-left: 40px;margin-bottom: 0px;">The notice will be given as follows:</p>
														<ul style="margin-bottom: 0px;margin-left: 30px">
															<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
															<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
															<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
															<li>One (1) month where the contract has continued for five (5) years or more.</li>
															<li>Any notice for one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														</ul>


														<p style="margin-bottom: 0px;margin-left: 45px;">RESIGNATION </p>
														<ul style="margin-bottom: 0px;margin-left: 30px">
															<li>An employee who has completed less than three (3) years must at least give two (2) weeks' notice with no right of pay-off.
															<li>An Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
															<li> For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in place of notice. </li>
														</ul>
														<p style="margin-bottom: 10px;margin-left: 55px;">Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														<p style="margin-bottom: 0px;margin-left: 25px;">It should be noted that the payment of Pay-Off where applicable shall be processed and completed within two (2) months from the date of issuance of termination letter or end of the resignation notice period.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">7. OTHER TERMS AND CONDITIONS </p>
														<ol type="a" style="margin-left: 20px;margin-bottom: 10px;">
															<li>On assumption of duty, you will be expected to strictly comply with the school’s Rules and Regulations and should not be involved in any conduct which may be against the school’s interest or which may bring disrepute to it </li>

															<li>You are to assume duty as early as possible and for record purposes; you are expected to indicate the exact date on the second copy of this offer letter.</li>
															<div class="html2pdf__page-break"></div>
															<li style="padding-top:10px;">You may be sent on official duties within or outside the state, for instance, entrance examinations, competitions, and workshops. (Expenses will be covered by School) </li>


															<li>You are expected to obey all the rules and regulations (examination, discipline and class teacher, etc.) to run the school effectively.</li>
															<li>No staff shall engage in any form of commercial activity within the school.</li>
														</ol>

														<p style="margin-bottom: 10px;margin-left: 25px;">This agreement is an extract from NTIC Condition of Service and it is subject to changes and periodic review as the need arises. Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>

														<p style="margin-bottom:15px;margin-left: 25px;">By signing the duplicate copy of the renewal of appointment confirms that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ. We look forward to your valuable contributions and wish you the very best for a rewarding career with our organization.</p>
														<p style="margin-bottom: 0px;margin-left: 25px;">Congratulations! </p>


														<!-- </div> -->

														<div class="row mt-4">
															<div class="col-sm-6 ml-4">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYER & DATE</p>
															</div>
															<div class="col-sm-4 ml-4 text-right">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYEE & DATE</p>
															</div>
														</div>









														<!-- /.tab-content -->
													</div>
													<!-- /.nav-tabs-custom -->

													<button class="btn btn-sm btn-danger" id="download" onclick="downloadInvoice1()">Download</button>

												<?php endif; ?>

											</div>
											<!-- /.col -->



											<div class="tab-pane" id="book2">
												<?php if (strtoupper($emp_details['EmpCategory']) === 'ACADEMIC'): ?>
													<div id="invoice2" class="text-dark ">

														<!-- <div class="row justify-content-center mt-1 "> -->
														<div style="margin-bottom:5px;">
															<img src="../pfimg/academic.png" alt="" width="100%" height="100">
														</div>

														<p style="margin-bottom: 1px; font-weight: 600; margin-left: 75%; ">DATE: <?php echo date("d F Y") ?></p>


														<div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
															<div class="row">
																<div class="col-md-10">

																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Branch of school:</span>

																		<?php

																		$sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

																		$sqgR = ScriptRunnercous($sqg);

																		echo ucwords(strtolower($sqgR['OName']));

																		?>

																	</p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?></p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Designation:</span>
																		<?php
																		$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

																		$sqR = ScriptRunnercous($sq);

																		echo (isset($sqR) ? ucwords(strtolower($sqR['ItemPos'])) : '');

																		?>
																	</p>
																</div>
																<div class="col-md-2 text-right">

																	<!-- <p>Branch of school: </p> -->
																	<img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
																</div>
															</div>

														</div>
														<p style="margin-bottom: 30px;margin-left: 25px;">The Management wishes to inform you that your appointment has been reviewed and is valid from
															September 01, 2021 to July 31, 2022 (being one academic session). The new terms and conditions
															are as follows: </p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">1. RENUMERATION</p>
														<p style="margin-bottom: 15px;margin-left: 25px;">Your gross monthly payment shall be stated below and the sum includes consolidated allowances as
															part of your basic salary. The below table represents all the lawful deductions by the existing laws.</p>

														<div class="row">
															<!-- <div class="col-sm-2"> -->
															<table style="width:90%;margin-left:40px; margin-bottom: 15px;">
																<tr>

																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600">Gross Salary </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> NET Salary </p>
																	</th>
																	<!-- </div> -->
																</tr>
																<!-- <div class="col-sm-2"> -->
																<tr>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details['Gross'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details["PayItem{$nm}"] / 12, 2) ?>
																	</td>
																	<td style="padding:5px;">
																		<p> <?= number_format($fin_details['PensionEmployee'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
																	</td>
																</tr>

															</table>
														</div>

														<p style="margin-left: 25px;margin-bottom: 15px;">This salary is for twenty-four (24) lessons a period a week (including Saturdays). It may be any kind of lesson, regular period, Olympiad preparation, supplementary courses, etc. If your period exceeds twenty-four (24) lessons per week, you will be paid extra and the amount for extra lessons will be announced at the beginning of each academic year.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">2. EMPLOYMENT DETAILS</p>
														<p style="margin-bottom: 15px;margin-left: 30px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</span></p>
														<ul style="margin-left: 10px;margin-bottom: 0px;">
															<li>This is a full-time position. Your regular weekly schedule will be Monday- Friday (7.30 am to 4.00 pm) and Saturday (8:00 am - 1:30 pm) with one (1) hour lunch break. The closing time may be determined by the exigency of work each day and would be advised daily by the School Principal.</li>
															<li>You would be required to work on weekends when the need arises without any extra payment where you have not worked for 24 lessons in that week within normal working days to make up for the working period.</li>
															<li>Your schedule of duties remains the same and/ or may be subject to review solely at Management’s directives. If need be, you will be asked to work outside the normal hours and be paid for accordingly.</li>
															<li>During the break, lunch is served at the dining hall in the school premises and the monthly rates are NGN 2500 for Academic staff and NGN 2000 for Non-Academic staff respectively. These rates are subject to change and the changes will be communicated accordingly.
																<p style="margin-bottom:0px;">NOTE: Taking lunch from the school dining hall is not mandatory hence, the fee is only applicable to those who wish to do so. These rates are subject to change and the changes will be communicated accordingly.</p>
															</li>
															<li>You will have one (1) day duty in school and one (1) day prep duty for hostel students. For every week during the prep period, you are expected to assist the students with their academic activities.</li>

															<div class="html2pdf__page-break"></div>

															<li>The management will activate its policy of Leave without pay or terminate the valid contract during the period of a pandemic or any situation that disrupts the effective running of the school and/or academic activities or in any situation where an employee is not able to perform his duties on account of ill health or any other reasons. </li>

															<li>The amount payable for the hours and days of work done within the period above shall be decided and communicated to the employee by Management in such circumstance(s).</li>
															<li>The organization will make available information on Public Holiday(s) as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday(s), you will be informed accordingly by your School Principal/ Reporting Manager. </li>


														</ul>


														<p style="margin-bottom: 0px;margin-left: 30px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</span></p>
														<p style="margin-bottom: 0px;margin-left: 60px;"> Confirmed Employee is entitled to any form of Leave. Annual Leave, Maternity Leave, Casual Leave, Compassionate Leave is granted but after twelve (12) months in the organization’s employ
														</p>
														<p style="margin-bottom: 0px;margin-left: 30px;">i. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
															<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
															<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
															<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
															<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
														</ul>


														<p style="margin-bottom: 2px;margin-left: 30px;">ii. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
														<!-- <div class="html2pdf__page-break"></div> -->

														<p style="margin-bottom: 0px;margin-left: 30px;">iii. <span style="margin-left: 10px;font-weight: 600;">Sick Leave</span> </p>
														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>With regards to Sick Leave, according to the Workmen's Compensation Act, an employee shall be entitled to paid wages up to twelve (12) working days in any one calendar year during absence from work caused by temporary illness certified by a medical practitioner or government-certified hospital. The employer reserves the right to require employees to provide a note from the doctor verifying that an absence was caused by a medical situation.</li>
															<li>To encourage good attendance standards, the employer reserves the right to require documentation from the doctor authorizing the employee’s leave extension and when fit to return to work. </li>
														</ul>

														<p style="margin-bottom: 0px;margin-left: 30px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>

														<p style="margin-bottom: 0px;margin-left: 30px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

														<ul style="margin-left: 40px;">
															<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
															<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
															<div class="html2pdf__page-break"></div>

															<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
															<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
														</ul>



														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest.</p>


														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary, and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information that will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
														<p style="margin-bottom: 0px;margin-left: 30px;">TERMINATION</p>
														<p style="margin-bottom: 0px;margin-left: 40px;"> Upon confirmation, the Employee appointment may be terminated if found guilty of serious or gross misconduct or inefficiency and lack of commitment. Furthermore, at the expiration of your contract, the management are at liberty to end the contract for any other reason or for no reason. </p>

														<p style="margin-left: 40px;margin-bottom: 0px;">The notice will be given as follows:</p>
														<ul style="margin-bottom: 0px;margin-left: 30px">
															<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
															<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
															<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
															<li>One (1) month where the contract has continued for five (5) years or more.</li>
															<li>Any notice for one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														</ul>

														<p style="margin-bottom: 0px;margin-left: 45px;">RESIGNATION </p>
														<ul style="margin-bottom: 5px;margin-left: 30px">
															<li>An employee who has completed less than three (3) years must at least give two (2) weeks' notice with no right of pay-off.
															<li>An Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
															<li> For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in place of notice. </li>
														</ul>
														<p style="margin-bottom: 5px;margin-left: 25px;">Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														<p style="margin-bottom: 0px;margin-left: 25px;">It should be noted that the payment of Pay-Off where applicable shall be processed and completed within two (2) months from the date of issuance of termination letter or end of the resignation notice period.</p>
														<div class="html2pdf__page-break"></div>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px; padding-top:10px;">7. OTHER TERMS AND CONDITIONS </p>
														<ol type="a" style="margin-left: 20px;margin-bottom: 10px;">
															<li>On assumption of duty, you will be expected to strictly comply with the school’s Rules and Regulations and should not be involved in any conduct which may be against the school’s interest or which may bring disrepute to it </li>
															<li>You are to assume duty as early as possible and for record purposes; you are expected to indicate the exact date on the second copy of this offer letter.</li>

															<li>You may be sent on official duties within or outside the state, for instance, entrance examinations, competitions, and workshops. (Expenses will be covered by School) </li>


															<li>You are expected to obey all the rules and regulations (examination, discipline and class teacher, etc.) to run the school effectively.</li>
															<li>No staff shall engage in any form of commercial activity within the school.</li>
														</ol>

														<p style="margin-bottom: 10px;margin-left: 25px;">This agreement is an extract from NTIC Condition of Service and it is subject to changes and periodic review as the need arises. Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>

														<p style="margin-bottom:15px;margin-left: 25px;">By signing the duplicate copy of the renewal of appointment confirms that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ. We look forward to your valuable contributions and wish you the very best for a rewarding career with our organization.</p>
														<p style="margin-bottom: 0px;margin-left: 25px;">Congratulations! </p>



														<!-- </div> -->

														<div class="row mt-4">
															<div class="col-sm-6 ml-4">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYER & DATE</p>
															</div>
															<div class="col-sm-4 ml-4 text-right">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYEE & DATE</p>
															</div>
														</div>






														<!-- /.tab-content -->
													</div>
													<!-- /.nav-tabs-custom -->
													<button class="btn btn-sm btn-danger" id="download2" onclick="downloadInvoice2()">Download</button>
												<?php endif; ?>
											</div>
											<!-- /.col -->

											<!-- <div class="tab-content"> -->


											<div class="tab-pane" id="book3">
												<?php if (strtoupper($emp_details['EmpCategory']) === 'ADMIN'): ?>
													<div id="invoice3" class="text-dark ">

														<!-- <div class="row justify-content-center mt-1 "> -->
														<div id="invoice3" class="text-dark ">
															<div style="margin-bottom:5px;">
																<img src="../pfimg/admin.png" alt="" width="100%" height="100">
															</div>

															<p style="margin-bottom: 1px; font-weight: 600; margin-left: 75%; ">DATE: <?php echo date("d F Y") ?></p>


															<div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
																<div class="row">
																	<div class="col-md-10">

																		<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Branch of school:</span>

																			<?php

																			$sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

																			$sqgR = ScriptRunnercous($sqg);

																			echo ucwords(strtolower($sqgR['OName']));

																			?>


																		</p>
																		<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?></p>
																		<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Designation:</span>
																			<?php

																			$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";
																			$sqR = ScriptRunnercous($sq);

																			echo (isset($sqR) ? ucwords(strtolower($sqR['ItemPos'])) : '');

																			?>
																		</p>
																	</div>
																	<div class="col-md-2 text-right">

																		<!-- <p>Branch of school: </p> -->
																		<img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
																	</div>
																</div>

															</div>
															<p style="margin-bottom: 30px;margin-left: 25px;">The Management wishes to inform you that your appointment has been reviewed and is valid from
																September 01, 2021 to July 31, 2022 (being one academic session). The new terms and conditions
																are as follows: </p>

															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">1. RENUMERATION</p>
															<p style="margin-bottom: 15px;margin-left: 25px;">Your gross monthly payment shall be stated below and the sum includes consolidated allowances as
																part of your basic salary. The below table represents all the lawful deductions by the existing laws.</p>

															<div class="row">
																<!-- <div class="col-sm-2"> -->
																<table style="width:90%;margin-left:40px; margin-bottom: 15px;">
																	<tr>

																		<th>
																			<p style="padding:5px;margin-bottom: 0px; font-weight: 600">Gross Salary </p>
																		</th>
																		<th>
																			<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
																		</th>
																		<th>
																			<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
																		</th>
																		<th>
																			<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
																		</th>
																		<th>
																			<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> NET Salary </p>
																		</th>
																		<!-- </div> -->
																	</tr>
																	<!-- <div class="col-sm-2"> -->
																	<tr>
																		<td style="padding:5px;">
																			<p><?php echo number_format($fin_details['Gross'] / 12, 2) ?></p>
																		</td>
																		<td style="padding:5px;">
																			<p><?php echo number_format($fin_details["PayItem{$nm}"] / 12, 2) ?>
																		</td>
																		<td style="padding:5px;">
																			<p> <?= number_format($fin_details['PensionEmployee'] / 12, 2) ?></p>
																		</td>
																		<td style="padding:5px;">
																			<p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
																		</td>
																		<td style="padding:5px;">
																			<p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
																		</td>
																	</tr>

																</table>
															</div>


															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;font-weight: 600;">2. EMPLOYMENT DETAILS</p>
															<p style="margin-bottom: 15px;margin-left: 30px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</span></p>
															<ul style="margin-left: 10px;margin-bottom: 0px;">
																<li>This is a full-time position. Your regular weekly schedule will be Monday- Friday (7.30 am to 4.30 pm) and Saturday (8:00 am- 4:00 pm) with one (1) hour lunch break. The closing time may be determined by the exigency of work each day and would be advised daily by the School Principal.</li>
																<li>Your schedule of duties remains the same and/ or may be subject to review solely at Management’s directives. If need be, you will be asked to work outside the normal hours and be paid for accordingly.</li>
																<li>During the break, lunch is served at the dining hall in the school premises and the monthly rates are NGN 2500 for Academic staff and NGN 2000 for Non-Academic staff respectively. These rates are subject to change and the changes will be communicated accordingly.
																	<p style="margin-bottom:0px;">NOTE: Taking lunch from the school dining hall is not mandatory hence, the fee is only applicable to those who wish to do so. These rates are subject to change and the changes will be communicated accordingly.</p>
																</li>
																<li>The management will activate its policy of Leave without pay or terminate the valid contract during the period of a pandemic or any situation that disrupts the effective running of the school and/or academic activities or in any situation where an employee is not able to perform his duties on account of ill health or any other reasons.</li>
																<li>The amount payable for the hours and days of work done within the period above shall be decided and communicated to the employee by Management in such circumstance(s).</li>
																<li>The organization will make available information on Public Holiday(s) as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday(s), you will be informed accordingly by your School Principal/ Reporting Manager. </li>
															</ul>
															<div class="html2pdf__page-break"></div>

															<p style="margin-bottom: 0px;margin-left: 30px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</span></p>
															<p style="margin-bottom: 0px;margin-left: 60px;"> Confirmed Employee is entitled to any form of Leave. Annual Leave, Maternity Leave, Casual Leave, Compassionate Leave is granted but after twelve (12) months in the organization’s employ
															</p>
															<p style="margin-bottom: 0px;margin-left: 30px;">i. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

															<ul style="margin-left: 40px;margin-bottom: 0px;">
																<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
																<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
																<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
																<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
																<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
															</ul>


															<p style="margin-bottom: 2px;margin-left: 30px;">ii. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
															<!-- <div class="html2pdf__page-break"></div> -->

															<p style="margin-bottom: 0px;margin-left: 30px;">iii. <span style="margin-left: 10px;font-weight: 600;">Sick Leave</span> </p>
															<ul style="margin-left: 40px;margin-bottom: 0px;">
																<li>With regards to Sick Leave, according to the Workmen's Compensation Act, an employee shall be entitled to paid wages up to twelve (12) working days in any one calendar year during absence from work caused by temporary illness certified by a medical practitioner or government-certified hospital. The employer reserves the right to require employees to provide a note from the doctor verifying that an absence was caused by a medical situation.</li>
																<li>To encourage good attendance standards, the employer reserves the right to require documentation from the doctor authorizing the employee’s leave extension and when fit to return to work. </li>
															</ul>

															<p style="margin-bottom: 0px;margin-left: 30px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>
															<p style="margin-bottom: 0px;margin-left: 30px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

															<ul style="margin-left: 40px;">
																<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
																<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
																<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
																<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
															</ul>


															<div class="html2pdf__page-break"></div>

															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
															<p style="margin-left: 25px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest.</p>


															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
															<p style="margin-left: 25px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
															<p style="margin-left: 25px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary, and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information that will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
															<p style="margin-bottom: 0px;margin-left: 30px;">TERMINATION</p>
															<p style="margin-bottom: 0px;margin-left: 40px;"> Upon confirmation, the Employee appointment may be terminated if found guilty of serious or gross misconduct or inefficiency and lack of commitment. Furthermore, at the expiration of your contract, the management are at liberty to end the contract for any other reason or for no reason. </p>

															<p style="margin-left: 40px;margin-bottom: 0px;">The notice will be given as follows:</p>
															<ul style="margin-bottom: 0px;margin-left: 30px">
																<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
																<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
																<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
																<li>One (1) month where the contract has continued for five (5) years or more.</li>
																<li>Any notice for one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
															</ul>


															<p style="margin-bottom: 0px;margin-left: 45px;">RESIGNATION </p>
															<ul style="margin-bottom: 0px;margin-left: 30px">
																<li>An employee who has completed less than three (3) years must at least give two (2) weeks' notice with no right of pay-off.
																<li>An Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
																<li> For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in place of notice. </li>
															</ul>
															<p style="margin-bottom: 10px;margin-left: 55px;">Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
															<p style="margin-bottom: 0px;margin-left: 25px;">It should be noted that the payment of Pay-Off where applicable shall be processed and completed within two (2) months from the date of issuance of termination letter or end of the resignation notice period.</p>

															<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">7. OTHER TERMS AND CONDITIONS </p>
															<ol type="a" style="margin-left: 20px;margin-bottom: 10px;">
																<li>On assumption of duty, you will be expected to strictly comply with the school’s Rules and Regulations and should not be involved in any conduct which may be against the school’s interest or which may bring disrepute to it </li>
																<li>You are to assume duty as early as possible and for record purposes; you are expected to indicate the exact date on the second copy of this offer letter.</li>
																<div class="html2pdf__page-break"></div>
																<li style="padding-top:10px;">You may be sent on official duties within or outside the state, for instance, entrance examinations, competitions, and workshops. (Expenses will be covered by School) </li>


																<li>You are expected to obey all the rules and regulations (examination, discipline and class teacher, etc.) to run the school effectively.</li>
																<li>No staff shall engage in any form of commercial activity within the school.</li>
															</ol>

															<p style="margin-bottom: 10px;margin-left: 25px;">This agreement is an extract from NTIC Condition of Service and it is subject to changes and periodic review as the need arises. Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>

															<p style="margin-bottom:15px;margin-left: 25px;">By signing the duplicate copy of the renewal of appointment confirms that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ. We look forward to your valuable contributions and wish you the very best for a rewarding career with our organization.</p>
															<p style="margin-bottom: 0px;margin-left: 25px;">Congratulations! </p>


															<!-- </div> -->

															<div class="row mt-4">
																<div class="col-sm-6 ml-4">
																	<p>...............................................</p>
																	<p style="font-weight: 600">EMPLOYER & DATE</p>
																</div>
																<div class="col-sm-4 ml-4 text-right">
																	<p>...............................................</p>
																	<p style="font-weight: 600">EMPLOYEE & DATE</p>
																</div>
															</div>






															<!-- /.tab-content -->
														</div>
														<!-- /.nav-tabs-custom -->




													</div>
													<button class="btn btn-sm btn-danger" id="download3" onclick="downloadInvoice3()">Download</button>
												<?php endif; ?>







											</div>


											<div class="tab-pane  " id="book4">

												<div id="invoice4" class="text-dark ">

													<?php if ($other): ?>

														<div class="row">
															<div class="col-sm-10  mx-auto">
																<div class="row text-center">
																	<div class="col-sm-2">
																		<!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
																	</div>

																</div>
															</div>
														</div>
														<!-- <div class="row justify-content-center mt-1 "> -->
														<div style="margin-bottom:5px;">
															<img src="../pfimg/non_academic.jpg" alt="" width="100%" height="100">
														</div>

														<p style="margin-bottom: 1px; font-weight: 600; margin-left: 75%; ">DATE: <?php echo date("d F Y") ?></p>


														<div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
															<div class="row">
																<div class="col-md-10">

																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Branch of school:</span>
																		<?php

																		$sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

																		$sqgR = ScriptRunnercous($sqg);

																		echo ucwords(strtolower($sqgR['OName']));

																		?>

																	</p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?></p>
																	<p style="margin-bottom: 0px;"><span style="font-weight: 600;">Designation:</span>
																		<?php

																		$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

																		$sqR = ScriptRunnercous($sq);

																		echo (isset($sqR) ? ucwords(strtolower($sqR['ItemPos'])) : '');

																		?>
																	</p>
																</div>
																<div class="col-md-2 text-right">

																	<!-- <p>Branch of school: </p> -->
																	<img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
																</div>
															</div>

														</div>
														<p style="margin-bottom: 30px;margin-left: 25px;">The Management wishes to inform you that your appointment has been reviewed and is valid from
															September 01, 2021 to July 31, 2022 (being one academic session). The new terms and conditions
															are as follows: </p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">1. RENUMERATION</p>
														<p style="margin-bottom: 15px;margin-left: 25px;">Your gross monthly payment shall be stated below and the sum includes consolidated allowances as
															part of your basic salary. The below table represents all the lawful deductions by the existing laws.</p>

														<div class="row">
															<!-- <div class="col-sm-2"> -->
															<table style="width:90%;margin-left:40px; margin-bottom: 15px;">
																<tr>

																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600">Gross Salary </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
																	</th>
																	<th>
																		<p style="padding:5px;margin-bottom: 0px; font-weight: 600"> NET Salary </p>
																	</th>
																	<!-- </div> -->
																</tr>
																<!-- <div class="col-sm-2"> -->
																<tr>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details['Gross'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?php echo number_format($fin_details["PayItem{$nm}"] / 12, 2) ?>
																	</td>
																	<td style="padding:5px;">
																		<p> <?= number_format($fin_details['PensionEmployee'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
																	</td>
																	<td style="padding:5px;">
																		<p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
																	</td>
																</tr>

															</table>
														</div>


														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;font-weight: 600;">2. EMPLOYMENT DETAILS</p>
														<p style="margin-bottom: 15px;margin-left: 30px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</span></p>
														<ul style="margin-left: 10px;margin-bottom: 0px;">
															<li>This is a full-time position. Your regular weekly schedule will be <?php echo $_POST['workSchedule']; ?> with one (1) hour lunch break. The closing time may be determined by the exigency of work each day and would be advised daily by the School Principal.</li>
															<li>Your schedule of duties remains the same and/ or may be subject to review solely at Management’s directives. If need be, you will be asked to work outside the normal hours and be paid for accordingly.</li>
															<li>During the break, lunch is served at the dining hall in the school premises and the monthly rates are NGN 2500 for Academic staff and NGN 2000 for Non-Academic staff respectively. These rates are subject to change and the changes will be communicated accordingly.
																<p style="margin-bottom:0px;">NOTE: Taking lunch from the school dining hall is not mandatory hence, the fee is only applicable to those who wish to do so. These rates are subject to change and the changes will be communicated accordingly.</p>
															</li>
															<li>The management will activate its policy of Leave without pay or terminate the valid contract during the period of a pandemic or any situation that disrupts the effective running of the school and/or academic activities or in any situation where an employee is not able to perform his duties on account of ill health or any other reasons.</li>
															<li>The amount payable for the hours and days of work done within the period above shall be decided and communicated to the employee by Management in such circumstance(s).</li>
															<li>The organization will make available information on Public Holiday(s) as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday(s), you will be informed accordingly by your School Principal/ Reporting Manager. </li>
														</ul>
														<div class="html2pdf__page-break"></div>

														<p style="margin-bottom: 0px;margin-left: 30px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</span></p>
														<p style="margin-bottom: 0px;margin-left: 60px;"> Confirmed Employee is entitled to any form of Leave. Annual Leave, Maternity Leave, Casual Leave, Compassionate Leave is granted but after twelve (12) months in the organization’s employ
														</p>
														<p style="margin-bottom: 0px;margin-left: 30px;">i. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
															<li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
															<li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
															<li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
															<li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
														</ul>


														<p style="margin-bottom: 2px;margin-left: 30px;">ii. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
														<!-- <div class="html2pdf__page-break"></div> -->

														<p style="margin-bottom: 0px;margin-left: 30px;">iii. <span style="margin-left: 10px;font-weight: 600;">Sick Leave</span> </p>
														<ul style="margin-left: 40px;margin-bottom: 0px;">
															<li>With regards to Sick Leave, according to the Workmen's Compensation Act, an employee shall be entitled to paid wages up to twelve (12) working days in any one calendar year during absence from work caused by temporary illness certified by a medical practitioner or government-certified hospital. The employer reserves the right to require employees to provide a note from the doctor verifying that an absence was caused by a medical situation.</li>
															<li>To encourage good attendance standards, the employer reserves the right to require documentation from the doctor authorizing the employee’s leave extension and when fit to return to work. </li>
														</ul>

														<p style="margin-bottom: 0px;margin-left: 30px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>
														<p style="margin-bottom: 0px;margin-left: 30px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

														<ul style="margin-left: 40px;">
															<li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
															<li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
															<li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
															<li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
														</ul>


														<div class="html2pdf__page-break"></div>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest.</p>


														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
														<p style="margin-left: 25px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary, and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information that will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
														<p style="margin-bottom: 0px;margin-left: 30px;">TERMINATION</p>
														<p style="margin-bottom: 0px;margin-left: 40px;"> Upon confirmation, the Employee appointment may be terminated if found guilty of serious or gross misconduct or inefficiency and lack of commitment. Furthermore, at the expiration of your contract, the management are at liberty to end the contract for any other reason or for no reason. </p>

														<p style="margin-left: 40px;margin-bottom: 0px;">The notice will be given as follows:</p>
														<ul style="margin-bottom: 0px;margin-left: 30px">
															<li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
															<li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
															<li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
															<li>One (1) month where the contract has continued for five (5) years or more.</li>
															<li>Any notice for one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														</ul>


														<p style="margin-bottom: 0px;margin-left: 45px;">RESIGNATION </p>
														<ul style="margin-bottom: 0px;margin-left: 30px">
															<li>An employee who has completed less than three (3) years must at least give two (2) weeks' notice with no right of pay-off.
															<li>An Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
															<li> For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in place of notice. </li>
														</ul>
														<p style="margin-bottom: 10px;margin-left: 55px;">Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
														<p style="margin-bottom: 0px;margin-left: 25px;">It should be noted that the payment of Pay-Off where applicable shall be processed and completed within two (2) months from the date of issuance of termination letter or end of the resignation notice period.</p>

														<p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">7. OTHER TERMS AND CONDITIONS </p>
														<ol type="a" style="margin-left: 20px;margin-bottom: 10px;">
															<li>On assumption of duty, you will be expected to strictly comply with the school’s Rules and Regulations and should not be involved in any conduct which may be against the school’s interest or which may bring disrepute to it </li>

															<li>You are to assume duty as early as possible and for record purposes; you are expected to indicate the exact date on the second copy of this offer letter.</li>
															<div class="html2pdf__page-break"></div>
															<li style="padding-top:10px;">You may be sent on official duties within or outside the state, for instance, entrance examinations, competitions, and workshops. (Expenses will be covered by School) </li>


															<li>You are expected to obey all the rules and regulations (examination, discipline and class teacher, etc.) to run the school effectively.</li>
															<li>No staff shall engage in any form of commercial activity within the school.</li>
														</ol>

														<p style="margin-bottom: 10px;margin-left: 25px;">This agreement is an extract from NTIC Condition of Service and it is subject to changes and periodic review as the need arises. Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>

														<p style="margin-bottom:15px;margin-left: 25px;">By signing the duplicate copy of the renewal of appointment confirms that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ. We look forward to your valuable contributions and wish you the very best for a rewarding career with our organization.</p>
														<p style="margin-bottom: 0px;margin-left: 25px;">Congratulations! </p>


														<!-- </div> -->

														<div class="row mt-4">
															<div class="col-sm-6 ml-4">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYER & DATE</p>
															</div>
															<div class="col-sm-4 ml-4 text-right">
																<p>...............................................</p>
																<p style="font-weight: 600">EMPLOYEE & DATE</p>
															</div>
														</div>









														<!-- /.tab-content -->
												</div>
												<!-- /.nav-tabs-custom -->

												<button class="btn btn-sm btn-danger" id="download4" onclick="downloadInvoice4()">Download</button>

												<?php //endif;
												?>

											</div>
											<!-- /.col -->



										<?php else : ?>



											<form action="" method="post" id="scheduleProcess" enctype="multipart/form-data">
												<div class="form-group">
													<label class="col-sm-4 col-form-label">Work Schedule <span style="color: red">*</span>:</label>
													<div class="col-sm-6 ">
														<textarea type="text" rows="3" name="workSchedule" maxlength="200" class="form-control" id="workSchedule"></textarea>
													</div>
												</div>

												<input type="hidden" name="other" value="<?= $_POST['bookselect1']  ?>">
												<div class="col-md-12 text-left">
													<input id="scheduleForm" name="scheduleForm" type="submit" class="btn btn-danger btn-sm" value="Submit" />
												</div>
											</form>


										<?php endif; ?>



										</div>

									</div>

								</div>

							<?php endif; ?>




							</div>
						</div>
						<!-- /.box-body -->




					</div>

					<!-- /.row -->
	</section>
	<!-- /.content -->


	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="html2pdf/dist/html2pdf.bundle.js"></script>


	<script>
		function downloadInvoice1() {
			const invoice = document.getElementById("invoice");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_NON_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.90, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice).set(opt).save();

			html2pdf().from(invoice).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice2() {
			const invoice2 = document.getElementById("invoice2");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice2).set(opt).save();

			html2pdf().from(invoice2).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice3() {

			const invoice3 = document.getElementById("invoice3");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ADMIN_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice3).set(opt).save();

			html2pdf().from(invoice3).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice4() {

			const invoice4 = document.getElementById("invoice4");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_NON_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice4).set(opt).save();

			html2pdf().from(invoice4).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}
	</script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->

	<script type="text/javascript">



	</script>


	<script>
		$('#example1').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example6').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example7').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		setInterval(function() {
			$(".tablesorter-filter-row").remove();
		}, 2000);

		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});



		}



		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select an employee', 'red');
				};

				return;

			} else {

				$('#theform1').submit();





			}



		});


		if (<?= json_encode($emp_details) ?>) {
			const emp_dets = <?= json_encode($emp_details) ?>;
			// console.log(emp_dets.EmpCategory);
			if ((emp_dets.EmpCategory).toUpperCase() === 'NON-ACADEMIC') {
				activaTab('book');
			}
			if ((emp_dets.EmpCategory).toUpperCase() === 'ACADEMIC') {
				activaTab('book2');
			}
			if ((emp_dets.EmpCategory).toUpperCase() === 'ADMIN') {
				activaTab('book3');
			}

		}

		if (<?= json_encode($_POST['scheduleForm']) ?>) {
			// alert('here');
			activaTab('book4');
		}


		function activaTab(tab) {
			// alert("here2");
			$('.nav-tabs a[href="#' + tab + '"]').tab('show');
		};



		// $("#scheduleForm").click(function(){

		// 		const workSchedule = $("#workSchedule").val();

		// 		 if(workSchedule === " " ){

		// 				  { parent.msgbox('Enter work schedule ', 'red'); };

		// 		 }
		// 		 else{

		// 			document.getElementById('scheduleProcess').submit();

		// 		 }

		// 	  });
	</script>
</body>

</html>
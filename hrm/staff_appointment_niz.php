<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <div class="row">

                <div class="col-md-6  text-md-left text-center">
                    <h3>
                        Generate Employee Renewal Letter
                    </h3>

                </div>
                <div class="col-md-6">
                    <form id="theform1" method="post">
                        <div class="row">

                            <div class="col-4"></div>
                            <!--  <div class="col-1" style="margin-top:2%;" >

                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
              </div> -->
                            <!-- <div class="col-1" style="margin-top:2%;" >



                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
              </div> -->
                            <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                                <select class="form-control select2" name="bookselect1" id="bookselect1">
                                    <!-- <select name="level1" id="level1"  class="form-control" > -->
                                    <option value="">--</option>

                                    <?php
                                    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                                    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                                    if ($conn2 === false) {
                                        echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                                    }
                                    $dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

                                    $result2 = sqlsrv_query($conn2, $dbOpen4);
                                    if ($result2 === false) {
                                        die(print_r(sqlsrv_errors(), true));
                                    }
                                    $emp_hash = '';
                                    if (!is_null($emp_details) && !empty($emp_details)) {
                                        $emp_hash = $emp_details['HashKey'];
                                    }
                                    while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
                                        <option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
                                            <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
                                        </option>
                                    <?php endwhile;
                                    sqlsrv_free_stmt($result2);

                                    ?>



                                </select>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger" id="getall1">Open</button>
                                </span>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="box-body">



            <?php if (!is_null($emp_details) && !empty($emp_details) || isset($_POST['scheduleForm'])): ?>
                <div class="row">


                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="nav-tabs-custom" style="min-height: 400px">
                                    <ul class="nav nav-tabs">
                                        <li>
                                            <a href="#book" data-toggle="tab">
                                                NON-MEDICAL STAFF
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#book3" data-toggle="tab">
                                                ADMIN STAFF
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#book2" data-toggle="tab">
                                                MEDICAL STAFF
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#book6" data-toggle="tab">
                                                EXPATRIATE STAFF
                                            </a>
                                        </li>


                                        <!-- <li>
                              <a href="#book5" data-toggle="tab">
                                   SUPER ADMIN STAFF
                              </a>
                          </li>

                       

                          <li>
                              <a href="#book4" data-toggle="tab">
                                   OTHERS
                              </a>
                          </li> -->

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane  " id="book">
                                            <?php if (strtoupper($emp_details['EmpCategory']) === 'NON MEDICAL'): ?>
                                                <div id="invoice" class="text-dark ">
                                                    <div class="row">
                                                        <div class="col-sm-10  mx-auto">
                                                            <div class="row text-center">
                                                                <div class="col-sm-2">
                                                                    <!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width:90%;margin-left:40px;">
                                                        <div class="col-sm-3">
                                                            <!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="60%" height="80%"> -->
                                                            <img src="<?= $ComLogo ?>" alt="" style="margin-top:-20px;width: 200px;height: 120px;" />
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div class="row align-items-center">
                                                                <div class="col-sm-9 text-center" style="font-weight: 600;">
                                                                    <span>NIZAMIYE HOSPITAL LTD</span><br>
                                                                    <span>RENEWAL OF APPOINTMENT FOR</span>
                                                                    <p>NON MEDICAL STAFF</p>
                                                                </div>
                                                                <div class="col-sm-3 d-flex justify-content-end mt-3">
                                                                    <div style="font-weight: 600;">RC882824</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p style="margin: 0px;">
                                                        <hr style="border: 1px solid black;margin:0px;">
                                                    </p>
                                                    <div style="margin-bottom:5px;">
                                                    </div>
                                                    <div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">


                                                        <div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo strtoupper("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}") ?></p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Designation:</span>
                                                                        <?php

                                                                        $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

                                                                        $sqR = ScriptRunnercous($sq);

                                                                        echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');

                                                                        ?>
                                                                    </p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Section of Hospital:</span> <?= strtoupper($emp_details['EmpCategory']) ?>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Renewal Date:</span> <?php echo date("d F Y") ?></p>

                                                                </div>
                                                                <div class="col-md-2 text-right">
                                                                    <img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <ul style="font-size:small;">
                                                            <li>With reference to the earlier agreement signed, the management of Nizamiye Hospital wish to inform you that your appointment has been reviewed and the new terms and conditions are as follows;</li>
                                                            <li>We are aware and would like to remind you that your first appointment with Nizamiye Hospital took effect from <span style="font-weight: 600;"><?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("dS F Y")  : ""   ?></span>.</li>
                                                            <li>Your net monthly pay shall be N<?= number_format($fin_details['NetPay'] / 12, 2) ?>, below is the breakdown of your monthly net salary</li>
                                                        </ul>

                                                        <?php

                                                        $netPay = $fin_details['NetPay'] / 12;
                                                        $basicPay = ($netPay * 25) / 100;
                                                        $transport = ($netPay * 15) / 100;
                                                        $housing = ($netPay * 10) / 100;
                                                        $hazard = ($netPay * 35) / 100;
                                                        $dressing = ($netPay * 10) / 100;
                                                        $utilities = ($netPay * 5) / 100;

                                                        ?>

                                                        <table align="center" cellpadding="1" cellspacing="1" style="width:500px">
                                                            <tbody>
                                                                <tr>
                                                                    <td>BASIC</td>
                                                                    <td><?= number_format($basicPay, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>ALLOWANCES:</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>TRANSPORT</td>
                                                                    <td><?= number_format($transport, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HOUSING</td>
                                                                    <td><?= number_format($housing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HAZARD</td>
                                                                    <td><?= number_format($hazard, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>DRESSING</td>
                                                                    <td><?= number_format($dressing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>UTILITIES</td>
                                                                    <td><?= number_format($utilities, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NET SALARY</td>
                                                                    <td><?= number_format($netPay, 2) ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <p style="margin-bottom: 0px;margin-left: 25px;margin-top: 10px;">I. <span style="margin-left: 16px; font-size:small;"> Pension shall be calculated and deducted in accordance with the Pension Act. If staff doesn't submit their PIN registration number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 25px;">II. <span style="margin-left: 12px; font-size:small;"> Personal Income Tax (PAYE) shall be calculated and paid to FIRS in accordance with Personal Income tax act.</span></p>
                                                        <p style="margin-bottom: 0x;margin-left: 25px;">III. <span style="margin-left: 8px;font-size:small;"> National Housing Fund (NHF) shall be calculated and paid in accordance with NHF act. If staff doesn't submit their NHF number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 80px;font-weight: 600;text-decoration:underline;">The detailed composition of the remuneration shall be as stated below:</p>

                                                        <ul style="font-size:small;margin-bottom: 0px;">
                                                            <li>Your salary is as indicated above, but you will be paid overtime duties as may be scheduled by your Head of Department in a month. Further details are contained in the condition of service booklet which is subject to changes upon renewal of your appointment.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm for Morning duty, 5.00pm to 8.00am for evening duty from Mondays to Sundays depending on the duty (Morning or Evening) you are on for staff on roster.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm from Monday to Friday, and 8.00am to 2.00pm on Saturdays for permanent morning staff</li>
                                                            <li>Closing time may be determined by the exigency of work each day and would be advised daily by Head of Department. Also, you would be required to work on public holidays when the need arises with extra payment as overtime, or to observe your holiday on a later date or on another public holiday.</li>
                                                            <li>You are entitled to annual leave according to the following schedule:
                                                                <ul>
                                                                    <li>18 days, for staff who have served with the hospital a period more than one year but less than five years.</li>
                                                                    <li>21 days, for staff who have served with the hospital a period more than five years but less than ten years.</li>
                                                                    <li>30 days, for staff who have served with the Hospital more than ten year.</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <p style="margin-top: 0px;margin-left: 25px;font-size:small;">Your previous rights will be conserved and the leave will mostly be taken during the long vacation in accordance with a predetermined schedule and your salary will run normally during the period.</p>
                                                    <p style="font-weight: 600;margin-bottom:0px;margin-left: 25px;">OTHER TERMS AND CONDITIONS:</p>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">A. The duration of your appointment is for two (2) years and the initial three months shall be a probationary period with effect from the date you assume duty;</p>

                                                    <div class="html2pdf__page-break"></div>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">B. At the end of the probation period, the Management may confirm your appointment as a permanent staff, extend your probation for a further trial period, or terminate your appointment.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">C. The following documents should be submitted before starting your duty.</p>
                                                    <ol style="font-size:small;margin-left:20px;margin-bottom:0px;">
                                                        <li style="font-weight: 600;">The photocopy of all your credentials </li>
                                                        <li style="font-weight: 600;">Letter of satisfactory medical report on the state of your health.</li>
                                                        <li style="font-weight: 600;">The photocopy of National ID card</li>
                                                        <li style="font-weight: 600;">If no ID card, reference letter from your local Police Department</li>
                                                        <li style="font-weight: 600;">Personal CV and valid practicing license (Medical Staff only)</li>
                                                        <li style="font-weight: 600;">4 passport photographs</li>
                                                        <li style="font-weight: 600;">Letter of satisfactory reference from your previous employers and from personal referees given by you is a pre-condition of this employment;</li>
                                                    </ol>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">D. From the date you assume duty, other terms and conditions of service of the Hospital will apply to you;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">E. On assumption of duty, you will be expected to strictly comply with the Hospital's Rules and Regulations and should not be involved in any conduct which may be against the hospital's interest or which may bring disrepute to it;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">F.You are expected to assume duty as early as possible in order to finish documentation process on time and start the work.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">G. No staff shall engage in any form of commercial activity within the Hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">H. Any staff found acting against any of the terms and conditions contained herein shall be given a query and a warning letter.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">I. In the situation where a staff receives up to 3 warning letters, his or her appointment may be terminated. In which case, the staff will not be paid any additional amount except for the days he/she worked.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">J. If the management is not satisfied with a staff&rsquo;s performance, his or her appointment will be terminated with or without a notice.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">K. Working in another place while working with Nizamiye hospital will render this contract invalid</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">The notice will be given as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>One day before the termination of the contract where the contract has continued for a period of three months or less,</li>
                                                        <li>One week before the termination of the contract where the contract has continued for more than three months but less than two years,</li>
                                                        <li>Two weeks before the termination of the contract where the contract has continued for a period of two years but less than five years,</li>
                                                        <li>One month before the termination of the contract where the contract has continued for five years and more.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">L. A staff must submit his/her resignation one month before the planned date of stopping work. Failure to do so will make the staff forfeit one month salary to the hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">M. If, at the Hospital&rsquo;s discretion, the employee&rsquo;s appointment is terminated without his/her notice, then he/she will receive the appropriate amount of payment in lieu of notice.</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>The staff who did not complete (3) three years with the Hospital, will not have any right of pay-off</li>
                                                        <li>The staff that completed more than (3) three and less than (5) five years, one month Net salary for each completed year.</li>
                                                        <li>The staff who completed (5) five and more years, one month Net Salary for each completed year.</li>
                                                        <li>However, this payment is not applicable when a staff is found guilty of gross misconduct.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">N. In the event when staff gives notice of resignation, the payment schedule will be as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Below three years: no right for pay-off.</li>
                                                        <li>Three years and above: one month Net salary for each competed year of service.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">O. The following is our pregnancy policy for female staff:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Any female staff that is not above one (1) year of contract is not entitled to maternity leave.</li>
                                                        <li>Maternity leave is four (4) weeks before and eight (8) weeks after delivery, that is three (3) months total.</li>
                                                        <li>Maternity leave automatically cancels annual leave of that particular year.</li>
                                                        <li>Resumption from maternity leave after 3 months accompanies Morning duty from 8am to 3pm.</li>
                                                        <li>After baby reaches 6 months, staffs should resume Morning duty from 8am to 4pm until baby is one (1) year old.</li>
                                                        <li>After baby reaches nine (9) months, staffs are expected to resume regular duty (Morning and Night).</li>
                                                        <li>Any staff who gets pregnant less than 3 years from previous delivery, will not be entitled to maternity leave.</li>
                                                        <li>Your marital and pregnancy status must be known before signing this contract. Providing wrong or deceitful information will render this contract invalid.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">P. Wedding leave is 7 days for both Male and Female staffs.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">Q. Paternity leave is 3 days.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">R. This agreement is an extract from condition of service booklet and it is subject to changes and renewal yearly.</p>
                                                    <!-- <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">S. Please confirm your acceptance of this offer and terms and conditions therein by signing below.</p> -->
                                                    <!-- </div> -->

                                                    <div class="row">
                                                        <div class="col-sm-6 ml-4">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style=" font-weight: 600; margin-bottom:5px">EMPLOYEE/ DATE</p>
                                                        </div>
                                                        <div class="col-sm-4 ml-4 text-right">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style="font-weight: 600;  margin-bottom:5px">MANAGEMENT/ DATE</p>
                                                        </div>
                                                    </div>



                                                    <div class="row d-flex justify-content-between px-5" style="margin-top: 0px;">

                                                        <div class="" style="margin-left: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/hos.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="" style="margin-right: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/branch.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>











                                                    <!-- /.tab-content -->
                                                </div>
                                                <!-- /.nav-tabs-custom -->

                                                <button class="btn btn-sm btn-danger p-3 text-white" id="download" onclick="downloadInvoice1()">Print</button>

                                            <?php endif; ?>

                                        </div>
                                        <!-- /.col -->

                                        <div class="tab-pane  " id="book3">
                                            <?php if (strtoupper($emp_details['EmpCategory']) === 'ADMIN'): ?>
                                                <div id="invoice55" class="text-dark ">
                                                    <div class="row">
                                                        <div class="col-sm-10  mx-auto">
                                                            <div class="row text-center">
                                                                <div class="col-sm-2">
                                                                    <!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width:90%;margin-left:40px;">
                                                        <div class="col-sm-3">
                                                            <!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="60%" height="80%"> -->
                                                            <img src="<?= $ComLogo ?>" alt="" style="margin-top:-20px;width: 200px;height: 100px;" />
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div class="row align-items-center">
                                                                <div class="col-sm-9 text-center" style="font-weight: 600;">
                                                                    <span>NIZAMIYE HOSPITAL LTD</span><br>
                                                                    <span>RENEWAL OF APPOINTMENT FOR</span>
                                                                    <p>ADMIN STAFF</p>
                                                                </div>
                                                                <div class="col-sm-3 d-flex justify-content-end mt-3">
                                                                    <div style="font-weight: 600;">RC882824</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p style="margin: 0px;">
                                                        <hr style="border: 1px solid black;margin:0px;">
                                                    </p>
                                                    <div style="margin-bottom:5px;">
                                                    </div>
                                                    <div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">


                                                        <div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo strtoupper("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}") ?></p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Designation:</span>
                                                                        <?php

                                                                        $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

                                                                        $sqR = ScriptRunnercous($sq);

                                                                        echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');

                                                                        ?>
                                                                    </p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Section of Hospital:</span> <?= strtoupper($emp_details['EmpCategory']) ?>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Renewal Date:</span> <?php echo date("d F Y") ?></p>

                                                                </div>
                                                                <div class="col-md-2 text-right">
                                                                    <img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <ul style="font-size:small;">
                                                            <li>With reference to the earlier agreement signed, the management of Nizamiye Hospital wish to inform you that your appointment has been reviewed and the new terms and conditions are as follows;</li>
                                                            <li>We are aware and would like to remind you that your first appointment with Nizamiye Hospital took effect from <span style="font-weight: 600;"><?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("dS F Y")  : ""   ?></span>.</li>
                                                            <li>Your net monthly pay shall be N<?= number_format($fin_details['NetPay'] / 12, 2) ?>, below is the breakdown of your monthly net salary</li>
                                                        </ul>

                                                        <?php

                                                        $netPay = $fin_details['NetPay'] / 12;
                                                        $basicPay = ($netPay * 25) / 100;
                                                        $transport = ($netPay * 15) / 100;
                                                        $housing = ($netPay * 10) / 100;
                                                        $hazard = ($netPay * 35) / 100;
                                                        $dressing = ($netPay * 10) / 100;
                                                        $utilities = ($netPay * 5) / 100;

                                                        ?>

                                                        <table align="center" cellpadding="1" cellspacing="1" style="width:500px">
                                                            <tbody>
                                                                <tr>
                                                                    <td>BASIC</td>
                                                                    <td><?= number_format($basicPay, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>ALLOWANCES:</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>TRANSPORT</td>
                                                                    <td><?= number_format($transport, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HOUSING</td>
                                                                    <td><?= number_format($housing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HAZARD</td>
                                                                    <td><?= number_format($hazard, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>DRESSING</td>
                                                                    <td><?= number_format($dressing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>UTILITIES</td>
                                                                    <td><?= number_format($utilities, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NET SALARY</td>
                                                                    <td><?= number_format($netPay, 2) ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <p style="margin-bottom: 0px;margin-left: 25px;margin-top: 10px;">I. <span style="margin-left: 16px; font-size:small;"> Pension shall be calculated and deducted in accordance with the Pension Act. If staff doesn't submit their PIN registration number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 25px;">II. <span style="margin-left: 12px; font-size:small;"> Personal Income Tax (PAYE) shall be calculated and paid to FIRS in accordance with Personal Income tax act.</span></p>
                                                        <p style="margin-bottom: 0x;margin-left: 25px;">III. <span style="margin-left: 8px;font-size:small;"> National Housing Fund (NHF) shall be calculated and paid in accordance with NHF act. If staff doesn't submit their NHF number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 80px;font-weight: 600;text-decoration:underline;">The detailed composition of the remuneration shall be as stated below:</p>

                                                        <ul style="font-size:small;margin-bottom: 0px;">
                                                            <li>Your salary is as indicated above, but you will be paid overtime duties as may be scheduled by your Head of Department in a month. Further details are contained in the condition of service booklet which is subject to changes upon renewal of your appointment.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm for Morning duty, 5.00pm to 8.00am for evening duty from Mondays to Sundays depending on the duty (Morning or Evening) you are on for staff on roster.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm from Monday to Friday, and 8.00am to 2.00pm on Saturdays for permanent morning staff</li>
                                                            <li>Closing time may be determined by the exigency of work each day and would be advised daily by Head of Department. Also, you would be required to work on public holidays when the need arises with extra payment as overtime, or to observe your holiday on a later date or on another public holiday.</li>
                                                            <li>You are entitled to annual leave according to the following schedule:
                                                                <ul>
                                                                    <li>18 days, for staff who have served with the hospital a period more than one year but less than five years.</li>
                                                                    <li>21 days, for staff who have served with the hospital a period more than five years but less than ten years.</li>
                                                                    <li>30 days, for staff who have served with the Hospital more than ten year.</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <p style="margin-top: 0px;margin-left: 25px;font-size:small;">Your previous rights will be conserved and the leave will mostly be taken during the long vacation in accordance with a predetermined schedule and your salary will run normally during the period.</p>
                                                    <p style="font-weight: 600;margin-bottom:0px;margin-left: 25px;">OTHER TERMS AND CONDITIONS:</p>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">A. The duration of your appointment is for two (2) years and the initial three months shall be a probationary period with effect from the date you assume duty;</p>

                                                    <div class="html2pdf__page-break"></div>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">B. At the end of the probation period, the Management may confirm your appointment as a permanent staff, extend your probation for a further trial period, or terminate your appointment.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">C. The following documents should be submitted before starting your duty.</p>
                                                    <ol style="font-size:small;margin-left:20px;margin-bottom:0px;">
                                                        <li style="font-weight: 600;">The photocopy of all your credentials </li>
                                                        <li style="font-weight: 600;">Letter of satisfactory medical report on the state of your health.</li>
                                                        <li style="font-weight: 600;">The photocopy of National ID card</li>
                                                        <li style="font-weight: 600;">If no ID card, reference letter from your local Police Department</li>
                                                        <li style="font-weight: 600;">Personal CV and valid practicing license (Medical Staff only)</li>
                                                        <li style="font-weight: 600;">4 passport photographs</li>
                                                        <li style="font-weight: 600;">Letter of satisfactory reference from your previous employers and from personal referees given by you is a pre-condition of this employment;</li>
                                                    </ol>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">D. From the date you assume duty, other terms and conditions of service of the Hospital will apply to you;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">E. On assumption of duty, you will be expected to strictly comply with the Hospital's Rules and Regulations and should not be involved in any conduct which may be against the hospital's interest or which may bring disrepute to it;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">F. You are expected to assume duty as early as possible in order to finish documentation process on time and start the work.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">G. No staff shall engage in any form of commercial activity within the Hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">H. Any staff found acting against any of the terms and conditions contained herein shall be given a query and a warning letter.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">I. In the situation where a staff receives up to 3 warning letters, his or her appointment may be terminated. In which case, the staff will not be paid any additional amount except for the days he/she worked.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">J. If the management is not satisfied with a staff&rsquo;s performance, his or her appointment will be terminated with or without a notice.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">K. Working in another place while working with Nizamiye hospital will render this contract invalid</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">The notice will be given as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>One day before the termination of the contract where the contract has continued for a period of three months or less,</li>
                                                        <li>One week before the termination of the contract where the contract has continued for more than three months but less than two years,</li>
                                                        <li>Two weeks before the termination of the contract where the contract has continued for a period of two years but less than five years,</li>
                                                        <li>One month before the termination of the contract where the contract has continued for five years and more.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">L. A staff must submit his/her resignation one month before the planned date of stopping work. Failure to do so will make the staff forfeit one month salary to the hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">M. If, at the Hospital&rsquo;s discretion, the employee&rsquo;s appointment is terminated without his/her notice, then he/she will receive the appropriate amount of payment in lieu of notice.</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>The staff who did not complete (3) three years with the Hospital, will not have any right of pay-off</li>
                                                        <li>The staff that completed more than (3) three and less than (5) five years, one month Net salary for each completed year.</li>
                                                        <li>The staff who completed (5) five and more years, one month Net Salary for each completed year.</li>
                                                        <li>However, this payment is not applicable when a staff is found guilty of gross misconduct.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">N. In the event when staff gives notice of resignation, the payment schedule will be as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Below three years: no right for pay-off.</li>
                                                        <li>Three years and above: one month Net salary for each competed year of service.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">O. The following is our pregnancy policy for female staff:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Any female staff that is not above one (1) year of contract is not entitled to maternity leave.</li>
                                                        <li>Maternity leave is four (4) weeks before and eight (8) weeks after delivery, that is three (3) months total.</li>
                                                        <li>Maternity leave automatically cancels annual leave of that particular year.</li>
                                                        <li>Resumption from maternity leave after 3 months accompanies Morning duty from 8am to 3pm.</li>
                                                        <li>After baby reaches 6 months, staffs should resume Morning duty from 8am to 4pm until baby is one (1) year old.</li>
                                                        <li>After baby reaches nine (9) months, staffs are expected to resume regular duty (Morning and Night).</li>
                                                        <li>Any staff who gets pregnant less than 3 years from previous delivery, will not be entitled to maternity leave.</li>
                                                        <li>Your marital and pregnancy status must be known before signing this contract. Providing wrong or deceitful information will render this contract invalid.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">P. Wedding leave is 7 days for both Male and Female staffs.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">Q. Paternity leave is 3 days.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">R. This agreement is an extract from condition of service booklet and it is subject to changes and renewal yearly.</p>
                                                    <!-- <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">S. Please confirm your acceptance of this offer and terms and conditions therein by signing below.</p> -->
                                                    <!-- </div> -->

                                                    <div class="row">
                                                        <div class="col-sm-6 ml-4">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style=" font-weight: 600; margin-bottom:5px">EMPLOYEE/ DATE</p>
                                                        </div>
                                                        <div class="col-sm-4 ml-4 text-right">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style="font-weight: 600;  margin-bottom:5px">MANAGEMENT/ DATE</p>
                                                        </div>
                                                    </div>



                                                    <div class="row d-flex justify-content-between px-5" style="margin-top: 0px;">

                                                        <div class="" style="margin-left: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/hos.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="" style="margin-right: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/branch.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>







                                                    <!-- /.tab-content -->
                                                </div>
                                                <!-- /.nav-tabs-custom -->

                                                <button class="btn btn-sm btn-danger p-3 text-white" id="download" onclick="downloadInvoice55()">Print</button>

                                            <?php endif; ?>

                                        </div>



                                        <div class="tab-pane" id="book2">
                                            <?php if (strtoupper($emp_details['EmpCategory']) === 'MEDICAL'): ?>
                                                <div id="invoice2" class="text-dark ">
                                                    <div class="row" style="width:90%;margin-left:40px;">
                                                        <div class="col-sm-3">
                                                            <!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="60%" height="80%"> -->
                                                            <img src="<?= $ComLogo ?>" alt="" style="margin-top:-20px;width: 200px;height: 120px;" />
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div class="row align-items-center">
                                                                <div class="col-sm-9 text-center" style="font-weight: 600;">
                                                                    <span>NIZAMIYE HOSPITAL LTD</span><br>
                                                                    <span>RENEWAL OF APPOINTMENT FOR</span>
                                                                    <p>MEDICAL STAFF</p>
                                                                </div>
                                                                <div class="col-sm-3 d-flex justify-content-end mt-3">
                                                                    <div style="font-weight: 600;">RC882824</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p style="margin: 0px;">
                                                        <hr style="border: 1px solid black;margin:0px;">
                                                    </p>
                                                    <div style="margin-bottom:5px;">
                                                    </div>
                                                    <div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">


                                                        <div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo strtoupper("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}") ?></p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Designation:</span>
                                                                        <?php
                                                                        $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

                                                                        $sqR = ScriptRunnercous($sq);

                                                                        echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');

                                                                        ?>
                                                                    </p>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Section of Hospital:</span> <?= strtoupper($emp_details['EmpCategory']) ?>
                                                                    <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Renewal Date:</span> <?php echo date("d F Y") ?></p>
                                                                </div>
                                                                <div class="col-md-2 text-right">
                                                                    <img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <ul style="font-size:small;">
                                                            <li>With reference to the earlier agreement signed, the management of Nizamiye Hospital wish to inform you that your appointment has been reviewed and the new terms and conditions are as follows;</li>
                                                            <li>We are aware and would like to remind you that your first appointment with Nizamiye Hospital took effect from <span style="font-weight: 600;"><?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("dS F Y")  : ""   ?></span>.</li>
                                                            <li>Your net monthly pay shall be N<?= number_format($fin_details['NetPay'] / 12, 2) ?>, below is the breakdown of your monthly net salary</li>
                                                        </ul>

                                                        <?php

                                                        $netPay = $fin_details['NetPay'] / 12;
                                                        $basicPay = ($netPay * 25) / 100;
                                                        $transport = ($netPay * 15) / 100;
                                                        $housing = ($netPay * 10) / 100;
                                                        $hazard = ($netPay * 35) / 100;
                                                        $dressing = ($netPay * 10) / 100;
                                                        $utilities = ($netPay * 5) / 100;

                                                        ?>

                                                        <table align="center" cellpadding="1" cellspacing="1" style="width:500px">
                                                            <tbody>
                                                                <tr>
                                                                    <td>BASIC</td>
                                                                    <td><?= number_format($basicPay, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>ALLOWANCES:</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>TRANSPORT</td>
                                                                    <td><?= number_format($transport, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HOUSING</td>
                                                                    <td><?= number_format($housing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>HAZARD</td>
                                                                    <td><?= number_format($hazard, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>DRESSING</td>
                                                                    <td><?= number_format($dressing, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>UTILITIES</td>
                                                                    <td><?= number_format($utilities, 2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NET SALARY</td>
                                                                    <td><?= number_format($netPay, 2) ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <p style="margin-bottom: 0px;margin-left: 25px;margin-top: 10px;">I. <span style="margin-left: 16px; font-size:small;"> Pension shall be calculated and deducted in accordance with the Pension Act. If staff doesn't submit their PIN registration number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 25px;">II. <span style="margin-left: 12px; font-size:small;"> Personal Income Tax (PAYE) shall be calculated and paid to FIRS in accordance with Personal Income tax act.</span></p>
                                                        <p style="margin-bottom: 0x;margin-left: 25px;">III. <span style="margin-left: 8px;font-size:small;"> National Housing Fund (NHF) shall be calculated and paid in accordance with NHF act. If staff doesn't submit their NHF number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                        <p style="margin-bottom: 0px;margin-left: 80px;font-weight: 600;text-decoration:underline;">The detailed composition of the remuneration shall be as stated below:</p>

                                                        <ul style="font-size:small;margin-bottom: 0px;">
                                                            <li>Your salary is as indicated above, but you will be paid overtime duties as may be scheduled by your Head of Department in a month. Further details are contained in the condition of service booklet which is subject to changes upon renewal of your appointment.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm for Morning duty, 5.00pm to 8.00am for evening duty from Mondays to Sundays depending on the duty (Morning or Evening) you are on for staff on roster.</li>
                                                            <li>Your working hours shall be from 8.00am to 5.00pm from Monday to Friday, and 8.00am to 2.00pm on Saturdays for permanent morning staff</li>
                                                            <li>Closing time may be determined by the exigency of work each day and would be advised daily by Head of Department. Also, you would be required to work on public holidays when the need arises with extra payment as overtime, or to observe your holiday on a later date or on another public holiday.</li>
                                                            <li>You are entitled to annual leave according to the following schedule:
                                                                <ul>
                                                                    <li>18 days, for staff who have served with the hospital a period more than one year but less than five years.</li>
                                                                    <li>21 days, for staff who have served with the hospital a period more than five years but less than ten years.</li>
                                                                    <li>30 days, for staff who have served with the Hospital more than ten year.</li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    </div>

                                                    <p style="margin-top: 0px;margin-left: 25px;font-size:small;">Your previous rights will be conserved and the leave will mostly be taken during the long vacation in accordance with a predetermined schedule and your salary will run normally during the period.</p>
                                                    <p style="font-weight: 600;margin-bottom:0px;margin-left: 25px;">OTHER TERMS AND CONDITIONS:</p>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">A. The duration of your appointment is for two (2) years and the initial three months shall be a probationary period with effect from the date you assume duty;</p>

                                                    <div class="html2pdf__page-break"></div>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">B. At the end of the probation period, the Management may confirm your appointment as a permanent staff, extend your probation for a further trial period, or terminate your appointment.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">C. The following documents should be submitted before starting your duty.</p>
                                                    <ol style="font-size:small;margin-left:20px;margin-bottom:0px;">
                                                        <li style="font-weight: 600;">The photocopy of all your credentials </li>
                                                        <li style="font-weight: 600;">Letter of satisfactory medical report on the state of your health.</li>
                                                        <li style="font-weight: 600;">The photocopy of National ID card</li>
                                                        <li style="font-weight: 600;">If no ID card, reference letter from your local Police Department</li>
                                                        <li style="font-weight: 600;">Personal CV and valid practicing license (Medical Staff only)</li>
                                                        <li style="font-weight: 600;">4 passport photographs</li>
                                                        <li style="font-weight: 600;">Letter of satisfactory reference from your previous employers and from personal referees given by you is a pre-condition of this employment;</li>
                                                    </ol>

                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">D. From the date you assume duty, other terms and conditions of service of the Hospital will apply to you;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">E. On assumption of duty, you will be expected to strictly comply with the Hospital's Rules and Regulations and should not be involved in any conduct which may be against the hospital's interest or which may bring disrepute to it;</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">F. You are expected to assume duty as early as possible in order to finish documentation process on time and start the work.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">G. No staff shall engage in any form of commercial activity within the Hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">H. Any staff found acting against any of the terms and conditions contained herein shall be given a query and a warning letter.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">I. In the situation where a staff receives up to 3 warning letters, his or her appointment may be terminated. In which case, the staff will not be paid any additional amount except for the days he/she worked.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">J. If the management is not satisfied with a staff&rsquo;s performance, his or her appointment will be terminated with or without a notice.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">K. Working in another place while working with Nizamiye hospital will render this contract invalid</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">The notice will be given as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>One day before the termination of the contract where the contract has continued for a period of three months or less,</li>
                                                        <li>One week before the termination of the contract where the contract has continued for more than three months but less than two years,</li>
                                                        <li>Two weeks before the termination of the contract where the contract has continued for a period of two years but less than five years,</li>
                                                        <li>One month before the termination of the contract where the contract has continued for five years and more.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">L. A staff must submit his/her resignation one month before the planned date of stopping work. Failure to do so will make the staff forfeit one month salary to the hospital.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">M. If, at the Hospital&rsquo;s discretion, the employee&rsquo;s appointment is terminated without his/her notice, then he/she will receive the appropriate amount of payment in lieu of notice.</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>The staff who did not complete (3) three years with the Hospital, will not have any right of pay-off</li>
                                                        <li>The staff that completed more than (3) three and less than (5) five years, one month Net salary for each completed year.</li>
                                                        <li>The staff who completed (5) five and more years, one month Net Salary for each completed year.</li>
                                                        <li>However, this payment is not applicable when a staff is found guilty of gross misconduct.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">N. In the event when staff gives notice of resignation, the payment schedule will be as follows:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Below three years: no right for pay-off.</li>
                                                        <li>Three years and above: one month Net salary for each competed year of service.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">O. The following is our pregnancy policy for female staff:</p>
                                                    <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                        <li>Any female staff that is not above one (1) year of contract is not entitled to maternity leave.</li>
                                                        <li>Maternity leave is four (4) weeks before and eight (8) weeks after delivery, that is three (3) months total.</li>
                                                        <li>Maternity leave automatically cancels annual leave of that particular year.</li>
                                                        <li>Resumption from maternity leave after 3 months accompanies Morning duty from 8am to 3pm.</li>
                                                        <li>After baby reaches 6 months, staffs should resume Morning duty from 8am to 4pm until baby is one (1) year old.</li>
                                                        <li>After baby reaches nine (9) months, staffs are expected to resume regular duty (Morning and Night).</li>
                                                        <li>Any staff who gets pregnant less than 3 years from previous delivery, will not be entitled to maternity leave.</li>
                                                        <li>Your marital and pregnancy status must be known before signing this contract. Providing wrong or deceitful information will render this contract invalid.</li>
                                                    </ol>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">P. Wedding leave is 7 days for both Male and Female staffs.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">Q. Paternity leave is 3 days.</p>
                                                    <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">R. This agreement is an extract from condition of service booklet and it is subject to changes and renewal yearly.</p>
                                                    <!-- <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">S. Please confirm your acceptance of this offer and terms and conditions therein by signing below.</p> -->
                                                    <!-- </div> -->

                                                    <div class="row">
                                                        <div class="col-sm-6 ml-4">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style=" font-weight: 600; margin-bottom:5px">EMPLOYEE/ DATE</p>
                                                        </div>
                                                        <div class="col-sm-4 ml-4 text-right">
                                                            <p style="margin-bottom:0px">...............................................</p>
                                                            <p style="font-weight: 600;  margin-bottom:5px">MANAGEMENT/ DATE</p>
                                                        </div>
                                                    </div>



                                                    <div class="row d-flex justify-content-between px-5" style="margin-top: 0px;">

                                                        <div class="" style="margin-left: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/hos.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="" style="margin-right: 15px">
                                                            <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
                                                            <div class="parent  d-flex flex-gap-3">
                                                                <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                <div class="context">
                                                                    <img width="230px" src="../pfimg/branch.png" alt="">
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>






                                                    <!-- /.tab-content -->
                                                </div>
                                                <!-- /.nav-tabs-custom -->
                                                <button class="btn btn-sm btn-danger p-3 text-white" id="download2" onclick="downloadInvoice2()">Print</button>
                                            <?php endif; ?>
                                        </div>
                                        <!-- /.col -->

                                        <!-- <div class="tab-content"> -->



                                        <div class="tab-pane" id="book6">
                                            <?php if (strtoupper($emp_details['EmpCategory']) === 'EXPATRIATE'): ?>
                                                <div id="invoice6" class="text-dark ">
                                                    <div id="invoice6" class="text-dark ">
                                                        <div class="row" style="width:90%;margin-left:40px;">
                                                            <div class="col-sm-3">
                                                                <!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="60%" height="80%"> -->
                                                                <img src="<?= $ComLogo ?>" alt="" style="margin-top:-20px;width: 200px;height: 120px;" />
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <div class="row align-items-center">
                                                                    <div class="col-sm-9 text-center" style="font-weight: 600;">
                                                                        <span>NIZAMIYE HOSPITAL LTD</span><br>
                                                                        <span>RENEWAL OF APPOINTMENT FOR</span>
                                                                        <p>EXPATRIATE STAFF</p>
                                                                    </div>
                                                                    <div class="col-sm-3 d-flex justify-content-end mt-3">
                                                                        <div style="font-weight: 600;">RC882824</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="margin-bottom:5px;">
                                                        </div>
                                                        <div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">
                                                            <div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
                                                                <div class="row">
                                                                    <div class="col-md-10">
                                                                        <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo strtoupper("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}") ?></p>
                                                                        <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Designation:</span>
                                                                            <?php
                                                                            $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

                                                                            $sqR = ScriptRunnercous($sq);

                                                                            echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');

                                                                            ?>
                                                                        </p>
                                                                        <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Section of Hospital:</span> <?= strtoupper($emp_details['EmpCategory']) ?>
                                                                        <p style="margin-bottom: 8px;"><span style="font-weight: 600;">Renewal Date:</span> <?php echo date("d F Y") ?></p>
                                                                    </div>
                                                                    <div class="col-md-2 text-right">

                                                                        <!-- <p>Branch of school: </p> -->
                                                                        <img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <ul style="font-size:small;">
                                                                <li>With reference to the earlier agreement signed, the management of Nizamiye Hospital wish to inform you that your appointment has been reviewed and the new terms and conditions are as follows;</li>
                                                                <li>We are aware and would like to remind you that your first appointment with Nizamiye Hospital took effect from <span style="font-weight: 600;"><?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("dS F Y")  : ""   ?></span>.</li>
                                                                <li>Your gross monthly payment shall be stated below and the sum includes consolidated allowances as part of your basic salary.</li>
                                                            </ul>

                                                            <table style="width:90%;margin-left: 25px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>
                                                                            <p>Gross Salary</p>
                                                                        </th>
                                                                        <th>
                                                                            <p>I. Pension Contribution</p>
                                                                        </th>
                                                                        <th>
                                                                            <p>II. PAYE(TAX)</p>
                                                                        </th>
                                                                        <th>
                                                                            <p>III. NET Salary</p>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <p><?= number_format($fin_details['Gross'] / 12, 2) ?></p>
                                                                        </td>
                                                                        <td>
                                                                            <p>&nbsp;</p>
                                                                        </td>
                                                                        <td>
                                                                            <p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
                                                                        </td>
                                                                        <td>
                                                                            <p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <p style="margin-bottom: 0px;margin-left: 25px;margin-top: 10px;">I. <span style="margin-left: 16px; font-size:small;"> Pension shall be calculated and deducted in accordance with the Pension Act. If staff doesn't submit their PIN registration number within 3 months, the appointment shall be automatically terminated.</span></p>
                                                            <p style="margin-bottom: 0px;margin-left: 25px;">II. <span style="margin-left: 12px; font-size:small;"> Personal Income Tax (PAYE) shall be calculated and paid to FIRS in accordance with Personal Income tax act.</span></p>
                                                            <p style="margin-bottom: 0px;margin-left: 80px;font-weight: 600;text-decoration:underline;">The detailed composition of the remuneration shall be as stated below:</p>

                                                            <ul style="font-size:small;margin-bottom: 0px;">
                                                                <li>Your salary is as indicated above, but you will be paid overtime duties as may be scheduled by your Head of Department in a month. Further details are contained in the condition of service booklet which is subject to changes upon renewal of your appointment. on salary amount or other benefits. </li>
                                                                <li>Your working hours shall be from 8.00am to 5.00pm for Morning duty, 5.00pm to 8.00am for evening duty from Mondays to Sundays depending on the duty (Morning or Evening) you are on.</li>
                                                                <li>Closing time may be determined by the exigency of work each day and would be advised daily by Head of Department. Also, you would be required to work on public holidays when the need arises with extra payment as overtime, or to observe your holiday on a later date or on another public holiday.</li>
                                                                <li>You are entitled to annual leave according to the following schedule:
                                                                    <ul>
                                                                        <li>18 days, for staff who have served with the hospital a period more than one year but less than five years.</li>
                                                                        <li>21 days, for staff who have served with the hospital a period more than five years but less than ten years.</li>
                                                                        <li>25 days, for staff who have served with the Hospital more than ten year.</li>
                                                                    </ul>
                                                                </li>
                                                            </ul>

                                                            <p style="margin-top: 0px;margin-left: 25px;font-size:small;">Your previous rights will be conserved and the leave will mostly be taken during the long vacation in accordance with a predetermined schedule and your salary will run normally during the period.</p>
                                                            <ul>
                                                                <li>Airline ticket fee of the person will be paid once in a year for the staff and staffs family.</li>
                                                                <li>80% of the annual school fee of each child is covered.</li>
                                                                <li>Management provides free accommodation in hospital premises.</li>
                                                            </ul>
                                                            <p style="font-weight: 600;margin-bottom:0px;margin-left: 25px;">OTHER TERMS AND CONDITIONS:</p>

                                                            <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">A. The duration of your appointment is for <span style="font: weight 600px;">two (2) years.</span></p>



                                                            <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">B. At the end of the probation period, the Management may confirm your appointment as a permanent staff, extend your probation for a further trial period, or terminate your appointment.</p>

                                                        </div>
                                                        <div class="html2pdf__page-break"></div>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">C. The following documents should be submitted before starting your duty.</p>
                                                        <ol style="font-size:small;margin-left:20px;margin-bottom:0px;">
                                                            <li style="font-weight: 600;">The photocopy of all your credentials </li>
                                                            <li style="font-weight: 600;">Letter of satisfactory medical report on the state of your health.</li>
                                                            <li style="font-weight: 600;">The photocopy of National ID card</li>
                                                            <li style="font-weight: 600;">If no ID card, reference letter from your local Police Department</li>
                                                            <li style="font-weight: 600;">Personal CV and valid practicing license (Medical Staff only)</li>
                                                            <li style="font-weight: 600;">4 passport photographs</li>
                                                            <li style="font-weight: 600;">Letter of satisfactory reference from your previous employers and from personal referees given by you is a pre-condition of this employment;</li>
                                                        </ol>

                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">D. From the date you assume duty, other terms and conditions of service of the Hospital will apply to you;</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">E. On assumption of duty, you will be expected to strictly comply with the Hospital's Rules and Regulations and should not be involved in any conduct which may be against the hospital's interest or which may bring disrepute to it;</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">F. You are expected to assume duty as early as possible in order to finish documentation process on time and start the work.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">G. No staff shall engage in any form of commercial activity within the Hospital.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">H. Any staff found acting against any of the terms and conditions contained herein shall be given a query and a warning letter.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">I. In the situation where a staff receives up to 3 warning letters, his or her appointment may be terminated. In which case, the staff will not be paid any additional amount except for the days he/she worked.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">J. If the management is not satisfied with a staff&rsquo;s performance, his or her appointment will be terminated with or without a notice.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">The notice will be given as follows:</p>
                                                        <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                            <li>One day before the termination of the contract where the contract has continued for a period of three months or less,</li>
                                                            <li>One week before the termination of the contract where the contract has continued for more than three months but less than two years,</li>
                                                            <li>Two weeks before the termination of the contract where the contract has continued for a period of two years but less than five years,</li>
                                                            <li>One month before the termination of the contract where the contract has continued for five years and more.</li>
                                                        </ol>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">K. If, at the Hospital&rsquo;s discretion, the employee&rsquo;s appointment is terminated without his/her notice, then he/she will receive the appropriate amount of payment in lieu of notice.</p>
                                                        <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                            <li>The staff who did not complete (3) three years with the Hospital, will not have any right of pay-off</li>
                                                            <li>The staff that completed more than (3) three and less than (5) five years, one month Net salary for each completed year.</li>
                                                            <li>The staff who completed (5) five and more years, one month Net Salary for each completed year.</li>
                                                            <li>However, this payment is not applicable when a staff is found guilty of gross misconduct, or when a staff resigned his/her appointment.</li>
                                                        </ol>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">L. In the event when staff gives notice of resignation, the payment schedule will be as follows:</p>
                                                        <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                            <li>Below three years: no right for pay-off.</li>
                                                            <li>Three years and above: one month Net salary for each competed year of service.</li>
                                                        </ol>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">M. The following is our pregnancy policy for female staff:</p>
                                                        <ol style="font-size:12px;margin-left:20px;margin-bottom:0px;">
                                                            <li>Any female staff that is not above one (1) year of contract is not entitled to maternity leave.</li>
                                                            <li>Maternity leave is four (4) weeks before and eight (8) weeks after delivery, that is three (3) months total.</li>
                                                            <li>Maternity leave automatically cancels annual leave of that particular year.</li>
                                                            <li>Resumption from maternity leave after 3 months accompanies Morning duty from 8am to 3pm.</li>
                                                            <li>After baby reaches 6 months, staffs should resume Morning duty from 8am to 4pm until baby is one (1) year old.</li>
                                                            <li>After baby reaches one (1) year, , staffs are expected to resume regular duty (Morning and Night).</li>
                                                            <li>Any staff who gets pregnant less than 3 years from previous delivery, will not be entitled to maternity leave.</li>
                                                        </ol>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">N. Wedding leave is 7 days for both Male and Female staffs.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">O. Paternity leave is 3 days.</p>
                                                        <p style="margin-bottom:0px;margin-left: 25px;font-size:small;">P. This agreement is an extract from condition of service booklet and it is subject to changes and renewal yearly.</p>
                                                        <div class="row">
                                                            <div class="col-sm-6 ml-4">
                                                                <p style="margin-bottom:0px">...............................................</p>
                                                                <p style=" font-weight: 600; margin-bottom:5px">EMPLOYEE/ DATE</p>
                                                            </div>
                                                            <div class="col-sm-4 ml-4 text-right">
                                                                <p style="margin-bottom:0px">...............................................</p>
                                                                <p style="font-weight: 600;  margin-bottom:5px">MANAGEMENT/ DATE</p>
                                                            </div>
                                                        </div>



                                                        <div class="row d-flex justify-content-between px-5" style="margin-top: 0px;">

                                                            <div class="" style="margin-left: 15px">
                                                                <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
                                                                <div class="parent  d-flex flex-gap-3">
                                                                    <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                    <div class="context">
                                                                        <img width="230px" src="../pfimg/hos.png" alt="">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="" style="margin-right: 15px">
                                                                <h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
                                                                <div class="parent  d-flex flex-gap-3">
                                                                    <div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
                                                                    <div class="context">
                                                                        <img width="230px" src="../pfimg/branch.png" alt="">
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>






                                                        <!-- /.tab-content -->
                                                    </div>
                                                    <!-- /.nav-tabs-custom -->




                                                </div>
                                                <button class="btn btn-sm btn-danger p-3 text-white" id="download6" onclick="downloadInvoice6()">Print</button>
                                                <?php //endif;
                                                ?>







                                        </div>




                                        <div class="tab-pane  " id="book4">

                                            <div id="invoice4" class="text-dark ">

                                                <?php if ($other): ?>

                                                    <div class="row">
                                                        <div class="col-sm-10  mx-auto">
                                                            <div class="row text-center">
                                                                <div class="col-sm-2">
                                                                    <!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="row justify-content-center mt-1 "> -->
                                                    <div style="margin-bottom:5px; margin-top:110px">
                                                        <!-- <img src="../pfimg/non_academic.jpg" alt="" width="100%" height="100" > -->
                                                        <h2 class="text-center">EMPLOYMENT CONTRACT</h2>
                                                    </div>

                                                    <p style="margin-bottom: 1px; font-weight: 600; margin-left: 75%; ">DATE: <?php echo date("d F Y") ?></p>


                                                    <div class="rectangle" style="width:90%;margin-left:40px;margin-bottom: 20px;padding: 3px;">
                                                        <div class="row">
                                                            <div class="col-md-10">

                                                                <p style="margin-bottom: 0px;"><span style="font-weight: 600;">Branch of school:</span>
                                                                    <?php

                                                                    $sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

                                                                    $sqgR = ScriptRunnercous($sqg);

                                                                    echo ucwords(strtolower($sqgR['OName']));

                                                                    ?>

                                                                </p>
                                                                <p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff Name:</span> <?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?></p>
                                                                <p style="margin-bottom: 0px;"><span style="font-weight: 600;">Staff ID:</span> <?= $emp_details['EmpID'] ?>
                                                                <p style="margin-bottom: 0px;"><span style="font-weight: 600;">Employment Date:</span> <?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("d F Y")  : ""   ?>

                                                                <p style="margin-bottom: 0px;"><span style="font-weight: 600;">Designation:</span>
                                                                    <?php

                                                                    $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

                                                                    $sqR = ScriptRunnercous($sq);

                                                                    echo (isset($sqR) ? ucwords(strtolower($sqR['ItemPos'])) : '');

                                                                    ?>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-2 text-right">

                                                                <!-- <p>Branch of school: </p> -->
                                                                <img src="../<?= $emp_details['StfImg'] ?>" class="card-img" alt="..." width="100" height=100>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <p style="margin-bottom: 30px;margin-left: 25px;">The Management wishes to inform you that your appointment has been reviewed and is valid from
                                                        September 01, 2021 to July 31, 2022 (being one academic session). The new terms and conditions
                                                        are as follows: </p>

                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">1. RENUMERATION</p>
                                                    <p style="margin-bottom: 15px;margin-left: 25px;">Your gross monthly payment shall be stated below and the sum includes consolidated allowances as
                                                        part of your basic salary. The below table represents all the lawful deductions by the existing laws.</p>

                                                    <div class="row">
                                                        <!-- <div class="col-sm-2"> -->
                                                        <table style="width:90%;margin-left:40px; margin-bottom: 15px;">
                                                            <tr>

                                                                <th>
                                                                    <p style="padding:5px;margin-bottom: 0px; font-weight: 600">Gross Salary </p>
                                                                </th>
                                                                <th>
                                                                    <p style="padding:5px;margin-bottom: 0px; font-weight: 600"> I. NHF Contribution </p>
                                                                </th>
                                                                <th>
                                                                    <p style="padding:5px;margin-bottom: 0px; font-weight: 600"> II. Pension Contribution </p>
                                                                </th>
                                                                <th>
                                                                    <p style="padding:5px;margin-bottom: 0px; font-weight: 600"> III. PAYE(TAX) </p>
                                                                </th>
                                                                <th>
                                                                    <p style="padding:5px;margin-bottom: 0px; font-weight: 600"> NET Salary </p>
                                                                </th>
                                                                <!-- </div> -->
                                                            </tr>
                                                            <!-- <div class="col-sm-2"> -->
                                                            <tr>
                                                                <td style="padding:5px;">
                                                                    <p><?php echo number_format($fin_details['Gross'] / 12, 2) ?></p>
                                                                </td>
                                                                <td style="padding:5px;">
                                                                    <p><?php echo number_format($fin_details["PayItem{$nm}"] / 12, 2) ?>
                                                                </td>
                                                                <td style="padding:5px;">
                                                                    <p> <?= number_format($fin_details['PensionEmployee'] / 12, 2) ?></p>
                                                                </td>
                                                                <td style="padding:5px;">
                                                                    <p><?= number_format($fin_details['PAYE'] / 12, 2) ?></p>
                                                                </td>
                                                                <td style="padding:5px;">
                                                                    <p><?= number_format($fin_details['NetPay'] / 12, 2) ?></p>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </div>


                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;font-weight: 600;">2. EMPLOYMENT DETAILS</p>
                                                    <p style="margin-bottom: 15px;margin-left: 30px;">I. <span style="margin-left: 10px;"> WORK SCHEDULE</span></p>
                                                    <ul style="margin-left: 10px;margin-bottom: 0px;">
                                                        <li>This is a full-time position. Your regular weekly schedule will be <?php echo $_POST['workSchedule']; ?> with one (1) hour lunch break. The closing time may be determined by the exigency of work each day and would be advised daily by the School Principal.</li>
                                                        <li>Your schedule of duties remains the same and/ or may be subject to review solely at Management’s directives. If need be, you will be asked to work outside the normal hours and be paid for accordingly.</li>
                                                        <li>During the break, lunch is served at the dining hall in the school premises and the monthly rates are NGN 2500 for Academic staff and NGN 2000 for Non-Academic staff respectively. These rates are subject to change and the changes will be communicated accordingly.
                                                            <p style="margin-bottom:0px;">NOTE: Taking lunch from the school dining hall is not mandatory hence, the fee is only applicable to those who wish to do so. These rates are subject to change and the changes will be communicated accordingly.</p>
                                                        </li>
                                                        <li>The management will activate its policy of Leave without pay or terminate the valid contract during the period of a pandemic or any situation that disrupts the effective running of the school and/or academic activities or in any situation where an employee is not able to perform his duties on account of ill health or any other reasons.</li>
                                                        <li>The amount payable for the hours and days of work done within the period above shall be decided and communicated to the employee by Management in such circumstance(s).</li>
                                                        <li>The organization will make available information on Public Holiday(s) as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday(s), you will be informed accordingly by your School Principal/ Reporting Manager. </li>
                                                    </ul>
                                                    <div class="html2pdf__page-break"></div>

                                                    <p style="margin-bottom: 0px;margin-left: 30px;">II. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</span></p>
                                                    <p style="margin-bottom: 0px;margin-left: 60px;"> Confirmed Employee is entitled to any form of Leave. Annual Leave, Maternity Leave, Casual Leave, Compassionate Leave is granted but after twelve (12) months in the organization’s employ
                                                    </p>
                                                    <p style="margin-bottom: 0px;margin-left: 30px;">i. <span style="margin-left: 10px;font-weight: 600;">Maternity Leave</span> </p>

                                                    <ul style="margin-left: 40px;margin-bottom: 0px;">
                                                        <li>the Employee will proceed for six (6) weeks before and six (6) weeks after confinement provided the pregnancy has been certified by a registered medical practitioner. </li>
                                                        <li>She will be entitled to her full basic monthly salary including allowances and any leave that has not been observed before confinement shall be forfeited.</li>
                                                        <li>If she gives birth while on Annual Leave within that year, she will have her Annual Leave entitlement forfeited but enjoy her full Maternity Leave.</li>
                                                        <li>If she agrees to work for some weeks during Maternity Leave, she will be given a bonus of 15% to 20% salary increase. </li>
                                                        <li>Upon resumption from Maternity Leave, the nursing mother shall be entitled to one (1) hour break in addition to her normal break period during working hours for nursing purposes for a period not exceeding three (3) months.</li>
                                                    </ul>


                                                    <p style="margin-bottom: 2px;margin-left: 30px;">ii. <span style="margin-left: 10px;font-weight: 600;">Compassionate Leave</span> is up to five (5) days or more depending on School Management’s discretion. For confirmed Employees, this Leave is with pay and may be granted in the case of death of spouse, death of biological parents (mother/father), death of child, death of biological sibling, as well as other similar situations approved by School Principal. </p>
                                                    <!-- <div class="html2pdf__page-break"></div> -->

                                                    <p style="margin-bottom: 0px;margin-left: 30px;">iii. <span style="margin-left: 10px;font-weight: 600;">Sick Leave</span> </p>
                                                    <ul style="margin-left: 40px;margin-bottom: 0px;">
                                                        <li>With regards to Sick Leave, according to the Workmen's Compensation Act, an employee shall be entitled to paid wages up to twelve (12) working days in any one calendar year during absence from work caused by temporary illness certified by a medical practitioner or government-certified hospital. The employer reserves the right to require employees to provide a note from the doctor verifying that an absence was caused by a medical situation.</li>
                                                        <li>To encourage good attendance standards, the employer reserves the right to require documentation from the doctor authorizing the employee’s leave extension and when fit to return to work. </li>
                                                    </ul>

                                                    <p style="margin-bottom: 0px;margin-left: 30px;">vi. <span style="margin-left: 10px;font-weight: 600;"> Casual Leave </span> is seven (7) days paid Leave in a year. Where Casual Leave has been exhausted within the year, any other request for Casual Leave shall be without pay.</p>
                                                    <p style="margin-bottom: 0px;margin-left: 30px;">v. <span style="margin-left: 10px;font-weight: 600;">Annual Leave</span> </p>

                                                    <ul style="margin-left: 40px;">
                                                        <li>Eighteen (18) working days, for staff who has been with the organization for more than one (1) and less than five (5) years.</li>
                                                        <li>Twenty- two (22) working days, for staff who has been with the organization for more than five (5) and less than ten (10) years.</li>
                                                        <li>Twenty- six (26) working days, for staff who has been with the organization for more than ten (10) years.</li>
                                                        <li>Annual Leave will mostly be taken during the long vacation in accordance with a predetermined schedule and the salary will run normally during the period.</li>
                                                    </ul>


                                                    <div class="html2pdf__page-break"></div>

                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">3. CONFLICT OF INTEREST POLICY</p>
                                                    <p style="margin-left: 25px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest.</p>


                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">4. DECLARATION OF INFORMATION</p>
                                                    <p style="margin-left: 25px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operations in any manner as soon as it comes to your knowledge.</p>

                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">5. NON-DISCLOSURE AGREEMENT</p>
                                                    <p style="margin-left: 25px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary, and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information that will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">6. TERMINATION AND RESIGNATION CONDITIONS </p>
                                                    <p style="margin-bottom: 0px;margin-left: 30px;">TERMINATION</p>
                                                    <p style="margin-bottom: 0px;margin-left: 40px;"> Upon confirmation, the Employee appointment may be terminated if found guilty of serious or gross misconduct or inefficiency and lack of commitment. Furthermore, at the expiration of your contract, the management are at liberty to end the contract for any other reason or for no reason. </p>

                                                    <p style="margin-left: 40px;margin-bottom: 0px;">The notice will be given as follows:</p>
                                                    <ul style="margin-bottom: 0px;margin-left: 30px">
                                                        <li>One (1) day where the contract/appointment has continued for a period of three (3) months or less.</li>
                                                        <li>One (1) week where the contract/appointment has continued for more than three (3) months but less than two (2) years.</li>
                                                        <li>Two (2) weeks where the contract/appointment has continued for a period of two (2) years but less than five (5) years.</li>
                                                        <li>One (1) month where the contract has continued for five (5) years or more.</li>
                                                        <li>Any notice for one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
                                                    </ul>


                                                    <p style="margin-bottom: 0px;margin-left: 45px;">RESIGNATION </p>
                                                    <ul style="margin-bottom: 0px;margin-left: 30px">
                                                        <li>An employee who has completed less than three (3) years must at least give two (2) weeks' notice with no right of pay-off.
                                                        <li>An Employee who has completed three (3) years and less than five (5) years must at least give three (3) weeks working days notice with no right of pay-off.</li>
                                                        <li> For an Employee who has completed five (5) years, you may resign by giving the organisation one (1) month notice or one (1) month salary in place of notice. </li>
                                                    </ul>
                                                    <p style="margin-bottom: 10px;margin-left: 55px;">Please note that any period of one (1) week or more shall be in writing excluding the day on which the notice is given.</li>
                                                    <p style="margin-bottom: 0px;margin-left: 25px;">It should be noted that the payment of Pay-Off where applicable shall be processed and completed within two (2) months from the date of issuance of termination letter or end of the resignation notice period.</p>

                                                    <p style="font-weight: 600;margin-left: 25px;margin-bottom: 0px;">7. OTHER TERMS AND CONDITIONS </p>
                                                    <ol type="a" style="margin-left: 20px;margin-bottom: 10px;">
                                                        <li>On assumption of duty, you will be expected to strictly comply with the school’s Rules and Regulations and should not be involved in any conduct which may be against the school’s interest or which may bring disrepute to it </li>

                                                        <li>You are to assume duty as early as possible and for record purposes; you are expected to indicate the exact date on the second copy of this offer letter.</li>
                                                        <div class="html2pdf__page-break"></div>
                                                        <li style="padding-top:10px;">You may be sent on official duties within or outside the state, for instance, entrance examinations, competitions, and workshops. (Expenses will be covered by School) </li>


                                                        <li>You are expected to obey all the rules and regulations (examination, discipline and class teacher, etc.) to run the school effectively.</li>
                                                        <li>No staff shall engage in any form of commercial activity within the school.</li>
                                                    </ol>

                                                    <p style="margin-bottom: 10px;margin-left: 25px;">This agreement is an extract from NTIC Condition of Service and it is subject to changes and periodic review as the need arises. Kindly refer to the Condition of Service and the Disciplinary Code for more details on rules and regulations.</p>

                                                    <p style="margin-bottom:15px;margin-left: 25px;">By signing the duplicate copy of the renewal of appointment confirms that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ. We look forward to your valuable contributions and wish you the very best for a rewarding career with our organization.</p>
                                                    <p style="margin-bottom: 0px;margin-left: 25px;">Congratulations! </p>

                                                    <!-- </div> -->

                                                    <div class="row mt-4">
                                                        <div class="col-sm-6 ml-4">
                                                            <p>...............................................</p>
                                                            <p style="font-weight: 600">EMPLOYER & DATE</p>
                                                        </div>
                                                        <div class="col-sm-4 ml-4 text-right">
                                                            <p>...............................................</p>
                                                            <p style="font-weight: 600">EMPLOYEE & DATE</p>
                                                        </div>
                                                    </div>









                                                    <!-- /.tab-content -->
                                            </div>
                                            <!-- /.nav-tabs-custom -->
                                            <!-- </div> -->
                                            <button class="btn btn-sm btn-danger" id="download4" onclick="downloadInvoice4()">Download</button>

                                        <?php endif; ?>

                                        </div>
                                        <!-- /.col -->



                                    <?php else: ?>



                                        <form action="" method="post" id="scheduleProcess" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label class="col-sm-4 col-form-label">Work Schedule <span style="color: red">*</span>:</label>
                                                <div class="col-sm-6 ">
                                                    <textarea type="text" rows="3" name="workSchedule" maxlength="200" class="form-control" id="workSchedule"></textarea>
                                                </div>
                                            </div>

                                            <input type="hidden" name="other" value="<?= $_POST['bookselect1'] ?>">
                                            <div class="col-md-12 text-left">
                                                <input id="scheduleForm" name="scheduleForm" type="submit" class="btn btn-danger btn-sm" value="Submit" />
                                            </div>
                                        </form>


                                    <?php endif; ?>



                                    </div>

                                </div>

                            </div>

                        <?php endif; ?>




                        </div>
                    </div>
                    <!-- /.box-body -->




                </div>

                <!-- /.row -->
</section>
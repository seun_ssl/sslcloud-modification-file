<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';

date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("EMPLOYMENT TERMINATION LETTER" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$emp_details = null;
$other = false;

if (isset($_POST['staff_termination'])) {
	if (empty($_POST['designation']) || empty($_POST['terminationDate']) || empty($_POST['reasonForTermination'])) {

		echo ("<script type='text/javascript'>{parent.msgbox('All fields are required', 'red'); }</script>");
	} else {

		$other = true;

		$emp = $_POST['other'];

		// var_dump($emp);
		// die();
		$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

		$emp_details = ScriptRunnercous($Script_Menu);

		// var_dump($emp_details);
		$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
		$EditID = ScriptRunner($Script_Edit, "SetValue17");
		$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
		$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
		// var_dump($emp_detailsHr['sig_img']);

		$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
		$comp_details = ScriptRunnercous($Script_Edit_com);
		$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
		$email_details = ScriptRunnercous($Script_Edit_email);
		$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
		$addr_details = ScriptRunnercous($Script_Edit_addr);
		$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
		$rc_details = ScriptRunnercous($Script_Edit_RC);
		$branchId = $emp_details['BranchID'];
		$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
		$bran_details = ScriptRunnercous($Script_Edit_Br);
		$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
		$fin_details = ScriptRunnercous($Script_Edit_Fin);
		$fin_hash = $fin_details['Scheme'];
		$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
		$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

		// var_dump($fin_set_details);
		$nm = null;
		for ($i = 1; $i <= 25; $i++) {
			if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
				$nm = $i;
				break;
			}
		}
	}
}

$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
	// var_dump($emp_detailsHr['sig_img']);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
	$rc_details = ScriptRunnercous($Script_Edit_RC);
	$branchId = $emp_details['BranchID'];
	$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
	$bran_details = ScriptRunnercous($Script_Edit_Br);
	$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
	$fin_details = ScriptRunnercous($Script_Edit_Fin);
	$fin_hash = $fin_details['Scheme'];
	$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
	$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

	// var_dump($fin_set_details);
	$nm = null;
	for ($i = 1; $i <= 25; $i++) {
		if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
			$nm = $i;
			break;
		}
	}

	// var_dump($bran_details);

}

?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		* {
			font-family: "Times New Roman", Times, serif !important;

			color: black !important;
		}

		table,
		th,
		td {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px !important;
			/* font-family: "Times New Roman", Times, serif; */
		}

		body {
			font-size: 16px !important;
		}

		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: 5px;
			line-height: 20px;
		}

		.box-body ul li {
			line-height: 18px !important;
		}

		.card-img {
			width: 76%;
		}

		.nav-tabs-custom>.tab-content {
			color: black !important;
			text-align: justify;
		}

		.rectangle {
			border: 1px solid black;
			/* padding: 10px; */
		}

		img {
			/* width: 100%;
  height: 160px;
  margin-left: 550px; */
		}
	</style>

</head>


<body>



	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Generate Employment Termiation Letter
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control " name="bookselect1" id="bookselect1">
										<!-- <select name="level1" id="level1"  class="form-control" > -->
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>



									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">



				<?php if (!is_null($emp_details) && !empty($emp_details) || isset($_POST['staff_termination'])): ?>

					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<?php //if (strtoupper($emp_details['EmpCategory']) === 'SUPER ADMIN'): 
									?>
									<?php //if (json_encode($_POST['scheduleForm'])): 
									?>
									<div id="invoice4" class="text-dark ">

										<?php if ($other): ?>

											<p style="margin-bottom: 30px;margin-left: 25px;margin-top: 100px">Date: <?php echo date("d F Y") ?></p>
											<p style="margin-left: 25px;"><span style="font-weight: 600;">Name: </span><?php echo ucwords(strtolower("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}")) ?> </p>
											<p style="margin-bottom: 30px;margin-left: 25px;"><span style="font-weight: 600;">Designation: </span><?php echo ucwords($_POST['designation']) ?> </p>
											<h6 style="font-weight: 600;margin-left: 25px;">TERMINATION OF EMPLOYMENT</h6>


											<p style="margin-bottom: 10px;margin-left: 25px;">This is to confirm the receipt and acceptance of your resignation letter dated <?php echo date('d M, Y', strtotime($_POST['letterReceivedDate'])); ?>, as a/an
												<?php echo ucwords($_POST['designation']) ?> with the school. You will be relieved from your duties on <?php echo date('d M, Y', strtotime($_POST['dutiesReliefDate'])); ?>.</p>

											<p style="margin-bottom: 10px;margin-left: 25px;">This is to inform you that your employment with the Nigerian Tulip International Colleges (NTIC) Abuja has come to an end effective <?php echo date('d M, Y', strtotime($_POST['terminationDate'])); ?>.
												This decision is not reversible.</p>
											<p style="margin-bottom: 10px;margin-left: 25px;">This termination is due to <?php echo ($_POST['reasonForTermination']) ?>.

											<p style="margin-bottom: 0px;margin-left: 25px;">By this, you are requested to return any NTIC property in your possession to the Human Resources Department.</p>
											<p style="margin-bottom: 10px;margin-left: 25px;">You will receive a Certificate of Service, and all your benefits including your payoff (where applicable).</p>

											<p style="margin-bottom: 10px;margin-left: 25px;">Also, keep in mind that you have signed a non-disclosure agreement and other relevant policies to maintain the highest degree of confidentiality
												relating to the business of the organization which was entrusted to you, or you were privy to by your role.</p>

											<p style="margin-bottom: 10px;margin-left: 25px;">Be guided accordingly even as we wish you the best in your future endeavors.</p>

											<p style="margin-bottom: 20px;margin-left: 25px;">For NTIC:</p>

											<p style="margin-bottom: 30px;margin-left: 25px;font-weight: 600;">Head of Human Resources.</p>






											<!-- /.tab-content -->
											<!-- </div> -->
											<!-- /.nav-tabs-custom -->
									</div>
									<button class="btn btn-sm btn-danger" id="download4" onclick="downloadInvoice4()">Download</button>

									<?php //endif;
									?>

								</div>
								<!-- /.col -->



							<?php else: ?>



								<form action="" method="post" id="scheduleProcess" enctype="multipart/form-data">
									<div class="row">

										<div class="col-md-6">
											<div class="form-group row ">
												<label class="col-sm-4 col-form-label"> Date of Termination <span style="color: red">*</span>:</label>
												<div class="col-sm-8 input-group-sm">
													<input name="terminationDate" id="terminationDate" type="date" class="form-control" maxlength="12" />
												</div>
											</div>
											<div class="form-group row ">
												<label class="col-sm-4 col-form-label"> Reason for Termination <span style="color: red">*</span>:</label>
												<div class="col-sm-8 ">
													<textarea type="text" rows="3" name="reasonForTermination" maxlength="200" class="form-control" id="reasonForTermination"></textarea>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Designation <span style="color: red">*</span>:</label>
												<div class="col-sm-8 input-group input-group-sm">
													<input type="text" name="designation" class="form-control" id="designation" />
												</div>
											</div>

										</div>

									</div>
							</div>
							<!-- <div class="col-md-6">
						
						<div class="form-group row ">
							<label  class="col-sm-4 col-form-label">Staff Address <span style="color: red">*</span>:</label>
							<div class="col-sm-8 " >
								<textarea type="text" rows="3" name="staffAddress" maxlength="200" class="form-control" id="staffAddress"></textarea>
							</div>
						</div>
					</div> -->
						</div>
						<!-- <div class="form-group">
			<label  class="col-sm-4 col-form-label">Work Schedule <span style="color: red">*</span>:</label>
			<div class="col-sm-6 ">
				<textarea type="text" rows="3" name="workSchedule" maxlength="200" class="form-control" id="workSchedule"></textarea>
			</div>
		</div> -->

						<input type="hidden" name="other" value="<?= $_POST['bookselect1'] ?>">
						<div class="col-md-12 text-center">
							<input id="staff_termination" name="staff_termination" type="submit" style="width:5%;" class="btn btn-danger btn-sm" value="Submit" />
						</div>
						</form>


					<?php endif; ?>



					</div>

			</div>

		</div>

	<?php endif; ?>




	</div>
	</div>
	<!-- /.box-body -->




	</div>

	<!-- /.row -->
	</section>
	<!-- /.content -->


	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="html2pdf/dist/html2pdf.bundle.js"></script>


	<script>
		function downloadInvoice1() {
			const invoice = document.getElementById("invoice");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_NON_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.90, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice).set(opt).save();

			html2pdf().from(invoice).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice2() {
			const invoice2 = document.getElementById("invoice2");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice2).set(opt).save();

			html2pdf().from(invoice2).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice3() {

			const invoice3 = document.getElementById("invoice3");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ADMIN_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice3).set(opt).save();

			html2pdf().from(invoice3).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice4() {

			const invoice4 = document.getElementById("invoice4");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_STAFF_TERMINATION ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice4).set(opt).save();

			html2pdf().from(invoice4).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice5() {

			const invoice5 = document.getElementById("invoice5");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_SUPER_ADMIN_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice5).set(opt).save();

			html2pdf().from(invoice5).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}
	</script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->

	<script type="text/javascript">



	</script>


	<script>
		$('#example1').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example6').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example7').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		setInterval(function() {
			$(".tablesorter-filter-row").remove();
		}, 2000);

		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});



		}



		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select an employee', 'red');
				};

				return;

			} else {

				$('#theform1').submit();





			}



		});


		if (<?= json_encode($emp_details) ?>) {
			const emp_dets = <?= json_encode($emp_details) ?>;
			// console.log(emp_dets.EmpCategory);
			// if((emp_dets.EmpCategory).toUpperCase() === 'NON-ACADEMIC'){
			//   activaTab('book');
			// }
			// if((emp_dets.EmpCategory).toUpperCase() === 'ACADEMIC'){
			//   activaTab('book2');
			// }
			// if((emp_dets.EmpCategory).toUpperCase() === 'ADMIN'){
			//   activaTab('book3');
			// }
			if ((emp_dets.EmpCategory).toUpperCase() === 'SUPER ADMIN') {
				activaTab('book5');
			}
			// if((emp_dets.EmpCategory).toUpperCase() === 'OTHERS'){
			//   activaTab('book4');
			// }

		}

		if (<?= json_encode($_POST['staff_termination']) ?>) {
			// alert('here');
			activaTab('book4');
		}


		function activaTab(tab) {
			// alert("here2");
			$('.nav-tabs a[href="#' + tab + '"]').tab('show');
		};



		// $("#scheduleForm").click(function(){

		// 		const workSchedule = $("#workSchedule").val();

		// 		 if(workSchedule === " " ){

		// 				  { parent.msgbox('Enter work schedule ', 'red'); };

		// 		 }
		// 		 else{

		// 			document.getElementById('scheduleProcess').submit();

		// 		 }

		// 	  });
	</script>
</body>

</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';

date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("PAYOFF TERMINATION LETTER" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$emp_details = null;
$other = false;




if (isset($_POST['payoff_termination'])) {
	if (
		empty($_POST['disengaged']) || empty($_POST['deduction'] || empty($_POST['annualLeaveRemaining']))
	) {

		echo ("<script type='text/javascript'>{parent.msgbox('All fields are required', 'red'); }</script>");
	} else {

		$other = true;

		$emp = $_POST['other'];
		// var_dump($emp);
		// die();
		$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

		$emp_details = ScriptRunnercous($Script_Menu);

		// var_dump($emp_details);
		$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
		$EditID = ScriptRunner($Script_Edit, "SetValue17");
		$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
		$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
		// var_dump($emp_detailsHr['sig_img']);

		$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
		$comp_details = ScriptRunnercous($Script_Edit_com);
		$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
		$email_details = ScriptRunnercous($Script_Edit_email);
		$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
		$addr_details = ScriptRunnercous($Script_Edit_addr);
		$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
		$rc_details = ScriptRunnercous($Script_Edit_RC);
		$branchId = $emp_details['BranchID'];
		$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
		$bran_details = ScriptRunnercous($Script_Edit_Br);
		$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
		$fin_details = ScriptRunnercous($Script_Edit_Fin);
		$fin_hash = $fin_details['Scheme'];
		$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
		$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

		$nm = null;
		for ($i = 1; $i <= 25; $i++) {
			if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
				$nm = $i;
				break;
			}
		}
	}
}

if (isset($_POST['approvePayoff'])) {
	if (empty($_POST['numberOfYears'])) {
		$payoffQuery = "UPDATE [Fin_PRCore] 
				SET 
			[numberOfYears]=" . intval($_POST['years']) . ",[createdPayoffDate]=GetDate(), [exitReason]='DISENGAGEMENT', [payoffDate]=GetDate(),
			[exitDate]=CONVERT(DATETIME, '" . $_POST['exitDate'] . "', 120), [lastMonthNetSalary]=" . floatval($_POST['lastMonthNetSalary']) . ", [totalPayoffAmount]=" . floatval($_POST['totalPayoffAmount']) . "
			where EmpID='" . $_POST['EmpID'] . "' and Status='A'";
		ScriptRunnerUD($payoffQuery, "Inst");
	} else {
		$payoffQuery = "UPDATE [Fin_PRCore] 
				SET 
			[numberOfYears]=" . intval($_POST['years']) . ",[updatedPayoffDate]=GetDate(),[exitReason]='DISENGAGEMENT', [payoffDate]=GetDate(),
			[exitDate]=CONVERT(DATETIME, '" . $_POST['exitDate'] . "', 120), [lastMonthNetSalary]=" . floatval($_POST['lastMonthNetSalary']) . ", [totalPayoffAmount]=" . floatval($_POST['totalPayoffAmount']) . "
			where EmpID='" . $_POST['EmpID'] . "' and Status='A'";
		ScriptRunnerUD($payoffQuery, "Inst");
	}
}


$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);
	// var_dump($emp_detailsHr['sig_img']);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
	$rc_details = ScriptRunnercous($Script_Edit_RC);
	$branchId = $emp_details['BranchID'];
	$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
	$bran_details = ScriptRunnercous($Script_Edit_Br);
	$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
	$fin_details = ScriptRunnercous($Script_Edit_Fin);
	$fin_hash = $fin_details['Scheme'];
	$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
	$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

	// var_dump($fin_set_details);
	$nm = null;
	for ($i = 1; $i <= 25; $i++) {
		if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
			$nm = $i;
			break;
		}
	}

	// var_dump($bran_details);

}

?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
		echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
	} else { ?>
		<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
	<script>
		$(function() {
					<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
					} else { ?>
						$("#ExDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-1Y",
							maxDate: "+1D",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})
						$("#EmpDt").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "+1M",
							dateFormat: 'dd M yy',
							yearRange: "-75:+75"
						})

					<?php } ?>
					$("#DOB").datepicker({
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						minDate: "-80Y",
						maxDate: "<?php
									$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
									if (number_format($kk, 0, '', '') == 0 || $kk == "") {
										echo "-18Y";
									} else {
										echo "-" . $kk . "Y";
									} ?>
						", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
					});
	</script>
	<script>
		$(function() {
			$("#tabs").tabs();
		});
		$(function() {
			$(document).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value = '';
			});
		});
	</script>
	<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
			});
		});
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

	<!-- Select 2-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">


	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		* {
			font-family: "Times New Roman", Times, serif !important;

			color: black !important;
		}

		table,
		th,
		td {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px !important;
			/* font-family: "Times New Roman", Times, serif; */
		}

		body {
			font-size: 16px !important;
		}

		.table>tbody>tr>td {
			padding-left: 7px !important;
		}

		.btn-group-sm>.btn,
		.btn-sm {
			font-size: 10px;
			padding: 5px;
			line-height: 20px;
		}

		.box-body ul li {
			line-height: 18px !important;
		}

		.card-img {
			width: 76%;
		}

		.nav-tabs-custom>.tab-content {
			color: black !important;
			text-align: justify;
		}

		.rectangle {
			border: 1px solid black;
			/* padding: 10px; */
		}

		img {
			/* width: 100%;
  height: 160px;
  margin-left: 550px; */
		}
	</style>

</head>


<body>



	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<div class="row">

					<div class="col-md-6  text-md-left text-center">
						<h3>
							Generate Payoff Termination Letter
						</h3>

					</div>
					<div class="col-md-6">
						<form id="theform1" method="post">
							<div class="row">

								<div class="col-4"></div>
								<!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
								<!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control select2" name="bookselect1" id="bookselect1">
										<!-- <select name="level1" id="level1"  class="form-control" > -->
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>



									</select>
									<span class="input-group-btn">
										<button type="button" class="btn btn-danger" id="getall1">Open</button>
									</span>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
			<div class="box-body">



				<?php if (!is_null($emp_details) && !empty($emp_details) || isset($_POST['payoff_termination'])): ?>

					<div class="row">


						<div class="col-md-12">
							<div class="row">
								<div class="col-12">
									<?php //if (strtoupper($emp_details['EmpCategory']) === 'SUPER ADMIN'): 
									?>
									<?php //if (json_encode($_POST['scheduleForm'])): 
									?>
									<div id="invoice4" class="text-dark ">

										<?php if ($other): ?>
											<?php

											// number of years calculation
											$empDate = strtotime($emp_details['EmpDt']->format("Y-m-d"));
											$disengaged = strtotime($_POST['disengaged']);
											$dateDiff = $disengaged - $empDate;

											// $days = ceil(abs($disengaged - $empDate) / 86400);
											$years = floor($dateDiff / (365 * 60 * 60 * 24));

											// number of months calculation
											// Convert dates to timestamps
											$empDateTimestamp = strtotime($emp_details['EmpDt']->format("Y-m-d"));
											$disengagedTimestamp = strtotime($_POST['disengaged']);

											// Create DateTime objects using timestamps
											$date1 = new DateTime("@$empDateTimestamp");
											$date2 = new DateTime("@$disengagedTimestamp");

											// Calculate the difference in months
											$interval = $date1->diff($date2);
											$totalMonths = $interval->y * 12 + $interval->m;

											$lastMonthNetSalary = ceil($fin_details['NetPay'] / 12);
											$totalAmountToBePaid = ceil($lastMonthNetSalary * $years);
											$totalPaymentNotice = ($lastMonthNetSalary) + ($totalAmountToBePaid);
											?>
											<form id="payoffDetails" method="POST">
												<input type="hidden" name="approvePayoff">
												<input type="hidden" name="lastMonthNetSalary" value="<?php echo $lastMonthNetSalary; ?>">
												<input type="hidden" name="totalPayoffAmount" value="<?php echo $totalPaymentNotice; ?>">
												<input type="hidden" name="EmpID" value="<?php echo $emp; ?>">
												<input type="hidden" name="numberOfYears" value="<?php echo $fin_details['numberOfYears']; ?>">
												<input type="hidden" name="years" value="<?php echo $years; ?>">
												<input type="hidden" name="exitDate" value="<?php echo $_POST['disengaged']; ?>">

											</form>


											<div class="row">
												<div class="col-sm-10  mx-auto">
													<div class="row text-center">
														<div class="col-sm-2">
															<!-- <img src="<?= $ComLogo ?>" alt="" width="100" > -->
														</div>

													</div>
												</div>
											</div>
											<!-- <div class="row justify-content-center mt-1 "> -->
											<div style="margin-bottom:5px;">
												<div class="row" style="width:90%;margin-left:40px;">
													<div class="col-sm-3">
														<!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="60%" height="80%"> -->
														<img src="<?= $ComLogo ?>" alt="" style="margin-top:-20px;width: 200px;height: 120px;" />
													</div>
													<div class="col-sm-9">
														<div class="row align-items-center">
															<div class="col-sm-6">
																<div style="display: flex; flex-direction:column; align-items:center; gap: 3px;">
																	<span>NIZAMIYE HOSPITAL LTD</span>
																	<span>ABUJA</span>
																</div>
																<p class="text-center" style="font-weight: bold;">TERMINATION FORM</p>
															</div>
															<div class="col-sm-6 d-flex justify-content-end">
																<div>RC882824</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">
												<div class="row">
													<!-- <div class="col-sm-2"> -->
													<table style="width:90%;margin-left:40px;">

														<tr>
															<td colspan="2" style="padding:0px 80px;text-align: right;">
																<h6 style="font-weight: 600;"><?php echo date("j/n/Y") ?></h6>
															</td>
														</tr>
														<tr style="width: 90%;">
															<td style="padding:5px;width: 30%;">
																<h6 style="font-weight: 600;">STATE</h6>
															</td>
															<?php

															$sqg = "select * from BrhMasters where Hashkey = '{$emp_details['BranchID']}' ";

															$sqgR = ScriptRunnercous($sqg);

															?>

															<!-- <td style="padding:5px"><?php //echo strtoupper($sqgR['OName']); 
																							?></td> -->
															<td style="padding:5px">ABUJA</td>
														</tr>
														<tr>
															<td style="padding:5px;width: 30%;">
																<h6 style="font-weight: 600;">SECTION</h6>
															</td>
															<td style="padding:5px;"><?php echo strtoupper($emp_details['EmpCategory']); ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 30%;">
																<h6 style="font-weight: 600;">STAFF NAME</h6>
															</td>
															<td style="padding:5px;"><?php echo strtoupper("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}") ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 30%;">
																<h6 style="font-weight: 600;">DESIGNATION</h6>
															</td>
															<td style="padding:5px;">
																<?php

																$sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') order by [ID] desc ";

																$sqR = ScriptRunnercous($sq);

																echo (isset($sqR) ? strtoupper($sqR['ItemPos']) : '');

																?>
															</td>
														</tr>
														<tr>
															<td style="padding:5px;width: 30%;">
																<h6 style="font-weight: 600;">REASON OF EXIT</h6>
															</td>
															<!-- <td style="padding:5px;"><?php //echo $_POST['reasonOfExit']; 
																							?></td> -->
															<td style="padding:5px;">DISENGAGEMENT</td>
														</tr>
													</table>
													<table style="width:90%;margin-left:40px; margin-bottom: 15px;">
														<tr>
															<td style="padding:5px;width: 55%;">
																<h6 style="font-weight: 600;">EMPLOYMENT DATE</h6>
															</td>
															<td style="padding:5px;width: 45%;"><?= $emp_details['EmpDt'] ? $emp_details['EmpDt']->format("d F, Y")  : ""   ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">DISENGAGED</h6>
															</td>
															<td style="padding:5px;"><?= date('d M, Y', strtotime($_POST['disengaged'])); ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">NUMBER OF YEARS IN NIZAMIYE</h6>
															</td>
															<td style="padding:5px;">
																<?php
																$empDate = strtotime($emp_details['EmpDt']->format("Y-m-d"));
																$disengaged = strtotime($_POST['disengaged']);
																$dateDiff = $disengaged - $empDate;

																// $days = ceil(abs($disengaged - $empDate) / 86400);
																$years = floor($dateDiff / (365 * 60 * 60 * 24));

																echo $years;
																?>
															</td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">NUMBER OF MONTHS IN NIZAMIYE</h6>
															</td>
															<td style="padding:5px;">
																<?php
																// Convert dates to timestamps
																$empDateTimestamp = strtotime($emp_details['EmpDt']->format("Y-m-d"));
																$disengagedTimestamp = strtotime($_POST['disengaged']);

																// Create DateTime objects using timestamps
																$date1 = new DateTime("@$empDateTimestamp");
																$date2 = new DateTime("@$disengagedTimestamp");

																// Calculate the difference in months
																$interval = $date1->diff($date2);
																$totalMonths = $interval->y * 12 + $interval->m;

																echo $totalMonths;

																$lastMonthNetSalary = ceil($fin_details['NetPay'] / 12);
																$totalAmountToBePaid = ceil($lastMonthNetSalary * $years);
																$totalPaymentNotice = ($lastMonthNetSalary) + ($totalAmountToBePaid)
																?>
															</td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">LAST MONTH NET SALARY</h6>
															</td>
															<td style="padding:5px;">
																<?php
																// ($years > 3) ? $_POST['lastMonthNetSalary'] * $years : $_POST['lastMonthNetSalary']
																echo number_format($lastMonthNetSalary, 2);
																?>
															</td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">DEDUCTION</h6>
															</td>
															<td style="padding:5px;">&#8358;<?php echo $_POST['deduction'] ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">TOTAL AMOUNT TO BE PAID</h6>
															</td>
															<td style="padding:5px;">&#8358;<?= number_format($totalAmountToBePaid, 2) ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">NOTICE PAYMENT (1 MONTH SALARY)</h6>
															</td>
															<td style="padding:5px;">&#8358;<?= number_format($lastMonthNetSalary, 2); ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">ANNUAL LEAVE REMAINING</h6>
															</td>
															<td style="padding:5px;"><?php echo $_POST['annualLeaveRemaining'] ?></td>
														</tr>
														<tr>
															<td style="padding:5px;width: 40%;">
																<h6 style="font-weight: 600;">TOTAL PAYMENT AND NOTICE</h6>
															</td>
															<td style="padding:5px;">&#8358;<?= number_format($totalPaymentNotice, 2); ?></td>
														</tr>

													</table>
												</div>
											</div>

											<p style="margin-left: 35px;font-size:small;">I .........................................................................................................................................................................................................................................................
											<p style="margin-left: 35px;font-size:small;">collected the sum of <label style="width:40%;text-align: center !important;">&#8358;<?= number_format($totalPaymentNotice, 2); ?> </label> as my total Notice Payment and pay off Gratuity.<br>
												<label style="margin-bottom: 15px;font-size:small;">I shall forthwith have no dealings with Nizamiye Hospital limited.
											</p>

											<!-- </div> -->

											<div class="row mt-3">
												<div class="col-sm-4">
													<p style="margin-left: 35px;">........................................<br>
														<label style="font-weight: 600;margin-left: 35px;text-align:center;font-size:small;">Staff Signature & Date</label>
												</div>
												<div class="col-sm-4">
													<p>..............................................<br>
														<label style="font-weight: 600;text-align: center;font-size:small;">Accountant Signature & Date</label>
												</div>
												<div class="col-sm-4">
													<p>.................................................<br>
														<label style="font-weight: 600;text-align: center;font-size:small;">HR Manager Signature & Date</label>
												</div>
											</div>

											<!-- <p style="font-weight: 600;margin-left: 35px;font-size:small;">BANK NAME: <br>
												<span style="font-weight: 600;">ACCOUNT NUMBER: </span> -->
											<div class="text-center">
												<p class="text-center" style="margin-top:20px;">.........................................................<br>
													<label for="approved" style="font-weight: 600;text-align: center;font-size:small;">APPROVED</label>
												</p>
											</div>

											<div class="row d-flex justify-content-between px-5" style="margin-top: 10px;">

												<div class="" style="margin-left: 15px">
													<h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
													<div class="parent  d-flex flex-gap-3">
														<div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
														<div class="context">
															<img width="230px" src="../pfimg/hos.png" alt="">
														</div>

													</div>
												</div>
												<div class="" style="margin-right: 15px">
													<h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
													<div class="parent  d-flex flex-gap-3">
														<div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
														<div class="context">
															<img width="230px" src="../pfimg/branch.png" alt="">
														</div>

													</div>
												</div>

											</div>










											<!-- /.tab-content -->
											<!-- </div> -->
											<!-- /.nav-tabs-custom -->
									</div>
									<button class="btn btn-sm btn-danger text-white p-3" id="download4" onclick="downloadInvoice4()">Print</button>

									<button type="button" class="btn btn-sm btn-danger  text-white p-3" id="approvebtn">Approve</button>

									<?php //endif;
									?>

								</div>
								<!-- /.col -->



							<?php else: ?>



								<form action="" method="post" id="scheduleProcess" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row ">
												<label class="col-sm-4 col-form-label">Deduction <span style="color: red !important">*</span>:</label>
												<div class="col-sm-8 input-group-sm">
													<input type="number" name="deduction" class="form-control" id="deduction">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group row ">
												<label class="col-sm-4 col-form-label">Disengaged <span style="color: red !important">*</span>:</label>
												<div class="col-sm-8 input-group-sm">
													<input name="disengaged" id="disengaged" type="date" class="form-control" />
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group row ">
												<label class="col-sm-4 col-form-label">Annual Leave Remainig <span style="color: red !important">*</span>:</label>
												<div class="col-sm-8 input-group-sm">
													<input type="text" name="annualLeaveRemaining" class="form-control" id="annualLeaveRemaining">
												</div>
											</div>
										</div>
									</div>

									<input type="hidden" name="other" value="<?= $_POST['bookselect1'] ?>">
									<div class="col-md-12 text-center">
										<input id="payoff_termination" name="payoff_termination" type="submit" style="width:5%;" class="btn btn-danger btn-sm" value="Submit" />
									</div>
								</form>


							<?php endif; ?>



							</div>

						</div>

					</div>

				<?php endif; ?>




			</div>
		</div>
		<!-- /.box-body -->




		</div>

		<!-- /.row -->
	</section>
	<!-- /.content -->


	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="html2pdf/dist/html2pdf.bundle.js"></script>


	<script>
		$("#approvebtn").click(function() {
			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText>Do you wish to <strong>approve</strong> the record(s)?</h5></div>')
				.dialog({
					modal: true,
					title: "Approve Payoff",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {

							// var form_1 = $('#payoffDetails');
							// console.log({form_1});
							var form_1 = document.getElementById('payoffDetails');
							var formData = new FormData(form_1);

							// Submit form data using fetch
							fetch(form_1.action, {
									method: 'POST',
									body: formData
								})
								.then(parent.msgbox('Payoff Appproved successfully', 'green'))
								.then(response => response.json()) // assuming the server returns JSON
								.then(data => {
									console.log('Success:', data);
									// Handle the response data as needed
								})
								.catch((error) => {
									console.error('Error:', error);
								});

							// Close the dialog
							$(this).dialog("close");
						},
						No: function() {
							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
			return;
		});


		function downloadInvoice1() {
			const invoice = document.getElementById("invoice");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_NON_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.90, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice).set(opt).save();


			html2pdf().from(invoice).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice2() {
			const invoice2 = document.getElementById("invoice2");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ACADEMIC_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice2).set(opt).save();

			html2pdf().from(invoice2).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice3() {

			const invoice3 = document.getElementById("invoice3");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_ADMIN_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice3).set(opt).save();

			html2pdf().from(invoice3).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice4() {

			const invoice4 = document.getElementById("invoice4");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_PAYOFF_TERMINATION ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice4).set(opt).save();

			html2pdf().from(invoice4).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}

		function downloadInvoice5() {

			const invoice5 = document.getElementById("invoice5");
			const opt = {
				html2canvas: {
					scale: 4
				},
				filename: '<?= ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_SUPER_ADMIN_SFAFF ") . date("Y-m-d H:i:s") ?>',
				margin: [.9, .75, .75, .75],
				image: {
					type: 'jpeg',
					quality: 1
				},
				jsPDF: {
					unit: 'cm',
					orientation: 'p'
				}
			};

			// html2pdf().from(invoice5).set(opt).save();

			html2pdf().from(invoice5).set(opt).toPdf().get('pdf').then(function(pdf) {
				// Open the PDF in a new tab
				const pdfBlob = pdf.output('blob');
				const pdfURL = URL.createObjectURL(pdfBlob);
				window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

				// Alternatively, trigger the browser print dialog directly
				window.open(pdfURL).print();
			});
		}
	</script>

	<script type="text/javascript">
		function getDetail(a, event) {

			event.preventDefault();

			let curren = $(a).closest("tr").attr("id");
			let nexxt = $(a).closest("tr").next().attr("id");
			let prevv = $(a).closest("tr").prev().attr("id");

			$("#next" + curren).val(nexxt);
			$("#prev" + curren).val(prevv);

			// console.log(curren,nexxt,prevv);

			$("#form" + curren).submit();


		}
	</script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>


	<!-- MinimalLite Admin App -->
	<script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<script>
		$('.select2').select2();
	</script>
	<!-- MinimalLite Admin for Data Table -->
	<!-- <script src="../assets/js/pages/data-table.js"></script> -->

	<script type="text/javascript">



	</script>


	<script>
		$('#example1').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example6').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		$('#example7').DataTable({
			'lengthChange': false,
			'paging': false,
		});

		setInterval(function() {
			$(".tablesorter-filter-row").remove();
		}, 2000);

		function amp(id) {


			var val = $('#' + id).val();
			// console.log(val);

			if (val == '') {
				$('#com' + id).prop('disabled', true);
				window['assignedId'] = val;

				{
					parent.msgbox('Please select an item', 'green');
				};

			} else {
				$('#com' + id).prop('disabled', false);
				window['assignedId'] = val;
			}

		}

		function confirm(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delete Booking Items",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
								.dialog({
									modal: true,
									title: "Confirm Custom Message",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {


											let name = '<?= $employee_name ?>';
											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "confirmed",
													val: assignedId
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed,a confirmation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});






											$(this).dialog("close");
										},
										No: function() {

											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});
		}

		function decline(id, email) {

			$('<div></div>').appendTo('body')
				.html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
				.dialog({
					modal: true,
					title: "Delicine Custom Message",
					zIndex: 10000,
					autoOpen: true,
					width: 'auto',
					resizable: true,
					height: 'auto',
					resizable: true,
					buttons: {
						Yes: function() {
							let name = '<?= $employee_name ?>';

							// parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

							$('<div></div>').appendTo('body')
								.html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

									modal: true,
									title: "Enter Custom Message",
									zIndex: 10000,
									autoOpen: true,
									height: 200 + 6,
									width: 500 + 20,
									buttons: {

										Send: function() {

											var msgC = document.getElementById("sd").value;



											if (msgC == '') {

												{
													parent.msgbox('Enter custom message', 'red');
												};

												return;

											}

											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined",
													val: assignedId,
													msgC: msgC
												},
												success: function(response) {

													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});




											$(this).dialog("close");

										}
									}

								});





							$(this).dialog("close");
						},
						No: function() {

							$('<div></div>').appendTo('body')
								.html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
								.dialog({
									modal: true,
									title: "Delete Booking Items",
									zIndex: 10000,
									autoOpen: true,
									width: 'auto',
									resizable: true,
									height: 'auto',
									resizable: true,
									buttons: {
										Yes: function() {

											let name = '<?= $employee_name ?>';



											$.ajax({
												url: "add_bookitem.php",
												method: "POST",
												data: {
													id: id,
													email: email,
													name: name,
													type: "declined"
												},
												success: function(response) {
													if (response === 'OK') {

														// console.log(allInfo);
														{
															parent.msgbox('Request confirmed, cancellation email has been sent to ' + name, 'green');
														};
														location.reload();




													} else if (response === "Wrong") {

														{
															parent.msgbox('Something went wrong', 'red');
														};

													}
												},
												error: function() {
													alert('Something went wrong');
												}
											});



											$(this).dialog("close");
										},
										No: function() {
											$(this).dialog("close");
										}
									},
									close: function(event, ui) {
										$(this).remove();
									}
								});





							$(this).dialog("close");
						}
					},
					close: function(event, ui) {
						$(this).remove();
					}
				});



		}



		$("#getall1").click(function() {

			let bookSelected = $('#bookselect1').val();


			if (bookSelected === "") {



				{
					parent.msgbox('Please select an employee', 'red');
				};

				return;

			} else {

				$('#theform1').submit();





			}



		});


		if (<?= json_encode($emp_details) ?>) {
			const emp_dets = <?= json_encode($emp_details) ?>;
			// console.log(emp_dets.EmpCategory);
			// if((emp_dets.EmpCategory).toUpperCase() === 'NON-ACADEMIC'){
			//   activaTab('book');
			// }
			// if((emp_dets.EmpCategory).toUpperCase() === 'ACADEMIC'){
			//   activaTab('book2');
			// }
			// if((emp_dets.EmpCategory).toUpperCase() === 'ADMIN'){
			//   activaTab('book3');
			// }
			if ((emp_dets.EmpCategory).toUpperCase() === 'SUPER ADMIN') {
				activaTab('book5');
			}
			// if((emp_dets.EmpCategory).toUpperCase() === 'OTHERS'){
			//   activaTab('book4');
			// }

		}

		if (<?= json_encode($_POST['payoff_termination']) ?>) {
			// alert('here');
			activaTab('book4');
		}


		function activaTab(tab) {
			// alert("here2");
			$('.nav-tabs a[href="#' + tab + '"]').tab('show');
		};



		// $("#scheduleForm").click(function(){

		// 		const workSchedule = $("#workSchedule").val();

		// 		 if(workSchedule === " " ){

		// 				  { parent.msgbox('Enter work schedule ', 'red'); };

		// 		 }
		// 		 else{

		// 			document.getElementById('scheduleProcess').submit();

		// 		 }

		// 	  });
	</script>
</body>

</html>
<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("PROBATION LETTER" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

?>

<head>
	<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
			echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
		} else { ?>
			<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
		<script>
			$(function() {
						<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {
						} else { ?>
							$("#ExDt").datepicker({
								changeMonth: true,
								changeYear: true,
								showOtherMonths: true,
								selectOtherMonths: true,
								minDate: "-1Y",
								maxDate: "+1D",
								dateFormat: 'dd M yy',
								yearRange: "-75:+75"
							})
							$("#EmpDt").datepicker({
								changeMonth: true,
								changeYear: true,
								showOtherMonths: true,
								selectOtherMonths: true,
								minDate: "-80Y",
								maxDate: "+1M",
								dateFormat: 'dd M yy',
								yearRange: "-75:+75"
							})

						<?php } ?>
						$("#DOB").datepicker({
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: "-80Y",
							maxDate: "<?php
										$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
										if (number_format($kk, 0, '', '') == 0 || $kk == "") {
											echo "-18Y";
										} else {
											echo "-" . $kk . "Y";
										} ?>
							", dateFormat: 'dd M yy', yearRange: " - 75: +75 "})
						});
		</script>
		<script>
			$(function() {
				$("#tabs").tabs();
			});
			$(function() {
				$(document).tooltip();
			});
			$(function() {
				$("#ClearDate").click(function() {
					document.getElementById("ExDt").value = '';
				});
			});
		</script>
		<script>
			$(function() {
				$("#COO").change(function() {
					var Pt = $("#COO").val();
					var Replmt = Pt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent=" + Replmt);
				});
				$("#SOO").change(function() {
					var Pt = $("#SOO").val();
					var Replmt = Pt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					var Replmt = Replmt.replace(" ", "+");
					$("#LGA").load("../main/getCh.php?Choice=LGA&Parent=" + Replmt);
				});
			});
		</script>

		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
		<style>
			* {
				font-family: "Times New Roman", Times, serif !important;

				color: black !important;
			}

			table,
			th,
			td {
				border: 1px solid black;
				border-collapse: collapse;
				font-size: 11px !important;
				/* font-family: "Times New Roman", Times, serif; */
			}

			body {
				font-size: 16px !important;
			}

			.table>tbody>tr>td {
				padding-left: 7px !important;
			}

			.btn-group-sm>.btn,
			.btn-sm {
				font-size: 10px;
				padding: 5px;
				line-height: 20px;
			}

			.box-body ul li {
				line-height: 18px !important;
			}

			.card-img {
				width: 76%;
			}

			.nav-tabs-custom>.tab-content {
				color: black !important;
				text-align: justify;
			}


			/* <style type="text/css"> */
			#from_time:before {
				content: '12:30 PM';
				margin-right: .6em;
				color: #9d9d9d;
			}

			#to_time:before {
				content: '12:30 PM';
				margin-right: .6em;
				color: #9d9d9d;
			}
		</style>
	</head>
	<?php if ($_SESSION['StkTckCustID'] === "092f904d7a11f9ad2c827afe889d7cda") {
		include 'staff_probation_niz.php';
	} else { ?>
		<?php

		// var_dump($_POST['']);

		if (empty($_POST)) : ?>
			<div class="box">
				<div class="box-header with-border  ">
					<div class="row">
						<div class="col-md-12 ">
							<h3 id="rty">
								Probation Form Details
							</h3>
						</div>
					</div>
				</div>
				<form action="#" method="post" id="probationProcess" enctype="multipart/form-data">
					<div class="box-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label">Staff Name <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="text" name="staffName" class="form-control" id="staffName">
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label">Staff Address <span style="color: red">*</span>:</label>
									<div class="col-sm-8 ">
										<textarea type="text" rows="3" name="staffAddress" maxlength="200" class="form-control" id="staffAddress"></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Designation <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm">
										<input type="text" name="designation" class="form-control" id="designation" />
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Supervisor <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm">
										<input type="text" name="supervisor" class="form-control" id="supervisor" />
									</div>
								</div>

							</div>
							<div class="col-md-4">
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label">Office Location: <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="text" name="officeLocation" class="form-control" id="officeLocation" />
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label">Agreement Date <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input name="agreementDate" id="agreementDate" type="date" class="form-control" maxlength="12" />
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label">Resumption Date <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input name="resumptionDate" id="resumptionDate" type="date" class="form-control" maxlength="12" />
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Section <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="text" name="section" class="form-control" id="section" />
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Probation period <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="text" name="probationPeriod" class="form-control" id="probationPeriod" />
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Work Schedule <span style="color: red">*</span>:</label>
									<div class="col-sm-8 ">
										<textarea type="text" rows="3" name="workSchedule" maxlength="200" class="form-control" id="workSchedule"></textarea>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Salary <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="text" name="salary" class="form-control" id="salary" />
									</div>
								</div>

								<div class="col-md-12 text-center">
									<input name="staff_probation" id="staff_probation" type="button" class="btn btn-danger btn-sm" value="Submit" />
								</div>

							</div>
						</div>
					</div>
					<!-- /.box-body -->
			</div>


			<!-- <div class="form-group row">
						<
					</div> -->
			</div>
			<div class="col-md-2"></div>
			</div>
			</div>
			</form>
			</div>
		<?php endif; ?>

		<!-- Main content -->
		<?php if (!empty($_POST)) : ?>
			<section class="content">

				<div class="box">
					<div class="box-header with-border">
						<div class="row" id="invoice">
							<div class="col-md-12">
								<div class="row">
									<div class="col-12">

										<div class="col-md-12  text-center">
											<h6 style="margin-bottom: 30px;margin-top: 150px;font-weight: 600;">EMPLOYMENT PROBATIONAL AGREEMENT </h6>
										</div>

										<p style="margin-left: 10px;">
											THIS AGREEMENT is made this <?php echo date('d M, Y', strtotime($_POST['agreementDate'])); ?> between [NIGERIAN TULIP INTERNATIONAL COLLEGES LIMITED]
											corporation incorporated under the Companies and Allied Matters Acts laws of the Federal Republic
											of Nigeria, and having its principal place of business at Plot 152 Ahmadu Bello way by
											Kashim Ibrahim Way Wuse II Abuja- FCT, (the "Employer of one part’’); and <?php echo ucwords($_POST['staffName']) ?>,
											of <?php echo ucwords($_POST['staffAddress']) ?>, (the "Employee of the other part").
										</p>
										<!-- <div class="row justify-content-center mt-1 "> -->


										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">1. EMPLOYMENT DETAILS</p>
										<p style="margin-bottom: 0px;margin-left: 10px;">I. <span style="margin-left: 18px;"> POSITION DETAILS</p>
										<p style="margin-bottom: 0px;margin-left: 10px;">You will resume on <?php echo date('d M, Y', strtotime($_POST['resumptionDate'])); ?> at <?php echo ucwords($_POST['officeLocation']) ?> as a <?php echo ucwords($_POST['designation']) ?> in the <?php echo ucwords($_POST['section']) ?> reporting to the <?php echo ucwords($_POST['supervisor']) ?>. This offer is for a <?php echo ucwords($_POST['probationPeriod']) ?> probation after which the Employer may decide to continue with the appointment or disengage without prejudice.

										<p style="margin-bottom: 0px;margin-left: 10px;">II. <span style="margin-left: 10px;"> WORK SCHEDULE</p>
										<p style="margin-bottom: 0px;margin-left: 10px;">This is a full-time position. Your regular weekly schedule will be <?php echo ucwords($_POST['workSchedule']) ?> with one (1) hour lunch break. </p>
										<p style="margin-bottom: 0px;margin-left: 10px;">Your schedule of duties shall be made available upon resumption and may be subject to review solely at Managements directives.
											If need be, you will be asked to work outside the normal hours and be paid for accordingly.
											The organization will make available information on Public Holidays as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday, you will be informed accordingly by your reporting Supervisor/ Manager.</p>

										<p style="margin-bottom: 0px;margin-left: 10px;">III. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</p>

										<ul style="margin-left: 20px;">

											<li>The Employee on probation is not entitled to any form of Leave.</li>
											<li>Annual Leave, Maternity Leave, Casual Leave will be granted upon confirmation only when Employee has served the Employer for a period of twelve (12) months. </li>
											<li>Compassionate Leave is up to five (5) days or more depending on Management’s discretion. For unconfirmed Employees, this Leave is without pay and may be granted in the case of death of spouse, death of biological mother/father, death of child, death of biological sibling, as well as other similar situations approved by Management. </li>

										</ul>

										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">2. CONFLICT OF INTEREST POLICY</p>
										<p style="margin-left: 15px;margin-bottom: 0px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest. By signing this Probational Agreement, you confirm that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ.</p>


										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">3. DECLARATION OF INFORMATION</p>
										<p style="margin-left: 15px;margin-bottom: 0px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operation(s) in any manner as soon as it comes to your knowledge.</p>

										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">4. NON-DISCLOSURE AGREEMENT</p>
										<p style="margin-left: 15px;margin-bottom: 0px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by virtue of your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, your duty is to safeguard and not disclose Confidential Information will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">5. RENUMERATION </p>
										<p style="margin-left: 15px;margin-bottom: 0px;">The Employer shall pay the employee N<?php echo $_POST['salary'] ?> (net of all deductions) per month until the expiration of the probationary period. Employee should note that after the probationary period the amount written MAY BE REVIEWED by the Employer. While on probation, Employee is not entitled to loans, allowances or advances of any kind.</p>
										<div class="html2pdf__page-break"></div>
										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">6. PENSION </p>
										<p style="margin-left: 15px;margin-bottom: 0px;">The employee pension contributions will be calculated and remitted after expiration of the probationary period; however, the employee should note that no cash shall be given to Employee in respect of pension remittance.</p>
										<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;">7. TERMINATION CONDITIONS </p>
										<p style="margin-bottom: 0px;margin-left: 20px;">I. <span style="margin-left: 10px;"> During your probational period, you will be governed by the Condition of Service and all other policies including confidentiality which continues whether or not you are confirmed at the end of your probation.</p>
										<p style="margin-bottom: 0px;margin-left: 20px;">II.<span style="margin-left: 10px;"> The Employer reserves the right to summarily dismiss any employee for any misconduct (incompetence, disobedience, negligence, criminal offence etc.) without notice and without payment in lieu of notice. Kindly refer to Employers Disciplinary Code for details of misconduct.</p>
										<p style="margin-bottom: 0px;margin-left: 20px;">III..<span style="margin-left: 10px;"> This agreement may be terminated by either party with one (1) days’ notice during the <?php echo ucwords($_POST['probationPeriod']) ?> probation.</p>
										<p style="margin-left: 15px;margin-bottom: 0px;">You may indicate your agreement with these terms and accept this offer by signing below.</p>


										<!-- </div> -->

										<div class="row mt-4">
											<div class="col-sm-6 text-right">
												<p>...............................................</p>
												<p style="font-weight: 600">EMPLOYER & DATE</p>
											</div>
											<div class="col-sm-4 ml-4">
												<p>...............................................</p>
												<p style="font-weight: 600">EMPLOYEE & DATE</p>
											</div>
										</div>



									</div>
								</div>
							</div>




							<!-- /.tab-content -->
						</div>
						<!-- /.nav-tabs-custom -->

						<button class="btn btn-sm btn-danger" id="download">Download</button>

					</div>
					<!-- /.col -->




				<?php endif; ?>

			<?php } ?>


			<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>



			<!-- SlimScroll -->
			<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>


			<!-- FastClick -->
			<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

			<!-- MinimalLite Admin App -->
			<script src="../assets/js/template.js"></script>

			<!-- MinimalLite Admin for demo purposes -->
			<script src="../assets/js/demo.js"></script>

			<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>

			<script type="text/javascript">
				<?php if ($_SESSION['StkTckCustID'] === "092f904d7a11f9ad2c827afe889d7cda") { ?>

					$("#submitForms").click(function() {
						// Validate the first form
						let bookSelected = $('#bookselect1').val();
						if (bookSelected === "") {
							parent.msgbox('Please select an employee', 'red');
							return;
						}

						// Validate the second form
						const agreementDate = $("#agreementDate").val();
						const resumptionDate = $("#resumptionDate").val();
						const probationPeriod = $("#probationPeriod").val();
						const workSchedule = $("#workSchedule").val();

						if (agreementDate === "" || resumptionDate === "" || probationPeriod === "" || workSchedule === "") {
							parent.msgbox('All fields are compulsory', 'red');
							return;
						}

						// If both forms are valid, submit both forms
						$('#theform1').submit();
						$('#probationProcess').submit();
					});
				<?php } else { ?>
					$("#staff_probation").click(function() {

						const staffName = $("#staffName").val();
						const staffAddress = $("#staffAddress").val();
						const designation = $("#designation").val();
						const supervisor = $("#supervisor").val();
						const officeLocation = $("#officeLocation").val();
						const agreementDate = $("#agreementDate").val();
						const resumptionDate = $("#resumptionDate").val();
						const section = $("#section").val();
						const probationPeriod = $("#probationPeriod").val();
						const workSchedule = $("#workSchedule").val();
						const salary = $("#salary").val();

						if (staffName === "" || staffAddress === "" || designation === "" || supervisor === "" || officeLocation === "" || agreementDate === "" || resumptionDate === "" || section === "" || probationPeriod === "" || workSchedule === "" || salary === "") {
							{
								parent.msgbox('All fields are are compulsory ', 'red');
							};
						} else {
							document.getElementById('probationProcess').submit();
						}


					})
				<?php } ?>
			</script>


			<!-- Bootstrap 4.0-->
			<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
			<script src="html2pdf/dist/html2pdf.bundle.js"></script>


			<script>
				window.onload = function() {
					document.getElementById("download").addEventListener("click", () => {
						const invoice = document.getElementById("invoice");


						const opt = {
							filename: '<?= ucwords("{$_POST['staffName']}_PROBATIONAL AGREEMENT") . date("Y-m-d H:i:s") ?>',
							margin: [2, .5, .5, .5],
							image: {
								type: 'jpeg',
								quality: 1
							},
							jsPDF: {
								unit: 'cm',
								orientation: 'p'
							}
						};

						// html2pdf().from(invoice).set(opt).save();


						html2pdf().from(invoice).set(opt).toPdf().get('pdf').then(function(pdf) {
							// Open the PDF in a new tab
							const pdfBlob = pdf.output('blob');
							const pdfURL = URL.createObjectURL(pdfBlob);
							window.open(pdfURL, '_blank'); // Opens the PDF in a new browser tab

							// Alternatively, trigger the browser print dialog directly
							window.open(pdfURL).print();
						});
					})
				}
			</script>










	</html>
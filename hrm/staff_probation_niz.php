<?php

$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

$emp_details = null;

if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
	$emp = $_POST['bookselect1'];
	$Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

	$emp_details = ScriptRunnercous($Script_Menu);
	$Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
	$EditID = ScriptRunner($Script_Edit, "SetValue17");
	$Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
	$emp_detailsHr = ScriptRunnercous($Script_MenuHr);

	$Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
	$comp_details = ScriptRunnercous($Script_Edit_com);
	$Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
	$email_details = ScriptRunnercous($Script_Edit_email);
	$Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
	$addr_details = ScriptRunnercous($Script_Edit_addr);
	$Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
	$rc_details = ScriptRunnercous($Script_Edit_RC);
	$branchId = $emp_details['BranchID'];
	$Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
	$bran_details = ScriptRunnercous($Script_Edit_Br);
	$Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
	$fin_details = ScriptRunnercous($Script_Edit_Fin);
	$fin_hash = $fin_details['Scheme'];
	$Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
	$fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);

	$nm = null;
	for ($i = 1; $i <= 25; $i++) {
		if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
			$nm = $i;
			break;
		}
	}
}
?>
<?php
if (empty($_POST)): ?>
	<form action="#" method="post" id="probationProcess" enctype="multipart/form-data">
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<div class="row">

						<div class="col-md-6  text-md-left text-center">
							<h3>
								Generate Employee Probation Letter
							</h3>

						</div>
						<div class="col-md-6">
							<div class="row">

								<div class="col-4"></div>
								<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
									<select class="form-control select2" name="bookselect1" id="bookselect1">
										<option value="">--</option>

										<?php
										$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
										$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
										if ($conn2 === false) {
											echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
										}
										$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

										$result2 = sqlsrv_query($conn2, $dbOpen4);
										if ($result2 === false) {
											die(print_r(sqlsrv_errors(), true));
										}
										$emp_hash = '';
										if (!is_null($emp_details) && !empty($emp_details)) {
											$emp_hash = $emp_details['HashKey'];
										}
										while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
											<option value="<?php echo $row3['HashKey']; ?>" <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?>>
												<?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
											</option>
										<?php endwhile;
										sqlsrv_free_stmt($result2);

										?>



									</select>
								</div>
							</div>

	</form>

	</div>
	</div>
	</div>


	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group row ">
					<label class="col-sm-4 col-form-label">Agreement Date <span style="color: red">*</span>:</label>
					<div class="col-sm-8 input-group-sm">
						<input name="agreementDate" id="agreementDate" type="date" class="form-control" maxlength="12" />
					</div>
				</div>
				<div class="form-group row ">
					<label class="col-sm-4 col-form-label">Resumption Date <span style="color: red">*</span>:</label>
					<div class="col-sm-8 input-group-sm">
						<input name="resumptionDate" id="resumptionDate" type="date" class="form-control" maxlength="12" />
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Reporting To <span style="color: red">*</span>:</label>
					<div class="col-sm-8 ">
						<textarea type="text" rows="3" name="workSchedule" maxlength="200" class="form-control" id="workSchedule"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12 text-center">
				<input name="submitForms" id="submitForms" type="button" class="btn btn-danger btn-sm" value="Submit" />
			</div>
		</div>
	</div>
	</div>
	</div>
	<div class="col-md-2"></div>
	</div>
	</div>
	</form>
	</div>
<?php endif; ?>

<!-- Main content -->
<?php if (!is_null($emp_details) && !empty($emp_details)): ?>
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<div class="row" id="invoice">
					<div class="col-md-12">
						<div class="row">
							<div class="col-12">
								<div class="row" style="width:100%;">
									<div class="col-sm-12">
										<div class="row align-items-center">
											<div class="col-sm-9">
												<!-- <img src="../pfimg/niz_logo_bg.png" alt="" width="128" height="112"> -->
												<img src="<?= $ComLogo ?>" alt="" style="margin-top:0px;width: 150px;height: 80px;" />
											</div>
											<div class="col-sm-3 d-flex justify-content-end" style="margin-top: 0px;">
												<span style="font-weight: 600;">RC882824</span>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-12  text-center">
									<p style="margin-bottom: 30px;font-weight: 600;font-size:small;">EMPLOYMENT PROBATIONAL AGREEMENT </p>
								</div>

								<div style="background-image: url('../pfimg/watermark.png'); background-repeat: no-repeat; background-size:40%; background-position:center;">

									<p style="margin-left: 10px;font-size:10px;">
										THIS AGREEMENT is made this <span style="font-weight: 600;"><?php echo date('d/m/Y', strtotime($_POST['agreementDate'])); ?></span> between <span style="font-weight: 600;">[NIZAMIYE HOSPITAL LTD] </span>
										a corporation incorporated under the Companies and Allied Matters Acts laws of the Federal Republic
										of Nigeria, and having its principal place of business <span style="font-weight: 600;">at Plot 113, Sector S cadastral zone life camp- FCT ABUJA,</span> (the "Employer of one part’’); and <span style="font-weight: 600;"><?php echo ucwords("[{$emp_details['FName']} {$emp_details['SName']} {$emp_details['ONames']}]") ?></span>,
										of <span style="font-weight: 600;"><?php echo ucwords("[{$emp_details['HmAdd']}]") ?></span> (the "Employee of the other part").
									</p>
									<!-- <div class="row justify-content-center mt-1 "> -->


									<p style="font-weight: 600;font-size:10px;margin-left: 15px;margin-bottom: 0px;">1. EMPLOYMENT DETAILS</p>
									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">I. <span style="margin-left: 18px;"> POSITION DETAILS</p>
									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">You will resume on <span style="font-weight: 600;"><?php echo date('d M, Y', strtotime($_POST['resumptionDate'])); ?></span> at NIZAMIYE HOSPITAL as <span style="font-weight: 600;"><?php $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') ORDER BY [ID] DESC ";

																																																																							$sqR = ScriptRunnercous($sq);

																																																																							echo ucwords("[{$sqR['ItemPos']}]") ?></span> reporting to the <span style="font-weight: 600;"><?php echo ucwords("[HOD {$emp_details['Department']}]") ?></span> This offer is for <span style="font-weight: 600;">three (3) months'</span> probation after which the Employer may decide to continue with the appointment or disengage without prejudice.

									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">II. <span style="margin-left: 10px;"> WORK SCHEDULE</p>
									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">This is a full-time position. Your regular weekly schedule will be <span style="font-weight: 600;">Monday-Friday (8am-5pm) Saturday (8am-2pm)</span> with one (1) hour lunch break. </p>
									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">Your schedule of duties shall be made available upon resumption and may be subject to review solely at Managements directives.
										If need be, you will be asked to work outside the normal hours and be paid for accordingly.
										The organization will make available information on Public Holidays as declared by the Federal Government. However, if you are expected to be on duty on Public Holiday, you will be informed accordingly by your reporting Supervisor/ Manager.</p>

									<p style="margin-bottom: 0px;margin-left: 10px;font-size:10px;">III. <span style="margin-left: 10px;"> LEAVE ENTITLEMENT</p>

									<ul style="margin-left: 20px;font-size:10px;">

										<li>The Employee on probation is not entitled to any form of Leave.</li>
										<li>Annual Leave, Maternity Leave, Casual Leave will be granted upon confirmation only when Employee has served the Employer for a period of twelve (12) months. </li>
										<li>Compassionate Leave is up to three (3) days or more depending on Management’s discretion. For unconfirmed Employees, this Leave is without pay and may be granted in the case of death of spouse, death of biological mother/father, death of child, death of biological sibling, as well as other similar situations approved by Management. </li>
										<li>Sick Leave is at the discretion of Management and will be given by our doctors here at NIZAMIYE. </li>

									</ul>

									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">2. CONFLICT OF INTEREST POLICY</p>
									<p style="margin-left: 15px;margin-bottom: 0px;font-size:10px;">You will not engage in any other employment, consulting or other business activity (full-time or part- time) that would create a conflict of interest. By signing this Probational Agreement, you confirm that you have no contractual or other legal obligations that would prohibit you from performing your duties while in our employ.</p>


									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">3. DECLARATION OF INFORMATION</p>
									<p style="margin-left: 15px;margin-bottom: 0px;font-size:10px;">You shall be expected to disclose any information on clients, third party and staff members that may affect the organization’s business and its operation(s) in any manner as soon as it comes to your knowledge. Marketing</p>
									<!-- <div class="html2pdf__page-break"></div> -->
									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">4. NON-DISCLOSURE AGREEMENT</p>
									<p style="margin-left: 15px;margin-bottom: 0px;font-size:10px;">Employee shall always maintain the highest degree of confidentiality and keep as confidential the records, documents, third-party proprietary and other Confidential Information relating to the business of the organization which may be entrusted to you or that you may be privy to by virtue of your job role and you will use such only in a duly authorized manner in the interest of the organization. As an Employee, you must safeguard and not disclose Confidential Information will survive the expiration or termination of this Agreement and/or your employment with the organization.</p>

									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">5. RENUMERATION </p>
									<p style="margin-left: 15px;margin-bottom: 0px;font-size:10px;">The Employer shall pay the employee N<?= number_format($fin_details['NetPay'] / 12, 2) ?> (net of all deductions) per month. The Employee is not entitled to loans, allowances or advances of any kind during probation.</p>
									<!-- <div class="html2pdf__page-break"></div> -->
									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">6. PENSION </p>
									<p style="margin-left: 15px;margin-bottom: 0px;font-size:10px;">The employee pension contributions will be calculated and remitted after expiration of the probationary period; however, the employee should note that no cash shall be given to Employee in respect of pension remittance.</p>
									<p style="font-weight: 600;font-size:12px;margin-left: 15px;margin-bottom: 0px;font-size:10px;">7. TERMINATION CONDITIONS </p>
									<p style="margin-bottom: 0px;margin-left: 20px;font-size:10px;">I <span style="margin-left: 10px;"> During your probational period, you will be governed by the Condition of Service and all other policies including confidentiality which continues whether or not you are confirmed at the end of your probation.</p>
									<p style="margin-bottom: 0px;margin-left: 20px;font-size:10px;">II <span style="margin-left: 10px;"> The Employer reserves the right to summarily dismiss any employee for any misconduct (incompetence, disobedience, negligence, criminal offence etc.) without notice and without payment in lieu of notice. Kindly refer to Employers Disciplinary Code for details of misconduct.</p>
									<p style="margin-bottom: 0px;margin-left: 20px;font-size:10px;">III<span style="margin-left: 10px;"> This agreement may be terminated by the management with <span style="font-weight: 600;">one (1) days’</span> notice and the staff must give <span style="font-weight: 600;">seven (7) days’</span> notice before the planned date of resignation during the three (3) months probation.</p>

								</div>
								<!-- </div> -->

								<div class="row mt-4">
									<div class="col-sm-6 ml-4">
										<p style="font-size: small;">...............................................</p>
										<p style="font-weight: 600;font-size: small;">EMPLOYEE/DATE</p>
									</div>
									<div class="col-sm-4 ml-4 text-right">
										<p style="font-size: small;">...............................................</p>
										<p style="font-weight: 600;font-size: small;">MANAGEMENT/DATE</p>
									</div>
								</div>

								<div class="row d-flex justify-content-between px-5" style="margin-top: 50px;">

									<div class="" style="margin-left: 15px">
										<h6 style="font-weight: 600; color:rgb(60 111 184) !important">Hospital</h6>
										<div class="parent  d-flex flex-gap-3">
											<div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
											<div class="context">
												<img width="230px" src="../pfimg/hos.png" alt="">
											</div>

										</div>
									</div>
									<div class="" style="margin-right: 15px">
										<h6 style="font-weight: 600; color:rgb(60 111 184) !important">Branch</h6>
										<div class="parent  d-flex flex-gap-3">
											<div class="icon"><img width="20px" src="../pfimg/location.png" alt=""></div>
											<div class="context">
												<img width="230px" src="../pfimg/branch.png" alt="">
											</div>

										</div>
									</div>

								</div>




							</div>
						</div>
					</div>




					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->

				<button class="btn btn-sm btn-danger" id="download">Print</button>

			</div>
			<!-- /.col -->




		<?php endif; ?>

	</section>
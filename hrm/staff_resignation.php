<?php error_reporting(E_ERROR);
session_start();
include '../login/scriptrunner.php';
date_default_timezone_set('Africa/Lagos');
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = true;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("RESIGNATION LETTER" . "V") != true) {include '../main/NoAccess.php';exit;}

$Script_Menu = "Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
$ComLogo = ScriptRunner($Script_Menu, "SetValue");
$ComLogo = "../pfimg/" . $_SESSION["StkTck" . "CustID"] . "/" . $ComLogo;

// var_dump($allInfo);
$emp_details = null;
if (isset($_POST['bookselect1']) && !empty($_POST['bookselect1'])) {
    $emp = $_POST['bookselect1'];
    $Script_Menu = "Select * from [EmpTbl] where [HashKey]='{$emp}'";

    $emp_details = ScriptRunnercous($Script_Menu);
    $Script_Edit = "Select * from Settings where Setting='HRSignedDoc'";
    $EditID = ScriptRunner($Script_Edit, "SetValue17");
    $Script_MenuHr = "Select * from [EmpTbl] where [HashKey]='{$EditID}'";
    $emp_detailsHr = ScriptRunnercous($Script_MenuHr);
    // var_dump($emp_detailsHr['sig_img']);

    $Script_Edit_com = "SELECT * from [Settings] where Setting='CompName'";
    $comp_details = ScriptRunnercous($Script_Edit_com);
    $Script_Edit_email = "SELECT * from [Settings] where Setting='CompEmail'";
    $email_details = ScriptRunnercous($Script_Edit_email);
    $Script_Edit_addr = "SELECT * from [Settings] where Setting='CompAddress'";
    $addr_details = ScriptRunnercous($Script_Edit_addr);
    $Script_Edit_RC = "Select * from Settings where Setting='CompRC'";
    $rc_details = ScriptRunnercous($Script_Edit_RC);
    $branchId = $emp_details['BranchID'];
    $Script_Edit_Br = "Select * from [BrhMasters] where [HashKey]='{$branchId}'";
    $bran_details = ScriptRunnercous($Script_Edit_Br);

    $Script_Edit_Fin = "select * from [Fin_PRCore] where EmpID='{$emp}' and Status='A'";
    $fin_details = ScriptRunnercous($Script_Edit_Fin);
    $fin_hash = $fin_details['Scheme'];
    $Script_Edit_Fin_set = "select * from [Fin_PRSettings] where Hashkey='{$fin_hash}'";
    $fin_set_details = ScriptRunnercous($Script_Edit_Fin_set);
    $nm = null;
    for ($i = 1; $i <= 25; $i++) {
        if (trim(strtolower($fin_set_details["PayItemNm{$i}"])) === "nhf") {
            $nm = $i;
            break;
        }
    }

    // var_dump($bran_details);

    $sq = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') ORDER BY [ID] DESC ";

    $sqR_ = ScriptRunnercous($sq);

    $first_position_sql = "SELECT * FROM [EmpDtl] WHERE [StaffID] ='" . $emp_details['HashKey'] . "' and [Status] in('N','U') ORDER BY [ID] ASC ";

    $first_position_sqR_ = ScriptRunnercous($first_position_sql);


    // if (isset($sqR_['ItemExpiry']) && $sqR_['ItemExpiry']->format('d-m-Y') < date('d-m-Y')) {
    //     echo ("<script type='text/javascript'>{parent.msgbox(' {$emp_details['SName']} {$emp_details['FName']}  {$emp_details['ONames']} has exceeded the probation period and has been confirmed on {$sqR_['ItemExpiry']->format('d-m-Y')}.', 'red'); }</script>");

    // }

    // var_dump($sqR_);

}

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
		<script>
		$(function()
		{
		<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {} else {?>
		$("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+1D", dateFormat: 'dd M yy',yearRange: "-75:+75"})
		$("#EmpDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "+1M", dateFormat: 'dd M yy',yearRange: "-75:+75"})

		<?php }?>
		$("#DOB").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-80Y", maxDate: "<?php
$kk = ScriptRunner("Select * from Settings where Setting='EmpMinAge'", "SetValue");
if (number_format($kk, 0, '', '') == 0 || $kk == "") {echo "-18Y";} else {echo "-" . $kk . "Y";}?>
		", dateFormat: 'dd M yy', yearRange: "-75:+75"})
		});
		</script>
		<script>
		$(function() {$( "#tabs" ).tabs();});
		$(function() {
			$( document ).tooltip();
		});
		$(function() {
			$("#ClearDate").click(function() {
				document.getElementById("ExDt").value='';
			});
		});
		</script>
		<script>
		$(function() {
			$("#COO").change(function() {
				var Pt = $("#COO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#SOO").load("../main/getCh.php?Choice=State+Of+Origin&Parent="+Replmt);
			});
			$("#SOO").change(function() {
				var Pt = $("#SOO").val();
				var Replmt = Pt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				var Replmt = Replmt.replace(" ", "+");
				$("#LGA").load("../main/getCh.php?Choice=LGA&Parent="+Replmt);
			});
		});
		</script>
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

		<!-- Select 2-->
		<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../assets/css/master_style.css">
		<link rel="stylesheet" href="../assets/css/responsive.css">


		<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
		<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
		<style>

*{
	font-family: "Times New Roman", Times, serif !important;

	color: black !important;
}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 11px !important;
  /* font-family: "Times New Roman", Times, serif; */
}

body{
	font-size: 16px !important;
}
		.table>tbody>tr>td {
				padding-left: 7px !important;
			}

      .btn-group-sm > .btn, .btn-sm {
    font-size: 10px;
    padding: 5px;
    line-height: 20px;
}
.box-body ul li {
    line-height: 18px !important;
}
.card-img {
    width: 76%;
 }

 .nav-tabs-custom>.tab-content {
  color: black !important;
    text-align: justify;
}

.rectangle {
  border: 1px solid black ;
  /* padding: 10px; */
}

img {
  /* width: 100%;
  height: 160px;
  margin-left: 550px; */
}

.form-details {
		color:red !important; 
}

.form-title {
	font-weight: bold !important;
}

		</style>

	</head>


	<body>






            <!-- Main content -->
            <section class="content">

                <div class="box">
              <div class="box-header with-border">
                    <div class="row">

                              <div class="col-md-6  text-md-left text-center">
                                <h3>
                                  Generate Employee Certificate of Service
                                </h3>

                              </div>
                             <div class="col-md-6">
                             	 <form id="theform1"  method="post">
                              <div class="row">

                             <div class="col-4" ></div>
                             <!--  <div class="col-1" style="margin-top:2%;" >

                                   <a href="" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                              </div> -->
                               <!-- <div class="col-1" style="margin-top:2%;" >



                                   <a href=""  class="btn btn-default btn-sm"  ><i class="fa  fa-address-card"></i></a>
                              </div> -->
                                 <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%">
                                   <select class="form-control " name="bookselect1" id="bookselect1" >
                                                <!-- <select name="level1" id="level1"  class="form-control" > -->
                                          <option value="">--</option>

                                   <?php
$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
if ($conn2 === false) {
    echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");

}
$dbOpen4 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Resigned' order by SName Asc");

$result2 = sqlsrv_query($conn2, $dbOpen4);
if ($result2 === false) {
    die(print_r(sqlsrv_errors(), true));
}
$emp_hash = '';
if (!is_null($emp_details) && !empty($emp_details)) {
    $emp_hash = $emp_details['HashKey'];
}
while ($row3 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)): ?>
                                      <option value="<?php echo $row3['HashKey']; ?>"  <?php echo $emp_hash === $row3['HashKey'] ? "selected" : ''; ?> >
                                          <?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " " . $row3['EmpID'] ?>
                                    </option>
                                <?php endwhile;
sqlsrv_free_stmt($result2);

?>



                                      </select>
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger"   id="getall1">Open</button>
                                          </span>
                                    </div>
                              </div>

								</form>

                             </div>
                            </div>
              </div>
              <div class="box-body">

			  <?php //if (!is_null($emp_details) && !empty($emp_details) && (isset($sqR_['ItemExpiry']) && $sqR_['ItemExpiry']->format('d-m-Y') > date('d-m-Y'))): ?>
			  <?php if (!is_null($emp_details) && !empty($emp_details)): ?>
                  <div class="row">


                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-12">
                                  <!-- <div class="nav-tabs-custom" style="min-height: 400px"> -->
                                      <!-- <ul class="nav nav-tabs">
                                          <li>
                                              <a class="active" href="#book" data-toggle="tab">
                                                   NON-ACADEMIC STAFF
                                              </a>
                                          </li>

										  <li>
                                              <a href="#book2" data-toggle="tab">
                                                    ACADEMIC STAFF
                                              </a>
                                          </li>



                                      </ul> -->
                                      <!-- <div class="tab-content">

                                          <div class="tab-pane active " id="book"> -->
										  <div id="invoice" class="text-dark ">
                                               <div class="row">
                                                    <div class="col-sm-10  mx-auto">
                                                        <div class="row text-center">
                                                        <div class="col-sm-2">
                                                            	<!-- <img src="<?=$ComLogo?>" alt="" width="100" > -->
                                                        </div>

                                                        </div>
                                                    </div>
                                               </div>
											   <!-- <div class="row justify-content-center mt-1 "> -->
											  <!-- <div style="text-align: center;"> -->
											  <p style="margin-bottom: 30px;margin-left: 25px; text-align: end; margin-top: 150px;"><?php echo date("F d, Y") ?></p>
											  <?php

$sql = "SELECT * FROM Settings where Setting = 'CertificateOfService'";
$result = sqlsrv_query($conn2, $sql);

$row = sqlsrv_fetch_array($result);

$old_date = $emp_details['UpdatedDate'];
$new_date = date_format($old_date, "F d, Y");

$cktext = $row['SetValue20'];

// $cktext = str_replace('#Gross#', number_format($fin_details['Gross'] / 12, 2), $cktext);
// $cktext = str_replace('#PayItem#', number_format($fin_details["PayItem{$nm}"] / 12, 2), $cktext);
// $cktext = str_replace('#PensionEmployee#', number_format($fin_details['PensionEmployee'] / 12, 2), $cktext);
// $cktext = str_replace('#PAYE#', number_format($fin_details['PAYE'] / 12, 2), $cktext);
// $cktext = str_replace('#NetPay#', number_format($fin_details['NetPay'] / 12, 2), $cktext);

$cktext = str_replace('#Name#', ucwords(strtolower($emp_details['FName']." ".$emp_details['SName'] ." ".$emp_details['ONames'])), $cktext);
$cktext = str_replace('#StaffID#', $emp_details['EmpID'] , $cktext);
$cktext = str_replace('#DateOfEngagement#', ($emp_details['EmpDt']->format('F d, Y')) , $cktext);
$cktext = str_replace('#FirstPositionHeld#', $first_position_sqR_['ItemPos'], $cktext);
$cktext = str_replace('#DateOfDisengagement#', $new_date , $cktext);
$cktext = str_replace('#ReasonsForLeaving#', $emp_details['EmpStatus'], $cktext);
$cktext = str_replace('#PositionHeld#', $sqR_['ItemPos'], $cktext);

echo $cktext;

?>
												 <!-- <h3 style="font-weight: 600; margin-bottom: 0px !important;">CERTIFICATE OF SERVICE</h3>
												<p style="margin-bottom: 30px; color: red !important;"><?php //echo ucwords(strtolower("{$emp_details['FName']} {$emp_details['SName']} {$emp_details['ONames']}")) ?> </p>
												<p style="margin-bottom: 0px !important;">With Staff ID</p>
												<p style="color: red !important;"><?php //echo "{$emp_details['EmpID']}" ?> </p>

												<h3 style="font-weight: 600; margin-bottom: 0px !important;">TO WHOM IT MAY CONCERN</h3>
												<p>The above-mentioned name was employed by NTIC as follows:</p>

												<h3 style="font-weight: 600; margin-bottom: 0px !important;">DATE OF ENGAGEMENT:</h3>
												<p style="color: red !important;"><?php //echo "{$emp_details['EmpDt']->format('F d, Y')}" ?></p>

												<h3 style="font-weight: 600; margin-bottom: 0px !important;">DATE OF DISENGAGEMENT:</h3>
												<p style="color: red !important;">
												<?php //$old_date = $emp_details['UpdatedDate'];
												$new_date //= //date_format($old_date, "F d, Y");
												//echo $new_date; ?>	
												</p>

												<h3 style="font-weight: 600; margin-bottom: 0px !important;">REASON FOR LEAVING:</h3>
												<p style="color: red !important;"><?php //echo "{$emp_details['EmpStatus']}" ?></p>

												<h3 style="font-weight: 600; margin-bottom: 0px !important;">LAST POSITION HELD:</h3>
												<p style="color: red !important;margin-bottom: 30px;"><?php //echo "{$sqR_['ItemPos']}" ?></p>

											  </div>
											  <p style="margin-bottom: 30px;">This is the only Certificate issued by NTIC.</p>
											  <p>HR Department</p>

                        </div>
 -->
									 			

                                  <!-- </div> -->
                                  <!-- /.col -->



                                              <!-- /.col -->

								  <!-- <div class="tab-content"> -->










                              </div>
							  <button class="btn btn-sm btn-danger" id="download" >Download</button>
                          </div>













                      </div>











                  </div>

<?php endif;?>






















              </div>
          </div>
          <!-- /.box-body -->




        </div>

















        <!-- /.row -->
        </section>
        <!-- /.content -->












		<!-- Bootstrap 4.0-->
		<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
				<script src="html2pdf/dist/html2pdf.bundle.js"></script>


         <script>

            window.onload =function(){
                document.getElementById("download").addEventListener("click",()=>{
                    // const invoice = document.getElementById("invoice");
					// const opt = {
					// 	margin:       [.5,.5,.5,.5],
					// 	 image:        { type: 'jpeg', quality: 1 },
                    //     jsPDF:        { unit: 'cm', orientation: 'p' }

                        const invoice = document.getElementById("invoice");
					const opt = {
						html2canvas: {scale: 4},
						filename:     '<?=ucwords("{$emp_details['SName']} {$emp_details['FName']} {$emp_details['ONames']}_CERTIFICATE OF SERVICE ") . date("Y-m-d H:i:s")?>',
						margin:       [.90,.75,.75,.75],
						 image:        { type: 'jpeg', quality: 1 },
                        jsPDF:        { unit: 'cm', orientation: 'p' }
                    };

                    html2pdf().from(invoice).set(opt).save();
                })



            }



         </script>

		<script type="text/javascript">






		 function getDetail(a,event){

		 	 event.preventDefault();

       let curren = $(a).closest("tr").attr("id");
       let nexxt= $(a).closest("tr").next().attr("id");
      let prevv=  $(a).closest("tr").prev().attr("id");

      $("#next"+curren).val(nexxt);
      $("#prev"+curren).val(prevv);

      // console.log(curren,nexxt,prevv);

      $("#form"+curren).submit();


      }


		</script>

		<!-- SlimScroll -->
		<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>



		<!-- FastClick -->
		<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

		<!-- MinimalLite Admin App -->
		<script src="../assets/js/template.js"></script>


			<!-- MinimalLite Admin App -->
 <script src="../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <!-- MinimalLite Admin for Data Table -->
  <!-- <script src="../assets/js/pages/data-table.js"></script> -->


		 <script>



	$('#example1').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example6').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

     $('#example7').DataTable({
    	'lengthChange': false,
    	'paging'      : false,
    });

setInterval(function () {
    $( ".tablesorter-filter-row" ).remove();
}, 2000);
		function amp(id){


			var val= $('#'+id).val();
			// console.log(val);

			if (val=='') {
				$('#com'+id).prop('disabled', true);
				window['assignedId']=val;

				 { parent.msgbox('Please select an item', 'green'); };

			}else{
				$('#com'+id).prop('disabled', false);
				window['assignedId']=val;
			}

			}
		 function confirm(id,email){

			 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you confirm this request</strong>?</h5></div>')
			        .dialog({
			          modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {
								let name='<?=$employee_name?>';

			             	 // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

			             	  $('<div></div>').appendTo('body')
			             	 		        .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

			             	 	modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
			             	   	height: 200 + 6,
			             	   	width: 500 + 20,
			             	 	buttons: {

			             	 		Send: function () {

			             	 		var msgC=document.getElementById("sd").value;



			             	 		if (msgC=='') {

			             	 			 { parent.msgbox('Enter custom message', 'red'); };

			             	 			 return;

			             	 		}

			             	 			  $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"confirmed", val:assignedId, msgC:msgC},
	           				                         success:function(response){

	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });




			             	 		  $(this).dialog("close");

			             	 		}
			             	 	}

			             	 	});





			              $(this).dialog("close");
			            },
			            No: function () {

			            		$('<div></div>').appendTo('body')
			            		       .html('<div><class=TextBoxText> Are you sure you want to confirm the selected booking record?</h5></div>')
			            		       .dialog({
			            		         modal: true, title: "Confirm Custom Message", zIndex: 10000, autoOpen: true,
			            		         width: 'auto', resizable: true,
			            		         height: 'auto', resizable: true,
			            		         buttons: {
			            		           Yes: function () {


			            		                       	let name='<?=$employee_name?>';
			            		           $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"confirmed", val:assignedId},
	           				                         success:function(response){
	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed,a confirmation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });






			            		             $(this).dialog("close");
			            		           },
			            		           No: function () {

			            		             $(this).dialog("close");
			            		           }
			            		         },
			            		         close: function (event, ui) {
			            		           $(this).remove();
			            		         }
			            		     });





			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });
		 }

		 function decline(id, email){

		 			 $('<div></div>').appendTo('body')
			        .html('<div><class=TextBoxText> Do you wish to <strong>add a custom message before you decline this request</strong>?</h5></div>')
			        .dialog({
			          modal: true, title: "Delicine Custom Message", zIndex: 10000, autoOpen: true,
			          width: 'auto', resizable: true,
			          height: 'auto', resizable: true,
			          buttons: {
			            Yes: function () {
								let name='<?=$employee_name?>';

			             	 // parent.ShowDisp11('Add&nbsp;to&nbsp;Masters','bkm/custom.php?type=confirmed&id='+id+'&email='+email+'&name='+name+'&val='+assignedId+'',300,500,'No');

			             	  $('<div></div>').appendTo('body')
			             	 		        .html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

			             	 	modal: true,title: "Enter Custom Message", zIndex: 10000, autoOpen: true,
			             	   	height: 200 + 6,
			             	   	width: 500 + 20,
			             	 	buttons: {

			             	 		Send: function () {

			             	 		var msgC=document.getElementById("sd").value;



			             	 		if (msgC=='') {

			             	 			 { parent.msgbox('Enter custom message', 'red'); };

			             	 			 return;

			             	 		}

			             	 			  $.ajax({
	           				                         url:"add_bookitem.php",
	           				                         method:"POST",
	           				                         data:{id:id, email:email,name:name,type:"declined", val:assignedId, msgC:msgC},
	           				                         success:function(response){

	           				                         if(response ==='OK'){

	           				                           // console.log(allInfo);
	           				                            { parent.msgbox('Request confirmed, cancellation email has been sent to '+name , 'green'); };
	           				                            location.reload();




	           				                         }else if(response==="Wrong"){

	           				                             { parent.msgbox('Something went wrong', 'red'); };

	           				                         }
	           				                       },
	           				                       error:function(){
	           				                           alert('Something went wrong');
	           				                       }
	           				                    });




			             	 		  $(this).dialog("close");

			             	 		}
			             	 	}

			             	 	});





			              $(this).dialog("close");
			            },
			            No: function () {

						    $('<div></div>').appendTo('body')
						        .html('<div><class=TextBoxText>Do you wish to <strong> Decline the booking request</strong> of the selected record?</h5></div>')
						        .dialog({
						          modal: true, title: "Delete Booking Items", zIndex: 10000, autoOpen: true,
						          width: 'auto', resizable: true,
						          height: 'auto', resizable: true,
						          buttons: {
						            Yes: function () {

						            	let name='<?=$employee_name?>';



						            		$.ajax({
							                         url:"add_bookitem.php",
							                         method:"POST",
							                         data:{id:id, email:email, name:name, type:"declined"},
							                         success:function(response){
							                         if(response ==='OK'){

							                           // console.log(allInfo);
							                            { parent.msgbox('Request confirmed, cancellation email has been sent to '+name , 'green'); };
							                            location.reload();




							                         }else if(response==="Wrong"){

							                             { parent.msgbox('Something went wrong', 'red'); };

							                         }
							                       },
							                       error:function(){
							                           alert('Something went wrong');
							                       }
							                    });



						              $(this).dialog("close");
						            },
						            No: function () {
						              $(this).dialog("close");
						            }
						          },
						          close: function (event, ui) {
						            $(this).remove();
						          }
						      });





			              $(this).dialog("close");
			            }
			          },
			          close: function (event, ui) {
			            $(this).remove();
			          }
			      });













































		 }

        $("#getall1").click(function(){

         let bookSelected = $('#bookselect1').val();


           if (bookSelected === "") {



             { parent.msgbox('Please select an employee', 'red'); };

             return;

           }else{

            $('#theform1').submit();






           }



           });



    </script>
	</body>

	</html>
USE [SSLCloud_0009]
GO

/****** Object:  Table [dbo].[formrequest]    Script Date: 10/14/2021 3:43:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[formrequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emp_hash] [varchar](50) NOT NULL,
	[request_name] [varchar](max) NOT NULL,
	[request] [text] NULL,
	[level1] [varchar](50) NOT NULL,
	[level1_status] [varchar](50) NULL,
	[level2] [varchar](50) NULL,
	[level2_status] [varchar](50) NULL,
	[level3] [varchar](50) NULL,
	[level3_status] [varchar](50) NULL,
	[request_date] [datetime] NOT NULL,
	[corres] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
error_reporting(E_ERROR | E_PARSE);

//if (ValidateURths("CREATE APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
if (ValidateURths("APPRAISAL SETTING" . "V") != true) {include '../main/NoAccess.php';exit;}

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($SelID)) {$SelID = "";}
//if (!isset($back_button)){$back_button="";}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Appraisal") {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        include 'ApprSetup_validate_6level_checks.php';

        /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff" . $_SESSION["StkTck" . "UName"];
        $HashKey = md5($UniqueKey);

        if ($GoValidate == true) {
            $curdate_end = date('Y-m-d');

            $Script90 = "SELECT CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),SDate,106) SDt FROM KPIStart WHERE Status = 'A' ORDER BY SDate DESC";
            $EDate_value90 = ScriptRunner($Script90, "EDt");
            $SDate_value90 = ScriptRunner($Script90, "SDt");

            ///*
            if (!$EDate_value90) {
                echo ("<script type='text/javascript'>{parent.msgbox('Sorry, you have to setup Start/End Appraisal and approved it.', 'red');}</script>");
                $GoValidate = false;
                exit('Sorry, you have to setup Start/End Appraisal and approve it. <a href="StartAppr.php?PgTy=\"\"">Return</a>');
            }
            //*/
            $Script95 = "SELECT CONVERT(Varchar(11),EDate,106) EDt FROM KPISettingNew WHERE Status = 'A'";
            $EDate_value95 = ScriptRunner($Script95, "EDt");

            //20-7-2016                // e.g 27-5-2016

            /*
            if(isset($EDate_value90)&&(strtotime($EDate_value90) >= strtotime($curdate_end))){
            echo ("<script type='text/javascript'>{parent.msgbox('Sorry the date falls within another apprisal period currently running.', 'red');}</script>");    $GoValidate = false;
            exit('Sorry the date falls within another apprisal period curruently running. <a href="ApprSetup.php?PgTy=\"\"">Return</a>');
            }
             */

            $Script = "UPDATE KPISettingNew
							SET [Status] = 'U'";
            ScriptRunnerUD($Script, "Authorize");
            $data = [];
            if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                    if (!empty($_REQUEST["objectives"][$i])) {
                        $data[] = sanitize($_REQUEST["objectives"][$i]);
                    }
                }
                if (count($data)) {
                    $objectives = json_encode($data);
                } else {
                    $objectives = null;
                }
            } else {
                $objectives = null;
            }

            $ratings = [];
            if (isset($_REQUEST["ratings"]) && !empty($_REQUEST["ratings"])) {
                for ($i = 0; $i < count($_REQUEST["ratings"]); $i++) {
                    if (!empty($_REQUEST["ratings"][$i])) {
                        $ratings[] = sanitize($_REQUEST["ratings"][$i]);
                    }
                }
            }

            $comments = [];
            if (isset($_REQUEST["comments"]) && !empty($_REQUEST["comments"])) {
                for ($i = 0; $i < count($_REQUEST["comments"]); $i++) {
                    if (!empty($_REQUEST["comments"][$i])) {
                        $comments[] = sanitize($_REQUEST["comments"][$i]);
                    }
                }
            }
            $managerAssessment = json_encode(['ratings' => $ratings, 'comments' => $comments]);
            $Script = "INSERT INTO [KPISettingNew] (
				[AID],
				[Status],
				[AddedBy],
				[AddedDate],
				[HashKey],
				[MDate],
				[ACount],
				[EDate],
				[SDate],
				[Accomplishments],
				[allow_emp_mgr_1],
				[show_objectives],
	      [Appr_allow_emp]
	      ,[Appr_allow_supervisor]
	      ,[Appr1_emp]
	      ,[Appr1_deliv]
	      ,[Appr1_comment]
	      ,[Appr2_emp]
	      ,[Appr2_deliv]
	      ,[Appr2_comment]
	      ,[Appr3_emp]
	      ,[Appr3_deliv]
	      ,[Appr3_comment]
	      ,[Appr4_emp]
	      ,[Appr4_deliv]
	      ,[Appr4_comment]
	      ,[Appr5_emp]
	      ,[Appr5_deliv]
	      ,[Appr5_comment]
	      ,[Appr6_emp]
	      ,[Appr6_deliv]
	      ,[Appr6_comment]
	      ,[Appr7_emp]
	      ,[Appr7_deliv]
	      ,[Appr7_comment]
	      ,[Appr8_emp]
	      ,[Appr8_deliv]
	      ,[Appr8_comment]

		  ,[Appr1_section_comment]
		  ,[Appr2_section_comment]
		  ,[Appr3_section_comment]
		  ,[Appr4_section_comment]
		  ,[Appr5_section_comment]
		  ,[Appr6_section_comment]

		  ,[final_score_setup]
	      ,[goal_obj]

		  ,[goal_desc]
		  ,[goal_period_from_month]
		  ,[goal_period_from_year]
		  ,[goal_period_to_month]
		  ,[goal_period_to_year]
		  ,[goal_header1]
		  ,[goal_header2]
		  ,[goal_header3]
		  ,[goal_header4]
		  ,[goal_num_row_allow]

		  ,[num_appraisers]
		  ,[enforce_comment]
		  ,[enforce_comment_mgr]
		  ,[enforce_summ_comm_mgr]
		  ,[enforce_final]
		  ,[head_hr]
		  ,[num_of_appraisals]
		  ,[appr_increments]
		  ,[manager_assessment]
		  ,[objectives])
		  VALUES ('" . ECh($_REQUEST["AID"]) . "','A','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "','" . ECh($_REQUEST["MDate"]) . "','" . ECh($_REQUEST["ACount"]) . "','" . ECh($EDate_value90) . "',
		  '" . ECh($SDate_value90) . "',

		  '" . ECh($_REQUEST["accomplishments"]) . "',
		  '" . ECh($_REQUEST["allow_emp_mgr_1"]) . "',
		  '" . ECh($_REQUEST["show_objectives"]) . "',
		  '" . ECh($_REQUEST["allow_appraisee"]) . "',
		  '" . ECh($_REQUEST["allow_supervisor"]) . "',

		  '" . ECh($_REQUEST["appr_emp"]) . "',
		  '" . ECh($_REQUEST["appr_deliv"]) . "',
		  '" . ECh($_REQUEST["appr_comm"]) . "',

		  '" . ECh($_REQUEST["appr_emp2"]) . "',
		  '" . ECh($_REQUEST["appr_deliv2"]) . "',
		  '" . ECh($_REQUEST["appr_comm2"]) . "',

		  '" . ECh($_REQUEST["appr_emp3"]) . "',
		  '" . ECh($_REQUEST["appr_deliv3"]) . "',
		  '" . ECh($_REQUEST["appr_comm3"]) . "',

		  '" . ECh($_REQUEST["appr_emp4"]) . "',
		  '" . ECh($_REQUEST["appr_deliv4"]) . "',
		  '" . ECh($_REQUEST["appr_comm4"]) . "',

		  '" . ECh($_REQUEST["appr_emp5"]) . "',
		  '" . ECh($_REQUEST["appr_deliv5"]) . "',
		  '" . ECh($_REQUEST["appr_comm5"]) . "',

		  '" . ECh($_REQUEST["appr_emp6"]) . "',
		  '" . ECh($_REQUEST["appr_deliv6"]) . "',
		  '" . ECh($_REQUEST["appr_comm6"]) . "',

		  '" . ECh($_REQUEST["appr_emp7"]) . "',
		  '" . ECh($_REQUEST["appr_deliv7"]) . "',
		  '" . ECh($_REQUEST["appr_comm7"]) . "',

		  '" . ECh($_REQUEST["appr_emp8"]) . "',
		  '" . ECh($_REQUEST["appr_deliv8"]) . "',
		  '" . ECh($_REQUEST["appr_comm8"]) . "',

		  '" . ECh($_REQUEST["appr_section1"]) . "',
		  '" . ECh($_REQUEST["appr_section2"]) . "',
		  '" . ECh($_REQUEST["appr_section3"]) . "',
		  '" . ECh($_REQUEST["appr_section4"]) . "',
		  '" . ECh($_REQUEST["appr_section5"]) . "',
		  '" . ECh($_REQUEST["appr_section6"]) . "',

		  '" . ECh($_REQUEST["final_score_setup"]) . "',
		  '" . ECh($_REQUEST["goal_obj"]) . "',

		  '" . ECh($_REQUEST["g_desc"]) . "',
		  '" . ECh($_REQUEST["g_formMonth"]) . "',
		  '" . ECh($_REQUEST["g_from_year"]) . "',
		  '" . ECh($_REQUEST["g_toMonth"]) . "',
		  '" . ECh($_REQUEST["g_to_year"]) . "',
		  '" . ECh($_REQUEST["g_header1"]) . "',
		  '" . ECh($_REQUEST["g_header2"]) . "',
		  '" . ECh($_REQUEST["g_header3"]) . "',
		  '" . ECh($_REQUEST["g_header4"]) . "',
		  '" . ECh($_REQUEST["g_no_goals_row"]) . "',

		  '" . ECh($_REQUEST["num_appraisers"]) . "',
		  '" . ECh($_REQUEST["enforce_comm"]) . "',
		  '" . ECh($_REQUEST["enforce_comm_mgr"]) . "',
		  '" . ECh($_REQUEST["enforce_summ_comm_mgr"]) . "',
		  '" . ECh($_REQUEST["enforce_final"]) . "',
		  '" . ECh($_REQUEST["head_hr"]) . "',
		  '" . ECh($_REQUEST["num_of_appraisals"]) . "',
		  '" . ECh($_REQUEST["appr_increments"]) . "',
		  '" . $managerAssessment . "',
		  '" . $objectives . "')";
            //        echo $Script;
            ScriptRunnerUD($Script, "Inst");

            //Appraisal Audit Trail
            $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
            $AprsNm = ScriptRunner($Script99, "AprsNm");

            AuditLog("INSERT", "New appraisal Settings created by" . $AprsNm);

            include 'mail_trail_send_kpi_aprs_setting_update.php';

            /*
            if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
            {
            MailTrail("CREATE APPRAISAL","T",'','','',''); //Sends a mail notifications
            }
             */

            echo ("<script type='text/javascript'>{ parent.msgbox('New organisational appraisal created successfully.', 'green'); }</script>");
        }
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='--'";
    }
    //**************************************************************************************************
    //**************************************************************************************************
    elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Appraisal" && isset($_REQUEST["UpdID"]) && $_REQUEST["UpdID"] != "") {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set HashKey of Account to be Updated */
        $EditID = ECh($_REQUEST["UpdID"]);
        $HashKey = ECh($_REQUEST["UpdID"]);

        include 'ApprSetup_validate_6level_checks.php';
        $data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $data[] = sanitize($_REQUEST["objectives"][$i]);
                }
            }
            if (count($data)) {
                $objectives = json_encode($data);
            } else {
                $objectives = null;
            }
        } else {
            $objectives = null;
        }

        $ratings = [];
        if (isset($_REQUEST["ratings"]) && !empty($_REQUEST["ratings"])) {
            for ($i = 0; $i < count($_REQUEST["ratings"]); $i++) {
                if (!empty($_REQUEST["ratings"][$i])) {
                    $ratings[] = sanitize($_REQUEST["ratings"][$i]);
                }
            }
        }

        $comments = [];
        if (isset($_REQUEST["comments"]) && !empty($_REQUEST["comments"])) {
            for ($i = 0; $i < count($_REQUEST["comments"]); $i++) {
                if (!empty($_REQUEST["comments"][$i])) {
                    $comments[] = sanitize($_REQUEST["comments"][$i]);
                }
            }
        }
        $managerAssessment = json_encode(['ratings' => $ratings, 'comments' => $comments]);

        /*Check validation for updating an account */
        if (strlen($EditID) == 32) {

            $Script = "UPDATE KPISettingNew SET
								[Accomplishments]='" . ECh($_REQUEST["accomplishments"]) . "',
								[show_objectives]='" . ECh($_REQUEST["show_objectives"]) . "',
								[objectives]='" . $objectives . "',
								[manager_assessment]='" . $managerAssessment . "',
								[head_hr]='" . ECh($_REQUEST["head_hr"]) . "',
								[num_of_appraisals]='" . ECh($_REQUEST["num_of_appraisals"]) . "',
								[appr_increments]='" . ECh($_REQUEST["appr_increments"]) . "',
								[Appr_allow_emp]='" . ECh($_REQUEST["allow_appraisee"]) . "',
								[Appr_allow_supervisor]='" . ECh($_REQUEST["allow_supervisor"]) . "',
								[allow_emp_mgr_1]='" . ECh($_REQUEST["allow_emp_mgr_1"]) . "',

								  [Appr1_emp]='" . ECh($_REQUEST["appr_emp"]) . "',
								  [Appr1_deliv]='" . ECh($_REQUEST["appr_deliv"]) . "',
								  [Appr1_comment]='" . ECh($_REQUEST["appr_comm"]) . "',

								  [Appr2_emp]='" . ECh($_REQUEST["appr_emp2"]) . "',
								  [Appr2_deliv]='" . ECh($_REQUEST["appr_deliv2"]) . "',
								  [Appr2_comment]='" . ECh($_REQUEST["appr_comm2"]) . "',

								  [Appr3_emp]='" . ECh($_REQUEST["appr_emp3"]) . "',
								  [Appr3_deliv]='" . ECh($_REQUEST["appr_deliv3"]) . "',
								  [Appr3_comment]='" . ECh($_REQUEST["appr_comm3"]) . "',

								  [Appr4_emp]='" . ECh($_REQUEST["appr_emp4"]) . "',
								  [Appr4_deliv]='" . ECh($_REQUEST["appr_deliv4"]) . "',
								  [Appr4_comment]='" . ECh($_REQUEST["appr_comm4"]) . "',

								  [Appr5_emp]='" . ECh($_REQUEST["appr_emp5"]) . "',
								  [Appr5_deliv]='" . ECh($_REQUEST["appr_deliv5"]) . "',
								  [Appr5_comment]='" . ECh($_REQUEST["appr_comm5"]) . "',

								  [Appr6_emp]='" . ECh($_REQUEST["appr_emp6"]) . "',
								  [Appr6_deliv]='" . ECh($_REQUEST["appr_deliv6"]) . "',
								  [Appr6_comment]='" . ECh($_REQUEST["appr_comm6"]) . "',

								  [Appr1_section_comment]='" . ECh($_REQUEST["appr_section1"]) . "',
								  [Appr2_section_comment]='" . ECh($_REQUEST["appr_section2"]) . "',
								  [Appr3_section_comment]='" . ECh($_REQUEST["appr_section3"]) . "',
								  [Appr4_section_comment]='" . ECh($_REQUEST["appr_section4"]) . "',
								  [Appr5_section_comment]='" . ECh($_REQUEST["appr_section5"]) . "',
								  [Appr6_section_comment]='" . ECh($_REQUEST["appr_section6"]) . "',

								  [final_score_setup]='" . ECh($_REQUEST["final_score_setup"]) . "',
								  [goal_obj]='" . ECh($_REQUEST["goal_obj"]) . "',

								  [goal_desc]='" . ECh($_REQUEST["g_desc"]) . "',
								  [goal_period_from_month]='" . ECh($_REQUEST["g_formMonth"]) . "',
								  [goal_period_from_year]='" . ECh($_REQUEST["g_from_year"]) . "',
								  [goal_period_to_month]='" . ECh($_REQUEST["g_toMonth"]) . "',
								  [goal_period_to_year]='" . ECh($_REQUEST["g_to_year"]) . "',
								  [goal_header1]='" . ECh($_REQUEST["g_header1"]) . "',
								  [goal_header2]='" . ECh($_REQUEST["g_header2"]) . "',
								  [goal_header3]='" . ECh($_REQUEST["g_header3"]) . "',
								  [goal_header4]='" . ECh($_REQUEST["g_header4"]) . "',
								  [goal_num_row_allow]='" . ECh($_REQUEST["g_no_goals_row"]) . "',

						[num_appraisers]='" . ECh($_REQUEST["num_of_appraised"]) . "',
						[enforce_comment]='" . ECh($_REQUEST["enforce_comm"]) . "',
						[enforce_comment_mgr]='" . ECh($_REQUEST["enforce_comm_mgr"]) . "',
						[enforce_summ_comm_mgr]='" . ECh($_REQUEST["enforce_summ_comm_mgr"]) . "',
						[enforce_final]='" . ECh($_REQUEST["enforce_final"]) . "' WHERE HashKey='$EditID'";
            ScriptRunnerUD($Script, "Inst");

            echo ("<script type='text/javascript'>{ parent.msgbox('Organisational appraisal updated successfully.', 'green'); }</script>");
            $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where HashKey='--'";

            include 'mail_trail_send_kpi_aprs_setting_update.php';

            //Appraisal Audit Trail
            $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
            $AprsNm = ScriptRunner($Script99, "AprsNm");
            AuditLog("INSERT", "Appraisal Settings updated by " . $AprsNm);

        } else {
            $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='" . $EditID . "'";
            include '../main/prmt_blank.php';
        }

    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0) {
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) {
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='" . $EditID . "'";
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "T") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from KPISettingNew WHERE [HashKey] = '" . $EditID . "'") == true) {
                    $Script = "Select DATEDIFF(D,Getdate(),EDate) Dt from KPISettingNew where [HashKey] = '" . $EditID . "'";
                    if (ScriptRunner($Script, "Dt") > 0) {
                        //Mark the record as Authorized
                        $Script = "UPDATE KPISettingNew
							SET [Status] = 'A'
							,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
							,[AuthDate] = GetDate()
							WHERE [HashKey] = '" . $EditID . "'";
                        ScriptRunnerUD($Script, "Authorize");
                        AuditLog("AUTHORIZE", "Appraisal record authorized");

                        echo ("<script type='text/javascript'>{ parent.msgbox('Selected appraisal records(s) authorized successfully.','green'); }</script>");
                    } else {
                        echo ("<script type='text/javascript'>{ parent.msgbox('Unable to authorized past appraisal.','red'); }</script>");
                    }
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='" . $EditID . "'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "T") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [KPISettingNew] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                /*                if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".ECh($EditID)."'") != true)
                {echo "Here";
                exit;
                 */
                $Script = "Select DATEDIFF(D,Getdate(),EDate) Dt from KPISettingNew where [HashKey] = '" . $EditID . "'";
                if (ScriptRunner($Script, "Dt") >= 0) {
                    //Mark the record as Authorized
                    $Script = "UPDATE KPISettingNew
							SET [Status] = 'U'
							,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
							,[AuthDate] = GetDate()
							WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("UNAUTHORIZE", "Appraisal record unauthorized");

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) unauthorized successfully', 'green');}</script>");
                    $Script_Edit = "select CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where HashKey='--'";
                }
                //                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where HashKey='" . $EditID . "'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "D") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='" . $EditID . "'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '" . $EditID . "'") == true) {
                    //Mark the record as Authorized

                    $Script = "UPDATE KPISettingNew
						SET [Status] = 'D'
						,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("DELETE", "Appraisal record deleted");

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) deleted successfully', 'green');}</script>");
                    $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where HashKey='" . $EditID . "'";
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPISettingNew] where HashKey='--'";

    }
}
?>
<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script>
$(function()
  {
	$("#SDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#EDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#MDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#RevSDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-5Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#RevEDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-5Y", maxDate: "+1Y", dateFormat: 'dd M yy'})

  });


$(function() {$( "#tabs" ).tabs();});
$(function() {    $( document ).tooltip();  });
</script>
<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<body>
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3>
					Appraisal Setting
					</h3>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-1">
				</div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6">
							<?php
print '<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
								<div class="form-group row ">
									<label  class="col-sm-5 col-form-label" style="padding-right: 0px;">Goals/Objective Form:</label>
									<div class="col-sm-4 input-group-sm">
										<select style="height: 25px" name="goal_obj" id="goal_obj" onChange="this.form.submit()">';
print '
												<option> </option>
												<option value="1">Show</option>
												<option value="2">Hide</option>
											';
print '
											</select>
									</div>
								</div>
								</form>';
?>
						</div>
						<div class="col-md-6 text-right">
							<button type="button" class="btn btn-danger" Title="Add a new department eg. HR, Financ" id="MastersDepartment" onClick="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters_appraisal_grade.php?GroupName=Appraisal Grade&LDb=Single&amp;Elmt=Appraisal Grade',400,500,'No')">Report Settings</button>
						</div>
					</div>
					<div class="row">
						<div class ="col-md-12">
							<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
								<?php
if ((isset($_REQUEST['hash'])) && ($_REQUEST['type'] == "view_modify")) {
    //Edit/View mode ... Update
    $Script_kpi_view = "SELECT  Convert(Varchar(11),AddedDate,106) as Dt, * FROM KPISettingNew WHERE HashKey='" . $_REQUEST['hash'] . "'";
    $goal_desc = ScriptRunner($Script_kpi_view, "goal_desc");
    $goal_period_from_month = ScriptRunner($Script_kpi_view, "goal_period_from_month");
    $goal_period_from_year = ScriptRunner($Script_kpi_view, "goal_period_from_year");
    $goal_period_to_month = ScriptRunner($Script_kpi_view, "goal_period_to_month");
    $goal_period_to_year = ScriptRunner($Script_kpi_view, "goal_period_to_year");
    $goal_header1 = ScriptRunner($Script_kpi_view, "goal_header1");
    $goal_header2 = ScriptRunner($Script_kpi_view, "goal_header2");
    $goal_header3 = ScriptRunner($Script_kpi_view, "goal_header3");
    $goal_header4 = ScriptRunner($Script_kpi_view, "goal_header4");
    $goal_num_row_allow = ScriptRunner($Script_kpi_view, "goal_num_row_allow");

    $apr_status_sel = ScriptRunner($Script_kpi_view, "Status");
    $enforce_comment_sel = ScriptRunner($Script_kpi_view, "enforce_comment");
    $enforce_comment_mgr = ScriptRunner($Script_kpi_view, "enforce_comment_mgr");
    $enforce_summ_comm_mgr = ScriptRunner($Script_kpi_view, "enforce_summ_comm_mgr");
    $enforce_final_sel = ScriptRunner($Script_kpi_view, "enforce_final");
    if ($goal_period_from_month != "") {
        print "<div class='col-md-12'><b>SHOWING: Goals/Objective Form</b></div>
											<input type=\"hidden\" name=\"is_posted\" value=\"$_REQUEST[goal_obj]\"  />

											<input type=\"hidden\" name=\"former_goal_desc\" value=\"$goal_desc\"  />
											<input type=\"hidden\" name=\"former_goal_period_from_month\" value=\"$goal_period_from_month\"  />
											<input type=\"hidden\" name=\"former_goal_period_from_year\" value=\"$goal_period_from_year\"  />
											<input type=\"hidden\" name=\"former_goal_period_to_month\" value=\"$goal_period_to_month\"  />
											<input type=\"hidden\" name=\"former_goal_period_to_year\" value=\"$goal_period_to_year\"  />
											<input type=\"hidden\" name=\"former_goal_header1\" value=\"$goal_header1\"  />
											<input type=\"hidden\" name=\"former_goal_header2\" value=\"$goal_header2\"  />
											<input type=\"hidden\" name=\"former_goal_header3\" value=\"$goal_header3\"  />
											<input type=\"hidden\" name=\"former_goal_header4\" value=\"$goal_header4\"  />
											<input type=\"hidden\" name=\"former_goal_num_row_allow\" value=\"$goal_num_row_allow\"  />
											<br/>
											";
    }
}
if (isset($_REQUEST['goal_obj']) && ($_REQUEST['goal_obj'] == "1")) { //||((isset($_REQUEST['hash']))&&($_REQUEST['type']=="view_modify"))){
    if (isset($_REQUEST['hash']) && ($_REQUEST['type'] == "view_modify") && ($goal_period_from_month != "")) {
        print ' <div class="col-md-12">
							                        <h5>Modify Goals/Objective Form</h5>
							                    </div>';
        include 'goal_obj_form_setup.php';
    } else {
        print ' <div class="col-md-12">
							                        <h5>Setup Goals/Objective Form</h5>
							                    </div>';
        include 'goal_obj_form_setup.php';
    }
} /*elseif((isset($goal_period_from_month))&&(!$_REQUEST['goal_obj'])){
print '<b>Modify Goals/Objective Form2</b>';
include('goal_obj_form_setup.php');
}*/elseif (isset($_REQUEST['goal_obj']) && ($_REQUEST['goal_obj'] == "2")) {
    //print "<b>HIDING: Goals/Objective Form</b>";
}
//print $goal_period_from_month;
?>
								<?php
/*
if(isset($_REQUEST['num_app'])){
print $_REQUEST['num_app']."<b> Levels' of Appraisers<input type=\"hidden\" name=\"num_appraisers\" value=\"$_REQUEST[num_app]\"> </b>";

}elseif(isset($_REQUEST['hash'])){
$SelHaskKey=ECh($_REQUEST['hash']);
$Script ="SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where Hashkey = '$SelHaskKey' ";
$num_appraisers_sel=ScriptRunner($Script, "num_appraisers");
$_REQUEST['num_app']=$num_appraisers_sel;

$apr_status_sel=ScriptRunner($Script, "Status");
$enforce_comment_sel=ScriptRunner($Script, "enforce_comment");
$enforce_final_sel=ScriptRunner($Script, "enforce_final");
$EditID=$SelHaskKey;
}
print '<input name="num_of_appraised" type="hidden" value="'.$_REQUEST['num_app'].'" />'
 */
?>
								<?php
if ((isset($_REQUEST['hash'])) && ($_REQUEST['type'] == "view_modify")) {
    //Edit/View mode ... Update
    $Script_kpi = "SELECT  Convert(Varchar(11),AddedDate,106) as Dt, * FROM KPISettingNew WHERE HashKey='" . $_REQUEST['hash'] . "'";
    $created_date = ScriptRunner($Script_kpi, "Dt");
    print "<div class='col-md-12' style='color:#0099CC'>Viewing Appraisal Setting Created on: <b>" . $created_date . "</b></div>  <br/><br/>";

    $accomplishments = ScriptRunner($Script_kpi, "Accomplishments");
    $show_objectives = ScriptRunner($Script_kpi, "show_objectives");
    $objectives = json_decode(ScriptRunner($Script_kpi, "objectives"), true);
    $allow_appraisee_sel = ScriptRunner($Script_kpi, "Appr_allow_emp");
    $allow_supervisor_sel = ScriptRunner($Script_kpi, "Appr_allow_supervisor");
    $allow_emp_mgr_1 = ScriptRunner($Script_kpi, "allow_emp_mgr_1");

    $appr1_emp = ScriptRunner($Script_kpi, "Appr1_emp");
    $appr1_deliv = ScriptRunner($Script_kpi, "Appr1_deliv");
    $appr1_comment = ScriptRunner($Script_kpi, "Appr1_comment");
    $Appr1_section_comment = ScriptRunner($Script_kpi, "Appr1_section_comment");

    $appr2_emp = ScriptRunner($Script_kpi, "Appr2_emp");
    $appr2_deliv = ScriptRunner($Script_kpi, "Appr2_deliv");
    $appr2_comment = ScriptRunner($Script_kpi, "Appr2_comment");
    $Appr2_section_comment = ScriptRunner($Script_kpi, "Appr2_section_comment");

    $appr3_emp = ScriptRunner($Script_kpi, "Appr3_emp");
    $appr3_deliv = ScriptRunner($Script_kpi, "Appr3_deliv");
    $appr3_comment = ScriptRunner($Script_kpi, "Appr3_comment");
    $Appr3_section_comment = ScriptRunner($Script_kpi, "Appr3_section_comment");

    $appr4_emp = ScriptRunner($Script_kpi, "Appr4_emp");
    $appr4_deliv = ScriptRunner($Script_kpi, "Appr4_deliv");
    $appr4_comment = ScriptRunner($Script_kpi, "Appr4_comment");
    $Appr4_section_comment = ScriptRunner($Script_kpi, "Appr4_section_comment");

    $appr5_emp = ScriptRunner($Script_kpi, "Appr5_emp");
    $appr5_deliv = ScriptRunner($Script_kpi, "Appr5_deliv");
    $appr5_comment = ScriptRunner($Script_kpi, "Appr5_comment");
    $Appr5_section_comment = ScriptRunner($Script_kpi, "Appr5_section_comment");

    $appr6_emp = ScriptRunner($Script_kpi, "Appr6_emp");
    $appr6_deliv = ScriptRunner($Script_kpi, "Appr6_deliv");
    $appr6_comment = ScriptRunner($Script_kpi, "Appr6_comment");
    $Appr6_section_comment = ScriptRunner($Script_kpi, "Appr6_section_comment");

    $sel_apr_status = ScriptRunner($Script_kpi, "Status");

    $final_score_setup = ScriptRunner($Script_kpi, "final_score_setup");
    $head_hr = ScriptRunner($Script_kpi, "head_hr");
    $num_of_appraisals = ScriptRunner($Script_kpi, "num_of_appraisals");
    $appr_increments = ScriptRunner($Script_kpi, "appr_increments");

    //$goal_obj = ScriptRunner($Script_kpi,"goal_obj");
    //confirm if it is checked...
    //----------1
    if ($appr1_emp == '1') {
        $appr1_emp_confirm = 'checked="checked"';
    }
    if ($appr1_deliv == '1') {
        $appr1_deliv_confirm = 'checked="checked"';
    }
    if ($appr1_comment == '1') {
        $appr1_comment_confirm = 'checked="checked"';
    }
    if ($Appr1_section_comment == '1') {
        $Appr1_section_comment = 'checked="checked"';
    }
    //-----------2
    if ($appr2_emp == '1') {
        $appr2_emp_confirm = 'checked="checked"';
    }
    if ($appr2_deliv == '1') {
        $appr2_deliv_confirm = 'checked="checked"';
    }
    if ($appr2_comment == '1') {
        $appr2_comment_confirm = 'checked="checked"';
    }
    if ($Appr2_section_comment == '1') {
        $Appr2_section_comment = 'checked="checked"';
    }
    //-----------3
    if ($appr3_emp == '1') {
        $appr3_emp_confirm = 'checked="checked"';
    }
    if ($appr3_deliv == '1') {
        $appr3_deliv_confirm = 'checked="checked"';
    }
    if ($appr3_comment == '1') {
        $appr3_comment_confirm = 'checked="checked"';
    }
    if ($Appr3_section_comment == '1') {
        $Appr3_section_comment = 'checked="checked"';
    }
    //-----------4
    if ($appr4_emp == '1') {
        $appr4_emp_confirm = 'checked="checked"';
    }
    if ($appr4_deliv == '1') {
        $appr4_deliv_confirm = 'checked="checked"';
    }
    if ($appr4_comment == '1') {
        $appr4_comment_confirm = 'checked="checked"';
    }
    if ($Appr4_section_comment == '1') {
        $Appr4_section_comment = 'checked="checked"';
    }
    //-----------5
    if ($appr5_emp == '1') {
        $appr5_emp_confirm = 'checked="checked"';
    }
    if ($appr5_deliv == '1') {
        $appr5_deliv_confirm = 'checked="checked"';
    }
    if ($appr5_comment == '1') {
        $appr5_comment_confirm = 'checked="checked"';
    }
    if ($Appr5_section_comment == '1') {
        $Appr5_section_comment = 'checked="checked"';
    }
    //-----------6
    if ($appr6_emp == '1') {
        $appr6_emp_confirm = 'checked="checked"';
    }
    if ($appr6_deliv == '1') {
        $appr6_deliv_confirm = 'checked="checked"';
    }
    if ($appr6_comment == '1') {
        $appr6_comment_confirm = 'checked="checked"';
    }
    if ($Appr6_section_comment == '1') {
        $Appr6_section_comment = 'checked="checked"';
    }
    print '	<div class="col-md-12">
													<table class="table table-responsive table-hover table-striped">
										                <tr>
										                  <th><strong>No. of Appraisers</strong></th>
										                  <th><strong>Appraise Employee</strong></th>
										                  <th><strong>Comment on Deliverable</strong></th>
										                  <th><strong>Summary Comment</strong></th>
														  <th><strong>SECTION Comment</strong></th>
										                </tr>
														<tr>
															<td style="padding-bottom: 0px; padding-top: 3px;">1st Appraiser (Appraised Employee)</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div class="">
																	<input name="appr_emp" type="checkbox" value="1" id="appr_emp" ' . $appr1_emp_confirm . ' checked="checked" />
																	<label style="height: 15px;" for="appr_emp"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input name="appr_deliv" type="checkbox" value="1" id="appr_deliv" ' . $appr1_deliv_confirm . ' checked="checked"/>
																	<label style="height: 15px;" for="appr_deliv"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input name="appr_comm" type="checkbox" value="1" id="appr_comm" ' . $appr1_comment_confirm . ' checked="checked"/>
																	<label style="height: 15px;" for="appr_comm"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;" >
																<div>
																	<input name="appr_section1" type="checkbox" value="1" id="appr_section1" ' . $Appr1_section_comment . '/>
																	<label style="height: 15px;" for="appr_section1"></label>
																</div>
															</td>
														</tr>
														<tr>
															<td style="padding-bottom: 0px; padding-top: 3px;">2nd Appraiser (1st Appraiser\'s Manager)</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div class="">
																	<input type="checkbox" value="1" name="appr_emp2" id="appr_emp2" ' . $appr2_emp_confirm . ' checked="checked"/>
																	<label style="height: 15px;" for="appr_emp2"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input type="checkbox" value="1" name="appr_deliv2" id="appr_deliv2" ' . $appr2_deliv_confirm . ' checked="checked"/>
																	<label style="height: 15px;" for="appr_deliv2"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input type="checkbox" value="1" name="appr_comm2" id="appr_comm2" ' . $appr2_comment_confirm . ' checked="checked"/>
																	<label style="height: 15px;" for="appr_comm2"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;" >
																<div>
																	<input name="appr_section2" type="checkbox" value="1" id="appr_section2" ' . $Appr2_section_comment . '/>
																	<label style="height: 15px;" for="appr_section2"></label>
																</div>
															</td>
														</tr>
														<tr>
															<td style="padding-bottom: 0px; padding-top: 3px;">3rd Appraiser (2nd Appraiser\'s Manager)</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div class="">
																	<input type="checkbox" value="1" name="appr_emp3" id="appr_emp3" ' . $appr3_emp_confirm . '/>
																	<label style="height: 15px;" for="appr_emp3"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input type="checkbox" value="1" name="appr_deliv3" id="appr_deliv3" ' . $appr3_deliv_confirm . '/>
																	<label style="height: 15px;" for="appr_deliv3"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;">
																<div>
																	<input type="checkbox" value="1" name="appr_comm3" id="appr_comm3" ' . $appr3_comment_confirm . '/>
																	<label style="height: 15px;" for="appr_comm3"></label>
																</div>
															</td>
															<td style="padding-bottom: 0px; padding-top: 3px;" >
																<div>
																	<input name="appr_section3" type="checkbox" value="1" id="appr_section3" ' . $Appr3_section_comment . '/>
																	<label style="height: 15px;" for="appr_section3"></label>
																</div>
															</td>
														</tr>
									                	<tr>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">4th Appraiser (3rd Appraisers Manager)</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div class="">
									                				<input type="checkbox" value="1" name="appr_emp4" id="appr_emp4" ' . $appr4_emp_confirm . '/>
									                				<label style="height: 15px;" for="appr_emp4"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div>
									                				<input type="checkbox" value="1" name="appr_deliv4" id="appr_deliv4" ' . $appr4_deliv_confirm . '/>
									                				<label style="height: 15px;" for="appr_deliv4"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div>
									                				<input type="checkbox" value="1" name="appr_comm4" id="appr_comm4" ' . $appr4_comment_confirm . '/>
									                				<label style="height: 15px;" for="appr_comm4"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;" >
									                			<div>
									                				<input name="appr_section4" type="checkbox" value="1" id="appr_section4" ' . $Appr4_section_comment . '/>
									                				<label style="height: 15px;" for="appr_section4"></label>
									                			</div>
									                		</td>
									                	</tr>
										                <tr>
										                	<td style="padding-bottom: 0px; padding-top: 3px;">5th Appraiser (4th Appraiser\'s Manager))</td>
										                	<td style="padding-bottom: 0px; padding-top: 3px;">
										                		<div class="">
										                			<input type="checkbox" value="1" name="appr_emp5" id="appr_emp5" ' . $appr5_emp_confirm . '/>
										                			<label style="height: 15px;" for="appr_emp5"></label>
										                		</div>
										                	</td>
										                	<td style="padding-bottom: 0px; padding-top: 3px;">
										                		<div>
										                			<input type="checkbox" value="1" name="appr_deliv5" id="appr_deliv5" ' . $appr5_deliv_confirm . '/>
										                			<label style="height: 15px;" for="appr_deliv5"></label>
										                		</div>
										                	</td>
										                	<td style="padding-bottom: 0px; padding-top: 3px;">
										                		<div>
										                			<input type="checkbox" value="1" name="appr_comm5" id="appr_comm5" ' . $appr5_comment_confirm . '/>
										                			<label style="height: 15px;" for="appr_comm5"></label>
										                		</div>
										                	</td>
										                	<td style="padding-bottom: 0px; padding-top: 3px;" >
										                		<div>
										                			<input name="appr_section5" type="checkbox" value="1" id="appr_section5" ' . $Appr5_section_comment . '/>
										                			<label style="height: 15px;" for="appr_section5"></label>
										                		</div>
										                	</td>
										                </tr>
									                	<tr>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">6th Appraiser (5th Appraiser\'s Manager)</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div class="">
									                				<input type="checkbox" value="1" name="appr_emp6" id="appr_emp6" ' . $appr6_emp_confirm . '/>
									                				<label style="height: 15px;" for="appr_emp6"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div>
									                				<input type="checkbox" value="1" name="appr_deliv6" id="appr_deliv6" ' . $appr6_deliv_confirm . '/>
									                				<label style="height: 15px;" for="appr_deliv6"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;">
									                			<div>
									                				<input type="checkbox" value="1" name="appr_comm6" id="appr_comm6" ' . $appr6_comment_confirm . '/>
									                				<label style="height: 15px;" for="appr_comm6"></label>
									                			</div>
									                		</td>
									                		<td style="padding-bottom: 0px; padding-top: 3px;" >
									                			<div>
									                				<input name="appr_section6" type="checkbox" value="1" id="appr_section6" ' . $Appr6_section_comment . '/>
									                				<label style="height: 15px;" for="appr_section6"></label>
									                			</div>
									                		</td>
									                	</tr>
									              	</table>
									              	<br /><br />
													<!-- Editing form -->
													<div class="form-group row ">
														<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Head of HR :</label>
														<div class="col-sm-9  input-group-sm">
															<select class="form-control" name="head_hr" id="head_hr">
																<option value="--" selected>--</option>';
    $dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' order by SName Asc");
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $selected = ($head_hr == $row2['HashKey']) ? 'selected' : '';
        print '<option ' . $selected . ' value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
    }
    include '../login/dbClose2.php';
    print '
															</select>
														</div>
													</div>
													<div class="form-group row ">
														<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Number of Appraisals in a year:</label>
														<div class="col-sm-9  input-group-sm">
															<input type="number" min="1" name="num_of_appraisals" class="form-control" id="num_of_appraisal" value="' . $num_of_appraisals . '"/>
														</div>
													</div>
													<div class="form-group row ">
														<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Appraisal Incremental Values:</label>
														<div class="col-sm-9  input-group-sm">
															<input type="number" min="0" name="appr_increments" class="form-control" id="appr_increment" value="' . $appr_increments . '" step="0.05" />
														</div>
													</div>
													<!-- Editing form -->
													<div class="form-group row ">
														<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Final Score Setup:</label>
														<div class="col-sm-9  input-group-sm">
															<select class="form-control" name="final_score_setup" id="final_score_setup">';
    if ($final_score_setup == "1") {
        print '
																	<option value="1" selected="selected">Last Appraiser</option>
																	<option value="2">Supervisor/Direct Line Manager</option>
																	<!--<option value="3">Average Score of all Appraiser</option>-->
																	';
    } elseif ($final_score_setup == "2") {
        print '
																	<option value="1">Last Appraiser</option>
																	<option value="2" selected="selected">Supervisor/Direct Line Manager</option>
																	<!--<option value="3">Average Score of all Appraiser</option>-->
																	';
    } elseif ($final_score_setup == "3") {
        print '
																	<option value="1">Last Appraiser</option>
																	<option value="2">Supervisor/Direct Line Manager</option>
																	<!--<option value="3" selected="selected">Average Score of all Appraiser</option>-->
																	';
    } else {
        print '
																	<option value="1">Last Appraiser</option>
																	<option value="2">Supervisor/Direct Line Manager</option>
																	<!--<option value="3">Average Score of all Appraiser</option>-->
																	';
    }

    print '
															</select>
														</div>
													</div>
													<br />
												';
    include "inc_mail_temp.php";
    ?>
										<?php
$apprActive = ScriptRunner("Select DATEDIFF(D,Getdate(),EDate) GtD, HashKey from [KPIStart] where Status in ('A') order by SDate desc", 'GtD');
    if ($apprActive < 0) {
        $readonly = '';
    } else {
        $readonly = 'readonly';
    }
    ?>
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<h5 class="text-center text-bold">Objectives for the coming year</h5>
												<hr>
											  	<div class="row">
											  		<?php for ($i = 0; $i < 10; $i++): ?>
													    <div class="col-md-4">
													      <div class="form-group row">
													        <label  class="col-sm-5 col-form-label">Objective <?php echo ($i + 1) ?>:</label>
													        <div class="col-sm-7 input-group-sm">
													          	<input name="objectives[]" type="text" class="form-control" <?php echo $readonly ?> value="<?php echo (isset($objectives[$i])) ? $objectives[$i] : ''; ?>" />
													        </div>
													      </div>
													    </div>
											  		<?php endfor?>
											  	</div>
											</div>
										</div>
										<div class="box bg-pale-secondary">
											<?php
$mgrAssess = json_decode(ScriptRunner($Script_kpi, "manager_assessment"), true);
    ?>
											<div class="box-body">
												<h5 class="text-center text-bold">Manager's Assessment</h5>
												<hr>
												<h6 class="text-left text-bold">Rating Questions</h6>
											  	<div class="row">
									    		    <div class="col-md-12 rating-input_fields_wrap">
									    		    	<?php if ($apprActive < 0): ?>
								    		      			<button type="button" class="btn btn-danger btn-md mb-2" id="addRating"><i class="fa fa-plus"></i></button>
									    		    	<?php endif?>
								    		      		<?php if (isset($mgrAssess) && count($mgrAssess['ratings'])): ?>
									    		      		<?php for ($i = 1; $i <= count($mgrAssess['ratings']); $i++): ?>
											    		      	<div class="form-group row" id="rating-<?php echo $i; ?>">
												    		        <div class="col-sm-11 input-group-sm">
												    		          <?php
print '<input name="ratings[]" ' . $readonly . ' value="' . $mgrAssess['ratings'][$i - 1] . '" type="text" class="form-control" />';
    ?>
												    		        </div>
												    		        <?php if ($apprActive < 0): ?>
												    		        	<button type="button" class='btn btn-danger rating_remove' id="<?php echo $i; ?>" ><i class='fa fa-remove' ></i></button>
												    		        <?php endif?>
										    		      		</div>
									    		      		<?php endfor?>
								    		      		<?php else: ?>
		    		      				    		      	<div class="form-group row" id="rating-1">
		    		      					    		        <div class="col-sm-11 input-group-sm">
		    		      					    		          <?php
print '<input name="ratings[]" ' . $readonly . ' type="text" class="form-control" />';
    ?>
		    		      					    		        </div>
		    		      					    		        <?php if ($apprActive < 0): ?>
		    		      					    		        	<button type="button" class='btn btn-danger rating_remove'  id='1' ><i class='fa fa-remove' ></i></button>
		    		      					    		        <?php endif?>
		    		      			    		      		</div>
								    		      		<?php endif?>
									    		    </div>
											  	</div>
												<h6 class="text-left text-bold">Comment Questions</h5>
											  	<div class="row">
									    		    <div class="col-md-12 comment-input_fields_wrap">
									    		    	<?php if ($apprActive < 0): ?>
								    		      			<button type="button" class="btn btn-danger btn-md mb-2" id="addComment"><i class="fa fa-plus"></i></button>
									    		    	<?php endif?>
								    		      		<?php if (isset($mgrAssess) && count($mgrAssess['comments'])): ?>
									    		      		<?php for ($i = 1; $i <= count($mgrAssess['comments']); $i++): ?>
											    		      	<div class="form-group row" id="comment-<?php echo $i; ?>">
												    		        <div class="col-sm-11 input-group-sm">
												    		          <?php
print '<input name="comments[]" ' . $readonly . ' type="text" value="' . $mgrAssess['comments'][$i - 1] . '" class="form-control" />';
    ?>
												    		        </div>
												    		        <?php if ($apprActive < 0): ?>
												    		        	<button type="button" class='btn btn-danger comment_remove'  id='<?php echo $i; ?>' ><i class='fa fa-remove' ></i></button>
												    		        <?php endif?>
										    		      		</div>
									    		      		<?php endfor?>
							    		      			<?php else: ?>
	    		      					    		      	<div class="form-group row" id="comment-1">
	    		      						    		        <div class="col-sm-11 input-group-sm">
	    		      						    		          <?php
print '<input name="comments[]" ' . $readonly . ' type="text" class="form-control" />';
    ?>
	    		      						    		        </div>
	    		      						    		        <?php if ($apprActive < 0): ?>
	    		      						    		        	<button type="button" class='btn btn-danger comment_remove'  id='1' ><i class='fa fa-remove' ></i></button>
	    		      						    		        <?php endif?>
	    		      				    		      		</div>
								    		      		<?php endif?>
									    		    </div>
											  	</div>
											</div>
										</div>
										<?php
if ($sel_apr_status == "A") { //Show Update Button, for only Active Apraisal
        if (ValidateURths("APPRAISAL SETTING" . "A") == true) {
            echo '<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger" id="SubmitTrans" value="Update Appraisal" /><br />';
            echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $_REQUEST['hash'] . '" />';
        }
    }
    echo '</div>';
} else {
    print ' <div class="col-md-12">
											<table class="table-hover table-responsive table table-striped table-bordered">
								                <tr>
								                  <th>No. of Appraisers</th>
								                  <th>Appraise Employee</th>
								                  <th>Comment on Deliverable</th>
								                  <th>Summary Comment</th>
												  <th>SECTION Comment</th>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">1st Appraiser (Appraised Employee)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_emp" type="checkbox" value="1" id="appr_emp" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_emp"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_deliv" type="checkbox" value="1" id="appr_deliv" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_deliv"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm" type="checkbox" value="1" id="appr_comm" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_comm"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section1" type="checkbox" value="1" id="appr_section1"/>
								                  	    	<label style="height: 15px;"  for="appr_section1"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">2nd Appraiser (1st Appraiser\'s Manager)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_emp2" id="appr_emp2" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_emp2"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_deliv2" id="appr_deliv2" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_deliv2"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm2" type="checkbox" value="1" id="appr_comm2" checked="checked" />
								                  	    	<label style="height: 15px;"  for="appr_comm2"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section2" type="checkbox" value="1" id="appr_section2"/>
								                  	    	<label style="height: 15px;"  for="appr_section2"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">3rd Appraiser (2nd Appraiser\'s Manager)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_emp3" id="appr_emp3" />
								                  	    	<label style="height: 15px;"  for="appr_emp3"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_deliv3" id="appr_deliv3" />
								                  	    	<label style="height: 15px;"  for="appr_deliv3"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm3" type="checkbox" value="1" id="appr_comm3"/>
								                  	    	<label style="height: 15px;"  for="appr_comm3"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section3" type="checkbox" value="1" id="appr_section3"/>
								                  	    	<label style="height: 15px;"  for="appr_section3"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">4th Appraiser (3rd Appraiser\'s Manager)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_emp4" id="appr_emp4" />
								                  	    	<label style="height: 15px;"  for="appr_emp4"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_deliv4" id="appr_deliv4" />
								                  	    	<label style="height: 15px;"  for="appr_deliv4"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm4" type="checkbox" value="1" id="appr_comm4"/>
								                  	    	<label style="height: 15px;"  for="appr_comm4"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section4" type="checkbox" value="1" id="appr_section4"/>
								                  	    	<label style="height: 15px;"  for="appr_section4"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">5th Appraiser (4th Appraiser\'s Manager)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_emp5" id="appr_emp5" />
								                  	    	<label style="height: 15px;"  for="appr_emp5"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_deliv5" id="appr_deliv5" />
								                  	    	<label style="height: 15px;"  for="appr_deliv5"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm5" type="checkbox" value="1" id="appr_comm5"/>
								                  	    	<label style="height: 15px;"  for="appr_comm5"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section5" type="checkbox" value="1" id="appr_section5"/>
								                  	    	<label style="height: 15px;"  for="appr_section5"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
								                <tr>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">6th Appraiser (5th Appraiser\'s Manager)</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_emp6" id="appr_emp6" />
								                  	    	<label style="height: 15px;"  for="appr_emp6"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input type="checkbox" value="1" name="appr_deliv6" id="appr_deliv6" />
								                  	    	<label style="height: 15px;"  for="appr_deliv6"></label>
								                  	 	</div>
								                  	</td>
								                  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_comm6" type="checkbox" value="1" id="appr_comm6"/>
								                  	    	<label style="height: 15px;"  for="appr_comm6"></label>
								                  	 	</div>
								                  	</td>
												  	<td style="padding-bottom: 6px; padding-top: 6px;">
								                  	  	<div class="">
								                  	    	<input name="appr_section6" type="checkbox" value="1" id="appr_section6"/>
								                  	    	<label style="height: 15px;"  for="appr_section6"></label>
								                  	 	</div>
								                  	</td>
								                </tr>
				                          	</table>
				                          	<br /><br />
				                          	<div class="form-group row ">
				                          		<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Head of HR :</label>
				                          		<div class="col-sm-9  input-group-sm">
				                          			<select class="form-control" name="head_hr" id="head_hr">
				                          				<option value="--" selected>--</option>';
    $dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' order by SName Asc");
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        print '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
    }
    include '../login/dbClose2.php';
    print '
				                          			</select>
				                          		</div>
				                          	</div>
				                          	<div class="form-group row ">
												<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Number of Appraisals in a year:</label>
												<div class="col-sm-9  input-group-sm">
													<input type="number" min="1" name="num_of_appraisals" class="form-control" id="num_of_appraisal" value="2"/>
												</div>
											</div>
                                              <div class="form-group row ">
                                                <label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Appraisal Incremental Values:</label>
												<div class="col-sm-9  input-group-sm">
													<input type="number" min="0" name="appr_increments" class="form-control" id="appr_increment" value="0.5" step="0.01" />
												</div>
											</div>
				            				<!-- Editing form -->
				            				<div class="form-group row ">
					            				<label  class="col-sm-3 col-form-label" style="padding-right: 0px;" >Final Score Setup:</label>
					            				<div class="col-sm-9  input-group-sm">
													<select class="form-control" name="final_score_setup" id="final_score_setup">
													    <option value="1">Last Appraiser</option>
													    <option value="2">Supervisor/Direct Line Manager</option>
													    <!--<option value="3">Average Score of all Appraiser</option>-->
													</select>
												</div>
											</div>
											<br />
										';
    include "inc_mail_temp.php";
    ?>
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<h5 class="text-center text-bold">Objectives for the coming year</h5>
												<hr>
											  	<div class="row">
										      		<?php for ($i = 0; $i < 10; $i++): ?>
										    		    <div class="col-md-4">
										    		      <div class="form-group row">
										    		        <label  class="col-sm-5 col-form-label">Objective <?php echo ($i + 1) ?>:</label>
										    		        <div class="col-sm-7 input-group-sm">
										    		          <?php
print '<input name="objectives[]" type="text" class="form-control" />';
    ?>
										    		        </div>
										    		      </div>
										    		    </div>
										      		<?php endfor?>
											  	</div>
											</div>
										</div>
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<h5 class="text-center text-bold">Manager's Assessment</h5>
												<hr>
												<h6 class="text-left text-bold">Rating Questions</h6>
											  	<div class="row">
									    		    <div class="col-md-12 rating-input_fields_wrap">
								    		      		<button type="button" class="btn btn-danger btn-md mb-2" id="addRating"><i class="fa fa-plus"></i></button>
									    		      	<div class="form-group row" id="rating-1">
										    		        <div class="col-sm-11 input-group-sm">
										    		          <?php
print '<input name="ratings[]" type="text" class="form-control" />';
    ?>
										    		        </div>
										    		        <button type="button" class='btn btn-danger rating_remove'  id='1' ><i class='fa fa-remove' ></i></button>
								    		      		</div>
									    		    </div>
											  	</div>
												<h6 class="text-left text-bold">Comment Questions</h5>
											  	<div class="row">
									    		    <div class="col-md-12 comment-input_fields_wrap">
								    		      		<button type="button" class="btn btn-danger btn-md mb-2" id="addComment"><i class="fa fa-plus"></i></button>
									    		      	<div class="form-group row" id="comment-1">
										    		        <div class="col-sm-11 input-group-sm">
										    		          <?php
print '<input name="comments[]" type="text" class="form-control" />';
    ?>
										    		        </div>
										    		        <button type="button" class='btn btn-danger comment_remove'  id='1' ><i class='fa fa-remove' ></i></button>
								    		      		</div>
									    		    </div>
											  	</div>
											</div>
										</div>
										<?php if (ValidateURths("APPRAISAL SETTING" . "A") == true) {
        if ($EditID == "" || $EditID == '--') {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Create Appraisal" />';} elseif ((strlen(trim($EditID)) == 32) && ($apr_status_sel == 'A')) {echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Appraisal" />';
            echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';}
    }
    echo '</div>';
}
?>
								<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
									<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
									<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
//Check if any record was spolled at all before enabling buttons
//Set FAction Hidden Value for each button
echo "<input name='FAction' id='FAction' type='hidden'>";
?>
							</form>
						</div>
					</div>
					<div class="row mt-10">
						<div class="col-md-12">
						    <table class="table-bordered table-hover table table-responsive table-striped" id="table">
						        <thead>
							        <tr>
							            <th>End Date</th>
							            <th><strong>View/Edit</strong></th>
							            <th><strong>Set Instructions</strong></th>
							            <th>Set Header Text</th>
							        </tr>
						        </thead>
						        <tbody>
							        <?php
$Del = 0;
$dbOpen2 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPISettingNew] where (Status <> 'D') order by Status asc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

    $hash = $row2['HashKey'];
    $num_appr = $row2['num_appraisers'];
    $appr_status = $row2['Status'];
    ?>
									        <tr>
									            <?php $Script = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where EmpID=(Select EmpID from Users where LogName='" . $row2['AuthBy'] . "' and Status in('A') )"; // Gets staff full details from the Emp Table?>
									            <td>&nbsp;
									              	<?php
if ($appr_status == 'A') {
        echo '<b>' . (trim($row2['EDt'])) . '</b>';
    } else {
        echo (trim($row2['EDt']));
    }
    ?>
												</td>
									            <td>
										            <?php
print "<a href=\"ApprSetup.php?hash=$hash&type=view_modify\"><img src=\"../images/view-icon.png\"  width=\"25\" ></a>";
    ?>
												</td>
									            <td>
													<?php
if ($appr_status == 'A') {
        $instructio_group = trim($row2['Dt']);
        $ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
        $imgID = "MastersDepartment";
        if ($ModMaster == true) {
            $imgPath = "../images/plus_.jpg";
            $Titl = "Instructions";
            $MOvr = "";
            //$OnClk="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=".$instructio_group."&LDb=Single&amp;Elmt=Instructions',320,400,'No')";

            $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=Instruction&LDb=Single&amp;Elmt=Instructions',320,400,'No')";
        } else {
            $imgPath = "../images/bi_butn/infoBW.jpg";
            $Titl = "Select a Key Result Area under which this KPI falls";
            $MOvr = "MM_swapImage('" . $imgID . "','','../images/bi_butn/info.jpg',1)";
            $OnClk = "";
        }
        echo '<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="' . $MOvr . '"><img id="' . $imgID . '" src="' . $imgPath . '" width="15" title="' . $Titl . '" onClick="' . $OnClk . '" border="0"/></a>';
    }
    ?>
												</td>
									            <td>
													<?php
if ($appr_status == 'A') {
        $instructio_group = trim($row2['Dt']);
        $ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
        $imgID = "MastersDepartment";
        if ($ModMaster == true) {
            $imgPath = "../images/plus_.jpg";
            $Titl = "Appraisal Header";
            $MOvr = "";
            //$OnClk="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=".$instructio_group."&LDb=Single&amp;Elmt=Instructions',320,400,'No')";

            $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=Appraisal_Header&LDb=Single&amp;Elmt=Appraisal_Header',320,400,'No')";
        } else {
            $imgPath = "../images/bi_butn/infoBW.jpg";
            $Titl = "Select a Key Result Area under which this KPI falls";
            $MOvr = "MM_swapImage('" . $imgID . "','','../images/bi_butn/info.jpg',1)";
            $OnClk = "";
        }
        echo '<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="' . $MOvr . '"><img id="' . $imgID . '" src="' . $imgPath . '" width="15" title="' . $Titl . '" onClick="' . $OnClk . '" border="0"/></a>';
    }
    ?>
												</td>
									        </tr>
							       	<?php }
//include '../login/dbClose2.php';
?>
        						</tbody>
      						</table>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<div class="text-center">
      							<p>Click the button below to Restart Employee Appraisal</p>
      							<a href="DelApprisal.php"><img height="50" width="50" src="../images/refresh.jpg"></a>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
    <script type="text/javascript">
    	$(function() {
    	    const wrapper = $(".rating-input_fields_wrap");
    	    let x = <?php echo (isset($mgrAssess) && count($mgrAssess['ratings'])) ? count($mgrAssess['ratings']) : 1 ?>;
    	    $("#addRating").click(function(e) {
    	      	e.preventDefault();
    	        ++x;
    	        $(wrapper).append("<div class='form-group row' id='rating-"+x+"' >"
    	        +"<div class='col-sm-11 input-group-sm'>"
    	        +"<input name='ratings[]' type='text' class='form-control' />"
    	        +"</div>"
    	        +"<button type='button' class='btn btn-danger rating_remove '  id='"+x+"' ><i class='fa fa-remove' ></i></button>"
    	        +"</div>");
    	    });
    	     $(document).on('click','.rating_remove', function(){
    	        let button_id = $(this).attr("id");
	        	$("#rating-"+button_id+"").remove();
    	        return false;
    	    });
    	    //
    	    const comment_wrapper = $(".comment-input_fields_wrap");
    	    let count = <?php echo (isset($mgrAssess) && count($mgrAssess['comments'])) ? count($mgrAssess['comments']) : 1 ?>;
    	    $("#addComment").click(function(e) {
    	      	e.preventDefault();
    	        ++count;
    	        $(comment_wrapper).append("<div class='form-group row'  id='comment-"+count+"' >"
    	        +"<div class='col-sm-11 input-group-sm'>"
    	        +"<input name='comments[]' type='text' class='form-control' />"
    	        +"</div>"
    	        +"<button type='button' class='btn btn-danger comment_remove'  id='"+count+"' ><i class='fa fa-remove' ></i></button>"
    	        +"</div>");
    	    });
    	     $(document).on('click','.comment_remove', function(){
    	        let button_id = $(this).attr("id");
	        	$("#comment-"+button_id+"").remove();
    	        return false;
    	    });
    	})
    </script>
</body>

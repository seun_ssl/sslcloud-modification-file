<?php session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;include '../css/myscripts.php';

//Validate user viewing rights
// if (ValidateURths("APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
if (ValidateURths("VIEW APPRAISAL" . "T") != true) {include '../main/NoAccess.php';exit;}

/*
//AlL VARIABLES USED ARE DECLARED AT THIS POINT TO AVOID ANY ERRORS
 */
if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($SelID)) {$SelID = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($StatusSet)) {$StatusSet = 0;}
if (!isset($KPICount)) {$KPICount = 0;}
if (!isset($TWeightage)) {$TWeightage = 0;}
if (!isset($TotMgr)) {$TotMgr = 0;}

if (!isset($Total_Mgr_score)) {$Total_Mgr_score = 0;}

if (!isset($o)) {$o = 0;}
if (!isset($TotEmp)) {$TotEmp = 0;}
if (!isset($TotMed)) {$TotMed = 0;}
if (!isset($o)) {$o = 0;}
if (!isset($AllEmployee)) {$AllEmployee = "";}
if (!isset($kk_all)) {$kk_all = "";}
if (!isset($EScore)) {$EScore = "";}
if (!isset($DrpCnt)) {$DrpCnt = 0;}
if (!isset($UpdUser)) {$UpdUser = "";}
if (!isset($AQuartID_Val)) {$AQuartID_Val = "";}
if (!isset($AuthRec)) {$AuthRec = "";}
if (!isset($FinalHashKey)) {$FinalHashKey = "";}
if (!isset($final_score_type)) {$final_score_type = "";} // done

if (!isset($TotAScoreLevel3)) {$TotAScoreLevel3 = 0;}
if (!isset($TotAScoreLevel4)) {$TotAScoreLevel4 = 0;}
if (!isset($TotAScoreLevel5)) {$TotAScoreLevel5 = 0;}
if (!isset($TotAScoreLevel6)) {$TotAScoreLevel6 = 0;}
if (!isset($total_score_current)) {$total_score_current = 0;}

if (!isset($_REQUEST["3rdAps"])) {$_REQUEST["3rdAps"] = "";}
if (!isset($_REQUEST["4thAps"])) {$_REQUEST["4thAps"] = "";}
if (!isset($_REQUEST["5thAps"])) {$_REQUEST["5thAps"] = "";}
if (!isset($_REQUEST["6thAps"])) {$_REQUEST["6thAps"] = "";}

$Script_hkey = "Select * from KPISettingNew where Status = 'A'";
$_SESSION['KPISettingNewHashKey'] = ScriptRunner($Script_hkey, "HashKey");
$final_score_setup = ScriptRunner($Script_hkey, "final_score_setup");

$goal_desc = ScriptRunner($Script_hkey, "goal_desc");
$goal_period_from_month = ScriptRunner($Script_hkey, "goal_period_from_month");
$goal_period_from_year = ScriptRunner($Script_hkey, "goal_period_from_year");
$goal_period_to_month = ScriptRunner($Script_hkey, "goal_period_to_month");
$goal_period_to_year = ScriptRunner($Script_hkey, "goal_period_to_year");
$goal_header1 = ScriptRunner($Script_hkey, "goal_header1");
$goal_header2 = ScriptRunner($Script_hkey, "goal_header2");
$goal_header3 = ScriptRunner($Script_hkey, "goal_header3");
$goal_header4 = ScriptRunner($Script_hkey, "goal_header4");
$goal_num_row_allow = ScriptRunner($Script_hkey, "goal_num_row_allow");

//=========================== END KPI (Show END button) ==================================
/*$kpi_Appr1_emp=ScriptRunner($Script_hkey,"Appr1_emp");
$kpi_Appr1_deliv=ScriptRunner($Script_hkey,"Appr1_deliv");
$kpi_Appr1_comment=ScriptRunner($Script_hkey,"Appr1_comment");
$kpi_Appr1_section_comment=ScriptRunner($Script_hkey,"Appr1_section_comment");

$kpi_Appr2_emp=ScriptRunner($Script_hkey,"Appr2_emp");
$kpi_Appr2_deliv=ScriptRunner($Script_hkey,"Appr2_deliv");
$kpi_Appr2_comment=ScriptRunner($Script_hkey,"Appr2_comment");
$kpi_Appr2_section_comment=ScriptRunner($Script_hkey,"Appr2_section_comment");
 */

$kpi_Appr3_emp = ScriptRunner($Script_hkey, "Appr3_emp");
$kpi_Appr3_deliv = ScriptRunner($Script_hkey, "Appr3_deliv");
$kpi_Appr3_comment = ScriptRunner($Script_hkey, "Appr3_comment");
$kpi_Appr3_section_comment = ScriptRunner($Script_hkey, "Appr3_section_comment");

$kpi_Appr4_emp = ScriptRunner($Script_hkey, "Appr4_emp");
$kpi_Appr4_deliv = ScriptRunner($Script_hkey, "Appr4_deliv");
$kpi_Appr4_comment = ScriptRunner($Script_hkey, "Appr4_comment");
$kpi_Appr4_section_comment = ScriptRunner($Script_hkey, "Appr4_section_comment");

$kpi_Appr5_emp = ScriptRunner($Script_hkey, "Appr5_emp");
$kpi_Appr5_deliv = ScriptRunner($Script_hkey, "Appr5_deliv");
$kpi_Appr5_comment = ScriptRunner($Script_hkey, "Appr5_comment");
$kpi_Appr5_section_comment = ScriptRunner($Script_hkey, "Appr5_section_comment");

$kpi_Appr6_emp = ScriptRunner($Script_hkey, "Appr6_emp");
$kpi_Appr6_deliv = ScriptRunner($Script_hkey, "Appr6_deliv");
$kpi_Appr6_comment = ScriptRunner($Script_hkey, "Appr6_comment");
$kpi_Appr6_section_comment = ScriptRunner($Script_hkey, "Appr6_section_comment");

$kpi_Appr7_emp = ScriptRunner($Script_hkey, "Appr7_emp");
$kpi_Appr7_deliv = ScriptRunner($Script_hkey, "Appr7_deliv");
$kpi_Appr7_comment = ScriptRunner($Script_hkey, "Appr7_comment");

//==========================================================================================

$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

//$mgr_name=ScriptRunner($Script,"Nm");
$mgr_name = $_SESSION["StkTck" . "FName"];

$next_manager = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
$next_manager_hash = ScriptRunner($next_manager, "MgrHashKey");
// var_dump($next_manager_hash);
// die();
/*
//PROVIDES SECURITY FROM SUBMITTING UNVERIFIED DATA TO THE PAGE CODE HANDLER
//USE DoS TOKEN TO HOLD OUT AND NOT ALLOW THE FINAL VALUE PASS
 */
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
/*
//IF AUTHORIZED BUTTON IS CLOCKED. PERFORM THIS ACTION
 */
    if (isset($_REQUEST['button1']) && $_REQUEST['button1'] == "SUBMIT TO NEXT LEVEL APPRAISER") {
        echo ("<script type='text/javascript'>{ parent.msgbox('Please Wait..', 'green'); }</script>");
        //----------- new
        $Script8 = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
        $MgrHashKey = ScriptRunner($Script8, "MgrHashKey");

        $Script11 = "SELECT * FROM KPIFinalScore WHERE HashKey='" . ECh($_REQUEST["HashNum"]) . "' and Status <> 'D'";
        $EID = ScriptRunner($Script11, "EID");
        $CAprsID3 = ScriptRunner($Script11, "AprsID3");
        $CAprsID4 = ScriptRunner($Script11, "AprsID4");
        $CAprsID5 = ScriptRunner($Script11, "AprsID5");
        $CAprsID6 = ScriptRunner($Script11, "AprsID6");
        $CAprsID7 = ScriptRunner($Script11, "AprsID7");
        $CAprsID8 = ScriptRunner($Script11, "AprsID8");
        $LUO_Num_old = ScriptRunner($Script11, "LUO_Num");
        $ManagerScore = ScriptRunner($Script11, "MScore");

        if ($final_score_setup == "2") { //If Supervisor/Direct Line Mgr totScore is selected to be Final score
            $final_score = ECh($_REQUEST["FScore2"]);
        }

        //----------- new

        //if($CAprsID3==""){
        if (!$CAprsID3) {

            //--------------- loop to update indvKPI
            if ($kpi_Appr3_emp == "1") {
                $EditID = 0;
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = $_REQUEST["DelBox" . $php_i];

                        $Script = "Update [KPIIndvScore] set [AScoreLevel3Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                        ScriptRunnerUD($Script, "Inst");
                        $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];

                    }
                }
            }

            if ($final_score_setup == "1" && $kpi_Appr3_emp == "1") { //If Last person totScore is selected to be Final score
                $final_score = $total_score_current;
            } elseif ($final_score_setup == "1" && $kpi_Appr3_emp != "1") {
                $final_score = ECh($_REQUEST["FScore2"]);
            } elseif ($final_score_setup == "3") { //Final Score==Avereage
                include 'final_score_average_function.php';
            } else {
                $final_score = $ManagerScore;
            }
            //---------------------------------------

            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $data = [];
            if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                    $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                    $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                    $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '';
                    $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                    $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                    $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                }
                if (count($data)) {
                    $NxtLvl_Accmp = json_encode($data);
                } else {
                    $NxtLvl_Accmp = null;
                }
            } else {
                $NxtLvl_Accmp = null;
            }

            $objective_data = [];
            if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                    if (!empty($_REQUEST["objectives"][$i])) {
                        $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                        $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                        $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                        $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                        $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                        $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                        $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                        $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                    }
                }
                if (count($objective_data)) {
                    $objectivesData = json_encode($objective_data);
                } else {
                    $objectivesData = null;
                }
            } else {
                $objectivesData = null;
            }

            $Script = "Update [KPIFinalScore] SET
				[FScore]=" . ECh($final_score) . ",
			[AprsID3]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[AprsComment3]='" . ECh($_REQUEST["3rdAps"]) . "',
								[AprsName3]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            ScriptRunnerUD($Script, "CM");

            echo ("<script type='text/javascript'>{ parent.msgbox('(3) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "third person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        } elseif (!$CAprsID4) {
            $total_score_current = 0;

            //--------------- loop to update indvKPI
            if ($kpi_Appr4_emp == "1") {
                $EditID = 0;
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = $_REQUEST["DelBox" . $php_i];

                        $Script = "Update [KPIIndvScore] set [AScoreLevel4Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                        ScriptRunnerUD($Script, "Inst");
                        $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];

                    }
                }
            }
            if ($final_score_setup == "1" && $kpi_Appr4_emp == "1") { //If Last person totScore is selected to be Final score
                $final_score = $total_score_current;
            } elseif ($final_score_setup == "1" && $kpi_Appr4_emp != "1") {
                $final_score = ECh($_REQUEST["FScore2"]);
            } elseif ($final_score_setup == "3") { //Final Score==Avereage
                include 'final_score_average_function.php';
            } else {
                $final_score = $ManagerScore;
            }
            //---------------------------------------

            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $data = [];
            if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                    $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                    $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                    $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                    $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                    $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                    $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                }
                if (count($data)) {
                    $NxtLvl_Accmp = json_encode($data);
                } else {
                    $NxtLvl_Accmp = null;
                }
            } else {
                $NxtLvl_Accmp = null;
            }

            $objective_data = [];
            if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                    if (!empty($_REQUEST["objectives"][$i])) {
                        $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                        $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                        $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                        $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                        $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                        $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                        $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                        $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                    }
                }
                if (count($objective_data)) {
                    $objectivesData = json_encode($objective_data);
                } else {
                    $objectivesData = null;
                }
            } else {
                $objectivesData = null;
            }

            $Script = "Update [KPIFinalScore] SET [AprsID4]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[AprsComment4]='" . ECh($_REQUEST["4thAps"]) . "',
								[AprsName4]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[FScore]=" . ECh($final_score) . ",
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            ScriptRunnerUD($Script, "CM");

            echo ("<script type='text/javascript'>{ parent.msgbox('(4) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "fourth person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        } elseif (!$CAprsID5) {
            $total_score_current = 0;

            //--------------- loop to update indvKPI
            if ($kpi_Appr5_emp == "1") {
                $EditID = 0;
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = $_REQUEST["DelBox" . $php_i];

                        $Script = "Update [KPIIndvScore] set [AScoreLevel5Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                        ScriptRunnerUD($Script, "Inst");
                        $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];

                    }
                }
            }
            if ($final_score_setup == "1" && $kpi_Appr5_emp == "1") { //If Last person totScore is selected to be Final score
                $final_score = $total_score_current;
            } elseif ($final_score_setup == "1" && $kpi_Appr5_emp != "1") {
                $final_score = ECh($_REQUEST["FScore2"]);
            } elseif ($final_score_setup == "3") { //Final Score==Avereage
                include 'final_score_average_function.php';
            } else {
                $final_score = $ManagerScore;
            }
            //---------------------------------------

            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $data = [];
            if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                    $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                    $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                    $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                    $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                    $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                    $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                }
                if (count($data)) {
                    $NxtLvl_Accmp = json_encode($data);
                } else {
                    $NxtLvl_Accmp = null;
                }
            } else {
                $NxtLvl_Accmp = null;
            }

            $objective_data = [];
            if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                    if (!empty($_REQUEST["objectives"][$i])) {
                        $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                        $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                        $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                        $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                        $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                        $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                        $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                        $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                    }
                }
                if (count($objective_data)) {
                    $objectivesData = json_encode($objective_data);
                } else {
                    $objectivesData = null;
                }
            } else {
                $objectivesData = null;
            }

            $Script = "Update [KPIFinalScore] SET [AprsID5]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
			[FScore]=" . ECh($final_score) . ",
								[AprsComment5]='" . ECh($_REQUEST["5thAps"]) . "',
								[AprsName5]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            ScriptRunnerUD($Script, "CM");

            echo ("<script type='text/javascript'>{ parent.msgbox('(5) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "fifth person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        } elseif (!$CAprsID6) {
            $total_score_current = 0;

            //--------------- loop to update indvKPI
            if ($kpi_Appr6_emp == "1") {
                $EditID = 0;
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = $_REQUEST["DelBox" . $php_i];

                        $Script = "Update [KPIIndvScore] set [AScoreLevel6Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                        ScriptRunnerUD($Script, "Inst");
                        $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];
                    }
                }
            }
            if ($final_score_setup == "1" && $kpi_Appr6_emp == "1") { //If Last person totScore is selected to be Final score
                $final_score = $total_score_current;
            } elseif ($final_score_setup == "1" && $kpi_Appr6_emp != "1") {
                $final_score = ECh($_REQUEST["FScore2"]);
            } elseif ($final_score_setup == "3") { //Final Score==Avereage
                include 'final_score_average_function.php';
            } else {
                $final_score = $ManagerScore;
            }

            //---------------------------------------

            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $data = [];
            if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                    $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                    $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                    $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                    $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                    $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '----';
                    $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                }
                if (count($data)) {
                    $NxtLvl_Accmp = json_encode($data);
                } else {
                    $NxtLvl_Accmp = null;
                }
            } else {
                $NxtLvl_Accmp = null;
            }

            $objective_data = [];
            if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                    if (!empty($_REQUEST["objectives"][$i])) {
                        $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                        $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                        $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                        $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                        $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                        $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                        $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '----';
                        $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                    }
                }
                if (count($objective_data)) {
                    $objectivesData = json_encode($objective_data);
                } else {
                    $objectivesData = null;
                }
            } else {
                $objectivesData = null;
            }

            $Script = "Update [KPIFinalScore] SET [AprsID6]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
			[FScore]=" . ECh($final_score) . ",
								[AprsComment6]='" . ECh($_REQUEST["6thAps"]) . "',
								[AprsName6]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            ScriptRunnerUD($Script, "CM");

            echo ("<script type='text/javascript'>{ parent.msgbox('(6) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "sixth person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        } elseif (!$CAprsID7) {
            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $Script = "Update [KPIFinalScore] SET [AprsID7]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[AprsComment7]='" . ECh($_REQUEST["7thAps"]) . "',
								[AprsName7]='" . $name_rev . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            echo ("<script type='text/javascript'>{ parent.msgbox('(7) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "seventh person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        } elseif (!$CAprsID8) {
            $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
            $name_rev = ScriptRunner($Script_name_rev, "Nm");

            $Script = "Update [KPIFinalScore] SET [AprsID8]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[AprsComment8]='" . ECh($_REQUEST["8thAps"]) . "',
								[AprsName8]='" . $name_rev . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[LUO_HashKey]='" . $MgrHashKey . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

            echo ("<script type='text/javascript'>{ parent.msgbox('(8) Your appraisal was successfully Submited...', 'green'); }</script>");

            $mailer_from = "eigth person";
            $mailer_action = "submit";
            include "mail_trail_send_kpi.php";

        }

        include '../main/prmt_blank.php';

        $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $EID . "'";
        $AprsNm = ScriptRunner($Script99, "AprsNm");
        AuditLog("INSERT", "Comment on Appraisal done for " . $AprsNm);

    }

}

if (isset($_REQUEST['button2']) && $_REQUEST['button2'] == "Review Appraisal") {

    //----------- new
    /*
    $Script8="SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='".ECh($_SESSION["StkTck"."HKey"])."'";
    $MgrHashKey=ScriptRunner($Script8,"MgrHashKey");
     */

    $Script11 = "SELECT * FROM KPIFinalScore WHERE HashKey='" . ECh($_REQUEST["HashNum"]) . "' and Status <> 'D'";
    $CAprsID3 = ScriptRunner($Script11, "AprsID3");
    $CAprsID4 = ScriptRunner($Script11, "AprsID4");
    $CAprsID5 = ScriptRunner($Script11, "AprsID5");
    $CAprsID6 = ScriptRunner($Script11, "AprsID6");
    $CAprsID7 = ScriptRunner($Script11, "AprsID7");
    $CAprsID8 = ScriptRunner($Script11, "AprsID8");
    $LUO_Num_old = ScriptRunner($Script11, "LUO_Num");
    $EID = ScriptRunner($Script11, "EID");

    ///*

    if ($CAprsID3 == "") {

        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET
								[AprsID3]='',
								[LUO_HashKey]='',
								[EmpUpdCnt]='0',
								[MgrUpdCnt]='0',
								[Status]='U',
								[AprsComment3]='" . ECh($_REQUEST["3rdAps"]) . "',
								[AprsName3]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

        ScriptRunnerUD($Script, "CM");

        echo ("<script type='text/javascript'>{ parent.msgbox('(3) Your appraisal was successfully Reviewed...', 'green'); }</script>");
        $mailer_from = "third person";
        $mailer_action = "review";
        include "mail_trail_send_kpi.php";

    } elseif (!$CAprsID4) {

        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET
								[AprsID3]='',
								[AprsID4]='',
								[LUO_HashKey]='',
								[Status]='U',
								[AprsComment4]='" . ECh($_REQUEST["4thAps"]) . "',
								[AprsName4]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

        ScriptRunnerUD($Script, "CM");

        echo ("<script type='text/javascript'>{ parent.msgbox('(4) Your appraisal was successfully Reviewed...', 'green'); }</script>");
        $mailer_from = "fourth person";
        $mailer_action = "review";
        include "mail_trail_send_kpi.php";

    } elseif (!$CAprsID5) {

        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET
								[AprsID3]='',
								[AprsID4]='',
								[AprsID5]='',
								[LUO_HashKey]='',
								[Status]='U',
								[AprsComment5]='" . ECh($_REQUEST["5thAps"]) . "',
								[AprsName5]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

        ScriptRunnerUD($Script, "CM");

        echo ("<script type='text/javascript'>{ parent.msgbox('(5) Your appraisal was successfully Reviewed...', 'green'); }</script>");
        $mailer_from = "fifth person";
        $mailer_action = "review";
        include "mail_trail_send_kpi.php";

    } elseif (!$CAprsID6) {

        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
						HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '----';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '----';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET
								[AprsID3]='',
								[AprsID4]='',
								[AprsID5]='',
								[AprsID6]='',
								[LUO_HashKey]='',
								[Status]='U',
								[AprsComment6]='" . ECh($_REQUEST["6thAps"]) . "',
								[AprsName6]='" . $name_rev . "',
								[Accomp]='" . $NxtLvl_Accmp . "',
								[objectives_remark]='" . $objectivesData . "',
								[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
								[UpdatedDate]=GetDate()
								where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
								and Status <> 'D'
								";

        ScriptRunnerUD($Script, "CM");

        echo ("<script type='text/javascript'>{ parent.msgbox('(6) Your appraisal was successfully Reviewed...', 'green'); }</script>");
        $mailer_from = "sixth person";
        $mailer_action = "review";
        include "mail_trail_send_kpi.php";

    }

    //*/

    //REVIEW; Remove or clear value from LUO_HashKey
    $Script_review = "Update [KPIFinalScore] SET [Status]='U' where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "' and Status <> 'D'";
    ScriptRunnerUD($Script_review, "CM");

    //REVIEW; Unlock the buttons for Emp and Manager; That is they can start alover again.
    $Script = "Update [KPIIndvScore] SET [Status]='U' where [FinalHashKey]='" . ECh($_REQUEST["HashNum"]) . "'";
    ScriptRunnerUD($Script, "Inst");

    include '../main/prmt_blank.php';

    $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $EID . "'";
    $AprsNm = ScriptRunner($Script99, "AprsNm");
    AuditLog("INSERT", "Review Appraisal performed for " . $AprsNm);
    //AuditLog("INSERT","Review Appraisal triggered by  ".$_SESSION["StkTck"."FName"]);

}

if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "End Appraisal") {
    //----------- END no ManagerKey required
    //$Script8="SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='".ECh($_SESSION["StkTck"."HKey"])."'";
    //$MgrHashKey=ScriptRunner($Script8,"MgrHashKey");
    $Script11 = "SELECT * FROM KPIFinalScore WHERE HashKey='" . ECh($_REQUEST["HashNum"]) . "' and Status <> 'D'";
    $CAprsID3 = ScriptRunner($Script11, "AprsID3");
    $CAprsID4 = ScriptRunner($Script11, "AprsID4");
    $CAprsID5 = ScriptRunner($Script11, "AprsID5");
    $CAprsID6 = ScriptRunner($Script11, "AprsID6");
    $CAprsID7 = ScriptRunner($Script11, "AprsID7");
    $CAprsID8 = ScriptRunner($Script11, "AprsID8");
    $LUO_Num_old = ScriptRunner($Script11, "LUO_Num");
    $EID = ScriptRunner($Script11, "EID");
    $ManagerScore = ScriptRunner($Script11, "MScore");
    $MgrHashKey = "END";
    if ($final_score_setup == "2") { //If Supervisor/Direct Line Mgr totScore is selected to be Final score
        $final_score = ECh($_REQUEST["FScore2"]);
    }
    if ($CAprsID3 == "") {
        $EditID = 0;
        $total_score_current = 0;
        //--------------- loop to update indvKPI
        if ($kpi_Appr3_emp == "1") {
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $EditID = $_REQUEST["DelBox" . $php_i];
                    $Script = "Update [KPIIndvScore] set [AScoreLevel3Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                    ScriptRunnerUD($Script, "Inst");
                    $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];
                }
            }
        }
        if ($final_score_setup == "1" && $kpi_Appr3_emp == "1") { //If Last person totScore is selected to be Final score
            $final_score = $total_score_current;
        } elseif ($final_score_setup == "1" && $kpi_Appr3_emp != "1") {
            $final_score = ECh($_REQUEST["FScore2"]);
        } elseif ($final_score_setup == "3") { //Final Score==Avereage
            include 'final_score_average_function.php';
        } else {
            $final_score = $ManagerScore;
        }
        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET
		[FScore]=" . ECh($final_score) . ",
		[AprsID3]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[AprsComment3]='" . ECh($_REQUEST["3rdAps"]) . "',
		[AprsName3]='" . $name_rev . "',
		[Accomp]='" . $NxtLvl_Accmp . "',
		[objectives_remark]='" . $objectivesData . "',
		[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[LUO_HashKey]='" . $MgrHashKey . "',
		[UpdatedDate]=GetDate()
		where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
		and Status <> 'D'
		";
        ScriptRunnerUD($Script, "CM");
        echo ("<script type='text/javascript'>{ parent.msgbox('(3) Your appraisal was successfully Submited and Ended...', 'green'); }</script>");
        $mailer_from = "third person";
        $mailer_action = "end";
        include "mail_trail_send_kpi.php";
    } elseif (!$CAprsID4) {
        $EditID = 0;
        $total_score_current = 0;
        //--------------- loop to update indvKPI
        if ($kpi_Appr4_emp == "1") {
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $EditID = $_REQUEST["DelBox" . $php_i];
                    $Script = "Update [KPIIndvScore] set [AScoreLevel4Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Inst");
                    $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];
                }
            }
        }
        if ($final_score_setup == "1" && $kpi_Appr4_emp == "1") { //If Last person totScore is selected to be Final score
            $final_score = $total_score_current;
        } elseif ($final_score_setup == "1" && $kpi_Appr4_emp != "1") {
            $final_score = ECh($_REQUEST["FScore2"]);
        } elseif ($final_score_setup == "3") { //Final Score==Avereage
            include 'final_score_average_function.php';
        } else {
            $final_score = $ManagerScore;
        }
        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
		HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }
        $Script = "Update [KPIFinalScore] SET [AprsID4]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[FScore]=" . ECh($final_score) . ",
		[AprsComment4]='" . ECh($_REQUEST["4thAps"]) . "',
		[AprsName4]='" . $name_rev . "',
		[Accomp]='" . $NxtLvl_Accmp . "',
		[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[LUO_HashKey]='" . $MgrHashKey . "',
		[UpdatedDate]=GetDate()
		where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
		and Status <> 'D'
		";
        ScriptRunnerUD($Script, "CM");
        echo ("<script type='text/javascript'>{ parent.msgbox('(4) Your appraisal was successfully Submited and Ended...', 'green'); }</script>");
        $mailer_from = "fourth person";
        $mailer_action = "end";
        include "mail_trail_send_kpi.php";
    } elseif (!$CAprsID5) {
        $EditID = 0;
        $total_score_current = 0;
        //--------------- loop to update indvKPI
        if ($kpi_Appr5_emp == "1") {

            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $EditID = $_REQUEST["DelBox" . $php_i];
                    $Script = "Update [KPIIndvScore] set [AScoreLevel5Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";

                    ScriptRunnerUD($Script, "Inst");
                    $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];
                }
            }
        }
        if ($final_score_setup == "1" && $kpi_Appr5_emp == "1") { //If Last person totScore is selected to be Final score
            $final_score = $total_score_current;
        } elseif ($final_score_setup == "1" && $kpi_Appr5_emp != "1") {
            $final_score = ECh($_REQUEST["FScore2"]);
        } elseif ($final_score_setup == "3") { //Final Score==Avereage
            include 'final_score_average_function.php';
        } else {
            $final_score = $ManagerScore;
        }
        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
		HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET [AprsID5]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[FScore]=" . ECh($final_score) . ",
		[AprsComment5]='" . ECh($_REQUEST["5thAps"]) . "',
		[AprsName5]='" . $name_rev . "',
		[Accomp]='" . $NxtLvl_Accmp . "',
		[objectives_remark]='" . $objectivesData . "',
		[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[LUO_HashKey]='" . $MgrHashKey . "',
		[UpdatedDate]=GetDate()
		where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
		and Status <> 'D'
		";
        ScriptRunnerUD($Script, "CM");
        echo ("<script type='text/javascript'>{ parent.msgbox('(5) Your appraisal was successfully Submited and Ended...', 'green'); }</script>");
        $mailer_from = "fifth person";
        $mailer_action = "end";
        include "mail_trail_send_kpi.php";
    } elseif (!$CAprsID6) {
        $EditID = 0;
        $total_score_current = 0;
        if ($kpi_Appr6_emp == "1") {
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $EditID = $_REQUEST["DelBox" . $php_i];

                    $Script = "Update [KPIIndvScore] set [AScoreLevel6Comm]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Inst");
                    $total_score_current = $total_score_current + $_REQUEST["AScoreMrg_" . $EditID];
                }
            }
        }
        if ($final_score_setup == "1" && $kpi_Appr6_emp == "1") { //If Last person totScore is selected to be Final score
            $final_score = $total_score_current;
        } elseif ($final_score_setup == "1" && $kpi_Appr6_emp != "1") {
            $final_score = ECh($_REQUEST["FScore2"]);
        } elseif ($final_score_setup == "3") { //Final Score==Avereage
            include 'final_score_average_function.php';
        } else {
            $final_score = $ManagerScore;
        }
        $Script_name_rev = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where
		HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
        $name_rev = ScriptRunner($Script_name_rev, "Nm");

        $data = [];
        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? ECh($_REQUEST["mgr_accomp"][$i]) : '----';
                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? ECh($_REQUEST["apr3_accomp"][$i]) : '----';
                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? ECh($_REQUEST["apr4_accomp"][$i]) : '----';
                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? ECh($_REQUEST["apr5_accomp"][$i]) : '----';
                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? ECh($_REQUEST["apr6_accomp"][$i]) : '----';
                $data[] = ['employee_accomp' => ECh($_REQUEST["accomplishments"][$i]), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
            }
            if (count($data)) {
                $NxtLvl_Accmp = json_encode($data);
            } else {
                $NxtLvl_Accmp = null;
            }
        } else {
            $NxtLvl_Accmp = null;
        }

        $objective_data = [];
        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                if (!empty($_REQUEST["objectives"][$i])) {
                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '----';
                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '----';
                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '----';
                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '----';
                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '----';
                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                }
            }
            if (count($objective_data)) {
                $objectivesData = json_encode($objective_data);
            } else {
                $objectivesData = null;
            }
        } else {
            $objectivesData = null;
        }

        $Script = "Update [KPIFinalScore] SET [AprsID6]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[FScore]=" . ECh($final_score) . ",
		[AprsComment6]='" . ECh($_REQUEST["6thAps"]) . "',
		[AprsName6]='" . $name_rev . "',
		[Accomp]='" . $NxtLvl_Accmp . "',
		[objectives_remark]='" . $objectivesData . "',
		[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
		[LUO_HashKey]='" . $MgrHashKey . "',
		[UpdatedDate]=GetDate()
		where [HashKey]='" . ECh($_REQUEST["HashNum"]) . "'
		and Status <> 'D'
		";
        ScriptRunnerUD($Script, "CM");
        echo ("<script type='text/javascript'>{ parent.msgbox('(6) Your appraisal was successfully Submited and Ended...', 'green'); }</script>");
        $mailer_from = "sixth person";
        $mailer_action = "end";
        include "mail_trail_send_kpi.php";
    }
    include '../main/prmt_blank.php';
    $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $EID . "'";
    $AprsNm = ScriptRunner($Script99, "AprsNm");
    AuditLog("INSERT", "End Appraisal done for " . $AprsNm);
}
//}
?>


<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
<script type="text/javascript">
function GetTotal($ObjName,$ObjWhtID)
{
		$kk = document.getElementById("Wght"+$ObjName).innerHTML;
		//document.getElementById("TD"+$ObjName).innerHTML = ((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);
		document.getElementById("TD"+$ObjName).innerHTML = ((1 * document.getElementById($ObjName).value)).toFixed(2);
		document.getElementById("Wth"+$ObjWhtID).value = document.getElementById("TD"+$ObjName).innerHTML;

		//alert (document.getElementById("DelMax").value);
		document.getElementById("TOTALTDAScoreMrg").innerHTML=0;
		for ($i=1; $i<=document.getElementById("DelMax").value; $i++)
		{
			document.getElementById("TOTALTDAScoreMrg").innerHTML =
			parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML) + parseFloat(document.getElementById("Wth"+$i).value);
		}
		//Get value of total Managers score in an INPUT box
		document.getElementById("MgrTScore").value = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1);
		document.getElementById("TOTALTDAScoreMrg").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1) + '%';
//	}
}
// var total=0;
function GetTotal2($ObjName,$ObjWhtID,$weight,$quantity)
{
		var $kk = document.getElementById($ObjName).value;
		// alert($kk);
		// document.getElementById("TD"+$ObjName).innerHTML = ((parseFloat($kk)).toFixed(2);
		if ($quantity) {
			let actual= document.getElementById($ObjName+"_try").value;
			if(parseInt(actual) >= parseInt($quantity)){
					actual = $quantity;
			}
		 document.getElementById("TD"+$ObjName).innerHTML =(($weight * actual)/$quantity).toFixed(1);
		 document.getElementById($ObjName).value=(($weight * actual)/$quantity).toFixed(1);
		//  console.log($kk,document.getElementById($ObjName+"_try").value,$quantity);
		} else {
		document.getElementById("TD"+$ObjName).innerHTML =((1 * document.getElementById($ObjName).value)).toFixed(2);

		}


		 // alert(sum);
		document.getElementById("Wth"+$ObjWhtID).value = document.getElementById("TD"+$ObjName).innerHTML;

		// //alert (document.getElementById("DelMax").value);
		document.getElementById("Total").innerHTML=0;
		for ($i=1; $i<=document.getElementById("DelMax").value; $i++)
		{
		 	// document.getElementById("Total").innerHTML=parseFloat(sum)  + total;
			document.getElementById('Total').innerHTML = parseFloat(document.getElementById("Total").innerHTML) + parseFloat(document.getElementById("Wth"+$i).value)+ '%';
		}
		// //Get value of total Managers score in an INPUT box
		// document.getElementById("MgrTScore").value = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1);
		document.getElementById("Total").innerHTML = parseFloat(document.getElementById("Total").innerHTML).toFixed(1) + '%';
//	}
}
</script>

<script>
  $(function() {
    $( document ).tooltip();
  });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong{
		font-weight: 550;
	}
	[type=checkbox]+label:before, [type=checkbox]:not(.filled-in)+label:after{

	    width: 15px;
	    height: 15px;
	}

	.fieldList .form-group{
		margin-bottom: 0px;
	}
</style>
<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border tdMenu_HeadBlock">
				<div class="row">
					<div class="col-md-12 subHeader text-center" style="font-size: 13px;">
						<a style="color: #000;" href="ApprStartComment.php"><span class="fa fa-angle-double-left">&nbsp;&nbsp; GO BACK</span></a>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<?php
if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
    echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="' . $_REQUEST["AcctNo"] . '" />';
    if (isset($_REQUEST["AcctNo"])) {
        $EmpID = ECh($_REQUEST["AcctNo"]);
        $EditID = ECh($_REQUEST["AcctNo"]);
    }

} else {
    echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="';if (isset($_REQUEST["ApprEmpID"])) {echo $_REQUEST["ApprEmpID"];}
    echo '" />';
    if (isset($_REQUEST["ApprEmpID"])) {
        $EmpID = $_REQUEST["ApprEmpID"];
        $EditID = $_REQUEST["ApprEmpID"];
    }
}
//$Script="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.HashKey='".$EditID."'";
//echo ScriptRunner($Script,"Nm");
?>
					<?php
if (isset($_REQUEST["HashNum"])) {
    //    print "Hello";

    $Script15 = "SELECT * FROM KPIFinalScore WHERE HashKey='" . $_REQUEST["HashNum"] . "' and Status <> 'D'";
    $EID = ScriptRunner($Script15, "EID");

    //print $EID;

    $Script_name2 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $EID . "'";
    $emp_name = ScriptRunner($Script_name2, "Nm");
}
$empl_id = ECh($_REQUEST['AcctNo']);
$Script_sel = "Select * from KPIFinalScore where LUO_HashKey ='" . ECh($_SESSION["StkTck" . "HKey"]) . "' AND EID='$empl_id' and Status <> 'D'";

$LUO_HashKey_sel = ScriptRunner($Script_sel, "LUO_HashKey");
$Script_apsid = "Select * from KPIFinalScore WHERE [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $_REQUEST["AcctNo"] . "' and Status <> 'D'";
$Aprs_ID3 = ScriptRunner($Script_apsid, "AprsID3");
$Aprs_ID4 = ScriptRunner($Script_apsid, "AprsID4");
$Aprs_ID5 = ScriptRunner($Script_apsid, "AprsID5");
$Aprs_ID6 = ScriptRunner($Script_apsid, "AprsID6");
?>
					<?php
//if (!isset($_REQUEST["AcctNo"]))
//{exit;}
//GET THE ID FOR THE CURRENT OPEN APPRAISAL FOR THE SELECTED EMPLOYEE
?>



 <?php
$all_dev = [];
$query = "select [KPI],[KRA],[Status],[Score_Type]
      ,[Quantity_Value] from KPICore where Status in ('A') and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $EditID . "')";
//    "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."' and Status in('A') )))";
$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
if ($conn2 === false) {
    echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");

}
$res2 = sqlsrv_query($conn2, $query);
while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_ASSOC)) {
    //   $all_dev[trim($row['KPI'])]=$row;
    $all_dev[ECh(trim($row['KPI']))] = $row;

}

?>













					<div class="col-md-12 text-center">
				        <?php
if (isset($_REQUEST["HashKey"])) {
    $Script_name = "SELECT (SName+' '+FName+' '+ONames) Nm FROM EmpTbl WHERE HashKey='" . $_REQUEST["HashKey"] . "'";
    $EmpName = ScriptRunner($Script_name, "Nm");
    print "Employee/Apraisee name: <strong>" . $EmpName . "</strong>";
}
?>
					</div>
    				<br />
    				<div class="col-md-12" style="overflow-x:scroll">
						<div class="ViewPatch">
							<table class="table table-responsive table-bordered" id="table">
								<thead>
									<tr class="">
										<th>Area of Focus</th>
										<th>Sub Area of Focus</th>
										<th> Departmental Objectives</th>
										<th>Individual Performance Objectives</th>
										<th>Weightage</th>
										<th>Employee</th>
										<th>Supervisor</th>
										<!-- ================================== extend here ========================== -->
										<?php
include 'level_check_mgr.php';
if ($appr3_emp == '1') {
    print '<th>Level 3 Appraiser<br>
											            Score
											          </th>
											          ';
}
if ($appr4_emp == '1') {
    print '<th>Level 4 Appraiser<br>
											            Score
											          </th>';
}
if ($appr5_emp == '1') {
    print '<th>Level 5 Appraiser<br>
											            Score
											          </th>';
}
if ($appr6_emp == '1') {
    print '<th>Level 6 Appraiser<br>
											            Score
											          </th>';
}
?>
										<!-- ========================================================================= -->
										<th width="10%" align="center" valign="middle"  scope="col"><span class="TinyTextTightBold">Appraiser Score</span></th>
										<th width="5%" align="center" valign="middle" scope="col">
											<!-- <img src="../images/icon_print_.gif" title="Print blank paper form" name="EditEdu<?php echo $Del; ?>" height="10" border="0" id="EditEdu<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('View&nbsp;Employee&nbsp;KPI/KRA','kpi/Rpt_KPIDtl_Blk.php?APPID=<?php echo ScriptRunner($Script, "AQuartID"); ?>&HID=<?php echo $EmpID; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/icon_print_.gif',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" /> -->
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
$Del = 0;
//$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");

/*
$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and
AQuartID =(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),MDate,106)+' 23:59:59')
and
AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");
$sel_id
 */

/*
$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE
AQuartID = '$AQuartID_Val'
and
AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");
 */

//$Script_sel="Select * from KPIFinalScore where LUO_HashKey ='a28a1e1e8667096c9e98c942e1d65406'";
//$Script_sel="Select * from KPIFinalScore where LUO_HashKey ='".ECh($_SESSION["StkTck"."HKey"])."' AND EID='$EID'";
$empl_id = ECh($_REQUEST['AcctNo']);
$Script_sel = "Select * from KPIFinalScore where LUO_HashKey ='" . ECh($_SESSION["StkTck" . "HKey"]) . "' AND EID='$empl_id' and Status <> 'D' AND AQuartID='" . ECh($_REQUEST['AQuartID']) . "'";

$LUO_HashKey_sel = ScriptRunner($Script_sel, "LUO_HashKey");
$AQuartID_sel = ScriptRunner($Script_sel, "AQuartID");
$KPISetting_hashkey = ScriptRunner($Script_sel, "KPISetting_hashkey");
$final_score_value = ScriptRunner($Script_sel, "FScore");

$row1_col1 = ScriptRunner($Script_sel, "row1_col1");
$row1_col2 = ScriptRunner($Script_sel, "row1_col2");
$row1_col3 = ScriptRunner($Script_sel, "row1_col3");
$row1_col4 = ScriptRunner($Script_sel, "row1_col4");

$row2_col1 = ScriptRunner($Script_sel, "row2_col1");
$row2_col2 = ScriptRunner($Script_sel, "row2_col2");
$row2_col3 = ScriptRunner($Script_sel, "row2_col3");
$row2_col4 = ScriptRunner($Script_sel, "row2_col4");

$row3_col1 = ScriptRunner($Script_sel, "row3_col1");
$row3_col2 = ScriptRunner($Script_sel, "row3_col2");
$row3_col3 = ScriptRunner($Script_sel, "row3_col3");
$row3_col4 = ScriptRunner($Script_sel, "row3_col4");

$row4_col1 = ScriptRunner($Script_sel, "row4_col1");
$row4_col2 = ScriptRunner($Script_sel, "row4_col2");
$row4_col3 = ScriptRunner($Script_sel, "row4_col3");
$row4_col4 = ScriptRunner($Script_sel, "row4_col4");

$row5_col1 = ScriptRunner($Script_sel, "row5_col1");
$row5_col2 = ScriptRunner($Script_sel, "row5_col2");
$row5_col3 = ScriptRunner($Script_sel, "row5_col3");
$row5_col4 = ScriptRunner($Script_sel, "row5_col4");

$row6_col1 = ScriptRunner($Script_sel, "row6_col1");
$row6_col2 = ScriptRunner($Script_sel, "row6_col2");
$row6_col3 = ScriptRunner($Script_sel, "row6_col3");
$row6_col4 = ScriptRunner($Script_sel, "row6_col4");

$row7_col1 = ScriptRunner($Script_sel, "row7_col1");
$row7_col2 = ScriptRunner($Script_sel, "row7_col2");
$row7_col3 = ScriptRunner($Script_sel, "row7_col3");
$row7_col4 = ScriptRunner($Script_sel, "row7_col4");

$row8_col1 = ScriptRunner($Script_sel, "row8_col1");
$row8_col2 = ScriptRunner($Script_sel, "row8_col2");
$row8_col3 = ScriptRunner($Script_sel, "row8_col3");
$row8_col4 = ScriptRunner($Script_sel, "row8_col4");

$row9_col1 = ScriptRunner($Script_sel, "row9_col1");
$row9_col2 = ScriptRunner($Script_sel, "row9_col2");
$row9_col3 = ScriptRunner($Script_sel, "row9_col3");
$row9_col4 = ScriptRunner($Script_sel, "row9_col4");

$row10_col1 = ScriptRunner($Script_sel, "row10_col1");
$row10_col2 = ScriptRunner($Script_sel, "row10_col2");
$row10_col3 = ScriptRunner($Script_sel, "row10_col3");
$row10_col4 = ScriptRunner($Script_sel, "row10_col4");

//$EmpIdSel=ScriptRunner($Script_sel,"EID");
//print $EmpIdSel;
//print ECh($_SESSION["StkTck"."HKey"]);

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Logical error check
/*
$Script_sel="Select * from KPIIndvScore where AQuartID = '$AQuartID_sel' and AID = '$KPISetting_hashkey' AND AddedBy='$EID'";

$AddedBy=ScriptRunner($Script_sel,"AddedBy");
if(!$AddedBy){
exit('Record not found');
}
 */
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE AQuartID = '$AQuartID_sel' and AID = '$KPISetting_hashkey' AND status <> 'D' AND AddedBy='$EID'  ORDER BY KRA_Group");
$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE AQuartID = '$AQuartID_sel' and AID = '$KPISetting_hashkey' AND status <> 'D' ORDER BY KRA_Group");

include '../login/dbOpen2.php';

$prevValue = "";

while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    $UpdUser = $row2['UpdatedBy'];
    $AuthRec = $row2['Status'];
    $AQuartID_Val = $row2['AQuartID'];
    $FHashKey = $row2['FinalHashKey'];
    $KRA_Group_value = $row2['KRA_Group'];
    //new function, for secction comment

    if ((isset($KRA_Group_value)) && ($KRA_Group_value != $prevValue)) {

        //------ Option to display Section Comment
        if ($appr2_section == '1') {

            $icon = "<button type=\"button\"
														title=\"Edit Record\"
														name=\"EditEdu" . $Del . "\"
														id=\"EditEdu" . $Del . "\"
														onClick=\"parent.ShowDisp('KPI&nbsp;Section&nbsp;Comment','kpi/KPI_CommSection.php?kra_group=" . $KRA_Group_value . "&RecStatus=U&EmpTyp=Others&HID=" . $FHashKey . "',320,600,'Yes')\"
														class=\"btn btn-default btn-sm\" style=\"line-height: 17px\">
														<i class=\"fa fa-edit\"></i></button>";

        } else {
            $icon = "";
        }
        //----------------------------------------

        print "
												<tr class=\"tdMenu_HeadBlock_Light\">
													<td align=\"center\" valign=\"middle\" colspan=\"11\" style=\"padding-bottom: 5px; padding-top:5px;\"><strong>" . $icon . " $KRA_Group_value</strong></td>
												</tr>
												";
        $prevValue = $KRA_Group_value;
    }
    ?>
									<tr>
										<?php
//This makes the check boxes into hidden fields
    echo '<input name="' . ("DelBox" . $Del) . '" type="hidden" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" />';
    ?>
										<td>
											<?php
echo "<strong>" . (trim($row2['KRA'])) . "</strong>";
    // echo "<br/>".(trim($row2['ARemark']));
     ?>
										</td>
										<td>
											<?php
echo "<strong>" . (trim($row2['KRA_Sub_Area'])) . "</strong>";
    // echo "<br/>".(trim($row2['ARemark']));
     ?>
										</td>

	            						<td>
                                            <?php
echo (trim($row2['KPI'])) . "<br/>";
    $measures = '';
    for ($i = 0; $i <= 6; $i++) {
        if (trim($row2['Opt' . $i])) {
            $measures .= trim($row2['Opt' . $i]) . "; ";
        }
    }
    if ($measures) {
        echo 'Measures: ' . $measures;
    }
    ?>
                                        </td>

										<td>
											<?php
// echo "<strong>".(trim($row2['KRA']))."</strong>";
    // echo "<br/>".(trim($row2['ARemark']));
    echo "<b>" . (trim($row2['ARemark'])) . "</b>";
    ?>
										</td>
										<td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "WghtAScoreGrpMrg_" . $row2['HashKey']; ?>">	<?php echo (trim($row2['Weightage']));
    $TWeightage = $TWeightage + $row2['Weightage']; ?>
										</td>
										<td align="center" valign="middle" class="subHeader" scope="col">
											<?php
echo number_format($row2['AScoreEmp'], 1, '.', '');
    $TotEmp = $TotEmp + number_format($row2['AScoreEmp'], 1, '.', '');
    ?>
										</td>
										<td align="center" valign="middle" class="subHeader" scope="col">
											<?php
$TotMgr = $TotMgr + number_format($row2['AScoreMrg'], 1, '.', '');
    $Total_Mgr_score = $Total_Mgr_score + $row2['AScoreMrg'];
    echo number_format($row2['AScoreMrg'], 1, '.', '');
    ?>
										</td>
										<!-- ============================extend here =========================== -->
										<?php
$Script_apsid = "Select * from KPIFinalScore WHERE [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $_REQUEST["AcctNo"] . "' and Status <> 'D'";

    $Aprs_ID3 = ScriptRunner($Script_apsid, "AprsID3");
    $Aprs_ID4 = ScriptRunner($Script_apsid, "AprsID4");
    $Aprs_ID5 = ScriptRunner($Script_apsid, "AprsID5");
    $Aprs_ID6 = ScriptRunner($Script_apsid, "AprsID6");
    include 'level_check_mgr.php';
    if ($appr3_emp == '1') {
        $Script = ("Select * from KPIStart where HashKey ='" . $_REQUEST['AQuartID'] . "'");
        $GradeType = ScriptRunner($Script, "ScoreType");
        if (($LUO_HashKey_sel == $_SESSION["StkTck" . "HKey"]) && ($Aprs_ID3 == "")) {

            $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];
            if (!isset($amends)) {

                echo ("<script type='text/javascript'>{ parent.msgbox('Appraisal records has been modified. Kindly contact HR department to restart this appariasal record. ', 'red'); }</script>");
                exit;
            }

            if ($amends === "--") {

                $DrpCnt = $DrpCnt + 1;
                //echo $DrpCnt;
                echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";
                echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                echo '<option selected value="0">--</option>';
                $SelID = $row2['AScoreLevel3Comm'];
                if ($GradeType == "Tags") {
                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';
                } else {
                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;
                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel3 = $TotAScoreLevel3 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }
                }
                echo '</select>	</td>';
                $Aprs_ID3 = "set";

            } else {

                $DrpCnt = $DrpCnt + 1;
                if ($amends === "Tags") {
                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel3Comm'];

                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';

                    echo '</select>	</td>';
                    $Aprs_ID3 = "set";

                } else if ($amends === "Values") {

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel3Comm'];

                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;
                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel3 = $TotAScoreLevel3 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }

                    echo '</select>	</td>';
                    $Aprs_ID3 = "set";
                } else if ($amends === "Quantity") {

                    $SelID = $row2['AScoreLevel3Comm'];
                    if ($SelID) {
                        $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];

                    }
                    echo '<td><div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                    echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreMrg_' . $row2['HashKey'] . '_try" class="" id="AScoreMrg_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div></td> ';
                    $Aprs_ID3 = "set";

                    // $Aprs_ID3="set";

                }

            }

        } else {
            print '
													<td align="center" valign="middle" class="subHeader" scope="col">';
            $TotAScoreLevel3 = $TotAScoreLevel3 + number_format($row2['AScoreLevel3Comm'], 1, '.', '');
            echo number_format($row2['AScoreLevel3Comm'], 1, '.', '');
            print '</td>';
        }

    }

    if ($appr4_emp == '1') {
        if (($LUO_HashKey_sel == $_SESSION["StkTck" . "HKey"]) && (($Aprs_ID4 == "") && ($Aprs_ID3 != "set"))) {

            $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];
            if ($amends === "--") {

                $DrpCnt = $DrpCnt + 1;
                //echo $DrpCnt;
                echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                echo '<option selected value="0">--</option>';
                $SelID = $row2['AScoreLevel4Comm'];
                if ($GradeType == "Tags") {
                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';
                } else {
                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;
                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel4 = $TotAScoreLevel4 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }
                }
                echo '</select>	</td>';
                $Aprs_ID4 = "set";

            } else {
                $DrpCnt = $DrpCnt + 1;
                if ($amends === "Tags") {

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel4Comm'];

                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';

                    echo '</select>	</td>';
                    $Aprs_ID4 = "set";

                } else if ($amends === "Values") {
                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel4Comm'];

                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;
                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel4 = $TotAScoreLevel4 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }

                    echo '</select>	</td>';
                    $Aprs_ID4 = "set";

                } else if ($amends === "Quantity") {

                    $SelID = $row2['AScoreLevel4Comm'];
                    if ($SelID) {
                        $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];
                    }
                    echo '<td><div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                    echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreMrg_' . $row2['HashKey'] . '_try" class="" id="AScoreMrg_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div></td> ';
                    $Aprs_ID4 = "set";

                }

            }

        } else {
            print '
													<td align="center" valign="middle" class="subHeader" scope="col">';
            $TotAScoreLevel4 = $TotAScoreLevel4 + number_format($row2['AScoreLevel4Comm'], 1, '.', '');
            echo number_format($row2['AScoreLevel4Comm'], 1, '.', '');
            print '</td>';
        }
    }

    if ($appr5_emp == '1') {
        if (($LUO_HashKey_sel == $_SESSION["StkTck" . "HKey"]) && (($Aprs_ID5 == "") && ($Aprs_ID4 != "set" && $Aprs_ID3 != "set"))) {

            $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];
            if ($amends === "--") {

                $DrpCnt = $DrpCnt + 1;
                //echo $DrpCnt;
                echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '"  onChange="' . $kk . '" >';
                echo '<option selected value="0">--</option>';
                $SelID = $row2['AScoreLevel5Comm'];
                if ($GradeType == "Tags") {
                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';
                } else {
                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;

                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel5 = $TotAScoreLevel5 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }
                }
                echo '</select>	</td>';
                $Aprs_ID5 = "set";

            } else {
                $DrpCnt = $DrpCnt + 1;
                if ($amends === "Tags") {

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '"  onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel5Comm'];

                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';

                    echo '</select>	</td>';
                    $Aprs_ID5 = "set";

                } else if ($amends === "Values") {
                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '"  onChange="' . $kk . '" >';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel5Comm'];

                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;

                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel5 = $TotAScoreLevel5 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }

                    echo '</select>	</td>';
                    $Aprs_ID5 = "set";
                } else if ($amends === "Quantity") {
                    $SelID = $row2['AScoreLevel5Comm'];
                    if ($SelID) {
                        $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];
                    }
                    echo '<td><div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                    echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreMrg_' . $row2['HashKey'] . '_try" class="" id="AScoreMrg_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div></td> ';
                    $Aprs_ID5 = "set";

                }

            }

        } else {
            print '
													<td align="center" valign="middle" class="subHeader" scope="col">';
            $TotAScoreLevel5 = $TotAScoreLevel5 + number_format($row2['AScoreLevel5Comm'], 1, '.', '');
            echo number_format($row2['AScoreLevel5Comm'], 1, '.', '');
            print '</td>';
        }
    }

    if ($appr6_emp == '1') {
        if (($LUO_HashKey_sel == $_SESSION["StkTck" . "HKey"]) && (($Aprs_ID6 == "") && ($Aprs_ID5 != "set" && $Aprs_ID4 != "set" && $Aprs_ID3 != "set"))) {

            $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];
            if ($amends === "--") {
                $DrpCnt = $DrpCnt + 1;
                //echo $DrpCnt;
                echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";

                echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '">';
                echo '<option selected value="0">--</option>';
                $SelID = $row2['AScoreLevel6Comm'];
                if ($GradeType == "Tags") {
                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';
                } else {
                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;

                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel6 = $TotAScoreLevel6 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }
                }
                echo '</select>	</td>';

            } else {
                $DrpCnt = $DrpCnt + 1;
                if ($amends === "Tags") {
                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";

                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '">';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel6Comm'];
                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                    include '../login/dbOpen3.php';
                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
                    }
                    include '../login/dbClose3.php';

                    echo '</select>	</td>';

                } else if ($amends === "Values") {
                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                    // $kk ="GetTotal2('AScoreMrg_".$row2['HashKey']."')";

                    echo '<td align="center" valign="middle" class="subHeader" scope="col">';
                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="TextBoxText_Short" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '">';
                    echo '<option selected value="0">--</option>';
                    $SelID = $row2['AScoreLevel6Comm'];

                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                    $incrementsBy = ($val) ? $val : 0.5;

                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                        if ($SelID == $php_i) {
                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                            $TotAScoreLevel6 = $TotAScoreLevel6 + $php_i;
                        } else {
                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                        }
                    }
                    echo '</select>	</td>';

                } else if ($amends === "Quantity") {
                    $SelID = $row2['AScoreLevel6Comm'];
                    if ($SelID) {
                        $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];
                    }
                    echo '<td><div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';

                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                    $kk = "GetTotal2('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                    echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreMrg_' . $row2['HashKey'] . '_try" class="" id="AScoreMrg_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div></td> ';
                    $Aprs_ID6 = "set";

                }

            }

        } else {
            print '
													<td align="center" valign="middle" class="subHeader" scope="col">';
            $TotAScoreLevel6 = $TotAScoreLevel6 + number_format($row2['AScoreLevel6Comm'], 1, '.', '');
            echo number_format($row2['AScoreLevel6Comm'], 1, '.', '');
            print '</td>';
        }
    }
    ?>
										<!-- =================================================================== -->
										<td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "TDAScoreMrg_" . $row2['HashKey']; ?>">0.0</td>
							            <td>
							            	<?php
if (strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
        $bg = 'info';
    } elseif (strlen(trim($row2['MComm'])) > 0 && !strlen(trim($row2['EComm'])) > 0) {
        $bg = 'orange';
    } elseif (!strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
        $bg = 'danger';
    } else {
        $bg = 'default';
    }
    ?>
							            	<button type="button" class="btn btn-<?php echo $bg ?> btn-sm"  alt="Edit Record" name="EditEdu<?php echo $Del; ?>" id="EditEdu<?php echo $Del; ?>" onClick="parent.ShowDisp('KPI&nbsp;Comment','kpi/KPI_Comm.php?EmpTyp=Others&RecStatus=U&EmpIDHash=<?php echo $EditID; ?>&HID=<?php echo $row2['HashKey']; ?>',320,600,'Yes')">
							            		<span class="fa fa-edit"></span>
							            	</button>
										</td>
									</tr>
									<?php
/*
    $kk_all=$kk_all."<script type='text/javascript'>{".$kk.";}</script>";
     */
    $Script8 = "Select * from KPIFinalScore where [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $_REQUEST["AcctNo"] . "' and Status <> 'D'";
    $LUO_Num_selected = ScriptRunner($Script8, "LUO_Num");
    $HashKey_fkpi = ScriptRunner($Script8, "HashKey");

    $Script_name_main = "Select * from KPIFinalScore WHERE HashKey ='" . $HashKey_fkpi . "' and Status <> 'D'";
    $AprsID1 = ScriptRunner($Script_name_main, "EID");
    $AprsID2 = ScriptRunner($Script_name_main, "MID");

    $Script_name1 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $AprsID1 . "'";
    $name1 = ScriptRunner($Script_name1, "Nm");

    $Script_name2 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $AprsID2 . "'";
    $name2 = ScriptRunner($Script_name2, "Nm");
    ?>
									<?php
$TotMed = $TotMed + number_format($row2['Weightage'], 1, '.', '');
}
include '../login/dbClose2.php';
?>
									<tr>
										<td colspan="3" align="right" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col" >Score:</td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col"></td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">&nbsp;<?php echo $TWeightage; ?>%</td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col" >
											<?php echo $TotEmp . "%";
echo '<input name="FScore1" id="FScore2" type="hidden" value="' . $TotEmp . '">';
?>
										</td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col" >
											<?php echo $TotMgr;
echo '<input name="FScore2" id="FScore2" type="hidden" value="' . $TotMgr . '">';
?>%
										</td>
					         		   <!-- ============================total extend here =========================== -->
										<?php
include 'level_check_mgr.php';
if ($appr3_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    //if(!$TotAScoreLevel3==""){
    echo number_format($TotAScoreLevel3, 1, '.', '') . "%";
    echo '<input name="FScore3" id="FScore3" type="hidden" value="' . $TotAScoreLevel3 . '">';

    //}
    print '</td>';
}

if ($appr4_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel4, 1, '.', '') . "%";
    echo '<input name="FScore4" id="FScore4" type="hidden" value="' . $TotAScoreLevel4 . '">';

    print '</td>';
}

if ($appr5_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel5, 1, '.', '') . "%";
    echo '<input name="FScore5" id="FScore5" type="hidden" value="' . $TotAScoreLevel5 . '">';

    print '</td>';
}

if ($appr6_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel6, 1, '.', '') . "%";
    echo '<input name="FScore6" id="FScore6" type="hidden" value="' . $TotAScoreLevel6 . '">';

    print '</td>';
}
?>
						                <!-- =================================================================== -->
						            	<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" id="Total" scope="col">0.0</td>
						            	<td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col"></td>
						          	</tr>
						        </tbody>
						    </table>
						</div>
					</div>
					<div class="col-md-12">
    					<div class="row">
    						<div class="col-md-1"></div>
    						<div class="col-md-10">
								<?php
include "final_score_total_2nd.php";
/*
$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AQuartID='$AQuartID_sel'
and AID='$KPISetting_hashkey' ORDER BY KRA_Group");

include '../login/dbOpen2.php';

$prevValue = "";
$main_total=0;
$w_main_total=0;

if($final_score_setup=="1"){
$final_score_type="Last Appraiser";
}elseif($final_score_setup=="2"){
$final_score_type="Supervisor/Direct Line Manager";
}elseif($final_score_setup=="3"){
$final_score_type="Average Score of all Appraiser";
}

print "<table width=\"100%\" border=\"1\" cellpadding=\"4\" cellspacing=\"0\">
<tr>
<td>OVERALL RATING</td>
<td>WEIGHTAGE</td>
<td>FINAL SCORE($final_score_type)</td>
</tr>";

while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
$Del = $Del + 1;
//$UpdUser = $row2['UpdatedBy'];
$AuthRec = $row2['Status'];
$AQuartID_Val = $row2['AQuartID'];

$KRA_Group_value = $row2['KRA_Group'];
//records=$row2['KRA_Group'];

if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)&&($KRA_Group_value != "--")){

$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".$KRA_Group_value."'
AND Status <>'D' and AQuartID='$AQuartID_sel' and AID='$KPISetting_hashkey'");

$total_sum = ScriptRunner($Script,"TotMgrScore");
$total_weightage = ScriptRunner($Script,"TotWeightage");
//----------------------------

print "
<tr>
<td><b>$KRA_Group_value</b></td>
<td>$total_weightage</td>
<td></td>
</tr>
";

$prevValue = $KRA_Group_value;
$main_total=$main_total+$total_sum;
$w_main_total=$w_main_total+$total_weightage;
}

}
print "
<tr>
<td><b>TOTAL</b></td>
<td><b>$w_main_total</b></td>
<td><b>$final_score_value</b></td>
</tr>
";

include '../login/dbClose2.php';
print "</table>";

 */
?>
								<?php
/*
$Script="Select UpdatedBy, EmpUpdCnt, MgrUpdCnt from KPIFinalScore where EID='".$EmpID."' and [AQuartID]='".$AQuartID_Val."'";
$MaxIter = ScriptRunner($Script,"MgrUpdCnt");
$MaxIter_count = $MaxIter;

if($MaxIter==""){
$MaxIter = 0;
}
$Script="Select ACount from [KPIStart] where [HashKey]='".$AQuartID_Val."'";
$MaxIterMax=ScriptRunner($Script,"ACount");

if ($MaxIterMax > 0)
{$MaxIter = "Update Count: [".$MaxIter." of ". $MaxIterMax."]";}
else {$MaxIter="Update Count: Unlimited";}

echo $MaxIter;
if($MaxIter_count==""){
$MaxIter_count = 0;
}

$alert = "<font size=\"3\"<b>You can update your record a maximum of ".$MaxIterMax." unique times. You have updated (";
$alert .= $MaxIter_count." out of ".$MaxIterMax.")</b></font>";

error_reporting(E_ERROR | E_PARSE);
if((!$_REQUEST['button'])&&(!$_REQUEST["FAction"])){
echo ("<script type='text/javascript'>{parent.msgbox('$alert','red');}</script>");
}
 */

$Script8 = "Select * from KPIFinalScore where LUO_HashKey ='" . ECh($_SESSION["StkTck" . "HKey"]) . "' AND EID='$empl_id' and Status <> 'D' AND AQuartID='" . ECh($_REQUEST['AQuartID']) . "'";
$LUO_Num_selected = ScriptRunner($Script8, "LUO_Num");
$HashKey_fkpi = ScriptRunner($Script8, "HashKey");

$Script_name_main = "Select * from KPIFinalScore WHERE HashKey ='" . $HashKey_fkpi . "' and Status <> 'D'";
$AprsID1 = ScriptRunner($Script_name_main, "EID");
$AprsID2 = ScriptRunner($Script_name_main, "MID");

$Script_name1 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $AprsID1 . "'";
$name1 = ScriptRunner($Script_name1, "Nm");

$Script_name2 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $AprsID2 . "'";
$name2 = ScriptRunner($Script_name2, "Nm");
if (!isset($EmpID) || !isset($AQuartID_Val)) {
    exit;
}
$Script2 = "Select * from KPIFinalScore WHERE HashKey ='" . $HashKey_fkpi . "' AND EID='$AprsID1' and Status <> 'D'";
$selMID = ScriptRunner($Script2, "MID");

$AprsID3 = ScriptRunner($Script2, "AprsID3");
$AprsName33 = ScriptRunner($Script2, "AprsName3");
$AprsComment3 = ScriptRunner($Script2, "AprsComment3");

$AprsID4 = ScriptRunner($Script2, "AprsID4");
$AprsName44 = ScriptRunner($Script2, "AprsName4");
$AprsComment4 = ScriptRunner($Script2, "AprsComment4");

$AprsID5 = ScriptRunner($Script2, "AprsID5");
$AprsName55 = ScriptRunner($Script2, "AprsName5");
$AprsComment5 = ScriptRunner($Script2, "AprsComment5");

$AprsID6 = ScriptRunner($Script2, "AprsID6");
$AprsName66 = ScriptRunner($Script2, "AprsName6");
$AprsComment6 = ScriptRunner($Script2, "AprsComment6");

$AprsID7 = ScriptRunner($Script2, "AprsID7");
$AprsName77 = ScriptRunner($Script2, "AprsName7");
$AprsComment7 = ScriptRunner($Script2, "AprsComment7");

$AprsID8 = ScriptRunner($Script2, "AprsID8");
$AprsName88 = ScriptRunner($Script2, "AprsName8");
$AprsComment8 = ScriptRunner($Script2, "AprsComment8");

$HashKey_sel = ScriptRunner($Script2, "HashKey");
$num_appraisers = ScriptRunner($Script2, "LUO_Num");
$SubmittedBy = ScriptRunner($Script2, "SubmittedBy");

print "<input name=\"HashNum\" id=\"HashNum\" type=\"hidden\" value=\"$HashKey_sel\" />";

//if(!$AprsID3){

$button_message = "";
if ((!$AprsID3) && ((isset($AprsID4)) || (isset($AprsID5)) || (isset($AprsID6)) || (isset($AprsID7)) || (isset($AprsID8)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = '';
    $ReOly4 = ' readonly="readonly" ';
    $ReOly5 = ' readonly="readonly" ';
    $ReOly6 = ' readonly="readonly" ';
    $ReOly7 = ' readonly="readonly" ';
    $ReOly8 = ' readonly="readonly" ';
    // var_dump($kpi_Appr4_emp);
    // var_dump($kpi_Appr4_deliv);
    // die();

    if ((($kpi_Appr4_emp == "") && ($kpi_Appr4_deliv == "") && ($kpi_Appr4_comment == "") && ($kpi_Appr4_section_comment == "")) || ($next_manager_hash == "--")) {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\"/>
												 ";
    } else {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\"/>
												 ";
    }

} elseif ((!$AprsID4) && ((isset($AprsID3)) || (isset($AprsID5)) || (isset($AprsID6)) || (isset($AprsID7)) || (isset($AprsID8)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = ' readonly="readonly" ';
    $ReOly4 = '';
    $ReOly5 = ' readonly="readonly" ';
    $ReOly6 = ' readonly="readonly" ';
    $ReOly7 = ' readonly="readonly" ';
    $ReOly8 = ' readonly="readonly" ';

    if ((($kpi_Appr5_emp == "") && ($kpi_Appr5_deliv == "") && ($kpi_Appr5_comment == "") && ($kpi_Appr5_section_comment == "")) || ($next_manager_hash == "--")) {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    } else {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    }
} elseif ((!$AprsID5) && ((isset($AprsID3)) || (isset($AprsID4)) || (isset($AprsID6)) || (isset($AprsID7)) || (isset($AprsID8)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = ' readonly="readonly" ';
    $ReOly4 = ' readonly="readonly" ';
    $ReOly5 = '';
    $ReOly6 = ' readonly="readonly" ';
    $ReOly7 = ' readonly="readonly" ';
    $ReOly8 = ' readonly="readonly" ';

    if ((($kpi_Appr6_emp == "") && ($kpi_Appr6_deliv == "") && ($kpi_Appr6_comment == "") && ($kpi_Appr6_section_comment == "")) || ($next_manager_hash == "--")) {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\"/>
												 ";
    } else {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\"/>
												 ";
    }

} elseif ((!$AprsID6) && ((isset($AprsID3)) || (isset($AprsID4)) || (isset($AprsID5)) || (isset($AprsID7)) || (isset($AprsID8)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = ' readonly="readonly" ';
    $ReOly4 = ' readonly="readonly" ';
    $ReOly5 = ' readonly="readonly" ';
    $ReOly6 = '';
    $ReOly7 = ' readonly="readonly" ';
    $ReOly8 = ' readonly="readonly" ';

    if ((($kpi_Appr7_emp == "") && ($kpi_Appr7_deliv == "") && ($kpi_Appr7_comment == "")) || ($next_manager_hash == "--")) {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    } else {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    }

} elseif ((!$AprsID7) && ((isset($AprsID3)) || (isset($AprsID4)) || (isset($AprsID6)) || (isset($AprsID5)) || (isset($AprsID8)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = ' readonly="readonly" ';
    $ReOly4 = ' readonly="readonly" ';
    $ReOly5 = ' readonly="readonly" ';
    $ReOly6 = ' readonly="readonly" ';
    $ReOly7 = '';
    $ReOly8 = ' readonly="readonly" ';

    if ($num_appraisers == "7") {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    } else {
        $button_message = "
												 <input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
												 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 <input type=\"button\" name=\"button3\" id=\"button3\" value=\"END Appraisal\" class=\"btn btn-danger btn-sm\" onClick=\"YesNo3('End Appraisal', 'End Appraisal')\" />
												 ";
    }

} elseif ((!$AprsID8) && ((isset($AprsID3)) || (isset($AprsID4)) || (isset($AprsID6)) || (isset($AprsID7)) || (isset($AprsID5)))) {
    $ReOly1 = ' readonly="readonly" ';
    $ReOly2 = ' readonly="readonly" ';
    $ReOly3 = ' readonly="readonly" ';
    $ReOly4 = ' readonly="readonly" ';
    $ReOly5 = ' readonly="readonly" ';
    $ReOly6 = ' readonly="readonly" ';
    $ReOly7 = ' readonly="readonly" ';
    $ReOly8 = '';

}

/*
if(isset($HashKey_sel)&&($SubmittedBy==$AprsID3)){
$ReOly1 = ' readonly="readonly" ';
$ReOly2 = ' readonly="readonly" ';
$ReOly3 = '';
$ReOly4 = ' readonly="readonly" ';
$ReOly5 = ' readonly="readonly" ';
$ReOly6 = ' readonly="readonly" ';
$ReOly7 = ' readonly="readonly" ';
$ReOly8 = 'readonly="readonly ';

$button_message = "
<input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
";

}elseif(isset($HashKey_sel)&&($SubmittedBy==$AprsID4)){
$ReOly1 = ' readonly="readonly" ';
$ReOly2 = ' readonly="readonly" ';
$ReOly3 = ' readonly="readonly" ';
$ReOly4 = '';
$ReOly5 = ' readonly="readonly" ';
$ReOly6 = ' readonly="readonly" ';
$ReOly7 = ' readonly="readonly" ';
$ReOly8 = 'readonly="readonly ';

$button_message = "
<input type=\"submit\" name=\"button2\" id=\"button2\" value=\"Review Appraisal\" class=\"btn btn-danger btn-sm\" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=\"submit\" name=\"button1\" id=\"button1\" value=\"SUBMIT TO NEXT LEVEL APPRAISER\" class=\"btn btn-danger btn-sm\" />
";
}
 */

if ($button_message == "") {
    echo ("<script type='text/javascript'>{ parent.msgbox('<b>No pending Appraisal is awaiting your comment from this Employee</b>', 'green'); }</script>");
    exit;
}

/*
$ReOly1 = '';
$ReOly2 = '';
$ReOly3 = '';
$ReOly4 = '';
$ReOly5 = '';
$ReOly6 = '';
$ReOly7 = '';
$ReOly8 = '';
 */
//print "$EmpID<br/>";
$Script_Edit = "Select * from [KPIFinalScore] WHERE HashKey ='" . $HashKey_fkpi . "' and Status <> 'D'";
?>
								<?php
//Show goal form....
if ($goal_period_from_month != "") {
    print '<br/><br/>';
    include 'goal_obj_form_readonly.php';
    print '<br/><br/>';
}
?>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- ======================================== EMPLOYEE STRENGHT AND WEAKNESS ===================== -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12"  style="padding-left: 0px; padding-right: 0px;" >
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
//Loop through the Header
$dbOpen2 = ("SELECT [Val1] FROM Masters where (ItemName='Appraisal_Header') AND Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';

$coun = 0;
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $header[$coun] = $row2['Val1'];
    $coun++;
}

if (isset($header[0])) {
    print $header[0];
} else {
    print " State Employee's Strength(s)";
}
?>
												</p>
												<p>
													<?php
//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpStrenght") . '</textarea>';
} elseif (isset($_REQUEST["MWeak"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . $_REQUEST["MWeak"] . '</textarea>';
} else {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[1])) {
    print $header[1];
} else {
    print "State Employee's perceived weakness(es)";
}
?>
												</p>
												<p>
													<?php
//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpWeakness") . '</textarea>';
} elseif (isset($_REQUEST["MWeak"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . $_REQUEST["MWeak"] . '</textarea>';
} else {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
						<!-- ======================================== LEARNING & DEVELOPMENT ACTION ===================== -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[2])) {
    print $header[2];
} else {
    print "Identify Employee's Performance Gaps";
}
?>
												</p>
												<p>
													<?php
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpPerfGap") . '</textarea>';
} elseif (isset($_REQUEST["MGap"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '>' . $_REQUEST["MGap"] . '</textarea>';
} else {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[3])) {
    print $header[3];
} else {
    print "State Remedial Actions that will be relevant over the next FY ";
}
?>
												</p>
												<p>
													<?php
//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpRemAct") . '</textarea>';
} elseif (isset($_REQUEST["MRem"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '>' . $_REQUEST["MRem"] . '</textarea>';
} else {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<div class="row">
													<div class="col-12">
														<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
												            <?php
if (isset($header[4])) {
    print $header[4];
} else {
    print "Medical Fitness and Alertness";
}
?>
														</p>
													</div>
												</div>
												<small>
													<?php
$Script5 = "SELECT [Desc] FROM Masters where (ItemName='Medical') AND Status<>'D'";
$medical_more_msg = ScriptRunner($Script5, "Desc");
if (isset($medical_more_msg)) {
    print $medical_more_msg;
}
?>
												</small>
												<p>
													<?php
if ($EditID != "" || $EditID > 0) {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "MRecommendation") . '</textarea>';} elseif (isset($_REQUEST["MRecom"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '>' . $_REQUEST["MRecom"] . '</textarea>';} else {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '></textarea>';}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>



						<!-- =======================================================================================--------->
						<!-- this section was pushed with leave allowance bugfix but for consistency i pushed it separately
                        =============================================RECCOMENDATIONS==========================================--------->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<div class="row">
													<div class="col-6">
														<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
												            Reccomended For
														</p>
													</div>
													<div class="col-6"></div>
												</div>
												<hr style="margin: 14px auto;">
												<p style="font-weight: bold">
													<div class="row">
														<div class="col-sm-3">
												        	<?php
if (ScriptRunner($Script_Edit, "MTraining") == 'on') {
    echo '<input name="MTraining" id="MTraining" type="checkbox" checked="checked"  onclick="return false;"  /><label for="MTraining"  style="padding-left:25px; line-height:2" >Training</label>';
} else {
    echo '<input name="MTraining" id="MTraining" type="checkbox"   onclick="return false;"  /><label for="MTraining"  style="padding-left:25px; line-height:2" >Training</label>';
}
?>
														</div>
														<div class="col-sm-3">
								            	            <?php
if (ScriptRunner($Script_Edit, "MPromote") == 'on') {
    echo '<input name="MPromote" id="MPromote" type="checkbox" checked="checked"   onclick="return false;" /><label for="MPromote" style="padding-left:25px; line-height:2" >Promotion</label>';
} else {
    echo '<input name="MPromote" id="MPromote" type="checkbox"  onclick="return false;"  /><label for="MPromote" style="padding-left:25px; line-height:2" >Promotion</label>';
}
?>
											            </div>
											            <div class="col-sm-3">
											            	<?php
if (ScriptRunner($Script_Edit, "MTransfer") == 'on') {echo '<input name="MTransfer" id="MTransfer" type="checkbox" checked="checked"    onclick="return false;" /><label for="MTransfer" style="padding-left:25px; line-height:2" >Transfer</label>';} else {echo '<input name="MTransfer" id="MTransfer" type="checkbox"   onclick="return false;"  /><label for="MTransfer" style="padding-left:25px; line-height:2" >Transfer</label>';}
?>
											            </div>
											            <div class="col-sm-3">
											            	<?php
if (ScriptRunner($Script_Edit, "MSalary") == 'on') {echo '<input name="MSalary" id="MSalary" type="checkbox" checked="checked"   onclick="return false;"  /><label for="MSalary" style="padding-left:25px; line-height:2" >Salary Adjustment</label>';} else {echo '<input name="MSalary" id="MSalary" type="checkbox"   onclick="return false;" /><label for="MSalary" style="padding-left:25px; line-height:2" >Salary Adjustment</label>';}
?>
											            </div>
						            		  			<div class="col-sm-4">
						            		  				<?php
if (ScriptRunner($Script_Edit, "MExtension") == 'on') {echo '<input name="MExtension" id="MExtension" type="checkbox" checked="checked"  onclick="return false;"  /><label for="MExtension" style="padding-left:25px; line-height:2" >Extension of Probationary Period</label>';} else {echo '<input name="MExtension" id="MExtension" type="checkbox"   onclick="return false;"  /><label for="MExtension" style="padding-left:25px; line-height:2" >Extension of Probationary Period</label>';}
?>
											            </div>
											            <div class="col-sm-4">
											            	<?php
if (ScriptRunner($Script_Edit, "MReview") == 'on') {echo '<input name="MReview" id="MReview" type="checkbox" checked="checked"   onclick="return false;"  /><label for="MReview" style="padding-left:25px; line-height:2" >Review of Continued Employment</label>';} else {echo '<input name="MReview" id="MReview" type="checkbox"   onclick="return false;"  /><label for="MReview" style="padding-left:25px; line-height:2" >Review of Continued Employment</label>';}
?>
											            </div>
											            <div class="col-sm-4 px-0">
						             			            <?php
if (ScriptRunner($Script_Edit, "MConfirmation") == 'on') {echo '<input name="MConfirmation" id="MConfirmation" type="checkbox" checked="checked"   onclick="return false;"  /><label for="MConfirmation" style="padding-left:25px; line-height:2" >Confirmation of Appointment/Acting Appointment</label>';} else {echo '<input name="MConfirmation" id="MConfirmation" type="checkbox"  onclick="return false;"  /><label for="MConfirmation" style="padding-left:25px; line-height:2" >Confirmation of Appointment/Acting Appointment</label>';}
?>
							            		  		</div>
													</div>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
						<!-- =======================================================================================--------->























						<!-- =======================================================================================--------->
						<!-- =======================================================================================--------->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Employee's Comment (<?php print $name1;?>) </p>
												<p>
													<?php
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '>' . ScriptRunner($Script_Edit, "EComment") . '</textarea>';
} elseif (isset($_REQUEST["EComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '>' . $_REQUEST["EComm"] . '</textarea>';
} else {
    echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Supervisor's Comment (<?php print $name2;?>)</p>
												<p>
													<?php
$Script = "Select EComm, MComm from [KPIIndvScore] WHERE HashKey ='" . $HashKey_fkpi . "' AND status <> 'D'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "MComment") . '</textarea>';
} elseif (isset($_REQUEST["MComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '>' . $_REQUEST["MComm"] . '</textarea>';
} else {
    echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '></textarea>';
}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<?php
//print $num_appraisers." $EmpTyp (". $who_is_appraising;

if (isset($AprsComment3)) {
    $apcom3 = $AprsComment3;
    $name3 = $AprsName33;
} else {
    $apcom3 = "";
    $name3 = "";
}

if (isset($AprsComment4)) {
    $apcom4 = $AprsComment4;
    $name4 = $AprsName44;
} else {
    $apcom4 = "";
    $name4 = "";
}

if (isset($AprsComment5)) {
    $apcom5 = $AprsComment5;
    $name5 = $AprsName55;
} else {
    $apcom5 = "";
    $name5 = "";
}

if (isset($AprsComment6)) {
    $apcom6 = $AprsComment6;
    $name6 = $AprsName66;
} else {
    $apcom6 = "";
    $name6 = "";
}

if (isset($AprsComment7)) {
    $apcom7 = $AprsComment7;
    $name7 = $AprsName77;
} else {
    $apcom7 = "";
    $name7 = "";
}

if (isset($AprsComment8)) {
    $apcom8 = $AprsComment8;
    $name8 = $AprsName88;
} else {
    $apcom8 = "";
    $name8 = "";
}
//----------------------------------------------- Summary Comment Box
include 'level_check_mgr.php';
?>
		               <!-- =======================================================================================--------->
						<div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ($appr3_comment == '1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 3rd Appraiser's Summary Comment (<?php echo $name3 ?>)</p>
				            						<p>
				            							<textarea name="3rdAps" rows="3" class="form-control" <?=$ReOly3?> >
				            								<?=$apcom3?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            				<?php if ($appr4_comment == '1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 4th Appraiser's Summary Comment (<?php echo $name4 ?>)</p>
				            						<p>
				            							<textarea name="4thAps" rows="3" class="form-control" <?=$ReOly4?> >
				            								<?=$apcom4?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- =======================================================================================--------->
			            <!-- =======================================================================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ($appr5_comment == '1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 5th Appraiser's Summary Comment (<?php echo $name5 ?>)</p>
				            						<p>
				            							<textarea name="5thAps" rows="3" class="form-control" <?=$ReOly5?> >
				            								<?=$apcom5?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            				<?php if ($appr6_comment == '1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 6th Appraiser's Summary Comment (<?php echo $name6 ?>)</p>
				            						<p>
				            							<textarea name="6thAps" rows="3" class="form-control" <?=$ReOly6?> >
				            								<?=$apcom6?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- =======================================================================================--------->
			            <!-- ============================================== OUTGOING YEAR =========================================== -->
						<?php
$data = ScriptRunner($Script_Edit, "Accomp");
$EmpAccomp = json_decode($data, true);
if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "Accomplishments") == '1' && isset($data) && $data !== ''): ?>
	        				<div class="row">
	        					<!-- <div class="col-md-1"></div> -->
	        					<div class="col-md-12">
	        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
	        							<div class="box-group">
	        								<div class="box">
	        									<div class="box-body">
	        										<div class="row">
	        											<div class="col-6">
			        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
			        								            OUTGOING YEAR
			        										</p>
			        									</div>
	        										</div>
													<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
														<thead>
															<tr>
																<th><div style="width: 150px">Accomplishments</div></th>
																<th><div style="width: 150px">Supervisor's Comment</div></th>
																<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>)</div></th>
																<?php endif?>
																<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>)</div></th>
																<?php endif?>
																<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>)</div></th>
																<?php endif?>
																<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>)</div></th>
																<?php endif?>
															</tr>
														</thead>
														<tbody class="input_fields_wrap fieldList">
															<?php for ($i = 1; $i <= count($EmpAccomp); $i++): ?>
																<tr id="row<?php echo $i ?>">
																	<td>
																		<div class="form-group">
																			<textarea readonly name="accomplishments[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['employee_accomp']; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
if (!empty($EmpAccomp[$i - 1]['mgr_accomp_comment'])) {
    $mgr_accomp = trim($EmpAccomp[$i - 1]['mgr_accomp_comment']);
}
?>
																			<textarea readonly name="mgr_accomp[]" rows='1' class="form-control"><?php echo $mgr_accomp; ?></textarea>
																		</div>
																	</td>
																	<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($EmpAccomp[$i - 1]['apr3_accomp_comment'])) {
    $apr3_accomp = trim($EmpAccomp[$i - 1]['apr3_accomp_comment']);
} else {
    $apr3_accomp = '';
}
?>
																				<textarea name="apr3_accomp[]" <?=$ReOly3?> rows='1' class="form-control"><?php echo $apr3_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($EmpAccomp[$i - 1]['apr4_accomp_comment'])) {
    $apr4_accomp = trim($EmpAccomp[$i - 1]['apr4_accomp_comment']);
} else {
    $apr4_accomp = '';
}
?>
																				<textarea name="apr4_accomp[]" <?=$ReOly4?> rows='1' class="form-control"><?php echo $apr4_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($EmpAccomp[$i - 1]['apr5_accomp_comment'])) {
    $apr5_accomp = trim($EmpAccomp[$i - 1]['apr5_accomp_comment']);
} else {
    $apr5_accomp = '';
}
?>
																				<textarea name="apr5_accomp[]" <?=$ReOly5?> rows='1' class="form-control"><?php echo $apr5_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($EmpAccomp[$i - 1]['apr6_accomp_comment'])) {
    $apr6_accomp = trim($EmpAccomp[$i - 1]['apr6_accomp_comment']);
} else {
    $apr6_accomp = '';
}
?>
																				<textarea name="apr6_accomp[]" <?=$ReOly6?> rows='1' class="form-control"><?php echo $apr6_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																</tr>
															<?php endfor;?>
														</tbody>
													</table>
	        									</div>
	        								</div>
	        							</div>
	        						</div>
	        					</div>
	        					<!-- <div class="col-md-1"></div> -->
	        				</div>
						<?php endif?>
			            <!-- =======================================================================================--------->
			            <!-- ============================================== OBJECTIVES =========================================== -->
						<?php
$obj_column = json_decode(ScriptRunner($Script_Edit, "objectives_remark"), true);
if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "show_objectives") == '1'): ?>
	        				<div class="row">
	        					<!-- <div class="col-md-1"></div> -->
	        					<div class="col-md-12">
	        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
	        							<div class="box-group">
	        								<div class="box">
	        									<div class="box-body">
	        										<div class="row">
	        											<div class="col-6">
			        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
			        								        	OBJECTIVES FOR THE COMING YEAR
			        										</p>
			        									</div>
	        										</div>
													<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
														<thead>
															<tr>
																<th><div style="width: 150px">Objectives</div></th>
																<th><div style="width: 150px">Remarks</div></th>
																<th><div style="width: 150px">Supervisor's Remark</div></th>
																<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>) Remark</div></th>
																<?php endif?>
															</tr>
														</thead>
														<tbody class="input_fields_wrap fieldList">
															<?php for ($i = 1; $i <= count($obj_column); $i++): ?>
																<tr>
																	<td>
																		<div class="form-group">
																			<textarea name="objectives[]" readonly rows='1' class="form-control"><?php echo $obj_column[$i - 1]['objectives']; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
if (!empty($obj_column[$i - 1]['emp_objectives'])) {
    $emp_objectives = trim($obj_column[$i - 1]['emp_objectives']);
} else {
    $emp_objectives = '';
}
?>
																			<textarea name="emp_objectives[]" readonly rows='1' class="form-control"><?php echo $emp_objectives; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
if (!empty($obj_column[$i - 1]['mgr_objectives'])) {
    $mgr_objectives = trim($obj_column[$i - 1]['mgr_objectives']);
} else {
    $mgr_objectives = '';
}
?>
																			<textarea  name="mgr_objectives[]" readonly rows='1' class="form-control"><?php echo $mgr_objectives; ?></textarea>
																		</div>
																	</td>
																	<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr3_objectives'])) {
    $apr3_objectives = trim($obj_column[$i - 1]['apr3_objectives']);
} else {
    $apr3_objectives = '';
}
?>
																				<textarea name="apr3_objectives[]" <?=$ReOly3?>  rows='1' class="form-control"><?php echo $apr3_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr4_objectives'])) {
    $apr4_objectives = trim($obj_column[$i - 1]['apr4_objectives']);
} else {
    $apr4_objectives = '';
}
?>
																				<textarea name="apr4_objectives[]" <?=$ReOly4?>  rows='1' class="form-control"><?php echo $apr4_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr5_objectives'])) {
    $apr5_objectives = trim($obj_column[$i - 1]['apr5_objectives']);
} else {
    $apr5_objectives = '';
}
?>
																				<textarea name="apr5_objectives[]" <?=$ReOly5?>  rows='1' class="form-control"><?php echo $apr5_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr6_objectives'])) {
    $apr6_objectives = trim($obj_column[$i - 1]['apr6_objectives']);
} else {
    $apr6_objectives = '';
}
?>
																				<textarea name="apr6_objectives[]" <?=$ReOly6?>  rows='1' class="form-control"><?php echo $apr6_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																</tr>
															<?php endfor;?>
														</tbody>
													</table>
	        									</div>
	        								</div>
	        							</div>
	        						</div>
	        					</div>
	        					<!-- <div class="col-md-1"></div> -->
	        				</div>
						<?php endif?>
			            <!-- =======================================================================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10 text-center">
								<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
									<input name="FAction" id="FAction" type="hidden">
									<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
									<input name="SndBk_Acct" id="SndBk_Acct" type="hidden" value="" />';
?>
								<!-- <input name="SubmitTrans" id="SubmitTrans" type="button" class="smallButton" value="Reject & Update" onClick="YesNo('Update KPI', 'Update')" /> -->
								<br /><br />
								<?php
$isAppraisalActive = ScriptRunner("SELECT HashKey FROM KPIStart WHERE Status='A' AND HashKey='" . ECh($_REQUEST['AQuartID']) . "' AND GetDate() BETWEEN CONVERT(VARCHAR(12),SDate,106)+' 00:00:00' AND CONVERT(VARCHAR(12),EDate,106)+' 23:59:59'", "HashKey");
if ($isAppraisalActive) {
    print $button_message;
}
?>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</form>
	<?php echo $kk_all; ?>
</body>

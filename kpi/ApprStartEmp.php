<?php
session_start();
include '../login/scriptrunner.php';
//if (ValidateURths("APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
if (ValidateURths("EMP APPRAISAL" . "B") != true) {include '../main/NoAccess.php';exit;}
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;include '../css/myscripts.php';
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($SelID)) {$SelID = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($StatusSet)) {$StatusSet = 0;}
if (!isset($KPICount)) {$KPICount = 0;}
if (!isset($KRAs)) {$KRAs = 0;}
if (!isset($TWeightage)) {$TWeightage = 0;}
if (!isset($TotMgr)) {$TotMgr = 0;}
if (!isset($Total_Mgr_score)) {$Total_Mgr_score = 0;}
if (!isset($o)) {$o = 0;}
if (!isset($AllEmployee)) {$AllEmployee = "";}
if (!isset($kk_all)) {$kk_all = "";}
if (!isset($EScore)) {$EScore = "";}
if (!isset($DrpCnt)) {$DrpCnt = 0;}
if (!isset($UpdUser)) {$UpdUser = "";}
if (!isset($HashKey_set)) {$HashKey_set = "";}

if (!isset($ReOly3)) {$ReOly3 = "";}
if (!isset($ReOly4)) {$ReOly4 = "";}
if (!isset($ReOly5)) {$ReOly5 = "";}
if (!isset($ReOly6)) {$ReOly6 = "";}
if (!isset($ReOly7)) {$ReOly7 = "";}
if (!isset($ReOly8)) {$ReOly8 = "";}

if (!isset($apcom3)) {$apcom3 = "";}
if (!isset($apcom4)) {$apcom4 = "";}
if (!isset($apcom5)) {$apcom5 = "";}
if (!isset($apcom6)) {$apcom6 = "";}
if (!isset($apcom7)) {$apcom7 = "";}
if (!isset($apcom8)) {$apcom8 = "";}

if (!isset($name3)) {$name3 = "";}
if (!isset($name4)) {$name4 = "";}
if (!isset($name5)) {$name5 = "";}
if (!isset($name6)) {$name6 = "";}
if (!isset($name7)) {$name7 = "";}
if (!isset($name8)) {$name8 = "";}
if (!isset($enforce_fin)) {$enforce_fin = "";}
if (!isset($AQuartID_Val)) {$AQuartID_Val = "";}

if (!isset($total_weightage)) {$total_weightage = "";}
if (!isset($total_sum)) {$total_sum = "";}

if (!isset($TotAScoreLevel3)) {$TotAScoreLevel3 = "";}
if (!isset($TotAScoreLevel4)) {$TotAScoreLevel4 = "";}
if (!isset($TotAScoreLevel5)) {$TotAScoreLevel5 = "";}
if (!isset($TotAScoreLevel6)) {$TotAScoreLevel6 = "";}

if (!isset($final_score_type)) {$final_score_type = "";}

$Script28 = "Select * from KPISettingNew where Status = 'A'";
$_SESSION['KPISettingNewHashKey'] = ScriptRunner($Script28, "HashKey");
$num_appraisers1 = ScriptRunner($Script28, "num_appraisers");
$if_enforce_comment = ScriptRunner($Script28, "enforce_comment");
$enforce_final1 = ScriptRunner($Script28, "enforce_final");

$final_score_setup = ScriptRunner($Script28, "final_score_setup");

$goal_desc = ScriptRunner($Script28, "goal_desc");
$goal_period_from_month = ScriptRunner($Script28, "goal_period_from_month");
$goal_period_from_year = ScriptRunner($Script28, "goal_period_from_year");
$goal_period_to_month = ScriptRunner($Script28, "goal_period_to_month");
$goal_period_to_year = ScriptRunner($Script28, "goal_period_to_year");
$goal_header1 = ScriptRunner($Script28, "goal_header1");
$goal_header2 = ScriptRunner($Script28, "goal_header2");
$goal_header3 = ScriptRunner($Script28, "goal_header3");
$goal_header4 = ScriptRunner($Script28, "goal_header4");
$goal_num_row_allow = ScriptRunner($Script28, "goal_num_row_allow");

$Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
$AprsNm = ScriptRunner($Script99, "AprsNm");

$GoValidate = true;

//echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["AQuartID"]) && strlen($_REQUEST["AQuartID"]) != 32) {
    echo ("<script type='text/javascript'>{ parent.msgbox('No appraisal records found', 'red'); }</script>");
    exit;
}
//Check if there is a Valid Appriasal Open
$Script = "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )))";
if (ScriptRunner($Script, "Ct") == 0) {
    echo ("<script type='text/javascript'>{ parent.msgbox('You do not have any appraisal records. Contact your HR team', 'red'); }</script>");
    exit;
}

//Check if Appraisal has been authorized and submitted //'N','U',
$Script = "select count(*) Ct from KPICore where Status in ('N','U','A') and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
if (ScriptRunner($Script, "Ct") == 0) {
    echo ("<script type='text/javascript'>{ parent.msgbox('You do not have any open appraisals records. Contact your HR team', 'red'); }</script>");
    exit;
}
//echo $Script;

//echo $_REQUEST["FAction"].$_REQUEST["PgDoS"];
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    //============================ FINAL SUBMISSION
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Agreed Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        //exit('STOP HERE NOW');

        //if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Agreed Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0)
        //    {
        $empScore = 0;
        foreach ($_REQUEST as $key => $val) {
            if (substr(trim($key), 0, 10) === "AScoreEmp_") {
                $empScore = $empScore + number_format($val, 1);

            }
        }

        $EditID = 0;
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $php_i]);
                $StatusSet = 'A';
                $Script = "Update [KPIIndvScore] set
					[AScoreFinal]=[AScoreMrg],
					[WScore]=((Weightage * AScoreMrg)/100),
					[Status]='" . $StatusSet . "',[SubmittedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[SubmittedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                ScriptRunnerUD($Script, "Inst");

                if ($php_i == 1) {
                    //[EScore]=".ECh($_REQUEST["FScore"]).", DONT Update employee score. He is submitting his/het managers score

                    $Script_supervisor = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
                    $Supervisor_HashKey = ScriptRunner($Script_supervisor, "MgrHashKey");

                    $Script_hod = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . $Supervisor_HashKey . "'";
                    $hod_HashKey = ScriptRunner($Script_hod, "MgrHashKey");
                    $Script_set = "Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "'";
                    $HashKey_set = ScriptRunner($Script_set, "HashKey");

                    /*
                    $Script="Update [KPIFinalScore] set
                    [EComment]='".ECh($_REQUEST["EComm"])."',
                    [Status]='".$StatusSet."',
                    [KPISetting_hashkey]='".$HashKey_set."',
                    [SubmittedBy]='".$_SESSION["StkTck"."HKey"]."',
                    [LUO_Num]='".ECh($_REQUEST["num_appraisers"])."',
                    [LUO_HashKey]='".$hod_HashKey."',
                    [SubmittedDate]=GetDate()
                    where
                    EID='".$_SESSION["StkTck"."HKey"]."'
                    and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '".$EditID."')
                    and Status <> 'D'
                    ";
                     */

                    $data = [];
                    if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                        for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                            if (!empty($_REQUEST["accomplishments"][$i])) {
                                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '';
                                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                                $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                            }
                        }
                        if (count($data)) {
                            $Emp_Accmp = json_encode($data);
                        } else {
                            $Emp_Accmp = null;
                        }
                    } else {
                        $Emp_Accmp = null;
                    }

                    $objective_data = [];
                    if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                        for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                            if (!empty($_REQUEST["objectives"][$i])) {
                                $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '';
                                $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                            }
                        }
                        if (count($objective_data)) {
                            $objectivesData = json_encode($objective_data);
                        } else {
                            $objectivesData = null;
                        }
                    } else {
                        $objectivesData = null;
                    }

                    $Script = "Update [KPIFinalScore] set
						[Status]='" . $StatusSet . "',
						[EScore]='" . $empScore . "',
						[KPISetting_hashkey]='" . $HashKey_set . "',
						[SubmittedBy]='" . $_SESSION["StkTck" . "HKey"] . "',
						[Accomp]='" . $Emp_Accmp . "',
						[objectives_remark]='" . $objectivesData . "',
						[LUO_Num]='" . ECh($_REQUEST["num_appraisers"]) . "',
						[LUO_HashKey]='" . $hod_HashKey . "',
						[SubmittedDate]=GetDate()
						where
						EID='" . $_SESSION["StkTck" . "HKey"] . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
						and Status <> 'D'
						";
                    ScriptRunnerUD($Script, "FS");

                    AuditLog("UPDATE", "Employee appriasal submitted successfully by self ");
                    /*
                    $Script_Mail="Select * from MailTemp where MSub='Appraisal Submitted'"; //Appraisal Score Submitted
                    $LoginDet=ScriptRunner($Script_Mail,"MMsg");
                    $Subj = ScriptRunner($Script_Mail,"MSub");

                    $Script="select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='".ECh($_REQUEST["ApprEmpID"])."')";
                    $LoginDet=str_replace('#Name#',ECh($_SESSION["StkTck"."FName"]),$LoginDet);
                    //$LoginDet=str_replace('#Score#','<strong>'.ECh($_REQUEST["FScore"]).'%</strong>',$LoginDet);

                    //$To=ScriptRunner($Script,"Email");
                    //if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",$To))
                    //{
                    QueueMail($To, $Subj, $LoginDet,'','');
                    //}

                    if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",ECh($_SESSION["StkTck"."Email"]) ))
                    {
                    QueueMail(ECh($_SESSION["StkTck"."Email"]), $Subj, $LoginDet,'','');
                    }

                    $Script="Select Email from EmpTbl Et where HashKey in ((Select Convert(Varchar(32),SetValue5) from Settings where Setting='ApprNotify'),(Select Convert(Varchar(32),SetValue6) from Settings where Setting='ApprNotify'))";
                    $To = ScriptRunnerJoin($Script,"Email");
                    if ($To!='')
                    {
                    QueueMail($To, $Subj, $LoginDet,'','');
                    }
                     */

                    $Script_Mail = "Select * from MailTemp where MSub='Appraisal Submitted'"; //Appraisal Submitted
                    $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                    $Subj = ScriptRunner($Script_Mail, "MSub");

                    $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                    if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", ECh($_SESSION["StkTck" . "Email"]))) {
                        QueueMail(ECh($_SESSION["StkTck" . "Email"]), $Subj, $LoginDet, '', '');
                    }

                    //+++++++++++++++++++++++ To the Manager/Supervisor
                    $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Update'";
                    $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                    $Subj = ScriptRunner($Script_Mail, "MSub");

                    $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                    $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                    $LoginDet = str_replace('#name_of_2nd_Appraiser#', ScriptRunner($Script, "Nm"), $LoginDet);
                    $To = ScriptRunner($Script, "Email");
                    if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                        QueueMail($To, $Subj, $LoginDet, '', '');
                    }

                    //Send email to third person
                    $mailer_from = "first person";
                    $mailer_to = "third person";
                    include "mail_trail_send_kpi.php";

                }
            }
            echo ("<script type='text/javascript'>{ parent.msgbox('Your appraisal score has been submitted successfully.', 'green'); }</script>");
        }
        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
    }
    //=====================SAVE AND CONTINUE
    if (isset($_REQUEST['button']) && $_REQUEST['button'] == "Save and Continue Later") {
        $EditID = "";
        $empScore = 0;
        foreach ($_REQUEST as $key => $val) {
            if (substr(trim($key), 0, 10) === "AScoreEmp_") {
                $empScore = $empScore + number_format($val, 1);

            }
        }

        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $php_i]);
                if ($php_i == 1) {
                    $Status = 'S'; //Save and Continue later
                    //Select HashKey from KPISetting where Status='A' and AName='".$_SESSION["StkTck"."HKey"]."')";
                    $data = [];
                    if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                        for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                            if (!empty($_REQUEST["accomplishments"][$i])) {
                                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '';
                                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                                $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                            }
                        }
                        if (count($data)) {
                            $Emp_Accmp = json_encode($data);
                        } else {
                            $Emp_Accmp = null;
                        }
                    } else {
                        $Emp_Accmp = null;
                    }

                    $objective_data = [];
                    if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                        for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                            if (!empty($_REQUEST["objectives"][$i])) {
                                $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '';
                                $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                            }
                        }
                        if (count($objective_data)) {
                            $objectivesData = json_encode($objective_data);
                            // var_dump($objective_data);
                        } else {
                            $objectivesData = null;
                        }
                    } else {
                        $objectivesData = null;
                    }

                    //$Script_set="Select HashKey from KPISetting where Status='A' ORDER BY AddedDate DESC";
                    $Script_set = "Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "'";
                    $HashKey_set = ScriptRunner($Script_set, "HashKey");

                    //[UpdatedBy]='".ECh($_SESSION["StkTck"."HKey"])."',
                    $Script = "Update [KPIFinalScore] set
						[EScore]='" . $empScore . "',
						[Accomp]='" . $Emp_Accmp . "',
						[objectives_remark]='" . $objectivesData . "',
						[EComment]='" . ECh($_REQUEST["EComm"]) . "',
						[LUO_Num]='" . ECh($_REQUEST["num_appraisers"]) . "',
						[Status]='" . $Status . "',
						[KPISetting_hashkey]='" . $HashKey_set . "',
						[UpdatedDate]=GetDate()
						where
						EID='" . $_SESSION["StkTck" . "HKey"] . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
						and Status <> 'D'
						";
                    ScriptRunnerUD($Script, "FS");
                }
                $Script = "Update [KPIIndvScore] set [AScoreEmp]='" . ECh($_REQUEST["AScoreEmp_" . $EditID]) . "',[Status]='" . $Status . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                // var_dump($Script);
                ScriptRunnerUD($Script, "Inst");
                echo ("<script type='text/javascript'>{ parent.msgbox('Your appraisal was successfully saved, you can continue later...', 'green'); }</script>");
            }
        }
        AuditLog("INSERT", "Saved and Continue appraisal later for " . $AprsNm);
        //Saved and Continue appraisal later for [Name of Appraised]
    }
    // ==============SAVE FOR MANAGER'S COMMENT
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        $empScore = 0;
        foreach ($_REQUEST as $key => $val) {
            if (substr(trim($key), 0, 10) === "AScoreEmp_") {
                $empScore = $empScore + number_format($val, 1);

            }
        }
        echo $empScore;
        // die();
        $goOn = true;
        include 'enforce_comment.php';
        if ($goOn) {
            $EditID = "";
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $EditID = ECh($_REQUEST["DelBox" . $php_i]);
                    //++++++++++++++++++++++++++++++++++++++++++++ Enforce Ccomment +++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    if ($php_i == 1) {
                        $Status = 'U'; //default Update Status
                        $Script = "Select EmpUpdCnt from KPIFinalScore where EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "') AND Status <> 'D'";

                        $MaxIter = ScriptRunner($Script, "EmpUpdCnt");
                        if ($MaxIter > 0) {
                            //$Script="select DeptMaxCnt from KPIDept where DeptID=(
                            //select Department from EmpTbl where HashKey='".$_REQUEST["ApprEmpID"]."')";

                            $Script = "select ACount from KPIStart where [HashKey]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')";
                            if ((ScriptRunner($Script, "ACount") - $MaxIter) <= 0) //Means max iteration has been reachd. Force Submit the record
                            { $Status = 'PR'; /*Resolution - Disables update or submit button for both manager and employee */}
                        }

                        $Script = "Select UpdatedBy from KPIFinalScore where EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')";
                        if (ScriptRunner($Script, "UpdatedBy") != $_SESSION["StkTck" . "HKey"]) {
                            $Script = "Update [KPIFinalScore] set [EmpUpdCnt]=EmpUpdCnt + 1 where EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
								and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')";
                            ScriptRunnerUD($Script, "Ct");

                        }

                        //[FScore]=".ECh($_REQUEST["FScore"]).",
                        $Script_set = "Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "'";
                        $HashKey_set = ScriptRunner($Script_set, "HashKey");
                        $Script_set4 = "Select MgrFinal, MID from KPIFinalScore where
							EID='" . $_SESSION["StkTck" . "HKey"] . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')";
                        $MgrFinal = ScriptRunner($Script_set4, "MgrFinal");
                        $MID = ScriptRunner($Script_set4, "MID");

                        if (!$MgrFinal) {
                            $enforce_fin = ECh($_REQUEST["enforce_final"]);
                        }
                        $data = [];
                        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                                if (!empty($_REQUEST["accomplishments"][$i])) {
                                    $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '';
                                    $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                                    $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                                    $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                                    $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                                    $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                                }
                            }
                            if (count($data)) {
                                $Emp_Accmp = json_encode($data);
                            } else {
                                $Emp_Accmp = null;
                            }
                        } else {
                            $Emp_Accmp = null;
                        }

                        $objective_data = [];
                        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                                if (!empty($_REQUEST["objectives"][$i])) {
                                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '';
                                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                                }
                            }
                            if (count($objective_data)) {
                                $objectivesData = json_encode($objective_data);
                            } else {
                                $objectivesData = null;
                            }
                        } else {
                            $objectivesData = null;
                        }

                        $Script = "Update [KPIFinalScore] set
							[EScore]='" . $empScore . "',
							[EComment]='" . ECh($_REQUEST["EComm"]) . "',
							[Accomp]='" . $Emp_Accmp . "',
							[objectives_remark]='" . $objectivesData . "',
							[LUO_Num]='" . ECh($_REQUEST["num_appraisers"]) . "',
							[MgrFinal]='" . $enforce_fin . "',
							[Status]='" . $Status . "',
							[KPISetting_hashkey]='" . $HashKey_set . "',
							[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
							[UpdatedDate]=GetDate()
							where
							EID='" . $_SESSION["StkTck" . "HKey"] . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
							and Status <> 'D'
							";
                        ScriptRunnerUD($Script, "FS");
                        /*[MRecommendation]='".ECh($_REQUEST["MRecom"])."',
                    [MComment]='".ECh($_REQUEST["MComm"])."',
                    [MPromote]='".ECh($_REQUEST["MPromote"])."',*/
                    }

                    $Script = "Update [KPIIndvScore] set [AScoreEmp]='" . ECh($_REQUEST["AScoreEmp_" . $EditID]) . "',[Status]='" . $Status . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Inst");
                    if ($php_i == 1) {
                        if ($Status == 'PR') //This is Moderator
                        {

                            $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) MgrAprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $MID . "'";
                            $MgrAprsNm = ScriptRunner($Script99, "MgrAprsNm");

                            AuditLog("INSERT", "Employee appriasal forced submit by self iteration max out to Supervisor/Line Manager name: " . $MgrAprsNm);

                            //AuditLog("UPDATE","Employee appriasal forced submit by self iteration max out");

                            echo ("<script type='text/javascript'>{ parent.msgbox('Appraisal score updated and forced submitted.', 'green'); }</script>");
                            $Script_Mail = "Select * from MailTemp where MSub='Appraisal Submitted'"; //Appraisal Submitted
                            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                            $Subj = ScriptRunner($Script_Mail, "MSub");

                            $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                            if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", ECh($_SESSION["StkTck" . "Email"]))) {
                                QueueMail(ECh($_SESSION["StkTck" . "Email"]), $Subj, $LoginDet, '', '');
                            }

                            //+++++++++++++++++++++++ To the Manager/Supervisor
                            $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Update'";
                            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                            $Subj = ScriptRunner($Script_Mail, "MSub");
                            $Script288 = "Select * from KPISettingNew where Status = 'A'";
                            $allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
                            $Script289 = "Select * from [KPIFinalScore] where EID='" . $_SESSION["StkTck" . "HKey"] . "' and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "') and Status <> 'D'";
                            $secondary_mgr = ScriptRunner($Script289, "secondary_mgr");

                            // check if to send mail to secondary manager

                            if ((int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {

                                $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr_1 from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                            } else {
                                $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";

                            }

                            $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                            $LoginDet = str_replace('#name_of_2nd_Appraiser#', ScriptRunner($Script, "Nm"), $LoginDet);
                            $To = ScriptRunner($Script, "Email");
                            if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                                QueueMail($To, $Subj, $LoginDet, '', '');
                            }
                        } else {
                            $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) MgrAprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $MID . "'";
                            $MgrAprsNm = ScriptRunner($Script99, "MgrAprsNm");
                            AuditLog("INSERT", "Submit Appraisal to Supervisor/Line Manager name: " . $MgrAprsNm);
                            echo ("<script type='text/javascript'>{ parent.msgbox('Your appraisal score was successfully Submitted', 'green'); }</script>");
                            $Script_Mail = "Select * from MailTemp where MSub='Appraisal Submitted'"; //Appraisal Submitted
                            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                            $Subj = ScriptRunner($Script_Mail, "MSub");
                            $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                            if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", ECh($_SESSION["StkTck" . "Email"]))) {
                                QueueMail(ECh($_SESSION["StkTck" . "Email"]), $Subj, $LoginDet, '', '');
                            }
                            //+++++++++++++++++++++++ To the Manager/Supervisor
                            $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Update'";
                            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                            $Subj = ScriptRunner($Script_Mail, "MSub");

                            $Script288 = "Select * from KPISettingNew where Status = 'A'";
                            $allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
                            $Script289 = "Select * from [KPIFinalScore] where EID='" . $_SESSION["StkTck" . "HKey"] . "' and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "') and Status <> 'D'";
                            $secondary_mgr = ScriptRunner($Script289, "secondary_mgr");

                            // check if to send mail to secondary manager

                            if ((int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {

                                $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr_1 from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                            } else {
                                $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";

                            }

                            $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                            $LoginDet = str_replace('#name_of_2nd_Appraiser#', ScriptRunner($Script, "Nm"), $LoginDet);
                            $To = ScriptRunner($Script, "Email");
                            if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                                QueueMail($To, $Subj, $LoginDet, '', '');
                            }
                        }
                    }
                }
                //echo ("<script type='text/javascript'>{ parent.msgbox('Your appraisal score was successfully updated.', 'green'); }<//script>");
            }
            include '../main/prmt_blank.php';
            /* Clear ID to avoid opening this record */
        }
    }
}
$Script = "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
?>
<script>
  $(function()
  {
	$('#datepicker').datepicker({
    showButtonPanel: true,
    dateFormat: "mm/dd/yy",
//    beforeShow: function(){
});


	$("#StartDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-12M", maxDate: "+12M", dateFormat: 'dd MM yy'})
	$("#EndDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-12M", maxDate: "+12M", dateFormat: 'dd MM yy'})
  });
</script>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>


<script type="text/javascript">
function GetTotal($ObjName,$ObjWhtID,$weight,$quantity)
{
	// console.log($ObjName,$ObjWhtID,$weight,$quantity);

	// $tr= document.getElementById($ObjName).value;
	// if($quantity && isNaN(parseFloat($tr)) && isFinite($tr)){
	// 	alert("qauntity must be a valid number.")
	// 	return false;
	// }
/*	if(document.getElementById($ObjName).value=='')
	{
		document.getElementById("TD"+$ObjName).innerHTML='0.0';
		document.getElementById("TOTALTDAScoreEmp").innerHTML = '0.0';
	}
	else
	{
*/
		$kk = document.getElementById("Wght"+$ObjName).innerHTML;
		// console.log($kk);
		//document.getElementById("TD"+$ObjName).innerHTML =((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);

		//document.getElementById("TD"+$ObjName).innerHTML =((parseFloat($kk) * document.getElementById($ObjName).value)).toFixed(2);
		if ($quantity) {
			let actual=document.getElementById($ObjName+"_try").value;
			if(parseInt(actual) >= parseInt($quantity)){
					actual = $quantity;
			}

		 document.getElementById("TD"+$ObjName).innerHTML =(($kk * actual)/$quantity).toFixed(1);
		 document.getElementById($ObjName).value=(($kk * actual)/$quantity).toFixed(1);
		} else {
		document.getElementById("TD"+$ObjName).innerHTML =((1 * document.getElementById($ObjName).value)).toFixed(2);

		}

		document.getElementById("Wth"+$ObjWhtID).value = document.getElementById("TD"+$ObjName).innerHTML;

		// console.log(document.getElementById("TD"+$ObjName).innerHTML);
		// console.log(document.getElementById("Wth"+$ObjWhtID));

		//alert (document.getElementById("DelMax").value);
		document.getElementById("TOTALTDAScoreEmp").innerHTML=0;
		document.getElementById('EmpTScore').value=0;



		for ($i=1; $i<=document.getElementById("DelMax").value; $i++)
		{
			document.getElementById("TOTALTDAScoreEmp").innerHTML =
			parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML) + parseFloat(document.getElementById("Wth"+$i).value);


			//alert(parseFloat(document.getElementById("Wth"+$i).value));
		}
		//Write the total of the employee score into an INPUT for passing to the Final Score when update is clicked
		document.getElementById("EmpTScore").value = parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML).toFixed(2);
		//alert (document.getElementById("EmpTScore").value);
		document.getElementById("TOTALTDAScoreEmp").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML).toFixed(2) + '%';
//	}
}
function GetTotalTags($ObjName,$ObjWhtID,$weight)
{
/*	if(document.getElementById($ObjName).value=='')
	{
		document.getElementById("TD"+$ObjName).innerHTML='0.0';
		document.getElementById("TOTALTDAScoreEmp").innerHTML = '0.0';
	}
	else
	{
*/
		$kk = document.getElementById("Wght"+$ObjName).innerHTML;
		//document.getElementById("TD"+$ObjName).innerHTML =((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);

		//document.getElementById("TD"+$ObjName).innerHTML =((parseFloat($kk) * document.getElementById($ObjName).value)).toFixed(2);

		//Get Employee Score from select tag
		document.getElementById("TD"+$ObjName).innerHTML =(document.getElementById($ObjName).value).toFixed(2);

		document.getElementById("Wth"+$ObjWhtID).value = document.getElementById("TD"+$ObjName).innerHTML;

		//alert (document.getElementById("DelMax").value);
		document.getElementById("TOTALTDAScoreEmp").innerHTML=0;
		document.getElementById('EmpTScore').value=0;



		for ($i=1; $i<=document.getElementById("DelMax").value; $i++)
		{
			document.getElementById("TOTALTDAScoreEmp").innerHTML =
			parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML) + parseFloat(document.getElementById("Wth"+$i).value);


			//alert(parseFloat(document.getElementById("Wth"+$i).value));
		}
		//Write the total of the employee score into an INPUT for passing to the Final Score when update is clicked
		document.getElementById("EmpTScore").value = parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML).toFixed(2);
		//alert (document.getElementById("EmpTScore").value);
		document.getElementById("TOTALTDAScoreEmp").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreEmp").innerHTML).toFixed(2) + '%';
//	}
}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong{
		font-weight: 550;
	}
	[type=checkbox]+label:before, [type=checkbox]:not(.filled-in)+label:after{

	    width: 15px;
	    height: 15px;
	}
	.fieldList .form-group{
		margin-bottom: 0px;
	}
</style>
 <?php
$all_dev = [];
$query = "select [KPI],[KRA],[Status],[Score_Type]
      ,[Quantity_Value] from KPICore where Status in ('A') and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
//    "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."' and Status in('A') )))";
$connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
$conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
if ($conn2 === false) {
    echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");

}
$res2 = sqlsrv_query($conn2, $query);
while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_ASSOC)) {
    $all_dev[ECh(trim($row['KPI']))] = $row;

    //   array_push($all_dev,$row);
}

// var_dump($all_dev);

?>






<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row"  style="line-height: 16px; font-size: 13px;">
					<div class="col-md-6">
						<strong>
							<span style="padding-right: 5px;">
								Employee:
							</span>
							<span style="letter-spacing: 2px;">
								<?php
$Script = "SELECT HashKey,(Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
$EmpID = ScriptRunner($Script, "HashKey");
$EditID = $EmpID;
echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="' . $EmpID . '" />';
//        $Script="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Et.EmpMgr from EmpTbl Et where Et.EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."' and Status in('A') )";
echo ScriptRunner($Script, "Nm");
?>
							</span>
						</strong>
					</div>
					<div class="col-md-6">
						<strong>
							<span style="padding-right: 5px;">
								Supervisor:
							</span>
							<span style="letter-spacing: 2px;">
						        <?php
$Script288 = "Select * from KPISettingNew where Status = 'A'";
$allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
$Script289 = "Select * from KPIFinalScore where AQuartID='" . ECh($_REQUEST["AQuartID"]) . "' and Status <>'D' and EID ='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
// var_dump($Script289);
$secondary_mgr = ScriptRunner($Script289, "secondary_mgr");
// check if to send mail to secondary manager
if ((int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {
    $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr_1 from EmpTbl where HashKey='" . $EmpID . "')";
    $mgr_name = ScriptRunner($Script, "Nm");
    if ($mgr_name) {

        echo ScriptRunner($Script, "Nm");
    } else {
        $Script = "SELECT * from EmpTbl Et where HashKey='" . $_SESSION['StkTckHKey'] . "'";

        $Script = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where HashKey='" . ScriptRunner($Script, "EmpMgr") . "'";
        echo ScriptRunner($Script, "Nm");
        $mgr_name = ScriptRunner($Script, "Nm");
        $Script2888 = "Update KPIFinalScore set [secondary_mgr] = '1' where AQuartID='" . ECh($_REQUEST["AQuartID"]) . "' and Status <>'D' and EID ='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
        ScriptRunnerUD($Script2888, "Inst");

    }

} else {
    $Script = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where HashKey='" . ScriptRunner($Script, "EmpMgr") . "'";

    echo ScriptRunner($Script, "Nm");

    $mgr_name = ScriptRunner($Script, "Nm");
}

?>
							</span>
						</strong>
					</div>
				</div>
			</div>
			<div class="box-body"  >
				<div class="row">
					<div class="col-md-12" style="overflow-x:scroll" >
					    <!-- <div class="ViewPatch"> -->
						    <table class="table-responsive table table-bordered" id="" style="overflow:scroll" >
							    <thead>
							        <tr class="">
										<th>Area of Focus</th>
										<th>Sub Area of Focus</th>

										<th>Departmental Objectives</th>
										<th>Individual Performance Objectives</th>
										<th>Weightage</th>
										<th>Supervisor's<br>Score</th>
										<!-- ================================== extend here ========================== -->
										<?php
include 'level_check_mgr.php';
if ($appr3_emp == '1') {
    print '<th>Level 3 Appraiser<br>
											            Score
											          </th>';
}
if ($appr4_emp == '1') {
    print '<th>Level 4 Appraiser<br>
											            Score
											          </th>';
}
if ($appr5_emp == '1') {
    print '<th>Level 5 Appraiser<br>
											            Score
											          </th>';
}
if ($appr6_emp == '1') {
    print '<th>Level 6 Appraiser<br>
											            Score
											          </th>';
}
?>
										<!-- ========================================================================= -->
										<th>Employee's<br>
											Score
										</th>
										<th>Employee's<br>Score</th>
										<th >&nbsp;</th>
									</tr>
								</thead>
								<tbody>
							        <?php
$Script = "Select * from KPIStart where HashKey='" . ECh($_REQUEST["AQuartID"]) . "'";
$GradeType = ScriptRunner($Script, "ScoreType");
$Del = 0;
$dbOpen2 = ("Select * from KPIFinalScore where AQuartID='" . ECh($_REQUEST["AQuartID"]) . "' and Status <>'D' and EID ='" . ECh($_SESSION["StkTck" . "HKey"]) . "'");
$UpdUser = ScriptRunner($dbOpen2, "UpdatedBy");
$MID = ScriptRunner($dbOpen2, "MID");
$FHashKey = ScriptRunner($dbOpen2, "HashKey"); //new function, for secction comment
$final_score_value = ScriptRunner($dbOpen2, "FScore");
//$inc_LUO_HashKey = ScriptRunner ($dbOpen2,"LUO_HashKey");
$row1_col1 = ScriptRunner($dbOpen2, "row1_col1");
$row1_col2 = ScriptRunner($dbOpen2, "row1_col2");
$row1_col3 = ScriptRunner($dbOpen2, "row1_col3");
$row1_col4 = ScriptRunner($dbOpen2, "row1_col4");

$row2_col1 = ScriptRunner($dbOpen2, "row2_col1");
$row2_col2 = ScriptRunner($dbOpen2, "row2_col2");
$row2_col3 = ScriptRunner($dbOpen2, "row2_col3");
$row2_col4 = ScriptRunner($dbOpen2, "row2_col4");

$row3_col1 = ScriptRunner($dbOpen2, "row3_col1");
$row3_col2 = ScriptRunner($dbOpen2, "row3_col2");
$row3_col3 = ScriptRunner($dbOpen2, "row3_col3");
$row3_col4 = ScriptRunner($dbOpen2, "row3_col4");

$row4_col1 = ScriptRunner($dbOpen2, "row4_col1");
$row4_col2 = ScriptRunner($dbOpen2, "row4_col2");
$row4_col3 = ScriptRunner($dbOpen2, "row4_col3");
$row4_col4 = ScriptRunner($dbOpen2, "row4_col4");

$row5_col1 = ScriptRunner($dbOpen2, "row5_col1");
$row5_col2 = ScriptRunner($dbOpen2, "row5_col2");
$row5_col3 = ScriptRunner($dbOpen2, "row5_col3");
$row5_col4 = ScriptRunner($dbOpen2, "row5_col4");

$row6_col1 = ScriptRunner($dbOpen2, "row6_col1");
$row6_col2 = ScriptRunner($dbOpen2, "row6_col2");
$row6_col3 = ScriptRunner($dbOpen2, "row6_col3");
$row6_col4 = ScriptRunner($dbOpen2, "row6_col4");

$row7_col1 = ScriptRunner($dbOpen2, "row7_col1");
$row7_col2 = ScriptRunner($dbOpen2, "row7_col2");
$row7_col3 = ScriptRunner($dbOpen2, "row7_col3");
$row7_col4 = ScriptRunner($dbOpen2, "row7_col4");

$row8_col1 = ScriptRunner($dbOpen2, "row8_col1");
$row8_col2 = ScriptRunner($dbOpen2, "row8_col2");
$row8_col3 = ScriptRunner($dbOpen2, "row8_col3");
$row8_col4 = ScriptRunner($dbOpen2, "row8_col4");

$row9_col1 = ScriptRunner($dbOpen2, "row9_col1");
$row9_col2 = ScriptRunner($dbOpen2, "row9_col2");
$row9_col3 = ScriptRunner($dbOpen2, "row9_col3");
$row9_col4 = ScriptRunner($dbOpen2, "row9_col4");

$row10_col1 = ScriptRunner($dbOpen2, "row10_col1");
$row10_col2 = ScriptRunner($dbOpen2, "row10_col2");
$row10_col3 = ScriptRunner($dbOpen2, "row10_col3");
$row10_col4 = ScriptRunner($dbOpen2, "row10_col4");

//$dbOpen2 = ("Select * from KPIIndvScore where AQuartID='".ECh($_REQUEST["AQuartID"])."' and (Status <>'D' AND Status <>'S')  and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."') ORDER BY KRA_Group");

$dbOpen2 = ("Select * from KPIIndvScore where AQuartID='" . ECh($_REQUEST["AQuartID"]) . "' and (Status <>'D')  and AID=(Select HashKey from KPISetting where Status='A' and AName='" . ECh($_SESSION["StkTck" . "HKey"]) . "') ORDER BY KRA_Group");
include '../login/dbOpen2.php';
$prevValue = "";
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    // var_dump($row2)
    $Del = $Del + 1;
    //$UpdUser = $row2['UpdatedBy'];
    $AuthRec = $row2['Status'];
    $AQuartID_Val = $row2['AQuartID'];
    $KRA_Group_value = $row2['KRA_Group'];
    //records=$row2['KRA_Group'];
    if ((isset($KRA_Group_value)) && ($KRA_Group_value != $prevValue)) {
        //------ Option to display Section Comment
        if ($appr1_section == '1') {
            $icon = "<button type=\"button\"
														title=\"Edit Record\"
														name=\"EditEdu" . $Del . "\"
														id=\"EditEdu" . $Del . "\"
														onClick=\"parent.ShowDisp('KPI&nbsp;Section&nbsp;Comment','kpi/KPI_CommSection.php?kra_group=" . $KRA_Group_value . "&RecStatus=" . $row2['Status'] . "&EmpTyp=Emp&HID=" . $FHashKey . "',320,600,'Yes')\"
														class=\"btn btn-default btn-sm\" style=\"line-height: 17px\">
														<i class=\"fa fa-edit\"></i></button>";
        } else {
            $icon = "";
        }
        //----------------------------------------
        print "
												<tr class=\"tdMenu_HeadBlock_Light\">
														<td align=\"center\" valign=\"middle\" colspan=\"11\" style=\"padding-bottom: 5px; padding-top:5px;\"><strong>" . $icon . " $KRA_Group_value</strong></td>
												</tr>
												";
        $prevValue = $KRA_Group_value;
    }
    ?>
							        <tr>
								        <?php
//This makes the check boxes into hidden fields
    echo '<input name="' . ("DelBox" . $Del) . '" type="hidden" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" />
										 		<label for="' . ("DelBox" . $Del) . '"></label>';
    if (($row2['HashKey']) == $EditID) {$bgColor = 'bgcolor="#cccccc"';} else { $bgColor = 'bgcolor="#ffffff"';}
    ?>
								        <td>
										  	<?php
echo "<b>" . (trim($row2['KRA'])) . "</b>";
    // echo "<br/>".(trim($row2['ARemark']));
     ?>
								        </td>
								        <td>
										  	<?php
echo "<b>" . (trim($row2['KRA_Sub_Area'])) . "</b>";
    // echo "<br/>".(trim($row2['ARemark']));
     ?>
								        </td>
								        <td>
                                            <?php
echo (trim($row2['KPI'])) . "<br/>";
    $measures = '';
    for ($i = 0; $i <= 6; $i++) {
        if (trim($row2['Opt' . $i])) {
            $measures .= trim($row2['Opt' . $i]) . "; ";
        }
    }
    if ($measures) {
        echo 'Measures: ' . $measures;
    }
    ?>
                                        </td>
										<td>
												<?php
// echo "<b>".(trim($row2['KRA']))."</b>";
    echo "<b>" . (trim($row2['ARemark'])) . "</b>";

    ?>
										</td>


								        <td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "WghtAScoreEmp_" . $row2['HashKey']; ?>">
										  <?php echo (trim($row2['Weightage']));
    $TWeightage = $TWeightage + $row2['Weightage']; ?>
								        </td>
								        <td align="center" valign="middle" class="subHeader" scope="col">
								          	<?php

    // if ($GradeType=="Tags")
    // {
    //     //echo '<select class="TextBoxText_Short">';
    //     // $dbOpen3=("Select Val1, ItemCode from Masters where ItemCode='".number_format($row2['AScoreMrg'])."' and ItemName='KPI Tags' and Status<>'D' order by Val1");
    //     // include '../login/dbOpen3.php';
    //     // while( $row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH))
    //     // {
    //     //     //echo '<option selected value="'.$row3['ItemCode'].'">'.$row3['Val1'].'</option>';
    //     //     echo $row3['Val1'];
    //     // }
    //     // include '../login/dbClose3.php';
    //     //echo '</select>';
    //     $TotMgr = $TotMgr + (number_format($row2['AScoreMrg'], 2, '.', '')/100*$row2['Weightage']);

    //     $Total_Mgr_score = $Total_Mgr_score + $row2['AScoreMrg'];

    //          echo  number_format($row2['AScoreMrg'], 2, '.', '')/100*$row2['Weightage'];
    // }
    // else
    // {
    $TotMgr = $TotMgr + number_format($row2['AScoreMrg'], 1, '.', '');

    $Total_Mgr_score = $Total_Mgr_score + $row2['AScoreMrg'];

    echo number_format($row2['AScoreMrg'], 1, '.', '');
    // }
    ?>
								        </td>
										<!-- ============================extend here =========================== -->
										<?php
include 'level_check_mgr.php';
    if ($appr3_emp == '1') {
        print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
        $TotAScoreLevel3 = $TotAScoreLevel3 + number_format($row2['AScoreLevel3Comm'], 1, '.', '');
        echo number_format($row2['AScoreLevel3Comm'], 1, '.', '');
        print '</td>';
    }

    if ($appr4_emp == '1') {
        print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
        $TotAScoreLevel4 = $TotAScoreLevel4 + number_format($row2['AScoreLevel4Comm'], 1, '.', '');
        echo number_format($row2['AScoreLevel4Comm'], 1, '.', '');
        print '</td>';
    }

    if ($appr5_emp == '1') {
        print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
        $TotAScoreLevel5 = $TotAScoreLevel5 + number_format($row2['AScoreLevel5Comm'], 1, '.', '');
        echo number_format($row2['AScoreLevel5Comm'], 1, '.', '');
        print '</td>';
    }

    if ($appr6_emp == '1') {
        print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
        $TotAScoreLevel6 = $TotAScoreLevel6 + number_format($row2['AScoreLevel6Comm'], 1, '.', '');
        echo number_format($row2['AScoreLevel6Comm'], 1, '.', '');
        print '</td>';
    }
    ?>
										<!-- =================================================================== -->
							          	<td align="center"  class="subHeader" scope="col">



							          		<?php
//   var_dump($all_dev);

    $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];

    if (!isset($amends)) {

        echo ("<script type='text/javascript'>{ parent.msgbox('Appraisal records has been modified. Kindly contact HR department to restart this appariasal record. ', 'red'); }</script>");
        exit;
    }

    if ($amends === "--") {

        $DrpCnt = $DrpCnt + 1;
        //echo $DrpCnt;
        echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
        $kk = "GetTotal('AScoreEmp_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
        echo '<select name="AScoreEmp_' . $row2['HashKey'] . '" class="" id="AScoreEmp_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
        $SelID = $row2['AScoreEmp'];

        if ($GradeType == "Tags") {
            $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
            include '../login/dbOpen3.php';
            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option selected value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
            }
            include '../login/dbClose3.php';
        } else {
            $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
            $incrementsBy = ($val) ? $val : 0.5;
            //for ($php_i=0;$php_i<=100;$php_i++)
            for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                if ($SelID == $php_i) {echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';} else {echo '<option value="' . $php_i . '">' . $php_i . '</option>';}
            }
        }
        echo '</select>';

    } else {
        // echo "here";
        $DrpCnt = $DrpCnt + 1;
        //echo $DrpCnt;

        if ($amends === "Tags") {

            echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
            $kk = "GetTotal('AScoreEmp_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
            echo '<select name="AScoreEmp_' . $row2['HashKey'] . '" class="" id="AScoreEmp_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
            $SelID = $row2['AScoreEmp'];
            $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
            include '../login/dbOpen3.php';
            while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {echo '<option selected value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';} else {echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';}
            }
            include '../login/dbClose3.php';
            echo '</select>';
        } else if ($amends === "Values") {
            echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
            $kk = "GetTotal('AScoreEmp_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
            echo '<select name="AScoreEmp_' . $row2['HashKey'] . '" class="" id="AScoreEmp_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
            $SelID = $row2['AScoreEmp'];
            $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
            $incrementsBy = ($val) ? $val : 0.5;
            //for ($php_i=0;$php_i<=100;$php_i++)
            for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                if ($SelID == $php_i) {echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';} else {echo '<option value="' . $php_i . '">' . $php_i . '</option>';}
            }
            echo '</select>';
        } else if ($amends === "Quantity") {
            $SelID = $row2['AScoreEmp'];
            // var_dump($SelID);
            if ($SelID) {
                $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];
            }
            // var_dump($SelID);
            // var_dump($row2['Weightage']);
            // var_dump($all_dev[trim($row2["KPI"])]["Quantity_Value"]);

            echo '<div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';
            echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
            $kk = "GetTotal('AScoreEmp_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
            // echo '<select name="AScoreEmp_'.$row2['HashKey'].'" class="" id="AScoreEmp_'.$row2['HashKey'].'" onChange="'.$kk.'" >';
            // var_dump($SelID);
            echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreEmp_' . $row2['HashKey'] . '_try" class="" id="AScoreEmp_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreEmp_' . $row2['HashKey'] . '" class="" id="AScoreEmp_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div> ';

        }

    }

    ?>
										</td>
          								<td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "TDAScoreEmp_" . $row2['HashKey']; ?>">0.0</td>
										<td>
											<?php
if (strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
        $bg = 'info';
    } elseif (strlen(trim($row2['MComm'])) > 0 && !strlen(trim($row2['EComm'])) > 0) {
        $bg = 'success';
    } elseif (!strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
        $bg = 'danger';
    } else {
        $bg = 'default';
    }
    ?>
											<button type="button" class="btn btn-<?php echo $bg ?> btn-sm" alt="Edit Record" name="EditEdu<?php echo $Del; ?>" id="EditEdu<?php echo $Del; ?>" onClick="parent.ShowDisp('KPI&nbsp;Comment','kpi/KPI_Comm.php?&RecStatus=<?php echo $row2['Status']; ?>&EmpTyp=Emp&HID=<?php echo $row2['HashKey']; ?>',320,600,'Yes')">

												<span class="fa fa-edit"></span>
											</button>
											<!-- <img src="../images/bi_butn/EditData.jpg"  /> -->
										</td>
          							</tr>
          							<?php $kk_all = $kk_all . "<script type='text/javascript'>{" . $kk . ";}</script>";?>
							        <?php
}
include '../login/dbClose2.php';?>
									<tr>
										<td colspan="3" align="right" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col" >Score:</td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col"></td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col"><?php echo $TWeightage; ?>%</td>
										<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col" >
											<?php
echo $TotMgr . "%";
//echo '<input name="FScore" id="FScore" type="hidden" value="'.$TotMgr.'">';
echo '<input name="EmpTScore" id="EmpTScore" type="hidden" value="0">';
?>
										</td>
          								<!-- ============================extend here =========================== -->
										<?php
include 'level_check_mgr.php';
if ($appr3_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel3, 1, '.', '') . "%";
    print '</td>';
}

if ($appr4_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel4, 1, '.', '') . "%";
    print '</td>';
}

if ($appr5_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel5, 1, '.', '') . "%";
    print '</td>';
}

if ($appr6_emp == '1') {
    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
    echo number_format($TotAScoreLevel6, 1, '.', '') . "%";
    print '</td>';
}
?>
						                <!-- =================================================================== -->

							           <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col" id="TOTALTDAScoreEmp" name="TOTALTDAScoreEmp" > 0% </td>
							           <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col"> </td>
							           <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col"> </td>
          							</tr>
								</tbody>
      						</table>
    					<!-- </div> -->
    				</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<?php
include "final_score_total_2nd.php";
?>
								<?php
//Show goal form....
if ($goal_period_from_month != "") {
    print '<br/><br/>';
    include 'goal_obj_form_readonly.php';
    print '<br/><br/>';
}
?>
							</div>
							<div class="col-md-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12"  style="padding-left: 0px; padding-right: 0px;" >
									<div class="box-group text-center" style="font-weight: 550; font-size: 13px;">
										<div class="box bg-pale-secondary">
											<?php
$Script = "Select UpdatedBy, EmpUpdCnt, MgrUpdCnt from KPIFinalScore where EID='" . $EmpID . "' and [AQuartID]='" . $AQuartID_Val . "' AND Status <> 'D'";
$MaxIter = ScriptRunner($Script, "EmpUpdCnt");

$MaxIter_count = $MaxIter;
$Script = "Select ACount from [KPIStart] where [HashKey]='" . $AQuartID_Val . "'";
$MaxIterMax = ScriptRunner($Script, "ACount");

if ($MaxIterMax > 0) {$MaxIter = "Update Count: [" . $MaxIter . " of " . $MaxIterMax . "]";} else { $MaxIter = "Update Count: Unlimited";}
echo $MaxIter;

$alert = "<font size=\"3\"<b>You can update your record a maximum of " . $MaxIterMax . " unique times. You have updated (";
$alert .= $MaxIter_count . " out of " . $MaxIterMax . ")</b></font>";
error_reporting(E_ERROR | E_PARSE);
if ((!$_REQUEST['button']) && (!$_REQUEST["FAction"])) {
    echo ("<script type='text/javascript'>{parent.msgbox('$alert','red');}</script>");
}

$Script_Edit = "Select * from [KPIFinalScore] where  Status <> 'D' AND [AQuartID]='" . $AQuartID_Val . "' and [EID]=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') ))";
///*
$EmpTyp = "Emp";
if ($EmpTyp == "Mrg") {$ReOly = ' readonly="readonly" ';} elseif ($EmpTyp == "Emp") {$ReOly = '';}
//*/
//$EmpTyp = "Emp";
//$ReOly = ' readonly="readonly" ';
?>

										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
						<?php
$Script_set = "Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "'";
//$Script_set="Select HashKey from KPISetting where Status='A' and AName='eede8135f17bb7690ecb26dd49042bfb'";
$HashKey_set = ScriptRunner($Script_set, "HashKey");
$Script2 = "Select * from KPIFinalScore WHERE EID ='" . $_SESSION["StkTck" . "HKey"] . "' AND KPISetting_hashkey='" . $HashKey_set . "' AND [AQuartID]='" . $AQuartID_Val . "' AND Status <> 'D'";
//include '../login/dbOpen2.php';
$selMID = ScriptRunner($Script2, "MID");

$AprsID3 = ScriptRunner($Script2, "AprsID3");
$AprsName33 = ScriptRunner($Script2, "AprsName3");
$AprsComment3 = ScriptRunner($Script2, "AprsComment3");

$AprsID4 = ScriptRunner($Script2, "AprsID4");
$AprsName44 = ScriptRunner($Script2, "AprsName4");
$AprsComment4 = ScriptRunner($Script2, "AprsComment4");

$AprsID5 = ScriptRunner($Script2, "AprsID5");
$AprsName55 = ScriptRunner($Script2, "AprsName5");
$AprsComment5 = ScriptRunner($Script2, "AprsComment5");

$AprsID6 = ScriptRunner($Script2, "AprsID6");
$AprsName66 = ScriptRunner($Script2, "AprsName6");
$AprsComment6 = ScriptRunner($Script2, "AprsComment6");

$AprsID7 = ScriptRunner($Script2, "AprsID7");
$AprsName77 = ScriptRunner($Script2, "AprsName7");
$AprsComment7 = ScriptRunner($Script2, "AprsComment7");

$AprsID8 = ScriptRunner($Script2, "AprsID8");
$AprsName88 = ScriptRunner($Script2, "AprsName8");
$AprsComment8 = ScriptRunner($Script2, "AprsComment8");

$HashKey_sel = ScriptRunner($Script2, "HashKey");
$num_appraisers = ScriptRunner($Script2, "LUO_Num");

$ReOly1 = ' readonly="readonly" ';
$ReOly2 = ' readonly="readonly" ';
$ReOly3 = ' readonly="readonly" ';
$ReOly4 = ' readonly="readonly" ';
$ReOly5 = ' readonly="readonly" ';
$ReOly6 = ' readonly="readonly" ';
$ReOly7 = ' readonly="readonly" ';
$ReOly8 = ' readonly="readonly" ';

//print $num_appraisers;
//print $num_appraisers." $EmpTyp (". $who_is_appraising;

if (isset($AprsComment3)) {
    $apcom3 = $AprsComment3;
    $name3 = $AprsName33;
} else {
    $apcom3 = "";
    $name3 = "";
}

if (isset($AprsComment4)) {
    $apcom4 = $AprsComment4;
    $name4 = $AprsName44;
} else {
    $apcom4 = "";
    $name4 = "";
}

if (isset($AprsComment5)) {
    $apcom5 = $AprsComment5;
    $name5 = $AprsName55;
} else {
    $apcom5 = "";
    $name5 = "";
}

if (isset($AprsComment6)) {
    $apcom6 = $AprsComment6;
    $name6 = $AprsName66;
} else {
    $apcom6 = "";
    $name6 = "";
}

if (isset($AprsComment7)) {
    $apcom7 = $AprsComment7;
    $name7 = $AprsName77;
} else {
    $apcom7 = "";
    $name7 = "";
}

if (isset($AprsComment8)) {
    $apcom8 = $AprsComment8;
    $name8 = $AprsName88;
} else {
    $apcom8 = "";
    $name8 = "";
}
//----------------------------------------------- Summary Comment Box
include 'level_check_mgr.php';
?>
						<!-- ============================================== OUTGOING YEAR =========================================== -->
						<?php if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "Accomplishments") == '1'): ?>
							<?php
$data = ScriptRunner($Script_Edit, "Accomp");
$EmpAccomp = json_decode($data, true);
?>
		        				<div class="row">
		        					<!-- <div class="col-md-1"></div> -->
		        					<div class="col-md-12">
		        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
		        							<div class="box-group">
		        								<div class="box">
		        									<div class="box-body">
		        										<div class="row">
		        											<div class="col-6">
				        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
				        								            OUTGOING YEAR
				        										</p>
				        									</div>
		        										</div>
														<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
															<thead>
																<tr>
																	<th><div style="width: 150px">Accomplishments</div></th>
																	<th><div style="width: 150px">Supervisor's Comment</div></th>
																	<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																	<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>)</div></th>
																	<?php endif?>
																	<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																	<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>)</div></th>
																	<?php endif?>
																	<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																	<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>)</div></th>
																	<?php endif?>
																	<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																	<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>)</div></th>
																	<?php endif?>
																	<th></th>
																</tr>
															</thead>
															<?php if (isset($data) && $data !== ''): ?>
																<tbody class="input_fields_wrap fieldList">
																	<?php for ($i = 1; $i <= count($EmpAccomp); $i++): ?>
																		<tr id="row<?php echo $i ?>">
																			<td>
																				<div class="form-group">
																					<textarea name="accomplishments[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['employee_accomp']; ?></textarea>
																				</div>
																			</td>
																			<td>
																				<div class="form-group">
																					<textarea readonly name="mgr_accomp[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['mgr_accomp_comment']; ?></textarea>
																				</div>
																			</td>
																			<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																				<td>
																					<div class="form-group">
																						<textarea readonly name="apr3_accomp[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['apr3_accomp_comment']; ?></textarea>
																					</div>
																				</td>
																			<?php endif?>
																			<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																				<td>
																					<div class="form-group">
																						<textarea readonly name="apr4_accomp[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['apr4_accomp_comment']; ?></textarea>
																					</div>
																				</td>
																			<?php endif?>
																			<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																				<td>
																					<div class="form-group">
																						<textarea readonly name="apr5_accomp[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['apr5_accomp_comment']; ?></textarea>
																					</div>
																				</td>
																			<?php endif?>
																			<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																				<td>
																					<div class="form-group">
																						<textarea readonly name="apr6_accomp[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['apr6_accomp_comment']; ?></textarea>
																					</div>
																				</td>
																			<?php endif?>
																			<td>
																				<button style='margin-top: 10px' class='btn btn-danger btn_remove'  id='<?php echo $i ?>' ><i class='fa fa-remove' ></i></button>
																			</td>
																		</tr>
																	<?php endfor;?>
																</tbody>
															<?php else: ?>
																<tbody class="input_fields_wrap fieldList">
																	<tr id="row1">
																		<td>
																			<div class="form-group">
																				<textarea name="accomplishments[]" rows='1' class="form-control"></textarea>
																			</div>
																		</td>
																		<td>
																			<div class="form-group">
																				<textarea readonly name="mgr_accomp[]" rows='1' class="form-control"></textarea>
																			</div>
																		</td>
																		<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																			<td>
																				<div class="form-group">
																					<textarea readonly name="apr3_accomp" rows='1' class="form-control"></textarea>
																				</div>
																			</td>
																		<?php endif?>
																		<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																			<td>
																				<div class="form-group">
																					<textarea readonly name="apr4_accomp" rows='1' class="form-control"></textarea>
																				</div>
																			</td>
																		<?php endif?>
																		<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																			<td>
																				<div class="form-group">
																					<textarea readonly name="apr5_accomp" rows='1' class="form-control"></textarea>
																				</div>
																			</td>
																		<?php endif?>
																		<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																			<td>
																				<div class="form-group">
																					<textarea readonly name="apr6_accomp" rows='1' class="form-control"></textarea>
																				</div>
																			</td>
																		<?php endif?>
																		<td>
																			<button type="button" style='margin-top: 10px' class='btn btn-danger btn_remove'  id='1' ><i class='fa fa-remove' ></i></button>
																		</td>
																	</tr>
																</tbody>
															<?php endif?>
														</table>
														<button type="button" style='margin-top: 15px' class="btn btn-danger btn-md" id="addMore"><i class="fa fa-plus"></i></button> &nbsp;&nbsp;
		        									</div>
		        								</div>
		        							</div>
		        						</div>
		        					</div>
		        					<!-- <div class="col-md-1"></div> -->
		        				</div>
						<?php endif?>
						<!-- ======================================== EMPLOYEE STRENGHT AND WEAKNESS ===================== -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12"  style="padding-left: 0px; padding-right: 0px;" >
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
//Loop through the Header
$dbOpen2 = ("SELECT [Val1] FROM Masters where (ItemName='Appraisal_Header') AND Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';

$coun = 0;
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $header[$coun] = $row2['Val1'];
    $coun++;
}

if (isset($header[0])) {
    print $header[0];
} else {
    print " State Employee's Strength(s)";
}
?>
												</p>
												<p>
													<?php
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MStrn"  rows="3" class="form-control" id="MStrn" readonly="readonly">' . ScriptRunner($Script_Edit, "EmpStrenght") . '</textarea>';
} elseif (isset($_REQUEST["MStrn"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MStrn"  rows="3" class="form-control" id="MStrn" readonly="readonly">' . $_REQUEST["MStrn"] . '</textarea>';
} else {
    echo '<textarea name="MStrn"  rows="3" class="form-control" id="MStrn" readonly="readonly"></textarea>';
}
?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[1])) {
    print $header[1];
} else {
    print "State Employee's perceived weakness(es)";
}
?>
												</p>
												<p>
													<?php
//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly">' . ScriptRunner($Script_Edit, "EmpWeakness") . '</textarea>';
} elseif (isset($_REQUEST["MWeak"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly">' . $_REQUEST["MWeak"] . '</textarea>';
} else {
    echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly"></textarea>';
}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- ======================================================================================= -->
						<!-- ======================================== LEARNING & DEVELOPMENT ACTION ===================== -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[2])) {
    print $header[2];
} else {
    print "Identify Employee's Performance Gaps";
}
?>
												</p>
												<p>
													<?php
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly">' . ScriptRunner($Script_Edit, "EmpPerfGap") . '</textarea>';
} elseif (isset($_REQUEST["MGap"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly">' . $_REQUEST["MGap"] . '</textarea>';
} else {
    echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly"></textarea>';
}
?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
if (isset($header[3])) {
    print $header[3];
} else {
    print "State Remedial Actions that will be relevant over the next FY ";
}
?>
												</p>
												<p>
													<?php

//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
if ($EditID != "" || $EditID > 0) {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly">' . ScriptRunner($Script_Edit, "EmpRemAct") . '</textarea>';
} elseif (isset($_REQUEST["MRem"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly">' . $_REQUEST["MRem"] . '</textarea>';
} else {
    echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly"></textarea>';
}
?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
        				<!-- ========================================MEDICALS===============================================--------->
        				<div class="row">
        					<div class="col-md-1"></div>
        					<div class="col-md-10">
        						<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
        							<div class="box-group">
        								<div class="box bg-pale-secondary ">
        									<div class="box-body">
        										<div class="row">
        											<div class="col-6">
		        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
		        								            <?php
if (isset($header[4])) {
    print $header[4];
} else {
    print "Medical Fitness and Alertness";
}
?>
		        										</p>
		        									</div>
        										</div>
        										<small>
        											<?php
$Script5 = "SELECT [Desc] FROM Masters where (ItemName='Medical') AND Status<>'D'";
$medical_more_msg = ScriptRunner($Script5, "Desc");
if (isset($medical_more_msg)) {
    print $medical_more_msg;
}
?>
        										</small>
        										<p>
        											<?php
//-------------------------------------------------------
/*
//$Script = "Select Convert(varchar(20),LogDate,113) as LogDate, LogStatus, Session_ID from Users where LogName='Employee'";

$Script = "Select Convert(varchar(20),LogDate,109) as LogDate, LogStatus FROM Users WHERE LogDate < DATEADD(minute, -50, GETDATE()) AND LogName='Employee'";
$LogStatus=ScriptRunner($Script,"LogStatus");

$Script = "Update [Users] set [Session_ID]='".session_id()."' WHERE LogDate < DATEADD(minute, -30, GETDATE()) AND LogName='Employee'";
ScriptRunnerUD($Script,"Inst");

if(isset($LogStatus)){
print "yes <br/>";
print ScriptRunner($Script,"LogDate");
print "<br>".date('M d Y h:i:s');
}else{
print "no <br/>";
print ScriptRunner($Script,"LogDate");
}

 */
//------------------------------------------------------------------------------------------------------

if ($EditID != "" || $EditID > 0) {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly">' . ScriptRunner($Script_Edit, "MRecommendation") . '</textarea>';} elseif (isset($_REQUEST["MRecom"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly">' . $_REQUEST["MRecom"] . '</textarea>';} else {echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly"></textarea>';}
?>
        										</p>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="col-md-1"></div>
        				</div>
        				<!-- ==========================================RECCOMENDED=============================================--------->
        				<div class="row">
        					<div class="col-md-1"></div>
        					<div class="col-md-10">
        						<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
        							<div class="box-group">
        								<div class="box bg-pale-secondary ">
        									<div class="box-body">
        										<div class="row">
        											<div class="col-6">
		        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
		        								            Reccomended For
		        										</p>
		        									</div>
		        									<div class="col-6"></div>
        										</div>
        										<hr style="margin: 14px auto;">
        										<p style="font-weight: bold">
        											<div class="row">
        												<div class="col-sm-2">
        													<?php
if (ScriptRunner($Script_Edit, "MPromote") == 'on') {echo '<input disabled name="MPromote" id="MPromote" type="checkbox" checked="checked" />
							            		  					<label for="MPromote"  style="padding-left:17px;" >Training</label>';} else {echo '<input disabled name="MPromote" id="MPromote" type="checkbox" /><label for="MPromote"  style="padding-left:17px;" >Training</label>';}
?>
        												</div>
        												<div class="col-sm-2">
		    									            <?php
if (ScriptRunner($Script_Edit, "MPromote") == 'on') {echo '<input disabled name="MPromote" id="MPromote" type="checkbox" checked="checked" /><label for="MPromote" style="padding-left:17px;" >Promotion</label>';} else {echo '<input disabled name="MPromote" id="MPromote" type="checkbox" /><label for="MPromote" style="padding-left:17px;" >Promotion</label>';}
?>
											            </div>
											            <div class="col-sm-2">
		    									            <?php
if (ScriptRunner($Script_Edit, "MTransfer") == 'on') {echo '<input disabled name="MTransfer" id="MTransfer" type="checkbox" checked="checked" /><label for="MTransfer" style="padding-left:17px;" >Transfer</label>';} else {echo '<input disabled name="MTransfer" id="MTransfer" type="checkbox" /><label for="MTransfer" style="padding-left:17px;" >Transfer</label>';}
?>
											            </div>
											            <div class="col-sm-2">
		    									            <?php
if (ScriptRunner($Script_Edit, "MSalary") == 'on') {echo '<input disabled name="MSalary" id="MSalary" type="checkbox" checked="checked" /><label for="MSalary" style="padding-left:17px;" >Salary Adjustment</label>';} else {echo '<input disabled name="MSalary" id="MSalary" type="checkbox" /><label for="MSalary" style="padding-left:17px;" >Salary Adjustment</label>';}
?>
											            </div>
						            		  			<div class="col-sm-4">
		    									            <?php
if (ScriptRunner($Script_Edit, "MExtension") == 'on') {echo '<input disabled name="MExtension" id="MExtension" type="checkbox" checked="checked" /><label for="MExtension" style="padding-left:17px;" >Extension of Probationary Period</label>';} else {echo '<input disabled name="MExtension" id="MExtension" type="checkbox" /><label for="MExtension" style="padding-left:17px;" >Extension of Probationary Period</label>';}
?>
											            </div>
											            <div class="col-sm-4">
		    									            <?php
if (ScriptRunner($Script_Edit, "MReview") == 'on') {echo '<input disabled name="MReview" id="MReview" type="checkbox" checked="checked" /><label for="MReview" style="padding-left:17px;" >Review of Continued Employment</label>';} else {echo '<input disabled name="MReview" id="MReview" type="checkbox" /><label for="MReview" style="padding-left:17px;" >Review of Continued Employment</label>';}
?>
											            </div>
											            <div class="col-sm-5">
											             	<?php
if (ScriptRunner($Script_Edit, "MConfirmation") == 'on') {echo '<input disabled name="MConfirmation" id="MConfirmation" type="checkbox" checked="checked" />
							            		  					<label for="MConfirmation"  style="padding-left:17px;" >Confirmation of Appointment/Acting Appointment</label>';} else {echo '<input disabled name="MConfirmation" id="MConfirmation" type="checkbox" /><label for="MConfirmation"  style="padding-left:17px;" >Confirmation of Appointment/Acting Appointment</label>';}
?>
							            		  		</div>
        											</div>
        										</p>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="col-md-1"></div>
        				</div>
        				<!-- =============================================COMMENT==========================================--------->
        				<div class="row">
        					<div class="col-md-1"></div>
        					<div class="col-md-10">
        						<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
        							<div class="box-group">
        								<div class="box bg-pale-secondary">
        									<div class="box-body">
        										<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Employee's Comment (
        											<?php
$Script = "Select (SName + ' ' + FName)as Nm from [EmpTbl] where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
echo ScriptRunner($Script, "Nm");
?>
        										)</p>
        										<p>
        											<?php
if ($EditID != "" || $EditID > 0) {echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly . '>' . ScriptRunner($Script_Edit, "EComment") . '</textarea>';} elseif (isset($_REQUEST["EComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly . '>' . $_REQUEST["EComm"] . '</textarea>';} else {echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly . '></textarea>';}
?>
        										</p>
        									</div>
        								</div>
        								<div class="box bg-pale-secondary ">
        									<div class="box-body">
        										<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Supervisor's Comment</p>
        										<p>
        											<?php
if ($EmpTyp == "Emp") {$ReOly = ' readonly="readonly" ';} elseif ($EmpTyp == "Mrg") {$ReOly = '';}

$Script = "Select EComm, MComm from [KPIIndvScore] where HashKey='" . $EditID . "'";
if ($EditID != "" || $EditID > 0) {echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly . '>' . ScriptRunner($Script_Edit, "MComment") . '</textarea>';} elseif (isset($_REQUEST["MComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly . '>' . $_REQUEST["MComm"] . '</textarea>';} else {echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly . '></textarea>';}
?>
        										</p>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="col-md-1"></div>
        				</div>
        				<!-- =======================================================================================--------->
			            <!-- ================================================COMMENT 3 & 4=======================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 3rd Appraiser's Summary Comment (<?php echo $name3 ?>)</p>
				            						<p>
				            							<textarea name="3rdAps" rows="3" class="form-control" <?=$ReOly3?> >
				            								<?=$apcom3?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            				<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 4th Appraiser's Summary Comment (<?php echo $name4 ?>)</p>
				            						<p>
				            							<textarea name="4thAps" rows="3" class="form-control" <?=$ReOly4?> >
				            								<?=$apcom4?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- =======================================================================================--------->
			            <!-- ==============================================COMMENT 5 & 6=========================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 5th Appraiser's Summary Comment (<?php echo $name5 ?>)</p>
				            						<p>
				            							<textarea name="5thAps" rows="3" class="form-control" <?=$ReOly5?> >
				            								<?=$apcom5?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            				<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 6th Appraiser's Summary Comment (<?php echo $name6 ?>)</p>
				            						<p>
				            							<textarea name="6thAps" rows="3" class="form-control" <?=$ReOly6?> >
				            								<?=$apcom6?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- =======================================================================================--------->
			            <!-- ============================================== OBJECTIVES =========================================== -->
						<?php if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "show_objectives") == '1'): ?>
							<?php
$objectives = json_decode(ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "objectives"), true);
$obj_column = json_decode(ScriptRunner($Script_Edit, "objectives_remark"), true);
?>
	        				<div class="row">
	        					<!-- <div class="col-md-1"></div> -->
	        					<div class="col-md-12">
	        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
	        							<div class="box-group">
	        								<div class="box">
	        									<div class="box-body">
	        										<div class="row">
	        											<div class="col-6">
			        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
			        								            OBJECTIVES FOR THE COMING YEAR
			        										</p>
			        									</div>
	        										</div>
													<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
														<thead>
															<tr>
																<th><div style="width: 150px">Objectives</div></th>
																<th><div style="width: 150px">Remarks</div></th>
																<th><div style="width: 150px">Supervisor's Remark</div></th>
																<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>) Remark</div></th>
																<?php endif?>
																<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>) Remark</div></th>
																<?php endif?>
															</tr>
														</thead>
														<?php for ($i = 1; $i <= count($objectives); $i++): ?>
															<tbody class="fieldList">
																<tr id="row1">
																	<td>
																		<div class="form-group">
																			<textarea name="objectives[]" readonly rows='1' class="form-control"><?php echo $objectives[$i - 1]; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
if (!empty($obj_column[$i - 1]['emp_objectives'])) {
    $emp_objectives = trim($obj_column[$i - 1]['emp_objectives']);
} else {
    $emp_objectives = '';
}
?>
																			<textarea name="emp_objectives[]" rows='1' class="form-control"><?php echo $emp_objectives; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
if (!empty($obj_column[$i - 1]['mgr_objectives'])) {
    $mgr_objectives = trim($obj_column[$i - 1]['mgr_objectives']);
} else {
    $mgr_objectives = '';
}
?>
																			<textarea readonly name="mgr_objectives[]" rows='1' class="form-control"><?php echo $mgr_objectives; ?></textarea>
																		</div>
																	</td>
																	<?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr3_objectives'])) {
    $apr3_objectives = trim($obj_column[$i - 1]['apr3_objectives']);
} else {
    $apr3_objectives = '';
}
?>
																				<textarea readonly name="apr3_objectives[]" rows='1' class="form-control"><?php echo $apr3_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr4_objectives'])) {
    $apr4_objectives = trim($obj_column[$i - 1]['apr4_objectives']);
} else {
    $apr4_objectives = '';
}
?>
																				<textarea readonly name="apr4_objectives[]" rows='1' class="form-control"><?php echo $apr4_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr5_objectives'])) {
    $apr5_objectives = trim($obj_column[$i - 1]['apr5_objectives']);
} else {
    $apr5_objectives = '';
}
?>
																				<textarea readonly name="apr5_objectives[]" rows='1' class="form-control"><?php echo $apr5_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																	<?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
if (!empty($obj_column[$i - 1]['apr6_objectives'])) {
    $apr6_objectives = trim($obj_column[$i - 1]['apr6_objectives']);
} else {
    $apr6_objectives = '';
}
?>
																				<textarea readonly name="apr6_objectives[]" rows='1' class="form-control"><?php echo $apr6_objectives; ?></textarea>
																			</div>
																		</td>
																	<?php endif?>
																</tr>
															</tbody>
														<?php endfor?>
													</table>
	        									</div>
	        								</div>
	        							</div>
	        						</div>
	        					</div>
	        					<!-- <div class="col-md-1"></div> -->
	        				</div>
						<?php endif?>
			            <!-- =======================================================================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10 text-center">
	            		        <?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
			            			  <input name="FAction" id="FAction" type="hidden">
			            			  <input name="SndBk_Acct" id="SndBk_Acct" type="hidden" value="" />
			            		   	  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
			            			  <input name="num_appraisers" id="num_appraisers" type="hidden" value="' . $num_appraisers1 . '">
			            			  <input name="enforce_final" id="enforce_final" type="hidden" value="' . $enforce_final1 . '">
			            			  <input name="AQuartID" id="AQuartID" type="hidden" value="' . $_REQUEST["AQuartID"] . '"/>';

//if ($AuthRec=='N' || $AuthRec=='U')
//{
 ?>
	            				<!-- <input name="SubmitTrans" id="SubmitTrans" type="button" class="smallButton" value="Reject & Update" onClick="YesNo('Update KPI', 'Update')" /> -->
	            		        <br /><br />
           		                <!--<input type="submit" name="button" id="button" value="Save and Continue Later" class="smallButton" />-->
            		            <?php
$Script_kpi = "SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'";
$appr_allow_emp = ScriptRunner($Script_kpi, "Appr_allow_emp");
if (((isset($UpdUser) && $UpdUser == $_SESSION["StkTck" . "HKey"])) || ($AuthRec != 'N' && $AuthRec != 'U' && $AuthRec != 'S' && $AuthRec != 'E')) {
    echo "<input type=\"submit\" name=\"button\" id=\"button\" value=\"Save and Continue Later\" disabled class=\"btn btn-sm btn-danger\" />&nbsp;&nbsp;";
    echo " <input name=\"SubmitTrans\" id=\"SubmitTrans\" type=\"button\" disabled class=\"btn btn-sm btn-danger\" value=\"Submit for Supervisor's Confirmation\" />&nbsp;&nbsp;";
    if ($appr_allow_emp == '1') {
        echo '<input name="SubmitTrans" id="SubmitTrans" disabled type="button" class="btn btn-sm btn-danger" disabled value="FINAL SUBMISSION (Agree to ' . $mgr_name . '\'s score") />&nbsp;&nbsp;';
    }
} else {
    echo '<input type="submit" name="button" id="button" value="Save and Continue Later" class="btn btn-danger btn-sm" />&nbsp;&nbsp;';
    echo " <input name=\"SubmitTrans\" id=\"SubmitTrans\" type=\"button\" class=\"btn btn-danger btn-sm\" value=\"Submit for Supervisor's Confirmation\" onClick=\"YesNo('Update KPI', 'Update')\" />&nbsp;&nbsp;";
    if ($appr_allow_emp == '1') {
        if ($MID != "") {

            echo ' <input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="FINAL SUBMISSION (Agree to ' . $mgr_name . '\'s score") onClick="YesNo2(\'Final Submission Notice\', \'Agreed\')" />&nbsp;&nbsp;
		            		        			<br/>
		            		        		By clicking on the <b>FINAL SUBMISSION</b> button, any Employee\'s Score modification or Employee\'s Comment modification will not be saved, if you need to save this information then you must click the <b>Submit for Supervisor\'s Confirmation</b> button again.

		            		        		';
            /*
        echo ' <input name="SubmitTrans" id="SubmitTrans" type="button" class="smallButton" value="FINAL SUBMISSION (Agree to '.$mgr_name.'\'s score") onClick="YesNo2(\'Final Submission Notice\', \'Hello! You clicked on Final Submission. This does not submit your Appraisal to your Supervisor, rather it submits directly to your HOD. Click Final Submission to submit to your HOD or Click Cancel, then go back within this page to Click on Submit for Supervisors Confirmation.\')" />
        <br/>
        By clicking on the <b>FINAL SUBMISSION</b> button, any Employee\'s Score modification or Employee\'s Comment modification will not be saved, if you need to save this information then you must click the <b>Submit for Supervisor\'s Confirmation</b> button again.
        ';*/
        } else {
            echo '<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" disabled value="FINAL SUBMISSION (Agree to ' . $mgr_name . '\'s score") />';
        }
    }
}
?>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</form>
  	<?php echo $kk_all; ?>
	<script type="text/javascript">
		$(function() {
		    const max_fields = 5;
		    let wrapper = $(".input_fields_wrap");
		    let x = <?php echo (isset($data) && $data !== '') ? count($EmpAccomp) : 1 ?>;
		    $("#addMore").click(function(e) {
		      e.preventDefault();
		      if(x < max_fields){
		        ++x;
		        $(wrapper).append("<tr id='row"+x+"' >"
		        +"<td><div class='form-group'><textarea name='accomplishments[]' rows='1' class='form-control'></textarea></div></td>"
		        +"<td><div class='form-group'><textarea readonly rows='1' class='form-control'></textarea></div></td>"
		        <?php if ((isset($appr3_comment)) && ($appr3_comment == '1')): ?>
		        +"<td><div class='form-group'><textarea readonly rows='1' class='form-control'></textarea></div></td>"
		        <?php endif?>
		        <?php if ((isset($appr4_comment)) && ($appr4_comment == '1')): ?>
		        +"<td><div class='form-group'><textarea readonly rows='1' class='form-control'></textarea></div></td>"
		        <?php endif?>
		        <?php if ((isset($appr5_comment)) && ($appr5_comment == '1')): ?>
		        +"<td><div class='form-group'><textarea readonly rows='1' class='form-control'></textarea></div></td>"
		        <?php endif?>
		        <?php if ((isset($appr6_comment)) && ($appr6_comment == '1')): ?>
		        +"<td><div class='form-group'><textarea readonly rows='1' class='form-control'></textarea></div></td>"
		        <?php endif?>
		        +"<td><button type='button' style='margin-top: 10px' class='btn btn-danger btn_remove '  id='"+x+"' ><i class='fa fa-remove' ></i></button></td>"
		        +"</tr>");
		      }else{
		        // alert("You can only have a maximum of 10 action plans")
		        parent.msgbox(`You can only have a maximum of ${max_fields} accomplishments`, 'green');
		      }
		    });
		     $(document).on('click','.btn_remove', function(){
		        let button_id=$(this).attr("id");
		        // if (x > 0) {
		        	// x--;
		        // }
		        $("#row"+button_id+"").remove();
		        return false;
		    });
		})
	</script>
</body>

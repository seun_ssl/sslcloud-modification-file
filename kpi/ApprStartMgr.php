<?php
session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
//exit('See here ').ValidateURths("TEAM APPRAISAL"."V");
if (ValidateURths("TEAM APPRAISAL MGR" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}

/*
//AlL VARIABLES USED ARE DECLARED AT THIS POINT TO AVOID ANY ERRORS
 */
if (!isset($EditID)) {
    $EditID = "";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($SelID)) {
    $SelID = "";
}
if (!isset($Del)) {
    $Del = 0;
}
if (!isset($StatusSet)) {
    $StatusSet = 0;
}
if (!isset($KPICount)) {
    $KPICount = 0;
}
if (!isset($TWeightage)) {
    $TWeightage = 0;
}
if (!isset($TotMgr)) {
    $TotMgr = 0;
}
if (!isset($Total_Mgr_score)) {
    $Total_Mgr_score = 0;
}
if (!isset($o)) {
    $o = 0;
}
if (!isset($AllEmployee)) {
    $AllEmployee = "";
}
if (!isset($kk_all)) {
    $kk_all = "";
}
if (!isset($EScore)) {
    $EScore = "";
}
if (!isset($DrpCnt)) {
    $DrpCnt = 0;
}
if (!isset($UpdUser)) {
    $UpdUser = "";
}
if (!isset($AQuartID_Val)) {
    $AQuartID_Val = "";
}
if (!isset($AuthRec)) {
    $AuthRec = "";
}
if (!isset($total_weightage)) {
    $total_weightage = "";
}
if (!isset($total_sum)) {
    $total_sum = "";
}
if (!isset($TotAScoreLevel3)) {
    $TotAScoreLevel3 = "";
}
if (!isset($TotAScoreLevel4)) {
    $TotAScoreLevel4 = "";
}
if (!isset($TotAScoreLevel5)) {
    $TotAScoreLevel5 = "";
}
if (!isset($TotAScoreLevel6)) {
    $TotAScoreLevel6 = "";
}
if (!isset($final_score_type)) {
    $final_score_type = "";
}

$Script_hkey = "Select * from KPISettingNew where Status = 'A'";
$_SESSION['KPISettingNewHashKey'] = ScriptRunner($Script_hkey, "HashKey");

$if_enforce_comment_mgr = ScriptRunner($Script_hkey, "enforce_comment_mgr");
$enforce_summ_comm_mgr = ScriptRunner($Script_hkey, "enforce_summ_comm_mgr");

$final_score_setup = ScriptRunner($Script_hkey, "final_score_setup");
//$goal_obj=ScriptRunner($Script_hkey,"goal_obj");
$goal_desc = ScriptRunner($Script_hkey, "goal_desc");
$goal_period_from_month = ScriptRunner($Script_hkey, "goal_period_from_month");
$goal_period_from_year = ScriptRunner($Script_hkey, "goal_period_from_year");
$goal_period_to_month = ScriptRunner($Script_hkey, "goal_period_to_month");
$goal_period_to_year = ScriptRunner($Script_hkey, "goal_period_to_year");
$goal_header1 = ScriptRunner($Script_hkey, "goal_header1");
$goal_header2 = ScriptRunner($Script_hkey, "goal_header2");
$goal_header3 = ScriptRunner($Script_hkey, "goal_header3");
$goal_header4 = ScriptRunner($Script_hkey, "goal_header4");
$goal_num_row_allow = ScriptRunner($Script_hkey, "goal_num_row_allow");

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

//$mgr_name=ScriptRunner($Script,"Nm");
$mgr_name = $_SESSION["StkTck" . "FName"];

//Handle all the input for the Goal/Obj value
if (isset($_REQUEST["row1col1"])) {
    $row1col1 = $_REQUEST["row1col1"];
} else {
    $row1col1 = '';
}
if (isset($_REQUEST["row1col2"])) {
    $row1col2 = $_REQUEST["row1col2"];
} else {
    $row1col2 = '';
}
if (isset($_REQUEST["row1col3"])) {
    $row1col3 = $_REQUEST["row1col3"];
} else {
    $row1col3 = '';
}
if (isset($_REQUEST["row1col4"])) {
    $row1col4 = $_REQUEST["row1col4"];
} else {
    $row1col4 = '';
}

if (isset($_REQUEST["row2col1"])) {
    $row2col1 = $_REQUEST["row2col1"];
} else {
    $row2col1 = '';
}
if (isset($_REQUEST["row2col2"])) {
    $row2col2 = $_REQUEST["row2col2"];
} else {
    $row2col2 = '';
}
if (isset($_REQUEST["row2col3"])) {
    $row2col3 = $_REQUEST["row2col3"];
} else {
    $row2col3 = '';
}
if (isset($_REQUEST["row2col4"])) {
    $row2col4 = $_REQUEST["row2col4"];
} else {
    $row2col4 = '';
}

if (isset($_REQUEST["row3col1"])) {
    $row3col1 = $_REQUEST["row3col1"];
} else {
    $row3col1 = '';
}
if (isset($_REQUEST["row3col2"])) {
    $row3col2 = $_REQUEST["row3col2"];
} else {
    $row3col2 = '';
}
if (isset($_REQUEST["row3col3"])) {
    $row3col3 = $_REQUEST["row3col3"];
} else {
    $row3col3 = '';
}
if (isset($_REQUEST["row3col4"])) {
    $row3col4 = $_REQUEST["row3col4"];
} else {
    $row3col4 = '';
}

if (isset($_REQUEST["row4col1"])) {
    $row4col1 = $_REQUEST["row4col1"];
} else {
    $row4col1 = '';
}
if (isset($_REQUEST["row4col2"])) {
    $row4col2 = $_REQUEST["row4col2"];
} else {
    $row4col2 = '';
}
if (isset($_REQUEST["row4col3"])) {
    $row4col3 = $_REQUEST["row4col3"];
} else {
    $row4col3 = '';
}
if (isset($_REQUEST["row4col4"])) {
    $row4col4 = $_REQUEST["row4col4"];
} else {
    $row4col4 = '';
}

if (isset($_REQUEST["row5col1"])) {
    $row5col1 = $_REQUEST["row5col1"];
} else {
    $row5col1 = '';
}
if (isset($_REQUEST["row5col2"])) {
    $row5col2 = $_REQUEST["row5col2"];
} else {
    $row5col2 = '';
}
if (isset($_REQUEST["row5col3"])) {
    $row5col3 = $_REQUEST["row5col3"];
} else {
    $row5col3 = '';
}
if (isset($_REQUEST["row5col4"])) {
    $row5col4 = $_REQUEST["row5col4"];
} else {
    $row5col4 = '';
}

if (isset($_REQUEST["row6col1"])) {
    $row6col1 = $_REQUEST["row6col1"];
} else {
    $row6col1 = '';
}
if (isset($_REQUEST["row6col2"])) {
    $row6col2 = $_REQUEST["row6col2"];
} else {
    $row6col2 = '';
}
if (isset($_REQUEST["row6col3"])) {
    $row6col3 = $_REQUEST["row6col3"];
} else {
    $row6col3 = '';
}
if (isset($_REQUEST["row6col4"])) {
    $row6col4 = $_REQUEST["row6col4"];
} else {
    $row6col4 = '';
}

if (isset($_REQUEST["row7col1"])) {
    $row7col1 = $_REQUEST["row7col1"];
} else {
    $row7col1 = '';
}
if (isset($_REQUEST["row7col2"])) {
    $row7col2 = $_REQUEST["row7col2"];
} else {
    $row7col2 = '';
}
if (isset($_REQUEST["row7col3"])) {
    $row7col3 = $_REQUEST["row7col3"];
} else {
    $row7col3 = '';
}
if (isset($_REQUEST["row7col4"])) {
    $row7col4 = $_REQUEST["row7col4"];
} else {
    $row7col4 = '';
}

if (isset($_REQUEST["row8col1"])) {
    $row8col1 = $_REQUEST["row8col1"];
} else {
    $row8col1 = '';
}
if (isset($_REQUEST["row8col2"])) {
    $row8col2 = $_REQUEST["row8col2"];
} else {
    $row8col2 = '';
}
if (isset($_REQUEST["row8col3"])) {
    $row8col3 = $_REQUEST["row8col3"];
} else {
    $row8col3 = '';
}
if (isset($_REQUEST["row8col4"])) {
    $row8col4 = $_REQUEST["row8col4"];
} else {
    $row8col4 = '';
}

if (isset($_REQUEST["row9col1"])) {
    $row9col1 = $_REQUEST["row9col1"];
} else {
    $row9col1 = '';
}
if (isset($_REQUEST["row9col2"])) {
    $row9col2 = $_REQUEST["row9col2"];
} else {
    $row9col2 = '';
}
if (isset($_REQUEST["row9col3"])) {
    $row9col3 = $_REQUEST["row9col3"];
} else {
    $row9col3 = '';
}
if (isset($_REQUEST["row9col4"])) {
    $row9col4 = $_REQUEST["row9col4"];
} else {
    $row9col4 = '';
}

if (isset($_REQUEST["row10col1"])) {
    $row10col1 = $_REQUEST["row10col1"];
} else {
    $row10col1 = '';
}
if (isset($_REQUEST["row10col2"])) {
    $row10col2 = $_REQUEST["row10col2"];
} else {
    $row10col2 = '';
}
if (isset($_REQUEST["row10col3"])) {
    $row10col3 = $_REQUEST["row10col3"];
} else {
    $row10col3 = '';
}
if (isset($_REQUEST["row10col4"])) {
    $row10col4 = $_REQUEST["row10col4"];
} else {
    $row10col4 = '';
}

// Check if Manager is Final Appraisser
function isFinal()
{
    $Script_hkey = "Select * from KPISettingNew where Status = 'A'";
    $final_score_setup = ScriptRunner($Script_hkey, "final_score_setup");
    $kpi_Appr3_emp = ScriptRunner($Script_hkey, "Appr3_emp");
    $kpi_Appr3_deliv = ScriptRunner($Script_hkey, "Appr3_deliv");
    $kpi_Appr3_comment = ScriptRunner($Script_hkey, "Appr3_comment");
    $kpi_Appr3_section_comment = ScriptRunner($Script_hkey, "Appr3_section_comment");

    $kpi_Appr4_emp = ScriptRunner($Script_hkey, "Appr4_emp");
    $kpi_Appr4_deliv = ScriptRunner($Script_hkey, "Appr4_deliv");
    $kpi_Appr4_comment = ScriptRunner($Script_hkey, "Appr4_comment");
    $kpi_Appr4_section_comment = ScriptRunner($Script_hkey, "Appr4_section_comment");

    $kpi_Appr5_emp = ScriptRunner($Script_hkey, "Appr5_emp");
    $kpi_Appr5_deliv = ScriptRunner($Script_hkey, "Appr5_deliv");
    $kpi_Appr5_comment = ScriptRunner($Script_hkey, "Appr5_comment");
    $kpi_Appr5_section_comment = ScriptRunner($Script_hkey, "Appr5_section_comment");

    $kpi_Appr6_emp = ScriptRunner($Script_hkey, "Appr6_emp");
    $kpi_Appr6_deliv = ScriptRunner($Script_hkey, "Appr6_deliv");
    $kpi_Appr6_comment = ScriptRunner($Script_hkey, "Appr6_comment");
    $kpi_Appr6_section_comment = ScriptRunner($Script_hkey, "Appr6_section_comment");

    // var_dump("kpi_Appr3_emp", $kpi_Appr3_emp);
    // var_dump("kpi_Appr3_deliv", $kpi_Appr3_deliv);
    // var_dump("kpi_Appr3_comment", $kpi_Appr3_comment);
    // var_dump("kpi_Appr3_section_comment", $kpi_Appr3_section_comment);

    // var_dump("kpi_Appr4_emp", $kpi_Appr4_emp);
    // var_dump("kpi_Appr4_deliv", $kpi_Appr4_deliv);
    // var_dump("kpi_Appr4_comment", $kpi_Appr4_comment);
    // var_dump("kpi_Appr4_section_comment", $kpi_Appr4_section_comment);
    // var_dump("kpi_Appr5_emp", $kpi_Appr5_emp);
    // var_dump("kpi_Appr5_deliv", $kpi_Appr5_deliv);
    // var_dump("kpi_Appr5_comment", $kpi_Appr5_comment);
    // var_dump("kpi_Appr5_section_comment", $kpi_Appr5_section_comment);
    // var_dump("kpi_Appr6_emp", $kpi_Appr6_emp);
    // var_dump("kpi_Appr6_deliv", $kpi_Appr6_deliv);
    // var_dump("kpi_Appr6_comment", $kpi_Appr6_comment);
    // var_dump("kpi_Appr6_section_comment", $kpi_Appr6_section_comment);


    if (empty($kpi_Appr3_emp) && empty($kpi_Appr3_deliv) && empty($kpi_Appr3_comment) && empty($kpi_Appr3_section_comment) && empty($kpi_Appr4_emp) && empty($kpi_Appr4_deliv) && empty($kpi_Appr4_comment) && empty($kpi_Appr4_section_comment) && empty($kpi_Appr5_emp) && empty($kpi_Appr5_deliv) && empty($kpi_Appr5_comment) && empty($kpi_Appr5_section_comment) && empty($kpi_Appr6_emp) && empty($kpi_Appr6_deliv) && empty($kpi_Appr6_comment) && empty($kpi_Appr6_section_comment)) {
        return true;
    } else {
        return false;
    }
}

/*
//PROVIDES SECURITY FROM SUBMITTING UNVERIFIED DATA TO THE PAGE CODE HANDLER
//USE DoS TOKEN TO HOLD OUT AND NOT ALLOW THE FINAL VALUE PASS
 */
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    /*
    //IF AUTHORIZED BUTTON IS CLOCKED. PERFORM THIS ACTION
     */
    $Script288 = "Select * from KPISettingNew where Status = 'A'";
    $allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
    $Script289 = "Select * from [KPIFinalScore] where EID='" . $_REQUEST["ApprEmpID"] . "' and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $_REQUEST["DelBox"] . "') and Status <> 'D'";
    $secondary_mgr = ScriptRunner($Script289, "secondary_mgr");

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Agreed Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0 && (int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {
        unset($_SESSION['disable']);
        $totalScoreMgr = 0.0;
        $EditID = 0;
        $goOn = true;
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                //Employee Stenght/Weaknes
                $Status = 'A';
                if (isset($_REQUEST["MStrn"])) {
                    $MStrn_ = $_REQUEST["MStrn"];
                } else {
                    $MStrn_ = '';
                }
                if (isset($_REQUEST["MWeak"])) {
                    $MWeak_ = $_REQUEST["MWeak"];
                } else {
                    $MWeak_ = '';
                }
                if (isset($_REQUEST["MGap"])) {
                    $MGap_ = $_REQUEST["MGap"];
                } else {
                    $MGap_ = '';
                }
                if (isset($_REQUEST["MRem"])) {
                    $MRem_ = $_REQUEST["MRem"];
                } else {
                    $MRem_ = '';
                }
                $EditID = $_REQUEST["DelBox" . $php_i];
                $StatusSet = 'A';
                include 'enforce_comment.php';
                if (!$goOn) {
                    break;
                }
                //+++++++++++++++++++Start Enoforce Supervisor made Summary comment++++++++++++++++++
                if ((isset($enforce_summ_comm_mgr)) && ($enforce_summ_comm_mgr == "1")) {
                    //$Script="select HashKey from KPIStart where [Status]='A'";
                    //$ApprID=ScriptRunner($Script,"HashKey");
                    //$Script="Select * from KPIStart where HashKey='".ECh($_REQUEST["AQuartID"])."'";
                    if ($_REQUEST["MComm"] == "") {
                        // include '../main/prmt_blank.php';
                        echo ("<script type='text/javascript'>{parent.msgbox('SORRY, CANNOT BE SUBMITTED: Please you are enforced to make summary comment.', 'red');}</script>");
                        $goOn = false;
                        break;
                    }
                }
                $totalScoreMgr += ECh($_REQUEST["AScoreMrg_" . $EditID]);

                //---------------------- Just added -----------------------
                $Script = "Update [KPIIndvScore] set
					[AScoreMrg]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',

					[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',
					[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                ScriptRunnerUD($Script, "Inst");
                //------------------------------------ Issues here --------------------------------
                $Script = "Update [KPIIndvScore] set
					[AScoreFinal]=[AScoreMrg],
					[WScore]=((Weightage * AScoreEmp)/100),
					[SubmittedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
					[SubmittedDate]=GetDate()
					where [HashKey]= '" . ECh($EditID) . "'";
                ScriptRunnerUD($Script, "Inst");
                //Update Final Employee Score
                if ($php_i == 1) {
                    $Script = "Select DeptApprval from KPIDept where DeptID=(select Department from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                    if (ScriptRunner($Script, "DeptApprval") == "on") {
                        $StatusSet = "A";
                    } else {
                        $StatusSet = "PA";
                    } //Pending Authorization / Approval  PR= Pending Resolution PC=Pending Cancellation

                    if (isset($_REQUEST["MPromote"])) {
                        $MPromote = "on";
                    } else {
                        $MPromote = "";
                    }

                    if (isset($_REQUEST["MTraining"])) {
                        $MTraining = "on";
                    } else {
                        $MTraining = "";
                    }

                    if (isset($_REQUEST["MConfirmation"])) {
                        $MConfirmation = "on";
                    } else {
                        $MConfirmation = "";
                    }

                    if (isset($_REQUEST["MTransfer"])) {
                        $MTransfer = "on";
                    } else {
                        $MTransfer = "";
                    }

                    if (isset($_REQUEST["MSalary"])) {
                        $MSalary = "on";
                    } else {
                        $MSalary = "";
                    }

                    if (isset($_REQUEST["MExtension"])) {
                        $MExtension = "on";
                    } else {
                        $MExtension = "";
                    }

                    if (isset($_REQUEST["MReview"])) {
                        $MReview = "on";
                    } else {
                        $MReview = "";
                    }

                    $Script8 = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
                    $MgrHashKey = ScriptRunner($Script8, "MgrHashKey");
                    //<input type="checkbox" value="1" name="end_aprs">
                    // if(isset($_REQUEST['end_aprs'])&& $_REQUEST['end_aprs']=="1" || isFinal()){
                    //     $MgrHashKey="END"; //end appraisal
                    // }
                    if ((isset($_REQUEST['end_aprs']) && $_REQUEST['end_aprs'] == "1") || strlen($MgrHashKey) !== 32) {

                        $MgrHashKey = "END"; //end appraisal
                    }

                    $data = [];
                    if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                        for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                            $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '----';
                            $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                            $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                            $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                            $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                            $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                        }
                        if (count($data)) {
                            $Mgr_Accmp = json_encode($data);
                        } else {
                            $Mgr_Accmp = null;
                        }
                    } else {
                        $Mgr_Accmp = null;
                    }

                    $objective_data = [];
                    if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                        for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                            if (!empty($_REQUEST["objectives"][$i])) {
                                $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                                $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                            }
                        }
                        if (count($objective_data)) {
                            $objectivesData = json_encode($objective_data);
                        } else {
                            $objectivesData = null;
                        }
                    } else {
                        $objectivesData = null;
                    }
                    $Script = "Update [KPIFinalScore] set
						[MRecommendation]='" . ECh($_REQUEST["MRecom"]) . "',
						[MID]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
						[MComment]='" . ECh($_REQUEST["MComm"]) . "',
						[MPromote]='" . $MPromote . "',
						[MTraining]='" . $MTraining . "',
						[MConfirmation]='" . $MConfirmation . "',
						[MTransfer]='" . $MTransfer . "',
						[MExtension]='" . $MExtension . "',
						[MSalary]='" . $MSalary . "',
						[MReview]='" . $MReview . "',
						[SubmittedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
						[LUO_HashKey]='" . $MgrHashKey . "',
						[EmpStrenght]='" . ECh($MStrn_) . "',
						[EmpWeakness]='" . ECh($MWeak_) . "',
						[EmpPerfGap]='" . ECh($MGap_) . "',
						[EmpRemAct]='" . ECh($MRem_) . "',
						[Accomp]='" . $Mgr_Accmp . "',
						[MScore]=" . ECh($_REQUEST["MgrTScore"]) . ",
						[FScore] = " . ECh($_REQUEST["MgrTScore"]) . ",
						[objectives_remark]='" . $objectivesData . "',
						[row1_col1]='" . ECh($row1col1) . "',
						[row1_col2]='" . ECh($row1col2) . "',
						[row1_col3]='" . ECh($row1col3) . "',
						[row1_col4]='" . ECh($row1col4) . "',
						[row2_col1]='" . ECh($row2col1) . "',
						[row2_col2]='" . ECh($row2col2) . "',
						[row2_col3]='" . ECh($row2col3) . "',
						[row2_col4]='" . ECh($row2col4) . "',
						[row3_col1]='" . ECh($row3col1) . "',
						[row3_col2]='" . ECh($row3col2) . "',
						[row3_col3]='" . ECh($row3col3) . "',
						[row3_col4]='" . ECh($row3col4) . "',
						[row4_col1]='" . ECh($row4col1) . "',
						[row4_col2]='" . ECh($row4col2) . "',
						[row4_col3]='" . ECh($row4col3) . "',
						[row4_col4]='" . ECh($row4col4) . "',
						[row5_col1]='" . ECh($row5col1) . "',
						[row5_col2]='" . ECh($row5col2) . "',
						[row5_col3]='" . ECh($row5col3) . "',
						[row5_col4]='" . ECh($row5col4) . "',
						[row6_col1]='" . ECh($row6col1) . "',
						[row6_col2]='" . ECh($row6col2) . "',
						[row6_col3]='" . ECh($row6col3) . "',
						[row6_col4]='" . ECh($row6col4) . "',
						[row7_col1]='" . ECh($row7col1) . "',
						[row7_col2]='" . ECh($row7col2) . "',
						[row7_col3]='" . ECh($row7col3) . "',
						[row7_col4]='" . ECh($row7col4) . "',
						[row8_col1]='" . ECh($row8col1) . "',
						[row8_col2]='" . ECh($row8col2) . "',
						[row8_col3]='" . ECh($row8col3) . "',
						[row8_col4]='" . ECh($row8col4) . "',
						[row9_col1]='" . ECh($row9col1) . "',
						[row9_col2]='" . ECh($row9col2) . "',
						[row9_col3]='" . ECh($row9col3) . "',
						[row9_col4]='" . ECh($row9col4) . "',
						[row10_col1]='" . ECh($row10col1) . "',
						[row10_col2]='" . ECh($row10col2) . "',
						[row10_col3]='" . ECh($row10col3) . "',
						[row10_col4]='" . ECh($row10col4) . "',
						[SubmittedDate]=GetDate(),
						[AuthDate]=GetDate()
						where
						EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')
						and Status <> 'D'
						";
                    ScriptRunnerUD($Script, "CM");
                    $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $_REQUEST["ApprEmpID"] . "'";
                    $AprsNm = ScriptRunner($Script99, "AprsNm");

                    AuditLog("INSERT", "Final Submission for " . $AprsNm);
                    if ((isset($MgrHashKey)) && ($MgrHashKey == "END")) {
                        //Send email to 1st and 2nd person, appraiser ended by 2nd person

                        $mailer_from = "second person end";
                        include "mail_trail_send_kpi.php";
                    } else {
                        //+++++++++++++++++++++++ To the Manager/Supervisor

                        $Script288 = "Select * from KPISettingNew where Status = 'A'";
                        $allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
                        $Script289 = "Select * from [KPIFinalScore] where EID='" . $_REQUEST["ApprEmpID"] . "' and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "') and Status <> 'D'";
                        $secondary_mgr = ScriptRunner($Script289, "secondary_mgr");

                        if ((int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {

                            $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Update'";
                            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                            $Subj = ScriptRunner($Script_Mail, "MSub");

                            $Script20 = "select (Et.SName+' '+Et.FName)Nm from EmpTbl Et where Et.HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "'";
                            $empName = ScriptRunner($Script20, "Nm");

                            $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                            $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . $empName . '</strong>', $LoginDet);
                            $LoginDet = str_replace('#name_of_2nd_Appraiser#', ScriptRunner($Script, "Nm"), $LoginDet);
                            $To = ScriptRunner($Script, "Email");
                            if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                                QueueMail($To, $Subj, $LoginDet, '', '');
                            }

                            $Script = "Update [KPIFinalScore] set
						[secondary_mgr]='1',
						 [EmpUpdCnt]='0',
						[MgrUpdCnt]='0'
						where
						EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')
						and Status <> 'D'
						";
                            ScriptRunnerUD($Script, "CM");

                            $_SESSION['disable'] = true;
                        } else {

                            //Send email to third person
                            $mailer_from = "second person";
                            $mailer_to = "third person";
                            include "mail_trail_send_kpi.php";
                        }
                    }
                }
            }
        }
        if ($goOn) {

            if (isFinal()) {

                $Script = "UPDATE [KPIFinalScore] SET [FScore] = '" . $totalScoreMgr . "' WHERE EID='" . ECh($_REQUEST["ApprEmpID"]) . "' AND [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "') AND Status <> 'D'";
                ScriptRunnerUD($Script, "CM");
            }
            /* Prompt User to Select a record to edit - NO RECORD SELECTED */
            include '../main/prmt_blank.php';
            echo ("<script type='text/javascript'>{ parent.msgbox('Employee appraisal score has been submitted successfully.', 'green'); }</script>");
        }
    } else {
        // seun added this
        if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Agreed Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
            $totalScoreMgr = 0.0;
            $EditID = 0;
            $goOn = true;
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    //Employee Stenght/Weaknes
                    $Status = 'A';
                    if (isset($_REQUEST["MStrn"])) {
                        $MStrn_ = $_REQUEST["MStrn"];
                    } else {
                        $MStrn_ = '';
                    }
                    if (isset($_REQUEST["MWeak"])) {
                        $MWeak_ = $_REQUEST["MWeak"];
                    } else {
                        $MWeak_ = '';
                    }
                    if (isset($_REQUEST["MGap"])) {
                        $MGap_ = $_REQUEST["MGap"];
                    } else {
                        $MGap_ = '';
                    }
                    if (isset($_REQUEST["MRem"])) {
                        $MRem_ = $_REQUEST["MRem"];
                    } else {
                        $MRem_ = '';
                    }
                    $EditID = $_REQUEST["DelBox" . $php_i];
                    $StatusSet = 'A';
                    include 'enforce_comment.php';
                    if (!$goOn) {
                        break;
                    }
                    //+++++++++++++++++++Start Enoforce Supervisor made Summary comment++++++++++++++++++
                    if ((isset($enforce_summ_comm_mgr)) && ($enforce_summ_comm_mgr == "1")) {
                        //$Script="select HashKey from KPIStart where [Status]='A'";
                        //$ApprID=ScriptRunner($Script,"HashKey");
                        //$Script="Select * from KPIStart where HashKey='".ECh($_REQUEST["AQuartID"])."'";
                        if ($_REQUEST["MComm"] == "") {
                            // include '../main/prmt_blank.php';
                            echo ("<script type='text/javascript'>{parent.msgbox('SORRY, CANNOT BE SUBMITTED: Please you are enforced to make summary comment.', 'red');}</script>");
                            $goOn = false;
                            break;
                        }
                    }
                    $totalScoreMgr += ECh($_REQUEST["AScoreMrg_" . $EditID]);

                    //---------------------- Just added -----------------------
                    $Script = "Update [KPIIndvScore] set [AScoreMrg]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[Status]='" . $Status . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Inst");
                    //------------------------------------ Issues here --------------------------------
                    $Script = "Update [KPIIndvScore] set
					[AScoreFinal]=[AScoreMrg],
					[WScore]=((Weightage * AScoreEmp)/100),
					[Status]='" . $StatusSet . "',
					[SubmittedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
					[SubmittedDate]=GetDate()
					where [HashKey]= '" . ECh($EditID) . "'";
                    ScriptRunnerUD($Script, "Inst");
                    //Update Final Employee Score
                    if ($php_i == 1) {
                        $Script = "Select DeptApprval from KPIDept where DeptID=(select Department from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                        if (ScriptRunner($Script, "DeptApprval") == "on") {
                            $StatusSet = "A";
                        } else {
                            $StatusSet = "PA";
                        } //Pending Authorization / Approval  PR= Pending Resolution PC=Pending Cancellation

                        if (isset($_REQUEST["MPromote"])) {
                            $MPromote = "on";
                        } else {
                            $MPromote = "";
                        }

                        if (isset($_REQUEST["MTraining"])) {
                            $MTraining = "on";
                        } else {
                            $MTraining = "";
                        }

                        if (isset($_REQUEST["MConfirmation"])) {
                            $MConfirmation = "on";
                        } else {
                            $MConfirmation = "";
                        }

                        if (isset($_REQUEST["MTransfer"])) {
                            $MTransfer = "on";
                        } else {
                            $MTransfer = "";
                        }

                        if (isset($_REQUEST["MSalary"])) {
                            $MSalary = "on";
                        } else {
                            $MSalary = "";
                        }

                        if (isset($_REQUEST["MExtension"])) {
                            $MExtension = "on";
                        } else {
                            $MExtension = "";
                        }

                        if (isset($_REQUEST["MReview"])) {
                            $MReview = "on";
                        } else {
                            $MReview = "";
                        }

                        $Script8 = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
                        $MgrHashKey = ScriptRunner($Script8, "MgrHashKey");
                        //<input type="checkbox" value="1" name="end_aprs">
                        if (isset($_REQUEST['end_aprs']) && $_REQUEST['end_aprs'] == "1" || isFinal()) {
                            $MgrHashKey = "END"; //end appraisal
                        }

                        $data = [];
                        if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                            for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                                $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '----';
                                $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                                $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                                $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                                $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                                $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                            }
                            if (count($data)) {
                                $Mgr_Accmp = json_encode($data);
                            } else {
                                $Mgr_Accmp = null;
                            }
                        } else {
                            $Mgr_Accmp = null;
                        }

                        $objective_data = [];
                        if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                            for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                                if (!empty($_REQUEST["objectives"][$i])) {
                                    $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                    $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                    $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                                    $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                    $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                    $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                    $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                    $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                                }
                            }
                            if (count($objective_data)) {
                                $objectivesData = json_encode($objective_data);
                            } else {
                                $objectivesData = null;
                            }
                        } else {
                            $objectivesData = null;
                        }
                        $Script = "Update [KPIFinalScore] set
						[MRecommendation]='" . ECh($_REQUEST["MRecom"]) . "',
						[MID]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
						[MComment]='" . ECh($_REQUEST["MComm"]) . "',
						[MPromote]='" . $MPromote . "',
						[MTraining]='" . $MTraining . "',
						[MConfirmation]='" . $MConfirmation . "',
						[MTransfer]='" . $MTransfer . "',
						[MExtension]='" . $MExtension . "',
						[MSalary]='" . $MSalary . "',
						[MReview]='" . $MReview . "',
						[SubmittedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
						[LUO_HashKey]='" . $MgrHashKey . "',
						[EmpStrenght]='" . ECh($MStrn_) . "',
						[EmpWeakness]='" . ECh($MWeak_) . "',
						[EmpPerfGap]='" . ECh($MGap_) . "',
						[EmpRemAct]='" . ECh($MRem_) . "',
						[Accomp]='" . $Mgr_Accmp . "',
						[MScore]=" . ECh($_REQUEST["MgrTScore"]) . ",
						[FScore] = " . ECh($_REQUEST["MgrTScore"]) . ",
						[objectives_remark]='" . $objectivesData . "',
						[row1_col1]='" . ECh($row1col1) . "',
						[row1_col2]='" . ECh($row1col2) . "',
						[row1_col3]='" . ECh($row1col3) . "',
						[row1_col4]='" . ECh($row1col4) . "',
						[row2_col1]='" . ECh($row2col1) . "',
						[row2_col2]='" . ECh($row2col2) . "',
						[row2_col3]='" . ECh($row2col3) . "',
						[row2_col4]='" . ECh($row2col4) . "',
						[row3_col1]='" . ECh($row3col1) . "',
						[row3_col2]='" . ECh($row3col2) . "',
						[row3_col3]='" . ECh($row3col3) . "',
						[row3_col4]='" . ECh($row3col4) . "',
						[row4_col1]='" . ECh($row4col1) . "',
						[row4_col2]='" . ECh($row4col2) . "',
						[row4_col3]='" . ECh($row4col3) . "',
						[row4_col4]='" . ECh($row4col4) . "',
						[row5_col1]='" . ECh($row5col1) . "',
						[row5_col2]='" . ECh($row5col2) . "',
						[row5_col3]='" . ECh($row5col3) . "',
						[row5_col4]='" . ECh($row5col4) . "',
						[row6_col1]='" . ECh($row6col1) . "',
						[row6_col2]='" . ECh($row6col2) . "',
						[row6_col3]='" . ECh($row6col3) . "',
						[row6_col4]='" . ECh($row6col4) . "',
						[row7_col1]='" . ECh($row7col1) . "',
						[row7_col2]='" . ECh($row7col2) . "',
						[row7_col3]='" . ECh($row7col3) . "',
						[row7_col4]='" . ECh($row7col4) . "',
						[row8_col1]='" . ECh($row8col1) . "',
						[row8_col2]='" . ECh($row8col2) . "',
						[row8_col3]='" . ECh($row8col3) . "',
						[row8_col4]='" . ECh($row8col4) . "',
						[row9_col1]='" . ECh($row9col1) . "',
						[row9_col2]='" . ECh($row9col2) . "',
						[row9_col3]='" . ECh($row9col3) . "',
						[row9_col4]='" . ECh($row9col4) . "',
						[row10_col1]='" . ECh($row10col1) . "',
						[row10_col2]='" . ECh($row10col2) . "',
						[row10_col3]='" . ECh($row10col3) . "',
						[row10_col4]='" . ECh($row10col4) . "',
						[SubmittedDate]=GetDate(),
						[Status]='" . $Status . "',
						[AuthDate]=GetDate()
						where
						EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')
						and Status <> 'D'
						";
                        ScriptRunnerUD($Script, "CM");
                        $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $_REQUEST["ApprEmpID"] . "'";
                        $AprsNm = ScriptRunner($Script99, "AprsNm");

                        AuditLog("INSERT", "Final Submission for " . $AprsNm);
                        if ((isset($MgrHashKey)) && ($MgrHashKey == "END")) {
                            //Send email to 1st and 2nd person, appraiser ended by 2nd person

                            $mailer_from = "second person end";
                            include "mail_trail_send_kpi.php";
                        } else {
                            //+++++++++++++++++++++++ To the Manager/Supervisor

                            $Script288 = "Select * from KPISettingNew where Status = 'A'";
                            $allow_emp_mgr_1 = ScriptRunner($Script288, "allow_emp_mgr_1");
                            $Script289 = "Select * from [KPIFinalScore] where EID='" . $_REQUEST["ApprEmpID"] . "' and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "') and Status <> 'D'";
                            $secondary_mgr = ScriptRunner($Script289, "secondary_mgr");

                            if ((int) $allow_emp_mgr_1 === 1 && (int) $secondary_mgr === 0) {

                                $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Update'";
                                $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                                $Subj = ScriptRunner($Script_Mail, "MSub");

                                $Script20 = "select (Et.SName+' '+Et.FName)Nm from EmpTbl Et where Et.HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "'";
                                $empName = ScriptRunner($Script20, "Nm");

                                $Script = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey=(Select EmpMgr from EmpTbl where HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "')";
                                $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . $empName . '</strong>', $LoginDet);
                                $LoginDet = str_replace('#name_of_2nd_Appraiser#', ScriptRunner($Script, "Nm"), $LoginDet);
                                $To = ScriptRunner($Script, "Email");
                                if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                                    QueueMail($To, $Subj, $LoginDet, '', '');
                                }

                                $Script = "Update [KPIFinalScore] set
						[secondary_mgr]='1'
						where
						EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "')
						and Status <> 'D'
						";
                                ScriptRunnerUD($Script, "CM");
                            } else {

                                //Send email to third person
                                $mailer_from = "second person";
                                $mailer_to = "third person";
                                include "mail_trail_send_kpi.php";
                            }
                        }
                    }
                }
            }
            if ($goOn) {


                if (isFinal()) {

                    $Script = "UPDATE [KPIFinalScore] SET [FScore] = '" . $totalScoreMgr . "' WHERE EID='" . ECh($_REQUEST["ApprEmpID"]) . "' AND [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . ECh($EditID) . "') AND Status <> 'D'";
                    ScriptRunnerUD($Script, "CM");
                }
                /* Prompt User to Select a record to edit - NO RECORD SELECTED */
                include '../main/prmt_blank.php';
                echo ("<script type='text/javascript'>{ parent.msgbox('Employee appraisal score has been submitted successfully.', 'green'); }</script>");
            }
        }
    }

    if (isset($_REQUEST['button']) && $_REQUEST['button'] == "Save and Continue Later") {

        //exit('Stop here');

        $EditID = "";

        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $php_i]);
                if ($php_i == 1) {
                    $Status = 'U'; //default Update Status
                    //$Status = 'S';

                    if (isset($_REQUEST["MPromote"])) {
                        $MPromote_ = $_REQUEST["MPromote"];
                    } else {
                        $MPromote_ = '';
                    }
                    if (isset($_REQUEST["MTraining"])) {
                        $MTraining_ = $_REQUEST["MTraining"];
                    } else {
                        $MTraining_ = '';
                    }

                    if (isset($_REQUEST["MConfirmation"])) {
                        $MConfirmation_ = $_REQUEST["MConfirmation"];
                    } else {
                        $MConfirmation_ = "";
                    }

                    if (isset($_REQUEST["MTransfer"])) {
                        $MTransfer_ = $_REQUEST["MTransfer"];
                    } else {
                        $MTransfer_ = "";
                    }

                    if (isset($_REQUEST["MSalary"])) {
                        $MSalary_ = $_REQUEST["MSalary"];
                    } else {
                        $MSalary_ = "";
                    }

                    if (isset($_REQUEST["MExtension"])) {
                        $MExtension_ = $_REQUEST["MExtension"];
                    } else {
                        $MExtension_ = "";
                    }

                    if (isset($_REQUEST["MReview"])) {
                        $MReview_ = $_REQUEST["MReview"];
                    } else {
                        $MReview_ = "";
                    }

                    //----------------------------------------------- Employee Stenght/Weaknes
                    if (isset($_REQUEST["MStrn"])) {
                        $MStrn_ = $_REQUEST["MStrn"];
                    } else {
                        $MStrn_ = '';
                    }

                    if (isset($_REQUEST["MWeak"])) {
                        $MWeak_ = $_REQUEST["MWeak"];
                    } else {
                        $MWeak_ = '';
                    }

                    if (isset($_REQUEST["MGap"])) {
                        $MGap_ = $_REQUEST["MGap"];
                    } else {
                        $MGap_ = '';
                    }

                    if (isset($_REQUEST["MRem"])) {
                        $MRem_ = $_REQUEST["MRem"];
                    } else {
                        $MRem_ = '';
                    }

                    //$Script8="SELECT MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='".ECh($_SESSION["StkTck"."HKey"])."'";
                    //$MgrHashKey=ScriptRunner($Script8,"MgrHashKey");

                    $Script8 = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
                    $MgrHashKey = ScriptRunner($Script8, "MgrHashKey");

                    $data = [];
                    if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                        for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                            $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '----';
                            $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                            $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                            $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                            $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                            $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                        }
                        if (count($data)) {
                            $Mgr_Accmp = json_encode($data);
                        } else {
                            $Mgr_Accmp = null;
                        }
                    } else {
                        $Mgr_Accmp = null;
                    }

                    $objective_data = [];
                    if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                        for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                            if (!empty($_REQUEST["objectives"][$i])) {
                                $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                                $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                            }
                        }
                        if (count($objective_data)) {
                            $objectivesData = json_encode($objective_data);
                        } else {
                            $objectivesData = null;
                        }
                    } else {
                        $objectivesData = null;
                    }

                    $Script = "Update [KPIFinalScore] set
							[MRecommendation]='" . ECh($_REQUEST["MRecom"]) . "',
							[MComment]='" . ECh($_REQUEST["MComm"]) . "',
							[MPromote]='" . ECh($MPromote_) . "',
							[MTraining]='" . ECh($MTraining_) . "',
							[MConfirmation]='" . ECh($MConfirmation_) . "',
							[MTransfer]='" . ECh($MTransfer_) . "',
							[MExtension]='" . ECh($MExtension_) . "',
							[MSalary]='" . ECh($MSalary_) . "',
							[MReview]='" . ECh($MReview_) . "',
							[EmpStrenght]='" . ECh($MStrn_) . "',
							[EmpWeakness]='" . ECh($MWeak_) . "',
							[EmpPerfGap]='" . ECh($MGap_) . "',
							[EmpRemAct]='" . ECh($MRem_) . "',
							[Status]='" . $Status . "',
							[Accomp]='" . $Mgr_Accmp . "',
							[objectives_remark]='" . $objectivesData . "',
							[row1_col1]='" . ECh($row1col1) . "',
							[row1_col2]='" . ECh($row1col2) . "',
							[row1_col3]='" . ECh($row1col3) . "',
							[row1_col4]='" . ECh($row1col4) . "',

							[row2_col1]='" . ECh($row2col1) . "',
							[row2_col2]='" . ECh($row2col2) . "',
							[row2_col3]='" . ECh($row2col3) . "',
							[row2_col4]='" . ECh($row2col4) . "',

							[row3_col1]='" . ECh($row3col1) . "',
							[row3_col2]='" . ECh($row3col2) . "',
							[row3_col3]='" . ECh($row3col3) . "',
							[row3_col4]='" . ECh($row3col4) . "',

							[row4_col1]='" . ECh($row4col1) . "',
							[row4_col2]='" . ECh($row4col2) . "',
							[row4_col3]='" . ECh($row4col3) . "',
							[row4_col4]='" . ECh($row4col4) . "',

							[row5_col1]='" . ECh($row5col1) . "',
							[row5_col2]='" . ECh($row5col2) . "',
							[row5_col3]='" . ECh($row5col3) . "',
							[row5_col4]='" . ECh($row5col4) . "',

							[row6_col1]='" . ECh($row6col1) . "',
							[row6_col2]='" . ECh($row6col2) . "',
							[row6_col3]='" . ECh($row6col3) . "',
							[row6_col4]='" . ECh($row6col4) . "',

							[row7_col1]='" . ECh($row7col1) . "',
							[row7_col2]='" . ECh($row7col2) . "',
							[row7_col3]='" . ECh($row7col3) . "',
							[row7_col4]='" . ECh($row7col4) . "',

							[row8_col1]='" . ECh($row8col1) . "',
							[row8_col2]='" . ECh($row8col2) . "',
							[row8_col3]='" . ECh($row8col3) . "',
							[row8_col4]='" . ECh($row8col4) . "',

							[row9_col1]='" . ECh($row9col1) . "',
							[row9_col2]='" . ECh($row9col2) . "',
							[row9_col3]='" . ECh($row9col3) . "',
							[row9_col4]='" . ECh($row9col4) . "',

							[row10_col1]='" . ECh($row10col1) . "',
							[row10_col2]='" . ECh($row10col2) . "',
							[row10_col3]='" . ECh($row10col3) . "',
							[row10_col4]='" . ECh($row10col4) . "',
							[UpdatedDate]=GetDate()
							where
							EID='" . $_REQUEST["ApprEmpID"] . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
							and Status <> 'D'
							";
                    //echo $Script;
                    ScriptRunnerUD($Script, "CM");
                }

                $Script = "Update [KPIIndvScore] set [AScoreMrg]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[Status]='" . $Status . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                ScriptRunnerUD($Script, "Inst");

                //AuditLog("UPDATE","Employee appriasal successfully updated by manager");

                echo ("<script type='text/javascript'>{ parent.msgbox('Your appraisal was successfully saved, you can continue later...', 'green'); }</script>");
            }
        }

        $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $_REQUEST["ApprEmpID"] . "'";
        $AprsNm = ScriptRunner($Script99, "AprsNm");

        AuditLog("INSERT", "Saved and Continue appraisal later for " . $AprsNm);
    }
    //======================================================= SAVE FOR EMPLOYEE COMMENT
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        $EditID = 0;
        $goOn = true;
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = $_REQUEST["DelBox" . $php_i];
                include 'enforce_comment.php';
                if (!$goOn) {
                    break;
                }
                //+++++++++++++++++++Enoforce Supervisor made Summary comment++++++++++++++++++
                if ((isset($enforce_summ_comm_mgr)) && ($enforce_summ_comm_mgr == "1")) {
                    //$Script="select HashKey from KPIStart where [Status]='A'";
                    //$ApprID=ScriptRunner($Script,"HashKey");
                    //$Script="Select * from KPIStart where HashKey='".ECh($_REQUEST["AQuartID"])."'";
                    if ($_REQUEST["MComm"] == "") {
                        echo ("<script type='text/javascript'>{parent.msgbox('SORRY, CANNOT BE SUBMITTED: Please you are enforced to make summary comment.', 'red');}</script>");
                        $goOn = false;
                        break;
                    }
                }
                if ($php_i == 1) {
                    $Status = 'E'; //default Update Status
                    $Script = "Select MgrUpdCnt from KPIFinalScore where Status <> 'D' and EID='" . $_REQUEST["ApprEmpID"] . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')";
                    $MaxIter = ScriptRunner($Script, "MgrUpdCnt");
                    if ($MaxIter > 0) {
                        //$Script="select DeptMaxCnt from KPIDept where DeptID=(
                        //select Department from EmpTbl where HashKey='".$_REQUEST["ApprEmpID"]."')";

                        $Script = "select ACount from KPIStart where [HashKey]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')";
                        if ((ScriptRunner($Script, "ACount") - $MaxIter) <= 0) { //Means max iteration has been reachd. Force Submit the record
                            $Status = 'PR'; /*Resolution - Disables update or submit button for both manager and employee */
                            //$Status = 'U';
                        }
                    }
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    $Script = "Select UpdatedBy from KPIFinalScore where Status <> 'D' and EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')";
                    if (ScriptRunner($Script, "UpdatedBy") != $_SESSION["StkTck" . "HKey"]) {
                        $Script = "Update [KPIFinalScore] set
							[MID]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
							[MScore]=" . ECh($_REQUEST["MgrTScore"]) . ",
							[MgrUpdCnt]=MgrUpdCnt + 1
							where EID='" . ECh($_REQUEST["ApprEmpID"]) . "'
							and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
							and Status <> 'D'
							";
                        ScriptRunnerUD($Script, "Ct");
                    }
                    if (isset($_REQUEST["MPromote"])) {
                        $MPromote_ = $_REQUEST["MPromote"];
                    } else {
                        $MPromote_ = '';
                    }
                    if (isset($_REQUEST["MTraining"])) {
                        $MTraining_ = $_REQUEST["MTraining"];
                    } else {
                        $MTraining_ = '';
                    }
                    if (isset($_REQUEST["MConfirmation"])) {
                        $MConfirmation_ = $_REQUEST["MConfirmation"];
                    } else {
                        $MConfirmation_ = "";
                    }
                    if (isset($_REQUEST["MTransfer"])) {
                        $MTransfer_ = $_REQUEST["MTransfer"];
                    } else {
                        $MTransfer_ = "";
                    }
                    if (isset($_REQUEST["MSalary"])) {
                        $MSalary_ = $_REQUEST["MSalary"];
                    } else {
                        $MSalary_ = "";
                    }
                    if (isset($_REQUEST["MExtension"])) {
                        $MExtension_ = $_REQUEST["MExtension"];
                    } else {
                        $MExtension_ = "";
                    }
                    if (isset($_REQUEST["MReview"])) {
                        $MReview_ = $_REQUEST["MReview"];
                    } else {
                        $MReview_ = "";
                    }
                    // Employee Stenght/Weaknes
                    if (isset($_REQUEST["MStrn"])) {
                        $MStrn_ = $_REQUEST["MStrn"];
                    } else {
                        $MStrn_ = '';
                    }
                    if (isset($_REQUEST["MWeak"])) {
                        $MWeak_ = $_REQUEST["MWeak"];
                    } else {
                        $MWeak_ = '';
                    }
                    if (isset($_REQUEST["MGap"])) {
                        $MGap_ = $_REQUEST["MGap"];
                    } else {
                        $MGap_ = '';
                    }
                    if (isset($_REQUEST["MRem"])) {
                        $MRem_ = $_REQUEST["MRem"];
                    } else {
                        $MRem_ = '';
                    }
                    $Script8 = "SELECT Et.EmpMgr as MgrHashKey FROM EmpTbl Et WHERE Et.HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
                    $MgrHashKey = ScriptRunner($Script8, "MgrHashKey");

                    $data = [];
                    if (isset($_REQUEST["accomplishments"]) && !empty($_REQUEST["accomplishments"])) {
                        for ($i = 0; $i < count($_REQUEST["accomplishments"]); $i++) {
                            $mgr_accomp_comment = (isset($_REQUEST["mgr_accomp"][$i]) && !empty($_REQUEST["mgr_accomp"][$i])) ? sanitize(ECh($_REQUEST["mgr_accomp"][$i])) : '----';
                            $apr3_accomp_comment = (isset($_REQUEST["apr3_accomp"][$i]) && !empty($_REQUEST["apr3_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr3_accomp"][$i])) : '';
                            $apr4_accomp_comment = (isset($_REQUEST["apr4_accomp"][$i]) && !empty($_REQUEST["apr4_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr4_accomp"][$i])) : '';
                            $apr5_accomp_comment = (isset($_REQUEST["apr5_accomp"][$i]) && !empty($_REQUEST["apr5_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr5_accomp"][$i])) : '';
                            $apr6_accomp_comment = (isset($_REQUEST["apr6_accomp"][$i]) && !empty($_REQUEST["apr6_accomp"][$i])) ? sanitize(ECh($_REQUEST["apr6_accomp"][$i])) : '';
                            $data[] = ['employee_accomp' => sanitize(ECh($_REQUEST["accomplishments"][$i])), 'mgr_accomp_comment' => $mgr_accomp_comment, 'apr3_accomp_comment' => $apr3_accomp_comment, 'apr4_accomp_comment' => $apr4_accomp_comment, 'apr5_accomp_comment' => $apr5_accomp_comment, 'apr6_accomp_comment' => $apr6_accomp_comment];
                        }
                        if (count($data)) {
                            $Mgr_Accmp = json_encode($data);
                        } else {
                            $Mgr_Accmp = null;
                        }
                    } else {
                        $Mgr_Accmp = null;
                    }

                    $objective_data = [];
                    if (isset($_REQUEST["objectives"]) && !empty($_REQUEST["objectives"])) {
                        for ($i = 0; $i < count($_REQUEST["objectives"]); $i++) {
                            if (!empty($_REQUEST["objectives"][$i])) {
                                $objectives = (isset($_REQUEST["objectives"][$i]) && !empty($_REQUEST["objectives"][$i])) ? sanitize(ECh($_REQUEST["objectives"][$i])) : '';
                                $emp_objectives = (isset($_REQUEST["emp_objectives"][$i]) && !empty($_REQUEST["emp_objectives"][$i])) ? sanitize(ECh($_REQUEST["emp_objectives"][$i])) : '';
                                $mgr_objectives = (isset($_REQUEST["mgr_objectives"][$i]) && !empty($_REQUEST["mgr_objectives"][$i])) ? sanitize(ECh($_REQUEST["mgr_objectives"][$i])) : '----';
                                $apr3_objectives = (isset($_REQUEST["apr3_objectives"][$i]) && !empty($_REQUEST["apr3_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr3_objectives"][$i])) : '';
                                $apr4_objectives = (isset($_REQUEST["apr4_objectives"][$i]) && !empty($_REQUEST["apr4_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr4_objectives"][$i])) : '';
                                $apr5_objectives = (isset($_REQUEST["apr5_objectives"][$i]) && !empty($_REQUEST["apr5_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr5_objectives"][$i])) : '';
                                $apr6_objectives = (isset($_REQUEST["apr6_objectives"][$i]) && !empty($_REQUEST["apr6_objectives"][$i])) ? sanitize(ECh($_REQUEST["apr6_objectives"][$i])) : '';
                                $objective_data[] = ['objectives' => $objectives, 'emp_objectives' => $emp_objectives, 'mgr_objectives' => $mgr_objectives, 'apr3_objectives' => $apr3_objectives, 'apr4_objectives' => $apr4_objectives, 'apr5_objectives' => $apr5_objectives, 'apr6_objectives' => $apr6_objectives];
                            }
                        }
                        if (count($objective_data)) {
                            $objectivesData = json_encode($objective_data);
                        } else {
                            $objectivesData = null;
                        }
                    } else {
                        $objectivesData = null;
                    }

                    $Script = "Update [KPIFinalScore] set
						[MID]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
						[MRecommendation]='" . ECh($_REQUEST["MRecom"]) . "',
						[MComment]='" . ECh($_REQUEST["MComm"]) . "',
						[MPromote]='" . ECh($MPromote_) . "',
						[MTraining]='" . ECh($MTraining_) . "',
						[MConfirmation]='" . ECh($MConfirmation_) . "',
						[MTransfer]='" . ECh($MTransfer_) . "',
						[MExtension]='" . ECh($MExtension_) . "',
						[MSalary]='" . ECh($MSalary_) . "',
						[MReview]='" . ECh($MReview_) . "',
						[EmpStrenght]='" . ECh($MStrn_) . "',
						[EmpWeakness]='" . ECh($MWeak_) . "',
						[EmpPerfGap]='" . ECh($MGap_) . "',
						[EmpRemAct]='" . ECh($MRem_) . "',
						[Status]='" . $Status . "',
						[Accomp]='" . $Mgr_Accmp . "',
						[objectives_remark]='" . $objectivesData . "',
						[row1_col1]='" . ECh($row1col1) . "',
						[row1_col2]='" . ECh($row1col2) . "',
						[row1_col3]='" . ECh($row1col3) . "',
						[row1_col4]='" . ECh($row1col4) . "',
						[row2_col1]='" . ECh($row2col1) . "',
						[row2_col2]='" . ECh($row2col2) . "',
						[row2_col3]='" . ECh($row2col3) . "',
						[row2_col4]='" . ECh($row2col4) . "',
						[row3_col1]='" . ECh($row3col1) . "',
						[row3_col2]='" . ECh($row3col2) . "',
						[row3_col3]='" . ECh($row3col3) . "',
						[row3_col4]='" . ECh($row3col4) . "',
						[row4_col1]='" . ECh($row4col1) . "',
						[row4_col2]='" . ECh($row4col2) . "',
						[row4_col3]='" . ECh($row4col3) . "',
						[row4_col4]='" . ECh($row4col4) . "',

						[row5_col1]='" . ECh($row5col1) . "',
						[row5_col2]='" . ECh($row5col2) . "',
						[row5_col3]='" . ECh($row5col3) . "',
						[row5_col4]='" . ECh($row5col4) . "',

						[row6_col1]='" . ECh($row6col1) . "',
						[row6_col2]='" . ECh($row6col2) . "',
						[row6_col3]='" . ECh($row6col3) . "',
						[row6_col4]='" . ECh($row6col4) . "',

						[row7_col1]='" . ECh($row7col1) . "',
						[row7_col2]='" . ECh($row7col2) . "',
						[row7_col3]='" . ECh($row7col3) . "',
						[row7_col4]='" . ECh($row7col4) . "',

						[row8_col1]='" . ECh($row8col1) . "',
						[row8_col2]='" . ECh($row8col2) . "',
						[row8_col3]='" . ECh($row8col3) . "',
						[row8_col4]='" . ECh($row8col4) . "',

						[row9_col1]='" . ECh($row9col1) . "',
						[row9_col2]='" . ECh($row9col2) . "',
						[row9_col3]='" . ECh($row9col3) . "',
						[row9_col4]='" . ECh($row9col4) . "',

						[row10_col1]='" . ECh($row10col1) . "',
						[row10_col2]='" . ECh($row10col2) . "',
						[row10_col3]='" . ECh($row10col3) . "',
						[row10_col4]='" . ECh($row10col4) . "',

						[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',

						[UpdatedDate]=GetDate()
						where
						EID='" . $_REQUEST["ApprEmpID"] . "'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '" . $EditID . "')
						and Status <> 'D'
						";
                    //echo $Script;
                    ScriptRunnerUD($Script, "CM");
                }
                //Update Regular Score
                $Script = "Update [KPIIndvScore] set [AScoreMrg]='" . ECh($_REQUEST["AScoreMrg_" . $EditID]) . "',[Status]='" . $Status . "',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $EditID . "'";
                ScriptRunnerUD($Script, "Inst");
                if ($php_i == 1) {
                    $Script99 = "SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='" . $_REQUEST["ApprEmpID"] . "'";
                    $AprsNm = ScriptRunner($Script99, "AprsNm");
                    AuditLog("INSERT", "Submit Appraisal for Employee Confirmation for " . $AprsNm);
                    echo ("<script type='text/javascript'>{ parent.msgbox('Employee appraisal score Successfully Submitted.', 'green'); }</script>");
                    $Script_nm = "select (Et.SName+' '+Et.FName)Nm, Email from EmpTbl Et where Et.HashKey='" . ECh($_REQUEST["ApprEmpID"]) . "'";
                    //+++++++++++++++++++++++ To the Manager/Supervisor
                    $Script_Mail = "Select * from MailTemp where MSub='Appraisal Score Updated by Supervisor'"; //Appraisal Submitted
                    $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                    $Subj = ScriptRunner($Script_Mail, "MSub");
                    $LoginDet = str_replace('#name_of_2nd_Appraiser#', ECh($_SESSION["StkTck" . "FName"]), $LoginDet);
                    $LoginDet = str_replace('#name_of_1st_Appraiser#', '<strong>' . ScriptRunner($Script_nm, "Nm") . '</strong>', $LoginDet);

                    if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", ECh($_SESSION["StkTck" . "Email"]))) {
                        QueueMail(ECh($_SESSION["StkTck" . "Email"]), $Subj, $LoginDet, '', '');
                    }

                    //To the Employee
                    $Script_Mail = "Select * from MailTemp where MSub='Appraisal Submitted by Supervisor'";
                    $LoginDet = ScriptRunner($Script_Mail, "MMsg");
                    $Subj = ScriptRunner($Script_Mail, "MSub");
                    $LoginDet = str_replace('#name_of_2nd_Appraiser#', '<strong>' . ECh($_SESSION["StkTck" . "FName"]) . '</strong>', $LoginDet);
                    $LoginDet = str_replace('#name_of_1st_Appraiser#', ScriptRunner($Script_nm, "Nm"), $LoginDet);
                    $To = ScriptRunner($Script_nm, "Email");
                    if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $To)) {
                        QueueMail($To, $Subj, $LoginDet, '', '');
                    }
                }
            }
        }
        include '../main/prmt_blank.php';
        /* Clear ID to avoid opening this record */
    }
}

?>


<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<script type="text/javascript">
    function GetTotal($ObjName, $ObjWhtID, $weight, $quantity) {
        $kk = document.getElementById("Wght" + $ObjName).innerHTML;
        //document.getElementById("TD"+$ObjName).innerHTML = ((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);
        if ($quantity) {
            let actual = document.getElementById($ObjName + "_try").value;
            if (parseInt(actual) >= parseInt($quantity)) {
                actual = $quantity;
            }

            document.getElementById("TD" + $ObjName).innerHTML = (($kk * actual) / $quantity).toFixed(1);
            document.getElementById($ObjName).value = (($kk * actual) / $quantity).toFixed(1);
        } else {
            document.getElementById("TD" + $ObjName).innerHTML = ((1 * document.getElementById($ObjName).value)).toFixed(2);

        }


        document.getElementById("Wth" + $ObjWhtID).value = document.getElementById("TD" + $ObjName).innerHTML;

        //alert (document.getElementById("DelMax").value);
        document.getElementById("TOTALTDAScoreMrg").innerHTML = 0;
        for ($i = 1; $i <= document.getElementById("DelMax").value; $i++) {
            calcScore = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML) + parseFloat(document.getElementById("Wth" + $i).value);
            document.getElementById("TOTALTDAScoreMrg").innerHTML = calcScore
            document.getElementById("MgrTScore").value = parseFloat(calcScore).toFixed(1);
        }
        //Get value of total Managers score in an INPUT box
        document.getElementById("TOTALTDAScoreMrg").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1) + '%';
        //	}
    }

    function GetTotalTags($ObjName, $ObjWhtID, $weight) {
        $kk = document.getElementById("Wght" + $ObjName).innerHTML;
        //document.getElementById("TD"+$ObjName).innerHTML = ((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);
        document.getElementById("TD" + $ObjName).innerHTML = ((1 * document.getElementById($ObjName).value)).toFixed(2);
        document.getElementById("Wth" + $ObjWhtID).value = document.getElementById("TD" + $ObjName).innerHTML;

        //alert (document.getElementById("DelMax").value);
        document.getElementById("TOTALTDAScoreMrg").innerHTML = 0;
        for ($i = 1; $i <= document.getElementById("DelMax").value; $i++) {
            document.getElementById("TOTALTDAScoreMrg").innerHTML =
                parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML) + parseFloat(document.getElementById("Wth" + $i).value);
        }
        //Get value of total Managers score in an INPUT box
        document.getElementById("MgrTScore").value = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1);
        document.getElementById("TOTALTDAScoreMrg").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreMrg").innerHTML).toFixed(1) + '%';
        //	}
    }
</script>

<script>
    $(function() {
        $(document).tooltip();
    });
</script>

<?php
/////////////////////////POP UP INSTRUCTION //////////////////
/*
$dbOpen2 = ("SELECT [Val1],[Desc] FROM Masters where (ItemName='Instruction') AND Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';

$alert="INSTRUCTION <br/>";
while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
$alert .= strtoupper($row2['Val1'])."<br/>";
$alert .= $row2['Desc']."<br/>";
}

//print $alert;

echo ("<script type='text/javascript'>{parent.msgbox('$alert','red');}</script>");
//exit();
 */
//////////////////////////////////////////////////////////////
?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
    strong {
        font-weight: 550;
    }

    [type=checkbox]+label:before,
    [type=checkbox]:not(.filled-in)+label:after {

        width: 15px;
        height: 15px;
    }

    .fieldList .form-group {
        margin-bottom: 0px;
    }
</style>

<body oncontextmenu="return false;">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-7">
                        <div class="row" style="padding-bottom: 10px;">
                            <div class="col-md-12" style="padding-bottom: 10px;"><strong>Employee:
                                    <span>
                                        <?php
                                        if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
                                            echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="' . $_REQUEST["AcctNo"] . '" />';
                                            if (isset($_REQUEST["AcctNo"])) {
                                                $EmpID = ECh($_REQUEST["AcctNo"]);
                                                $EditID = ECh($_REQUEST["AcctNo"]);
                                            }
                                        } else {
                                            echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="';
                                            if (isset($_REQUEST["ApprEmpID"])) {
                                                echo $_REQUEST["ApprEmpID"];
                                            }
                                            echo '" />';
                                            if (isset($_REQUEST["ApprEmpID"])) {
                                                $EmpID = $_REQUEST["ApprEmpID"];
                                                $EditID = $_REQUEST["ApprEmpID"];
                                            }
                                        }
                                        $Script = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.HashKey='" . $EditID . "'";
                                        echo ScriptRunner($Script, "Nm");
                                        ?>
                                    </span></strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><strong>Select an Employee</strong></label>
                            <div class="col-sm-8 input-group input-group-sm">
                                <select name="AcctNo" class="form-control" id="AcctNo">
                                    <?php
                                    // modified by Seun
                                    // ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' and EmpMgr='".$_SESSION["StkTck"."HKey"]."' order by SName Asc")
                                    echo '<option value="--" selected="selected">--</option>';
                                    $dbOpen2 = ("SELECT * from EmpTbl where (Status='A') and [EmpStatus]='Active' and EmpMgr='" . $_SESSION["StkTck" . "HKey"] . "'  or  EmpMgr_1='" . $_SESSION["StkTck" . "HKey"] . "'  order by SName Asc");
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($EditID == $row2['HashKey']) {
                                            $AllEmployee = $AllEmployee . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                        } else {
                                            $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                        }
                                    }
                                    echo $AllEmployee;
                                    include '../login/dbClose2.php';
                                    ?>
                                </select>
                                <span class="input-group-btn ">
                                    <input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php
                    if (!isset($_REQUEST["AcctNo"])) {
                        //---------------------------------------
                        $dbOpen2 = ("SELECT [Val1],[Desc] FROM Masters where (ItemName='Instruction') AND Status<>'D' ORDER BY Val1");
                        include '../login/dbOpen2.php';
                        print "
								<div class=\"col-md-12\">
									<div class=\"row\">
										<div class=\"col-md-1\"></div>
										<div class=\"col-md-10\">
											<div class=\"row\">
												<div class=\"row\">
													<div class=\"col-md-12 tdMenu_HeadBlock text-center\">
														INSTRUCTIONS
													</div>";
                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                            print "
								<div class=\"col-md-12 tdMenu_HeadBlock_Light text-center\">
									" . $row2['Val1'] . "
								</div>
								<div class=\"col-md-12 tdMenu_HeadBlock_Light text-center\">
									" . $row2['Desc'] . "
								</div>
							  ";
                        }
                        print "</div></div><div class=\"col-md-1\"></div></div></div>";
                        //---------------------------------------
                        exit;
                    }
                    //GET THE ID FOR THE CURRENT OPEN APPRAISAL FOR THE SELECTED EMPLOYEE
                    ?>


                    <?php
                    $all_dev = [];
                    $query = "select [KPI],[KRA],[Status],[Score_Type]
      ,[Quantity_Value] from KPICore where Status in ('A') and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $EditID . "')";
                    //    "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."' and Status in('A') )))";
                    $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                    $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                    if ($conn2 === false) {
                        echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                    }
                    $res2 = sqlsrv_query($conn2, $query);
                    while ($row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_ASSOC)) {
                        //   $all_dev[trim($row['KPI'])]=$row;
                        $all_dev[ECh(trim($row['KPI']))] = $row;

                        //   array_push($all_dev,$row);
                    }

                    // var_dump($all_dev);

                    ?>







                    <div class="col-md-12" style="overflow-x:scroll">
                        <!-- <div class="ViewPatch"> -->
                        <table class="table-bordered table table-responsive" id="">
                            <thead>
                                <?php $Script = ("Select AQuartID from KPIIndvScore where Status <>'D'
									and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59')
									 and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $EditID . "')");
                                $Script = "Select * from KPIStart where HashKey='" . ECh(ScriptRunner($Script, "AQuartID")) . "'";
                                $GradeType = ScriptRunner($Script, "ScoreType"); ?>
                                <tr class="">
                                    <th>Area of Focus</th>
                                    <th>Sub Area of Focus</th>
                                    <th>Departmental Objectives</th>
                                    <th>Individaul Performance Objectives</th>
                                    <th>Weightage</th>
                                    <th>Employee's<br>
                                        Score</th>
                                    <!-- ================================== extend here ========================== -->
                                    <?php
                                    include 'level_check_mgr.php';
                                    if ($appr3_emp == '1') {
                                        print '<th>Level 3 Appraiser<br>
											            Score
											          </th>';
                                    }
                                    if ($appr4_emp == '1') {
                                        print '<th>Level 4 Appraiser<br>
											            Score
											          </th>';
                                    }
                                    if ($appr5_emp == '1') {
                                        print '<th>Level 5 Appraiser<br>
											            Score
											          </th>';
                                    }
                                    if ($appr6_emp == '1') {
                                        print '<th>Level 6 Appraiser<br>
											            Score
											          </th>';
                                    }
                                    ?>
                                    <!-- ========================================================================= -->
                                    <th>Supervisor's<br>
                                        Score
                                    </th>
                                    <th>Supervisor's<br>
                                        Score</th>
                                    <th>
                                        <!-- <img src="../images/icon_print_.gif" title="Print blank paper form" name="EditEdu<?php echo $Del; ?>" height="10" border="0" id="EditEdu<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('View&nbsp;Employee&nbsp;KPI/KRA','kpi/Rpt_KPIDtl_Blk.php?APPID=<?php echo ScriptRunner($Script, "AQuartID"); ?>&HID=<?php echo $EmpID; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/icon_print_.gif',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" /> -->
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Del = 0;
                                $dbOpen2 = ("Select * from KPIFinalScore where AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59') and (Status <>'D' AND Status <>'S') and EID ='" . $EditID . "'");
                                $UpdUser = ScriptRunner($dbOpen2, "UpdatedBy");
                                $MID = ScriptRunner($dbOpen2, "MID");
                                $EID = ScriptRunner($dbOpen2, "EID");
                                $MgrFinal = ScriptRunner($dbOpen2, "MgrFinal");
                                $final_score_value = ScriptRunner($dbOpen2, "FScore");

                                $row1_col1 = ScriptRunner($dbOpen2, "row1_col1");
                                $row1_col2 = ScriptRunner($dbOpen2, "row1_col2");
                                $row1_col3 = ScriptRunner($dbOpen2, "row1_col3");
                                $row1_col4 = ScriptRunner($dbOpen2, "row1_col4");

                                $row2_col1 = ScriptRunner($dbOpen2, "row2_col1");
                                $row2_col2 = ScriptRunner($dbOpen2, "row2_col2");
                                $row2_col3 = ScriptRunner($dbOpen2, "row2_col3");
                                $row2_col4 = ScriptRunner($dbOpen2, "row2_col4");

                                $row3_col1 = ScriptRunner($dbOpen2, "row3_col1");
                                $row3_col2 = ScriptRunner($dbOpen2, "row3_col2");
                                $row3_col3 = ScriptRunner($dbOpen2, "row3_col3");
                                $row3_col4 = ScriptRunner($dbOpen2, "row3_col4");

                                $row4_col1 = ScriptRunner($dbOpen2, "row4_col1");
                                $row4_col2 = ScriptRunner($dbOpen2, "row4_col2");
                                $row4_col3 = ScriptRunner($dbOpen2, "row4_col3");
                                $row4_col4 = ScriptRunner($dbOpen2, "row4_col4");

                                $row5_col1 = ScriptRunner($dbOpen2, "row5_col1");
                                $row5_col2 = ScriptRunner($dbOpen2, "row5_col2");
                                $row5_col3 = ScriptRunner($dbOpen2, "row5_col3");
                                $row5_col4 = ScriptRunner($dbOpen2, "row5_col4");

                                $row6_col1 = ScriptRunner($dbOpen2, "row6_col1");
                                $row6_col2 = ScriptRunner($dbOpen2, "row6_col2");
                                $row6_col3 = ScriptRunner($dbOpen2, "row6_col3");
                                $row6_col4 = ScriptRunner($dbOpen2, "row6_col4");

                                $row7_col1 = ScriptRunner($dbOpen2, "row7_col1");
                                $row7_col2 = ScriptRunner($dbOpen2, "row7_col2");
                                $row7_col3 = ScriptRunner($dbOpen2, "row7_col3");
                                $row7_col4 = ScriptRunner($dbOpen2, "row7_col4");

                                $row8_col1 = ScriptRunner($dbOpen2, "row8_col1");
                                $row8_col2 = ScriptRunner($dbOpen2, "row8_col2");
                                $row8_col3 = ScriptRunner($dbOpen2, "row8_col3");
                                $row8_col4 = ScriptRunner($dbOpen2, "row8_col4");

                                $row9_col1 = ScriptRunner($dbOpen2, "row9_col1");
                                $row9_col2 = ScriptRunner($dbOpen2, "row9_col2");
                                $row9_col3 = ScriptRunner($dbOpen2, "row9_col3");
                                $row9_col4 = ScriptRunner($dbOpen2, "row9_col4");

                                $row10_col1 = ScriptRunner($dbOpen2, "row10_col1");
                                $row10_col2 = ScriptRunner($dbOpen2, "row10_col2");
                                $row10_col3 = ScriptRunner($dbOpen2, "row10_col3");
                                $row10_col4 = ScriptRunner($dbOpen2, "row10_col4");

                                $dbOpen2 = ("Select * from KPIIndvScore where (Status <>'D' AND Status <>'S')
										and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59')
										 and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $EditID . "') ORDER BY KRA_Group");

                                //echo $dbOpen2;

                                include '../login/dbOpen2.php';
                                /*
$prevValue = "";

while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
$Del = $Del + 1;
//$UpdUser = $row2['UpdatedBy'];
$AuthRec = $row2['Status'];
$AQuartID_Val=$row2['AQuartID'];

$KRA_Group_value = $row2['KRA_Group'];
if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)){
print "
<tr class=\"tdMenu_HeadBlock_Light\">
<td align=\"center\" valign=\"middle\" c\"9\" ><b>$KRA_Group_value</b></td>
</tr>
";

$prevValue = $KRA_Group_value;
}
 */
                                $prevValue = "";
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    $Del = $Del + 1;
                                    //$UpdUser = $row2['UpdatedBy'];
                                    $AuthRec = $row2['Status'];
                                    $AQuartID_Val = $row2['AQuartID'];
                                    $FHashKey = $row2['FinalHashKey']; //new function, for secction comment
                                    $KRA_Group_value = $row2['KRA_Group'];
                                    //records=$row2['KRA_Group'];
                                    if ((isset($KRA_Group_value)) && ($KRA_Group_value != $prevValue)) {
                                        //------ Option to display Section Comment
                                        if ($appr2_section == '1') {

                                            $icon = "<button type=\"button\"
														title=\"Edit Record\"
														name=\"EditEdu" . $Del . "\"
														id=\"EditEdu" . $Del . "\"
														onClick=\"parent.ShowDisp('KPI&nbsp;Section&nbsp;Comment','kpi/KPI_CommSection.php?kra_group=" . $KRA_Group_value . "&RecStatus=" . $row2['Status'] . "&EmpTyp=Mrg&HID=" . $FHashKey . "',320,600,'Yes')\"
														class=\"btn btn-default btn-sm\" style=\"line-height: 17px\">
														<i class=\"fa fa-edit\"></i></button>";
                                        } else {
                                            $icon = "";
                                        }
                                        //----------------------------------------
                                        print "
												<tr class=\"tdMenu_HeadBlock_Light\">
													<td align=\"center\" valign=\"middle\" colspan=\"11\" style=\"padding-bottom: 5px; padding-top:5px;\"><strong>" . $icon . " $KRA_Group_value</strong></td>
												</tr>
												";
                                        $prevValue = $KRA_Group_value;
                                    }
                                ?>
                                    <tr>
                                        <?php
                                        //This makes the check boxes into hidden fields
                                        echo '<input name="' . ("DelBox" . $Del) . '" type="hidden" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" /><label for="' . ("DelBox" . $Del) . '"></label>';
                                        ?>
                                        <td>
                                            <?php
                                            echo "<b>" . (trim($row2['KRA'])) . "</b>";
                                            // echo "<br/>".(trim($row2['ARemark']));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo "<b>" . (trim($row2['KRA_Sub_Area'])) . "</b>";
                                            // echo "<br/>".(trim($row2['ARemark']));
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            echo (trim($row2['KPI'])) . "<br/>";
                                            $measures = '';
                                            for ($i = 0; $i <= 6; $i++) {
                                                if (trim($row2['Opt' . $i])) {
                                                    $measures .= trim($row2['Opt' . $i]) . "; ";
                                                }
                                            }
                                            if ($measures) {
                                                echo 'Measures: ' . $measures;
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            // echo "<b>".(trim($row2['KRA']))."</b>";
                                            //   echo "<br/>".(trim($row2['ARemark']));
                                            echo "<b>" . (trim($row2['ARemark'])) . "</b>";
                                            ?>
                                        </td>

                                        <td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "WghtAScoreMrg_" . $row2['HashKey']; ?>"><?php echo (trim($row2['Weightage']));
                                                                                                                                                                    $TWeightage = $TWeightage + $row2['Weightage']; ?></td>
                                        <td align="center" valign="middle" class="subHeader" scope="col">
                                            <?php
                                            $TotMgr = $TotMgr + number_format($row2['AScoreEmp'], 2, '.', '');

                                            $Total_Mgr_score = $Total_Mgr_score + $row2['AScoreMrg'];

                                            //echo  number_format(($row2['AScoreEmp']*$row2['Weightage'])/100, 2, '.', '');
                                            echo number_format($row2['AScoreEmp'], 2, '.', '');
                                            ?>
                                        </td>
                                        <!-- ============================indv extend here =========================== -->
                                        <?php
                                        include 'level_check_mgr.php';
                                        if ($appr3_emp == '1') {
                                            print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
                                            $TotAScoreLevel3 = $TotAScoreLevel3 + number_format($row2['AScoreLevel3Comm'], 1, '.', '');
                                            echo number_format($row2['AScoreLevel3Comm'], 1, '.', '');
                                            print '</td>';
                                        }

                                        if ($appr4_emp == '1') {
                                            print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
                                            $TotAScoreLevel4 = $TotAScoreLevel4 + number_format($row2['AScoreLevel4Comm'], 1, '.', '');
                                            echo number_format($row2['AScoreLevel4Comm'], 1, '.', '');
                                            print '</td>';
                                        }

                                        if ($appr5_emp == '1') {
                                            print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
                                            $TotAScoreLevel5 = $TotAScoreLevel5 + number_format($row2['AScoreLevel5Comm'], 1, '.', '');
                                            echo number_format($row2['AScoreLevel5Comm'], 1, '.', '');
                                            print '</td>';
                                        }

                                        if ($appr6_emp == '1') {
                                            print '
												<td align="center" valign="middle" class="subHeader" scope="col">';
                                            $TotAScoreLevel6 = $TotAScoreLevel6 + number_format($row2['AScoreLevel6Comm'], 1, '.', '');
                                            echo number_format($row2['AScoreLevel6Comm'], 1, '.', '');
                                            print '</td>';
                                        }
                                        ?>
                                        <!-- =================================================================== -->
                                        <td align="center" valign="middle" class="subHeader" scope="col">
                                            <?php
                                            $amends = $all_dev[trim($row2["KPI"])]["Score_Type"];

                                            if (!isset($amends)) {

                                                echo ("<script type='text/javascript'>{ parent.msgbox('Appraisal records has been modified. Kindly contact HR department to restart this appariasal record. ', 'red'); }</script>");
                                                exit;
                                            }
                                            if ($amends === "--") {
                                                $DrpCnt = $DrpCnt + 1;

                                                echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';

                                                $kk = "GetTotal('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . ")";
                                                echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                                                $SelID = $row2['AScoreMrg'];

                                                if ($GradeType == "Tags") {
                                                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                                                    include '../login/dbOpen3.php';
                                                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {

                                                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {
                                                            echo '<option selected value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';
                                                        } else {
                                                            echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';
                                                        }
                                                    }
                                                    include '../login/dbClose3.php';
                                                } else {
                                                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                                                    $incrementsBy = ($val) ? $val : 0.5;
                                                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                                                        if ($SelID == $php_i) {
                                                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                                                        } else {
                                                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                                                        }
                                                    }
                                                }
                                                echo '</select>';
                                            } else {

                                                // echo "here";
                                                $DrpCnt = $DrpCnt + 1;
                                                //echo $DrpCnt;

                                                if ($amends === "Tags") {

                                                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                                                    $kk = "GetTotal('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                                                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                                                    $SelID = $row2['AScoreEmp'];
                                                    $dbOpen3 = ("select Val1, ItemCode from Masters where ItemName='KPI Tags' and Status<>'D' order by Val1");
                                                    include '../login/dbOpen3.php';
                                                    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
                                                        if ((float) $SelID == (float) number_format(($row2['Weightage'] * $row3['ItemCode'] / 100), 1, '.', '')) {
                                                            echo '<option selected value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';
                                                        } else {
                                                            echo '<option value="' . ($row2['Weightage'] * $row3['ItemCode'] / 100) . '">' . $row3['Val1'] . '</option>';
                                                        }
                                                    }
                                                    include '../login/dbClose3.php';
                                                    echo '</select>';
                                                } else if ($amends === "Values") {
                                                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                                                    $kk = "GetTotal('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                                                    echo '<select name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" onChange="' . $kk . '" >';
                                                    $SelID = $row2['AScoreMrg'];
                                                    $val = ScriptRunner("SELECT appr_increments FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "appr_increments");
                                                    $incrementsBy = ($val) ? $val : 0.5;
                                                    //for ($php_i=0;$php_i<=100;$php_i++)
                                                    for ($php_i = 0; $php_i <= $row2['Weightage']; $php_i += $incrementsBy) {
                                                        if ($SelID == $php_i) {
                                                            echo '<option selected=selected value="' . $php_i . '">' . $php_i . '</option>';
                                                        } else {
                                                            echo '<option value="' . $php_i . '">' . $php_i . '</option>';
                                                        }
                                                    }
                                                    echo '</select>';
                                                } else if ($amends === "Quantity") {
                                                    $SelID = $row2['AScoreMrg'];
                                                    if ($SelID) {
                                                        $SelID = $SelID / $row2['Weightage'] * $all_dev[trim($row2["KPI"])]["Quantity_Value"];
                                                    }

                                                    echo '<div style="display:flex"> <span><i> Target:</i></span> <input  type="text" value="' . number_format($all_dev[trim($row2["KPI"])]["Quantity_Value"]) . '"  readonly style="height:70%;" /> </div>';
                                                    echo '<input name="Wth' . $DrpCnt . '" id="Wth' . $DrpCnt . '" type="hidden" value="0">';
                                                    $kk = "GetTotal('AScoreMrg_" . $row2['HashKey'] . "'," . $DrpCnt . "," . $row2['Weightage'] . "," . $all_dev[trim($row2["KPI"])]["Quantity_Value"] . ")";
                                                    // echo '<select name="AScoreMrg_'.$row2['HashKey'].'" class="" id="AScoreMrg_'.$row2['HashKey'].'" onChange="'.$kk.'" >';
                                                    // var_dump($SelID);
                                                    echo '<div style="display:flex" ><span><i>Actual:</i></span><input  name="AScoreMrg_' . $row2['HashKey'] . '_try" class="" id="AScoreMrg_' . $row2['HashKey'] . '_try" style="height:70%;"  value="' . $SelID . '"   />  </div>  <input  name="AScoreMrg_' . $row2['HashKey'] . '" class="" id="AScoreMrg_' . $row2['HashKey'] . '" style="height:70%;"  value="' . $SelID . '"  readonly type="hidden"  />   <div onclick="' . $kk . '" style="cursor:pointer; background-color: red; display:inline; padding:2px; color:white; text-align:center" >Enter</div> ';
                                                }
                                            }

                                            ?>

                                        </td>
                                        <td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "TDAScoreMrg_" . $row2['HashKey']; ?>">0.0</td>
                                        <td>
                                            <?php
                                            if (strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
                                                $bg = 'info';
                                            } elseif (strlen(trim($row2['MComm'])) > 0 && !strlen(trim($row2['EComm'])) > 0) {
                                                $bg = 'success';
                                            } elseif (!strlen(trim($row2['MComm'])) > 0 && strlen(trim($row2['EComm'])) > 0) {
                                                $bg = 'danger';
                                            } else {
                                                $bg = 'default';
                                            }
                                            ?>
                                            <button type="button" class="btn btn-<?php echo $bg ?> btn-sm" alt="Edit Record" name="EditEdu<?php echo $Del; ?>" id="EditEdu<?php echo $Del; ?>" onClick="parent.ShowDisp('KPI&nbsp;Comment','kpi/KPI_Comm.php?EmpTyp=Mrg&RecStatus=<?php echo $row2['Status']; ?>&EmpIDHash=<?php echo $EditID; ?>&HID=<?php echo $row2['HashKey']; ?>',320,600,'Yes')">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $kk_all = $kk_all . "<script type='text/javascript'>{" . $kk . ";}</script>"; ?>
                                <?php
                                }
                                include '../login/dbClose2.php';
                                ?>
                                <tr>
                                    <td colspan="<?php
                                                    //if ($GradeType!="Tags")
                                                    {
                                                        echo "3";
                                                    } //else{echo "1";}
                                                    ?>" align="right" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col">Score:</td>
                                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col"></td>
                                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">&nbsp;<?php echo $TWeightage; ?>%</td>
                                    <td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">
                                        <?php echo $TotMgr . "%";
                                        echo '<input name="MgrTScore" id="MgrTScore" type="hidden" value="0">
												'; ?></span>
                                    </td>
                                    <!-- ============================total extend here =========================== -->
                                    <?php
                                    include 'level_check_mgr.php';
                                    if ($appr3_emp == '1') {
                                        print '
											<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
                                        if (!$TotAScoreLevel3 == "") {
                                            echo number_format($TotAScoreLevel3, 1, '.', '') . "%";
                                        }
                                        print '</td>';
                                    }

                                    if ($appr4_emp == '1') {
                                        print '
											<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
                                        if (!$TotAScoreLevel4 == "") {
                                            echo number_format($TotAScoreLevel4, 1, '.', '') . "%";
                                        }
                                        print '</td>';
                                    }

                                    if ($appr5_emp == '1') {
                                        print '
											<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
                                        if (!$TotAScoreLevel5 == "") {
                                            echo number_format($TotAScoreLevel5, 1, '.', '') . "%";
                                        }
                                        print '</td>';
                                    }

                                    if ($appr6_emp == '1') {
                                        print '
											<td align="center" valign="middle" style="background: #AAAAAA; padding-top: 8px;padding-bottom:  8px;" scope="col">';
                                        if (!$TotAScoreLevel6 == "") {
                                            echo number_format($TotAScoreLevel6, 1, '.', '') . "%";
                                        }
                                        print '</td>';
                                    }
                                    ?>
                                    <!-- =================================================================== -->
                                    <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col" id="TOTALTDAScoreMrg" name="TOTALTDAScoreMrg">0%</td>
                                    <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col">&nbsp;</td>
                                    <td align="center" valign="middle" style="background: #EAEAEA; padding-top: 8px;padding-bottom:  8px;" scope="col">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- </div> -->
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <?php
                                include "final_score_total_2nd.php"
                                /*
$dbOpen2 = ("Select * from KPIIndvScore where (Status <>'D' AND Status <>'S')
and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59')
and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."') ORDER BY KRA_Group");
include '../login/dbOpen2.php';
$prevValue = "";
$main_total=0;
$w_main_total=0;
if($final_score_setup=="1"){
$final_score_type="Last Appraiser";
}elseif($final_score_setup=="2"){
$final_score_type="Supervisor/Direct Line Manager";
}elseif($final_score_setup=="3"){
$final_score_type="Average Score of all Appraiser";
}
print "<table width=\"100%\" border=\"1\" cellpadding=\"4\" cellspacing=\"0\">
<tr>
<td>OVERALL RATING</td>
<td>WEIGHTAGE</td>
<td>FINAL SCORE($final_score_type)</td>
</tr>";
while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
$Del = $Del + 1;
//$UpdUser = $row2['UpdatedBy'];
$AuthRec = $row2['Status'];
$AQuartID_Val = $row2['AQuartID'];

$KRA_Group_value = $row2['KRA_Group'];
//records=$row2['KRA_Group'];

if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)&&($KRA_Group_value != "--")){

$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".$KRA_Group_value."'
AND (Status <>'D' AND Status <>'S') and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59') and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");

$total_sum = ScriptRunner($Script,"TotMgrScore");
$total_weightage = ScriptRunner($Script,"TotWeightage");
//----------------------------

print "
<tr>
<td><b>$KRA_Group_value</b></td>
<td>$total_weightage</td>
<td></td>
</tr>
";

$prevValue = $KRA_Group_value;
$main_total=$main_total+$total_sum;
$w_main_total=$w_main_total+$total_weightage;
}

}
print "
<tr>
<td><b>TOTAL</b></td>
<td><b>$w_main_total</b></td>
<td><b>$final_score_value</b></td>
</tr>
";
include '../login/dbClose2.php';
print "</table>";
 */
                                ?>
                                <?php
                                //Show goal form....
                                if ($goal_period_from_month != "") {
                                    print '<br/><br/>';
                                    include 'goal_obj_form.php';
                                    print '<br/><br/>';
                                }
                                ?>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group text-center" style="font-weight: 550; font-size: 13px;">
                                        <div class="box bg-pale-secondary">
                                            <?php
                                            $Script = "Select UpdatedBy, EmpUpdCnt, MgrUpdCnt from KPIFinalScore where (Status <>'D' AND Status <>'S') and EID='" . $EmpID . "' and [AQuartID]='" . $AQuartID_Val . "'";

                                            $MaxIter = ScriptRunner($Script, "MgrUpdCnt");
                                            //$empudcnt = ScriptRunner($Script,"EmpUpdCnt");

                                            $MaxIter_count = $MaxIter;

                                            if ($MaxIter == "") {
                                                $MaxIter = 0;
                                            }
                                            $Script = "Select ACount from [KPIStart] where [HashKey]='" . $AQuartID_Val . "'";
                                            $MaxIterMax = ScriptRunner($Script, "ACount");

                                            if ($MaxIterMax > 0) {
                                                $MaxIter = "Update Count: [" . $MaxIter . " of " . $MaxIterMax . "]";
                                            } else {
                                                $MaxIter = "Update Count: Unlimited";
                                            }

                                            echo $MaxIter;
                                            if ($MaxIter_count == "") {
                                                $MaxIter_count = 0;
                                            }
                                            $alert = "<font size=\"3\"<b>You can update your record a maximum of " . $MaxIterMax . " unique times. You have updated (";
                                            $alert .= $MaxIter_count . " out of " . $MaxIterMax . ")</b></font>";
                                            error_reporting(E_ERROR | E_PARSE);
                                            if ((!$_REQUEST['button']) && (!$_REQUEST["FAction"])) {
                                                echo ("<script type='text/javascript'>{parent.msgbox('$alert','red');}</script>");
                                            }
                                            if (!isset($EmpID) || !isset($AQuartID_Val)) {
                                                exit;
                                            }

                                            /*
$EmpTyp = "Mrg";
if ($EmpTyp == "Mrg"){
$ReOly = ' readonly="readonly" ';
}elseif ($EmpTyp == "Emp"){
$ReOly = '';
}*/

                                            //The READ-ONLY option load
                                            $Script2 = "Select * from KPISettingNew where Status = 'A'";

                                            $selMID = ScriptRunner($Script2, "MID");
                                            $enforce_final = ScriptRunner($Script2, "enforce_final");
                                            $AprsID3 = ScriptRunner($Script2, "AprsID3");
                                            $AprsID4 = ScriptRunner($Script2, "AprsID4");
                                            $AprsID5 = ScriptRunner($Script2, "AprsID5");
                                            $AprsID6 = ScriptRunner($Script2, "AprsID6");
                                            $AprsID7 = ScriptRunner($Script2, "AprsID7");
                                            $AprsID8 = ScriptRunner($Script2, "AprsID8");

                                            if (isset($selMID)) { //2
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = '';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID3) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = '';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID4) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = '';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID5) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = '';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID6) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = '';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID7) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = '';
                                                $ReOly8 = ' readonly="readonly" ';
                                            } elseif ($AprsID8) {
                                                $ReOly1 = ' readonly="readonly" ';
                                                $ReOly2 = ' readonly="readonly" ';
                                                $ReOly3 = ' readonly="readonly" ';
                                                $ReOly4 = ' readonly="readonly" ';
                                                $ReOly5 = ' readonly="readonly" ';
                                                $ReOly6 = ' readonly="readonly" ';
                                                $ReOly7 = ' readonly="readonly" ';
                                                $ReOly8 = '';
                                            }
                                            $Script_Edit = "Select * from [KPIFinalScore] where [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $EmpID . "' and (Status <>'D' AND Status <>'S')";
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- ======================================== EMPLOYEE STRENGHT AND WEAKNESS ===================== -->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">
                                                    <?php
                                                    //Loop through the Header
                                                    $dbOpen2 = ("SELECT [Val1] FROM Masters where (ItemName='Appraisal_Header') AND Status<>'D' ORDER BY Val1");
                                                    include '../login/dbOpen2.php';

                                                    $coun = 0;
                                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                        $header[$coun] = $row2['Val1'];
                                                        $coun++;
                                                    }

                                                    if (isset($header[0])) {
                                                        print $header[0];
                                                    } else {
                                                        print " State Employee's Strength(s)";
                                                    }
                                                    ?>
                                                </p>
                                                <p>
                                                    <?php
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpStrenght") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MStrn"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" ' . $ReOly2 . '>' . $_REQUEST["MStrn"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="box bg-pale-secondary ">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">
                                                    <?php
                                                    if (isset($header[1])) {
                                                        print $header[1];
                                                    } else {
                                                        print "State Employee's perceived weakness(es)";
                                                    }
                                                    ?>
                                                </p>
                                                <p>
                                                    <?php
                                                    //$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpWeakness") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MWeak"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '>' . $_REQUEST["MWeak"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- =======================================================================================--------->
                        <!-- ======================================== LEARNING & DEVELOPMENT ACTION ===================== -->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">
                                                    <?php
                                                    if (isset($header[2])) {
                                                        print $header[2];
                                                    } else {
                                                        print "Identify Employee's Performance Gaps";
                                                    }
                                                    ?>
                                                </p>
                                                <p>
                                                    <?php
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpPerfGap") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MGap"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '>' . $_REQUEST["MGap"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="box bg-pale-secondary ">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">
                                                    <?php
                                                    if (isset($header[3])) {
                                                        print $header[3];
                                                    } else {
                                                        print "State Remedial Actions that will be relevant over the next FY ";
                                                    }
                                                    ?>
                                                </p>
                                                <p>
                                                    <?php
                                                    //$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "EmpRemAct") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MRem"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '>' . $_REQUEST["MRem"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- =======================================================================================--------->
                        <!-- ============================================MEDICAL===========================================--------->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary ">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
                                                            <?php
                                                            if (isset($header[4])) {
                                                                print $header[4];
                                                            } else {
                                                                print "Medical Fitness and Alertness";
                                                            }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-6"></div>
                                                </div>
                                                <small>
                                                    <?php
                                                    $Script5 = "SELECT [Desc] FROM Masters where (ItemName='Medical') AND Status<>'D'";
                                                    $medical_more_msg = ScriptRunner($Script5, "Desc");
                                                    if (isset($medical_more_msg)) {
                                                        print $medical_more_msg;
                                                    }
                                                    ?>
                                                </small>
                                                <p>
                                                    <?php
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "MRecommendation") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MRecom"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '>' . $_REQUEST["MRecom"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- =======================================================================================--------->
                        <!-- =============================================RECCOMENDATIONS==========================================--------->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary ">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
                                                            Reccomended For
                                                        </p>
                                                    </div>
                                                    <div class="col-6"></div>
                                                </div>
                                                <hr style="margin: 14px auto;">
                                                <p style="font-weight: bold">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MTraining") == 'on') {
                                                            echo '<input name="MTraining" id="MTraining" type="checkbox" checked="checked" /><label for="MTraining"  style="padding-left:25px; line-height:2" >Training</label>';
                                                        } else {
                                                            echo '<input name="MTraining" id="MTraining" type="checkbox" /><label for="MTraining"  style="padding-left:25px; line-height:2" >Training</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MPromote") == 'on') {
                                                            echo '<input name="MPromote" id="MPromote" type="checkbox" checked="checked" /><label for="MPromote" style="padding-left:25px; line-height:2" >Promotion</label>';
                                                        } else {
                                                            echo '<input name="MPromote" id="MPromote" type="checkbox" /><label for="MPromote" style="padding-left:25px; line-height:2" >Promotion</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MTransfer") == 'on') {
                                                            echo '<input name="MTransfer" id="MTransfer" type="checkbox" checked="checked" /><label for="MTransfer" style="padding-left:25px; line-height:2" >Transfer</label>';
                                                        } else {
                                                            echo '<input name="MTransfer" id="MTransfer" type="checkbox" /><label for="MTransfer" style="padding-left:25px; line-height:2" >Transfer</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MSalary") == 'on') {
                                                            echo '<input name="MSalary" id="MSalary" type="checkbox" checked="checked" /><label for="MSalary" style="padding-left:25px; line-height:2" >Salary Adjustment</label>';
                                                        } else {
                                                            echo '<input name="MSalary" id="MSalary" type="checkbox" /><label for="MSalary" style="padding-left:25px; line-height:2" >Salary Adjustment</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MExtension") == 'on') {
                                                            echo '<input name="MExtension" id="MExtension" type="checkbox" checked="checked" /><label for="MExtension" style="padding-left:25px; line-height:2" >Extension of Probationary Period</label>';
                                                        } else {
                                                            echo '<input name="MExtension" id="MExtension" type="checkbox" /><label for="MExtension" style="padding-left:25px; line-height:2" >Extension of Probationary Period</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MReview") == 'on') {
                                                            echo '<input name="MReview" id="MReview" type="checkbox" checked="checked" /><label for="MReview" style="padding-left:25px; line-height:2" >Review of Continued Employment</label>';
                                                        } else {
                                                            echo '<input name="MReview" id="MReview" type="checkbox" /><label for="MReview" style="padding-left:25px; line-height:2" >Review of Continued Employment</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-4 px-0">
                                                        <?php
                                                        if (ScriptRunner($Script_Edit, "MConfirmation") == 'on') {
                                                            echo '<input name="MConfirmation" id="MConfirmation" type="checkbox" checked="checked" /><label for="MConfirmation" style="padding-left:25px; line-height:2" >Confirmation of Appointment/Acting Appointment</label>';
                                                        } else {
                                                            echo '<input name="MConfirmation" id="MConfirmation" type="checkbox" /><label for="MConfirmation" style="padding-left:25px; line-height:2" >Confirmation of Appointment/Acting Appointment</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- =======================================================================================--------->
                        <!-- =======================================================================================--------->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <div class="box bg-pale-secondary">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">Employee's Comment </p>
                                                <p>
                                                    <?php
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '>' . ScriptRunner($Script_Edit, "EComment") . '</textarea>';
                                                    } elseif (isset($_REQUEST["EComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '>' . $_REQUEST["EComm"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" ' . $ReOly1 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="box bg-pale-secondary ">
                                            <div class="box-body">
                                                <p class="box-title text-center  b-0 px-0" style="font-weight: bold">Supervisor's Comment</p>
                                                <p>
                                                    <?php
                                                    $Script = "Select EComm, MComm from [KPIIndvScore] where HashKey='" . $EditID . "'";
                                                    if ($EditID != "" || $EditID > 0) {
                                                        echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '>' . ScriptRunner($Script_Edit, "MComment") . '</textarea>';
                                                    } elseif (isset($_REQUEST["MComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
                                                        echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '>' . $_REQUEST["MComm"] . '</textarea>';
                                                    } else {
                                                        echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" ' . $ReOly2 . '></textarea>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <?php
                        $Script_set = "Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "'";
                        $HashKey_set = ScriptRunner($Script_set, "HashKey");
                        //$Script2="Select * from KPIFinalScore WHERE MID ='".$_SESSION["StkTck"."HKey"]."' AND KPISetting_hashkey='".$HashKey_set."'";
                        //$Script2="Select * from KPIFinalScore WHERE MID ='".$_SESSION["StkTck"."HKey"]." and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '".ECh($EditID)."')";
                        $Script2 = "Select * from KPIFinalScore WHERE EID='" . $EmpID . "' and MID ='" . $_SESSION["StkTck" . "HKey"] . "' and [AQuartID]='" . $AQuartID_Val . "' and Status <> 'D'";
                        //include '../login/dbOpen2.php';
                        $selMID = ScriptRunner($Script2, "MID");

                        $AprsID3 = ScriptRunner($Script2, "AprsID3");
                        $AprsName33 = ScriptRunner($Script2, "AprsName3");
                        $AprsComment3 = ScriptRunner($Script2, "AprsComment3");

                        $AprsID4 = ScriptRunner($Script2, "AprsID4");
                        $AprsName44 = ScriptRunner($Script2, "AprsName4");
                        $AprsComment4 = ScriptRunner($Script2, "AprsComment4");

                        $AprsID5 = ScriptRunner($Script2, "AprsID5");
                        $AprsName55 = ScriptRunner($Script2, "AprsName5");
                        $AprsComment5 = ScriptRunner($Script2, "AprsComment5");

                        $AprsID6 = ScriptRunner($Script2, "AprsID6");
                        $AprsName66 = ScriptRunner($Script2, "AprsName6");
                        $AprsComment6 = ScriptRunner($Script2, "AprsComment6");

                        $AprsID7 = ScriptRunner($Script2, "AprsID7");
                        $AprsName77 = ScriptRunner($Script2, "AprsName7");
                        $AprsComment7 = ScriptRunner($Script2, "AprsComment7");

                        $AprsID8 = ScriptRunner($Script2, "AprsID8");
                        $AprsName88 = ScriptRunner($Script2, "AprsName8");
                        $AprsComment8 = ScriptRunner($Script2, "AprsComment8");

                        $HashKey_sel = ScriptRunner($Script2, "HashKey");
                        $num_appraisers = ScriptRunner($Script2, "LUO_Num");

                        $ReOly1 = ' readonly="readonly" ';
                        $ReOly2 = ' readonly="readonly" ';
                        $ReOly3 = ' readonly="readonly" ';
                        $ReOly4 = ' readonly="readonly" ';
                        $ReOly5 = ' readonly="readonly" ';
                        $ReOly6 = ' readonly="readonly" ';
                        $ReOly7 = ' readonly="readonly" ';
                        $ReOly8 = ' readonly="readonly" ';
                        //print $num_appraisers;
                        //print $num_appraisers." $EmpTyp (". $who_is_appraising;

                        if (isset($AprsComment3)) {
                            $apcom3 = $AprsComment3;
                            $name3 = $AprsName33;
                        } else {
                            $apcom3 = "";
                            $name3 = "";
                        }

                        if (isset($AprsComment4)) {
                            $apcom4 = $AprsComment4;
                            $name4 = $AprsName44;
                        } else {
                            $apcom4 = "";
                            $name4 = "";
                        }

                        if (isset($AprsComment5)) {
                            $apcom5 = $AprsComment5;
                            $name5 = $AprsName55;
                        } else {
                            $apcom5 = "";
                            $name5 = "";
                        }

                        if (isset($AprsComment6)) {
                            $apcom6 = $AprsComment6;
                            $name6 = $AprsName66;
                        } else {
                            $apcom6 = "";
                            $name6 = "";
                        }

                        if (isset($AprsComment7)) {
                            $apcom7 = $AprsComment7;
                            $name7 = $AprsName77;
                        } else {
                            $apcom7 = "";
                            $name7 = "";
                        }

                        if (isset($AprsComment8)) {
                            $apcom8 = $AprsComment8;
                            $name8 = $AprsName88;
                        } else {
                            $apcom8 = "";
                            $name8 = "";
                        }
                        $Script2 = "Select * from KPISettingNew where Status = 'A'";
                        $num_appraisers = ScriptRunner($Script2, "num_appraisers");
                        //----------------------------------------------- Summary Comment Box
                        include 'level_check_mgr.php';
                        ?>
                        <!-- =======================================================================================--------->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <?php if ($appr3_comment == '1') : ?>
                                            <div class="box bg-pale-secondary">
                                                <div class="box-body">
                                                    <p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 3rd Appraiser's Summary Comment (<?php echo $name3 ?>)</p>
                                                    <p>
                                                        <textarea name="3rdAps" rows="3" class="form-control" <?= $ReOly3 ?>>
				            								<?= $apcom3 ?>
				            							</textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        <?php if ($appr4_comment == '1') : ?>
                                            <div class="box bg-pale-secondary">
                                                <div class="box-body">
                                                    <p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 4th Appraiser's Summary Comment (<?php echo $name4 ?>)</p>
                                                    <p>
                                                        <textarea name="4thAps" rows="3" class="form-control" <?= $ReOly4 ?>>
				            								<?= $apcom4 ?>
				            							</textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- =======================================================================================--------->
                        <!-- =======================================================================================--------->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="box-group">
                                        <?php if ($appr5_comment == '1') : ?>
                                            <div class="box bg-pale-secondary">
                                                <div class="box-body">
                                                    <p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 5th Appraiser's Summary Comment (<?php echo $name5 ?>)</p>
                                                    <p>
                                                        <textarea name="5thAps" rows="3" class="form-control" <?= $ReOly5 ?>>
				            								<?= $apcom5 ?>
				            							</textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        <?php if ($appr6_comment == '1') : ?>
                                            <div class="box bg-pale-secondary">
                                                <div class="box-body">
                                                    <p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 6th Appraiser's Summary Comment (<?php echo $name6 ?>)</p>
                                                    <p>
                                                        <textarea name="6thAps" rows="3" class="form-control" <?= $ReOly6 ?>>
				            								<?= $apcom6 ?>
				            							</textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <!-- ============================================== OUTGOING YEAR =========================================== -->
                        <?php
                        $data = ScriptRunner($Script_Edit, "Accomp");
                        $EmpAccomp = json_decode($data, true);
                        if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "Accomplishments") == '1' && isset($data) && $data !== '') : ?>
                            <div class="row">
                                <!-- <div class="col-md-1"></div> -->
                                <div class="col-md-12">
                                    <div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
                                        <div class="box-group">
                                            <div class="box">
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
                                                                OUTGOING YEAR
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div style="width: 150px">Accomplishments</div>
                                                                </th>
                                                                <th>
                                                                    <div style="width: 150px">Supervisor's Comment</div>
                                                                </th>
                                                                <?php if ((isset($appr3_comment)) && ($appr3_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser3 (<?php echo $name3 ?>)</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr4_comment)) && ($appr4_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser4 (<?php echo $name4 ?>)</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr5_comment)) && ($appr5_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser5 (<?php echo $name5 ?>)</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr6_comment)) && ($appr6_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser6 (<?php echo $name6 ?>)</div>
                                                                    </th>
                                                                <?php endif ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="input_fields_wrap fieldList">
                                                            <?php for ($i = 1; $i <= count($EmpAccomp); $i++) : ?>
                                                                <tr id="row<?php echo $i ?>">
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <textarea readonly name="accomplishments[]" rows='1' class="form-control"><?php echo $EmpAccomp[$i - 1]['employee_accomp']; ?></textarea>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if (!empty($EmpAccomp[$i - 1]['mgr_accomp_comment'])) {
                                                                                $mgr_accomp = trim($EmpAccomp[$i - 1]['mgr_accomp_comment']);
                                                                            } else {
                                                                                $mgr_accomp = '';
                                                                            }
                                                                            ?>
                                                                            <textarea name="mgr_accomp[]" rows='1' class="form-control"><?php echo $mgr_accomp; ?></textarea>
                                                                        </div>
                                                                    </td>
                                                                    <?php if ((isset($appr3_comment)) && ($appr3_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($EmpAccomp[$i - 1]['apr3_accomp_comment'])) {
                                                                                    $apr3_accomp = trim($EmpAccomp[$i - 1]['apr3_accomp_comment']);
                                                                                } else {
                                                                                    $apr3_accomp = '';
                                                                                }
                                                                                ?>
                                                                                <textarea name="apr3_accomp[]" readonly rows='1' class="form-control"><?php echo $apr3_accomp; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr4_comment)) && ($appr4_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($EmpAccomp[$i - 1]['apr4_accomp_comment'])) {
                                                                                    $apr4_accomp = trim($EmpAccomp[$i - 1]['apr4_accomp_comment']);
                                                                                } else {
                                                                                    $apr4_accomp = '';
                                                                                }
                                                                                ?>
                                                                                <textarea name="apr4_accomp[]" readonly rows='1' class="form-control"><?php echo $apr4_accomp; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr5_comment)) && ($appr5_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($EmpAccomp[$i - 1]['apr5_accomp_comment'])) {
                                                                                    $apr5_accomp = trim($EmpAccomp[$i - 1]['apr5_accomp_comment']);
                                                                                } else {
                                                                                    $apr5_accomp = '';
                                                                                }
                                                                                ?>
                                                                                <textarea name="apr5_accomp[]" readonly rows='1' class="form-control"><?php echo $apr5_accomp; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr6_comment)) && ($appr6_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                - <?php
                                                                                    if (!empty($EmpAccomp[$i - 1]['apr6_accomp_comment'])) {
                                                                                        $apr6_accomp = trim($EmpAccomp[$i - 1]['apr6_accomp_comment']);
                                                                                    } else {
                                                                                        $apr6_accomp = '';
                                                                                    }
                                                                                    ?>
                                                                                <textarea name="apr6_accomp[]" readonly rows='1' class="form-control"><?php echo $apr6_accomp; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                </tr>
                                                            <?php endfor; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-1"></div> -->
                            </div>
                        <?php endif ?>
                        <!-- ============================================== OBJECTIVES =========================================== -->
                        <?php if (ScriptRunner("SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'", "show_objectives") == '1') : ?>
                            <?php
                            $obj_column = json_decode(ScriptRunner($Script_Edit, "objectives_remark"), true);
                            ?>
                            <div class="row">
                                <!-- <div class="col-md-1"></div> -->
                                <div class="col-md-12">
                                    <div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
                                        <div class="box-group">
                                            <div class="box">
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
                                                                OBJECTIVES FOR THE COMING YEAR
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div style="width: 150px">Objectives</div>
                                                                </th>
                                                                <th>
                                                                    <div style="width: 150px">Remarks</div>
                                                                </th>
                                                                <th>
                                                                    <div style="width: 150px">Supervisor's Remark</div>
                                                                </th>
                                                                <?php if ((isset($appr3_comment)) && ($appr3_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser3 (<?php echo $name3 ?>) Remark</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr4_comment)) && ($appr4_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser4 (<?php echo $name4 ?>) Remark</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr5_comment)) && ($appr5_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser5 (<?php echo $name5 ?>) Remark</div>
                                                                    </th>
                                                                <?php endif ?>
                                                                <?php if ((isset($appr6_comment)) && ($appr6_comment == '1')) : ?>
                                                                    <th>
                                                                        <div style="width: 100px">Appraiser6 (<?php echo $name6 ?>) Remark</div>
                                                                    </th>
                                                                <?php endif ?>
                                                            </tr>
                                                        </thead>
                                                        <?php for ($i = 1; $i <= count($obj_column); $i++) : ?>
                                                            <tbody class="fieldList">
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <textarea name="objectives[]" readonly rows='1' class="form-control"><?php echo $obj_column[$i - 1]['objectives']; ?></textarea>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if (!empty($obj_column[$i - 1]['emp_objectives'])) {
                                                                                $emp_objectives = trim($obj_column[$i - 1]['emp_objectives']);
                                                                            } else {
                                                                                $emp_objectives = '';
                                                                            }
                                                                            ?>
                                                                            <textarea name="emp_objectives[]" readonly rows='1' class="form-control"><?php echo $emp_objectives; ?></textarea>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if (!empty($obj_column[$i - 1]['mgr_objectives'])) {
                                                                                $mgr_objectives = trim($obj_column[$i - 1]['mgr_objectives']);
                                                                            } else {
                                                                                $mgr_objectives = '';
                                                                            }
                                                                            ?>
                                                                            <textarea name="mgr_objectives[]" rows='1' class="form-control"><?php echo $mgr_objectives; ?></textarea>
                                                                        </div>
                                                                    </td>
                                                                    <?php if ((isset($appr3_comment)) && ($appr3_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($obj_column[$i - 1]['apr3_objectives'])) {
                                                                                    $apr3_objectives = trim($obj_column[$i - 1]['apr3_objectives']);
                                                                                } else {
                                                                                    $apr3_objectives = '';
                                                                                }
                                                                                ?>
                                                                                <textarea readonly name="apr3_objectives[]" rows='1' class="form-control"><?php echo $apr3_objectives; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr4_comment)) && ($appr4_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($obj_column[$i - 1]['apr4_objectives'])) {
                                                                                    $apr4_objectives = trim($obj_column[$i - 1]['apr4_objectives']);
                                                                                } else {
                                                                                    $apr4_objectives = '';
                                                                                }
                                                                                ?>
                                                                                <textarea readonly name="apr4_objectives[]" rows='1' class="form-control"><?php echo $apr4_objectives; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr5_comment)) && ($appr5_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($obj_column[$i - 1]['apr5_objectives'])) {
                                                                                    $apr5_objectives = trim($obj_column[$i - 1]['apr5_objectives']);
                                                                                } else {
                                                                                    $apr5_objectives = '';
                                                                                }
                                                                                ?>
                                                                                <textarea readonly name="apr5_objectives[]" rows='1' class="form-control"><?php echo $apr5_objectives; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                    <?php if ((isset($appr6_comment)) && ($appr6_comment == '1')) : ?>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <?php
                                                                                if (!empty($obj_column[$i - 1]['apr6_objectives'])) {
                                                                                    $apr6_objectives = trim($obj_column[$i - 1]['apr6_objectives']);
                                                                                } else {
                                                                                    $apr6_objectives = '';
                                                                                }
                                                                                ?>
                                                                                <textarea readonly name="apr6_objectives[]" rows='1' class="form-control"><?php echo $apr6_objectives; ?></textarea>
                                                                            </div>
                                                                        </td>
                                                                    <?php endif ?>
                                                                </tr>
                                                            </tbody>
                                                        <?php endfor ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-1"></div> -->
                            </div>
                        <?php endif ?>
                        <!-- =======================================================================================--------->
                        <?php //echo "FIDSON: ".md5("FIDSON")."<br> GEOSCAPE: ".md5("GEOSCAPE");
                        ?>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 text-center">
                                <br /><br />
                                <?php
                                echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
				            		      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
				            			  <input name="FAction" id="FAction" type="hidden">
				            		   	  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
				            			  <input name="SndBk_Acct" id="SndBk_Acct" type="hidden" value="" />';
                                if (ValidateURths("TEAM APPRAISAL MGR" . "A") == true) {
                                    if (isset($AuthRec) && ($AuthRec == 'N' || $AuthRec == 'U')) {
                                        // if(isset($MgrFinal))
                                        $Script_Edit = "Select [secondary_mgr], [LUO_HashKey] from [KPIFinalScore] where [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $EmpID . "' and Status <>'D'";
                                        $emp_script = "Select [EmpMgr_1] from [EmpTbl] where [HashKey] ='" . $EmpID . "'";

                                        $secondary_mgr_hash = ScriptRunner($emp_script, "EmpMgr_1");
                                        $secondary_mgr = ScriptRunner($Script_Edit, "secondary_mgr");
                                        $LUO_HashKey = ScriptRunner($Script_Edit, "LUO_HashKey");

                                        // if (isset($_SESSION['disable'])  && $_SESSION['disable']===true ) {
                                        if ($secondary_mgr === "1" && $_SESSION['StkTckHKey'] === $secondary_mgr_hash) {

                                            // if (isset($_SESSION['disable'])  && $_SESSION['disable']===true ) {
                                            echo '<input type="submit" name="button" id="button" value="Save and Continue Later" class="btn btn-danger btn-sm"  disabled  />&nbsp;&nbsp;';

                                            echo '<input name="SubmitTrans" id="SubmitTrans"  disabled  type="button" class="btn btn-danger btn-sm" value="Submit for Employee\'s Confirmation" onClick="YesNo(\'Update KPI\', \'Update\')" />&nbsp;&nbsp;';
                                        } else {
                                            if ($LUO_HashKey === "END") {
                                                echo '<input type="submit" name="button" id="button"  disabled   value="Save and Continue Later" class="btn btn-danger btn-sm" />&nbsp;&nbsp;';

                                                echo '<input name="SubmitTrans" id="SubmitTrans" type="button"  disabled  class="btn btn-danger btn-sm" value="Submit for Employee\'s Confirmation" onClick="YesNo(\'Update KPI\', \'Update\')" />&nbsp;&nbsp;';
                                            } else {

                                                echo '<input type="submit" name="button" id="button" value="Save and Continue Later" class="btn btn-danger btn-sm" />&nbsp;&nbsp;';

                                                echo '<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Submit for Employee\'s Confirmation" onClick="YesNo(\'Update KPI\', \'Update\')" />&nbsp;&nbsp;';
                                            }
                                        }

                                        // The below script checks if the enforce_final_sel is checked at the appraisal setup
                                        $Script_kpi_view = "SELECT  Convert(Varchar(11),AddedDate,106) as Dt, * FROM KPISettingNew WHERE HashKey=KPISettingNew.HashKey";
                                        $enforce_final_sel = ScriptRunner($Script_kpi_view, "enforce_final");

                                        if ($MgrFinal == "1" && $MaxIter_count == '0' && $enforce_final_sel = '1') {
                                            //if(!$MID){
                                            echo '&nbsp;&nbsp;';

                                            //echo '<input type="submit" name="button" id="button" value="FINAL SUBMISSION (Agree to '.$mgr_name.'\'s score") class="btn btn-danger btn-sm" />&nbsp;&nbsp;';
                                        } else {
                                            $Script_Edit = "Select [secondary_mgr], [LUO_HashKey]  from [KPIFinalScore] where [AQuartID]='" . $AQuartID_Val . "' and [EID]='" . $EmpID . "' and Status <>'D'";
                                            $emp_script = "Select [EmpMgr_1] from [EmpTbl] where [HashKey] ='" . $EmpID . "'";

                                            $secondary_mgr_hash = ScriptRunner($emp_script, "EmpMgr_1");
                                            $secondary_mgr = ScriptRunner($Script_Edit, "secondary_mgr");
                                            $LUO_HashKey = ScriptRunner($Script_Edit, "LUO_HashKey");

                                            // if (isset($_SESSION['disable'])  && $_SESSION['disable']===true ) {
                                            if ($secondary_mgr === "1" && $_SESSION['StkTckHKey'] === $secondary_mgr_hash) {
                                                echo '<input name="SubmitTrans" disabled id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="FINAL SUBMISSION A(Agree to ' . $mgr_name . '\'s score") onClick="YesNo(\'Agreed KPI\', \'Agreed\')" />&nbsp;&nbsp;';
                                            } else {
                                                if ($LUO_HashKey === "END") {
                                                    echo '<input name="SubmitTrans" id="SubmitTrans" type="button"   disabled class="btn btn-danger btn-sm" value="FINAL SUBMISSION A(Agree to ' . $mgr_name . '\'s score") onClick="YesNo(\'Agreed KPI\', \'Agreed\')" />&nbsp;&nbsp;';
                                                } else {
                                                    echo '<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="FINAL SUBMISSION A(Agree to ' . $mgr_name . '\'s score") onClick="YesNo(\'Agreed KPI\', \'Agreed\')" />&nbsp;&nbsp;';
                                                }
                                            }
                                        }
                                    } elseif (isset($AuthRec) && ($AuthRec == 'A' || $AuthRec == 'PA') || $MID == "") {
                                        echo '<input type="submit" name="button" id="button" value="Save and Continue Later" class="btn btn-sm btn-danger" disabled />&nbsp;&nbsp;';

                                        echo '<input type="submit" name="button" id="button" value="Submit for Employee\'s Confirmationr" class="btn btn-sm btn-danger" disabled />&nbsp;&nbsp;';

                                        echo '<input type="submit" name="button" id="button" value="FINAL SUBMISSION (Agree to ' . $mgr_name . '\'s score") class="btn btn-sm btn-danger" disabled />&nbsp;&nbsp;';
                                    } elseif (isset($AuthRec) && ($AuthRec == 'E')) {
                                        echo '<input type="submit" name="button" id="button" value="Save and Continue Later" class="btn btn-sm btn-danger" disabled />&nbsp;&nbsp;';

                                        echo '<input type="submit" name="button" id="button" value="Submit for Employee\'s Confirmationr" class="btn btn-sm btn-danger" disabled />&nbsp;&nbsp;';

                                        ///echo '<input type="submit" name="button" id="button" value="Submit for Employee\'s Confirmationr" class="smallButton" />'; //delete later

                                        echo '<input type="submit" name="button" id="button" value="FINAL SUBMISSION (Agree to ' . $mgr_name . '\'s score") class="btn btn-sm btn-danger" disabled/>&nbsp;&nbsp;';
                                    }
                                ?>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 text-center">
                            <?php
                                    $Script_kpi = "SELECT  * FROM KPISettingNew WHERE HashKey='" . $_SESSION['KPISettingNewHashKey'] . "'";
                                    $appr_allow_supervisor = ScriptRunner($Script_kpi, "Appr_allow_supervisor");
                                    if ($appr_allow_supervisor == '1') {
                                        print "<br/>
				            			<input type=\"checkbox\" value=\"1\" name=\"end_aprs\" id=\"end_aprss\">
				            			<label for=\"end_aprss\">End this Apraisal when I click the FINAL SUBMISSION (Agree to '.$mgr_name.'s score) Button</label>
				            			";
                                    }
                                }
                            ?>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php echo $kk_all; ?>
</body>
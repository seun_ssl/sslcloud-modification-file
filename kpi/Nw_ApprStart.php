<?php session_start();
include '../login/scriptrunner.php';

//ALL THE SCRIPTS HERE FOR JQUERY AND ALL ARE INCLUDED BELOW TO AVOID THE FAILING OF THE HEADER REDIRECT TO THE APPRAISAL LOAD PAGE.

//Validate user viewing rights

if (ValidateURths("EMP APPRAISAL" . "B") != true) {include '../main/NoAccess.php';exit;}

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($AQuartID)) {$AQuartID = 0;}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    //This echo has been moved below to allow an automatic redirection from this point
    //echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }<//script>");
    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Begin Selected") {
        //if (ValidateURths("APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}

        $Script = "Select distinct(AID) AID, ([HashKey]) AiDD from [KPIStart] where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59'";
        $KPIID = ScriptRunner($Script, "AiDD");
        $KPINm = ScriptRunner($Script, "AID");
        $dbOpen2 = ("Select * from KPICore where Status ='A' and AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )))");
        include '../login/dbOpen2.php';
        $FinalHashKey = "";
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $Del = $Del + 1;
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "KPI" . $Del . $_SESSION["StkTck" . "UName"];
            $HashKey = md5($UniqueKey);
            if ($Del == 1) {
                $Script_set = "Select HashKey from KPISetting where Status='A'";
                $HashKey_set = ScriptRunner($Script_set, "HashKey");

                $ScriptID = "Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
                $Script = "INSERT INTO [KPIFinalScore]([EID],[MID],[secondary_mgr],[AQuartNm],[AQuartID],[FScore],[Status],[AddedBy],[AddedDate],[KPISetting_hashkey],[HashKey])
				values ('" . ScriptRunner($ScriptID, "HashKey") . "','',0,'" . $KPINm . "','" . $KPIID . "',0,'N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey_set . "','" . $HashKey . "')";
                ScriptRunnerUD($Script, "Insert");
                $FinalHashKey = $HashKey;
            }
            $Script = "insert into KPIIndvScore ([AID]
    ,[KPI],[Weightage],[ARemark],[Opt0],[Opt1],[Opt2],[Opt3],[Opt4],[Opt5],[Opt6],[Opt7],[KRA],[KRA_Sub_Area],[KRA_Group],[AQuartID],[Status],[AddedBy],[AddedDate],[FinalHashKey],[HashKey]) values ('" . ECh(trim($row2['AID'])) . "','" . ECh(trim($row2['KPI'])) . "'," . ECh(trim($row2['Weightage'])) . ",'" . ECh(trim($row2['ARemark'])) . "','" . ECh(trim($row2['Opt0'])) . "','" . ECh(trim($row2['Opt1'])) . "','" . ECh(trim($row2['Opt2'])) . "','" . ECh(trim($row2['Opt3'])) . "','" . ECh(trim($row2['Opt4'])) . "','" . ECh(trim($row2['Opt5'])) . "','" . ECh(trim($row2['Opt6'])) . "','" . ECh(trim($row2['Opt7'])) 
	. "','" . ECh(trim($row2['KRA']))
	. "','" . ECh(trim($row2['KRA_Sub_Area']))
	 . "','" . ECh(trim($row2['KRA_Group'])) 
	 . "','" . $KPIID . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $FinalHashKey . "','" . $HashKey . "')";
            ScriptRunnerUD($Script, "Insert");
            $Script_Mail = "Select * from MailTemp where HashKey='6c802f68b6f54c41burg7e5bd83a4a13'"; //EmployeeAppraisal Started
            $LoginDet = ScriptRunner($Script_Mail, "MMsg");
            $Subj = ScriptRunner($Script_Mail, "MSub");
            $LoginDet = str_replace('#EmpName#', ECh($_SESSION["StkTck" . "FName"]), $LoginDet);

            //Send Mail to HR Team members if set
            $Script = "Select Email from EmpTbl Et where HashKey in ((Select Convert(Varchar(32),SetValue3) from Settings where Setting='ApprNotify'),(Select Convert(Varchar(32),SetValue4) from Settings where Setting='ApprNotify'))";
            $To = ScriptRunnerJoin($Script, "Email");
            if (strlen($To) > 5) {
                QueueMail($To, $Subj, $LoginDet, '', '');
            }

            header("Location:ApprStartEmp.php?AQuartID=" . $KPIID . "&pid=rffbf6545bghjj89jyl8r&amp;pg=KPI");
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Continue Selected") {
        $Script = "Select distinct(HashKey) Hk from [KPIStart] where GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59' and Status='A' Group By HashKey";
        header("Location:ApprStartEmp.php?AQuartID=" . ScriptRunner($Script, "Hk") . "&pid=rffbf6545bghjj89jyl8r&amp;pg=KPI");
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected") {
        /*$OpenID = $_REQUEST["GrpName"];
        $Script="Select * from KPISetting where HashKey='".$_REQUEST["GrpName"]."'";
        //echo ScriptRunner($Script,"AName");
        $KPICount = ScriptRunner($Script,"AFieldCnt"); */
        //header("Location:Nw_KPIItem?PgTy=Personal&pid=rffbf6545bghjj89jyl8r&amp;pg=KPI");

        $Script = "Select distinct(HashKey) Hk from [KPIStart] where GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59' and Status='A' Group By HashKey";
        $AQuartID = ScriptRunner($Script, "Hk");

        $dbOpen2 = ("Select * from KPIIndvScore where AQuartID='" . ECh($AQuartID) . "' and Status <>'D' and AID=(Select HashKey from KPISetting where Status='A' and AName='" . ECh($_SESSION["StkTck" . "HKey"]) . "') ORDER BY KRA_Group");
//echo $dbOpen2;
        include '../login/dbOpen2.php';

        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $apr_ind_hashkey = $row2['HashKey'];

            $Script = "UPDATE KPIIndvScore SET [Status] = 'D' WHERE [HashKey] = '" . $apr_ind_hashkey . "'";
            ScriptRunnerUD($Script, "Authorize");
        }
        //print $AQuartID;
        //exit(' delete now');
    }
}

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = true;include '../css/myscripts.php';
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<script>
	$(function() {$( "#tabs" ).tabs();});
	</script>
	<script>
	  $(function() {
	    $( document ).tooltip();
	  });
	</script>
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		strong {
		    font-weight: 550;
		}
	</style>
</head>

<body oncontextmenu="return false;">
	<div class="box">
		<div class="box-body">
			<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="box bg-pale-secondary">
							<div class="box-body text-center" style="padding: 0.9rem" >
								<?php
//Check if there is a Valid Appriasal Open
$Script = "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status<>'D' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
if (ScriptRunner($Script, "Ct") == 0) {echo "<h5>You do not have any appraisal records. Contact your HR team</h5>";exit;} else {
    //Check if employee has begun apprisal already
    $Script = "select count(*) Ct from KPIIndvScore where AID=(Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
    $SelID = ScriptRunner($Script, "Ct");
    if ($SelID == 0) {echo "Click on Begin to Start your Appraisal";} elseif ($SelID > 0) {echo "<h5>Click on Continue Appraisal to resume or review your appraisal scores</h5>";}
}
?>
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
					</div>
					<div class="row" style="margin-top: 10px; margin-bottom: 20px" >
						<div class="col-md-12 text-center">
							<?php
$Script = "Select top 1 count(*) Ct, HashKey from [KPIStart] where GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59' and Status='A' Group By HashKey";
$SelID = ScriptRunner($Script, "Ct");
if ($SelID > 0) {
    $AQuartID = ScriptRunner($Script, "HashKey");
    $Script = "Select Count(*) Ct from KPIIndvScore where Status<>'D' and AQuartID='" . ScriptRunner($Script, "HashKey") . "' and AID=(Select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
    if (ScriptRunner($Script, "Ct") > 0) {?>
							      		<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Continue Appraisal" onClick="YesNo('Continue Appraisal', 'Continue')" />&nbsp;
							      	<?php
} else {
        //$Script="SELECT Sum(Weightage) Sm from KPICore where Status = 'A' and AID='".$_SESSION["StkTck"."HKey"]."'";
        $Script = "SELECT Sum(Weightage) Sm from KPICore where Status = 'A' and AID=(select HashKey from KPISetting where Status='A' and AName='" . $_SESSION["StkTck" . "HKey"] . "')";
        if (ScriptRunner($Script, "Sm") != 100) {
            //Display a message that AUTHORIZED KPI'S DO not sum up to 100%
            $imgID = "NoAppr";
            $imgPath = "../images/bi_butn/infoBW.jpg";
            $Titl = "You current KPI/KRA do not sum up to 100%. Kindly inform your manager of this as it prevents you from beginning your appraisal.";
            $MOvr = "MM_swapImage('" . $imgID . "','','../images/bi_butn/info.jpg',1)";
            $OnClk = "";
            echo '<a href="javascript: void(0)" onMouseOut="MM_swapImgRestore()" onMouseOver="' . $MOvr . '"><img id="' . $imgID . '" src="' . $imgPath . '" width="15" title="' . $Titl . '" onClick="' . $OnClk . '" border="0"/></a>';
            echo '&nbsp;<input name="SubmitTrans" id="SubmitTrans" type="button" disabled class="btn btn-danger btn-sm" value="Begin Appraisal"  />&nbsp;';
        } else {
            ?>
							    	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Begin Appraisal" onClick="YesNo('Begin Appraisal', 'Begin')" />&nbsp;
								  	<?php
}
    }
} else {?>
							      	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" disabled value="Begin Appraisal" />&nbsp;
							      	<?php }?>
							    	<?php
$Script = "Select count(*) from [KPIStart] where GetDate() between [SDate] and [EDate]";
if ($SelID > 0) {?>
								 		<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" disabled value="Modify My Deliverables" /> &nbsp;
								    <?php
} else {?>
							  			<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Modify My Deliverables"  onClick="YesNo('Modify KPI', 'Modify')" />&nbsp;
							<?php }?>
							<!-- <input name="SubmitTrans" id="SubmitTrans3" type="button" class="btn btn-danger btn-sm" value="Not In Use" onClick="YesNo('Cancel Leave', 'Cancel')" /> &nbsp; -->
							<!--
							<input name="SubmitTrans" id="SubmitTrans" type="button" class="smallButton" value="Restart Appraisal"  onClick="YesNo('Delete KPI', 'Delete')" />
							-->
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li><a class="active" href="#note" data-toggle="tab">Appraisal Note</a></li>
									<li><a href="#myhistroy" data-toggle="tab">My Appraisal History</a></li>
								</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="note">
									<?php
if (strlen($AQuartID) == 32) {
    $Script = "select (DATENAME(WEEKDAY,SDate)+', '+CONVERT(char(11),SDate,106)) SDt, (DATENAME(WEEKDAY,EDate)+', '+CONVERT(char(11),EDate,106)) EDt, * from [KPIStart] where (GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59') and Status='A' and  HashKey='" . $AQuartID . "'";

    $start_date = ScriptRunner($Script, "SDt");
    ?>
									<div class="row">
										<div class="col-md-3">
											<div class="box bg-pale-secondary">
												<div class="box-body text-right" style="padding: 0.6rem" >
													<label style="font-weight: bolder;">Current Appraisal:</label>
												</div>
											</div>
										</div>
										<div class="col-md-9" >
											<p style="margin-top: 10px" >
												<p><?php echo ScriptRunner($Script, "AID"); ?></p>
												<p><?php echo ScriptRunner($Script, "ARemark"); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="box bg-pale-secondary">
												<div class="box-body text-right" style="padding: 0.6rem" >
													<label style="font-weight: bolder;">Start Date:</label>
												</div>
											</div>

										</div>
										<div class="col-md-9" >
											<p style="margin-top: 10px" > <?php echo ScriptRunner($Script, "SDt"); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="box bg-pale-secondary">
												<div class="box-body text-right" style="padding: 0.6rem" >
													<label style="font-weight: bolder;">End Date:</label>
												</div>
											</div>
										</div>
										<div class="col-md-9" >
											<p style="margin-top: 10px"><?php echo ScriptRunner($Script, "EDt"); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="box bg-pale-secondary">
												<div class="box-body text-right" style="padding: 0.6rem" >
													<label style="font-weight: bolder;">Progress:</label>
												</div>
											</div>
										</div>
										<div class="col-md-9" >
											<?php
$Script_hkey = "Select * from KPISettingNew where Status = 'A'";
    $hkey = ScriptRunner($Script_hkey, "HashKey");
    $Script_kpi = "SELECT  * FROM KPISettingNew WHERE HashKey='" . $hkey . "'";

    $appr3_emp = ScriptRunner($Script_kpi, "Appr3_emp");
    $appr3_deliv = ScriptRunner($Script_kpi, "Appr3_deliv");
    $appr3_comment = ScriptRunner($Script_kpi, "Appr3_comment");
    $appr3_section = ScriptRunner($Script_kpi, "Appr3_section_comment");

    $appr4_emp = ScriptRunner($Script_kpi, "Appr4_emp");
    $appr4_deliv = ScriptRunner($Script_kpi, "Appr4_deliv");
    $appr4_comment = ScriptRunner($Script_kpi, "Appr4_comment");
    $appr4_section = ScriptRunner($Script_kpi, "Appr4_section_comment");

    $appr5_emp = ScriptRunner($Script_kpi, "Appr5_emp");
    $appr5_deliv = ScriptRunner($Script_kpi, "Appr5_deliv");
    $appr5_comment = ScriptRunner($Script_kpi, "Appr5_comment");
    $appr5_section = ScriptRunner($Script_kpi, "Appr5_section_comment");

    $appr6_emp = ScriptRunner($Script_kpi, "Appr6_emp");
    $appr6_deliv = ScriptRunner($Script_kpi, "Appr6_deliv");
    $appr6_comment = ScriptRunner($Script_kpi, "Appr6_comment");
    $appr6_section = ScriptRunner($Script_kpi, "Appr6_section_comment");
    $Script_Edit = "Select * from KPIFinalScore where EID='" . $_SESSION["StkTck" . "HKey"] . "' and [AQuartID]='" . $AQuartID . "' and Status<>'D'";
    $nextLvLName = ScriptRunner("Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . ScriptRunner($Script_Edit, "LUO_HashKey") . "'", "Nm"); // Gets staff full details from the Emp Tab
    $rStatus = ScriptRunner($Script_Edit, "Status");
    $LUO_HashKey = ScriptRunner($Script_Edit, "LUO_HashKey");
    $rAprsID3 = ScriptRunner($Script_Edit, "AprsID3");
    $rAprsID4 = ScriptRunner($Script_Edit, "AprsID4");
    $rAprsID5 = ScriptRunner($Script_Edit, "AprsID5");
    $rAprsID6 = ScriptRunner($Script_Edit, "AprsID6");

    if (!$rStatus) {
        $progress = 'Not Started';
    } elseif ($rStatus == 'N' || $rStatus == 'S' || $rStatus == 'E') {
        $progress = 'Employee';
    } elseif ($rStatus == 'U') {
        $progress = 'Supervisor';
    } elseif ($rStatus == 'A' && $LUO_HashKey !== 'END' && $rAprsID3 == null && $rAprsID4 == null && $rAprsID5 == null && $rAprsID6 == null && ($appr3_emp == '1' || $appr3_deliv == '1' || $appr3_comment == '1' || $appr3_section == '1')) {
        $progress = '3rd Appraiser';
    } elseif ($rStatus == 'A' && $LUO_HashKey !== 'END' && $rAprsID3 != null && $rAprsID4 == null && $rAprsID5 == null && $rAprsID6 == null && ($appr4_emp == '1' || $appr4_deliv == '1' || $appr4_comment == '1' || $appr4_section == '1')) {
        $progress = '4th Appraiser';
    } elseif ($rStatus == 'A' && $LUO_HashKey !== 'END' && $rAprsID4 != null && $rAprsID5 == null && $rAprsID6 == null && ($appr5_emp == '1' || $appr5_deliv == '1' || $appr5_comment == '1' || $appr5_section == '1')) {
        $progress = '5th Appraiser';
    } elseif ($rStatus == 'A' && $LUO_HashKey !== 'END' && $rAprsID5 != null && $rAprsID6 == null && ($appr6_emp == '1' || $appr6_deliv == '1' || $appr6_comment == '1' || $appr6_section == '1')) {
        $progress = '6th Appraiser';
    } else {
        $progress = 'Completed';
    }
    ?>
											<p style="margin-top: 10px">
												<?php echo $progress ?>
												<?php if ($nextLvLName): ?>
													<strong>(<?=trim($nextLvLName)?>)</strong>
												<?php endif?>
											</p>
										</div>
									</div>
									<div class="box bg-lightest">
										<div class="box-body  text-center" style="padding: 0.6rem" >
											<?php
$Script_Edit = "Select EmpUpdCnt from KPIFinalScore where EID='" . $_SESSION["StkTck" . "HKey"] . "' and [AQuartID]='" . $AQuartID . "' and Status<>'D'";
    ?>
											<p>You can update your record a maximum of <?php echo ScriptRunner($Script, "ACount"); ?> unique times. You have updated ( <?php	echo ScriptRunner($Script_Edit, "EmpUpdCnt"); ?> out of <?php echo ScriptRunner($Script, "ACount"); ?>) </p>
										</div>
									</div>
									<?php
//$alert = "<font size=\"3\"<b>You can update your record a maximum of ".ScriptRunner ($Script,"ACount")." unique times. You have updated (";
    //$alert .= ScriptRunner($Script_Edit,"EmpUpdCnt")." out of ".ScriptRunner ($Script,"ACount").")</b></font>";

    //$_SESSION['count_attempt'] = $alert;

    //echo ("<script type='text/javascript'>{parent.msgbox('$alert','red');}</script>");
    //print "<br/>";
    ?>
									<?php
} else {
    echo ' <div class="box bg-lightest">
										<div class="box-body  text-center" style="padding: 0.6rem" >
											<p>There is currently no on-going appraisal<p>
											<p>You can check and modify your deliverables to ensure they match your job description.</p>
										</div>
									</div> ';
}
?>
								</div>
								<div class="tab-pane" id="myhistroy">
									<div class="row">
										<div class="col-12">
											<div class="box">
									            <div class="box-body">
													<table class="table table-bordered table-responsive" id="table">
														<thead>
															<tr>
																<th data-sorter="false">
													                <!-- <div class="checkbox"> -->
																		<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
																		<label for="DelChk_All"></label>
													      			<!-- </div> -->
																</th>
																<th>Period Name</th>
																<th>Started</th>
																<th>Ended</th>
																<th>Supervisor</th>
																<th>Score</th>
																<th>Promote</th>
																<th>Training</th>
																<th>Details</th>
															</tr>
														</thead>
														<tbody>
															<?php
$Del = 0;
$IDCnt = "";
$IDCnt_a = '<option value="0" selected="selected">--</option>';
//Populate SORT ID's 1 - 20
for ($x = 1; $x <= 20; $x++) {$IDCnt .= '<option value="' . $x . '">' . $x . '</option>';}
//Populate menu list
$MenuList = '<option value="--" selected="selected">--</option>';
$dbOpen2 = ("Select Convert(Varchar(11),AddedDate,106) as SDt, Convert(Varchar(11),SubmittedDate,106) as EDt, * from KPIFinalScore where EID='" . ECh($_SESSION["StkTck" . "HKey"]) . "' and Status<>'D' Order by AddedDate Desc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //Disable the menu if the link is authorized
    if ($row2['Status'] == "A") {$MenuStatus = "disabled";} else { $MenuStatus = "";}?>
																        <tr>
																          <td>
																          	<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
																          	<label for="<?php echo ("DelBox" . $Del); ?>"></label>
																          </td>
																          <td><?php
$Script = "Select * from [KPIStart] where HashKey='" . $row2['AQuartID'] . "'";
    echo ScriptRunner($Script, "AID");?></td>
																          <td><?php echo ($row2['SDt']); ?></td>
																          <td><?php echo ($row2['EDt']); ?></td>
																          <td><?php
$Script = "Select Count(*) Ct from [EmpTbl] where HashKey='" . $row2['MID'] . "'";
    if (ScriptRunner($Script, "Ct") == 1) {
        $Script = "Select ('['+ EmpID + '] ' + SName + ' ' + FName)as Nm from [EmpTbl] where HashKey='" . $row2['MID'] . "'";
        echo ScriptRunner($Script, "Nm");
    }?></td>
																          <td align="center" valign="middle"><?php echo ($row2['FScore']); ?></td>
																          <td align="center" valign="middle"><?php if ($row2['MPromote'] == "on") {echo '<img src="../images/green.png">';} else {echo '<img src="../images/amber.png">';}?></td>
																          <td align="center" valign="middle"><?php if ($row2['MTraining'] == "on") {echo '<img src="../images/green.png">';} else {echo '<img src="../images/amber.png">';}?></td>
																          <td align="center" valign="middle"><span class="TinyText"><img src="../images/bi_butn/EditDataBW.jpg" alt="Appraisal Details" name="EmpComm<?php echo $Del; ?>" width="15" height="15" border="0" id="EmpComm<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('Appraisal&nbsp;Details','kpi/KPI_AllComm.php?EmpTyp=Mrg&RecStatus=<?php echo $row2['Status']; ?>&EmpIDHash=<?php echo $EditID; ?>&HID=<?php echo $row2['HashKey']; ?>',320,600,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/bi_butn/EditData.jpg',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" /></span></td>
																        </tr>
																        <?php }
include '../login/dbClose2.php';?>
														</tbody>
													</table>
												</div>
											</div>
											<!-- /.col -->
											<!-- /.col -->
										</div>
									</div>
								</div>
								<!-- /.tab-content -->
							</div>
							<!-- /.nav-tabs-custom -->
						</div>
						<!-- /.col -->
					</div>
					<?php echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
					    <input name="PgTy" id="PgTy" type="hidden" value="';if (isset($_REQUEST["PgTy"])) {echo $_REQUEST["PgTy"];}
echo '" />
					  	<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
						<input name="FAction" id="FAction" type="hidden">';
?>
				</div>
			</form>
			<!-- /.row -->
			<div class="row">
				<?php
$dbOpen2 = ("SELECT [Val1],[Desc] FROM Masters where (ItemName='Instruction') AND Status<>'D' ORDER BY Val1");
include '../login/dbOpen2.php';
echo '
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="box bg-lightest">
						<div class="box-header with-border text-center ">
							<h5 class="box-title "><strong>INSTRUCTIONS</strong> <br>';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    echo '
							<small>' . $row2["Val1"] . '</small></h5>
						</div>
						<div class="box-body">' . $row2["Desc"] . '</div>';
}
echo '
					</div>
				</div>
				<div class="col-md-2"></div>';
?>
			</div>
	    </div>
		  <!-- /.box-body -->
	</div>
	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script> -->


	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
</body>

<?php 
	session_start();
	include '../login/scriptrunner.php';

	//if (ValidateURths("TEAM APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}

	$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=false; include '../css/myscripts.php';

	$GoValidate = true;

	echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

	if (!isset($EditID)){$EditID="";}
	if (!isset($Script_Edit)){$Script_Edit="";}
	if (!isset($HashKey)){$HashKey="";}
	if (!isset($SelID)){$SelID="";}
	if (!isset($Del)){$Del=0;}
	if (!isset($StatusSet)){$StatusSet=0;}
	if (!isset($KPICount)){$KPICount=0;}
	if (!isset($TWeightage)){$TWeightage=0;}
	if (!isset($TotMgr)){$TotMgr=0;}

	if (!isset($Total_Mgr_score)){$Total_Mgr_score=0;}

	if (!isset($TotEmp)){$TotEmp=0;}
	if (!isset($o)){$o=0;}
	if (!isset($AllEmployee)){$AllEmployee="";}
	if (!isset($kk_all)){$kk_all="";}
	if (!isset($AQuartID_Val)){$AQuartID_Val="";}
	if (!isset($EScore)){$EScore="";}
	if (!isset($DrpCnt)){$DrpCnt=0;}
	if (!isset($UpdUser)){$UpdUser="";}
	if (!isset($AQuartID_Val)){$AQuartID_Val="";}
	if (!isset($AuthRec)){$AuthRec="";}
	if (!isset($TotMed)){$TotMed=0;}
	if (!isset($AQuartID_sel)){$AQuartID_sel="";}
	if (!isset($KPISetting_hashkey)){$KPISetting_hashkey="";}

	if (!isset($total_weightage)){$total_weightage="";}
	if (!isset($total_sum)){$total_sum="";}

	if (!isset($FinalHashKey)){$FinalHashKey="";}
	if (!isset($TotAScoreLevel3)){$TotAScoreLevel3=0;}
	if (!isset($TotAScoreLevel4)){$TotAScoreLevel4=0;}
	if (!isset($TotAScoreLevel5)){$TotAScoreLevel5=0;}
	if (!isset($TotAScoreLevel6)){$TotAScoreLevel6=0;}

	if (!isset($final_score_value)){$final_score_value=0;}
	if (!isset($final_score_setup)){$final_score_setup="";}
	if (!isset($final_score_type)){$final_score_type="";}



	if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32)
	{	unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);

		if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Agreed Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0)
		{
			$EditID="";
			for ($php_i=0; $php_i <= $_REQUEST["DelMax"]; $php_i++)
			{
				if (isset($_REQUEST["DelBox".$php_i]) && strlen($_REQUEST["DelBox".$php_i]) == 32)
				{
					$EditID=ECh($_REQUEST["DelBox".$php_i]);
					$StatusSet='P';
					$StatusSet='A';

					$Script = "Update [KPIIndvScore] set
					[AScoreFinal]=[AScoreMed],
					[WScore]=((Weightage * AScoreMed)/100),
					[Status]='".$StatusSet."',[SubmittedBy]='".ECh($_SESSION["StkTck"."UName"])."',[SubmittedDate]=GetDate() where [HashKey]= '".$EditID."'";
					ScriptRunnerUD($Script,"Inst");

					//Update Final Employee Score			
					if ($php_i == 1)
					{
						if (isset($_REQUEST["MPromote"]))
						{$MPromote_ = $_REQUEST["MPromote"];} else {$MPromote_ = '';}
						if (isset($_REQUEST["MTraining"]))
						{$MTraining_ = $_REQUEST["MTraining"];} else {$MTraining_ = '';}
						
						$Script="Update [KPIFinalScore] set
						[FScore]=".ECh($_REQUEST["FScore"]).",
						[MedComment]='".ECh($_REQUEST["MedComment"])."',
						[MPromote]='".$MPromote_."',
						[MTraining]='".$MTraining_."',
						[Status]='U',[UpdatedBy]='".ECh($_SESSION["StkTck"."HKey"])."',
						[UpdatedDate]=GetDate()
						where
						EID='".ECh($_REQUEST["ApprEmpID"])."'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '".$EditID."')
						and Status <> 'D'
						";
						ScriptRunnerUD($Script,"CM");

						AuditLog("UPDATE","Employee appraisal submitted successfully by mediator");
						echo ("<script type='text/javascript'>{ parent.msgbox('Employee appraisal score has been submitted successfully.', 'green'); }</script>");
					}
				}
			}
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		}

		if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0)
		{
			$EditID="";
			for ($php_i=0; $php_i <= $_REQUEST["DelMax"]; $php_i++)
			{
				if (isset($_REQUEST["DelBox".$php_i]) && strlen($_REQUEST["DelBox".$php_i]) == 32)
				{
					$EditID = ECh($_REQUEST["DelBox".$php_i]);
					$Script = "Update [KPIIndvScore] set [AScoreGrpMrg]='".ECh($_REQUEST["AScoreGrpMrg_".$EditID])."',[Status]='U',[UpdatedBy]='".ECh($_SESSION["StkTck"."HKey"])."',[UpdatedDate]=GetDate() where [HashKey]= '".$EditID."'";
				ScriptRunnerUD($Script,"Inst");

					if ($php_i == 1)
					{
						if (isset($_REQUEST["MPromote"]))
						{$MPromote_ = $_REQUEST["MPromote"];} else {$MPromote_ = '';}
						if (isset($_REQUEST["MTraining"]))
						{$MTraining_ = $_REQUEST["MTraining"];} else {$MTraining_ = '';}
						
						$Script="Update [KPIFinalScore] set
						[FScore]=".ECh($_REQUEST["FScore"]).",
						[MedComment]='".ECh($_REQUEST["MedComment"])."',
						[MPromote]='".ECh($MPromote_)."',
						[MTraining]='".ECh($MTraining_)."',
						[Status]='U',[UpdatedBy]='".ECh($_SESSION["StkTck"."HKey"])."',
						[UpdatedDate]=GetDate()
						where
						EID='".ECh($_REQUEST["ApprEmpID"])."'
						and [AQuartID]=(Select AQuartID from KPIIndvScore where [HashKey]= '".$EditID."')
						and Status <> 'D'
						";
						ScriptRunnerUD($Script,"CM");
					}
		
				AuditLog("UPDATE","Employee appriasal successfully mediated on");
		
				echo ("<script type='text/javascript'>{ parent.msgbox('Employee appraisal score was successfully updated.', 'green'); }</script>");
				//Clear Selection//
				}
			}
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
		}
	}//DoS if 
?>
<?php if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script type="text/javascript">
function GetTotal($ObjName,$ObjWhtID)
{
	$kk = document.getElementById("Wght"+$ObjName).innerHTML;
	document.getElementById("TD"+$ObjName).innerHTML = ((parseFloat($kk) * document.getElementById($ObjName).value)/100).toFixed(2);
	document.getElementById("Wth"+$ObjWhtID).value = document.getElementById("TD"+$ObjName).innerHTML;

	//alert (document.getElementById("DelMax").value);
	document.getElementById("TOTALTDAScoreGrpMrg").innerHTML=0;
	for ($i=1; $i<=document.getElementById("DelMax").value; $i++)
	{
		document.getElementById("TOTALTDAScoreGrpMrg").innerHTML =
		parseFloat(document.getElementById("TOTALTDAScoreGrpMrg").innerHTML) + parseFloat(document.getElementById("Wth"+$i).value);
	}
	document.getElementById("TOTALTDAScoreGrpMrg").innerHTML = parseFloat(document.getElementById("TOTALTDAScoreGrpMrg").innerHTML).toFixed(1) + '%';
	document.getElementById("FScore").value = parseFloat(document.getElementById("TOTALTDAScoreGrpMrg").innerHTML).toFixed(1);
}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong{
		font-weight: 550;
	}
</style>
<body oncontextmenu="return false;">
	<?php
		$hash=$_REQUEST['hash'];
		$Script_hkey="Select * from KPISettingNew where Status = 'A'";
		$final_score_setup=ScriptRunner($Script_hkey,"final_score_setup");
		$_SESSION['KPISettingNewHashKey']=ScriptRunner($Script_hkey,"HashKey");
		$goal_desc = ScriptRunner($Script_hkey,"goal_desc");	
		$goal_period_from_month = ScriptRunner($Script_hkey,"goal_period_from_month");
		$goal_period_from_year = ScriptRunner($Script_hkey,"goal_period_from_year");
		$goal_period_to_month = ScriptRunner($Script_hkey,"goal_period_to_month");
		$goal_period_to_year = ScriptRunner($Script_hkey,"goal_period_to_year");
		$goal_header1 = ScriptRunner($Script_hkey,"goal_header1");
		$goal_header2 = ScriptRunner($Script_hkey,"goal_header2");
		$goal_header3 = ScriptRunner($Script_hkey,"goal_header3");
		$goal_header4 = ScriptRunner($Script_hkey,"goal_header4");
		$goal_num_row_allow = ScriptRunner($Script_hkey,"goal_num_row_allow");
		$dbOpen2 = ("SELECT HashKey, AID from [KPIStart] where HashKey = '$hash'");
		include '../login/dbOpen2.php';
		$row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH);
	?>
	<form action="#" method="post" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-12 text-center tdMenu_HeadBlock"><strong>[<a href="Nw_KPIIndvList.php">Go Back</a>] APPRAISAL NAME: <?php print $row2['AID']; ?></strong></div>
					<div class="col-md-7">
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12" style="padding-bottom: 10px;"><strong>Employee: 
								<span>
									<?php
										if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open")
										{
											echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="'.$_REQUEST["AcctNo"].'" />';
											$EmpID = ECh($_REQUEST["AcctNo"]);
											$EditID = ECh($_REQUEST["AcctNo"]);
										}
										else
										{
											if (isset($_REQUEST["ApprEmpID"]))
											{
												echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="'.$_REQUEST["ApprEmpID"].'" />';
												$EmpID=$_REQUEST["ApprEmpID"];
												$EditID = $_REQUEST["ApprEmpID"];
											}
										}
											
										//------- note 1 =======	 
										$Script="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.HashKey='".$EditID."'";
										echo ScriptRunner($Script,"Nm");
									?>
								</span></strong>
							</div>
							<div class="col-md-12"><strong>Supervisor: 
								<span>
									<?php
										if(isset($_REQUEST["AcctNo"])){
											$dbOpen2 = ("Select * from [KPIFinalScore] where [EID]='".$EmpID."' and Status <> 'D'");
											include '../login/dbOpen2.php';
											$row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH);
											$manager_hash_id = $row2['MID'];
											//$Script2="Select * from [KPIFinalScore] where [EID]='".$EmpID."'";
											//$manager_hash_id = ScriptRunner($Script2,"MID");
											//-------	 
											$Script_my = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr 
											from EmpTbl Et where Et.HashKey='".$EditID."'";
											$myManager = ScriptRunner($Script_my,"EmpMgr");
												
											$Script2="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where Et.HashKey='".$myManager."'";
											echo "&nbsp;&nbsp;".ScriptRunner($Script2,"Nm");
											//echo $EmpID;
											//echo "Mgr Id ".$manager_hash_id;
											//include '../login/dbClose2.php';
										}
									?>
								</span></strong>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group row">
						    <label  class="col-sm-4 col-form-label"><strong>Select an Employee</strong></label>
						   	<div class="col-sm-8 input-group input-group-sm">
								<select name="AcctNo" class="form-control" id="AcctNo">
									<?php
										echo '<option value="--" selected="selected">--</option>';
										//$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' and EmpMgr='".$_SESSION["StkTck"."HKey"]."' order by SName Asc");
										//$LoginUserID=$_SESSION["StkTck"."HKey"];
										//$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' AND HashKey='$LoginUserID' order by SName Asc");
										$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' order by SName Asc");
										//$dbOpen2 = ("SELECT * from KPIFinalScore where EID=(Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' and EmpMgr='".$_SESSION["StkTck"."HKey"]."' order by SName Asc");
										//$dbOpen2 = ("Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] WHERE [EmpTbl.HashKey]=(SELECT * from KPIFinalScore where EID)");
										include '../login/dbOpen2.php';
										while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
											{
												if ($EditID==$row2['HashKey'])
												{
												$AllEmployee = $AllEmployee.'<option selected value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
												}
												else
												{
												$AllEmployee = $AllEmployee.'<option value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
												}
											}
											echo $AllEmployee;
										include '../login/dbClose2.php'; 
									?>
								</select>
								<span class="input-group-btn ">
									<input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php 
				if (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Open"))
				{exit;}

				$Script = ("Select AQuartID from KPIIndvScore where Status <>'D'
				and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),MDate,106)+' 23:59:59')
				 and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");
			?>
			<?php
				/*
				if(isset($_POST['AcctNo'])){
					$sel_id=$_POST['AcctNo'];
					//print "<b>app if:</b> ".$hash." <b>emp id</b> ".ECh($_REQUEST["AcctNo"]);
					print "
					HERE: 
					<b>app if:</b> ".$hash." <b>emp id</b> ".$sel_id;
					
				}
				*/
			?>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">     
					    <div class="ViewPatch">
					    	<table class="table-responsive table table-bordered" id="">
							    <thead>
									<tr class="">
										<!-- tdMenu_HeadBlock_Light -->
										<th>Area of Focus</th>
										<th>Sub Area of Focus</th>
										<th> Departmental Objectives</th>
										<th>Individual Performance Objectives</th>
										<th>Weightage</th>
										<th>Employee</th>
										<th>Supervisor</th>
										<!-- ================================== extend here ========================== -->            
										<?php
											include 'level_check_mgr.php';
											if($appr3_emp=='1'){
												print '<th>Level 3 Appraiser<br>
											            Score
											          </th>';
											}
											if($appr4_emp=='1'){
												print '<th>Level 4 Appraiser<br>
											            Score
											          </th>';
											}
											if($appr5_emp=='1'){
												print '<th>Level 5 Appraiser<br>
											            Score
											          </th>';
											}
											if($appr6_emp=='1'){
												print '<th>Level 6 Appraiser<br>
											            Score
											          </th>';
											}
										?>           
										<!-- ========================================================================= -->    
		      						    <th align="center" valign="middle" scope="col">
			      						    <button type="button" title="Print blank paper form" name="EditEdu<?php echo $Del;?>" id="EditEdu<?php echo $Del;?>"  onClick="parent.ShowDisp('View&nbsp;Employee&nbsp;KPI/KRA','kpi/Rpt_KPIDtl_Blk.php?APPID=<?php echo ScriptRunner($Script,"AQuartID");?>&HID=<?php echo $EmpID;?>',600,900,'Yes')" class="btn btn-default btn-sm" style="line-height: 17px"><i class="fa fa-print"></i></button>
		      						    </th>
		         					</tr>
		        				</thead>
		      					<tbody>
									<?php
										$Del = 0;
										$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE
										AQuartID = '$hash'
										and
										AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."') and Status <> 'D' ORDER BY KRA_Group");
										//'41f84ae6fb7e8fa6c599ac1f859069e5'"); ///++++++++++++++++++++++++++++++ WORK STOP HERE (8/21/2015)
										include '../login/dbOpen2.php';
										$prevValue = "";
										while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
											{
											$Del = $Del + 1;
											$UpdUser = $row2['UpdatedBy'];
											$AuthRec = $row2['Status'];
											$AQuartID_Val=$row2['AQuartID'];
											$FHashKey = $row2['FinalHashKey'];
											$KRA_Group_value = $row2['KRA_Group'];
											if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)){
												//------ Option to display Section Comment
												if($appr2_section=='1'){
													$icon = "<button type=\"button\"
														title=\"Edit Record\"
														name=\"EditEdu".$Del."\"
														id=\"EditEdu".$Del."\"
														onClick=\"parent.ShowDisp('KPI&nbsp;Section&nbsp;Comment','kpi/KPI_CommSection.php?kra_group=".$KRA_Group_value."&RecStatus=A&EmpTyp=Others&HID=".$FHashKey."',320,600,'Yes')\" 
														class=\"btn btn-default btn-sm\" style=\"line-height: 17px\">
														<i class=\"fa fa-edit\"></i></button>";
												}else{
													$icon = "";
												}
												//----------------------------------------
												print "
												<tr class=\"tdMenu_HeadBlock_Light\">
														<td align=\"center\" valign=\"middle\" colspan=\"9\" style=\"padding-bottom: 5px; padding-top:5px;\"><strong>".$icon." $KRA_Group_value</strong></td>
												</tr>
												";
												$prevValue = $KRA_Group_value;
										}
									?>
							        <tr>
								        <?php
										//This makes the check boxes into hidden fields
										 	echo '<input name="'.("DelBox".$Del).'" type="hidden" id="'.("DelBox".$Del).'" value="'.($row2['HashKey']).'" />
										 		<label for="'.("DelBox".$Del).'"></label>';
										?>
							          	<td>
										  	<?php 
										  		echo "<b>".(trim($row2['KRA']))."</b>"; 
										   
										  	?>
							          	</td> 
							          	<td>
										  	<?php 
										  		echo "<b>".(trim($row2['KRA_Sub_Area']))."</b>"; 
										   
										  	?>
							          	</td> 
			          					<td><?php echo (trim($row2['KPI'])); ?></td>
			          					<td><?php echo (trim($row2['ARemark'])); ?></td>


			           					<td align="center" valign="middle" class="subHeader" scope="col" id="<?php echo "WghtAScoreGrpMrg_".$row2['HashKey'];?>">
			           						<?php 
			           							echo (trim($row2['Weightage'])); 
					  							$TWeightage = $TWeightage + $row2['Weightage'];
					  						?>
				  						</td>
							          	<td align="center" valign="middle" class="subHeader" scope="col">
							            <?php  echo  number_format($row2['AScoreEmp'], 2, '.', '');
									  	$TotEmp = $TotEmp + number_format($row2['AScoreEmp'], 2, '.', ''); ?></td>
							          	<td align="center" valign="middle" class="subHeader" scope="col"><?php 
									 	$TotMgr = $TotMgr + number_format($row2['AScoreMrg'], 2, '.', '');
									  	$Total_Mgr_score = $Total_Mgr_score + $row2['AScoreMrg'];
										echo  number_format($row2['AScoreMrg'], 2, '.', ''); ?></td>
										<!-- ============================extend here =========================== -->  
										<?php
											$Script_apsid="Select * from KPIFinalScore WHERE [AQuartID]='".$AQuartID_Val."' and [EID]='".$_REQUEST["AcctNo"]."' and Status <> 'D'";
													$Aprs_ID3=ScriptRunner($Script_apsid,"AprsID3");
													$Aprs_ID4=ScriptRunner($Script_apsid,"AprsID4");
													$Aprs_ID5=ScriptRunner($Script_apsid,"AprsID5");
													$Aprs_ID6=ScriptRunner($Script_apsid,"AprsID6");
											include 'level_check_mgr.php';
											if($appr3_emp=='1'){
													print '
													<td align="center" valign="middle" class="subHeader" scope="col">';		   
																$TotAScoreLevel3 = $TotAScoreLevel3 + number_format($row2['AScoreLevel3Comm'], 1, '.', '');
																echo  number_format($row2['AScoreLevel3Comm'], 1, '.', '');
													print '</td>';	
											}
											if($appr4_emp=='1'){	
													print '
													<td align="center" valign="middle" class="subHeader" scope="col">';		   
																$TotAScoreLevel4 = $TotAScoreLevel4 + number_format($row2['AScoreLevel4Comm'], 1, '.', '');
																echo  number_format($row2['AScoreLevel4Comm'], 1, '.', '');
													print '</td>';	
											}
											if($appr5_emp=='1'){	
													print '
													<td align="center" valign="middle" class="subHeader" scope="col">';		   
																$TotAScoreLevel5 = $TotAScoreLevel5 + number_format($row2['AScoreLevel5Comm'], 1, '.', '');
																echo  number_format($row2['AScoreLevel5Comm'], 1, '.', '');
													print '</td>';	
											}
											if($appr6_emp=='1'){	
													print '
													<td align="center" valign="middle" class="subHeader" scope="col">';		   
																$TotAScoreLevel6 = $TotAScoreLevel6 + number_format($row2['AScoreLevel6Comm'], 1, '.', '');
																echo  number_format($row2['AScoreLevel6Comm'], 1, '.', '');
													print '</td>';	
											}
										?>
										<!-- =================================================================== -->             
							          	<td>
							          		 <img src="../images/bi_butn/EditDataBW.jpg" alt="Edit Record" name="EditEdu<?php echo $Del;?>" width="15" height="15" border="0" id="EditEdu<?php echo $Del;?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('KPI&nbsp;Comment','kpi/KPI_Comm.php?EmpTyp=Mrg&RecStatus=<?php echo $row2['Status'];?>&EmpIDHash=<?php echo $EditID;?>&HID=<?php echo $row2['HashKey'];?>',320,600,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del;?>','','../images/bi_butn/EditData.jpg',1); ImgBorderOn('EditEdu<?php echo $Del;?>', 0);" />
			         					</td>
			          				</tr>
									<?php 
										/*
											$kk_all=$kk_all."<script type='text/javascript'>{".$kk.";}</script>";
										*/
										$Script8="Select * from KPIFinalScore where [AQuartID]='".$AQuartID_Val."' and [EID]='".$_REQUEST["AcctNo"]."' and Status <> 'D'";
										$LUO_Num_selected=ScriptRunner($Script8,"LUO_Num");
										$HashKey_fkpi=ScriptRunner($Script8,"HashKey");

										$Script_name_main="Select * from KPIFinalScore WHERE HashKey ='".$HashKey_fkpi."' and Status <> 'D'";
										$AprsID1=ScriptRunner($Script_name_main,"EID");
										$AprsID2=ScriptRunner($Script_name_main,"MID");
										$final_score_value = ScriptRunner ($Script_name_main,"FScore");

										$row1_col1 = ScriptRunner ($Script_name_main,"row1_col1");
										$row1_col2 = ScriptRunner ($Script_name_main,"row1_col2");
										$row1_col3 = ScriptRunner ($Script_name_main,"row1_col3");
										$row1_col4 = ScriptRunner ($Script_name_main,"row1_col4");

										$row2_col1 = ScriptRunner ($Script_name_main,"row2_col1");
										$row2_col2 = ScriptRunner ($Script_name_main,"row2_col2");
										$row2_col3 = ScriptRunner ($Script_name_main,"row2_col3");
										$row2_col4 = ScriptRunner ($Script_name_main,"row2_col4");

										$row3_col1 = ScriptRunner ($Script_name_main,"row3_col1");
										$row3_col2 = ScriptRunner ($Script_name_main,"row3_col2");
										$row3_col3 = ScriptRunner ($Script_name_main,"row3_col3");
										$row3_col4 = ScriptRunner ($Script_name_main,"row3_col4");

										$row4_col1 = ScriptRunner ($Script_name_main,"row4_col1");
										$row4_col2 = ScriptRunner ($Script_name_main,"row4_col2");
										$row4_col3 = ScriptRunner ($Script_name_main,"row4_col3");
										$row4_col4 = ScriptRunner ($Script_name_main,"row4_col4");

										$row5_col1 = ScriptRunner ($Script_name_main,"row5_col1");
										$row5_col2 = ScriptRunner ($Script_name_main,"row5_col2");
										$row5_col3 = ScriptRunner ($Script_name_main,"row5_col3");
										$row5_col4 = ScriptRunner ($Script_name_main,"row5_col4");

										$row6_col1 = ScriptRunner ($Script_name_main,"row6_col1");
										$row6_col2 = ScriptRunner ($Script_name_main,"row6_col2");
										$row6_col3 = ScriptRunner ($Script_name_main,"row6_col3");
										$row6_col4 = ScriptRunner ($Script_name_main,"row6_col4");

										$row7_col1 = ScriptRunner ($Script_name_main,"row7_col1");
										$row7_col2 = ScriptRunner ($Script_name_main,"row7_col2");
										$row7_col3 = ScriptRunner ($Script_name_main,"row7_col3");
										$row7_col4 = ScriptRunner ($Script_name_main,"row7_col4");

										$row8_col1 = ScriptRunner ($Script_name_main,"row8_col1");
										$row8_col2 = ScriptRunner ($Script_name_main,"row8_col2");
										$row8_col3 = ScriptRunner ($Script_name_main,"row8_col3");
										$row8_col4 = ScriptRunner ($Script_name_main,"row8_col4");

										$row9_col1 = ScriptRunner ($Script_name_main,"row9_col1");
										$row9_col2 = ScriptRunner ($Script_name_main,"row9_col2");
										$row9_col3 = ScriptRunner ($Script_name_main,"row9_col3");
										$row9_col4 = ScriptRunner ($Script_name_main,"row9_col4");

										$row10_col1 = ScriptRunner ($Script_name_main,"row10_col1");
										$row10_col2 = ScriptRunner ($Script_name_main,"row10_col2");
										$row10_col3 = ScriptRunner ($Script_name_main,"row10_col3");
										$row10_col4 = ScriptRunner ($Script_name_main,"row10_col4");

										$AQuartID_sel=ScriptRunner($Script_name_main,"AQuartID"); // new1
										$KPISetting_hashkey=ScriptRunner($Script_name_main,"KPISetting_hashkey"); //new1
												
										$Script_name1="Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='".$AprsID1."'";
										$name1 = ScriptRunner($Script_name1,"Nm");	

										$Script_name2="Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='".$AprsID2."'";
										$name2 = ScriptRunner($Script_name2,"Nm");
									?>
							        <?php 
										$TotMed = $TotMed + number_format($row2['AScoreMed'], 2, '.', '');
										}
										include '../login/dbClose2.php';
									?>
							        <tr>
							         
							          	<td colspan="4" align="right" valign="middle" scope="col" style="background: #EAEAEA;" >Score:</td>
							          	<td align="center" valign="middle" scope="col" style="background: #AAAAAA;"><?php echo $TWeightage; ?>%</td>
							          	<td align="center" valign="middle" scope="col" style="background: #EAEAEA;">
									 	 <?php echo "<font color=\"#0066CC\">".$TotEmp."%</font>";
							        			//echo '<input name="FScore" id="FScore" type="hidden" value="'.$TotMgr.'">';?></td>
							          	<td align="center" valign="middle" scope="col" style="background: #EAEAEA;">
									  	<?php echo "<font color=\"#0066CC\">".$TotMgr."%</font>";
						        			echo '<input name="FScore" id="FScore" type="hidden" value="0">';?></td>
		          						<!-- ============================total extend here =========================== -->  
										<?php
							                include 'level_check_mgr.php';
							                if($appr3_emp=='1'){
							                    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA;" scope="col">';
												//if(!$TotAScoreLevel3==""){
							                                echo  number_format($TotAScoreLevel3, 1, '.', '');
												//}
							                    print '</td>';	
							                }
							                
							                if($appr4_emp=='1'){
							                    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA;" scope="col">';
							                                echo  number_format($TotAScoreLevel4, 1, '.', '');
							                    print '</td>';	
							                }
							                
							                if($appr5_emp=='1'){
							                    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA;" scope="col">';	
							                                echo  number_format($TotAScoreLevel5, 1, '.', '');
							                    print '</td>';	
							                }
							                
							                if($appr6_emp=='1'){
							                    print '
							                    <td align="center" valign="middle" style="background: #AAAAAA;" scope="col">';
							                                echo  number_format($TotAScoreLevel6, 1, '.', '');
							                    print '</td>';
							                }
							            ?>
								        <!-- =================================================================== -->                     
							          	<td align="center" valign="middle" style="background: #EAEAEA;" scope="col" id="<?php echo "TOTALTDAScoreGrpMrg";?>" >&nbsp;</td>
							        </tr>       
								</tbody>
		      				</table>
		   				</div>
		   			</div>
		   			<div class="col-md-12">
						<div class="row">
							<div class="col-md-1"></div>
							<?php
								

							?>
							<div class="col-md-10">
								<?php
								
									$FinalHashKey = ScriptRunner($dbOpen2, "FinalHashKey");
									$AQuartID = ScriptRunner($dbOpen2, "AQuartID");
									$query= "SELECT calibrated_score from KPIFinalScore where HashKey = '".$FinalHashKey."' AND AQuartID = '".$AQuartID."'";
									$total_calibrated_score = ScriptRunner($query, "calibrated_score");
									
									include("final_score_total_2nd.php");
									/*
									$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AQuartID='$AQuartID_sel'
									 and AID='$KPISetting_hashkey' ORDER BY KRA_Group");
									include '../login/dbOpen2.php';
									$prevValue = "";
									$main_total=0;
									$w_main_total=0;
									print "<table width=\"100%\" border=\"1\" cellpadding=\"4\" cellspacing=\"0\">
											<tr>
												<td>OVERALL RATING</td>
												<td>WEIGHTAGE</td>
												<td>SCORE(MARKS OBTAINED)</td>
											</tr>";
									while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
									{
										$Del = $Del + 1;
										//$UpdUser = $row2['UpdatedBy'];
										$AuthRec = $row2['Status'];
										$AQuartID_Val = $row2['AQuartID'];
										$KRA_Group_value = $row2['KRA_Group'];
										//records=$row2['KRA_Group'];
										if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)&&($KRA_Group_value != "--")){
											//-----------------------------		
													
											$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".$KRA_Group_value."'
											 AND Status <>'D' and AQuartID='$AQuartID_sel' and AID='$KPISetting_hashkey'");
											
											
											$total_sum = ScriptRunner($Script,"TotMgrScore");
											$total_weightage = ScriptRunner($Script,"TotWeightage");
											//----------------------------
											
											print "
											<tr>
												<td><b>$KRA_Group_value</b></td> 
												<td>$total_weightage</td> 
												<td>$total_sum</td>  
											</tr> 
											";
											
											$prevValue = $KRA_Group_value;
											$main_total=$main_total+$total_sum;
											$w_main_total=$w_main_total+$total_weightage;
										}
										
									}
										print "
										   <tr>
									            <td><b>TOTAL</b></td>
												<td><b>$w_main_total</b></td>
									            <td><b>$main_total</b></td>
									       </tr>
										";

									include '../login/dbClose2.php';
									print "</table>";

									*/
								?>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- ======================================== EMPLOYEE STRENGHT AND WEAKNESS ===================== -->  
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12"  style="padding-left: 0px; padding-right: 0px;" >
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
														//Loop through the Header
														$dbOpen2 = ("SELECT [Val1] FROM Masters where (ItemName='Appraisal_Header') AND Status<>'D' ORDER BY Val1");
														include '../login/dbOpen2.php';

														$coun=0;
														while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
														{
															$header[$coun]=$row2['Val1'];
															$coun++;	
														}

														if(isset($header[0])){
															print $header[0];
														}else{
															print " State Employee's Strength(s)";
														}
													?>
												</p>
												<p>
													<?php
														$Script_Edit="Select * from [KPIFinalScore] where [AQuartID]='".$hash."' and [EID]='".$_REQUEST["AcctNo"]."' and Status <> 'D'";

														if ($EditID != "" || $EditID > 0){
																echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" readonly="readonly">'.ScriptRunner($Script_Edit,"EmpStrenght").'</textarea>';
															}elseif (isset($_REQUEST["MStrn"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear"))){
																echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" readonly="readonly">'.$_REQUEST["MStrn"].'</textarea>';
															}else{
																	echo '<textarea name="MStrn" rows="3" class="form-control" id="MStrn" readonly="readonly"></textarea>';
															}
													?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
														if(isset($header[1])){
															print $header[1];
														}else{
															print "State Employee's perceived weakness(es)";
														}
													?>
												</p>
												<p>
													<?php
														//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
														if ($EditID != "" || $EditID > 0){
															echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly">'.ScriptRunner($Script_Edit,"EmpWeakness").'</textarea>';
														}elseif (isset($_REQUEST["MWeak"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear"))){
															echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly">'.$_REQUEST["MWeak"].'</textarea>';
														}else{
															echo '<textarea name="MWeak" rows="3" class="form-control" id="MWeak" readonly="readonly"></textarea>';
														}
													?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->    
						<!-- ======================================== LEARNING & DEVELOPMENT ACTION ===================== --> 
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
														if(isset($header[2])){
															print $header[2];
														}else{
															print "Identify Employee's Performance Gaps";
														}
													?>
												</p>
												<p>
													<?php
														if ($EditID != "" || $EditID > 0){
																echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly">'.ScriptRunner($Script_Edit,"EmpPerfGap").'</textarea>';
															}elseif (isset($_REQUEST["MGap"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear"))){
																echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly">'.$_REQUEST["MGap"].'</textarea>';
															}else{
																	echo '<textarea name="MGap" rows="3" class="form-control" id="MGap" readonly="readonly"></textarea>';
															}
													?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">
													<?php
														if(isset($header[3])){
															print $header[3];
														}else{
															print "State Remedial Actions that will be relevant over the next FY ";
														}
													?>
												</p>
												<p>
													<?php
														//$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
														if ($EditID != "" || $EditID > 0){
															echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly">'.ScriptRunner($Script_Edit,"EmpRemAct").'</textarea>';
														}elseif (isset($_REQUEST["MRem"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear"))){
															echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly">'.$_REQUEST["MRem"].'</textarea>';
														}else{
															echo '<textarea name="MRem" rows="3" class="form-control" id="MRem" readonly="readonly"></textarea>';
														}
													?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- =======================================================================================--------->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
										            <?php
														if(isset($header[4])){
															print $header[4];
														}else{
															print "Medical Fitness and Alertness";
														}
													?>
												</p>
												<small>
													<?php
														$Script5="SELECT [Desc] FROM Masters where (ItemName='Medical') AND Status<>'D'";
														$medical_more_msg=ScriptRunner($Script5,"Desc");
														if (isset($medical_more_msg)){
															print $medical_more_msg;
														}
													?>
												</small>
												<p>
													<?php
														if ($EditID != "" || $EditID > 0)
														{echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly">'.ScriptRunner($Script_Edit,"MRecommendation").'</textarea>';}
														elseif (isset($_REQUEST["MRecom"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear")))
														{echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly">'.$_REQUEST["MRecom"].'</textarea>';}
														else
														{echo '<textarea name="MRecom" rows="3" class="form-control" id="MRecom" readonly="readonly"></textarea>';}
													?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- ==========================================RECCOMENDED=============================================--------->
        				<div class="row">
        					<div class="col-md-1"></div>
        					<div class="col-md-10">
        						<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
        							<div class="box-group">
        								<div class="box bg-pale-secondary ">
        									<div class="box-body">
        										<div class="row">
        											<div class="col-6">
		        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
		        								            Reccomended For
		        										</p>
		        									</div>
		        									<div class="col-6"></div>
        										</div>
        										<hr style="margin: 14px auto;">
        										<p style="font-weight: bold">
        											<div class="row">
        												<div class="col-sm-2">
        													<?php
							            		  				if (ScriptRunner($Script_Edit,"MPromote") == 'on')
							            		  				{echo'<input disabled name="MPromote" id="MPromote" type="checkbox" checked="checked" />
							            		  					<label for="MPromote"  style="padding-left:17px;" >Training</label>';}
							            		  				else
							            		  				{echo'<input disabled name="MPromote" id="MPromote" type="checkbox" /><label for="MPromote"  style="padding-left:17px;" >Training</label>';}
					            		  					?>
        												</div>
        												<div class="col-sm-2">
		    									            <?php
											             		if (ScriptRunner($Script_Edit,"MPromote") == 'on')
											             		{echo'<input disabled name="MPromote" id="MPromote" type="checkbox" checked="checked" /><label for="MPromote" style="padding-left:17px;" >Promotion</label>';}
											             		else
											             		{echo'<input disabled name="MPromote" id="MPromote" type="checkbox" /><label for="MPromote" style="padding-left:17px;" >Promotion</label>';}
											             	?>
											            </div>
											            <div class="col-sm-2">
		    									            <?php
											             		if (ScriptRunner($Script_Edit,"MTransfer") == 'on')
											             		{echo'<input disabled name="MTransfer" id="MTransfer" type="checkbox" checked="checked" /><label for="MTransfer" style="padding-left:17px;" >Transfer</label>';}
											             		else
											             		{echo'<input disabled name="MTransfer" id="MTransfer" type="checkbox" /><label for="MTransfer" style="padding-left:17px;" >Transfer</label>';}
											             	?>
											            </div>
											            <div class="col-sm-3">
		    									            <?php
											             		if (ScriptRunner($Script_Edit,"MSalary") == 'on')
											             		{echo'<input disabled name="MSalary" id="MSalary" type="checkbox" checked="checked" /><label for="MSalary" style="padding-left:17px;" >Salary Adjustment</label>';}
											             		else
											             		{echo'<input disabled name="MSalary" id="MSalary" type="checkbox" /><label for="MSalary" style="padding-left:17px;" >Salary Adjustment</label>';}
											             	?>
											            </div>
						            		  			<div class="col-sm-3">
		    									            <?php
											             		if (ScriptRunner($Script_Edit,"MExtension") == 'on')
											             		{echo'<input disabled name="MExtension" id="MExtension" type="checkbox" checked="checked" /><label for="MExtension" style="padding-left:17px;" >Extension of Probationary Period</label>';}
											             		else
											             		{echo'<input disabled name="MExtension" id="MExtension" type="checkbox" /><label for="MExtension" style="padding-left:17px;" >Extension of Probationary Period</label>';}
											             	?>
											            </div>
											            <div class="col-sm-4">
		    									            <?php
											             		if (ScriptRunner($Script_Edit,"MReview") == 'on')
											             		{echo'<input disabled name="MReview" id="MReview" type="checkbox" checked="checked" /><label for="MReview" style="padding-left:17px;" >Review of Continued Employment</label>';}
											             		else
											             		{echo'<input disabled name="MReview" id="MReview" type="checkbox" /><label for="MReview" style="padding-left:17px;" >Review of Continued Employment</label>';}
											             	?>
											            </div>
											            <div class="col-sm-5">
											             	<?php
							            		  				if (ScriptRunner($Script_Edit,"MConfirmation") == 'on')
							            		  				{echo'<input disabled name="MConfirmation" id="MConfirmation" type="checkbox" checked="checked" />
							            		  					<label for="MConfirmation"  style="padding-left:17px;" >Confirmation of Appointment/Acting Appointment</label>';}
							            		  				else
							            		  				{echo'<input disabled name="MConfirmation" id="MConfirmation" type="checkbox" /><label for="MConfirmation"  style="padding-left:17px;" >Confirmation of Appointment/Acting Appointment</label>';}
							            		  			?>
							            		  		</div>
        											</div>
        										</p>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="col-md-1"></div>
        				</div>
						<!-- =======================================================================================--------->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
									<div class="box-group">
										<div class="box bg-pale-secondary">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Employee's Comment ( 
													<?php
														if(isset($name1)){
														 	print $name1; 
														}else{
															print "";
														}
													?> 
												)</p>
												<p>
													<?php
														if ($EditID != "" || $EditID > 0)
														{echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" readonly="readonly">'.ScriptRunner($Script_Edit,"EComment").'</textarea>';}
														elseif (isset($_REQUEST["EComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear")))
														{echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" readonly="readonly">'.$_REQUEST["EComm"].'</textarea>';}
														else
														{echo '<textarea name="EComm" rows="3" class="form-control" id="EComm" readonly="readonly"></textarea>';}
													?>
												</p>
											</div>
										</div>
										<div class="box bg-pale-secondary ">
											<div class="box-body">
												<p class="box-title text-center  b-0 px-0" style="font-weight: bold">Supervisor's Comment ( 
													<?php 
														if(isset($name2)){
														 	print $name2; 
														}else{
															print "";
														}
													?>
												)</p>
												<p>
													<?php		
														$Script="Select EComm, MComm from [KPIIndvScore] where HashKey='".$EditID."'";
														if ($EditID != "" || $EditID > 0)
														{echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" readonly="readonly">'.ScriptRunner($Script_Edit,"MComment").'</textarea>';}
														elseif (isset($_REQUEST["MComm"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear")))
														{echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" readonly="readonly">'.$_REQUEST["MComm"].'</textarea>';}
														else
														{echo '<textarea name="MComm" rows="3" class="form-control" id="MComm" readonly="readonly"></textarea>';}
													?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<?php
							$Script2="Select * from [KPIFinalScore] where [AQuartID]='".$hash."' and [EID]='".$_REQUEST["AcctNo"]."' and Status <> 'D'";		
							
							$selMID=ScriptRunner($Script2,"MID");
							
							$AprsID3=ScriptRunner($Script2,"AprsID3");
							$AprsName33=ScriptRunner($Script2,"AprsName3");		
							$AprsComment3=ScriptRunner($Script2,"AprsComment3");
							
							$AprsID4=ScriptRunner($Script2,"AprsID4");
							$AprsName44=ScriptRunner($Script2,"AprsName4");		
							$AprsComment4=ScriptRunner($Script2,"AprsComment4");
							
							$AprsID5=ScriptRunner($Script2,"AprsID5");
							$AprsName55=ScriptRunner($Script2,"AprsName5");		
							$AprsComment5=ScriptRunner($Script2,"AprsComment5");
							
							$AprsID6=ScriptRunner($Script2,"AprsID6");
							$AprsName66=ScriptRunner($Script2,"AprsName6");		
							$AprsComment6=ScriptRunner($Script2,"AprsComment6");
							
							$AprsID7=ScriptRunner($Script2,"AprsID7");
							$AprsName77=ScriptRunner($Script2,"AprsName7");	
							$AprsComment7=ScriptRunner($Script2,"AprsComment7");
							
							$AprsID8=ScriptRunner($Script2,"AprsID8");
							$AprsName88=ScriptRunner($Script2,"AprsName8");
							$AprsComment8=ScriptRunner($Script2,"AprsComment8");
							
							$HashKey_sel=ScriptRunner($Script2,"HashKey");
							$num_appraisers=ScriptRunner($Script2,"LUO_Num");

							$ReOly1 = ' readonly="readonly" ';
							$ReOly2 = ' readonly="readonly" ';
							$ReOly3 = ' readonly="readonly" ';
							$ReOly4 = ' readonly="readonly" ';
							$ReOly5 = ' readonly="readonly" ';
							$ReOly6 = ' readonly="readonly" ';
							$ReOly7 = ' readonly="readonly" ';
							$ReOly8 = ' readonly="readonly" ';

							//print $num_appraisers." $EmpTyp (". $who_is_appraising; 

							if(isset($AprsComment3)){
								$apcom3=$AprsComment3;
								$name3 = $AprsName33;
							}else{
								$apcom3="";	
								$name3="";
							}

							if(isset($AprsComment4)){
								$apcom4=$AprsComment4;
								$name4 = $AprsName44;
							}else{
								$apcom4="";
								$name4="";
							}

							if(isset($AprsComment5)){
								$apcom5=$AprsComment5;
								$name5 = $AprsName55;
							}else{
								$apcom5="";
								$name5 = "";
							}

							if(isset($AprsComment6)){
								$apcom6=$AprsComment6;
								$name6 = $AprsName66;
							}else{
								$apcom6="";
								$name6 = "";
							}

							if(isset($AprsComment7)){
								$apcom7=$AprsComment7;
								$name7 = $AprsName77;
							}else{
								$apcom7="";
								$name7 = "";
							}

							if(isset($AprsComment8)){
								$apcom8=$AprsComment8;
								$name8 = $AprsName88;	
							}else{
								$apcom8="";
								$name8 = "";
							}
							// Summary Comment Box
			                include 'level_check_mgr.php';
			            ?>
			            <!-- =======================================================================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ($appr3_comment=='1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 3rd Appraiser's Summary Comment (<?php echo $name3 ?>)</p>
				            						<p>
				            							<textarea name="3rdAps" rows="3" class="form-control" <?= $ReOly3 ?> >
				            								<?= $apcom3 ?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif ?>
			            				<?php if ($appr4_comment=='1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 4th Appraiser's Summary Comment (<?php echo $name4 ?>)</p>
				            						<p>
				            							<textarea name="4thAps" rows="3" class="form-control" <?= $ReOly4 ?> >
				            								<?= $apcom4 ?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif ?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- =======================================================================================--------->
			            <div class="row">
			            	<div class="col-md-1"></div>
			            	<div class="col-md-10">
			            		<div class="col-12" style="padding-left: 0px; padding-right: 0px;">
			            			<div class="box-group">
			            				<?php if ($appr5_comment=='1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 5th Appraiser's Summary Comment (<?php echo $name5 ?>)</p>
				            						<p>
				            							<textarea name="5thAps" rows="3" class="form-control" <?= $ReOly5 ?> >
				            								<?= $apcom5 ?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif ?>
			            				<?php if ($appr6_comment=='1'): ?>
				            				<div class="box bg-pale-secondary">
				            					<div class="box-body">
				            						<p class="box-title text-center  b-0 px-0" style="font-weight: bold"> 6th Appraiser's Summary Comment (<?php echo $name6 ?>)</p>
				            						<p>
				            							<textarea name="6thAps" rows="3" class="form-control" <?= $ReOly6 ?> >
				            								<?= $apcom6 ?>
				            							</textarea>
				            						</p>
				            					</div>
				            				</div>
			            				<?php endif ?>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="col-md-1"></div>
			            </div>
			            <!-- ============================================== OUTGOING YEAR =========================================== -->
						<?php 
							$data = ScriptRunner($Script_Edit,"Accomp");
							$EmpAccomp = json_decode($data, true);
							if (isset($data) && $data !== ''): ?>
	        				<div class="row">
	        					<!-- <div class="col-md-1"></div> -->
	        					<div class="col-md-12">
	        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
	        							<div class="box-group">
	        								<div class="box">
	        									<div class="box-body">
	        										<div class="row">
	        											<div class="col-6">
			        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
			        								            OUTGOING YEAR
			        										</p>
			        									</div>
	        										</div>
													<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
														<thead>
															<tr>
																<th><div style="width: 150px">Accomplishments</div></th>
																<th><div style="width: 150px">Supervisor's Comment</div></th>
																<?php if ((isset($appr3_comment))&&($appr3_comment=='1')): ?>
																<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>)</div></th>
																<?php endif ?>
																<?php if ((isset($appr4_comment))&&($appr4_comment=='1')): ?>
																<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>)</div></th>
																<?php endif ?>
																<?php if ((isset($appr5_comment))&&($appr5_comment=='1')): ?>
																<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>)</div></th>
																<?php endif ?>
																<?php if ((isset($appr6_comment))&&($appr6_comment=='1')): ?>
																<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>)</div></th>
																<?php endif ?>
															</tr>
														</thead>
														<tbody id="fieldList" class="input_fields_wrap">
															<?php for($i=1; $i <= count($EmpAccomp); $i++):?>
																<tr id="row<?php echo $i?>">
																	<td>
																		<div class="form-group">
																			<textarea disabled rows='2' class="form-control"><?php echo $EmpAccomp[$i-1]['employee_accomp']; ?></textarea>
																		</div>
																	</td>
																	<td>
																		<div class="form-group">
																			<?php
																				$mgr_accomp =  (!empty($EmpAccomp[$i-1]['mgr_accomp_comment'])) ? trim($EmpAccomp[$i-1]['mgr_accomp_comment']) : '----';
																			?>
																			<textarea disabled rows='2' class="form-control"><?php echo $mgr_accomp; ?></textarea>
																		</div>
																	</td>
																	<?php if ((isset($appr3_comment))&&($appr3_comment=='1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
																					$apr3_accomp = (!empty($EmpAccomp[$i-1]['apr3_accomp_comment'])) ? trim($EmpAccomp[$i-1]['apr3_accomp_comment']) : '----';
																				?>
																				<textarea disabled rows='2' class="form-control"><?php echo $apr3_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif ?>
																	<?php if ((isset($appr4_comment))&&($appr4_comment=='1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
																					$apr4_accomp = (!empty($EmpAccomp[$i-1]['apr4_accomp_comment'])) ? trim($EmpAccomp[$i-1]['apr4_accomp_comment']) : '----';
																				?>
																				<textarea disabled rows='2' class="form-control"><?php echo $apr4_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif ?>
																	<?php if ((isset($appr5_comment))&&($appr5_comment=='1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
																					$apr5_accomp = (!empty($EmpAccomp[$i-1]['apr5_accomp_comment'])) ? trim($EmpAccomp[$i-1]['apr5_accomp_comment']) : '----';
																				?>
																				<textarea disabled rows='2' class="form-control"><?php echo $apr5_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif ?>
																	<?php if ((isset($appr6_comment))&&($appr6_comment=='1')): ?>
																		<td>
																			<div class="form-group">
																				<?php
																					$apr6_accomp = (!empty($EmpAccomp[$i-1]['apr6_accomp_comment'])) ? trim($EmpAccomp[$i-1]['apr6_accomp_comment']) : '----';
																				?>
																				<textarea disabled rows='2' class="form-control"><?php echo $apr6_accomp; ?></textarea>
																			</div>
																		</td>
																	<?php endif ?>
																</tr>
															<?php endfor; ?>
														</tbody>
													</table>
	        									</div>
	        								</div>
	        							</div>
	        						</div>
	        					</div>
	        					<!-- <div class="col-md-1"></div> -->
	        				</div>
						<?php endif ?>
						<!-- ============================================== OBJECTIVES =========================================== -->
						<?php
							$data = ScriptRunner($Script_Edit,"objectives_remark");
							$obj_column = json_decode($data, true);
							if (isset($data) && $data !== ''):
						?>
        				<div class="row">
        					<!-- <div class="col-md-1"></div> -->
        					<div class="col-md-12">
        						<div class="col-12" style="padding-left: 0px; padding-right: 0px; margin: 10px 0">
        							<div class="box-group">
        								<div class="box">
        									<div class="box-body">
        										<div class="row">
        											<div class="col-6">
		        										<p class="box-title b-0 px-0" style="font-weight: bold ;margin-bottom: 1px">
		        								            OBJECTIVES FOR THE COMING YEAR
		        										</p>
		        									</div>
        										</div>
												<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
													<thead>
														<tr>
															<th><div style="width: 150px">Objectives</div></th>
															<th><div style="width: 150px">Remarks</div></th>
															<th><div style="width: 150px">Supervisor's Remark</div></th>
															<?php if ((isset($appr3_comment))&&($appr3_comment=='1')): ?>
															<th><div style="width: 100px">Appraiser3 (<?php echo $name3 ?>) Remark</div></th>
															<?php endif ?>
															<?php if ((isset($appr4_comment))&&($appr4_comment=='1')): ?>
															<th><div style="width: 100px">Appraiser4 (<?php echo $name4 ?>) Remark</div></th>
															<?php endif ?>
															<?php if ((isset($appr5_comment))&&($appr5_comment=='1')): ?>
															<th><div style="width: 100px">Appraiser5 (<?php echo $name5 ?>) Remark</div></th>
															<?php endif ?>
															<?php if ((isset($appr6_comment))&&($appr6_comment=='1')): ?>
															<th><div style="width: 100px">Appraiser6 (<?php echo $name6 ?>) Remark</div></th>
															<?php endif ?>
														</tr>
													</thead>
													<?php for($i=1; $i <= count($obj_column); $i++): ?>
														<tbody id="fieldList">
															<tr>
																<td>
																	<div class="form-group">
																		<textarea disabled rows='2' class="form-control"><?php echo $obj_column[$i-1]['objectives']; ?></textarea>
																	</div>
																</td>
																<td>
																	<div class="form-group">
																		<?php
																			$emp_objectives =  (!empty($obj_column[$i-1]['emp_objectives'])) ? trim($obj_column[$i-1]['emp_objectives']) : '---';
																		?>
																		<textarea name="emp_objectives[]" disabled rows='2' class="form-control"><?php echo $emp_objectives; ?></textarea>
																	</div>
																</td>
																<td>
																	<div class="form-group">
																		<?php
																			$mgr_objectives =  (!empty($obj_column[$i-1]['mgr_objectives'])) ? trim($obj_column[$i-1]['mgr_objectives']) : '---';
																		?>
																		<textarea  disabled rows='2' class="form-control"><?php echo $mgr_objectives; ?></textarea>
																	</div>
																</td>
																<?php if ((isset($appr3_comment))&&($appr3_comment=='1')): ?>
																	<td>
																		<div class="form-group">
																			<?php
																				$apr3_objectives =  (!empty($obj_column[$i-1]['apr3_objectives'])) ? trim($obj_column[$i-1]['apr3_objectives']) : '---';
																			?>
																			<textarea disabled rows='2' class="form-control"><?php echo $apr3_objectives; ?></textarea>
																		</div>
																	</td>
																<?php endif ?>
																<?php if ((isset($appr4_comment))&&($appr4_comment=='1')): ?>
																	<td>
																		<div class="form-group">
																			<?php
																				$apr4_objectives =  (!empty($obj_column[$i-1]['apr4_objectives'])) ? trim($obj_column[$i-1]['apr4_objectives']) : '---';
																			?>
																			<textarea disabled rows='2' class="form-control"><?php echo $apr4_objectives; ?></textarea>
																		</div>
																	</td>
																<?php endif ?>
																<?php if ((isset($appr5_comment))&&($appr5_comment=='1')): ?>
																	<td>
																		<div class="form-group">
																			<?php
																				$apr5_objectives =  (!empty($obj_column[$i-1]['apr5_objectives'])) ? trim($obj_column[$i-1]['apr5_objectives']) : '---';
																			?>
																			<textarea disabled rows='2' class="form-control"><?php echo $apr5_objectives; ?></textarea>
																		</div>
																	</td>
																<?php endif ?>
																<?php if ((isset($appr6_comment))&&($appr6_comment=='1')): ?>
																	<td>
																		<div class="form-group">
																			<?php
																				$apr6_objectives = (!empty($obj_column[$i-1]['apr6_objectives'])) ? trim($obj_column[$i-1]['apr6_objectives']) : '---';
																			?>
																			<textarea disabled rows='2' class="form-control"><?php echo $apr6_objectives; ?></textarea>
																		</div>
																	</td>
																<?php endif ?>
															</tr>
														</tbody>
													<?php endfor ?>
												</table>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        					<!-- <div class="col-md-1"></div> -->
        				</div>
        				<?php endif ?>
						<!-- ===================================================================================================== -->
						<?php
							/*
							if($num_appraisers <=2){
								
							}elseif($num_appraisers == 3){
								
								print '
									<!--=========================================== 3rd Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											 
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
									</tr>
								';
								
							}elseif($num_appraisers == 4){	
								print '
									<!--=========================================== 3rd and 4th Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment  ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">4th Appraiser\'s Comment ('.$name4.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="4thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly4.'>'.$apcom4.'</textarea></td>
									</tr>
								';

							}elseif($num_appraisers == 5){
									print '
									<!--=========================================== 3rd and 4th Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">4th Appraiser\'s Comment ('.$name4.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="4thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly4.'>'.$apcom4.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 5th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">5th Appraiser\'s Comment ('.$name5.') </td>
											  <td width="1%">&nbsp;</td>
											  
									</tr>
									  
									<tr>
										<td align="center"><textarea name="5thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly5.'>'.$apcom5.'</textarea></td>	
										<td>&nbsp;</td>	
										
									</tr>
								';		
								
							}elseif($num_appraisers == 6){
									print '
									<!--=========================================== 3rd and 4th Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">4th Appraiser\'s Comment ('.$name4.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="4thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly4.'>'.$apcom4.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 5th and 6th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">5th Appraiser\'s Comment ('.$name5.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">6th Appraiser\'s Comment ('.$name6.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="5thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly5.'>'.$apcom5.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="6thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly6.'>'.$apcom6.'</textarea></td>
									</tr>
								';	
								
							}elseif($num_appraisers == 7){	

								print '
									<!--=========================================== 3rd and 4th Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">4th Appraiser\'s Comment ('.$name4.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="4thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly4.'>'.$apcom4.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 5th and 6th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">5th Appraiser\'s Comment ('.$name5.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">6th Appraiser\'s Comment ('.$name6.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="5thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly5.'>'.$apcom5.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="6thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly6.'>'.$apcom6.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 7th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">7th Appraiser\'s Comment ('.$name7.') </td>
											  <td width="1%">&nbsp;</td>
											  
									</tr>
									  
									<tr>
										<td align="center"><textarea name="7thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly7.'>'.$apcom7.'</textarea></td>	
										<td>&nbsp;</td>	
										
									</tr>

								';	
									
							}elseif($num_appraisers ==8){
								
								print '
									<!--=========================================== 3rd and 4th Appraiser\'s Comment -->	
											  
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">3rd Appraiser\'s Comment ('.$name3.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">4th Appraiser\'s Comment ('.$name4.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="3rdAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly3.'>'.$apcom3.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="4thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly4.'>'.$apcom4.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 5th and 6th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">5th Appraiser\'s Comment ('.$name5.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">6th Appraiser\'s Comment ('.$name6.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="5thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly5.'>'.$apcom5.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="6thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly6.'>'.$apcom6.'</textarea></td>
									</tr>
									
									
									<!-- =========================================== 7th and 8th Appraiser\'s Comment -->
									<tr>
											  <td width="51%" align="center" class="tdMenu_HeadBlock">7th Appraiser\'s Comment ('.$name7.') </td>
											  <td width="1%">&nbsp;</td>
											  <td colspan="3" align="center" class="tdMenu_HeadBlock">8th Appraiser\'s Comment ('.$name8.') </td>
									</tr>
									  
									<tr>
										<td align="center"><textarea name="7thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly7.'>'.$apcom7.'</textarea></td>	
										<td>&nbsp;</td>	
										<td colspan="3" align="center"><textarea name="8thAps" cols="52" rows="5" class="TextBoxText_Long" '.$ReOly8.'>'.$apcom8.'</textarea></td>
									</tr>

								';	
							}

							*/
						?>
					</div>          
		          	<?php //print $EditID; ?>
		  			<?php //echo $kk_all;?>
		  		</div>
		  	</div>
		</div>
	</form>
</body>
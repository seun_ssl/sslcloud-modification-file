<?php
session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
/*
if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Manager")
{
if (ValidateURths("TEAM APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
}
elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Personal")
{
if (ValidateURths("APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}
}
elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Auth")
{
if (ValidateURths("CREATE APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
}
 */

//if (ValidateURths("EMP APPRAISAL"."M")!=true){include '../main/NoAccess.php';exit;}
//if (ValidateURths("APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}

if (!isset($KPICount)) {
    $KPICount = 0;
}
if (!isset($EditID)) {
    $EditID = "";
}
if (!isset($Script_Edit)) {
    $Script_Edit = "";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($TWeightage)) {
    $TWeightage = 0;
}
if (!isset($GpNameHash)) {
    $GpNameHash = "";
}
if (!isset($ChkKPIVal)) {
    $ChkKPIVal = "";
}

if (isset($_REQUEST["Opt0"])) {
    $Opt_0 = $_REQUEST["Opt0"];
} else {
    $Opt_0 = "";
}
if (isset($_REQUEST["Opt1"])) {
    $Opt_1 = $_REQUEST["Opt1"];
} else {
    $Opt_1 = "";
}
if (isset($_REQUEST["Opt2"])) {
    $Opt_2 = $_REQUEST["Opt2"];
} else {
    $Opt_2 = "";
}
if (isset($_REQUEST["Opt3"])) {
    $Opt_3 = $_REQUEST["Opt3"];
} else {
    $Opt_3 = "";
}
if (isset($_REQUEST["Opt4"])) {
    $Opt_4 = $_REQUEST["Opt4"];
} else {
    $Opt_4 = "";
}
if (isset($_REQUEST["Opt5"])) {
    $Opt_5 = $_REQUEST["Opt5"];
} else {
    $Opt_5 = "";
}
if (isset($_REQUEST["Opt6"])) {
    $Opt_6 = $_REQUEST["Opt6"];
} else {
    $Opt_6 = "";
}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
        $OpenID = $_REQUEST["GrpName"];
        $Script = "Select * from KPISetting where HashKey='" . ECh($_REQUEST["GrpName"]) . "'";
        //echo ScriptRunner($Script,"AName");
        $KPICount = ScriptRunner($Script, "AFieldCnt");
    }

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Add Deliverable") {
        $GoValidate = true;

        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
            if (ValidateURths("TEAM APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to add new deliverables.', 'red'); }</script>");
                $GoValidate = false;
            }
        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
            if (ValidateURths("CREATE APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to add a new employee deliverables.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
            if (ValidateURths("EMP APPRAISAL" . "M") != true) {
                include '../main/NoAccess.php';
                exit;
            }
        }

        if (strlen($_REQUEST["GpName"]) != 32) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Select an appraisal employee.', 'red'); }</script>");
            $GoValidate = false;
        }

        if (trim($_REQUEST["KPI"]) == "") {
            echo ("<script type='text/javascript'>{ parent.msgbox('Enter a valid key performance indicator deliverable name.', 'red'); }</script>");
            $GoValidate = false;
        }
        // var_dump( $_REQUEST);
        if ($_REQUEST['PgName'] !== 'Manage My Deliverables') {
            if (
                $_REQUEST["Score_Type"] === "Quantity" && ($_REQUEST["quantity_value"] === "" ||
                    is_numeric(trim($_REQUEST["quantity_value"])) === false)
            ) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Invalid target value.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if (is_numeric($_REQUEST["Weightage"])) {
            $Wght = number_format($_REQUEST["Weightage"], 1);
        } else {
            $Wght = 0;
            echo ("<script type='text/javascript'>{ parent.msgbox('Enter a numeric weightage for this key performance indicator.', 'red'); }</script>");
            $GoValidate = false;
        }

        if ($Wght == "") {
            $Wght = 0;
        }

        if ($_REQUEST["Weightage"] == "0.5" || $_REQUEST["Weightage"] == ".5") {
            $Wght = 0.5;
        }

        if ($_REQUEST["Weightage"] == "" || $_REQUEST["Weightage"] == "--") {
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] != "Personal") {
                echo ("<script type='text/javascript'>{ parent.msgbox('Select a weightage for this key performance indicator.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if ($Wght > 100) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Maximum employee weightage allowed is 100%', 'red'); }</script>");
            $GoValidate = false;
        }

        if ($GoValidate == true) {
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "KPI";
            $HashKey = md5($UniqueKey);

            //exit('Wait here');

            $gpname_val = str_replace("'", "''", ECh($_REQUEST["GpName"]));
            $kpi_val = str_replace("'", "''", $_REQUEST["KPI"]);
            $kra_val = str_replace("'", "''", ECh($_REQUEST["KRA"]));
            $kra_sub_val = str_replace("'", "''", ECh($_REQUEST["KRA_Sub_Area"]));
            $kra_group_val = str_replace("'", "''", ECh($_REQUEST["KRA_Group"]));
            //$wght_val = mysql_escape_string($Wght);
            $aremark_val = str_replace("'", "''", $_REQUEST["ARemark"]);

            if ($_REQUEST['PgName'] === 'Manage My Deliverables') {
                $Script = "INSERT INTO [KPICore] ([AID],[KPI],[KRA],[KRA_Sub_Area],[KRA_Group],[Weightage],[ARemark],[Opt0],[Opt1],[Opt2],[Opt3],[Opt4],[Opt5],[Opt6],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $gpname_val . "',
	'" . $kpi_val . "',
	'" . $kra_val . "',
	'" . $kra_sub_val . "',
	'" . $kra_group_val . "',
	'" . $Wght . "',
	'" . $aremark_val . "',
	'" . ECh($Opt_0) . "',
	'" . ECh($Opt_1) . "',
	'" . ECh($Opt_2) . "',
	'" . ECh($Opt_3) . "',
	'" . ECh($Opt_4) . "',
	'" . ECh($Opt_5) . "',
	'" . ECh($Opt_6) . "',
	'N',
	'" . $_SESSION["StkTck" . "HKey"] . "',
	GetDate(),
	'" . $HashKey . "'

	)";
            } else {
                $Script = "INSERT INTO [KPICore] ([AID],[KPI],[KRA],[KRA_Sub_Area],[KRA_Group],[Weightage],[ARemark],[Opt0],[Opt1],[Opt2],[Opt3],[Opt4],[Opt5],[Opt6],[Status],[AddedBy],[AddedDate],[HashKey],[Score_Type],[Quantity_Value]) VALUES ('" . $gpname_val . "',
	'" . $kpi_val . "',
	'" . $kra_val . "',
	'" . $kra_sub_val . "',
	'" . $kra_group_val . "',
	'" . $Wght . "',
	'" . $aremark_val . "',
	'" . ECh($Opt_0) . "',
	'" . ECh($Opt_1) . "',
	'" . ECh($Opt_2) . "',
	'" . ECh($Opt_3) . "',
	'" . ECh($Opt_4) . "',
	'" . ECh($Opt_5) . "',
	'" . ECh($Opt_6) . "',
	'N',
	'" . $_SESSION["StkTck" . "HKey"] . "',
	GetDate(),
	'" . $HashKey . "',
	'" . $_REQUEST['Score_Type'] . "',
	'" . trim($_REQUEST['quantity_value']) . "'


	)";
            }

            ScriptRunnerUD($Script, "Inst");
            unset($Script);
            //AuditLog("INSERT","New employee deliverable added successfully [".$_REQUEST["KPI"]."]");

            $KPI_str = substr($_REQUEST["KPI"], 0, 200);
            $KPI_str = str_replace("'", "''", $KPI_str);

            AuditLog("UPDATE", "New employee deliverable added successfully [" . $KPI_str . "]");

            echo ("<script type='text/javascript'>{ parent.msgbox('New deliverable added successfully.', 'green'); }</script>");

            /* ==================================================================================================*/
            /* ==================================================================================================*/
            /* ====================================================d==============================================*/
            /*
            Hi #EmpManager#,<br />#EmpName# has made a change to his/her deliverables which requires your confirmation and approval.<br /><br />#Change#<br /><br />You should check that this change matches your teams goals and overall deliverables and it covers all the duties you currently have assigned to him/her.<br /><br />Should you have any questions, you may contact him/her by email at <strong>#EmpEmail#</strong>.<br><br>For more information, please login to your account and your team's list of deliverables.
             */

            if (isset($_REQUEST["PgTy"]) && ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth")) {
                $Script = "Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41burg7e5bd83a4b17'"; //Employee KIP Modification
            }
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                $Script = "Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41burg6e5bd83a4c29'"; //Employee Self KIP Modification
            }

            $Script_Emp = "Select (E.SName + ' ' + E.FName) as Nm, Email, (Select (Et.Email) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgrEmail, (Select (Et.SName + ' ' + Et.FName) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgr from EmpTbl E where HashKey=(Select AName from KPISetting where HashKey='" . ECh($_REQUEST["GrpName"]) . "')";

            $LoginDet = ScriptRunner($Script, "MMsg");
            $LoginDet = str_replace('#EmpName#', ScriptRunner($Script_Emp, "Nm"), $LoginDet);
            $LoginDet = str_replace('#EmpManager#', ScriptRunner($Script_Emp, "EmpMgr"), $LoginDet);
            $LoginDet = str_replace('#Change#', "Deliverable: <strong>" . ECh($_REQUEST["KPI"]) . "</strong><br/>Key Result Area: <strong>" . ECh($_REQUEST["KRA"]) . "<br/>Weightage: <strong>" . ECh($Wght) . "</strong>.", $LoginDet);
            $LoginDet = str_replace('#EmpManagerEmail#', ScriptRunner($Script_Emp, "EmpMgrEmail"), $LoginDet);
            $LoginDet = str_replace('#EmpEmail#', ScriptRunner($Script_Emp, "Email"), $LoginDet);
            $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);

            $ToMgr = ScriptRunner($Script_Emp, "EmpMgrEmail");
            $ToEmp = ScriptRunner($Script_Emp, "Email");
            $Bdy = $LoginDet;
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                $Subj = ScriptRunner($Script, "MSub") . " [" . $_SESSION["StkTck" . "FName"] . "]";
            } else {
                $Subj = ScriptRunner($Script, "MSub") . " [" . ScriptRunner($Script_Emp, "Nm") . "]";
            }
            $Atth = "";

            //Send Mail to employee
            //Send Mail to employee
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                //QueueMail($ToEmp,$Subj,$Bdy,$Atth,'');
            }
            // **************************************************************
            //Send mail to manager
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                //QueueMail($ToMgr,$Subj,$Bdy,$Atth,'');
            }
        }
    }
    //**************************************************************************************************
    //**************************************************************************************************

    elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Deliverable") {
        /* Set HashKey of Account to be Updated */
        $EditID = ECh($_REQUEST["UpdID"]);
        $HashKey = ECh($_REQUEST["UpdID"]);

        $GoValidate = true;

        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
            if (ValidateURths("EMP APPRAISAL" . "M") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to update this deliverable.', 'red'); }</script>");
                $GoValidate = false;
            }
        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
            if (ValidateURths("TEAM APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to update this employee deliverable.', 'red'); }</script>");
                $GoValidate = false;
            }
        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
            if (ValidateURths("CREATE APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to add a new employee deliverables.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if (strlen($_REQUEST["GpName"]) != 32) {
            echo ("<script type='text/javascript'>{ parent.msgbox('Select an appraisal group.', 'red'); }</script>");
            $GoValidate = false;
        }

        if (trim($_REQUEST["KPI"]) == "") {
            echo ("<script type='text/javascript'>{ parent.msgbox('Enter a valid key performance indicator deliverable name.', 'red'); }</script>");
            $GoValidate = false;
        }
        // var_dump($_REQUEST);
        if ($_REQUEST['PgName'] !== 'Manage My Deliverables') {
            if (
                $_REQUEST["Score_Type"] === "Quantity" && ($_REQUEST["quantity_value"] === "" ||
                    is_numeric(trim($_REQUEST["quantity_value"])) === false)
            ) {
                echo ("<script type='text/javascript'>{ parent.msgbox('Invalid target value.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        $Wght = $_REQUEST["Weightage"];
        if ($Wght == "" || $Wght < 1) {
            $Wght = 0;
        }
        if ($_REQUEST["Weightage"] == "" || $_REQUEST["Weightage"] == "--") {
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
                echo ("<script type='text/javascript'>{ parent.msgbox('Select a weightage for this key performance indicator.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if ($_REQUEST["Weightage"] == "0.5" || $_REQUEST["Weightage"] == ".5") {
            $Wght = 0.5;
        }

        if ($GoValidate == true) {
            $gpname_val = str_replace("'", "''", ECh($_REQUEST["GpName"]));
            $kpi_val = str_replace("'", "''", $_REQUEST["KPI"]);
            $kra_val = str_replace("'", "''", ECh($_REQUEST["KRA"]));
            $kra_sub_val = str_replace("'", "''", ECh($_REQUEST["KRA_Sub_Area"]));
            $kra_group_val = str_replace("'", "''", ECh($_REQUEST["KRA_Group"]));
            //$wght_val = mysql_escape_string($Wght);
            $aremark_val = str_replace("'", "''", $_REQUEST["ARemark"]);

            /*
            print "gpname_val: $gpname_val <br/>
            kpi_val: $kpi_val <br/>
            kra_val: $kra_val <br/>
            kra_group_val: $kra_group_val <br/>
            aremark_val: $aremark_val <br/>
            ";
            exit('stop to test');
             */
            if ($_REQUEST['PgName'] === 'Manage My Deliverables') {
                $Script = "Update [KPICore] set [AID]='" . $gpname_val . "',[KPI]='" . $kpi_val . "',[KRA]='" . $kra_val . "',
                
                [KRA_Group]='" . $kra_group_val . "',
                [KRA_Sub_Area]='" . $kra_sub_val . "',
                
                [Weightage]='" . $Wght . "',[ARemark]='" . $aremark_val . "',[Opt0]='" . ECh($Opt_0) . "',[Opt1]='" . ECh($Opt_1) . "',[Opt2]='" . ECh($Opt_2) . "',[Opt3]='" . ECh($Opt_3) . "',[Opt4]='" . ECh($Opt_4) . "',[Opt5]='" . ECh($Opt_5) . "',[Opt6]='" . ECh($Opt_6) . "',[Status]='U',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $HashKey . "'";
            } else {
                $Script = "Update [KPICore] set [AID]='" . $gpname_val . "',[KPI]='" . $kpi_val . "',[KRA]='" . $kra_val . "',
                
                [KRA_Group]='" . $kra_group_val . "',
                  [KRA_Sub_Area]='" . $kra_sub_val . "',
                
                [Weightage]='" . $Wght . "',[ARemark]='" . $aremark_val . "',[Opt0]='" . ECh($Opt_0) . "',[Opt1]='" . ECh($Opt_1) . "',[Opt2]='" . ECh($Opt_2) . "',[Opt3]='" . ECh($Opt_3) . "',[Opt4]='" . ECh($Opt_4) . "',[Opt5]='" . ECh($Opt_5) . "',[Opt6]='" . ECh($Opt_6) . "',[Status]='U',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate(),[Score_Type]= '" . $_REQUEST['Score_Type'] . "',[Quantity_Value]='" . trim($_REQUEST['quantity_value']) . "' where [HashKey]= '" . $HashKey . "'";
            }

            ScriptRunnerUD($Script, "Inst");

            $KPI_str = substr($_REQUEST["KPI"], 0, 200);

            $KPI_str = str_replace("'", "''", $KPI_str);

            AuditLog("UPDATE", "Employee deliverable updated successfully [" . $KPI_str . "]");

            echo ("<script type='text/javascript'>{ parent.msgbox('Employee deliverable updated successfully.', 'green'); }</script>");
            //Clear Selection//

            /* ==================================================================================================*/
            /* ==================================================================================================*/
            /* ====================================================d==============================================*/
            /*
            Hi #EmpManager#,<br />#EmpName# has made a change to his/her deliverables which requires your confirmation and approval.<br /><br />#Change#<br /><br />You should check that this change matches your teams goals and overall deliverables and it covers all the duties you currently have assigned to him/her.<br /><br />Should you have any questions, you may contact him/her by email at <strong>#EmpEmail#</strong>.<br><br>For more information, please login to your account and your team's list of deliverables.
             */

            if (isset($_REQUEST["PgTy"]) && ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth")) {
                $Script = "Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41burg7e5bd83a4b17'"; //Employee KIP Modification
            }
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                $Script = "Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41burg6e5bd83a4c29'"; //Employee Self KIP Modification
            }

            $Script_Emp = "Select (E.SName + ' ' + E.FName) as Nm, Email, (Select (Et.Email) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgrEmail, (Select (Et.SName + ' ' + Et.FName) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgr from EmpTbl E where HashKey=(Select AName from KPISetting where HashKey='" . ECh($_REQUEST["GrpName"]) . "')";

            $LoginDet = ScriptRunner($Script, "MMsg");
            $LoginDet = str_replace('#EmpName#', ScriptRunner($Script_Emp, "Nm"), $LoginDet);
            $LoginDet = str_replace('#EmpManager#', ScriptRunner($Script_Emp, "EmpMgr"), $LoginDet);
            $LoginDet = str_replace('#Change#', "Deliverable: <strong>" . ECh($_REQUEST["KPI"]) . "</strong><br/>Key Result Area: <strong>" . ECh($_REQUEST["KRA"]) . "<br/>Weightage: <strong>" . ECh($Wght) . "</strong>.", $LoginDet);
            $LoginDet = str_replace('#EmpManagerEmail#', ScriptRunner($Script_Emp, "EmpMgrEmail"), $LoginDet);
            $LoginDet = str_replace('#EmpEmail#', ScriptRunner($Script_Emp, "Email"), $LoginDet);
            $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);

            $ToMgr = ScriptRunner($Script_Emp, "EmpMgrEmail");
            $ToEmp = ScriptRunner($Script_Emp, "Email");
            $Bdy = $LoginDet;
            if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                $Subj = ScriptRunner($Script, "MSub") . " [" . $_SESSION["StkTck" . "FName"] . "]";
            } else {
                $Subj = ScriptRunner($Script, "MSub") . " [" . ScriptRunner($Script_Emp, "Nm") . "]";
            }

            $Atth = "";

            //Send Mail to employee

            //echo ("<script type='text/javascript'>{ parent.msgbox('Got here to send mail to '".$ToEmp.", 'green'); }<//script>");
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                //QueueMail($ToEmp,$Subj,$Bdy,$Atth,'');
            }
            // **************************************************************
            //Send mail to manager
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                //QueueMail($ToMgr,$Subj,$Bdy,$Atth,'');
            }

            //Gets last selection
            $Script_Edit = "Select *, convert(varchar(5),AddedBy,114) Dt from KPICore where HashKey='" . $EditID . "'";
            //$EditID="--";
        }
        $Script_Edit = "Select *, convert(varchar(5),AddedBy,114) Dt from KPICore where HashKey='" . $EditID . "'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0) {
        $EditID = 0;
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && $_REQUEST["DelBox" . $php_i] != "") {
                $EditID = $_REQUEST["DelBox" . $php_i];
                $Script_Edit = "Select *, convert(varchar(5),AddedBy,114) Dt from KPICore where HashKey='" . $EditID . "'";
            }
        }
        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {

            if (ValidateURths("TEAM APPRAISAL" . "T") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to authorize these deliverable(s).', 'red'); }</script>");
                $GoValidate = false;
            }
        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {

            if (ValidateURths("CREATE APPRAISAL" . "T") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to authorize these deliverable(s).', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if ($GoValidate == true) {
            $o = 0;
            for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                    $o = $o + 1;
                    if ($o == 1) {
                        $ChkKPIVal = "'" . $_REQUEST["DelBox" . $php_i] . "'";
                    } elseif ($o > 1) {
                        $ChkKPIVal = $ChkKPIVal . ",'" . $_REQUEST["DelBox" . $php_i] . "'";
                    }
                    $EditID = $_REQUEST["DelBox" . $php_i];
                }
            }

            if (strlen($ChkKPIVal) < 32) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You must have selected records to authorize.', 'red'); }</script>");
                $GoValidate = false;
            }

            if ($GoValidate == true) {
                $Script = "Select Sum(Weightage) Sm from KPICore where (Status in ('A') or HashKey in (" . $ChkKPIVal . ")) and AID=(SELECT AID from KPICore where HashKey='" . $EditID . "')";
                //echo $Script;
                if (ScriptRunner($Script, "Sm") != 100) {
                    echo ("<script type='text/javascript'>{parent.msgbox('Total selected employee weightage must be equal to 100% to authorized.','red');}</script>");
                    $EditID = "--";
                    $GoValidate = false;
                }
            }

            if ($GoValidate == true) {
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = $_REQUEST["DelBox" . $php_i];

                        $Script_ID = "SELECT AName, HashKey from KPISetting where HashKey='" . $_REQUEST["DelBox" . $php_i] . "'";

                        $Script = "UPDATE [KPICore]
							SET [Status] = 'A'
							,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
							,[AuthDate]=GetDate()
							WHERE [HashKey] = '" . $EditID . "'";
                        ScriptRunnerUD($Script, "Authorize");
                        AuditLog("AUTHORIZE", "Deliverables authorized for group [" . ScriptRunner($Script_ID, "AName") . "]", $EditID);

                        echo ("<script type='text/javascript'>{ parent.msgbox('Selected deliverable(s) authorized successfully.', 'green'); }</script>");
                        $EditID = "--";
                    }
                } //End of For
            } //End of Govalidate
            $Script_Edit = "Select * from UGpRights where HashKey='z'";
            $EditID = "--";
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0) {
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = $_REQUEST["DelBox" . $php_i];
                $Script_ID = "SELECT AName, HashKey from KPISetting where HashKey='" . ECh($EditID) . "'";

                $Script = "UPDATE [KPICore]
					SET [Status] = 'U'
					,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
					,[AuthDate]=GetDate()
					WHERE [HashKey] = '" . ECh($EditID) . "'";
                ScriptRunnerUD($Script, "Authorize");
                AuditLog("UNAUTHORIZE", "Deliverables unauthorized for group [" . ScriptRunner($Script_ID, "AName") . "]");

                echo ("<script type='text/javascript'>{ parent.msgbox('Selected deliverable(s) unauthorized successfully.', 'green'); }</script>");
                $EditID = "--";
            }
        }
        $Script_Edit = "Select * from UGpRights where HashKey='z'";
        $EditID = "--";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
            if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                $EditID = $_REQUEST["DelBox" . $php_i];
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFOR UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from [LvSettings] WHERE [HashKey] = '" . ECh($EditID) . "'") == true) {
                    $Script_ID = "SELECT AName, HashKey from KPISetting where HashKey='" . ECh($EditID) . "'";

                    $Script = "UPDATE [KPICore]
					SET [Status] = 'D'
					,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
					,[AuthDate]=GetDate()
					WHERE [HashKey] = '" . ECh($EditID) . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("DELETE", "Deliverables deleted for group [" . ScriptRunner($Script_ID, "AName") . "]");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected deliverable(s) deleted successfully.', 'green'); }</script>");
                    $EditID = "--";
                }
            }
        }
        $EditID = "--";
    }
} // Closes the DoS IF
?>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>

<script>
    $(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(function() {
        $(document).tooltip();
    });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
        padding: 5px;
    }

    [type=checkbox]+label:before,
    [type=checkbox]:not(.filled-in)+label:after {

        width: 15px;
        height: 15px;
    }
</style>

<body onLoad="MM_preloadImages('../images/bi_butn/Search.jpg')">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-6" style="margin-top: 1%">
                        <div class="form-group row ">
                            <div class="col-sm-2" style="font-weight: bolder; font-size: 14px;">Employee:</div>
                            <div class="col-sm-10" style="font-weight: bolder; font-size: 14px;">
                                <?php
                                if (isset($_REQUEST["PgTy"]) && ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth")) {
                                    if (isset($_REQUEST["GrpName"]) && $_REQUEST["GrpName"] != "" && $_REQUEST["GrpName"] != "--") {
                                        $Script = "Select AType from KPISetting where HashKey='" . $_REQUEST["GrpName"] . "' and Status<>'D'";
                                        if (ScriptRunner($Script, "AType") == 'T') {
                                            $Script = "Select AName as Nm, * from KPISetting Ks where Ks.HashKey='" . $_REQUEST["GrpName"] . "'";
                                        } else {
                                            $Script = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.* from KPISetting Ks, EmpTbl Et where Et.Status in ('A') and Et.EmpStatus='Active' and Ks.AName=Et.HashKey and Ks.HashKey='" . $_REQUEST["GrpName"] . "'";
                                        }

                                        echo ScriptRunner($Script, "Nm");
                                        $KPICount = ScriptRunner($Script, "AFieldCnt");
                                        echo "<input type='hidden' name='GpName' id='GpName' value='" . $_REQUEST["GrpName"] . "' />";
                                        $GpNameHash = $_REQUEST["GrpName"];
                                    }
                                } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                                    $Script = "SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.HashKey as KHKey, Ks.* from KPISetting Ks, EmpTbl Et where Et.Status in ('A') and Et.EmpStatus='Active' and Ks.AName=Et.HashKey and Et.HashKey=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') ))";
                                    echo ScriptRunner($Script, "Nm");
                                    $KPICount = ScriptRunner($Script, "AFieldCnt");
                                    $GpNameHash = ScriptRunner($Script, "KHKey");
                                    echo "<input type='hidden' name='GpName' id='GpName' value='" . ScriptRunner($Script, "KHKey") . "' />";
                                    echo "<input type='hidden' name='GrpName' id='GrpName' value='" . ScriptRunner($Script, "KHKey") . "' />";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    if (isset($_REQUEST["PgTy"]) && ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth")) { ?>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-3 col-md-4"></div>
                                <div class="col-2 col-md-1" style="margin-top:2%;">
                                    <button class="btn btn-default btn-sm" title="Find an employee" name="FindanEmployee" id="FindanEmployee" onClick="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','hrm/srh_Emp.php?RetPg=kpi/Nw_KPIEmp.php&GroupName=Employee&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')"><i class="fa fa-search"></i></button>
                                </div>
                                <div class="input-group input-group-sm col-6  pull-right" style="margin-top: 2%">
                                    <select class="form-control" name="GrpName" id="GrpName">
                                        <?php
                                        echo '<option value="--" selected="selected">--</option>';
                                        if (isset($_REQUEST["GrpName"])) {
                                            $OpenID = $_REQUEST["GrpName"];
                                        } else {
                                            $OpenID = "";
                                        }
                                        //======================================================================================
                                        //===================== SELECTING EMPLOYEES ============================================
                                        //======================================================================================

                                        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {

                                            if (ValidateURths("CREATE APPRAISAL" . "A") == true) {
                                                $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A')and AType='T' order by AName Asc");
                                            } else {
                                                $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('Z')and AType='T' order by AName Asc");
                                            }
                                        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
                                            $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A')and AType='T' order by AName Asc");
                                        }
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            if ($OpenID == $row2['HashKey']) {
                                                if ($OpenID != '' && $OpenID != '--') {
                                                    echo '<option selected value="' . $row2['HashKey'] . '">[' . strtoupper($row2['Nm']) . ']</option>';
                                                }
                                            } else {
                                                echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';
                                            }
                                        }
                                        include '../login/dbClose2.php';

                                        //======================================================================================
                                        //===================== SELECTING EMPLOYEES ============================================
                                        //======================================================================================
                                        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
                                            $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.HashKey from KPISetting Ks, EmpTbl Et where Et.Status in ('A') and Ks.Status<>'D' and Ks.AName=Et.HashKey and EmpMgr=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) order by Et.SName Asc");
                                        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
                                            $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.* from KPISetting Ks, EmpTbl Et where Ks.Status<>'D' and Et.Status in ('A') and Ks.AName=Et.HashKey order by Et.SName Asc");
                                        }
                                        include '../login/dbOpen2.php';
                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                            if ($OpenID == $row2['HashKey']) {
                                                if ($OpenID != '' && $OpenID != '--') {
                                                    echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';
                                                }
                                            } else {
                                                echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';
                                            }
                                        }
                                        include '../login/dbClose2.php';
                                        ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row ">
                                    <label class="col-sm-5 col-form-label" style="font-size: 11px;">Area of Focus:</label>
                                    <div class="col-sm-7 input-group input-group-sm" style="padding-left: 30px">
                                        <select class="form-control" name="KRA" id="KRA">
                                            <option value="--" selected="selected">--</option>
                                            <?php
                                            if ($EditID == "" || $EditID == "--") {
                                                if (isset($_REQUEST["KRA"])) {
                                                    $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Key Result Areas' and Val1<>'" . $_REQUEST["KRA"] . "') AND Status <> 'D' ORDER BY Val1");
                                                } else {
                                                    $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Key Result Areas')  AND Status <> 'D' ORDER BY Val1");
                                                }
                                            } else {
                                                $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Key Result Areas')  AND Status <> 'D' ORDER BY Val1");
                                            }
                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                            }
                                            include '../login/dbClose2.php';
                                            if ($EditID != "") {
                                                echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "KRA") . '">' . ScriptRunner($Script_Edit, "KRA") . '</option>';
                                            } else {
                                                if (isset($_REQUEST["KRA"])) {
                                                    echo '<option selected=selected value="' . $_REQUEST["KRA"] . '">' . $_REQUEST["KRA"] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="input-group-btn">
                                            <?php
                                            $ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
                                            $imgID = "MastersDepartment";
                                            if ($ModMaster == true) {
                                                $imgPath = "fa fa-plus";
                                                $Titl = "Add a new Key Result Area eg. Revenue Management, Customer Management";
                                                $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=Key+Result+Areas&LDb=Single&amp;Elmt=Key+Result+Areas',320,400,'No')";
                                            } else {
                                                $imgPath = "fa fa-info";
                                                $Titl = "Select a Key Result Area under which this KPI falls";
                                                $OnClk = "";
                                            }
                                            echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row ">
                                    <label class="col-sm-5 col-form-label"> Sub Area of Focus:</label>
                                    <div class="col-sm-7 input-group input-group-sm" style="padding-left: 30px">
                                        <select class="form-control" name="KRA_Sub_Area" id="KRA_Sub_Area">
                                            <option value="--" selected="selected">--</option>
                                            <?php
                                            if ($EditID == "" || $EditID == "--") { /*
                                                                        if (isset($_REQUEST["KRA_Group"]))
                                                                        {$dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Val1<>'".$_REQUEST["KRA_Group"]."' and Status<>'D') ORDER BY Val1");}
                                                                        else
                                                                        {$dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Status<>'D') ORDER BY Val1");}
                                                                        */

                                                $dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Sub Area' and Val1<>'" . $_REQUEST["KRA_Sub_Area"] . "' and Status<>'D') ORDER BY Val1");
                                            } else {
                                                $dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Sub Area' and Status<>'D') ORDER BY Val1");
                                            }
                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                            }
                                            include '../login/dbClose2.php';
                                            if ($EditID != "") {
                                                echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "KRA_Sub_Area") . '">' . ScriptRunner($Script_Edit, "KRA_Sub_Area") . '</option>';
                                            } else {
                                                if (isset($_REQUEST["KRA_Sub_Area"])) {
                                                    echo '<option selected=selected value="' . $_REQUEST["KRA_Sub_Area"] . '">' . $_REQUEST["KRA_Sub_Area"] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="input-group-btn">
                                            <?php
                                            $ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
                                            $imgID = "MastersDepartment";
                                            if ($ModMaster == true) {
                                                $imgPath = "fa fa-plus";
                                                $Titl = "Add Sub Area of Focus";
                                                $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=KRA+Sub+Area&LDb=Single&amp;Elmt=Key+Result+Areas',320,400,'No')";
                                            } else {
                                                $imgPath = "fa fa-info";
                                                $Titl = "Select a Sub Area of Focus under which this KPI falls";
                                                $OnClk = "";
                                            }
                                            echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row ">
                                    <label class="col-sm-5 col-form-label"> Group:</label>
                                    <div class="col-sm-7 input-group input-group-sm" style="padding-left: 30px">
                                        <select class="form-control" name="KRA_Group" id="KRA_Group">
                                            <option value="--" selected="selected">--</option>
                                            <?php
                                            if ($EditID == "" || $EditID == "--") { /*
                                                                        if (isset($_REQUEST["KRA_Group"]))
                                                                        {$dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Val1<>'".$_REQUEST["KRA_Group"]."' and Status<>'D') ORDER BY Val1");}
                                                                        else
                                                                        {$dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Status<>'D') ORDER BY Val1");}
                                                                        */

                                                $dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Val1<>'" . $_REQUEST["KRA_Group"] . "' and Status<>'D') ORDER BY Val1");
                                            } else {
                                                $dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='KRA Group' and Status<>'D') ORDER BY Val1");
                                            }
                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                            }
                                            include '../login/dbClose2.php';
                                            if ($EditID != "") {
                                                echo '<option selected=selected value="' . ScriptRunner($Script_Edit, "KRA_Group") . '">' . ScriptRunner($Script_Edit, "KRA_Group") . '</option>';
                                            } else {
                                                if (isset($_REQUEST["KRA_Group"])) {
                                                    echo '<option selected=selected value="' . $_REQUEST["KRA_Group"] . '">' . $_REQUEST["KRA_Group"] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="input-group-btn">
                                            <?php
                                            $ModMaster = ValidateURths("APPRAISAL MODULE MASTERS" . "A");
                                            $imgID = "MastersDepartment";
                                            if ($ModMaster == true) {
                                                $imgPath = "fa fa-plus";
                                                $Titl = "Add KRA Group";
                                                $OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters?GroupName=KRA+Group&LDb=Single&amp;Elmt=Key+Result+Areas',320,400,'No')";
                                            } else {
                                                $imgPath = "fa fa-info";
                                                $Titl = "Select a Key Result Area under which this KPI falls";
                                                $OnClk = "";
                                            }
                                            echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '"><i class="' . $imgPath . '"></i></button>';
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>






                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label">Departmental Objectives:</label>
                            <div class="col-sm-9" style="padding-left: 5px">
                                <?php
                                if ($EditID != "" && $EditID != '--') {
                                    //echo '<input type="text" name="KPI" size="25" maxlength="250" class="TextBoxText_Long_Comp" id="KPI" value="'.ScriptRunner($Script_Edit,"KPI").'"/>';
                                    echo '<textarea name="KPI" rows="2" class="form-control" id="KPI">' . ScriptRunner($Script_Edit, "KPI") . '</textarea>';
                                } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                    //echo '<input type="text" name="KPI" size="25" maxlength="250" class="TextBoxText_Long_Comp" id="KPI" value=""/>';
                                    echo '<textarea name="KPI" rows="2" class="form-control" id="KPI"></textarea>';
                                } else {
                                    if (isset($_REQUEST["KPI"])) {
                                        //echo '<input type="text" name="KPI" size="25" maxlength="250" class="TextBoxText_Long_Comp" id="KPI" value="'.$_REQUEST["KPI"].'"/>';
                                        echo '<textarea name="KPI" rows="2" class="form-control" id="KPI">' . $_REQUEST["KPI"] . '</textarea>';
                                    } else {
                                        //echo '<input type="text" name="KPI" size="25" maxlength="250" class="TextBoxText_Long_Comp" id="KPI" value=""/>';
                                        echo '<textarea name="KPI" rows="2" class="form-control" id="KPI"></textarea>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label">Weightage(%)<span style="color: red">*</span>:</label>
                            <div class="col-sm-9" style="padding-left: 5px">
                                <?php
                                if ($EditID != "" && $EditID != '--') {
                                    echo '<input type="text" name="Weightage" maxlength="4" class="form-control" id="Weightage" value="' . ScriptRunner($Script_Edit, "Weightage") . '"/>';
                                } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                    echo '<input type="text" name="Weightage" maxlength="4" class="form-control" id="Weightage" value="0"/>';
                                } else {
                                    if (isset($_REQUEST["Weightage"])) {
                                        echo '<input type="text" name="Weightage"  maxlength="4" class="form-control" id="Weightage" value="' . $_REQUEST["Weightage"] . '"/>';
                                    } else {
                                        echo '<input type="text" name="Weightage"  maxlength="4" class="form-control" id="Weightage" value="0"/>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label">Individual Departmental Objectives:</label>
                            <div class="col-sm-9" style="padding-left: 5px">
                                <?php
                                if ($EditID != "" && $EditID != "--") {
                                    echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark">' . ScriptRunner($Script_Edit, "ARemark") . '</textarea>';
                                } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                    echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark"></textarea>';
                                } else {
                                    if (isset($_REQUEST["ARemark"])) {
                                        echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark">' . $_REQUEST["ARemark"] . '</textarea>';
                                    } else {
                                        echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark"></textarea>';
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <?php if ($_REQUEST['PgName'] !== 'Manage My Deliverables') : ?>


                            <?php if ($EditID != "" && $EditID != "--") :
                                $query = "Select *, convert(varchar(5),AddedBy,114) Dt from KPICore where HashKey='" . $EditID . "'";
                                //    "select count(*) Ct from KPICore where AID=(Select HashKey from KPISetting where Status='A' and AName=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')))";
                                $connectionInfo2 = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
                                $conn2 = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo2);
                                if ($conn2 === false) {
                                    echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/error/trap_error.php?ErrTyp=1';}</script>");
                                }
                                $res2 = sqlsrv_query($conn2, $query);
                                $row = sqlsrv_fetch_array($res2, SQLSRV_FETCH_ASSOC);

                                // var_dump($row);

                            ?>









                                <div class="form-group row ">
                                    <label class="col-sm-3 col-form-label">Score Type:</label>
                                    <div class="col-sm-9" style="padding-left: 5px">
                                        <select class="form-control" name="Score_Type" id="Score_Type" onchange="selectScoreType()">
                                            <option value="--">--</option>
                                            <option value="Tags" <?php echo (isset($row['Score_Type']) && $row['Score_Type'] === "Tags") ? "selected" : "" ?>>Tags</option>
                                            <option value="Values" <?php echo (isset($row['Score_Type']) && $row['Score_Type'] === "Values") ? 'selected' : '' ?>>Values</option>
                                            <option value="Quantity" <?php echo (isset($row['Score_Type']) && $row['Score_Type'] === "Quantity") ? 'selected' : '' ?>>Targets</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="tity_value" style="display:none">
                                    <div class="form-group row  ">
                                        <label class="col-sm-3 col-form-label">Target Value:</label>
                                        <div class="col-sm-9" style="padding-left: 5px">
                                            <input type="text" name="quantity_value" id="quantity_value" value="<?php echo isset($row['Quantity_Value']) ? trim($row['Quantity_Value']) : '' ?> " class="form-control">
                                        </div>
                                    </div>
                                </div>

                            <?php else : ?>




                                <div class="form-group row ">
                                    <label class="col-sm-3 col-form-label">Score Type:</label>
                                    <div class="col-sm-9" style="padding-left: 5px">
                                        <select class="form-control" name="Score_Type" id="Score_Type" onchange="selectScoreType()">
                                            <option value="--">--</option>
                                            <option value="Tags" <?php echo (isset($_REQUEST['Score_Type']) && $_REQUEST['Score_Type'] === "Tags") ? "selected" : "" ?>>Tags</option>
                                            <option value="Values" <?php echo (isset($_REQUEST['Score_Type']) && $_REQUEST['Score_Type'] === "Values") ? 'selected' : '' ?>>Values</option>
                                            <option value="Quantity" <?php echo (isset($_REQUEST['Score_Type']) && $_REQUEST['Score_Type'] === "Quantity") ? 'selected' : '' ?>>Target</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="tity_value" style="display:none">
                                    <div class="form-group row  ">
                                        <label class="col-sm-3 col-form-label">Target Value:</label>
                                        <div class="col-sm-9" style="padding-left: 5px">
                                            <input type="text" name="quantity_value" id="quantity_value" value="<?php echo isset($_REQUEST['quantity_value']) ? trim($_REQUEST['quantity_value']) : '' ?> " class="form-control">
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>





                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"><?php if ($KPICount > 0) {
                                                                        echo "Measure 0:";
                                                                    } ?></label>
                            <div class="col-sm-9 input-group-sm">
                                <?php
                                if ($KPICount > 1) {
                                    if ($EditID != "" && $EditID != '--') {
                                        echo '<input type="text" name="Opt0" maxlength="250" class="form-control" id="Opt0" value="' . ScriptRunner($Script_Edit, "Opt0") . '"/>';
                                    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                        echo '<input type="text" name="Opt0" maxlength="250" class="form-control" id="Opt0" value=""/>';
                                    } else {
                                        if (isset($_REQUEST["Opt0"]) && strlen($EditID) == 32) {
                                            echo '<input type="text" name="Opt0" maxlength="250" class="form-control" id="Opt0" value="' . $_REQUEST["Opt0"] . '"/>';
                                        } else {
                                            echo '<input type="text" name="Opt0" maxlength="250" class="form-control" id="Opt0" value=""/>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label"><?php if ($KPICount > 1) {
                                                                        echo "Measure 1:";
                                                                    } ?></label>
                            <div class="col-sm-9 input-group-sm">
                                <?php
                                if ($KPICount > 1) {
                                    if ($EditID != "" && $EditID != '--') {
                                        echo '<input type="text" name="Opt1" maxlength="250" class="form-control" id="Opt1" value="' . ScriptRunner($Script_Edit, "Opt1") . '"/>';
                                    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                        echo '<input type="text" name="Opt1" maxlength="250" class="form-control" id="Opt1" value=""/>';
                                    } else {
                                        if (isset($_REQUEST["Opt1"]) && strlen($EditID) == 32) {
                                            echo '<input type="text" name="Opt1" maxlength="250" class="form-control" id="Opt1" value="' . $_REQUEST["Opt1"] . '"/>';
                                        } else {
                                            echo '<input type="text" name="Opt1" maxlength="250" class="form-control" id="Opt1" value=""/>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label"><?php if ($KPICount > 2) {
                                                                        echo "Measure 2:";
                                                                    } ?></label>
                            <div class="col-sm-9 input-group-sm">
                                <?php
                                if ($KPICount > 2) {
                                    if ($EditID != "" && $EditID != '--') {
                                        echo '<input type="text" name="Opt2" maxlength="250" class="form-control" id="Opt2" value="' . ScriptRunner($Script_Edit, "Opt2") . '"/>';
                                    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                        echo '<input type="text" name="Opt2" maxlength="250" class="form-control" id="Opt2" value=""/>';
                                    } else {
                                        if (isset($_REQUEST["Opt2"]) && strlen($EditID) == 32) {
                                            echo '<input type="text" name="Opt2" maxlength="250" class="form-control" id="Opt2" value="' . $_REQUEST["Opt2"] . '"/>';
                                        } else {
                                            echo '<input type="text" name="Opt2" maxlength="250" class="form-control" id="Opt2" value=""/>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label"><?php if ($KPICount > 3) {
                                                                        echo "Measure 3:";
                                                                    } ?></label>
                            <div class="col-sm-9 input-group-sm">
                                <?php
                                if ($KPICount > 3) {
                                    if ($EditID != "" && $EditID != '--') {
                                        echo '<input type="text" name="Opt3" maxlength="250" class="form-control" id="Opt3" value="' . ScriptRunner($Script_Edit, "Opt3") . '"/>';
                                    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                        echo '<input type="text" name="Opt3" maxlength="250" class="form-control" id="Opt3" value=""/>';
                                    } else {
                                        if (isset($_REQUEST["Opt3"]) && strlen($EditID) == 33) {
                                            echo '<input type="text" name="Opt3" maxlength="250" class="form-control" id="Opt3" value="' . $_REQUEST["Opt3"] . '"/>';
                                        } else {
                                            echo '<input type="text" name="Opt3" maxlength="250" class="form-control" id="Opt3" value=""/>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-3 col-form-label"><?php if ($KPICount > 4) {
                                                                        echo "Measure 4:";
                                                                    } ?></label>
                            <div class="col-sm-9 input-group-sm">
                                <?php
                                if ($KPICount > 4) {
                                    if ($EditID != "" && $EditID != '--') {
                                        echo '<input type="text" name="Opt4" maxlength="250" class="form-control" id="Opt4" value="' . ScriptRunner($Script_Edit, "Opt4") . '"/>';
                                    } elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Clear") {
                                        echo '<input type="text" name="Opt4" maxlength="250" class="form-control" id="Opt4" value=""/>';
                                    } else {
                                        if (isset($_REQUEST["Opt4"]) && strlen($EditID) == 33) {
                                            echo '<input type="text" name="Opt4" maxlength="250" class="form-control" id="Opt4" value="' . $_REQUEST["Opt4"] . '"/>';
                                        } else {
                                            echo '<input type="text" name="Opt4" maxlength="250" class="form-control" id="Opt4" value=""/>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pull-right">
                    <div class="col-md-12">
                        <?php
                        if (ValidateURths("TEAM APPRAISAL" . "A") == true || (ValidateURths("EMP APPRAISAL" . "M") == true)) {
                            if (($EditID == "" || $EditID == '--') && isset($GpNameHash) && strlen($GpNameHash) == 32) {
                                if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager" && (ValidateURths("TEAM APPRAISAL" . "A") == true)) {
                                    //$but_Caption="Add Deliverable"; $but_HasRth=("TEAM APPRAISAL"."A"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Add Deliverable" />';
                                } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal" && (ValidateURths("EMP APPRAISAL" . "M") == true)) {
                                    //$but_Caption="Add Deliverable"; $but_HasRth=("APPRAISAL"."T"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Add Deliverable" />';
                                } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth" && (ValidateURths("TEAM APPRAISAL" . "A") == true)) {
                                    //$but_Caption="Add Deliverable"; $but_HasRth=("CREATE APPRAISAL"."A"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Add Deliverable" />';
                                }
                            } elseif (($EditID != "" && strlen($EditID) == 32) && isset($GpNameHash) && strlen($GpNameHash) == 32) {
                                if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager" && (ValidateURths("TEAM APPRAISAL" . "A") == true)) {
                                    //$but_Caption="Update Deliverable"; $but_HasRth=("TEAM APPRAISAL"."A"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    if (ScriptRunner($Script_Edit, "Status") !== 'A') {
                                        echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Deliverable" />';
                                    }
                                } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal" && (ValidateURths("EMP APPRAISAL" . "M") == true)) {
                                    //$but_Caption="Update Deliverable"; $but_HasRth=("APPRAISAL"."T"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Deliverable" />';
                                } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth" && (ValidateURths("TEAM APPRAISAL" . "A") == true)) {
                                    //$but_Caption="Update Deliverable"; $but_HasRth=("CREATE APPRAISAL"."A"); $but_Type = 'CustomNoSelect'; include '../main/buttons.php';
                                    if (ScriptRunner($Script_Edit, "Status") !== 'A') {
                                        echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Deliverable" />';
                                    }
                                }
                                echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
                            } else {
                                echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Add Deliverable" />';
                            }
                        }
                        ?>
                    </div>
                </div>
                <br>
                <hr style="margin-bottom: 1.0rem">
                <div class="row">
                    <div class="col-md-12 ">
                        <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
                        <input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
                    </div>
                </div>
                <hr style="margin-top: 1.0rem;margin-bottom: .5rem">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                        if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {

                            print "<h4>UNAUTHORIZED</h4>";
                            $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from KPICore where Status <> 'A' and Status <> 'D' and AID='" . ECh($GpNameHash) . "' order by AddedDate asc");
                            $o = $dbOpen2 . $_REQUEST["PgTy"];
                        } else {

                            print "<h4>AUTHORIZED</h4>";
                            $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from KPICore where Status = 'A'  and AID='" . ECh($GpNameHash) . "' order by AddedDate asc");
                        }
                        ?>
                    </div>
                    <div class="col-md-12">
                        <table width="100%" id="" class="table table-hover table-striped table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th valign="middle">
                                        <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                        <label for="DelChk_All"></label>
                                    </th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Created On</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Updated On</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold"> Group</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Area of Focus</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Sub Area of Focus</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Departmental Objectives</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Individual Departmental Objectives</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Target Value</span></th>
                                    <th align="center" valign="middle"><span class="TinyTextTightBold">Weightage</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $Del = 0;
                                include '../login/dbOpen2.php';
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                    $Del = $Del + 1;
                                ?>
                                    <tr>
                                        <td>
                                            <?php
                                            echo '<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" />';
                                            echo '<label for="' . ("DelBox" . $Del) . '"></label>';
                                            ?>
                                        </td>
                                        <td>&nbsp;<?php echo (trim($row2['Dt'])); ?> </td>
                                        <td>&nbsp;<?php echo ($row2['UpdatedDate'] ? $row2['UpdatedDate']->format('j M Y') : "--"); ?> </td>
                                        <td>&nbsp;<?php echo (trim($row2['KRA_Group'])); ?></td>
                                        <td>&nbsp;<?php echo (trim($row2['KRA'])); ?></td>
                                        <td>&nbsp;<?php echo (trim($row2['KRA_Sub_Area'])); ?></td>
                                        <td>&nbsp;<?php echo (trim($row2['KPI'])); ?></td>
                                        <td>&nbsp;<?php echo (trim($row2['ARemark'])); ?></td>
                                        <td>&nbsp;<?php echo (number_format($row2['Quantity_Value'], 2)); ?></td>
                                        <td>&nbsp;<?php echo (trim($row2['Weightage']));
                                                    $TWeightage = $TWeightage + $row2['Weightage']; ?></td>
                                    </tr>
                                <?php }
                                include '../login/dbClose2.php'; ?>
                                <tr>
                                    <td height="27" colspan="9" align="right" valign="middle" class="subHeader" scope="col">Total</td>
                                    <td align="center" valign="middle" class="tdMenu_HeadBlock" scope="col">&nbsp;<?php echo number_format($TWeightage, 2); ?>%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center col-md-12">
                        <?php
                        echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
	      					      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
	      					  	  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {

                            if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                                //Check if user has Delete rights
                                if (ValidateURths("TEAM APPRAISAL" . "D") == 1) {
                                    $but_HasRth = ("TEAM APPRAISAL" . "D");
                                    $but_Type = 'Delete';
                                    include '../main/buttons.php';
                                }
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("TEAM APPRAISAL" . "A");
                                $but_Type = 'View';
                                include '../main/buttons.php';

                                if (ValidateURths("TEAM APPRAISAL" . "T") == 1) {
                                    //Check if user has Authorization rights
                                    $but_HasRth = ("TEAM APPRAISAL" . "T");
                                    $but_Type = 'Authorize';
                                    include '../main/buttons.php';
                                }
                            } else {
                                //Check if user has Delete rights
                                if (ValidateURths("TEAM APPRAISAL" . "D") == 1) {
                                    $but_HasRth = ("TEAM APPRAISAL" . "");
                                    $but_Type = 'Delete';
                                    include '../main/buttons.php';
                                }
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("TEAM APPRAISAL" . "A");
                                $but_Type = 'View';
                                include '../main/buttons.php';

                                if (ValidateURths("TEAM APPRAISAL" . "T") == 1) {
                                    //Check if user has Authorization rights
                                    $but_HasRth = ("TEAM APPRAISAL" . "T");
                                    $but_Type = 'Unauthorize';
                                    include '../main/buttons.php';
                                }
                            }
                        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Personal") {
                            if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                                //Check if user has Delete rights
                                if (ValidateURths("TEAM APPRAISAL" . "D") == 1) {
                                    $but_HasRth = ("TEAM APPRAISAL" . "D");
                                    $but_Type = 'Delete';
                                    include '../main/buttons.php';
                                }
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("EMP APPRAISAL" . "M");
                                $but_Type = 'View';
                                include '../main/buttons.php';
                            } else {
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("TEAM APPRAISAL" . "");
                                $but_Type = 'View';
                                include '../main/buttons.php';
                            }
                        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
                            if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
                                //Check if user has Delete rights
                                if (ValidateURths("TEAM APPRAISAL" . "D") == 1) {
                                    $but_HasRth = ("TEAM APPRAISAL" . "D");
                                    $but_Type = 'Delete';
                                    include '../main/buttons.php';
                                }
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("TEAM APPRAISAL" . "A");
                                $but_Type = 'View';
                                include '../main/buttons.php';

                                if (ValidateURths("TEAM APPRAISAL" . "T") == 1) {
                                    //Check if user has Authorization rights
                                    $but_HasRth = ("TEAM APPRAISAL" . "T");
                                    $but_Type = 'Authorize';
                                    include '../main/buttons.php';
                                }
                            } else {
                                //Check if user has Add/Edit rights
                                $but_HasRth = ("TEAM APPRAISAL" . "A");
                                $but_Type = 'View';
                                include '../main/buttons.php';

                                if (ValidateURths("TEAM APPRAISAL" . "T") == 1) {
                                    //Check if user has Authorization rights
                                    $but_HasRth = ("TEAM APPRAISAL" . "T");
                                    $but_Type = 'Unauthorize';
                                    include '../main/buttons.php';
                                }
                            }
                        }

                        //Check if any record was spolled at all before enabling buttons
                        //Check if user has Delete rights
                        echo "<input name='FAction' id='FAction' type='hidden'>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
<script>
    function selectScoreType() {
        const score_type = document.getElementById("Score_Type").value;
        let quantity_value = document.getElementById("quantity_value").value;
        const tity_value = document.getElementById("tity_value");
        if (score_type === "Quantity") {
            tity_value.style.display = "block";
        } else {
            document.getElementById("quantity_value").value = "";
            tity_value.style.display = "none";

        }

        // $('<div></div>').appendTo('body').html('<textarea name="mesg" class="form-control" rows="5" id="sd"></textarea>').dialog({

        //               modal: true,title: " Reason for Cancellation ", zIndex: 10000, autoOpen: true,
        //                 height: 200 + 6,
        //                 width: 500 + 20,
        //               buttons: {

        //                 Send: function () {

        //                   $(this).dialog("close");

        //                 }
        //               }

        //               });
    }
    selectScoreType();
</script>
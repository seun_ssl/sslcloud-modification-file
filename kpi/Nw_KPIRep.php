<?php
session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;include '../css/myscripts.php';

//Validate user viewing rights
/*
if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Manager")
{
if (ValidateURths("TEAM APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
}
elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Personal")
{
if (ValidateURths("APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}
}
elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Auth")
{
if (ValidateURths("CREATE APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
}
 */

if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
    if (ValidateURths("MEDIATION" . "A") != true) {include '../main/NoAccess.php';exit;} //Replicate Team Deliverables

} elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
    if (ValidateURths("MEDIATION" . "V") != true) {include '../main/NoAccess.php';exit;} // Replicate Deliverables
}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($KPICount)) {$KPICount = 0;}
if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($TWeightage)) {$TWeightage = 0;}
if (!isset($o)) {$o = 0;}

if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
    $OpenID = $_REQUEST["GrpName1"];
    $Script = "Select * from KPISetting where HashKey='" . ECh($_REQUEST["GrpName1"]) . "'";
    $KPICount = ScriptRunner($Script, "AFieldCnt");
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Replicate All Selected" || $_REQUEST["FAction"] == "Copy Selected Selected") {
        $GoValidate = true;
        if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Manager") {
            if (ValidateURths("TEAM APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{parent.msgbox('You do not have sufficient permission to add new deliverables.','red');}</script>");
                $GoValidate = false;
            }
        } elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "Auth") {
            if (ValidateURths("CREATE APPRAISAL" . "A") != true) {
                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have sufficient permission to add a new employee deliverables.', 'red'); }</script>");
                $GoValidate = false;
            }
        }

        if (strlen($_REQUEST["GrpName1"]) != 32 || strlen($_REQUEST["GrpName2"]) != 32) {
            echo ("<script type='text/javascript'>{ parent.msgbox('You must select a source and destination deliverable owners.', 'red'); }</script>");
            $GoValidate = false;
        }

        if ($GoValidate == true) {
            if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Replicate All Selected") {
                $Script = "Update KPICore set status='D' where AID='" . ECh($_REQUEST["GrpName2"]) . "'";
                ScriptRunnerUD($Script, "Inst");unset($Script);
            } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Copy Selected Selected") {
                for ($php_i = 0; $php_i <= $_REQUEST["DelMax"]; $php_i++) {
                    if (isset($_REQUEST["DelBox" . $php_i]) && strlen($_REQUEST["DelBox" . $php_i]) == 32) {
                        $EditID = ECh($_REQUEST["DelBox" . $php_i]);
                        $o = $o + 1;
                        if ($o == 1) {$SpHash = "'" . trim($EditID) . "'";} else { $SpHash = $SpHash . ",'" . trim($EditID) . "'";}
                    }
                }
                unset($o);
            }

            if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Copy Selected Selected") {
                $dbOpen2 = ("Select * from KPICore where AID='" . ECh($_REQUEST["GrpName1"]) . "' and Status='A' and HashKey in (" . $SpHash . ")");
            } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Replicate All Selected") {
                $dbOpen2 = ("Select * from KPICore where AID='" . ECh($_REQUEST["GrpName1"]) . "' and Status='A'");
            }

            /////include '../login/dbOpen2.php';
            /////while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
            /////{
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
            ////    $UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true).$row2['KPI']."ReplicateCopyKPI";
            ////    $HashKey = md5($UniqueKey);

            include '../login/dbOpen2.php';
            $Counter = 0;
            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . $row2['KPI'] . "ReplicateCopyKPI" . $Counter;
                $Counter++;
                $HashKey = md5($UniqueKey);

                $Script = "INSERT INTO [KPICore] ([AID],[KPI],[KRA],[KRA_Group],[KRA_Sub_Area],[Weightage],[ARemark],[Opt0],[Opt1],[Opt2],[Opt3],[Opt4],[Opt5],[Opt6],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . ECh($_REQUEST["GrpName2"]) . "','" . ECh($row2['KPI']) 
                 ."','" . ECh($row2['KRA'])
                 .  "','" . ECh($row2['KRA_Group']) 
                 ."','" . ECh($row2['KRA_Sub_Area'])
                  . "'," . ECh($row2['Weightage']) . ",'" 
                  . ECh($row2['ARemark']) . "','" . ECh($row2['Opt0']) . "','" . ECh($row2['Opt1']) . "','" . ECh($row2['Opt2']) . "','" . ECh($row2['Opt3']) . "','" . ECh($row2['Opt4']) . "','" . ECh($row2['Opt5']) . "','" . ECh($row2['Opt6']) . "','N','" . ECh($_SESSION["StkTck" . "UName"]) . "',GetDate(),'" . $HashKey . "')";
                // echo $Script;
                // var_dump($Script);
                ScriptRunnerUD($Script, "Inst");unset($Script);
            }
            include '../login/dbClose2.php';

            if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Copy Selected Selected") {
                AuditLog("INSERT", "Selected deliverable copied from [" . ECh($_REQUEST["GrpName1"]) . "] to [" . ECh($_REQUEST["GrpName2"]) . "]");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee deliverable copied successfully.', 'green'); }</script>");
            } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Replicate All Selected") {
                AuditLog("INSERT", "Employee deliverable replicated from [" . ECh($_REQUEST["GrpName1"]) . "] to [" . ECh($_REQUEST["GrpName2"]) . "]");
                echo ("<script type='text/javascript'>{ parent.msgbox('Employee deliverable replicated successfully.', 'green'); }</script>");
            }

            /* ==================================================================================================*/
            /* ==================================================================================================*/
            /* ==================================================================================================*/
            /*
            Hi #EmpName#,<br />There has been a change to your deliverables.<br /><br />You should check that this change matches your teams goals and overall deliverables and it covers all the duties you currently handle.<br /><br />Should you have any questions, contact your line manager <strong>#EmpManager#</strong> at <strong>#EmpManagerEmail#</strong>.<br><br>For more information, please login to your account and view your list of deliverable.
             */

            $Script_Emp = "Select (E.SName + ' ' + E.FName) as Nm, Email, (Select (Et.Email) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgrEmail, (Select (Et.SName + ' ' + Et.FName) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgr from EmpTbl E where HashKey=(Select AName from KPISetting where HashKey='" . ECh($_REQUEST["GrpName2"]) . "')";
            $Script = "Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41burg7e5bd83a4b17'"; //Employee KIP Modification

            $LoginDet = ScriptRunner($Script, "MMsg");
            $LoginDet = str_replace('#EmpName#', ScriptRunner($Script_Emp, "Nm"), $LoginDet);
            $LoginDet = str_replace('#EmpManager#', ScriptRunner($Script_Emp, "EmpMgr"), $LoginDet);
            //$LoginDet=str_replace('#Change#',"Deliverable: <strong>".ECh($_REQUEST["KPI"])."</strong> modified<br/>Result Area: <strong>".ECh($_REQUEST["KRA"])."</strong> modified.",$LoginDet);
            $LoginDet = str_replace('#EmpManagerEmail#', ScriptRunner($Script_Emp, "EmpMgrEmail"), $LoginDet);
            $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);

            $ToMgr = ScriptRunner($Script_Emp, "EmpMgrEmail");
            $ToEmp = ScriptRunner($Script_Emp, "Email");
            $Bdy = $LoginDet;
            $Subj = ScriptRunner($Script, "MSub");
            $Atth = "";

            //Send Mail to employee
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                QueueMail($ToMgr, $Subj, $Bdy, $Atth, '');
            }
            // **************************************************************
            //Send mail to manager
            if (filter_var($ToEmp, FILTER_VALIDATE_EMAIL)) {
                QueueMail($ToEmp, $Subj, $Bdy, $Atth, '');
            }
        }
    }
} // Closes the DoS IF
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

	<script>
	$(function() {$( "#tabs" ).tabs();});
	</script>

	<script>
	$(function() {
	    $( document ).tooltip();
	  });
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
</head>
<body>
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-6">
						<?php
if ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth") {?>
						<div class="row">
							<div class="col-2 col-md-1" style="margin-top:2%;">
								<a href="#" class="btn btn-default btn-sm"  alt="Find a Dashboard" name="FindanEmployee21" id="FindanEmployee21" onClick="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','hrm/srh_Emp.php?RetPg=kpi/Nw_KPIEmp.php&GroupName=Nationality&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')">
									<i class="fa fa-search"></i>
								</a>
							</div>
							<div class="input-group input-group-sm col-6 " style="margin-top: 2%">
								<select class="form-control" name="GrpName1" id="GrpName1">
						            <?php
echo '<option value="--" selected="selected">--</option>';
    if (isset($_REQUEST["GrpName1"])) {$OpenID = $_REQUEST["GrpName1"];} else { $OpenID = "";}
    //======================================================================================
    //===================== SELECTING EMPLOYEES ============================================
    //======================================================================================
    if ($_REQUEST["PgTy"] == "Manager") {
        if (ValidateURths("CREATE APPRAISAL" . "A") == true) {
            $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A','U','N')and AType='T' order by AName Asc");
        } else {
            $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('Z')and AType='T' order by AName Asc");
        }
    } elseif ($_REQUEST["PgTy"] == "Auth") {
        $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A','U','N')and AType='T' order by AName Asc");
    }
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        if ($OpenID == $row2['HashKey']) {
            if ($OpenID != '' && $OpenID != '--') {echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
        } else {echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
    }
    include '../login/dbClose2.php';
    //======================================================================================
    //===================== SELECTING EMPLOYEES ============================================
    //======================================================================================
    if ($_REQUEST["PgTy"] == "Manager") {
        $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.HashKey from KPISetting Ks, EmpTbl Et where Et.Status in ('A') and Ks.Status<>'D' and Ks.AName=Et.HashKey and EmpMgr=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "')) and Status in('A') order by Et.SName Asc");
    } elseif ($_REQUEST["PgTy"] == "Auth") {
        $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.* from KPISetting Ks, EmpTbl Et where Ks.Status<>'D' and Et.Status in ('A') and Ks.AName=Et.HashKey order by Et.SName Asc");
    }
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        if ($OpenID == $row2['HashKey']) {
            if ($OpenID != '' && $OpenID != '--') {echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
        } else {echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
    }
    include '../login/dbClose2.php';
    ?>
								</select>
							</div>
						</div>
						<?php }?>
					</div>
					<div class="col-md-6">
						<?php
if ($_REQUEST["PgTy"] == "Manager" || $_REQUEST["PgTy"] == "Auth") {?>
						<div class="row">
							<div class=" col-md-4"></div>
							<div class="col-2 col-md-1" style="margin-top:2%;">
								<a href="#" class="btn btn-default btn-sm" name="FindanEmployee" id="FindanEmployee" onClick="parent.ShowDisp('Add&nbsp;to&nbsp;Masters','hrm/srh_Emp.php?RetPg=kpi/Nw_KPIEmp.php&GroupName=Nationality&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')">
									<i class="fa fa-search"></i>
								</a>
							</div>
							<div class="input-group input-group-sm col-6" style="margin-top: 2%">
								<select class="form-control" name="GrpName2" id="GrpName2">
									<?php
echo '<option value="--" selected="selected">--</option>';
    if (isset($_REQUEST["GrpName2"])) {$OpenID = $_REQUEST["GrpName2"];} else { $OpenID = "";}

    //======================================================================================
    //===================== SELECTING EMPLOYEES ============================================
    //======================================================================================

    if ($_REQUEST["PgTy"] == "Manager") {
        if (ValidateURths("CREATE APPRAISAL" . "A") == true) {
            $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A','U','N')and AType='T' order by AName Asc");
        } else {
            $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('Z')and AType='T' order by AName Asc");
        }
    } elseif ($_REQUEST["PgTy"] == "Auth") {
        $dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A','U','N')and AType='T' order by AName Asc");
    }
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        if ($OpenID == $row2['HashKey']) {
            if ($OpenID != '' && $OpenID != '--') {echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
        } else {echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
    }
    include '../login/dbClose2.php';

    //======================================================================================
    //===================== SELECTING EMPLOYEES ============================================
    //======================================================================================
    if ($_REQUEST["PgTy"] == "Manager") {
        $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.HashKey from KPISetting Ks, EmpTbl Et where Et.Status in ('A') and Ks.Status<>'D' and Ks.AName=Et.HashKey and EmpMgr=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "')) and Status in('A') order by Et.SName Asc");
    } elseif ($_REQUEST["PgTy"] == "Auth") {
        $dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.* from KPISetting Ks, EmpTbl Et where Ks.Status<>'D' and Et.Status in ('A') and Ks.AName=Et.HashKey order by Et.SName Asc");
    }
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        if ($OpenID == $row2['HashKey']) {
            if ($OpenID != '' && $OpenID != '--') {echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
        } else {echo '<option value="' . $row2['HashKey'] . '">' . $row2['Nm'] . '</option>';}
    }
    include '../login/dbClose2.php';
    ?>
								</select>
								<span class="input-group-btn">
									<input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
								</span>
							</div>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="box bg-pale-secondary" style="margin-bottom: 0px; border-radius: 0px;">
							<div class="box-body" style="padding: 0.6rem">
								<a Title="Only authorized deliverables can be copied or replicated. Copy makes a copy of the selected deliverable to the destination account. Replicate makes an exact replica of the source account to the destination account" name="AuthInfo" id="AuthInfo" onClick="Popup.show('simplediv');return false;"  class="btn btn-default btn-xs"><i class="fa fa-info" ></i></a> Source
							</div>
						</div>
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th style="padding-bottom: 0px;">
										<!-- <div class="checkbox"> -->
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
										<label for="DelChk_All"></label>
										<!-- </div> -->
									</th>
									<th align="center" valign="middle">Area of Focus</th>
									<th align="center" valign="middle">Sub Area of Focus</th>
									<th align="center" valign="middle">Departmental Objectives</th>
									<th align="center" valign="middle">Individual Performances Objectives</th>
									<th align="center" valign="middle">Weightage</th>
								</tr>
							</thead>
							<tbody>
								<?php
$Del = 0;
$TWeightage = 0;

if (isset($_REQUEST["GrpName1"])) {$OpenID = ECh($_REQUEST["GrpName1"]);} else { $OpenID = "";}

$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from KPICore where Status = 'A' and AID='" . $OpenID . "' order by AddedDate asc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    ?>
								<tr>
									<td>
										<!-- <div class="checkbox"> -->
											<?php echo '<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" />';
    ?>
											<label for="<?php echo ("DelBox" . $Del); ?>"></label>
										<!-- </div> -->
									</td>
									<td>
										<?php
if (isset($row2['KRA_Group'])) {
        echo "<>" . (trim($row2['KRA_Group'])) . "<hr/>";
    }
    echo (trim($row2['KRA']));
    ?>
									</td>
									<td>
										<?php

    echo (trim($row2['KRA_Sub_Area']));
    ?>
									</td>
									<td>
										<?php echo (trim($row2['KPI'])); ?>
									</td>
									<td>
										<?php echo (trim($row2['ARemark'])); ?>
									</td>
									<td>
										<?php echo (trim($row2['Weightage']));
    $TWeightage = $TWeightage + $row2['Weightage']; ?>
									</td>
								</tr>
								<?php }
include '../login/dbClose2.php';?>
								<tr>
									<td colspan="5" align="right" valign="middle" class="" scope="col">Total</td>
									<td align="center" valign="middle" class="" scope="col" >&nbsp;<?php echo number_format($TWeightage, 2, '.', ','); ?>%</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<div class="box bg-pale-secondary" style="margin-bottom: 0px; border-radius: 0px;">
							<div class="box-body" style="padding: 0.6rem">
								<a href="#" class="btn btn-default btn-xs" Title="All deliverables for this account are visible here" name="AuthInfo21" id="AuthInfo21" onClick="Popup.show('simplediv');return false;">
									<i class="fa fa-info"></i>
								</a> Destination
							</div>
						</div>
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th align="center" valign="middle">Area of Focus</th>
                                    <th align="center" valign="middle">Sub Area of Focus</th>
									<th align="center" valign="middle">Departmental Objectives</th>
									<th align="center" valign="middle">Individual Performances Objectives</th>
									<th align="center" valign="middle">Weightage</th>
								</tr>
							</thead>
							<tbody>
								<?php
$TWeightage = 0;
if (isset($_REQUEST["GrpName2"])) {$OpenID = ECh($_REQUEST["GrpName2"]);} else { $OpenID = "";}

$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from KPICore where Status in ('A','N','U') and AID='" . $OpenID . "' order by AddedDate asc");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {?>
								<tr>
									<td>
										<?php
if (isset($row2['KRA_Group'])) {
    echo "<>" . (trim($row2['KRA_Group'])) . "<hr/>";
}
    echo (trim($row2['KRA']));?>
							      	</td>

                                    									<td>
										<?php

    echo (trim($row2['KRA_Sub_Area']));
    ?>
									</td>
									<td>
										&nbsp;<?php echo (trim($row2['KPI'])); ?>
									</td>
									<td>
										&nbsp;<?php echo (trim($row2['ARemark'])); ?>
									</td>
									<td>&nbsp;<?php echo (trim($row2['Weightage']));
    $TWeightage = $TWeightage + $row2['Weightage']; ?>
      								</td>
								</tr>
								<?php }
include '../login/dbClose2.php';?>
								<tr>
								  <td colspan="4" align="right" valign="middle" class="">Total</td>
								  <td align="center" valign="middle" class="" scope="col">&nbsp;<?php echo number_format($TWeightage, 2); ?>%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
				      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
				    <input name="FAction" id="FAction" type="hidden">
				      <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
if ($_REQUEST["PgTy"] == "Manager" && $Del > 0) {
    $but_Caption = "Copy Selected";
    $but_HasRth = ("TEAM APPRAISAL" . "A");
    $YesNo = true;
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
    $but_Caption = "Replicate All";
    $but_HasRth = ("TEAM APPRAISAL" . "A");
    $YesNo = true;
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
} elseif ($_REQUEST["PgTy"] == "Auth" && $Del > 0) {
    $but_Caption = "Copy Selected";
    $but_HasRth = ("CREATE APPRAISAL" . "A");
    $YesNo = true;
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
    $but_Caption = "Replicate All";
    $but_HasRth = ("CREATE APPRAISAL" . "A");
    $YesNo = true;
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
} else {
    $but_Caption = "Copy Selected";
    $but_HasRth = ("CREATE APPRAISAL **" . "A");
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
    $but_Caption = "Replicate All";
    $but_HasRth = ("CREATE APPRAISAL **" . "A");
    $but_Type = 'CustomNoSelect';include '../main/buttons.php';
}

?>
			</div>
			<!-- /.box-body -->
		</div>
	</form>

<?php 
	session_start( );
	include '../login/scriptrunner.php';
	$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';

	//Validate user viewing rights
	if (ValidateURths("START END APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
	if (!isset($EditID)){$EditID="";}
	if (!isset($Script_Edit)){$Script_Edit="";}
	if (!isset($HashKey)){$HashKey="";}
	if (!isset($SelID)){$SelID="";}
	$GoValidate=true;
	echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
	if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32)
	{unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);
		if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Unlock Last Appraisal")
		{
			$query4 = "SELECT DATEDIFF(D,Getdate(),EDate) GtD, HashKey from [KPIStart] where Status in ('A') order by SDate desc";
			$HashKeyMod = ScriptRunnercous($query4)['HashKey'];
			$Script = "Update [KPIStart] set [status]='M' where [HashKey]= '".$HashKeyMod."'";
			ScriptRunnerUD($Script,"Inst");
			echo ("<script type='text/javascript'>{ parent.msgbox('Recent Appraisal Unauthorized Successfully. You can now make modification and authorize', 'green'); }</script>");
		}
		if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Create Appraisal")
		{
			//if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}

		//	include 'add_user_validation.php';
			if (isset($_REQUEST["AID"]) && trim($_REQUEST["AID"])!="" )
			{
				$GoValidate = true;
				if (strlen(trim($_REQUEST["AID"]))<3)
				{
					$GoValidate = false;
					echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
				}

				$Script ="Select * from KPIStart where AID='".ECh($_REQUEST["AID"])."' and Status<>'D'";
				if (strlen(ScriptRunner($Script, "LogName"))>0 || strlen(ScriptRunner($Script, "FullName")>0))
				{
					echo ("<script type='text/javascript'>{parent.msgbox('An existing appraisal with a duplicate name already exist. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
			}

			if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
			else //Means the date was selected. Check if it falls within another appraisal period
			{
				$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["SDate"])."' between [SDate] and [EDate] and Status<>'D'";
				if (ScriptRunner($Script,"Ct")>0)
				{
					echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
			}

			if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
			else //Means the date was selected. Check if it falls within another appraisal period
			{
				$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["EDate"])."' between [SDate] and [EDate] and Status<>'D'";
				if (ScriptRunner($Script,"Ct")>0)
				{
					echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
			}
			if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
				$GoValidate = false;
			}

			if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
				$GoValidate = false;
			}
			
			if (isset($_REQUEST["MDate"]) && trim($_REQUEST["MDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a mediation end date for the selected appraisal period to end. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
			/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
			$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."TimeOff".$_SESSION["StkTck"."UName"];
			$HashKey = md5($UniqueKey);
		
			if ($GoValidate == true)
			{
				$B4Start=ECh($_REQUEST["B4Start"]);
				$B4End=ECh($_REQUEST["B4End"]);
				$EDate = ECh($_REQUEST["EDate"]);
				$SDate = ECh($_REQUEST["SDate"]);

				$Script = "INSERT INTO [KPIStart] ([AID],[SDate],[EDate],[MDate],[RevSDate],[RevEDate],[ScoreType],[ACount],[ARemark],[Status],[AddedBy],[AddedDate],[HashKey],[B4Start],[B4End]) VALUES ('".ECh($_REQUEST["AID"])."','".ECh($_REQUEST["SDate"])."','".ECh($_REQUEST["EDate"])."','".ECh($_REQUEST["MDate"])."','".ECh($_REQUEST["RevSDate"])."','".ECh($_REQUEST["RevEDate"])."','".ECh($_REQUEST["ScoreType"])."',".ECh($_REQUEST["ACount"]).",'".ECh($_REQUEST["ARemark"])."','N','".ECh($_SESSION["StkTck"."HKey"])."',GetDate(),'".$HashKey."','".ECh($_REQUEST["ScoreType"])."','".ECh($_REQUEST["ACount"])."')";
		//		echo $Script;
				ScriptRunnerUD($Script,"Inst");
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
				$b4_end_date = date('Y-m-d', strtotime($EDate." ".-$B4End." days"));
				$b4_start_date = date('Y-m-d', strtotime($SDate." ".-$B4Start." days"));
				
				$Script5 = "INSERT INTO [KPI_Send_Mail_Manager] ([Appraisal_Hashkey], [StartDate], [EndDate], [Day_B4_Start], [Day_B4_Start_Date_Conv],[Day_B4_End],[Day_B4_End_Date_Conv]) VALUES ('".$HashKey."','".ECh($SDate)."','".ECh($EDate)."','".ECh($B4Start)."',
		  '".ECh($b4_start_date)."','".ECh($B4End)."','".ECh($b4_end_date)."')";
		//		echo $Script;
				ScriptRunnerUD($Script5,"Inst");
				
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				//Appraisal Audit Trail		
				AuditLog("INSERT","New appraisal start/end [".ECh($_REQUEST["AID"])."] created ");
				if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
				{
					MailTrail("CREATE APPRAISAL","T",'','','',''); //Sends a mail notifications
				}

				echo ("<script type='text/javascript'>{ parent.msgbox('New organisational appraisal created successfully.', 'green'); }</script>");
			}
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
		}
		//**************************************************************************************************
		//**************************************************************************************************
		elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Appraisal" && isset($_REQUEST["UpdID"]) && $_REQUEST["UpdID"] !="")
		{
			if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
			
			/* Set HashKey of Account to be Updated */
			$EditID = ECh($_REQUEST["UpdID"]);
			$HashKey = ECh($_REQUEST["UpdID"]);
			
			/*Check validation for updating an account */
			if (strlen($EditID) ==32)
			{
				if (strlen(trim($_REQUEST["AID"]))<3)
				{
					echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
					$GoValidate = false;
				}
			
				if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"])=="")
				{
					echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
				else //Means the date was selected. Check if it falls within another appraisal period
				{
					$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["SDate"])."' between [SDate] and [EDate] and Status<>'D' and HashKey<>'".$EditID."'";
					if (ScriptRunner($Script,"Ct")>0)
					{
						echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
						$GoValidate = false;
					}
				}
		
				if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"])=="")
				{
					echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
				else //Means the date was selected. Check if it falls within another appraisal period
				{
					$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["EDate"])."' between [SDate] and [EDate] and Status<>'D' and HashKey<>'".$EditID."'";
					if (ScriptRunner($Script,"Ct")>0)
					{
						echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
						$GoValidate = false;
					}
				}
		
				if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"])=="")
				{
					echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
					$GoValidate = false;
				}
		
				if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"])=="")
				{
					echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
					$GoValidate = false;
				}
			
				if ($GoValidate == true)
				{
					
					$EDate = ECh($_REQUEST["EDate"]);
					$SDate = ECh($_REQUEST["SDate"]);
					$B4Start=ECh($_REQUEST["B4Start"]);
					$B4End=ECh($_REQUEST["B4End"]);
				
				$Script = "Update [KPIStart] set [AID]='".ECh($_REQUEST["AID"])."',[B4Start]='".ECh($_REQUEST["B4Start"])."',[B4End]='".ECh($_REQUEST["B4End"])."',[SDate]='".ECh($_REQUEST["SDate"])."',[EDate]='".ECh($_REQUEST["EDate"])."',[MDate]='".ECh($_REQUEST["MDate"])."',[RevSDate]='".ECh($_REQUEST["RevSDate"])."',[RevEDate]='".ECh($_REQUEST["RevEDate"])."',[ScoreType]='".ECh($_REQUEST["ScoreType"])."',[ACount]=".ECh($_REQUEST["ACount"]).",[ARemark]='".ECh($_REQUEST["ARemark"])."',[Status]='U',[UpdatedBy]='".$_SESSION["StkTck"."HKey"]."',[UpdatedDate]=GetDate() where [HashKey]= '".$HashKey."'";
				
					ScriptRunnerUD($Script,"Inst");
					
					//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					
					$b4_end_date = date('Y-m-d', strtotime($EDate." ".-$B4End." days"));
					$b4_start_date = date('Y-m-d', strtotime($SDate." ".-$B4Start." days"));
				
				//Check if there is a Valid Appriasal Open
					$Script3 = "select count(*) Ct from KPI_Send_Mail_Manager where Appraisal_Hashkey='".$HashKey."'";
					if (ScriptRunner($Script3, "Ct")==0){
						
						$Script5 = "INSERT INTO [KPI_Send_Mail_Manager] 
			([Appraisal_Hashkey], [StartDate], [EndDate],[Day_B4_Start],[Day_B4_Start_Date_Conv],[Day_B4_End],[Day_B4_End_Date_Conv], [SD_Status], [ED_Status], [Day_B4_Start_Status], [Day_B4_End_Status]) 
		  VALUES 
		  ('".$HashKey."','".$SDate."','".$EDate."','".$B4Start."','".$b4_start_date."','".$B4End."','".$b4_end_date."','','','','')";
		
					ScriptRunnerUD($Script5,"Inst");
						
					}else{		
							
				$Script2 = "Update [KPI_Send_Mail_Manager] set [StartDate]='".$SDate."',[EndDate]='".$EDate."',[Day_B4_Start]='".$B4Start."',[Day_B4_Start_Date_Conv]='".$b4_start_date."',[Day_B4_End]='".$B4End."',[Day_B4_End_Date_Conv]='".$b4_end_date."', [SD_Status]='', [ED_Status]='', [Day_B4_Start_Status]='', [Day_B4_End_Status]='' where [Appraisal_Hashkey]= '".$HashKey."'";
				
					ScriptRunnerUD($Script2,"Inst");
					
					}
					
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

					//Appraisal Audit Trail		
					AuditLog("UPDATE","Appraisal start/end updated for [".ECh($_REQUEST["AID"])."]");
					
					//if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
					//{
						//MailTrail("CREATE APPRAISAL","T",'','','',''); //Sends a mail notifications
					//}
					
					
				}
			}
			else
			{
				$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
				include '../main/prmt_blank.php';
			}
		}

		elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0)
		{
			/* Set EditID to -- and Script_Edit to request a NULL HashKey */
			$EditID = '--';
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
		
			$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
			for ($i=0; $i<=$ArrayCount-1; $i++)
			{//ActLnk
				if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
				{
					$EditID = ECh($HashVals[$i]);

					$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
				}
			}

			/* Prompt User to Select a record to edit - NO RECORD SELECTED */
			include '../main/prmt_blank.php';
		}
		
		elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0)
		{
			if (ValidateURths("CREATE APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}
			
			/* Set EditID to -- and Script_Edit to request a NULL HashKey */
			$EditID = '--';
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
		
			$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
			for ($i=0; $i<=$ArrayCount-1; $i++)
			{//ActLnk
				if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
				{
					$EditID = ECh($HashVals[$i]);
				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
					if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".$EditID."'") == true)
					{
						$Script="Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '".$EditID."'";
						if (ScriptRunner($Script,"Dt")>0)
						{
							//Mark the record as Authorized
							$Script = "UPDATE KPIStart
							SET [Status] = 'A'
							,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
							,[AuthDate] = GetDate()
							WHERE [HashKey] = '".$EditID."'";
							ScriptRunnerUD ($Script, "Authorize");
							
							
							$Script2 = "Update [KPI_Send_Mail_Manager] set [Status]='A' where [Appraisal_Hashkey]= '".$EditID."'";
							ScriptRunnerUD ($Script2, "Authorize");
							
							
							$Script99="SELECT ([SName] +' '+ [ONames]+ ' '+ [FName]) AprsNm FROM EmpTbl Et WHERE Et.HashKey='".ECh($_SESSION["StkTck"."HKey"])."'";
							$AprsNm=ScriptRunner($Script99,"AprsNm");
							AuditLog("AUTHORIZE","Appraisal Start/End Authorized by ".$AprsNm);
			
							echo ("<script type='text/javascript'>{ parent.msgbox('Selected appraisal records(s) authorized successfully.','green'); }</script>");
							
							
							//include 'mail_trail_send_kpi_aprs_setting.php';
		
							echo ("<script type='text/javascript'>{ parent.msgbox('Organisational appraisal updated successfully.', 'green'); }</script>");
							$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";
					
					
						}
						else
						{
							echo ("<script type='text/javascript'>{ parent.msgbox('Unable to authorized past appraisal.','red'); }</script>");
						}
					}
				}
			}

			/* Prompt User to Select a record to edit - NO RECORD SELECTED */
			include '../main/prmt_blank.php';
			$EditID='--';
	$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
		}
		
		elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0)
		{
			if (ValidateURths("CREATE APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}
			
			/* Set EditID to -- and Script_Edit to request a NULL HashKey */
			$EditID = '--';
			$Script_Edit ="select * from [KPIStart] where HashKey='--'";
		
			$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
			for ($i=0; $i<=$ArrayCount-1; $i++)
			{//ActLnk
				if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
				{
					$EditID = ECh($HashVals[$i]);
				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
	/*				if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".ECh($EditID)."'") != true)
					{echo "Here";
					exit;
	*/
						$Script="Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '".$EditID."'";
						if (ScriptRunner($Script,"Dt")>=0)
						{
							//Mark the record as Authorized
							$Script = "UPDATE KPIStart
							SET [Status] = 'U'
							,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
							,[AuthDate] = GetDate()
							WHERE [HashKey] = '".$EditID."'";
							ScriptRunnerUD ($Script, "Authorize");
							AuditLog("UNAUTHORIZE","Appraisal record unauthorized");



							$Script2 = "Update [KPI_Send_Mail_Manager] set [Status]='U' where [Appraisal_Hashkey]= '".$EditID."'";
							ScriptRunnerUD ($Script2, "Authorize");
							
							echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) unauthorized successfully', 'green');}</script>");
							$Script_Edit ="select CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";	
						}
	//				}
				}
			}
			
			/* Prompt User to Select a record to edit - NO RECORD SELECTED */
			include '../main/prmt_blank.php';
			$EditID='';
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='".$EditID."'";
		}
		
		elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0)
		{
			if (ValidateURths("CREATE APPRAISAL"."D")!=true){include '../main/NoAccess.php';exit;}
			
			/* Set EditID to -- and Script_Edit to request a NULL HashKey */
			$EditID = '--';
	$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
		
			$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
			for ($i=0; $i<=$ArrayCount-1; $i++)
			{//ActLnk
				if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
				{
					$EditID = ECh($HashVals[$i]);

				//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
					if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".$EditID."'") == true)
					{
					//Mark the record as Authorized
				
						$Script = "UPDATE KPIStart
						SET [Status] = 'D'
						,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '".$EditID."'";
						ScriptRunnerUD ($Script, "Authorize");
						AuditLog("DELETE","Appraisal record deleted");
		
						echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) deleted successfully', 'green');}</script>");
						$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='".$EditID."'";
					}
				}
			}
			/* Prompt User to Select a record to edit - NO RECORD SELECTED */
			include '../main/prmt_blank.php';
			$EditID='--';
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";		

		}
	}
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

	<script>
	$(function()
	  {
		$("#SDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
		$("#EDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
		$("#MDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
		$("#RevSDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-5Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
		$("#RevEDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-5Y", maxDate: "+1Y", dateFormat: 'dd M yy'})

	  });
	  

	$(function() {$( "#tabs" ).tabs();});
	$(function() {    $( document ).tooltip();  });
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	</script>
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">
	<link rel="stylesheet" href="../assets/css/responsive.css">
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
	<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
	<style>
		strong{
			font-weight: 550;
		}
	</style>
</head>
<body>
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-12 text-center">
						<h5>
						Create A New Appraisal
						</h5>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col md-6">
						<div class="form-group row">
							<label  class="col-sm-3 col-form-label">Appraisal Name <span style="color: red">*</span>:</label>
							<div class="col-sm-9 input-group-sm">
								<?php
								     if ($EditID != "" || $EditID > 0)
								        {
								      echo '<input name="AID" id="AID" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"AID").'" size="16" maxlength="60" /></span>';
								        }
								        else
								    {
								      if (isset($_REQUEST["AID"]))
								      {echo '<input name="AID" id="AID" type="text" class="form-control" maxlength="60" value="'.$_REQUEST["AID"].'" /></span>';} 
								      else
								      {echo '<input name="AID" id="AID" type="text" class="form-control" maxlength="60" value="" /></span>';} 
								    }
								?>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-3 col-form-label">Start Date<span style="color: red">*</span>:</label>
							<div class="col-sm-9 input-group input-group-sm">
								<?php 
									if ($EditID != "" || $EditID=="--")
								        {
								        echo '<input name="SDate" id="SDate" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"SDt").'" size="16" maxlength="12" readonly="readonly" />';
								        }
								        else
								    {
								      if (isset($_REQUEST["SDate"]))
								          {echo '<input name="SDate" id="SDate" type="text" class="form-control" value="" size="16" maxlength="12" value="'.$_REQUEST["SDate"].'" />';}
								    else
								      {echo '<input name="SDate" id="SDate" type="text" class="form-control" value="" size="16" maxlength="12" value="" />';}
								    }
								?>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default" Title="Date to start all employee appraisals" name="AuthInfo23" id="AuthInfo23" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row ">
							<label  class="col-sm-3 col-form-label">End Date<span style="color: red">*</span>:</label>
							<div class="col-sm-9 input-group input-group-sm">
								<?php 
									if ($EditID != "")
								        {
								        echo '<input name="EDate" id="EDate" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"EDt").'" maxlength="12" readonly="readonly" /></span>';
								        }
								        else
								        {
								      if (isset($_REQUEST["EDate"]))
								      {echo '<input name="EDate" id="EDate" type="text" class="form-control" value="" maxlength="12" value="'.$_REQUEST["EDate"].'" /></span>';}
								      else
								      {echo '<input name="EDate" id="EDate" type="text" class="form-control" value="" maxlength="12" value="" /></span>';}
								    }
								?>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default" Title="Date to end all employee appraisals" name="AuthInfo22" id="AuthInfo22" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row ">
							<label  class="col-sm-3 col-form-label">Mediation End Date<span style="color: red">*</span>:</label>
							<div class="col-sm-9 input-group input-group-sm">
								<?php 
									if ($EditID != "" || $EditID > 0)
								       {
								       echo '<input name="MDate" id="MDate" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"MDt").'" maxlength="12" readonly="readonly" /></span>';
								       }
								       else
								       {
								     if (isset($_REQUEST["MDate"]))
								     {echo '<input name="MDate" id="MDate" type="text" class="form-control" value="" maxlength="12" value="'.$_REQUEST["MDate"].'" /></span>';}
								           else
								         {echo '<input name="MDate" id="MDate" type="text" class="form-control" value="" maxlength="12" value="" /></span>';}
								   }
								?>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default" Title="Set last date for group head to mediate on appraisal" name="AuthInfo22" id="AuthInfo22" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row ">
							<label  class="col-sm-3 col-form-label">Grading Type:</label>
							<div class="col-sm-9 input-group input-group-sm">
								<select class="form-control" name="ScoreType" id="ScoreType">
									<?php
										$SelID="";
										if ($EditID != "" && $EditID!="--")
										{$SelID = ScriptRunner ($Script_Edit,"ScoreType");}
										  if ($SelID=="Tags")
										  {echo '<option selected value="Tags" selected>Tags</option> <option value="Values">Values</option>';}
										  elseif ($SelID=="Values")
										  {echo '<option selected value="Values" selected>Values</option> <option value="Tags">Tags</option>';}
										  else
										  {echo '<option selected value="--" selected>--</option> <option value="Tags">Tags</option> <option value="Values">Values</option>';}  
									?>
								</select>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default" Title="Values uses (1-100) while tagging uses more friendly tags to grade" name="AuthInfo24" id="AuthInfo24" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label  style="margin-bottom: 0px;">Select on which day to send out Notification<strong> before</strong> the <strong>Start Date</strong> of Appraisal to All Employees</label>
							<div class="row" >
								<div class="col-md-3" >
								</div>
								<div class="col-md-9  input-group-sm" >
									<select name="B4Start" class="form-control" id="B4Start" >
								        <option selected="selected" value="0">--</option>
								        <?php
							                $SelID=ScriptRunner("Select B4Start from [KPIStart] where [HashKey]='".$EditID."'","B4Start");
							                for ($php_i=1;$php_i<=12;$php_i++)
							                {
							                    if ($SelID == $php_i)
							                    {echo '<option selected=selected value="'.$php_i.'">'.$php_i.'</option>';}
							                    else
							                    {echo '<option value="'.$php_i.'">'.$php_i.'</option>';}
							                }
							            ?>
							      </select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label  style="margin-bottom: 0px;">Select on which day to send out Notification <strong>before</strong> the <strong>End Date</strong> of Appraisal to All Employees </label>
							<div class="row" >
								<div class="col-md-3" >
								</div>
								<div class="col-md-9  input-group-sm" >
									<select name="B4End" class="form-control" id="B4End" >
								        <option selected="selected" value="0">--</option>
								        <?php
							                $SelID=ScriptRunner("Select B4End from [KPIStart] where [HashKey]='".$EditID."'","B4End");
							                for ($php_i=1;$php_i<=12;$php_i++)
							                {
							                    if ($SelID == $php_i)
							                    {echo '<option selected=selected value="'.$php_i.'">'.$php_i.'</option>';}
							                    else
							                    {echo '<option value="'.$php_i.'">'.$php_i.'</option>';}
							                }
								        ?>
								    </select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="box bg-pale-secondary">
							<div class="box-body text-center" style="padding: 0.9rem" >
								<h5>Period Under Review</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group row ">
									<label  class="col-sm-3 col-form-label">From <span style="color: red">*</span>:</label>
									<div class="col-sm-9 input-group-sm">
										<?php 
											/* 
											echo '<input name="RevSDate" id="RevSDate" type="text" class="TextBoxText_Medium_Comp" size="16" maxlength="12" value="" /></span>';
											*/
											if ($EditID != "" || $EditID=="--")
											    {
											    echo '<input name="RevSDate" id="RevSDate" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"RSDt").'" maxlength="12" readonly="readonly" /></span>';
											    }
											    else
											    {
											  if (isset($_REQUEST["RevSDate"]))
											  {echo '<input name="RevSDate" id="RevSDate" type="text" class="form-control" maxlength="12" value="'.$_REQUEST["RevSDate"].'" /></span>';}
											  else
											  {echo '<input name="RevSDate" id="RevSDate" type="text" class="form-control" maxlength="12" value="" /></span>';}
											}
										?>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row ">
									<label  class="col-sm-3 col-form-label">To<span style="color: red">*</span>:</label>
									<div class="col-sm-9 input-group input-group-sm">
										<?php  
											if ($EditID != "" || $EditID=="--")
										        {
										        echo '<input name="RevEDate" id="RevEDate" type="text" class="form-control" value="'.ScriptRunner($Script_Edit,"REDt").'" maxlength="12" readonly="readonly" /></span>';
										        }
										        else
										        {
										      if (isset($_REQUEST["RevEDate"]))
										      {echo '<input name="RevEDate" id="RevEDate" type="text" class="form-control" maxlength="12" value="'.$_REQUEST["RevEDate"].'" /></span>';}
										      else
										      {echo '<input name="RevEDate" id="RevEDate" type="text" class="form-control" maxlength="12" value="" /></span>';}
										    }
										?>
										<span class="input-group-btn ">
											<button type="button" class="btn btn-default"  Title="Period under review for which employee is being appraised" name="AuthInfo25" id="AuthInfo25" onClick="Popup.show('simplediv');return false;"><i class="fa fa-info"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row ">
							<label  class="col-sm-3 col-form-label">Appraisal Note:</label>
							<div class="col-sm-9" >
								<?php 
									if ($EditID != "" || $EditID > 0)
								    {echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark">'.ScriptRunner($Script_Edit,"ARemark").'</textarea>';}
								    else
								    {
								      if (isset($_REQUEST["ARemark"]))
								      {echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark">'.$_REQUEST["ARemark"].'</textarea>';}
								      else
								      {echo '<textarea name="ARemark" rows="2" class="form-control" id="ARemark"></textarea>';}
								    }
								?>
							</div>
						</div>
						<div class="form-group row ">
							<label  class="col-sm-3 col-form-label">Rejection Iterations:</label>
							<div class="col-sm-9 input-group input-group-sm">
								<select name="ACount" class="form-control" id="ACount" >
							        <option selected="selected" value="0">--</option>
							        <?php
						                $SelID=ScriptRunner("Select ACount from [KPIStart] where [HashKey]='".$EditID."'","ACount");
						                for ($php_i=1;$php_i<=12;$php_i++)
						                {
						                    if ($SelID == $php_i)
						                    {echo '<option selected=selected value="'.$php_i.'">'.$php_i.'</option>';}
						                    else
						                    {echo '<option value="'.$php_i.'">'.$php_i.'</option>';}
						                }
							        ?>
							    </select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h5 style="font-size: 12px">
							<strong>
								<i>After authorizing a New Appraisal
								<a href="ApprSetup.php">
								Click here to 
								Set Number of Appraisal Levels
								</a>
								</i>
							</strong>
						</h5>
					</div>
					<div class="col-md-6 ">
						<?php
						if (ValidateURths("START END APPRAISAL"."A")==true){
						?>          
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-2">
								<div class="demo-checkbox">
									<input type="checkbox" checked name="AuthNotifier" id="AuthNotifier">
									<label for="AuthNotifier">
									<button type="button" class="btn btn-default btn-sm" Title="Check if you want a request for authorization sent" name="AuthInfo" id="AuthInfo" onClick="Popup.show('simplediv');return false;"  style="line-height: 17px" ><i class="fa fa-info"></i></button></label>
								</div>
							</div>
							<div class="col-md-4 pull-right">
								<?php
								    if ($EditID =="" || $EditID=='--')
								    {echo '<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger" id="SubmitTrans" value="Create Appraisal" />';}
								    elseif (strlen(trim($EditID))==32) {
								    	if (ScriptRunner($Script_Edit,"Status") !== 'A') {
									    	echo '<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger" id="SubmitTrans" value="Update Appraisal" />';
									    	echo '<input name="UpdID" type="hidden"  id="UpdID" value="'.$EditID.'" />';}
								    	}
								    else
								    {echo '<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger" id="SubmitTrans" value="Create Appraisal" />';}
								}
							?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php 
							$SelID=ScriptRunner("Select Status from [KPIStart] where [Status]='M' OR [Status]='U'", 'Status');
							$GDID=ScriptRunner("Select DATEDIFF(D,Getdate(),EDate) GtD, HashKey from [KPIStart] where Status in ('A') order by SDate desc", 'GtD');
						?>
						<?php if ($GDID < 0): ?>
							<input name="SubmitTrans" type="submit" class="btn btn-sm btn-danger" id="SubmitTrans" <?php echo ($SelID) ? 'disabled' : ''; ?> value="Unlock Last Appraisal" />
						<?php endif ?>
					</div>
				</div><br><hr  style="margin-bottom: 1.0rem">
				<div class="row">
				  	<div class="col-md-12">
			          	<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE"/>
			          	<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE"/>
				  	</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
							if (isset($_REQUEST['SubmitTrans'])&&($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")){
								print "<h4>UNAUTHORIZED</h4>";
									$dbOpen2 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt,* from [KPIStart] where Status in ('N','U', 'M') order by SDate desc");	
								}else{
									
									print "<h4>AUTHORIZED</h4>";
									$dbOpen2 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where (Status = 'A') order by SDate desc");
							}
						?>
					</div>
					<div class="col-md-12">
						<!-- <div class="ViewPatch"> -->
    						<table class="table table-bordered table-responsive" id="table">
    							<thead>
									<tr>
										<th data-sorter="false"  align="center" valign="middle">
							                <!-- <div class="checkbox"> -->
												<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
												<label for="DelChk_All"></label>                  
							      			<!-- </div> -->
										</th>
										<th>Created On</th>
										<th>Appraisal Name</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Grading</th>
										<th>Rejection</th>       
										<th>Status</th>
									</tr>
						        </thead>
						        <tbody>
							        <?php
										$Del = 0;
										include '../login/dbOpen2.php';
										while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
										{
											$Del = $Del + 1;
									?>
							        <tr>
							          	<td>
			          		                <!-- <div class="checkbox"> -->
		          		                	<?php 
			          		                	if ($row2['Status'] == 'A')
			          		                	{
			          		                	  if ($row2['GtD'] < 0)
			          		                	  {
			          		                	    $SelID=" disabled ";
			          		                	  }
			          		                	  else
			          		                	  {
			          		                	    $SelID="";
			          		                	  }
			          		                	}
		          		                	?>
			          		                	<input name="<?php echo ("DelBox".$Del); ?>" <?php echo $SelID;?> type="checkbox" id="<?php echo ("DelBox".$Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
			          							<label for="<?php echo ("DelBox".$Del); ?>"></label>                  
			          		      			<!-- </div> -->
									  	</td>
									  	<?php $Script="Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where EmpID=(Select EmpID from Users where LogName='".$row2['AuthBy']."' and Status in('A') )"; // Gets staff full details from the Emp Table?> 
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['AID'])); ?></td>
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['SDt'])); ?></td>
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['EDt'])); ?></td>
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['ScoreType'])); ?></td>
							          	<td align="center" valign="middle">&nbsp;<?php echo (trim($row2['ACount'])); ?></td>
							          	<td align="center" valign="middle" class="TinyText" scope="col">&nbsp;
							          		<?php 
									          	if ($row2['Status'] == 'A')
									          	{ 
									          	  if (strtotime($row2['TDay']." 00:00:00") > strtotime($row2['EDt']." 23:59:59")) //if ($row2['GtD'] > $row2['EDate'])
									          	  {
									          	    echo "Done";
									          	  }
									          	  elseif (strtotime($row2['TDay']." 00:00:00") >= strtotime($row2['SDt']." 00:00:00") && strtotime($row2['TDay']." 00:00:00") <= strtotime($row2['EDt']." 23:59:59"))
									          	  {
									          	    echo "In Progress";
									          	  }
									          	  else
									          	  {
									          	    echo "Pending";
									          	  }
									          	}
									          	else
									          	{
									          	  echo "Unauthorized";
									          	}
									        ?>
								        </td>
								    </tr>
							        <?php }
									// include '../login/dbClose2.php';
									?>
								</tbody>
						    </table>
						<!-- </div> -->
					</div>
					<div class="ml-auto" style="margin-top: 10px;">
						<?php
						echo '<input name="DelMax" id="DelMax" type="hidden" value="'.$Del.'" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
						      <input name="PgTy" id="PgTy" type="hidden" value="'.$_REQUEST["PgTy"].'" />
						      <input name="PgDoS" id="PgDoS" type="hidden" value="'.DoSFormToken().'">';
							//Check if any record was spolled at all before enabling buttons
						  if (isset($_REQUEST['SubmitTrans'])&&($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")){
						    //Check if user has Delete rights
						    if (ValidateURths("START END APPRAISAL"."D")==true){
						      $but_HasRth=("START END APPRAISAL"."D"); $but_Type = 'Delete'; include '../main/buttons.php';
						    }
						    //Check if user has Add/Edit rights
						    $but_HasRth=("CREATE APPRAISAL"."A"); $but_Type = 'View'; include '../main/buttons.php';
						    //Check if user has Authorization rights
						    if (ValidateURths("START END APPRAISAL"."T")==true){
						      $but_HasRth=("START END APPRAISAL"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
						    }
						  }else{
						    //Check if user has Delete rights
						    if (ValidateURths("START END APPRAISAL"."D")==true){
						      $but_HasRth=("START END APPRAISAL".""); $but_Type = 'Delete'; include '../main/buttons.php';
						    }
						    //Check if user has Add/Edit rights
						    $but_HasRth=("CREATE APPRAISAL"."A"); $but_Type = 'View'; include '../main/buttons.php';
						    //Check if user has Authorization rights
						    if (ValidateURths("START END APPRAISAL"."T")==true){
						      $but_HasRth=("START END APPRAISAL"."T"); $but_Type = 'Unauthorize'; include '../main/buttons.php';
						    }
						  }
						//Set FAction Hidden Value for each button
						  echo "<input name='FAction' id='FAction' type='hidden'>";?>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</form>
</body>
<?php
session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';

if (ValidateURths("APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

function addKPICoreRecord($getData)
{
    if(isset($getData[0]))
    {    
        // Get row data
        $empID = sanitize($getData[0]);
        $keyResultArea = sanitize($getData[1]);
        $group = sanitize($getData[2]);
        $deliverable = sanitize($getData[3]);
        $weightAge = sanitize($getData[4]);
        $measure = sanitize($getData[5]);
        $scoreType = sanitize($getData[6]);

        $query = "SELECT HashKey from EmpTbl WHERE EmpID = '".$empID."' AND Status = 'A'";
        $empHash = ScriptRunner($query, "HashKey");
        // $result = getData($query);

        $getKsHashKey = "SELECT HashKey FROM KPISetting WHERE AName = '".$empHash."' AND Status = 'A'";
        $kpiCoreHash = ScriptRunner($getKsHashKey, "HashKey");
        $kpiCoreHashResult = getData($getKsHashKey);

        if(is_array($kpiCoreHashResult) && count($kpiCoreHash) === 0 && count($empHash) === 1)
            {
                $UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true).$deliverable.$empHash;
                $ks_HashKey = md5($UniqueKey);
                
                $Script = "INSERT INTO [KPISetting] ([AName],[AType],[Status],[AddedBy],[AddedDate],[HashKey]) 
                VALUES ('".$empHash."','E','A','".$_SESSION["StkTck"."HKey"]."',
                GetDate(),'".$ks_HashKey."')";
                ScriptRunnerUD($Script,"Inst");
                AuditLog("INSERT","New employee appraisal template created for [".$empHash."]");
            } 
    }
}

function addKPIRecord($getData)
{
    if(isset($getData[0]))
    {    
        // Get row data
        $empID = sanitize($getData[0]);
        $keyResultArea = sanitize($getData[1]);
        $keyResultArea_sub = sanitize($getData[2]);
        $group = sanitize($getData[3]);
        $deliverable = sanitize($getData[4]);
        $weightAge = sanitize($getData[5]);
        $measure = sanitize($getData[6]);
        $scoreType = sanitize($getData[7]);

        $query = "SELECT HashKey from EmpTbl WHERE EmpID = '".$empID."' AND Status = 'A' and EmpStatus = 'Active'";
        $empHash = ScriptRunner($query, "HashKey");

        $getKsHashKey = "SELECT HashKey FROM KPISetting WHERE AName = '".$empHash."' AND Status = 'A'";
        $kpiCoreHash = ScriptRunner($getKsHashKey, "HashKey");
        $kpiCoreHashResult = getData($getKsHashKey);

        $kpiCoreQuery = "SELECT * FROM KPICore WHERE CONVERT(VARCHAR(MAX), KPI) = '".$deliverable."' AND AID = '".$kpiCoreHash."'";
        $kpiCoreResult = getData($kpiCoreQuery);
        
        if(count($empHash) === 1)
        {
                $UniqueHash= date('l jS \of F Y h:i:s A').gettimeofday(true).$deliverable;
                $kc_HashKey = md5($UniqueHash);

                if(!empty($kpiCoreResult)){
                    $kpiCoreScript = "UPDATE KPICore SET AID = '".$kpiCoreHash."', KPI = '".$deliverable."', 

                                     KRA = '".$keyResultArea."',
                                     KRA_Sub_Area = '".$keyResultArea_sub."',
                                     KRA_Group = '".$group."', 
                                     Weightage = '".$weightAge."', ARemark = '".$measure."', 
                                    Status = 'N', AddedBy = '".$_SESSION["StkTck" . "HKey"]."', AddedDate = GetDate(), HashKey = '".$kc_HashKey."' 
                                    WHERE CONVERT(VARCHAR(MAX), KPI) = '".$deliverable."' AND AID = '".$kpiCoreHash."'";     
                    ScriptRunner($kpiCoreScript, "kpiCoreScript");
                    return ['status'=>false,'msg' =>'Deliverable already exists'];
                }
               else{
                if(is_array($kpiCoreHashResult) && count($kpiCoreHashResult) === 1 && empty($kpiCoreResult))
                {
                    $Script_kc = "INSERT INTO [KPICore] ([AID],[KPI]
                    ,[KRA],
                    [KRA_Sub_Area],
                    [KRA_Group],
                    [Weightage],[ARemark],
                    [Status],[AddedBy],[AddedDate],[HashKey]) 
                    VALUES
                        ('" . $kpiCoreHash . "', '" . $deliverable . 
                        "', '" .  $keyResultArea . 
                        "', '" .  $keyResultArea_sub . 
                         "','" . $group . 
                        "', '" . $weightAge . 
                        "', '" . $measure . 
                        "','N', '" . $_SESSION["StkTck" . "HKey"] . "', GetDate(), '" . $kc_HashKey . "'
                    )";
                    ScriptRunnerUD($Script_kc, "Script");
                }
                }

            return ['status'=>true,'msg' =>'Successful']; 
        } else
        {
            return ['status'=>false,'msg' =>'Invalid Deliverable']; 
        }
        
    }
}


if (isset($_REQUEST["SubmitDoc"]) && $_REQUEST["SubmitDoc"] == "Upload")
{
    
    $success = 0;
    $error = 0;
    $errorRows = [];
    $msg = '';
    $file = $_FILES['kpiupd']['tmp_name'];
    if ($file)
    {
        $csv_mimetypes = ['text/csv', 'application/csv', 'text/comma-separated-values', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/octet-stream'];
        if (!in_array($_FILES['kpiupd']['type'], $csv_mimetypes))
        {
            echo ("<script type='text/javascript'>{ parent.msgbox('File format not accepted.', 'red'); }</script>");
        }elseif(($_FILES['kpiupd']['size'] >= 20000000))
        {
            echo ("<script type='text/javascript'>{ parent.msgbox('File too large. File must not be greater than 20MB.', 'red'); }</script>");
        } else
        {
            if (($handle = fopen($file, "r")) !== FALSE)
            {
                fgetcsv($handle);
                fgetcsv($handle);
                $count = 3;

                while(($getData = fgetcsv($handle, 1000, ",")) !== false)
                {
                    addKPICoreRecord($getData); 
                    $res =addKPIRecord($getData);

                    if($res['status'] === true)
                    {
                        $success++;
                    }else
                    {
                        $error++;
                        $errorRows[$count] = $res['msg'];
                    }

                    $count++;
                }

                fclose($handle);

                $_SESSION['errorRowsKPI'] = (count($errorRows)) ? $errorRows : false;
                echo ("<script type='text/javascript'>{parent.msgbox('Completed<br>{$success} Records Uploded <br> {$error} Records Failed', 'red'); }</script>");
            } else 
            {
                echo ("<script type='text/javascript'>{ parent.msgbox('Something went wrong. Please Try Again', 'red'); }</script>");
            }
        }
    } else 
    {
        echo ("<script type='text/javascript'>{ parent.msgbox('Select a file to upload', 'red'); }</script>");
    }
}



?>
<html>

<head>
    <title></title>
    <?php
		if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css">
    <?php } ?>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
</head>

<body oncontextmenu="return false;">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center mt-70">
                    <form name="Upd" method="post" action="#" enctype="multipart/form-data">
                        <p style="color:black; font-weight:bold;">UPLOAD DELIVERABLES</p>
                        <p> Select a document file to upload</p>
                        <div class="form-group row" style="margin-bottom: 5px">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="box bg-danger" style="margin-bottom: 0px">
                                    <div class="box-body text-center" style="padding: 0.7rem">
                                        <div class="col-sm-6 input-group-sm">
                                            <input type="file" name="kpiupd" id="kpiupd" accept=".csv" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-12 text-center mt-30 mb-20">
                                <input type="submit" name="SubmitDoc" id="SubmitDoc" value="Upload"
                                    class="btn btn-danger btn-sm">
                                <p style="margin-top: 1rem">Only CSV comma delimiter files allowed. Max file individual
                                    upload size is: 20Mb</p>
                                <a href="Sample Deliverable Upload Sheet format.csv">Download Template file</a>
                            </div>
                            <div class="col-md-6">
                                <?php if (isset($_SESSION['errorRowsKPI'])): ?>
                                <?php if ($_SESSION['errorRowsKPI']): ?>
                                <?php $errors = $_SESSION['errorRowsKPI'] ?>
                                <?php foreach ($errors as $key => $value): ?>
                                <p><strong style="font-weight: 700">Row Number <?php echo $key ?> not inserted
                                        (<?php echo $value ?>)</strong></p>
                                <?php endforeach ?>
                                <?php endif ?>
                                <?php unset($_SESSION['errorRowsKPI']); ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

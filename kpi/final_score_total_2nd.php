<?php
if(isset($EditID)){
	$dbOpen2 = ("Select * from KPIIndvScore where (Status <>'D' AND Status <>'S')
and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59')
 and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."') ORDER BY KRA_Group");
}elseif(isset($AQuartID_sel)){
	$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AQuartID='$AQuartID_sel'
 and AID='$KPISetting_hashkey' ORDER BY KRA_Group"); 
}else{
	$dbOpen2 = ("Select * from KPIIndvScore where AQuartID='".ECh($_REQUEST["AQuartID"])."' and Status <>'D' and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."') ORDER BY KRA_Group");
}
//echo $dbOpen2;
include '../login/dbOpen2.php';


$prevValue = "";
$main_total=0;
$w_main_total=0;

if($final_score_setup=="1"){
	$final_score_type="Last Appraiser";
}elseif($final_score_setup=="2"){
	$final_score_type="Supervisor/Direct Line Manager";
}elseif($final_score_setup=="3"){
	$final_score_type="Average Score of all Appraiser";
}

print "<table class=\"table table-bordered table-responsive\">
		<tr>
			<th>OVERALL RATING</th>
			<th>WEIGHTAGE</th>
			<th>FINAL SCORE($final_score_type)</th>
			<th>CALIBRATED SCORE</th>
		</tr>";

		$FinalHashKey = ScriptRunner($dbOpen2, "FinalHashKey");
		$query= "SELECT calibrated_score from KPIFinalScore where HashKey = '".$FinalHashKey."'";
		$total_calibrated_score = ScriptRunner($query, "calibrated_score");
		// var_dump($FinalHashKey);

while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
	$Del = $Del + 1;
	//$UpdUser = $row2['UpdatedBy'];
	$AuthRec = $row2['Status'];
	$AQuartID_Val = $row2['AQuartID'];
	$KRA_Group_value = $row2['KRA_Group'];
	//records=$row2['KRA_Group'];

	if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)&&($KRA_Group_value != "--")){
		//-----------------------------		
		
		if(isset($EditID)){
			$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".$KRA_Group_value."'
		 AND (Status <>'D' AND Status <>'S') and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59') and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");
		}elseif(isset($AQuartID_sel)){
			$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AQuartID='$AQuartID_sel'
 and AID='$KPISetting_hashkey' ORDER BY KRA_Group");  
		}else{
			$Script="Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".			$KRA_Group_value."' AND AQuartID='".ECh($_REQUEST["AQuartID"])."' and Status <>'D' and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."')";
		}
		
		
		$total_sum = ScriptRunner($Script,"TotMgrScore"); // not used anymore
		$total_weightage = ScriptRunner($Script,"TotWeightage");
		//----------------------------
		
		print "
		<tr>
			<td><strong>$KRA_Group_value</strong></td> 
			<td>$total_weightage</td> 
			<td>$total_sum</td>
			<td></td>
		</tr> 
		";
		$prevValue = $KRA_Group_value;
		$main_total=$main_total+$total_sum; 
		$w_main_total=$w_main_total+$total_weightage; // not used anymore
	}
}

		if($final_score_setup=="1"){
		
			//++++++++ START++++++++ Total in case of Last Person
			if(isset($EditID)){
				$sql = ("Select SUM(AScoreLevel3Comm) lv3, SUM(AScoreLevel4Comm) lv4, SUM(AScoreLevel5Comm) lv5, SUM(AScoreLevel6Comm) lv6 from KPIIndvScore where (Status <>'D' AND Status <>'S')
and AQuartID=(Select HashKey from KPIStart where Status='A' and GetDate() between convert(Varchar(12),SDate,106)+' 00:00:00' and convert(Varchar(12),EDate,106)+' 23:59:59')
 and AID=(Select HashKey from KPISetting where Status='A' and AName='".$EditID."')");
			}elseif(isset($AQuartID_sel)){
				$sql = ("SELECT SUM(AScoreLevel3Comm) lv3, SUM(AScoreLevel4Comm) lv4, SUM(AScoreLevel5Comm) lv5, SUM(AScoreLevel6Comm) lv6 FROM KPIIndvScore WHERE AQuartID = '$AQuartID_sel' and AID = '$KPISetting_hashkey' AND status <> 'D'");
			}else{
				$sql = ("Select SUM(AScoreLevel3Comm) lv3, SUM(AScoreLevel4Comm) lv4, SUM(AScoreLevel5Comm) lv5, SUM(AScoreLevel6Comm) lv6 from KPIIndvScore where AQuartID='".ECh($_REQUEST["AQuartID"])."' and (Status <>'D' AND Status <>'S')  and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."')");
			}
			
				$lv6 = ScriptRunner($sql,"lv6");
				$lv5 = ScriptRunner($sql,"lv5");
				$lv4 = ScriptRunner($sql,"lv4");
				$lv3 = ScriptRunner($sql,"lv3");
				
				if($lv6 > 0){
					$Total_Mgr_score = $lv6;
				}elseif($lv5  > 0){
					$Total_Mgr_score = $lv5;
				}elseif($lv4  > 0){
					$Total_Mgr_score = $lv4;
				}elseif($lv3  > 0){
					$Total_Mgr_score = $lv3;
				}	
	
			//++++++++END+++++
			$final_score_value = $Total_Mgr_score;
		}elseif($final_score_setup=="2"){
			//$final_score_type="Supervisor/Direct Line Manager";
			$final_score_value = $Total_Mgr_score;
		}
		
	print "
	   <tr>
            <td><strong>TOTAL</strong></td>
			<td><strong>$w_main_total</strong></td>
            <td><strong>$final_score_value</strong></td>
			<td><strong>".(float) $total_calibrated_score."</strong></td>
       </tr>
	";

include '../login/dbClose2.php';
print "</table>";
?> 
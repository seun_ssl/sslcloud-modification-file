<table class="table-responsive table">
  <tr>
    <td valign="top">Allow Appraisee to do final submission.</td>
    <td valign="top">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($allow_appraisee_sel=='1'){
    echo '<input name="allow_appraisee" type="checkbox" id="allow_appraisee" value="1" checked="checked" />';
    echo '<label for="allow_appraisee"></label>';
  }else{
    echo '<input name="allow_appraisee" type="checkbox" id="allow_appraisee" value="1" />';
    echo '<label for="allow_appraisee"></label>';
  }
  ?>    </td>
  </tr>
  <tr>
    <td valign="top">Allow Supervisor to do final submission.</td>
    <td valign="top">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($allow_supervisor_sel=='1'){
    echo '<input name="allow_supervisor" type="checkbox" id="allow_supervisor" value="1" checked="checked" />';
    echo '<label for="allow_supervisor"></label>';
  }else{
    echo '<input name="allow_supervisor" type="checkbox" id="allow_supervisor" value="1" />';
    echo '<label for="allow_supervisor"></label>';
  }
  ?>    </td>
  </tr>

  <tr>
    <td valign="top">Allow Secondary Manager/Supervisor score employee.</td>
    <td valign="top">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($allow_emp_mgr_1=='1'){
    echo '<input name="allow_emp_mgr_1" type="checkbox" id="allow_emp_mgr_1" value="1" checked="checked" />';
    echo '<label for="allow_emp_mgr_1"></label>';
  }else{
    echo '<input name="allow_emp_mgr_1" type="checkbox" id="allow_emp_mgr_1" value="1" />';
    echo '<label for="allow_emp_mgr_1"></label>';
  }
  ?>    </td>
  </tr>










  <tr>
    <td valign="top">Show Accomplishments Form.</td>
    <td valign="top">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($accomplishments=='1'){
    echo '<input name="accomplishments" type="checkbox" id="accomplishments" value="1" checked="checked" />';
    echo '<label for="accomplishments"></label>';
  }else{
    echo '<input name="accomplishments" type="checkbox" id="accomplishments" value="1" />';
    echo '<label for="accomplishments"></label>';
  }
  ?>    </td>
  </tr>
  <tr>
    <td valign="top">Show Objectives</td>
    <td valign="top">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($show_objectives=='1'){
    echo '<input name="show_objectives" type="checkbox" id="show_objectives" value="1" checked="checked" />';
    echo '<label for="show_objectives"></label>';
  }else{
    echo '<input name="show_objectives" type="checkbox" id="show_objectives" value="1" />';
    echo '<label for="show_objectives"></label>';
  }
  ?>    </td>
  </tr>
  <tr>
    <td width="988" valign="top" bgcolor="#FFFFFF"><strong>Send Mail</strong></td>
    <td width="204" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td width="988" valign="top" >Send out Notification on the Start Date of Appraisal to All Employees</td>
    <td width="204" valign="top"><input name="appr_comm4" type="checkbox" value="1" id="appr_comm4" checked="checked" disabled="disabled" /><label for="appr_comm4"></label></td>
  </tr>
  <tr>
    <td width="988" valign="top">Send out Notification on the End Date of Appraisal to All Employees</td>
    <td width="204" valign="top"><input name="appr_comm5" type="checkbox" value="1" id="appr_comm5" checked="checked" disabled="disabled" /><label for="appr_comm5"></label></td>
  </tr>


<!--  
  <tr>
    <td width="988" valign="top" bgcolor="#F5F5F5">Review Appraisal Notification Reminder</td>
    <td width="204" valign="top" bgcolor="#F5F5F5"><input name="appr_comm5" type="checkbox" value="1" id="appr_comm5" disabled="disabled" /></td>
  </tr>
  
  <tr>
    <td width="988" valign="top">End Appraisal Notification Reminder</td>
    <td width="204" valign="top"><input name="appr_comm5" type="checkbox" value="1" id="appr_comm5" disabled="disabled" /></td>
  </tr>
  
  <tr>
    <td width="988" valign="top" bgcolor="#F5F5F5">Final Submission Appraisal Notification Reminder</td>
    <td width="204" valign="top" bgcolor="#F5F5F5"><input name="appr_comm5" type="checkbox" value="1" id="appr_comm5" disabled="disabled" /></td>
  </tr>
-->
  
</table>
<br /><br />
<table class="table table-responsive table-striped">
  <tr>
    <td width="988" valign="top" bgcolor="#FFFFFF"><strong>Enforcement</strong></td>
    <td width="204" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#F5F5F5">Employee's Supervisor to Submit Back to the Employee before having the Option to do Final Submission.</td>
    <td valign="top" bgcolor="#F5F5F5">
    <?php
   //if ((strlen(trim($EditID))==32)&&($enforce_final_sel=='1')){
   if ($enforce_final_sel=='1'){
    echo '<input name="enforce_final" type="checkbox" id="enforce_final" value="1" checked="checked" />';
    echo '<label for="enforce_final"></label>';
  }else{
    echo '<input name="enforce_final" type="checkbox" id="enforce_final" value="1" />';
    echo '<label for="enforce_final"></label>';
	}
	?>    </td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF">Enforce the Employee to make a Comment on each Deliverable</td>
    <td valign="top" bgcolor="#FFFFFF"><?php
    /*if ((strlen(trim($EditID))==32)&&($enforce_comment_sel=='1')){
		echo '<input name="enforce_comm" type="checkbox" value="1" id="enforce_comm" checked="checked" />';
	}else{*/
	if ($enforce_comment_sel=='1'){
		echo '<input name="enforce_comm" type="checkbox" value="1" id="enforce_comm" checked="checked" />';
    echo '<label for="enforce_comm"></label>';
  }else{
    echo '<input name="enforce_comm" type="checkbox" value="1" id="enforce_comm" />';
    echo '<label for="enforce_comm"></label>';
  }
  ?>    </td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#F5F5F5">Enforce Line Managers to make a Comment on each Deliverable</td>
    <td valign="top" bgcolor="#F5F5F5"><?php   
  if ($enforce_comment_mgr=='1'){
    echo '<input name="enforce_comm_mgr" type="checkbox" value="1" id="enforce_comm_mgr" checked="checked" />';
    echo '<label for="enforce_comm_mgr"></label>';
  }else{
    echo '<input name="enforce_comm_mgr" type="checkbox" value="1" id="enforce_comm_mgr" />';
    echo '<label for="enforce_comm_mgr"></label>';
  }
  ?></td>
  </tr>
  <tr>
    <td width="988" valign="top" bgcolor="#FFFFFF">Enforce Supervisor Summary Comment</td>
    <td width="204" valign="top" bgcolor="#FFFFFF"><?php   
  if ($enforce_summ_comm_mgr=='1'){ 
    echo '<input name="enforce_summ_comm_mgr" type="checkbox" value="1" id="enforce_summ_comm_mgr" checked="checked" />';
    echo '<label for="enforce_summ_comm_mgr"></label>';
  }else{
    echo '<input name="enforce_summ_comm_mgr" type="checkbox" value="1" id="enforce_summ_comm_mgr" />';
    echo '<label for="enforce_summ_comm_mgr"></label>';
	}
	?></td>
  </tr>
</table>
<br /><br />
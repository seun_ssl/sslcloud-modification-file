<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
//Validate user viewing rights
if (ValidateURths("TRACK APPRAISAL" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}
$Script_hkey = "Select * from KPISettingNew where Status = 'A'";
$hkey = ScriptRunner($Script_hkey, "HashKey");
$Script_kpi = "SELECT  * FROM KPISettingNew WHERE HashKey='" . $hkey . "'";

$appr1_emp = ScriptRunner($Script_kpi, "Appr1_emp");
$appr1_deliv = ScriptRunner($Script_kpi, "Appr1_deliv");
$appr1_comment = ScriptRunner($Script_kpi, "Appr1_comment");
$appr1_section = ScriptRunner($Script_kpi, "Appr1_section_comment");

$appr2_emp = ScriptRunner($Script_kpi, "Appr2_emp");
$appr2_deliv = ScriptRunner($Script_kpi, "Appr2_deliv");
$appr2_comment = ScriptRunner($Script_kpi, "Appr2_comment");
$appr2_section = ScriptRunner($Script_kpi, "Appr2_section_comment");

$appr3_emp = ScriptRunner($Script_kpi, "Appr3_emp");
$appr3_deliv = ScriptRunner($Script_kpi, "Appr3_deliv");
$appr3_comment = ScriptRunner($Script_kpi, "Appr3_comment");
$appr3_section = ScriptRunner($Script_kpi, "Appr3_section_comment");

$appr4_emp = ScriptRunner($Script_kpi, "Appr4_emp");
$appr4_deliv = ScriptRunner($Script_kpi, "Appr4_deliv");
$appr4_comment = ScriptRunner($Script_kpi, "Appr4_comment");
$appr4_section = ScriptRunner($Script_kpi, "Appr4_section_comment");

$appr5_emp = ScriptRunner($Script_kpi, "Appr5_emp");
$appr5_deliv = ScriptRunner($Script_kpi, "Appr5_deliv");
$appr5_comment = ScriptRunner($Script_kpi, "Appr5_comment");
$appr5_section = ScriptRunner($Script_kpi, "Appr5_section_comment");

$appr6_emp = ScriptRunner($Script_kpi, "Appr6_emp");
$appr6_deliv = ScriptRunner($Script_kpi, "Appr6_deliv");
$appr6_comment = ScriptRunner($Script_kpi, "Appr6_comment");
$appr6_section = ScriptRunner($Script_kpi, "Appr6_section_comment");
$GDID = ScriptRunner("SELECT DATEDIFF(D,Getdate(),EDate) GtD, * from [KPIStart] where Status = 'A' order by SDate desc", 'HashKey');
if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
	if (isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32) {
		$GDID = $_REQUEST["AcctNo"];
	}
}
if (isset($_REQUEST['button']) && $_REQUEST['button'] == "Send to Manager") {
	$HashKey = $_REQUEST['HashKey'];
	$Script = "Update [KPIFinalScore] set [Status]='U' where HashKey='" . $HashKey . "' and Status = 'S' and EmpUpdCnt = '1' and MgrUpdCnt = '0'";
	ScriptRunnerUD($Script, "FS");
	$Script2 = "Update [KPIIndvScore] set [Status]='U' where FinalHashKey='" . $HashKey . "' and Status = 'S'";
	ScriptRunnerUD($Script2, "FS");
}
?>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
	echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
	<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>

<script>
	$(function() {
		$("#tabs").tabs();
	});
	$(function() {
		$(document).tooltip();
	});
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<body>
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-4 mb-4"></div>
				<div class="col-md-8 mb-4">
					<form action="#" method="post" name="Records" target="_self" autocomplete="off" id="open-data">
						<div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%;">
							<select name="AcctNo" class="form-control" id="AcctNo" required>
								<?php
								$dbOpen2 = ("SELECT * FROM KPIStart WHERE Status = 'A'");
								include '../login/dbOpen2.php';
								while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
									<option value="<?php echo $row2['HashKey'] ?>" <?php echo ($row2['HashKey'] === $GDID) ? 'selected' : '' ?>>
										<?php echo $row2['AID'] ?>
									</option>
								<?php
								endwhile;
								include '../login/dbClose2.php';
								?>
							</select>
							<span class="input-group-btn">
								<!-- Ensure the name and value of this hidden field is same as button -->
								<input type="hidden" value="Open" name="SubmitTrans" />
								<input type="submit" class="btn btn-danger" name="SubmitTrans" value="Open" />
							</span>
						</div>
					</form>
				</div>
				<div class="col-md-12">
					<?php
					$appr_name = "SELECT AID, Convert(varchar(25),SDate,106) AS start_date, Convert(varchar(25),EDate,106) AS end_date FROM [KPIStart] WHERE hashkey = '" . $GDID . "'";
					$start_date = ScriptRunner($appr_name, "start_date");
					$end_date = ScriptRunner($appr_name, "end_date");
					?>
					<h5>APPRAISAL: <?php echo strtoupper(ScriptRunner($appr_name, "AID")); ?></h5>
					<h5>START DATE: <?php echo $start_date; ?></h5>
					<h5>END DATE: <?php echo $end_date; ?></h5>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div>
						<div class="row">
							<div class="col-md-12">
								<?php include '../main/pagination.php'; ?>
								<table class="table table-responsive tablesorter" id="table">
									<thead>
										<tr>
											<th>Emp ID</th>
											<th>Employee</th>
											<th>Department</th>
											<th>Gender</th>
											<th>Started on</th>
											<th>Last Updated By</th>
											<th>Last Updated</th>
											<th>Supervisor Final Submission</th>
											<th>Supervisor Final Date</th>
											<th>Status</th>
											<th>Emp Count</th>
											<th>Mgr Count</th>
											<th>Final Score</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$Del = 0;
										$dbOpen2 = ("SELECT Convert(varchar(25),AddedDate,100) as Dt, Convert(varchar(25),SubmittedDate,100) as SubDt, Convert(varchar(25),UpdatedDate,100) as UpDt, * from [KPIFinalScore] where AQuartID = '$GDID' AND Status <> 'D'");

										// $dbOpen2 = ("Select * from KPIIndvScore where AQuartID='".$GDID."' and (Status <>'D')  and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."') ORDER BY KRA_Group");
										include '../login/dbOpen2.php';
										while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
											$Del = $Del + 1;
										?>
											<tr>
												<?php
												$Script2 = "SELECT * from  EmpTbl Et where HashKey='" . $row2['EID'] . "'";
												$empdetails = ScriptRunnercous($Script2);
												$name = $empdetails['SName'] . ' ' . $empdetails['FName'];
												?>
												<?php $nextLvLName = ScriptRunner("Select (SName+' '+FName) Nm from [EmpTbl] where HashKey='" . $row2['LUO_HashKey'] . "'", "Nm"); // Gets staff full details from the Emp Table
												?>
												<?php $MID = ScriptRunner("Select (SName+' '+FName) Nm from [EmpTbl] where HashKey='" . $row2['MID'] . "'", "Nm"); // Gets staff full details from the Emp Table
												?>
												<td>&nbsp;<?php echo (trim($empdetails['EmpID'])); ?></td>
												<td>&nbsp;<?php echo (trim($name)); ?></td>
												<td>&nbsp;<?php echo (trim($empdetails['Department'])); ?></td>
												<td>&nbsp;<?php echo (trim($empdetails['Sex'])); ?></td>
												<td>&nbsp;<?php echo ($row2['Dt']); ?></td>
												<td>&nbsp;<?php echo (ScriptRunner("SELECT (SName+' '+FName) Nm FROM [EmpTbl] WHERE HashKey='" . $row2['UpdatedBy'] . "'", "Nm")); ?></td>
												<td>&nbsp;<?php echo ($row2['UpDt']); ?></td>
												<td>&nbsp;<?php echo (ScriptRunner("SELECT (SName+' '+FName) Nm FROM [EmpTbl] WHERE HashKey='" . $row2['SubmittedBy'] . "'", "Nm")); ?></td>
												<td>&nbsp;
													<?php // if ($row2['LUO_HashKey'] == 'END'): 
													?>
													<?php echo ($row2['SubDt']); ?>
													<?php // else: 
													?>
													<!-- -- -->
													<?php // endif 
													?>
												</td>
												<td>
													<?php if ($row2['Status'] == 'N' || $row2['Status'] == 'S' || $row2['Status'] == 'E') : ?>
														Employee


													<?php elseif ($row2['Status'] == 'U' && $row2['LUO_HashKey'] !== 'END') : ?>
														<?php echo trim($MID) . ' - Supervisor'; ?>
													<?php elseif ($row2['Status'] == 'A' && $row2['LUO_HashKey'] !== 'END' && $row2['AprsID3'] == null && $row2['AprsID4'] == null && $row2['AprsID5'] == null && $row2['AprsID6'] == null && ($appr3_emp == '1' || $appr3_deliv == '1' || $appr3_comment == '1' || $appr3_section == '1')) : ?>
														<?php echo (trim($nextLvLName)); ?> - 3rd Appraiser
													<?php elseif ($row2['Status'] == 'A' && $row2['LUO_HashKey'] !== 'END' && $row2['AprsID3'] != null && $row2['AprsID4'] == null && $row2['AprsID5'] == null && $row2['AprsID6'] == null && ($appr4_emp == '1' || $appr4_deliv == '1' || $appr4_comment == '1' || $appr4_section == '1')) : ?>
														<?php echo (trim($nextLvLName)); ?> - 4th Appraiser
													<?php elseif ($row2['Status'] == 'A' && $row2['LUO_HashKey'] !== 'END' && $row2['AprsID4'] != null && $row2['AprsID5'] == null && $row2['AprsID6'] == null && ($appr5_emp == '1' || $appr5_deliv == '1' || $appr5_comment == '1' || $appr5_section == '1')) : ?>
														<?php echo (trim($nextLvLName)); ?> - 5th Appraiser
													<?php elseif ($row2['Status'] == 'A' && $row2['LUO_HashKey'] !== 'END' && $row2['AprsID5'] != null && $row2['AprsID6'] == null && ($appr6_emp == '1' || $appr6_deliv == '1' || $appr6_comment == '1' || $appr6_section == '1')) : ?>
														<?php echo (trim($nextLvLName)); ?> - 6th Appraiser
													<?php else : ?>
														Completed
													<?php endif ?>
													<?php if ($row2['Status'] == 'S' && $row2['EmpUpdCnt'] == 1 && $row2['MgrUpdCnt'] == 0) : ?>
														<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
															<input type="" hidden name="HashKey" value="<?php echo $row2['HashKey'] ?>">
															<input type="submit" name="button" class="btn btn-success btn-sm float-right" value="Send to Manager">
														</form>
													<?php endif ?>
												</td>
												<td>&nbsp;<?php echo (trim($row2['EmpUpdCnt'])); ?></td>
												<td>&nbsp;<?php echo (trim($row2['MgrUpdCnt'])); ?></td>
												<td>&nbsp;<?php echo $row2['FScore'] == 0 ? '--' : trim($row2['FScore']) ?></td>
											</tr>
										<?php }
										include '../login/dbClose2.php';
										?>
									</tbody>
								</table>
								<?php include '../main/pagination.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
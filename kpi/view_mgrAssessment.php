<?php
	session_start( );
	include '../login/scriptrunner.php';
	if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
	
	$Script28="SELECT * FROM KPISettingNew WHERE Status = 'A'";
	$head_hr = ScriptRunner($Script28,"head_hr");
	if (!isset($head_hr) || empty($head_hr) || $head_hr == '--') {
		//Check if head HR has been selected 
		$msg = 'Head of HR not defined';
	} else {
		$hrScript = "SELECT * FROM EmpTbl WHERE Hashkey='$head_hr'";
		$hr_dept = ScriptRunner($hrScript,"Department");
		$hr_mgr = ScriptRunner($hrScript,'EmpMgr');
		$loggedUser = $_SESSION["StkTck"."HKey"];
		if ($loggedUser !== $head_hr && $loggedUser !== $hr_mgr) {
			$msg = 'No data Available';
		}
	}



	if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32) {
		unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);
		if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
			if (isset($_POST["AcctNo"]) && !empty($_POST["AcctNo"])) {
				$EID = $_POST["AcctNo"];
			}
			if (isset($_POST["Appr"]) && !empty($_POST["Appr"])) {
				$Appr = $_POST["Appr"];
			}
			if (isset($EID) && isset($Appr)) {
				$Script="SELECT HashKey,(Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr, * from EmpTbl Et where Et.HashKey='$EID'";
				$EmpMgr = ScriptRunner("SELECT mid from kpi_mgr_assess where eid = '$EID' AND aid = '$Appr'", 'mid');
				$checkAssessment = ScriptRunner("SELECT data from kpi_mgr_assess where eid = '$EID' AND aid = '$Appr'", 'data');
				if ($checkAssessment) {
					$assessment = json_decode($checkAssessment, true);
				}
			}
		}
	}

?>
<?php if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	strong{
		font-weight: 550;
	}
	[type=radio].with-gap:checked+label:after, [type=radio]:checked+label:after {
	    background-color: #6dab69;
	}
	[type=radio].with-gap:checked+label:after, [type=radio].with-gap:checked+label:before, [type=radio]:checked+label:after {
	    border: 2px solid #6dab69;
	}
	hr {
	    margin: 1rem auto;
	}
</style>
<body oncontextmenu="return false;">
	<div class="box">
		<!-- Check if there is value in error $msg -->
		<?php if (!empty($msg)): ?>
			<div class="bg-lightest">
				<div class="box-body  text-center" style="padding: 0.6rem" >
					<p><?php echo $msg ?><p>
				</div>
			</div>
		<?php else: ?>
			<form action="#" method="post" name="Records" target="_self" id="Records" autocomplete="off">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-7">
							<div class="form-group row">
							    <label  class="col-sm-2 col-form-label"><strong>Appraisal</strong></label>
							   	<div class="col-sm-8 input-group input-group-sm">
									<select name="Appr" class="form-control" id="Appr">
										<?php
											echo '<option value="">--</option>';
											$dbOpen2 = ("SELECT * from KPIStart where Status='A' order by SDate DESC");
											include '../login/dbOpen2.php';
											while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
												{
													if ($Appr==$row2['HashKey'])
													{
													$appr_records = $appr_records.'<option selected value="'.$row2['HashKey'].'">'.strtoupper($row2['AID']).'</option>';
													}
													else
													{
													$appr_records = $appr_records.'<option value="'.$row2['HashKey'].'">'.strtoupper($row2['AID']).'</option>';
													}
												}
												echo $appr_records;
											include '../login/dbClose2.php';
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group row">
							    <label  class="col-sm-4 col-form-label"><strong>Select an Employee</strong></label>
							   	<div class="col-sm-8 input-group input-group-sm">
									<select name="AcctNo" class="form-control" id="AcctNo">
										<?php
											echo '<option value="" selected="selected">--</option>';
											if ($loggedUser === $head_hr) {
												$dbOpen2 = ("SELECT * from EmpTbl where (Status='A') and [EmpStatus]='Active' and DEPARTMENT <> '$hr_dept' order by SName Asc");
											} elseif ($loggedUser === $hr_mgr) {
												$dbOpen2 = ("SELECT * from EmpTbl where (Status='A') and [EmpStatus]='Active' order by SName Asc");
											}
											include '../login/dbOpen2.php';
											while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
												{
													if ($EID==$row2['HashKey'])
													{
													$AllEmployee = $AllEmployee.'<option selected value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
													}
													else
													{
													$AllEmployee = $AllEmployee.'<option value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
													}
												}
												echo $AllEmployee;
											include '../login/dbClose2.php';
										?>
									</select>
									<input name="PgDoS" id="PgDoS" type="hidden" value="<?php echo DoSFormToken() ?>">
									<span class="input-group-btn ">
										<input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<?php if (isset($EID) && isset($Appr)): ?>
				<div class="box-header with-border">
					<div class="row"  style="line-height: 16px; font-size: 13px;">
						<div class="col-md-6">
							<strong>
								<span style="padding-right: 5px;">
									Employee:
								</span>
								<span style="letter-spacing: 2px;">
									<?php
										echo ScriptRunner($Script,"Nm");
									?>
								</span>
							</strong>
						</div>
						<div class="col-md-6">
							<strong>
								<span style="padding-right: 5px;">
									Supervisor:
								</span>
								<span style="letter-spacing: 2px;">
							        <?php
							           echo ScriptRunner("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where HashKey='".$EmpMgr."'","Nm");
									?>
								</span>
							</strong>
						</div>
					</div>
				</div>
				<!-- ============================================================================================== -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<div class="row">
								<div class="col-md-8">
									<p class="text-left text-uppercase px-2 py-2" style="background: #f7fafc; font-weight: bold">
										Employee's 360 rating of line manager
									</p>
								</div>
								<div class="col-md-4 text-center">
									<p class="text-uppercase py-2" style="background: #f7fafc; font-weight: bold">
										Rating
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-1"></div>
				    </div>
				    <?php if (isset($assessment)): ?>
					    <div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<?php $i = 0; foreach ($assessment['ratings'] as $rating => $value): ?>
									<div class="row">
										<div class="col-md-8">
											<p><?php echo $rating; ?></p>
										</div>
										<div class="col-md-4 text-center">
											<span class="form-check-inline">
												<input type="radio" class="form-check-input" name="rating-<?php echo $i; ?>" id="yes-<?php echo $i; ?>" value="yes" <?php echo ($value === 'yes') ? 'checked' : '' ?> >
												<label for="yes-<?php echo $i; ?>" class="form-check-label">Yes</label>
											</span>
											<span class="form-check-inline">
												<input type="radio" class="form-check-input" name="rating-<?php echo $i; ?>" id="no-<?php echo $i; ?>" value="no" <?php echo ($value === 'no') ? 'checked' : '' ?> >
												<label for="no-<?php echo $i; ?>" class="form-check-label">No</label>
											</span>
										</div>
									</div>
									<hr>
								<?php $i++; endforeach ?>
		    				</div>
							<div class="col-md-1"></div>
					    </div>
			        	<?php $i = 0; foreach ($assessment['comments'] as $comment => $value): ?>
					        <div class="row">
					        	<div class="col-md-1"></div>
						        	<div class="col-md-10">
						    		    <div class="box bg-pale-secondary">
						    				<div class="box-body">
						    					<p class="box-title text-left text-uppercase b-0 px-0" style="font-weight: bold">
						    						<?php echo $comment; ?>
						    					</p>
												<input type="hidden" name="comment[]" value="<?php echo $comment; ?>">
						    					<p>
						    						<textarea name="comment-<?php echo $i; ?>" readonly rows="2" class="form-control"><?php echo trim($value) ?></textarea>
						    					</p>
						    				</div>
						    			</div>
						        	</div>
					        	<div class="col-md-1"></div>
					        </div>
		        		<?php $i++; endforeach ?>
				    <?php endif ?>
				</div>
			<?php endif ?>
		<?php endif ?>
	</div>
</body>
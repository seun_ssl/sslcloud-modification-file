<?php session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("APPROVE TEAM LEAVE" . "V") != true) {include '../main/NoAccess.php';exit;}

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($SelID)) {$SelID = "";}
if (!isset($ToBackUp)) {$ToBackUp = "";}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("APPROVE TEAM LEAVE" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script = "Select Status from [LvDetails] WHERE [HashKey] ='" . $EditID . "'";
                $SelID = ScriptRunner($Script, "Status");

                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "PA" || $SelID == "N" || $SelID == "U") {$SelID = "A";} elseif ($SelID == "PC") {$SelID = "C";}

                //Move on to Cancel leave request or seek for leave cancellation approval
                $Script = "UPDATE [LvDetails]
				SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = '" . $SelID . "'
				WHERE [HashKey]='" . $EditID . "'";
                ScriptRunnerUD($Script, "CA");
                //echo $Script;

                $Script = "UPDATE [LvIndTOff]
				SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = '" . $SelID . "'
				WHERE [LID]='" . $EditID . "'
				and Status not in ('C','A')";
                ScriptRunnerUD($Script, "CA");

                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "A") {
                    AuditLog("AUTHORIZE", "Employee leave sucessfully authorized by [" . $_SESSION["StkTck" . "UName"] . "].");
                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) authorized successfully.', 'green'); }</script>");
                } elseif ($SelID == "C") {
                    AuditLog("AUTHORIZE", "Employee leave sucessfully cancelled by [" . $_SESSION["StkTck" . "UName"] . "].");
                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) rejected successfully.', 'green'); }</script>");
                }
            }
        }

        //////////////////////////////// APPROVED NOTIFICATIONS ==============================////////////////////////////////

        include "mail_trail_send.php";
        include "mail_leave_group.php";

        $kk = ScriptRunner($Script_Det, "ItemDocHID");
        if (strlen($kk) == 32) //Check if there is an attached file to the leave specified.
        {
            $Script_Doc = "Select * from DocTrack where HashKey='" . $kk . "'"; // Get details for the leave application
            $Recipients = $ToMgr . "," . $ToBackUp;

            include '../dms/stf_upload_file.php'; //Include ShareFile function
            //ShareFile($Recipients,ScriptRunner($Script_Doc,"DocTitle"),$kk,"7"); //Share file attached to leave with Manager and Backup. Default leave share is 30 days.
        }
    }

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Reject Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("APPROVE TEAM LEAVE" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script = "Select Status, EmpID, LID from [LvDetails] WHERE [HashKey]='" . $EditID . "'";
                $SelID = ScriptRunner($Script, "Status");
                $SelEmpID = ScriptRunner($Script, "EmpID");
                $leave_id = ScriptRunner($Script, "LID");

                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "PA" || $SelID == "N" || $SelID == "U") {$SelID = "R";} elseif ($SelID == "PC") {$SelID = "R";}

                //Move on to Cancel leave request or seek for leave cancellation approval

                $Script = "select COUNT(*) Ct ,LID from LvIndTOff where LID='" . $EditID . "' and [EmpID]='" . $SelEmpID . "' and Status in('PA') group by LID";
                // var_dump($Script);
                // die();
                $coount = ScriptRunner($Script, "Ct");

                if ($coount > 0) {

                    $Script1 = "select * from LvDetails where HashKey='" . $EditID . "'";
                    $RDays = ScriptRunner($Script1, "RDays");
                    $reverse_count = (int) $coount + (int) $RDays;
                    $Script = "UPDATE [LvDetails]
					SET [ApprovedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					,[ApprovedDate] = GetDate()
					,[Status] = '" . $SelID . "'
					,RDays='" . $reverse_count . "'
					WHERE [HashKey]='" . $EditID . "'";
                    ScriptRunner($Script, "R");
                    //echo $Script;

                    $Script = "UPDATE [LvIndTOff]
				SET [ApprovedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[ApprovedDate] = GetDate()
				,[Status] = '" . $SelID . "'
				WHERE [LID]='" . $EditID . "'
				and Status not in ('C','A')";
                    ScriptRunner($Script, "R");

                    //If PendingAuthorization (PA) cancel leave at once without seeking approval
                    AuditLog("AUTHORIZE", "Employee timeoff rejected for [" . $_SESSION["StkTck" . "UName"] . "].");
                    //Get employee manager to send cancellation email to for approval
                    $Script = "Select [Email] from EmpTbl where HashKey=(select EmpID from [LvIndTOff] where HashKey='" . $EditID . "')";
                    //MailTrail("TIMEOFF REJECTED","EMP",'','',ScriptRunner($Script,"Email"),''); //Sends a mail notifications
                    //MailTrail("TIMEOFF REJECTED","MGR",'','',$_SESSION["StkTck"."Email"],''); //Sends a mail notifications

                    include "mail_trail_send.php";

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) rejected successfully.', 'green'); }</script>");

                }
            }
        }
    }
}?>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script>
  $(function() {
    $( document ).tooltip();
  });
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">


<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-4">
						<h6>
						[<a href="Auth_LvReq.php">Clear Page</a>]
						</h6>
					</div>
					<div class="col-md-4 text-center">
						<h4>
						Employee (Team) TimeOff Authorization
						</h4>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<?php $Load_Date = true;
$Load_Search = false;
$Load_Auth = false;include '../main/report_opt.php';?>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="AUTHORIZE"/>&nbsp;&nbsp;
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="UNAUTHORIZE"/>&nbsp;&nbsp;
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="REJECTED"/>&nbsp;&nbsp;
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-sm btn-danger" value="CANCELLED"/>&nbsp;&nbsp;
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {

    $date1 = date_create($_REQUEST["S_RptDate"]);
    $date1 = date_format($date1, 'Y-m-d');
    //print "<br />";
    $date2 = date_create($_REQUEST["E_RptDate"]);
    $date2 = date_format($date2, 'Y-m-d');
    $PayMthDt = " and (SDate >= '$date1' AND EDate <= '$date2') ";
} else {
    //$PayMthDt = '';
    $Script_Date = "Select Convert(Varchar(11),SetStart,106) LDate_Start, Convert(Varchar(11),SetEnd,106) LDate_End from [Settings] WHERE [Setting]= 'LeaveTyp'";
    $LDate_Start = ScriptRunner($Script_Date, "LDate_Start");
    $LDate_End = ScriptRunner($Script_Date, "LDate_End");
    $date1 = date_create($LDate_Start);
    $date1 = date_format($date1, 'Y-m-d');
    $date2 = date_create($LDate_End);
    $date2 = date_format($date2, 'Y-m-d');
    $PayMthDt = " and (SDate >= '$date1' AND EDate <= '$date2') ";
    //$PayMthDt = '';
}
//------------------------
if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "AUTHORIZE")) {
    print "<h4>AUTHORIZED</h4>";
    $dbOpen2 = ("SELECT LvD.ID, Convert(Varchar(11),LvD.AddedDate,106) as Dt, Convert(Varchar(11),LvD.SDate,106) as SDt, Convert(Varchar(11),LvD.EDate,106) as EDt,  (select LType from LvSettings where HashKey=LvD.LID) LType, LvD.*, ([SName] +' '+ [FName]+ ' ['+Et.EmpID+']') Nm from LvDetails LvD, EmpTbl Et where LvD.EmpID=Et.HashKey and LvD.Status in ('A') and LvD.EmpID in (select HashKey from EmpTbl where EmpMgr=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') ))) " . $PayMthDt . " order by LvD.SDate desc, LvD.AuthDate desc");
} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    print "<h4>UNAUTHORIZED</h4>";
    $dbOpen2 = ("SELECT LvD.ID, Convert(Varchar(11),LvD.AddedDate,106) as Dt, Convert(Varchar(11),LvD.SDate,106) as SDt, Convert(Varchar(11),LvD.EDate,106) as EDt, (select LType from LvSettings where HashKey=LvD.LID) LType, LvD.*, ([SName] +' '+ [FName]+ ' ['+Et.EmpID+']') Nm from LvDetails LvD, EmpTbl Et where LvD.EmpID=Et.HashKey and LvD.Status in ('PA','PC') and LvD.EmpID in
								(select HashKey from EmpTbl where EmpMgr=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "'))) " . $PayMthDt . " order by LvD.SDate desc, LvD.AuthDate desc");

} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "REJECTED")) {

    print "<h4>REJECTED</h4>";

    $dbOpen2 = ("SELECT LvD.ID, Convert(Varchar(11),LvD.AddedDate,106) as Dt, Convert(Varchar(11),LvD.SDate,106) as SDt, Convert(Varchar(11),LvD.EDate,106) as EDt, (select LType from LvSettings where HashKey=LvD.LID) LType, LvD.*, ([SName] +' '+ [FName]+ ' ['+Et.EmpID+']') Nm from LvDetails LvD, EmpTbl Et where LvD.EmpID=Et.HashKey and LvD.Status in ('R') and LvD.EmpID in
								(select HashKey from EmpTbl where EmpMgr=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "'))) " . $PayMthDt . " order by LvD.SDate desc, LvD.AuthDate desc");

} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "CANCELLED")) {

    print "<h4>CANCELLED</h4>";

    $dbOpen2 = ("SELECT LvD.ID, Convert(Varchar(11),LvD.AddedDate,106) as Dt, Convert(Varchar(11),LvD.SDate,106) as SDt, Convert(Varchar(11),LvD.EDate,106) as EDt, (select LType from LvSettings where HashKey=LvD.LID) LType, LvD.*, ([SName] +' '+ [FName]+ ' ['+Et.EmpID+']') Nm from LvDetails LvD, EmpTbl Et where LvD.EmpID=Et.HashKey and LvD.Status in ('C') and LvD.EmpID in
								(select HashKey from EmpTbl where EmpMgr=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "'))) " . $PayMthDt . " order by LvD.SDate desc, LvD.AuthDate desc");

}
//------------------------
?>
					</div>
					<div class="col-md-12">
						<table width="100%" align="left" cellpadding="1" cellspacing="1" id="table">
							<thead>
								<tr>
									<th valign="middle" class="smallText" scope="col" data-sorter="false"><span class="TinyTextTight">
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
										<label for="DelChk_All"></label>
									</span></th>
									<th align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Employee</span></th>
									<th  align="center" valign="middle"  scope="col"><span class="TinyTextTightBold">Timeoff Type</span></th>
									<th  align="center" valign="middle"  scope="col"><span class="TinyTextTightBold">Start Date</span></th>
									<th  align="center" valign="middle"  scope="col"><span class="TinyTextTightBold">End Date</span></th>
									<th align="center" valign="middle" scope="col">  Days</th>
									<th  align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Backup Employee</span></th>
									<th align="center" valign="middle" scope="col" data-sorter="false"><span class="TinyTextTightBold">Status</span></th>
									<th align="center" valign="middle" scope="col" data-sorter="false">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php
$Del = 0;
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    if ($Del == 1) {$PrevID = $row2['ID'];} else { $NextID = $row2['ID'];}
    if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized") {
    } elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized") {
        //=======THIS ENSURES THAT ANY LEAVE REQUEST THAT THE CHILD DAYS HAVE ALL BEEN CANCELLED DOES NOT SHOW UP FOR AUTHORIZATION
        $Script = "Select Count(*) Ct from LvIndTOff where LID='" . $row2['HashKey'] . "' and Status not in ('C','A','D')";
        if (ScriptRunner($Script, "Ct") == 0) {
            $Script = "Update LvDetails set Status='C' where HashKey='" . $row2['HashKey'] . "'";
            ScriptRunnerUD($Script, "Cancel");
        }
    }
    ?>
								<tr>
									<td width="20" height="27" valign="middle" scope="col" class="TinyTextTight">
										<?php
$Script = "Select * from Settings where Setting='LeaveTyp'";
    $kk = ScriptRunner($Script, "SetValue2");

    $Dsbl = "";
    if (strtolower($kk) == 'on') {
        if ($row2['Verified'] == 0) {
            $Dsbl = " disabled ";
        }
    }

    if ($Dsbl == " disabled ") {echo '<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" ' . $Dsbl . ' />
													<label for="' . ("DelBox" . $Del) . '"></label>';} else {echo '<input name="' . ("DelBox" . $Del) . '" type="checkbox" id="' . ("DelBox" . $Del) . '" value="' . ($row2['HashKey']) . '" ' . $Dsbl . ' />
													<label for="' . ("DelBox" . $Del) . '"></label>';}
    ?>

									</td>
									<td align="center" valign="middle" class="TinyText" scope="col" >&nbsp;
										<?php
echo (trim($row2['Nm'])); ?></td>
									<td align="center" valign="middle" class="TinyText" ><?php echo (trim($row2['LType'])); ?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" ><?php echo $row2['SDt']; ?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" ><?php echo $row2['EDt']; ?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" ><?php
$Script = "select Count(*) Ct from [LvIndTOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C')";
    echo ScriptRunner($Script, "Ct");?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" ><?php
$Script = "select Count(*) as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
    if (ScriptRunner($Script, "Ct") > 0) {
        $Script = "select [SName] +' '+ [FName]+ ' ['+[EmpID]+']' as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
        echo ScriptRunner($Script, "Ct");
    }?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" >&nbsp;
										<?php
if ($Dsbl == " disabled ") {echo "Verifying";} else {
        if ($row2['Status'] == "PA") {echo "Pending";} elseif ($row2['Status'] == "C") {echo "Cancelled";} elseif ($row2['Status'] == "R") {echo "Rejected";} elseif ($row2['Status'] == "A") {echo "Approved";} elseif ($row2['Status'] == "PC") {echo "Canceling";}
    }?></td>
									<td align="center" valign="middle" class="TinyText" scope="col" >
										<?php if ($row2['Status'] == 'A') {
        if (ValidateURths("APPROVE TEAM LEAVE" . "A") == true) {
            ?>
										<img src="../images/icon_print_.gif" title="Print approval notice for employee" name="EditEdu<?php echo $Del; ?>" height="10" border="0" id="EditEdu<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('View&nbsp;Leave&nbsp;Details','lms/Rpt_Lv_Doc.php?HID=<?php echo $row2['HashKey']; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/icon_print_.gif',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" />
										<?php
}
    } elseif ($row2['Status'] == 'R') {
        if (ValidateURths("APPROVE TEAM LEAVE" . "A") == true) {
            ?>
										<img src="../images/icon_print_.gif" title="Print approval notice for employee" name="EditEdu<?php echo $Del; ?>" height="10" border="0" id="EditEdu<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('View&nbsp;Leave&nbsp;Details','lms/Rpt_Lv_Doc_Rej.php?HID=<?php echo $row2['HashKey']; ?>',600,900,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/icon_print_.gif',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" />
										<?php
}
    } elseif ($row2['Status'] == 'PA') {
        if (ValidateURths("APPROVE TEAM LEAVE" . "T") == true) {
            ?>
										<img src="../images/power_but.gif" title="View employee leave details" name="EditEdu<?php echo $Del; ?>" height="10" border="0" id="EditEdu<?php echo $Del; ?>" onMouseOut="MM_swapImgRestore()" onClick="parent.ShowDisp('View&nbsp;Leave&nbsp;Details','lms/Lv_Details.php?HID=<?php echo $row2['HashKey']; ?>',320,600,'Yes')" onMouseOver="MM_swapImage('EditEdu<?php echo $Del; ?>','','../images/power_but.gif',1); ImgBorderOn('EditEdu<?php echo $Del; ?>', 0);" />
										<?php
}
    }?>
									</td>
								</tr>
						        <?php
}
include '../login/dbClose2.php';
?>
      						</tbody>
    					</table>
    				</div>
					<div class="col-md-6 pt-5">
				    	<?php include '../main/pagination.php';?>
				  	</div>
				  	<div class="col-md-6 pt-5">
				  		<div class="row">
							<div class="col-md-5"></div>
							<div class="col-md-7">
								<?php
if (isset($_REQUEST["PgTy"])) {
    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
										<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
										<input name="FAction" id="FAction" type="hidden">
										<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
} else {
    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
									<input name="PgTy" id="PgTy" type="hidden" />
										<input name="FAction" id="FAction" type="hidden">
										<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
}

if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
    if (ValidateURths("APPROVE TEAM LEAVE" . "T") == true) {
        $but_HasRth = ("APPROVE TEAM LEAVE" . "T");
        $but_Type = 'Authorize';include '../main/buttons.php';
        $but_HasRth = ("APPROVE TEAM LEAVE" . "T");
        $but_Type = 'Reject';include '../main/buttons.php';
    }

    if (ValidateURths("APPROVE TEAM LEAVE" . "T") != true) {
        $but_HasRth = ("APPROVE TEAM LEAVE" . "T");
        $but_Type = 'Authorize';include '../main/buttons.php';
    }
} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "AUTHORIZE")) {
    if (ValidateURths("APPROVE TEAM LEAVE" . "T") != true) {
        $but_HasRth = ("APPROVE TEAM LEAVE" . "T");
        $but_Type = 'Unauthorize';include '../main/buttons.php';
    }
}
?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>



    </tr>
	</form>
</table>
</body>

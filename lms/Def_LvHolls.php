<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("SET HOLIDAYS" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}


if (!isset($EditID)) {
	$EditID = "";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($Del)) {
	$Del = 0;
}
if (!isset($SelID)) {
	$SelID = "";
}
if (!isset($kk)) {
	$kk = "";
}
if (!isset($icount)) {
	$icount = "";
}
if (!isset($PubHols)) {
	$PubHols = "";
}

$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Apply Now" && (isset($_REQUEST["SMDays_H"]) && $_REQUEST["SMDays_H"] == "Single" || $_REQUEST["SMDays_H"] == "Multiple")) {
		//Validate user viewing rights
		if (ValidateURths("SET HOLIDAYS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		if (trim($_REQUEST["PName"]) == "") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must enter a day name', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["PName"]) == "") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must enter a day name', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["HType"]) == "" || trim($_REQUEST["HType"]) == "--") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a day type (Event or Holiday)', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["OffOn"]) == "" || trim($_REQUEST["OffOn"]) == "--") {
			echo ("<script type='text/javascript'>{ parent.msgbox('Specify an Off/On day type for employees', 'red'); }</script>");
			$GoValidate = false;
		} else {
			$StartDate = $_REQUEST["StartDt"];
			$EndDate = $_REQUEST["StartDt"];
		}

		if (isset($_REQUEST["SMDays_H"]) && trim($_REQUEST["SMDays_H"]) == "Multiple") {
			if (trim($_REQUEST["StartDt"]) == "" || trim($_REQUEST["EndDt"]) == "") {
				echo ("<script type='text/javascript'>{ parent.msgbox('You must select a From and To date to continue', 'red'); }</script>");
				$GoValidate = false;
			} else {
				$EndDate = $_REQUEST["EndDt"];
			}

			if (strtotime($_REQUEST["StartDt"]) > strtotime($_REQUEST["EndDt"])) {
				echo ("<script type='text/javascript'>{ parent.msgbox('Your From Date must be a calendar date before the To Date', 'red'); }</script>");
				$GoValidate = false;
			}
		} else {
			$EndDate = $_REQUEST["StartDt"];
		}


		if ($GoValidate == true) {
			$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "NewEventDay" . $_SESSION["StkTck" . "UName"];
			$HashKey = md5($UniqueKey);

			$Script = "INSERT INTO [LvPHolsDetails] 
	([LID],[HType],[OffOn],[SDate],[EDate],[Comment],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES 
	('" . ECh($_REQUEST["PName"]) . "','" . ECh($_REQUEST["HType"]) . "','" . ECh($_REQUEST["OffOn"]) . "','" . ECh($StartDate) . "','" . ECh($EndDate) . "','" . ECh($_REQUEST["GpDesc"]) . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
			ScriptRunnerUD($Script, "Inst");

			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////

			$Script = "Update LvPHolsOff set status='D' where status<>'A' and Status<>'D' and HashKey='" . $HashKey . "'";
			$Script_Edit = "Select Convert(varchar(11),LvDate,106) LD from LvDates where LvDate between '" . ECh($StartDate) . "' and '" . ECh($EndDate) . "'"; //Get all days between event days
			//echo $Script_Edit;
			$dbOpen2 = ($Script_Edit);

			include '../login/dbOpen2.php';
			while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
				$Script = "Select max(id) Ct from LvPHolsOff";
				$New_HashKey = md5(ScriptRunner($Script, "Ct") . "NewIndvEventDays");
				//$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."NewIndvEventDays".$_SESSION["StkTck"."UName"];
				//$HashKey = md5($UniqueKey);

				$Script = "Insert into LvPHolsOff 
			 ([LID],[PDate],[PName],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . $HashKey . "','" . $row2['LD'] . "','" . ECh($_REQUEST["PName"]) . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $New_HashKey . "')";
				//echo "<br>".$Script;
				ScriptRunnerUD($Script, "Inst");
			}
			include '../login/dbClose2.php';
			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////

			/*	
			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////	
			$Script="Select MMsg, MSub from MailTemp where HashKey='6c802f68b6f54c41b17a7e5bd83a4b17'"; //MSub='User Account Login'";
			$Script_Edit="Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')";
			
			/*The following date(s) have been set as notice day(s) by #UserName# and requires your authorization<br />
			<br />
			Notice day(s) name : #NoticeDay# <br />
			Request Type : #RequestType# Day(s)<br />
			Notice day(s) Type : #NoticeDayType#<br />
			Notice day(s) Date : #StartDate# #EndDate#<br />
			Notes : #EmpNote#<br />
			<br />
			For more information, please login into your account.<br />';
			
	
			$LoginDet=ScriptRunner($Script,"MMsg");
			//$LoginDet=str_replace('#DaysCount#',$BookDays,$LoginDet);
			$LoginDet=str_replace('#NoticeDay#',$_REQUEST["PName"],$LoginDet);
			$LoginDet=str_replace('#UserName#',$_SESSION["StkTck"."FName"],$LoginDet);
			$LoginDet=str_replace('#TimeOffType#',ECh($_REQUEST["PType"]),$LoginDet);
			$LoginDet=str_replace('#ServerName#',$_SERVER['HTTP_HOST'],$LoginDet);
			$LoginDet=str_replace('#StartDate#',$StartDate,$LoginDet);
			$LoginDet=str_replace('#EndDate#',' to '.$EndDate,$LoginDet);
			$LoginDet=str_replace('#EmpNote#',ECh(trim($_REQUEST["GpDesc"])),$LoginDet);
			$LoginDet=str_replace('#Link#',$_SERVER['HTTP_HOST'].'/SSLCloud',$LoginDet);
	
	
			$ToMgr=ScriptRunner("Select Email from EmpTbl where HashKey='".ScriptRunner($Script_Edit,"EmpMgr")."'","Email");
			$ToEmp=ScriptRunner($Script_Edit,"Email");
			$Bdy = $LoginDet;
			$Subj = ScriptRunner($Script,"MSub");
			$Atth="";
	
			//Send Mail to employee
			QueueMail($ToEmp,$Subj,$Bdy,'','');
*/


			AuditLog("INSERT", "New event date added - " . ECh($_REQUEST["PName"]));
			echo ("<script type='text/javascript'>{ parent.msgbox('New event day(s) added successfully.', 'green'); }</script>");
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************

	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Record" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		//Validate user viewing rights

		// var_dump($_REQUEST);
		if (ValidateURths("SET HOLIDAYS" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set HashKey of Account to be Updated */
		if (isset($_REQUEST["UpdID"])) {
			$EditID = ECh($_REQUEST["UpdID"]);
			$HashKey = ECh($_REQUEST["UpdID"]);
		}

		if (trim($_REQUEST["PName"]) == "") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must enter a day name', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["PName"]) == "") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must enter a day name', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["HType"]) == "" || trim($_REQUEST["HType"]) == "--") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a day type (Event or Holiday)', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["OffOn"]) == "" || trim($_REQUEST["OffOn"]) == "--") {
			echo ("<script type='text/javascript'>{ parent.msgbox('Specify an Off/On day type for employees', 'red'); }</script>");
			$GoValidate = false;
		} else {
			$StartDate = $_REQUEST["StartDt"];
			$EndDate = $_REQUEST["StartDt"];
		}

		if (isset($_REQUEST["SMDays_H"]) && trim($_REQUEST["SMDays_H"]) == "Multiple") {
			if (trim($_REQUEST["StartDt"]) == "" || trim($_REQUEST["EndDt"]) == "") {
				echo ("<script type='text/javascript'>{ parent.msgbox('You must select a From and To date to continue', 'red'); }</script>");
				$GoValidate = false;
			} else {
				$EndDate = $_REQUEST["EndDt"];
			}

			if (strtotime($_REQUEST["StartDt"]) > strtotime($_REQUEST["EndDt"])) {
				echo ("<script type='text/javascript'>{ parent.msgbox('Your From Date must be a calendar date before the To Date', 'red'); }</script>");
				$GoValidate = false;
			}
		} else {
			$EndDate = $_REQUEST["StartDt"];
		}


		if ($GoValidate == true) {
			$EditID = ECh($_REQUEST["UpdID"]);
			$HashKey = ECh($_REQUEST["UpdID"]);

			$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "NewEventDay" . $_SESSION["StkTck" . "UName"];
			// $HashKey = md5($UniqueKey);

			$Script = "Update [LvPHolsDetails] set [LID]='" . ECh($_REQUEST["PName"]) . "',[HType]='" . ECh($_REQUEST["HType"]) . "',[OffOn]='" . ECh($_REQUEST["OffOn"]) . "',[SDate]='" . ECh($StartDate) . "',[EDate]='" . ECh($EndDate) . "',[Comment]='" . ECh($_REQUEST["GpDesc"]) . "',[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',[UpdatedDate]=GetDate() where HashKey='" . $HashKey . "'";

			// var_dump($Script);
			ScriptRunnerUD($Script, "Inst");

			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////

			// $Script = "Update LvPHolsOff set status='D' where status<>'A' and Status<>'D' and LID ='" . $HashKey . "'";
			$Script = "Update LvPHolsOff set status='D' where LID ='" . $HashKey . "'";
			ScriptRunnerUD($Script, "Inst");
			// var_dump(ECh($StartDate));
			// var_dump(ECh($EndDate));
			$Script_Edit = "Select Convert(varchar(11),LvDate,106) LD, LvDate from LvDates where LvDate between '" . ECh($StartDate) . "' and '" . ECh($EndDate) . "'"; //Get all days between event days
			$dbOpen2 = ($Script_Edit);

			include '../login/dbOpen2.php';
			while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
				$Script = "Select max(id) Ct from LvPHolsOff";
				$New_HashKey = md5(ScriptRunner($Script, "Ct") . "NewIndvEventDays");
				//$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."NewIndvEventDays".$_SESSION["StkTck"."UName"];
				//$HashKey = md5($UniqueKey);

				$Script = "Insert into LvPHolsOff 
			 ([LID],[PDate],[PName],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . $HashKey . "','" . $row2['LD'] . "','" . ECh($_REQUEST["PName"]) . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $New_HashKey . "')";
				ScriptRunnerUD($Script, "Inst");
			}
			include '../login/dbClose2.php';
			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////

			AuditLog("INSERT", "New event date updated for -" . ECh($_REQUEST["PName"]));
			echo ("<script type='text/javascript'>{ parent.msgbox('Event day(s) updated successfully.', 'green'); }</script>");
			$EditID = "";
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		$EditID = 0;
		for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
			if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
				$EditID = $_REQUEST["DelBox" . $i];
				$Script_Edit = "Select *, convert(varchar(11),SDate,106) SDate_, convert(varchar(11),EDate,106) EDate_, convert(varchar(11),AddedDate,106) Dt from LvPHolsDetails where HashKey='" . $EditID . "'";
			}
		}
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		/* Clear ID to avoid opening this record */
	}

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		//Validate user viewing rights
		if (ValidateURths("SET HOLIDAYS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
			if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
				$EditID = $_REQUEST["DelBox" . $i];
				//Move on to Cancel leave request or seek for leave cancellation approval
				$Script = "UPDATE [LvPHolsDetails]
				SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = 'A'
				WHERE [HashKey]='" . $EditID . "'";
				ScriptRunnerUD($Script, "A");
				//echo $Script;

				$Script = "UPDATE [LvPHolsOff]
				SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = 'A'
				WHERE [LID]='" . $EditID . "'";
				ScriptRunnerUD($Script, "A");

				AuditLog("AUTHORIZE", "Even day(s) authorized successfully.");

				MailTrail("SET HOLIDAYS", "APPRV", '', '', $_SESSION["StkTck" . "Email"], ''); //Sends a mail notifications
				echo ("<script type='text/javascript'>{ parent.msgbox('Selected event days authorized successfully.', 'green'); }</script>");
				$EditID = "--";
			}
		}
	}


	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		//Validate user viewing rights
		if (ValidateURths("SET HOLIDAYS" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
			if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
				$EditID = $_REQUEST["DelBox" . $i];
				//Move on to Cancel leave request or seek for leave cancellation approval
				$Script = "UPDATE [LvPHolsDetails]
				SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = 'U'
				WHERE [HashKey]='" . $EditID . "'";
				ScriptRunnerUD($Script, "U");
				//echo $Script;

				$Script = "UPDATE [LvPHolsOff]
				SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[AuthDate] = GetDate()
				,[Status] = 'U'
				WHERE [LID]='" . $EditID . "'";
				ScriptRunnerUD($Script, "U");

				AuditLog("UNAUTHORIZE", "Even day(s) unauthorized successfully.");

				MailTrail("SET HOLIDAYS", "APPRV", '', '', $_SESSION["StkTck" . "Email"], ''); //Sends a mail notifications
				echo ("<script type='text/javascript'>{ parent.msgbox('Selected event days unauthorized successfully.', 'green'); }</script>");
				$EditID = "--";
			}
		}
	}


	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		//Validate user viewing rights
		if (ValidateURths("SET HOLIDAYS" . "D") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
			if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
				$EditID = ECh($_REQUEST["DelBox" . $i]);
				//Move on to Cancel leave request or seek for leave cancellation approval
				$Script = "UPDATE [LvPHolsDetails]
				SET [DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
				,[DeletedDate] = GetDate()
				,[Status] = 'D'
				WHERE [HashKey]='" . $EditID . "'";
				ScriptRunnerUD($Script, "D");

				$Script = "UPDATE [LvPHolsOff]
				SET [DeletedBy] = '" . $_SESSION["StkTck" . "HKey"] . "'
				,[DeletedDate] = GetDate()
				,[Status] = 'D'
				WHERE [LID]='" . $EditID . "'";
				ScriptRunnerUD($Script, "D");

				AuditLog("DELETE", "Even day(s) deleted successfully.");

				MailTrail("SET HOLIDAYS", "APPRV", '', '', $_SESSION["StkTck" . "Email"], ''); //Sends a mail notifications
				echo ("<script type='text/javascript'>{ parent.msgbox('Selected event days deleted successfully.', 'green'); }</script>");
			}
		}
	}
} ?>



<script>
	<?php
	$icount = 0; //GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR
	$dbOpen2 = ("Select PName as PDes, HashKey, (CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,PDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,PDate))) as PubHol,  (CONVERT(VARCHAR(4),DATEPART(M,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))) as Dt from [LvPHolsOff] where Status<>'D'");
	/*UNION
Select LType as PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,LvDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,LvDate))) as PubHol, (CONVERT(VARCHAR(4),DATEPART(M,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))) as Dt from [LvIndTOff] where [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')) and Status not in('D','C') order by Dt desc"); //LvDate>=GetDate() and  */
	include '../login/dbOpen2.php';
	while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { //["3-21-2014","3-28-2014"];   DEACTIVATE SPECIFIC DATES ON THE CALENDAR
		$icount = $icount + 1;

		if (trim($HashKey) == trim($row2['HashKey'])) {
			$ClsSet = ", className: ['smallButton_Auth']";
		} else {
			$ClsSet = ", className: ['smallButton']";
		}
		if ($kk == "") {
			$ClsSet = ", className: ['smallButton_disabled']";
		}

		if ($icount > 1) {
			$SelID = $SelID . ',"' . $row2['Dt'] . '"';
			$PubHols = $PubHols . ", {title: '" . $row2['PDes'] . "', url:'lms/Nw_LvCanl.php?', start: new Date(" . $row2['PubHol'] . "),end: new Date(" . $row2['PubHol'] . ")}";
		} else {
			$SelID = '"' . $row2['Dt'] . '"';
			$PubHols = " {title: '" . $row2['PDes'] . "', url:'lms/Nw_LvCanl.php?', start: new Date(" . $row2['PubHol'] . "),end: new Date(" . $row2['PubHol'] . "), allDay: true}";
		}
	}
	include '../login/dbClose2.php';
	?>



	/* create an array of days which need to be disabled */
	var disabledDays = [<?php echo $SelID; ?>];

	/* utility functions */
	function nationalDays(date) {
		var m = date.getMonth(),
			d = date.getDate(),
			y = date.getFullYear();
		//console.log('Checking (raw): ' + m + '-' + d + '-' + y);
		for (i = 0; i < disabledDays.length; i++) {
			if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1 || new Date() > date) {
				//console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
				return [false];
			}
		}
		//console.log('good:  ' + (m+1) + '-' + d + '-' + y);
		return [true];
	}

	function noWeekendsOrHolidays(date) {
		var noWeekend = jQuery.datepicker.noWeekends(date);
		return noWeekend[0] ? nationalDays(date) : noWeekend;
	}

	$(function() {
		$('#datepicker').datepicker({
			showButtonPanel: true,
			dateFormat: "mm/dd/yy",
			//    beforeShow: function(){    
		});


		$("#StartDt").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "+1D",
			maxDate: "+12M",
			dateFormat: 'dd MM yy'
			<?php if (strlen($SelID) != '') {
				echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
			} ?>
		})


		$("#EndDt").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "+1D",
			maxDate: "+12M",
			dateFormat: 'dd MM yy'
			<?php if (strlen($SelID) != '') {
				echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
			} ?>
		})


	});
</script>

<link rel='stylesheet' type='text/css' href='../jqry/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='../jqry/fullcalendar/fullcalendar.min.js'></script>

<script type='text/javascript'>
	$(document).ready(function() {

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			editable: false,
			events: 'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic',
			dayClick: function(date, allDay, jsEvent, view) {
				if (allDay) {
					//parent.msgbox(date+' is free. You can book an off day here', 'green');
					//alert('Clicked on the entire day: ' + date);
				} else {
					//parent.msgbox(date+' is free. You can book an off day here', 'green');
					//alert('Clicked on the slot: ' + date);
				}
			},

			eventClick: function(event) {
				// opens events in a popup window
				parent.ShowDisp('Cancel my leave', 'lms/Nw_LvCanl.php?c=d' + '&LDate=' + event.start, 320, 600, 'Yes')
				//window.open(event.start, 'gcalevent', 'width=700,height=600');
				return false;
			},

			events: [
				/*{
					title: 'All Day Event',
					start: '2014,March,2' //new Date(y, m, 1)
					
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
					
					or 
					
					allDay: false // No END date when using this.
					url: 'http://google.com/' To use hyperlink 
				},*/
				// {title: 'Christmas2',	url:'http://www.yahoo.com', start: new Date(14, 3, 25),end: new Date(14, 12, 25);}

				<?php echo $PubHols; ?>

			]
		});
	});
</script>
<style type='text/css'>
	#calendar {
		width: 450px;
		margin: 0 auto;
	}
</style>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
	echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
	<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center" style="margin-top: 1%;">
						<h4>
							Define Calendar Event Days
						</h4>
					</div>
					<div class="col-md-4">
						<input name="SubmitTrans" type="reset" class="btn btn-danger btn-sm pull-right " style="margin-top: 1%;" id="SubmitTrans" value="Clear" />
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-7">
						<div id='calendar'></div>
					</div>
					<div class="col-md-5">
						<div class="box bg-lightest">
							<div class="box-header with-border text-center">
								<h5 class="box-title">
									<strong>Define Your Notice Dates </strong>
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5"></div>
							<div class="col-sm-7">
								<!-- <input name="SMDays" type="submit" class="btn btn-danger btn-sm" id="SMDays" value="Single Day">
								<input name="SMDays" type="submit" class="btn btn-danger btn-sm" id="SMDays" value="Multiple Days"> -->
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Day(s) Name<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" && $EditID != "--") {
											echo '<input name="PName" id="PName" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "LID") . '" size="16" maxlength="45" /></span>';
										} else {
											echo '<input name="PName" id="PName" type="text" class="form-control" value="" size="16" maxlength="45" value="" /></span>';
										}
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Day Type:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<select name="HType" class="form-control" id="HType">
											<?php
											$SelID = "";
											if ($EditID != "" && $EditID != "--") {
												$SelID = ScriptRunner($Script_Edit, "HType");
											}
											if ($SelID == "Event") {
												echo '<option selected value="Event" selected>Event</option> <option value="Holiday">Holiday</option>';
											} elseif ($SelID == "Holiday") {
												echo '<option selected value="Holiday" selected>Holiday</option> <option value="Event">Event</option>';
											} else {
												echo '<option selected value="--" selected>--</option> <option value="Event">Event</option> <option value="Holiday">Holiday</option>';
											}
											?>
										</select>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" id="MastersLeave2" title="Select a day type. An official event or pubic holiday">
												<i class="fa fa-info"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Day(s) Off:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<select name="OffOn" class="form-control" id="OffOn">
											<?php
											$SelID = "";
											if ($EditID != "" && $EditID != "--") {
												$SelID = ScriptRunner($Script_Edit, "OffOn");
											}
											if ($SelID == "1") {
												echo '<option selected value="1" selected>Yes</option> <option value="0">No</option>';
											} elseif ($SelID == "No") {
												echo '<option selected value="0" selected>No</option> <option value="1">Yes</option>';
											} else {
												echo '<option selected value="--" selected>--</option> <option value="1">Yes</option> <option value="0">No</option>';
											}
											?>
										</select>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" id="MastersLeave" title="Is the day an Off day to employees">
												<i class="fa fa-info"></i>
											</button>
										</span>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">From Date<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" && $EditID != "--") {
											echo '<input name="StartDt" id="StartDt" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "SDate_") . '" size="16" maxlength="12" readonly="readonly" /></span>';
										} else {
											echo '<input name="StartDt" id="StartDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly="readonly" value="" /></span>';
										}
										?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">To Date<span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<?php
										if ($EditID != "" || $EditID > 0) {
											echo '<input name="EndDt" id="EndDt" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "EDate_") . '" size="16" maxlength="12" readonly="readonly" /></span>';
										} else {
											echo '<input name="EndDt" id="EndDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly="readonly" value="" /></span>';
										}
										?>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label"> Day(s) Description:</label>
									<div class="col-sm-8" style=" padding-right: 35px">
										<?php
										if ($EditID != "" && $EditID != "--") {
											echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc">' . ScriptRunner($Script_Edit, "Comment") . '</textarea>';
										} elseif (isset($_REQUEST["GpDesc"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] != "Clear"))) {
											echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc">' . $_REQUEST["GpDesc"] . '</textarea>';
										} else {
											echo '<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc"></textarea>';
										}
										?>
									</div>
								</div>
								<div class="form-group row pull-right mr-20">
									<?php
									// if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Multiple Days") {
									// 	echo '<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Single" />';
									// } else {
									echo '<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Multiple" />';
									// }
									// BEN HERE 
									if (ValidateURths("SET HOLIDAYS" . "A") == true) {
										if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "AddIndTrans" && ($EditID == "" || $EditID == '--')) {
											echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Apply Now" />';
										} elseif ($EditID != "" && $EditID != '--') {
											/*
												if (ScriptRunner($Script_Edit,"Status") =='A')
												{
													$AuthStat = "A";
													echo '<input name="SubmitTrans" disabled="disabled" type="submit" class="btn btn-danger btn-sm_disabled" id="SubmitTrans" value="Update Record" />';
												}
												else
												{ */
											echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update Record" />';
											echo '<input name="UpdID" type="hidden"  id="UpdID" value="' . $EditID . '" />';
											//}
										} else {
											echo '<input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm" id="SubmitTrans" value="Apply Now" />';
										}
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ">
						<!-- ++++++++++++++++++++++++++++++ BUTTON +++++++++++++++++++++++++++++++-->
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						//------------------------
						if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,* from LvPHolsDetails where (Status='N' or Status='U') order by [SDate] desc, AddedDate desc");
						} else {

							print "<h4>AUTHORIZED</h4>";
							$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * from LvPHolsDetails where Status='A' order by [SDate] desc, AuthDate desc");
						}
						//------------------------
						?>
					</div>
					<div class="col-md-12">
						<div class="ViewPatch">
							<table width="100%" class="table table-responsive table-striped table-bordered" id="">
								<thead>
									<tr>
										<th valign="middle" bgcolor="#F0FFFF" class="smallText" scope="col"><span class="TinyTextTight">
												<input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
												<label for="DelChk_All"></label>
											</span></th>
										<th width="167" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Added On</span></th>
										<th width="454" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Event Day Name</span></th>
										<th width="141" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Start Date</span></th>
										<th width="132" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">End Date</span></th>
										<th width="90" align="center" valign="middle" scope="col"> No. of Days</th>
										<th width="90" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Off Day(s)</span></th>
										<th width="167" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Status</span></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$Del = 0;
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										$Del = $Del + 1;
									?>
										<tr>
											<td width="21" height="27" valign="middle" scope="col" class="TinyTextTight">
												<input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" />
												<label for="<?php echo ("DelBox" . $Del); ?>"></label>
												<?php
												?>
											</td>
											<td width="167" align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
											<td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['LID'])); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col">&nbsp; <?php echo $row2['SDt']; ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col"><?php echo $row2['EDt']; ?>
											</td>
											<td align="center" valign="middle" class="TinyText" scope="col"><?php
																											$Script = "select Count(*) Ct from [LvPHolsOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C')";
																											echo ScriptRunner($Script, "Ct"); ?></td>
											<td align="center" valign="middle" class="TinyText" scope="col"><?php
																											if ($row2['OffOn'] == 1) {
																												echo "Yes";
																											} else {
																												echo "No";
																											}
																											?></td>
											<td width="167" align="center" valign="middle" scope="col" class="TinyText">&nbsp;
												<?php
												if ($row2['Status'] == "PA") {
													echo "Pending";
												} elseif ($row2['Status'] == "C") {
													echo "Cancelled";
												} elseif ($row2['Status'] == "A") {
													echo "Approved";
												} elseif ($row2['Status'] == "PC") {
													echo "Pending";
												}
												?></td>
										</tr>
									<?php }
									include '../login/dbClose2.php'; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-7 pt-5">
						<?php
						echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
							<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
							<input name="FAction" id="FAction" type="hidden" />
							<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
						?>
					</div>
					<div class="col-md-5 pt-5">
						<?php
						//Check if any record was spolled at all before enabling buttons
						if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNAUTHORIZE")) {
							//Check if user has Delete rights
							if (ValidateURths("SET HOLIDAYS" . "D") == true) {
								$but_HasRth = ("SET HOLIDAYS" . "D");
								$but_Type = 'Delete';
								include '../main/buttons.php';
							}

							if (ValidateURths("SET HOLIDAYS" . "V") == true) {
								//Check if user has Add/Edit rights
								$but_HasRth = ("SET HOLIDAYS" . "V");
								$but_Type = 'View';
								include '../main/buttons.php';
							}

							if (ValidateURths("SET HOLIDAYS" . "T") == true) {
								//Check if user has Authorization rights
								$but_HasRth = ("SET HOLIDAYS" . "T");
								$but_Type = 'Authorize';
								include '../main/buttons.php';
							}
						} else {
							if (ValidateURths("SET HOLIDAYS" . "D") == true) {
								//Check if user has Delete rights
								//$but_HasRth=("SET HOLIDAYS".""); $but_Type = 'Delete'; include '../main/buttons.php';
							}

							if (ValidateURths("SET HOLIDAYS" . "V") == true) {
								//Check if user has Add/Edit rights
								$but_HasRth = ("SET HOLIDAYS" . "V");
								$but_Type = 'View';
								include '../main/buttons.php';
							}

							if (ValidateURths("SET HOLIDAYS" . "T") == true) {
								//Check if user has Authorization rights
								$but_HasRth = ("SET HOLIDAYS" . "T");
								$but_Type = 'Unauthorize';
								include '../main/buttons.php';
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
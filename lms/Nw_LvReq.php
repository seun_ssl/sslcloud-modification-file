<?php session_start();
include '../login/scriptrunner.php';
//==========================================================================
//==========================================================================
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("TIMEOFF REQUEST" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

$ModMaster = ValidateURths("LEAVE MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS

$GoValidate = true;
//==========================================================================
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (!isset($EditID)) {
	$EditID = "--";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($StDate)) {
	$StDate = "";
}
if (!isset($EdDate)) {
	$EdDate = "";
}

if (!isset($StartDate)) {
	$StartDate = "";
}
if (!isset($EndDate)) {
	$EndDate = "";
}
if (!isset($AcctNo)) {
	$AcctNo = "";
}


$Accrued_Months = "";
$Display = "";
$RetVal = "";
$PubHols = "";
$SelID = '';

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Apply Now Selected" && ($_REQUEST["SMDays_H"] == "Single" || $_REQUEST["SMDays_H"] == "Multiple")) {
		if (ValidateURths("TIMEOFF REQUEST" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		include 'add_lms_validation.php';

		if ($GoValidate == true) {
			/* If a document is attached to the updated record then call the file upload function */
			if ($_FILES["dmsupd"]["name"] != "") {
				include '../dms/stf_upload_file.php';

				//Upload file to DMS

				$Script = "Select (SName+' '+FName+' '+Convert(Varchar(18),EmpID)) Nm from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
				$RetVal = UploadDoc("Add", "dmsupd", "", "", ScriptRunner($Script, "Nm") . " - " . ECh($_REQUEST["LID"]) . "[" . $StartDate . " to " . $EndDate . "]", "", "L", "");

				if (strlen($RetVal) == 32) {
					$Err = "";
				} else {
					$Err = "<font color=red> Unable to attach file.</font>";
				}
			}


			$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff" . $_SESSION["StkTck" . "UName"];
			$HashKey = md5($UniqueKey);


			//Get Employee ID Hash
			$EmpID = $_SESSION["StkTck" . "HKey"];


			$Script = "INSERT INTO [LvDetails]
	([EmpID],[LID],[RDays],[Subst],[ItemDocHID],[SDate],[EDate],[EComment],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
	('" . $EmpID . "','" . ECh($_REQUEST["LID"]) . "'," . $DaysLeft . ",'" . ECh($_REQUEST["Subst"]) . "','" . $RetVal . "','" . $StartDate . "','" . $EndDate . "','" . ECh(trim($_REQUEST["GpDesc"])) . "','PA','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "')";
			ScriptRunnerUD($Script, "Inst");

			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////

			$Script = "Update LvIndTOff set status='D' where status<>'A' and Status<>'D' and HashKey='" . $HashKey . "'";

			$dbOpen2 = ($Script_Edit);
			include '../login/dbOpen2.php';
			while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
				$UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff_Indv" . $_SESSION["StkTck" . "UName"];
				$New_HashKey = md5($UniqueKey);

				$Script = "Insert into LvIndTOff
				([EmpID],[LID],[LvDate],[LType],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES
				('" . $EmpID . "','" . $HashKey . "','" . $row2['LvDate_'] . "','" . ECh($_REQUEST["LID"]) . "','PA','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $New_HashKey . "')";
				ScriptRunnerUD($Script, "Inst");
			}
			include '../login/dbClose2.php';

			////////////////////////////////-=============================================////////////////////////////////
			////////////////////////////////-=============================================////////////////////////////////
			$backup_name = ScriptRunner("Select (FName+' '+SName) Nm from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Nm");
			$backup_name = isset($backup_name) ? $backup_name : 'Not Selected';

			$Script = "Select SName, ONames, FName from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
			$sname = ScriptRunner($Script, "SName");
			$oname = ScriptRunner($Script, "ONames");
			$fname = ScriptRunner($Script, "FName");
			$for_name = "$sname $oname $fname";


			$Script = "Select MMsg, MSub from MailTemp where MSub='New Time-off Request Pending Approval'"; //MSub='User Account Login'";
			$Script_Edit = "Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";

			$s_name = ScriptRunner($Script_Edit, "SName");
			$o_name = ScriptRunner($Script_Edit, "ONames");
			$f_name = ScriptRunner($Script_Edit, "FName");
			$EmpLeaveGp = ScriptRunner($Script_Edit, "EmpLeaveGp");
			$full_name = "$f_name $o_name $s_name";
			$for_name = $full_name;


			$LoginDet = ScriptRunner($Script, "MMsg");
			$TType = ECh(ScriptRunner("Select LType from LvSettings where HashKey='" . ECh($_REQUEST["LID"]) . "'", "LType"));
			$LoginDet = str_replace('#DaysCount#', $BookDays, $LoginDet);
			$LoginDet = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet);
			$LoginDet = str_replace('#Name#', ScriptRunner("Select (FName+' '+SName) Nm, Email from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'", "Nm"), $LoginDet);
			$LoginDet = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet);
			$LoginDet = str_replace('#TimeOffType#', $TType, $LoginDet);
			$LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
			$LoginDet = str_replace('#StartDate#', $StartDate, $LoginDet);
			$LoginDet = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet);
			$LoginDet = str_replace('#DaysLeft#', $DaysLeft, $LoginDet);
			$LoginDet = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet);
			$LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);
			$LoginDet = str_replace('#Backup#', $backup_name, $LoginDet);
			$LoginDet = str_replace('#for#', $for_name, $LoginDet);

			/* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */

			$ToMgr = ScriptRunner("Select Email from EmpTbl where HashKey='" . ScriptRunner($Script_Edit, "EmpMgr") . "'", "Email");
			$ToEmp = ScriptRunner($Script_Edit, "Email");
			$Bdy = $LoginDet;
			$Subj = ScriptRunner($Script, "MSub");
			$Atth = "";


			//Send Mail to employee
			$Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
							VALUES('" . $Subj . "','" . $ToEmp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
			ScriptRunnerUD($Script_Mail, "SendLogin");



			//Send mail to manager +++++++++++++++++++++++++++++++++++++++++++++++++++



			$Script = "Select MMsg, MSub from MailTemp where MSub='New Time-off Request Pending Approval'";
			$Script_Edit = "Select * from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
			$kk = ScriptRunner($Script_Edit, "EmpMgr");



			$mgr_name_query = "Select HashKey, (FName+' '+SName) as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
			$mgr_name = ScriptRunner($mgr_name_query, "Nm");

			$LoginDet5 = ScriptRunner($Script, "MMsg");

			$LoginDet5 = str_replace('#Name#', $mgr_name, $LoginDet5);
			$LoginDet5 = str_replace('#DaysCount#', $BookDays, $LoginDet5);
			$LoginDet5 = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet5);
			$LoginDet5 = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet5);
			$LoginDet5 = str_replace('#TimeOffType#', $TType, $LoginDet5);
			$LoginDet5 = str_replace('#StartDate#', $StartDate, $LoginDet5);
			$LoginDet5 = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet5);
			$LoginDet5 = str_replace('#DaysLeft#', $DaysLeft, $LoginDet5);
			$LoginDet5 = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet5);
			$LoginDet5 = str_replace('#Backup#', $backup_name, $LoginDet5);
			$LoginDet5 = str_replace('#for#', $for_name, $LoginDet5);
			$Bdy = $LoginDet5;


			//Send mail to manager
			$Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
			VALUES('" . $Subj . "','" . $ToMgr . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToMgr . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
			ScriptRunnerUD($Script_Mail, "SendLogin");

			// Send mail to Project Manager if selected
			$Script_Edit = "Select * from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
			$kk = ScriptRunner($Script_Edit, "ProMgr");

			$mgr_name_query = "Select HashKey, (FName+' '+SName) as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
			$mgr_name = ScriptRunner($mgr_name_query, "Nm");

			if (isset($mgr_name)) {

				$LoginDet5 = ScriptRunner($Script, "MMsg");

				$LoginDet5 = str_replace('#Name#', $mgr_name, $LoginDet5);
				$LoginDet5 = str_replace('#DaysCount#', $BookDays, $LoginDet5);
				$LoginDet5 = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet5);
				$LoginDet5 = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet5);
				$LoginDet5 = str_replace('#TimeOffType#', $TType, $LoginDet5);
				$LoginDet5 = str_replace('#StartDate#', $StartDate, $LoginDet5);
				$LoginDet5 = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet5);
				$LoginDet5 = str_replace('#DaysLeft#', $DaysLeft, $LoginDet5);
				$LoginDet5 = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet5);
				$LoginDet5 = str_replace('#Backup#', $backup_name, $LoginDet5);
				$LoginDet5 = str_replace('#for#', $for_name, $LoginDet5);
				$Bdy = $LoginDet5;


				$Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
			VALUES('" . $Subj . "','" . $ToMgr . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToMgr . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
				ScriptRunnerUD($Script_Mail, "SendLogin");
			}



			//Back employee

			if (isset($_REQUEST["Subst"]) && strlen($_REQUEST["Subst"]) == 32) {
				$Script = "Select MMsg, MSub from MailTemp where HashKey='ab9e9daa406bbfb1656cdb8254ce2343'"; //MSub='BackUp'";
				$Script_Edit = "Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
				if (strtolower(trim(ScriptRunner($Script_Edit, "Sex"))) == "male") {
					$Sex = " his ";
				} else {
					$Sex = " her ";
				}

				/*A new time-off has been requested by <strong>#EmpName#</strong> and you were selected as #EmpSex# backup.<br />
				<br />
				Request Type : #RequestType# Day(s) #DaysCount# day(s)<br />
				Time-off Type : #TimeOffType#<br />
				Time-off Date : #StartDate# #EndDate#<br />
				Notes : #EmpNote#<br />
				<br />
				You would be notified if this time-off is approved.<br />
				For more information, please login into your account.<br />';
				*/

				$LoginDet = ScriptRunner($Script, "MMsg");
				$s_name = ScriptRunner($Script_Edit, "SName");
				$o_name = ScriptRunner($Script_Edit, "ONames");
				$f_name = ScriptRunner($Script_Edit, "FName");
				$full_name = "$f_name $o_name $s_name";

				$LoginDet = str_replace('#Applicant#', $full_name, $LoginDet);

				$backup_name = ScriptRunner("Select (FName+' '+SName) Nm from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Nm");
				$LoginDet = str_replace('#Backup#', $backup_name, $LoginDet);


				$LoginDet = str_replace('#Request#', $full_name, $LoginDet);

				$LoginDet = str_replace('#EmpSex#', $Sex, $LoginDet);
				$LoginDet = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet);
				$LoginDet = str_replace('#DaysCount#', $BookDays, $LoginDet);
				$LoginDet = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet);
				$LoginDet = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet);
				$LoginDet = str_replace('#TimeOffType#', $TType, $LoginDet);
				$LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
				$LoginDet = str_replace('#StartDate#', $StartDate, $LoginDet);
				$LoginDet = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet);
				$LoginDet = str_replace('#DaysLeft#', $DaysLeft, $LoginDet);
				$LoginDet = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet);
				$LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);

				$ToBackUp = ScriptRunner("Select Email from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Email");
				$Bdy = $LoginDet;
				$Subj = ScriptRunner($Script, "MSub");
				$Atth = "";

				//Send to backup
				$Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
                    VALUES('" . $Subj . "','" . $ToBackUp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToBackUp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";

				ScriptRunnerUD($Script_Mail, "SendLogin");
			}


			//

			//++++++++++++++++++++++++++++++Team Request Notification+++++++++++++++++++++++++++++
			include("mail_trail_send.php");
			include("mail_leave_group.php");
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



			AuditLog("INSERT", "New " . $TType . " timeoff request booked for " . $_SESSION["StkTck" . "UName"]);
			echo ("<script type='text/javascript'>{ parent.msgbox('New timeoff request booked successfully.', 'green'); }</script>");
		}
	}
	//**************************************************************************************************
	//**************************************************************************************************


	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Cancel Selected" && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("TIMEOFF REQUEST" . "D") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$HashVals = explode(";", $_REQUEST["ActLnk"]);
		$ArrayCount = count($HashVals);
		for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
				$EditID = ECh($HashVals[$i]);
				/*$Script="Select Status from [LvIndTOff] WHERE [HashKey] = '" . $EditID . "'";
				$SelID = ScriptRunner($Script,"Status");

				//If PendingAuthorization (PA) cancel leave at once without seeking approval
				if ($SelID =="PA" || $SelID =="N" || $SelID =="U")
				{$SelID = "C";}
				elseif ($SelID =="A")
				{$SelID = "PC";}
				*/

				//Move on to Cancel leave request or seek for leave cancellation approval
				$Script = "UPDATE [LvIndTOff]
				SET [CancelledBy] = '" . $_SESSION["StkTck" . "UName"] . "'
				,[CancelledDate] = GetDate()
				,[Status] = 'C'
				WHERE [LID]='" . $EditID . "' and Status<>'A'";
				ScriptRunnerUD($Script, "Cancel");

				//CANCELS THE MAIN LEAVE HEADER
				$Script = "Select count(*) Ct from LvIndTOff where LID='" . $EditID . "' and status in ('N','PC','PA','A')";
				if (ScriptRunner($Script, "Ct") == 0) {
					$Script = "Update LvDetails set status='C' where HashKey='" . $EditID . "'";
					ScriptRunnerUD($Script, "Cancel");
				}

				AuditLog("UPDATE", "Employee leave cancelled successfully for [" . ECh($_SESSION["StkTck" . "UName"]) . "].");				//Get employee manager to send cancellation email to for approval

				//$Script="Select [Email] from EmpTbl where EmpMgr=(select EmpMgr from Users where LogName='".$_SESSION["StkTck"."UName"]."')";
				//MailTrail("TIMEOFF CANCELLED","A",'','',ScriptRunner($Script,"Email")); //Sends a mail notifications

				////////////////////////////////-=============================================////////////////////////////////
				////////////////////////////////-=============================================////////////////////////////////
				/*	$LoginDet='Hi #EmpName#,<br />
Your following time-off request has been Approved.<br />
<br />
Time-off Type : #TimeOffType# Day(s) #DaysCount# day(s)<br />
Time-off Date : #StartDate# #EndDate#<br />
Remaining Balance : #DaysLeft# day(s)<br />
Your Notes : #EmpNote#<br />
Manager Notes : #MgrNote#<br />
<br />
Approved by : #MrgName#<br />

For more information, please login into your account.';
*/

				$Script_Det = "Select Convert(varchar(11),SDate,106) as SD, Convert(varchar(11),EDate,106) as ED, * from [LvDetails] WHERE [HashKey]='" . $EditID . "'";  // Get details for the leave application
				$Script = "Select MMsg, MSub from MailTemp where HashKey='b0b4f9e5cb933606109105f3d3sf1f97'"; //MSub='TimeOff Approved'";
				$Script_Edit = "Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";



				$LeaveScript = "Select * from LvSettings where HashKey='" . $leave_id . "'";
				$LeaveType = ScriptRunner($LeaveScript, "LType");



				//Set the message to send to Manager and Employee
				$LoginDet = ScriptRunner($Script, "MMsg");

				//$MsgVal=ScriptRunner($Script_Edit,"Nm");
				//$LoginDet=str_replace('#MrgName#',$MsgVal,$LoginDet);

				$MsgVal = ScriptRunner("Select (FName+' '+SName) Nm from EmpTbl where HashKey=(Select EmpID from [LvDetails] WHERE [HashKey]='" . $EditID . "')", "Nm");
				$LoginDet = str_replace('#EmpName#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner("Select Count(*) Ct from LvIndTOff where Status='A' and LID='" . $EditID . "'", "Ct");
				$LoginDet = str_replace('#DaysCount#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "LID");
				//$MsgVal=ScriptRunner($Script_Det,"LType");
				$LoginDet = str_replace('#TimeOffType#', $MsgVal, $LoginDet);

				$LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "SD");
				$LoginDet = str_replace('#StartDate#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "ED");
				$LoginDet = str_replace('#EndDate#', ' to ' . $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "RDays");
				$LoginDet = str_replace('#DaysLeft#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "EComment");
				$LoginDet = str_replace('#EmpNote#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "MComment");
				$LoginDet = str_replace('#MgrNote#', $MsgVal, $LoginDet);

				$MsgVal = ScriptRunner($Script_Det, "HRComment");
				$LoginDet = str_replace('#HRNote#', $MsgVal, $LoginDet);

				$LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);


				//	$ToMgr=ScriptRunner($Script_Edit,"Email");
				//	echo $Script_Edit; //=======================================================
				$ToEmp = ScriptRunner("Select Email from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'", "Email");
				$Bdy = $LoginDet;
				$Subj = ScriptRunner($Script, "MSub");
				$Atth = "";

				//Send Mail to employee
				$Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
	VALUES('" . $Subj . "','" . $ToEmp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
				//echo $Script_Mail;
				ScriptRunnerUD($Script_Mail, "SendLogin");

				////////////===========================================================
				////////////===========================================================
				////////////===========================================================
				$Script = "select COUNT(*) Ct ,LID from LvIndTOff where LID='" . $EditID . "' and [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) and Status in('A') group by LID";
				if (ScriptRunner($Script, "Ct") == 0) {
					$Script = "Update LvDetails set Status='C' where HashKey='" . $EditID . "'";
					ScriptRunnerUD($Script, "Cancel");
				} else {
					$Script = "Update LvDetails set Status='A' where HashKey='" . $EditID . "'";
					ScriptRunnerUD($Script, "Cancel");
				}
				echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) cancelled successfully.', 'green'); }</script>");
			}
		}
	}
}




$EmpLeaveGp = "";
//echo "jkhfjklgsfljkgsf";
$Script = "Select * from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
//echo $Script."<br>";
$EmpLeaveGp = ScriptRunner($Script, "EmpLeaveGp");
//echo $EmpLeaveGp."ll";

if (strlen($EmpLeaveGp) != 32) {
	$Script = "Select GpLeave from UGpRights where HashKey='" . $_SESSION["StkTck" . "UGrp"] . "'";
	$EmpLeaveGp = ScriptRunner($Script, "GpLeave");
}
//echo 'alert("'.$EmpLeaveGp.'");';
//echo $EmpLeaveGp;
?>





<script>
	<?php
	$icount = 0; //GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR

	$Script = "select Count(*) Ct from [LvIndTOff] where [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) and Status not in('D','C','R')";

	if (ScriptRunner($Script, "Ct") > 0) {
		$got_val = "got val";

		$dbOpen2 = ("
	Select Status, '' as DtDiff, PName as PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,PDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,PDate))) as PubHol,  (CONVERT(VARCHAR(4),DATEPART(M,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))) as Dt from [LvPHolsOff] where Status='A'
	UNION
	Select Status, DateDiff(D,GetDate(),LvDate) DtDiff, (select LType from LvSettings where HashKey=LiO.LType) as PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,LvDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,LvDate))) as PubHol, (CONVERT(VARCHAR(4),DATEPART(M,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))) as Dt from [LvIndTOff] LiO

	where [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) and Status not in('D','C','R')

	order by Dt desc"); //LvDate>=GetDate() and
	} else {
		$got_val = "NO val";

		$dbOpen2 = ("
Select Status, DateDiff(D,GetDate(),LvDate) DtDiff, (select LType
from
LvSettings
 where HashKey=LiO.LType) as PDes,
 (CONVERT(VARCHAR(4), DATEPART(YEAR,LvDate))+','
 +CONVERT(VARCHAR(4),(DATEPART(M,LvDate)-1))+',
 '+CONVERT(VARCHAR(4),DATEPART(D,LvDate)))
 as PubHol, (CONVERT(VARCHAR(4),DATEPART(M,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))) as Dt from [LvIndTOff] LiO"); //LvDate>=GetDate() and
	}


	include '../login/dbOpen2.php';
	while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { //["3-21-2014","3-28-2014"];   DEACTIVATE SPECIFIC DATES ON THE CALENDAR
		$icount = $icount + 1;

		if ($row2['Status'] == "A") {
			$ClsSet = ", className: ['smallButton_Auth']";
		} else {
			$ClsSet = ", className: ['smallButton']";
		}

		if ($row2['DtDiff'] < 0) {
			$ClsSet = ", className: ['smallButton_disabled']";
		}

		if ($icount > 1) {
			$SelID = $SelID . ',"' . $row2['Dt'] . '"';
			//$PubHols = $PubHols.", {title: '".$row2['PDes']."', url:'lms/Nw_LvCanl.php?', start: new Date(".$row2['PubHol']."),end: new Date(".$row2['PubHol']."), allDay: true ".$ClsSet." }";

			$PubHols = $PubHols . ", {title: '" . $row2['PDes'] . "', start: new Date(" . $row2['PubHol'] . "), end: new Date(" . $row2['PubHol'] . "), allDay: true " . $ClsSet . " }";
		} else {
			//if ($row2['Dt'] is less than today or the emp id is not mine change ///// $EvtColor= "className: ['smallButton']"
			$SelID = '"' . $row2['Dt'] . '"';
			//$PubHols = " {title: '".$row2['PDes']."', url:'lms/Nw_LvCanl.php?', start: new Date(".$row2['PubHol']."),end: new Date(".$row2['PubHol']."), allDay: true ".$ClsSet."}";
			$PubHols = " {title: '" . $row2['PDes'] . "', start: new Date(" . $row2['PubHol'] . "), end: new Date(" . $row2['PubHol'] . "), allDay: true " . $ClsSet . "}";
		}
	}
	include '../login/dbClose2.php';

	//Added By Odunayo Ogungbure
	//Disable Future Public Holidays
	$disabledDays = [];
	$dbOpen2 = ("SELECT CONVERT(VARCHAR, PDate, 23) AS Dt FROM [LvPHolsOff] WHERE Status='A' AND PDate >= GETDATE()");
	include '../login/dbOpen2.php';
	while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
		$disabledDays[] = $row2['Dt'];
	}
	include '../login/dbClose2.php'
	?>



	/* create an array of days which need to be disabled */
	var disabledDays = [<?php echo $SelID; ?>];

	/* utility functions */
	function nationalDays(date) {
		var m = date.getMonth(),
			d = date.getDate(),
			y = date.getFullYear();
		//console.log('Checking (raw): ' + m + '-' + d + '-' + y);
		for (i = 0; i < disabledDays.length; i++) {
			if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1 || new Date() > date) {
				//console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
				return [false];
			}
		}
		//console.log('good:  ' + (m+1) + '-' + d + '-' + y);
		return [true];
	}

	function noWeekendsOrHolidays(date) {
		var noWeekend = jQuery.datepicker.noWeekends(date);
		return noWeekend[0] ? nationalDays(date) : noWeekend;
	}

	$(function() {
		$('#datepicker').datepicker({
			showButtonPanel: true,
			dateFormat: "mm/dd/yy",
			//    beforeShow: function(){
		});

		<?php //GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR
		$icount = 0;
		$dbOpen2 = ("
SELECT DeactivateDay =
      CASE Val1
         WHEN 'Sunday' THEN 'day != 0'
         WHEN 'Monday' THEN 'day != 1'
         WHEN 'Tuesday' THEN 'day != 2'
         WHEN 'Wednesday' THEN 'day != 3'
         WHEN 'Thursday' THEN 'day != 4'
         WHEN 'Friday' THEN 'day != 5'
         WHEN 'Saturday' THEN 'day != 6'
         ELSE ''
      END
from Masters where ItemName='Weekends'");
		include '../login/dbOpen2.php';
		while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { //day != 6 && day != 0   DEACTIVATE DAY of THE WEEK ON CALENDAR
			$icount = $icount + 1;
			if ($icount > 1) {
				$SelID = $SelID . " && " . $row2['DeactivateDay'];
			} else {
				$SelID = $row2['DeactivateDay'];
			}
		}
		include '../login/dbClose2.php';


		//$Script="Select Earliest, Stay from LvSettings where HashKey=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')";
		$Script = "Select Earliest, Stay from LvSettings where HashKey='" . $EmpLeaveGp . "'";
		$minDt = ScriptRunner($Script, "Earliest");
		if ($minDt == "--") {
			$minDt == "";
		}
		$maxDt = ""; //ScriptRunner($Script,"Furthest");
		if ($maxDt == "--") {
			$maxDt == "";
		} ?>






	});
</script>

<script>
	$(function() {
		$("#tabs").tabs();
	});
</script>



<link rel='stylesheet' type='text/css' href='../jqry/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='../jqry/fullcalendar/fullcalendar.min.js'></script>

<script type='text/javascript'>
	$(document).ready(function() {

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			editable: false,
			events: false, //'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic',
			dayClick: function(date, allDay, jsEvent, view) {
				if (allDay) {
					parent.msgbox(date + ' is free. You can book an off day here', 'green');
					//alert('Clicked on the entire day: ' + date);
				} else {
					parent.msgbox(date + ' is free. You can book an off day here', 'green');
					//alert('Clicked on the slot: ' + date);
				}
			},

			/*
			eventClick: function(event)
			{
				// opens events in a popup window
				parent.ShowDisp('Cancel my leave','lms/Nw_LvCanl.php?c=d&LDate='+event.start,320,600,'Yes')
				//window.open(event.start, 'gcalevent', 'width=700,height=600');
				return false;
			},
			*/


			events: [
				/*{
					title: 'All Day Event',
					start: '2014,March,2' //new Date(y, m, 1)

					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)

					or

					allDay: false // No END date when using this.
					url: 'http://google.com/' To use hyperlink
				},*/
				// {title: 'Christmas2',	url:'http://www.yahoo.com', start: new Date(14, 3, 25),end: new Date(14, 12, 25);}

				<?php echo $PubHols; ?>

			],
			color: 'yellow', // an option!
			textColor: 'black' // an option!
		});
	});
</script>

<script>
	$(function() {
		$(document).tooltip();
	});
</script>

<style type='text/css'>
	#calendar {
		width: 100%;
		margin: 0 auto;
	}
</style>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
	echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
	<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-12 text-center ">
						<h4>
							Book A TimeOff
						</h4>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-7">
						<div class="row">
							<div class="col-12">
								<div class="nav-tabs-custom" style="min-height: 400px">
									<ul class="nav nav-tabs">
										<li>
											<a class="active" href="#tabs-1" data-toggle="tab">My Calendar</a>
										</li>
										<li>
											<a href="#tabs-2" data-toggle="tab">Leave Details</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tabs-1">
											<?php
											if ($got_val == "NO val") {

												print "<div id=''></div>";
											} else {
												print "<div id='calendar'></div>";
											}
											?>
										</div>
										<div class="tab-pane" id="tabs-2">
											<?php
											//$Script="Select HashKey from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey=(Select EmpMgr from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."'))";
											//if (strlen(ScriptRunner($Script,"HashKey"))!=32)
											if (strlen($EmpLeaveGp) != 32) {
												echo ("<script type='text/javascript'>{ parent.msgbox('You do not have an active manager tied to your employee record. Contact your HR', 'red'); }</script>");
												exit;
											}
											//$Script="Select Count(LType) Ct from LvSettings where Status='A' and LID=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')";
											$Script = "Select Count(LType) Ct from LvSettings where Status='A' and LID='" . $EmpLeaveGp . "'";
											//echo $Script;
											if (ScriptRunner($Script, "Ct") < 1) {
												echo ("<script type='text/javascript'>{parent.msgbox('You have no leave group defined for your account. Contact your administrator', 'red');}i</script>");
												exit;
											}

											$Script = "Select *, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";
											if (ScriptRunner($Script, "SetValue") == "Period") {
												$StDate = ScriptRunner($Script, "SYear");
												$EdDate = ScriptRunner($Script, "EYear");
											} else {
												$Script = "Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";

												//Check what year the anniversary falls into [Last year to this year] or [This year to next year]
												$StDate = ScriptRunner($Script, "Dt") . ' ' . ScriptRunner($Script, "Yr");
												if (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
													$EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") + 1);
												} elseif (strtotime($StDate) >= strtotime(ScriptRunner($Script, "TDay"))) {
													$StDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") - 1);
													$EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr"));
												}
											}

											// $Script="Select DATEDIFF(M,'".$StDate."',GETDATE()) Mts";
											$Script = "Select DATEDIFF(D,'" . $StDate . "',GETDATE()) Mts";
											$Accrued_Months = ScriptRunner($Script, "Mts"); //Used to divide to get Accrued days count - Accrued months is the start month minus the current month as an integer
											//AccrueD MONTH MUST BEGIN COUNTING ONLY AFTER YOU ARE SURE OF THE START MONTH EITHER FOR PERIOD BASED OR ANNIVERSARY BASED LEAVE TYPE.

											echo '<table width="100%"><tr>
												<td align="right" class="tdMenu_HeadBlock_Light">Start Period</td>
												<td align="left" class="subHeader">' . $StDate . '</td>
												<td align="right" class="tdMenu_HeadBlock_Light">End Period</td>
												<td align="left" class="subHeader">' . $EdDate . '</td>
												</tr>';



											$Display = "";
											//$dbOpen2 = ("Select LType from LvSettings where Status='A' and LID=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')");
											$dbOpen2 = ("Select LType, HashKey from LvSettings where Status='A' and LID='" . $EmpLeaveGp . "'");

											include '../login/dbOpen2.php';
											while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
												$Script = "Select Accrue, NDays from LvSettings LvSet where LvSet.LID='" . $EmpLeaveGp . "' and LvSet.HashKey='" . $row2['HashKey'] . "' and Status<>'D'";


												$SetDays = ScriptRunner($Script, "NDays");
												$AccruType = ScriptRunner($Script, "Accrue");

												$Script = "(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D' and Status<>'R') and LType='" . $row2['HashKey'] . "' and ([LvDate] between ('" . $StDate . "')	and ('" . $EdDate . "') ) and EmpID='" . $_SESSION["StkTck" . "HKey"] . "')";
												// var_dump($Script);
												//exit;
												$UsedDays = ScriptRunner($Script, "Ct");

												//CALCULATE AccrueD DAYS
												echo '<tr><td align="center" colspan="4" class="tdMenu_HeadBlock">' . $row2['LType'] . '</td></tr>
															  <tr>
															  		<td align="center" class="tdMenu_HeadBlock_Light">Total:&nbsp;' . $SetDays . '</td>';

												if ($AccruType == 1) {
													if ($SetDays > 0) {
														echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:&nbsp;' . number_format((($SetDays / 365) * $Accrued_Months), 0) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays) . '</td>';
													} else {
														echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:hh&nbsp;' . number_format((($SetDays / 365) * $Accrued_Months), 0) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays) . '</td>';
													}

													if ($Display == "") {
														$Display = $row2['LType'] . "  <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
													} //Setup Display
													else {
														$Display = $Display . ", " . $row2['LType'] . "  <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
													} //Setup Display
												} else {
													echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:&nbsp;' . $SetDays . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . ($SetDays - $UsedDays) . '</td>';
													if ($Display == "") {
														$Display = $row2['LType'] . ":" . ($SetDays - $UsedDays);
													} //Setup Display
													else {
														$Display = $Display . ", " . $row2['LType'] . ":" . ($SetDays - $UsedDays);
													} //Setup Display
												}
												echo '</tr>';
											}
											include '../login/dbClose2.php';
											echo '</table>';
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="box bg-lightest">
							<div class="box-header with-border text-center ">
								<h5 class="box-title">
									<strong>Days Left </strong>
								</h5>
							</div>
							<div class="box-body">
								<strong style="font-weight: 550">
									<?php
									/*
										$Script="Select HashKey from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey=(Select EmpMgr from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."'))";
										if (strlen(ScriptRunner($Script,"HashKey"))!=32)
										{
											echo ("<script type='text/javascript'>{ parent.msgbox('You do not have an active manager tied to your employee record. Contact your HR', 'red'); }</script>");
											exit;
										}

										$Script="Select Count(LType) Ct from LvSettings where Status='A' and LID=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')";
										if (ScriptRunner($Script,"Ct") < 1)
										{
											echo ("<script type='text/javascript'>{parent.msgbox('You have no leave group defined for your account. Contact your administrator', 'red');}</script>");
											exit;
										}

										$Script="Select *, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";


										if (ScriptRunner($Script,"SetValue")=="Period")
										{
											$StDate=ScriptRunner($Script,"SYear");
											$EdDate=ScriptRunner($Script,"EYear");
										}
										else
										{
											$Script="Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='".$_SESSION["StkTck"."HKey"]."'";

											//Check what year the anniversary falls into [Last year to this year] or [This year to next year]
											$StDate=ScriptRunner($Script,"Dt").' '.ScriptRunner($Script,"Yr");
											if (strtotime($StDate) < strtotime(ScriptRunner($Script,"TDay")))
											{
												$EdDate=ScriptRunner($Script,"Dt").' '.(ScriptRunner($Script,"Yr") + 1);
											}
											elseif (strtotime($StDate) >= strtotime(ScriptRunner($Script,"TDay")))
											{
												$StDate=ScriptRunner($Script,"Dt").' '.(ScriptRunner($Script,"Yr")-1);
												$EdDate=ScriptRunner($Script,"Dt").' '.(ScriptRunner($Script,"Yr"));
											}
										}

										$Script="Select DATEDIFF(M,'".$StDate."',GETDATE()) Mts";
										$Accrued_Months=ScriptRunner($Script,"Mts"); //Used to divide to get Accrued days count - Accrued months is the start month minus the current month as an integer
										//AccrueD MONTH MUST BEGIN COUNTING ONLY AFTER YOU ARE SURE OF THE START MONTH EITHER FOR PERIOD BASED OR ANNIVERSARY BASED LEAVE TYPE.

										$dbOpen2 = ("Select Accrue, LType from LvSettings where Status='A' and LID=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')");
										include '../login/dbOpen2.php';
										while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
										{
											$Script="select distinct(LvSet.NDays-(select COUNT(*) from LvIndTOff where (Status<>'C' and Status<>'D') and LType='".$row2['LType']."'
											and ([LvDate]
											between
											('".$StDate."')
											and
											('".$EdDate."')
											)
											and EmpID=
											(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."'))
											)) as NDays,LType from LvSettings as LvSet where LvSet.LID=(
											Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')
											and LvSet.LType='".$row2['LType']."' order by LvSet.LType asc"; //LvDet.LID=LvSet.LType and

										//echo $Script."<br><br>";

												//CALCULATE AccrueD DAYS
												$SetID = ScriptRunner($Script,"NDays");


												//if ($SetID == ""){$SetID=0;}
												//echo $row2['LType'].":".$SetID.",&nbsp;";

												if ($row2['Accrue']==1)
												{
													if ($SetID>0)
													{
														echo $row2['LType'].":".number_format( (($SetID / 12) * $Accrued_Months) ,0).",&nbsp;"; //Do not show day value with decimal places
													}
												}
												else
												{
													echo $row2['LType'].":".$SetID.",&nbsp;";
												}
										}
										include '../login/dbClose2.php';*/
									echo $Display;
									?>
								</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5"></div>
							<div class="col-sm-7">
								<input name="SMDays" type="submit" class="btn btn-danger btn-sm" id="SMDays" value="Single Day">
								<input name="SMDays" type="submit" class="btn btn-danger btn-sm" id="SMDays" value="Multiple Days">
							</div>
						</div><br>
						<?php //print $got_val; 
						?>
						<div class="row">
							<!-- <div class="col-md-1"></div> -->
							<div class="col-md-12">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Type:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<select class="form-control" name="LID" id="LID" onchange="getEarliestAndMax(this)">
											<option value="--" selected="selected">--</option>
											<?php
											if (isset($EditID) && ($EditID != "" && $EditID != "--")) {
												$SelID = ScriptRunner($Script_Edit, "LID");
											}

											$dbOpen2 = ("select LType, HashKey from LvSettings where LID='" . $EmpLeaveGp . "'");
											//echo $dbOpen2;
											//$dbOpen2 = ("select LType from LvSettings where LID=(Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."')");
											include '../login/dbOpen2.php';
											while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
												if ($SelID == $row2['LType']) {
													echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['LType'] . '</option>';
												} else {
													echo '<option value="' . $row2['HashKey'] . '">' . $row2['LType'] . '</option>';
												}
											}
											include '../login/dbClose2.php';
											?>
										</select>
										<span class="input-group-btn">
											<?php
											$imgID = "MastersEmpStatus";
											if ($ModMaster == true) {
												/*	$imgPath = "../images/plus_.jpg";
													$Titl="Add a new employee status eg. Active, Retired, Terminated";
													$MOvr="";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=EmployeeStatus&LDb=Single&amp;Elmt=EmployeeStatus',320,400,'No')";
													*/
												$imgPath = "fa fa-info";
												$Titl = "Select a time-off type you wish to apply for";
												$OnClk = "";
											} else {
												$imgPath = "fa fa-info";
												$Titl = "Select a time-off type to apply for now";
												$OnClk = "";
											}

											echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
												<i class="' . $imgPath . '"></i>
												</button>';
											?>
										</span>
									</div>
								</div>
								<?php if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Single Day") { ?>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Select Date:<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
											<input name="StartDt" id="StartDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
										</div>
									</div>
								<?php } else { ?>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label" style="padding-right: 0px;">From Date<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
											<input name="StartDt" id="StartDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label" style="padding-right: 0px;">To Date
											<span style="color: red">*</span>:</label>
										<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
											<input name="EndDt" id="EndDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
										</div>
									</div>

								<?php } ?>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Back up Employee <span style="color: red">*</span>:</label>
									<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
										<select class="form-control" name="Subst" id="Subst">
											<?php
											$AllEmployee = '';
											echo '<option value="--" selected="selected">--</option>';
											$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' order by SName Asc");
											include '../login/dbOpen2.php';
											while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
												$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
											}
											echo $AllEmployee;
											include '../login/dbClose2.php';
											if ($EditID != '' && $EditID != '--') {
												$Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
												$empnm = ScriptRunner($Script_Edit, "Nm");
												echo '<option selected="selected" value="' . $EditID . '">' . $empnm . '</option>';
											}
											?>
										</select>
										<span class="input-group-btn">
											<?php
											$imgID = "MastersEmpStatus";
											if ($ModMaster == true) {
												/*	$imgPath = "../images/plus_.jpg";
													$Titl="Add a new employee status eg. Active, Retired, Terminated";
													$MOvr="";
													$OnClk = "parent.ShowDisp('Add&nbsp;to&nbsp;Masters','main/add_masters.php?GroupName=EmployeeStatus&LDb=Single&amp;Elmt=EmployeeStatus',320,400,'No')";
													*/
												$imgPath = "fa fa-info";
												$Titl = "Select an employee to work in your stead";
												$OnClk = "";
											} else {
												$imgPath = "fa fa-info";
												$Titl = "Select an employee to work in your stead";
												$OnClk = "";
											}

											echo '<button type="button" class="btn btn-default" id="' . $imgID . '" title="' . $Titl . '" onClick="' . $OnClk . '">
												<i class="' . $imgPath . '"></i>
												</button>';
											?>
										</span>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputFile" class="col-sm-4 col-form-label">Upload Document
										:</label>
									<div class="col-sm-8 input-group-sm">
										<input type="file" name="dmsupd" id="dmsupd" class="btn btn-danger btn-sm" />
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-sm-4 col-form-label"> Leave Description:</label>
									<div class="col-sm-8" style=" padding-right: 35px">
										<textarea name="GpDesc" rows="2" class="form-control" id="GpDesc"></textarea>
									</div>
								</div>
								<div class="form-group row pull-right mr-20">
									<?php
									if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Single Day") {
										echo '<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Single" />';
									} else {
										echo '<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Multiple" />';
									}
									//=====================================================================================
									//===================== CHECK USER RIGHT FOR ADD UPDATE BUTTON ========================
									//=====================================================================================

									if (ValidateURths("TIMEOFF REQUEST" . "A") == true) {

										if ($EditID == "" || $EditID == '--') {
											$but_Caption = "Apply Now";
											$but_HasRth = ("TIMEOFF REQUEST" . "A");
											$but_Type = 'CustomNoSelect';
											include '../main/buttons.php';
										}
									}
									//else
									//{
									//	echo '<input name="SubmitTrans" disabled="disabled" type="submit" class="smallButton_disabled" id="SubmitTrans" value="Update Record" />';
									//}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ">
						<!-- ++++++++++++++++++++++++++++++ BUTTON +++++++++++++++++++++++++++++++-->
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="AUTHORIZE" />
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn btn-danger btn-sm" value="UNAUTHORIZE" />
					</div>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						//------------------------
						if (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "UNAUTHORIZE")) {
							print "<h4>UNAUTHORIZED</h4>";

							$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,(select LType from LvSettings where HashKey=LvD.LID) LType, * from LvDetails LvD where (Status='N' or Status='U' or Status='PC' or Status='PA' or Status='R')  and [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) order by [SDate] desc, AddedDate desc");
						} elseif (isset($_POST['SubmitTrans']) && ($_POST['SubmitTrans'] == "AUTHORIZE")) {
							print "<h4>AUTHORIZED</h4>";

							$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, (select LType from LvSettings where HashKey=LvD.LID) LType,* from LvDetails LvD where (Status='A') and [EmpID]=(select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )) order by [SDate] desc, AuthDate desc");
						} else {
							$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");
						}
						//------------------------
						?>
					</div>
					<div class="col-md-12">
						<div class="ViewPatch">
							<table width="100%" align="left" cellpadding="1" cellspacing="1" id="table">
								<thead>
									<tr>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Applied On</span></th>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Timeoff Type</span></th>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Start Date</span></th>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">End Date</span></th>
										<th align="left" valign="middle" scope="col"> No. of Day</th>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Backup Employee</span></th>
										<th align="left" valign="middle" scope="col"><span class="TinyTextTightBold">Status</span></th>
									</tr>
								</thead>
								<tbody>
									<?php
									if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] <> '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] <> '')) {
										$SearchDt = " and SDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "' or EDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "'";
									} else {
										$SearchDt = '';
									}
									$Del = 0;

									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										$Del = $Del + 1;
									?>
										<tr>
											<td align="left" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
											<td align="left" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['LType'])); ?></td>
											<td align="left" valign="middle" class="TinyText" scope="col">&nbsp; <?php echo $row2['SDt']; ?></td>
											<td align="left" valign="middle" class="TinyText" scope="col"><?php echo $row2['EDt']; ?>
											</td>
											<td align="left" valign="middle" class="TinyText" scope="col"><?php
																											$Script = "select Count(*) Ct from [LvIndTOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C','R')";
																											echo ScriptRunner($Script, "Ct"); ?></td>
											<td align="left" valign="middle" class="TinyText" scope="col"><?php
																											$Script = "select Count(*) as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
																											if (ScriptRunner($Script, "Ct") > 0) {
																												$Script = "select ([SName] +' '+ [FName]) as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
																												echo ScriptRunner($Script, "Ct");
																											} ?></td>
											<td align="left" valign="middle" scope="col" class="TinyText">&nbsp;
												<?php
												if ($row2['Status'] == "PA") {
													echo "Pending";
												} elseif ($row2['Status'] == "C") {
													echo "Cancelled";
												} elseif ($row2['Status'] == "R") {
													echo "Rejected";
												} elseif ($row2['Status'] == "A") {
													echo "Approved";
												} elseif ($row2['Status'] == "PC") {
													echo "Pending";
												}
												?></td>
										</tr>
									<?php }
									include '../login/dbClose2.php'; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6 pt-5">
						<?php include '../main/pagination.php'; ?>
					</div>
					<div class="col-md-6 pt-5">
						<?php
						//Set FAction Hidden Value for each button

						echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
				  	    	      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
				  	    		  <input name="FAction" id="FAction" type="hidden" />
				  	    		  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
						/*
				  	    	//Check if any record was spolled at all before enabling buttons
				  	    	//Check if user has Delete rights
				  	    		$but_HasRth=("TIMEOFF REQUEST"."D"); $but_Type = 'Cancel'; include '../main/buttons.php';
				  	    	//Check if user has Add/Edit rights
				  	    		$but_HasRth=("TIMEOFF ** REQUEST"."A"); $but_Type = 'View'; include '../main/buttons.php';
				  	    	//Check if user has Authorization rights
				  	    		$but_HasRth=("TIMEOFF ** REQUEST"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
				  	    	*/
						?>
					</div>
				</div>
			</div>
		</div>
	</form>

	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>


	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>

	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>

	<script type="text/javascript">
		const holidays = <?php echo json_encode($disabledDays) ?>;
		setDatePicker();

		function getEarliestAndMax(e) {
			const hashkey = e.value;
			if (hashkey == '--') {
				setDatePicker();
				return;
			}
			$.post({
				url: 'getEarliestAndMaxAjax.php',
				data: {
					hashkey,
					PgDoS: '<?php echo DoSFormToken() ?>'
				},
				cache: false,
				success(res) {
					console.log(res.earliest);
					setDatePicker(res.earliest);
				},
				error(e) {
					console.log(e);
				}
			})
		}

		function setDatePicker(min = 0) {
			$("#StartDt").datepicker("destroy");
			$("#EndDt").datepicker("destroy");
			$("#StartDt").datepicker({
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: min + "D",
				maxDate: "+12M",
				dateFormat: 'dd MM yy',
				beforeShowDay: function(date) {
					var day = date.getDay();
					var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
					return [<?php echo $SelID ?> && holidays.indexOf(string) == -1]
				},
			});

			$("#EndDt").datepicker({
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: min + "D",
				maxDate: "+12M",
				dateFormat: 'dd MM yy',
				beforeShowDay: function(date) {
					var day = date.getDay();
					var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
					return [<?php echo $SelID ?> && holidays.indexOf(string) == -1]
				},
			});
		}
	</script>
</body>
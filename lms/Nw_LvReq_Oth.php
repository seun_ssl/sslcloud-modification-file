<?php
session_start();
include '../login/scriptrunner.php';
//==========================================================================
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';
//Validate user viewing rights
if (ValidateURths("REQUEST FOR ALL" . "V") != true) {
    include '../main/NoAccess.php';
    exit;
}
$ModMaster = ValidateURths("LEAVE MODULE MASTERS" . "A"); // MODULE MASTERS RIGHTS SETTINGS
$GoValidate = true;
//==========================================================================
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
if (!isset($EditID)) {
    $EditID = "--";
}
if (!isset($HashKey)) {
    $HashKey = "";
}
if (!isset($StDate)) {
    $StDate = "";
}
if (!isset($EdDate)) {
    $EdDate = "";
}

if (!isset($StartDate)) {
    $StartDate = "";
}
if (!isset($EndDate)) {
    $EndDate = "";
}
if (!isset($AcctNo)) {
    $AcctNo = "";
}

$AllEmployee = "";
$LvGrp = "";
$Accrued_Months = "";
$Display = "";
$RetVal = "";
$EmpID = "";

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    //THIS PART OF THE CODE MUST BE ON TOP.
    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open" && isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32) {
        // var_dump("here");
        // die();
        if (ValidateURths("REQUEST FOR ALL" . "V") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        // include 'add_lms_validation_Oth.php';
        $EmpID = ECh($_REQUEST["AcctNo"]);
        $Script = "Select Count(*) Ct from Users where EmpID=(Select top 1 EmpID from EmpTbl where HashKey='" . $EmpID . "')";
        if (ScriptRunner($Script, "Ct") == 0) {
            $Script = "Select EmpLeaveGp from EmpTbl where HashKey='" . $EmpID . "'";
            $LvGrp = ScriptRunner($Script, "EmpLeaveGp");
            //Blessing, Joseph Unhang
        } else {
            $script = "Select EmpLeaveGp, EmpID from EmpTbl where HashKey='" . $EmpID . "'";
            $EmpolyeeID = ScriptRunner($script, "EmpID");
            $LvGrp = ScriptRunner($script, "EmpLeaveGp");
            $script = "Select GpName from Users where EmpID='" . $EmpolyeeID . "'";
            $GpName = ScriptRunner($script, "GpName");

            $script = "Select GpLeave from UGpRights where GpType='P' and HashKey='" . $GpName . "'";
            //$LvGrp = ScriptRunner($script,"GpLeave");
            //exit('stop 2 '.$EmpID);
            //$Script = "Select GpLeave from UGpRights where GpType='P' and UGpRights.HashKey=(Select GpName from Users where EmpID=(Select EmpID from EmpTbl where HashKey='".$EmpID."'))";
            //$Script = "Select GpLeave from UGpRights where GpType='P' and HashKey=(Select GpName from Users where EmpID='WSty10065')";
            //$Script = "Select GpLeave from UGpRights where GpType='P' and HashKey='7258e7251413465e0a3eb58094430bde'";
            //$Script = "Select GpLeave from UGpRights where UGpRights.GpType='P' and UGpRights.HashKey=(Select GpName from Users where Users.EmpID=(Select EmpID from EmpTbl where EmpTbl.HashKey='".$EmpID."'))";
            //$LvGrp = ScriptRunner($Script,"GpLeave");
            //Blessing, Joseph Unhang
        }
        if ($LvGrp == "") {
            echo ("<script type='text/javascript'>{ parent.msgbox('User Has No Leave Group Configured', 'red'); }</script>");
        }
    }
    //if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Apply Now Selected" && ($_REQUEST["SMDays_H"]=="Single" || $_REQUEST["SMDays_H"]=="Multiple"))
    //{
    //if($_REQUEST["posted"]=="posted"){
    if (isset($_REQUEST["LID"]) && $_REQUEST["LID"] != "--") {
        if (ValidateURths("REQUEST FOR ALL" . "A") != true) {
            include '../main/NoAccess.php';
            exit;
        }

        // var_dump($_POST);die();
        include 'add_lms_validation_Oth.php';
        if ($GoValidate == true) {
            /* If a document is attached to the updated record then call the file upload function */
            if ($_FILES["dmsupd"]["name"] != "") {
                include '../dms/stf_upload_file.php';
                //Upload file to DMS
                //$RetVal = UploadDoc("Add","dmsupd","","",ScriptRunner($Script,"Nm")." - ".ECh($_REQUEST["InstitutionType"])." ".ECh($_REQUEST["Designation"])." ".ECh($_REQUEST["DegreeType"]),"","L","");
                $Script = "Select (SName+' '+FName+' '+Convert(Varchar(18),EmpID)) Nm from EmpTbl where HashKey='" . $_SESSION["StkTck" . "HKey"] . "'";
                $RetVal = UploadDoc("Add", "dmsupd", "", "", ScriptRunner($Script, "Nm") . " - " . ECh($_REQUEST["LID"]) . "[" . $StartDate . " to " . $EndDate . "]", "", "L", "");
                if (strlen($RetVal) == 32) {
                    $Err = "";
                } else {
                    $Err = "<font color=red> Unable to attach file.</font>";
                }
            }
            //$Script ="Select max(id) Ct from LvDetails";
            //$HashKey = md5(ScriptRunner($Script, "Ct")."NewLeaveRequest");
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff" . $_SESSION["StkTck" . "UName"];
            $HashKey = md5($UniqueKey);
            //Get Employee ID Hash
            //$Script="select HashKey from EmpTbl where EmpID=(select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')";
            $EmpID = ECh($_REQUEST["AcctNo_"]);
            $EditID = $EmpID; //Don't remove else the Apply now button changes to UPDATE RECORD
            $Script = "INSERT INTO [LvDetails]  ([EmpID],[LID],[RDays],[Subst],[ItemDocHID],[SDate],[EDate],[EComment],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . ECh($EmpID) . "','" . ECh($_REQUEST["LID"]) . "'," . $DaysLeft . ",'" . ECh($_REQUEST["Subst"]) . "','" . $RetVal . "','" . $StartDate . "','" . $EndDate . "','" . ECh(trim($_REQUEST["GpDesc"])) . "','PA','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "')";
            ScriptRunnerUD($Script, "Inst");
            ////////////////////////////////-=============================================////////////////////////////////
            $Script = "Update LvIndTOff set status='D' where status<>'A' and Status<>'D' and HashKey='" . $HashKey . "'";
            $dbOpen2 = ($Script_Edit);
            include '../login/dbOpen2.php';
            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                //$Script ="Select max(id) Ct from LvIndTOff";
                //$New_HashKey = md5(ScriptRunner($Script, "Ct")."NewIndvLeaveRequest");
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff_Indv" . $_SESSION["StkTck" . "UName"] . $EmpID;
                $New_HashKey = md5($UniqueKey);
                $Script = "INSERT INTO LvIndTOff  ([EmpID],[LID],[LvDate],[LType],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . $EmpID . "','" . $HashKey . "','" . $row2['LvDate_'] . "','" . ECh($_REQUEST["LID"]) . "','PA','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $New_HashKey . "')";
                ScriptRunnerUD($Script, "Inst");
            }
            include '../login/dbClose2.php';
            ////////////////////////////////-=============================================////////////////////////////////
            $backup_name = ScriptRunner("Select (FName+' '+SName) Nm from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Nm");
            $backup_name = isset($backup_name) ? $backup_name : 'Not Selected';
            $Script = "Select SName, ONames, FName from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey='" . ECh($_REQUEST["AcctNo"]) . "'";
            $sname = ScriptRunner($Script, "SName");
            $oname = ScriptRunner($Script, "ONames");
            $fname = ScriptRunner($Script, "FName");
            $for_name = "$sname $oname $fname";

            $Script = "Select MMsg, MSub from MailTemp where MSub='New Time-off Request Pending Approval'"; //MSub='User Account Login'";
            $Script_Edit = "Select * from EmpTbl where HashKey='" . $EmpID . "'";
            $EmpLeaveGp = ScriptRunner($Script_Edit, "EmpLeaveGp");

            $LeaveScript2 = "Select * from LvSettings where HashKey='" . ECh($_REQUEST["LID"]) . "'";
            $LeaveType2 = ScriptRunner($LeaveScript2, "LType");

            $LoginDet = ScriptRunner($Script, "MMsg");
            $LoginDet = ScriptRunner($Script, "MMsg");
            $s_name = ScriptRunner($Script_Edit, "SName");
            $o_name = ScriptRunner($Script_Edit, "ONames");
            $f_name = ScriptRunner($Script_Edit, "FName");

            $full_name = "$f_name $o_name $s_name";
            $LoginDet = str_replace('#Name#', $full_name, $LoginDet);
            $LoginDet = str_replace('#DaysCount#', $BookDays, $LoginDet);
            $LoginDet = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet);
            $LoginDet = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet);
            $LoginDet = str_replace('#TimeOffType#', $LeaveType2, $LoginDet);
            $LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
            $LoginDet = str_replace('#StartDate#', $StartDate, $LoginDet);
            $LoginDet = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet);
            $LoginDet = str_replace('#DaysLeft#', $DaysLeft, $LoginDet);
            $LoginDet = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet);
            $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);
            $LoginDet = str_replace('#Backup#', $backup_name, $LoginDet);
            $LoginDet = str_replace('#for#', $for_name, $LoginDet);
            /* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */
            $ToMgr = ScriptRunner("Select Email from EmpTbl where HashKey='" . ScriptRunner($Script_Edit, "EmpMgr") . "'", "Email");
            $ToEmp = ScriptRunner($Script_Edit, "Email");
            $Bdy = $LoginDet;
            $Subj = ScriptRunner($Script, "MSub");
            $Atth = "";
            //if (strlen($ToEmp)>6) {Send Mail to employee}
            $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
					VALUES('" . $Subj . "','" . $ToEmp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
            ScriptRunnerUD($Script_Mail, "SendLogin");
            //Send mail to manager +++++++++++++++++++++++++++++++++++++++++++++++++++
            $Script = "Select MMsg, MSub from MailTemp where MSub='New Time-off Request Pending Approval'";
            $Script_Edit = "Select * from EmpTbl where HashKey='" . $EmpID . "'";
            $kk = ScriptRunner($Script_Edit, "EmpMgr");
            $mgr_name_query = "Select HashKey, (FName+' '+SName) as Nm, * from [EmpTbl] where HashKey='" . $kk . "'";
            $mgr_name = ScriptRunner($mgr_name_query, "Nm");
            $LoginDet5 = ScriptRunner($Script, "MMsg");
            $LoginDet5 = str_replace('#Name#', $mgr_name, $LoginDet5);
            $LoginDet5 = str_replace('#DaysCount#', $BookDays, $LoginDet5);
            //$LoginDet5=str_replace('#RequestType#',$_REQUEST["SMDays_H"],$LoginDet5);
            $LoginDet5 = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet5);
            $LoginDet5 = str_replace('#TimeOffType#', $LeaveType2, $LoginDet5);
            $LoginDet5 = str_replace('#StartDate#', $StartDate, $LoginDet5);
            $LoginDet5 = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet5);
            $LoginDet5 = str_replace('#DaysLeft#', $DaysLeft, $LoginDet5);
            $LoginDet5 = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet5);
            $LoginDet5 = str_replace('#Backup#', $backup_name, $LoginDet5);
            $LoginDet5 = str_replace('#for#', $for_name, $LoginDet5);
            $Bdy = $LoginDet5;
            //Send mail to manager
            $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $ToMgr . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToMgr . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
            ScriptRunnerUD($Script_Mail, "SendLogin");
            if (strlen($_REQUEST["Subst"]) == 32) {
                $Script = "Select MMsg, MSub from MailTemp where HashKey='ab9e9daa406bbfb1656cdb8254ce2343'"; //MSub='BackUp'";
                //$Script_Edit="Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')";
                $Script_Edit = "Select * from EmpTbl where HashKey='" . $EmpID . "'";
                if (strtolower(trim(ScriptRunner($Script_Edit, "Sex"))) == "male") {
                    $Sex = " his ";
                } else {
                    $Sex = " her ";
                }
                $LoginDet = ScriptRunner($Script, "MMsg");
                $LoginDet = ScriptRunner($Script, "MMsg");
                $s_name = ScriptRunner($Script_Edit, "SName");
                $o_name = ScriptRunner($Script_Edit, "ONames");
                $f_name = ScriptRunner($Script_Edit, "FName");
                $full_name = "$f_name $o_name $s_name";
                $LoginDet = str_replace('#Applicant#', $full_name, $LoginDet);
                $LoginDet = str_replace('#Backup#', $backup_name, $LoginDet);
                $LoginDet = str_replace('#EmpSex#', $Sex, $LoginDet);
                $LoginDet = str_replace('#RequestType#', $_REQUEST["SMDays_H"], $LoginDet);
                $LoginDet = str_replace('#DaysCount#', $BookDays, $LoginDet);
                //$LoginDet=str_replace('#RequestType#',$_REQUEST["SMDays_H"],$LoginDet);
                $LoginDet = str_replace('#Request#', $_SESSION["StkTck" . "FName"], $LoginDet);
                $LoginDet = str_replace('#EmpName#', $_SESSION["StkTck" . "FName"], $LoginDet);
                $LoginDet = str_replace('#TimeOffType#', $LeaveType2, $LoginDet);
                $LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
                $LoginDet = str_replace('#StartDate#', $StartDate, $LoginDet);
                $LoginDet = str_replace('#EndDate#', ' to ' . $EndDate, $LoginDet);
                $LoginDet = str_replace('#DaysLeft#', $DaysLeft, $LoginDet);
                $LoginDet = str_replace('#EmpNote#', ECh(trim($_REQUEST["GpDesc"])), $LoginDet);
                $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);
                $ToBackUp = ScriptRunner("Select Email from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Email");
                $Bdy = $LoginDet;
                $Subj = ScriptRunner($Script, "MSub");
                $Atth = "";
                //Send to backup
                $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
					VALUES('" . $Subj . "','" . $ToBackUp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToBackUp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
                ScriptRunnerUD($Script_Mail, "SendLogin");
            }
            //++++++++++++++++++++++++++++++Team Request Notification+++++++++++++++++++++++++++++
            include "mail_trail_send.php";
            include "mail_leave_group.php";
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            AuditLog("INSERT", "New " . $_REQUEST["LID"] . " timeoff request booked for " . $_SESSION["StkTck" . "UName"], $New_HashKey);
            echo ("<script type='text/javascript'>{ parent.msgbox('New timeoff request booked successfully.', 'green'); }</script>");
        }
    }
    //**************************************************************************************************
    if ($_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("REQUEST FOR ALL" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script = "Select Status from [LvDetails] WHERE [HashKey] = '" . $EditID . "'";
                $SelID = ScriptRunner($Script, "Status");
                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "PA" || $SelID == "N" || $SelID == "U") {
                    $SelID = "A";
                } elseif ($SelID == "PC") {
                    $SelID = "C";
                }
                //Move on to Cancel leave request or seek for leave cancellation approval
                $Script = "UPDATE [LvDetails]
					SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					,[Status] = '" . $SelID . "'
					WHERE [HashKey]='" . $EditID . "'";
                ScriptRunnerUD($Script, "CA");
                //echo $Script;
                $Script = "UPDATE [LvIndTOff]
					SET [AuthBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
					,[AuthDate] = GetDate()
					,[Status] = '" . $SelID . "'
					WHERE [LID]='" . $EditID . "'
					and Status not in ('C','A','R')";
                ScriptRunnerUD($Script, "CA");
                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "A") {
                    AuditLog("AUTHORIZE", "Employee leave sucessfully authorized by [" . $_SESSION["StkTck" . "UName"] . "].", $EditID);
                    //Get employee manager to send cancellation email to for approval
                    //$Script="Select [Email] from EmpTbl where HashKey=(select EmpID from [LvIndTOff] where HashKey='".$EditID."')";
                    //MailTrail("TIMEOFF APPROVED","EMP",'','',ScriptRunner($Script,"Email")); //Sends a mail notifications
                    //MailTrail("TIMEOFF APPROVED","MGR",'','',$_SESSION["StkTck"."Email"]); //Sends a mail notifications
                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) authorized successfully.', 'green'); }</script>");
                } elseif ($SelID == "C") {
                    AuditLog("AUTHORIZE", "Employee leave sucessfully cancelled by [" . $_SESSION["StkTck" . "UName"] . "].", $EditID);
                    //Get employee manager to send cancellation email to for approval
                    //$Script="Select [Email] from EmpTbl where HashKey=(select EmpID from [LvIndTOff] where HashKey='".$EditID."'";
                    //MailTrail("TIMEOFF CANCELLED","EMP",'','',ScriptRunner($Script,"Email")); //Sends a mail notifications
                    //MailTrail("TIMEOFF CANCELLED","MGR",'','',$_SESSION["StkTck"."Email"]); //Sends a mail notifications

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) rejected successfully.', 'green'); }</script>");
                }
                ////////////////////////////////-=============================================////////////////////////////////
                $Script_FNotify = "Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
                $Script_Det = "Select * from [LvDetails] WHERE [HashKey]='" . $EditID . "'"; // Get details for the leave application
                $Script = "Select MMsg, MSub from MailTemp where HashKey='666fdca926ed30105d0eee6ccdc160e5'"; //MSub='TimeOff Approved'";
                $Script_Edit = "Select *, (FName+' '+SName) Nm from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
                $Script_Backup = "Select MMsg, MSub from MailTemp where HashKey='666fdca926ed3010730eee6ccdc160e5'"; //MSub='BackUp'";
                //Set the message to send to Manager and Employee
                $Script_Edit = "Select * from EmpTbl where EmpID=(select EmpID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "' and Status in('A') )";
                if (strtolower(trim(ScriptRunner($Script_Edit, "Sex"))) == "male") {
                    $Sex = " his ";
                } else {
                    $Sex = " her ";
                }
                //Set the message to send to Manager and Employee
                $LoginDet = ScriptRunner($Script, "MMsg");
                $LoginDet_Backup = ScriptRunner($Script_Backup, "MMsg");
                $MsgVal = ScriptRunner($Script_Edit, "Nm");
                $LoginDet = str_replace('#MrgName#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#MrgName#', $MsgVal, $LoginDet_Backup);
                $MsgVal = ScriptRunner("Select (FName+' '+SName) Nm from EmpTbl where HashKey=(Select EmpID from [LvDetails] WHERE [HashKey]='" . $EditID . "')", "Nm");
                $LoginDet = str_replace('#EmpName#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#EmpName#', $MsgVal, $LoginDet_Backup);
                $MsgVal = ScriptRunner("Select Count(*) Ct from LvIndTOff where Status='A' and LID='" . $EditID . "'", "Ct");
                $LoginDet = str_replace('#DaysCount#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#DaysCount#', $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "LID");
                $LoginDet = str_replace('#TimeOffType#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#TimeOffType#', $MsgVal, $LoginDet);
                $LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
                $LoginDet_Backup = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "SDate");
                $LoginDet = str_replace('#StartDate#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#StartDate#', $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "EDate");
                $LoginDet = str_replace('#EndDate#', ' to ' . $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#EndDate#', ' to ' . $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "RDays");
                $LoginDet = str_replace('#DaysLeft#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#DaysLeft#', $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "EComment");
                $LoginDet = str_replace('#EmpNote#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#EmpNote#', $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "MComment");
                $LoginDet = str_replace('#MgrNote#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#MgrNote#', $MsgVal, $LoginDet);
                $MsgVal = ScriptRunner($Script_Det, "HRComment");
                $LoginDet = str_replace('#HRNote#', $MsgVal, $LoginDet);
                $LoginDet_Backup = str_replace('#HRNote#', $MsgVal, $LoginDet);
                $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);
                $LoginDet_Backup = str_replace('#Link#', $_SERVER['HTTP_HOST'] . '/SSLCloud', $LoginDet);
                /* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */
                $ToMgr = ScriptRunner($Script_Edit, "Email");
                //echo $Script_Edit; //=======================================================
                $ToEmp = ScriptRunner("Select Email from EmpTbl where HashKey='" . ScriptRunner($Script_Det, "EmpID") . "'", "Email");
                $Bdy = $LoginDet;
                $Subj = ScriptRunner($Script, "MSub");
                $Atth = "";
                //Send Mail to employee
                $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
					VALUES('" . $Subj . "','" . $ToEmp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToEmp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
                //echo $Script_Mail;
                ScriptRunnerUD($Script_Mail, "SendLogin");
                //**************************************************************
                //Send mail to manager
                $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
					VALUES('" . $Subj . "','" . $ToMgr . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToMgr . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
                //echo $Script_Mail;
                ScriptRunnerUD($Script_Mail, "SendLogin");
                //**************************************************************
                //Send mail to FNotify 1 if SET
                if (ScriptRunner() == 32) {
                    $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
						VALUES('" . $Subj . "','" . $ToMgr . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToMgr . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
                    ScriptRunnerUD($Script_Mail, "SendLogin");
                }
                if (strlen($_REQUEST["Subst"]) == 32) {
                    $ToBacUp = ScriptRunner($Script_Edit, "Email");
                    $Subj_Backup = ScriptRunner($Script, "MSub");
                    $ToBackUp = ScriptRunner("Select Email from EmpTbl where HashKey='" . $_REQUEST["Subst"] . "'", "Email");
                    $Bdy = $LoginDet;
                    $Subj = ScriptRunner($Script, "MSub");
                    $Atth = "";
                    //Send Mail to employee
                    $Script_Mail = "INSERT INTO [dbo].[MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
						VALUES('" . $Subj . "','" . $ToBackUp . "','" . ECh($Bdy) . "','" . $Atth . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),0,'" . md5($Subj . $ToBackUp . $Bdy . $_SESSION["StkTck" . "UName"]) . "')";
                    ScriptRunnerUD($Script_Mail, "SendLogin");
                    //**************************************************************
                }
                if ($g == 2) {
                    $Script = "Select MMsg, MSub from MailTemp where MSub='ShareFile'";
                    $Bdy = ScriptRunner($Script, "MMsg");
                    $Subj = ScriptRunner($Script, "MSub");
                    $Script = "Select Max(ID) as MaxID from DocShare";
                    $PubShare = md5(ScriptRunner($Script, "MaxID") . "DocShare");
                    $To = trim($Recipients[$i]);
                    $ToCryp = md5(strtoupper($To) . $PubShare); //User email and the hashkey value for this specific share
                    $Bdy = str_replace('#UserName#', $_SESSION["StkTck" . "FName"], $Bdy);
                    $Bdy = str_replace('#Link1#', $_SERVER['HTTP_HOST'] . '/SSLCloud/Public?S=T&Em=' . $ToCryp . '&Vw=Y&Dwloadble=Y&CID=' . $_SESSION["StkTck" . "CustID"] . '&SFSearch=' . $PubShare, $Bdy);
                    $Bdy = str_replace('#Title#', ' <b>' . $_REQUEST["FShName"] . '</b> ', $Bdy);
                    $Atth = "";
                    $Script_Mail = "INSERT INTO [DocShare]([FName],[ShareHash],[CID],[AuthEmail],[SDur],[Status],[AddedBy],[AddedDate],[HashKey])
						VALUES('" . $_REQUEST["FShName"] . "','" . $_REQUEST["ShareFile"] . "','" . $_SESSION["StkTck" . "CustID"] . "','" . $ToCryp . "','" . $_REQUEST["SDur"] . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $PubShare . "')";
                    ScriptRunnerUD($Script_Mail, "DocShare");
                }
            }
        }
    }

    if ($_REQUEST["FAction"] == "Reject Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("REQUEST FOR ALL" . "T") != true) {
            include '../main/NoAccess.php';
            exit;
        }
        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) {
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                $Script = "Select Status from [LvDetails] WHERE [HashKey] = '" . $EditID . "'";
                $SelID = ScriptRunner($Script, "Status");
                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                if ($SelID == "PA" || $SelID == "N" || $SelID == "U") {
                    $SelID = "R";
                } elseif ($SelID == "PC") {
                    $SelID = "R";
                }
                //Move on to Cancel leave request or seek for leave cancellation approval
                $Script = "UPDATE [LvDetails]
					SET [ApprovedBy] =  '" . $_SESSION["StkTck" . "UName"] . "'
					,[ApprovedDate] = GetDate()
					,[Status] = '" . $SelID . "'
					WHERE [HashKey]='" . $EditID . "'";
                ScriptRunner($Script, "R");
                //echo $Script;
                $Script = "UPDATE [LvIndTOff]
					SET [ApprovedBy] =  '" . $_SESSION["StkTck" . "UName"] . "'
					,[ApprovedDate] = GetDate()
					,[Status] = '" . $SelID . "'
					WHERE [LID]='" . $EditID . "'
					and Status not in ('C','A')";
                ScriptRunner($Script, "R");
                //If PendingAuthorization (PA) cancel leave at once without seeking approval
                AuditLog("AUTHORIZE", "Employee timeoff rejected for [" . $_SESSION["StkTck" . "UName"] . "].", $EditID);
                //Get employee manager to send cancellation email to for approval
                $Script = "Select [Email] from EmpTbl where HashKey=(select EmpID from [LvIndTOff] where HashKey='" . $EditID . "'";
                MailTrail("TIMEOFF REJECTED", "EMP", '', '', ScriptRunner($Script, "Email")); //Sends a mail notifications
                MailTrail("TIMEOFF REJECTED", "MGR", '', '', $_SESSION["StkTck" . "Email"]); //Sends a mail notifications
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) rejected successfully.', 'green'); }</script>");
            }
        }
    }
}

?>

<script>
    <?php
    $icount = 0;
    $SelID = '';
    $PubHols = '{}';
    //GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR
    $Script = "SELECT COUNT(*) Ct FROM [LvIndTOff] WHERE [EmpID]='" . $EmpID . "' AND Status NOT IN('D','C','R')";
    if (ScriptRunner($Script, "Ct") > 0) {
        $got_val = "got val";
        $dbOpen2 = ("SELECT Status, '' AS DtDiff, PName AS PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,PDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,PDate))) AS PubHol, (CONVERT(VARCHAR(4),DATEPART(M,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))) AS Dt FROM [LvPHolsOff] where Status='A' UNION SELECT Status, DateDiff(D,GetDate(),LvDate) DtDiff, (SELECT LType FROM LvSettings where HashKey=LiO.LType) AS PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,LvDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,LvDate))) AS PubHol, (CONVERT(VARCHAR(4),DATEPART(M,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))) AS Dt FROM [LvIndTOff] LiO WHERE [EmpID]='" . $EmpID . "' AND Status NOT IN('D','C','R') ORDER BY Dt DESC"); //LvDate>=GetDate() and
    } else {
        $got_val = "NO val";
        $dbOpen2 = ("SELECT Status, DateDiff(D,GetDate(),LvDate) DtDiff, (SELECT LType FROM LvSettings WHERE HashKey=LiO.LType) AS PDes, (CONVERT(VARCHAR(4), DATEPART(YEAR,LvDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,LvDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,LvDate))) AS PubHol, (CONVERT(VARCHAR(4),DATEPART(M,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,LvDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,LvDate))) AS Dt FROM [LvIndTOff] LiO"); //LvDate>=GetDate() and
    }
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        //["3-21-2014","3-28-2014"];   DEACTIVATE SPECIFIC DATES ON THE CALENDAR
        $icount = $icount + 1;
        if ($row2['Status'] == "A") {
            $ClsSet = ", className: ['smallButton_Auth']";
        } else {
            $ClsSet = ", className: ['smallButton']";
        }
        if ($row2['DtDiff'] < 0) {
            $ClsSet = ", className: ['smallButton_disabled']";
        }
        if ($icount > 1) {
            $SelID = $SelID . ',"' . $row2['Dt'] . '"';
            $PubHols = $PubHols . ", {title: '" . $row2['PDes'] . "', url:'lms/Nw_LvCanl.php?', start: new Date(" . $row2['PubHol'] . "),end: new Date(" . $row2['PubHol'] . "), allDay: true " . $ClsSet . " }";
        } else {
            //if ($row2['Dt'] is less than today or the emp id is not mine change ///// $EvtColor= "className: ['smallButton']"
            $SelID = '"' . $row2['Dt'] . '"';
            $PubHols = " {title: '" . $row2['PDes'] . "', url:'lms/Nw_LvCanl.php?', start: new Date(" . $row2['PubHol'] . "),end: new Date(" . $row2['PubHol'] . "), allDay: true " . $ClsSet . " }";
        }
    }
    include '../login/dbClose2.php';
    ?>
    /* create an array of days which need to be disabled */
    var disabledDays = [<?php echo $SelID; ?>];

    /* utility functions */
    function nationalDays(date) {
        var m = date.getMonth(),
            d = date.getDate(),
            y = date.getFullYear();
        //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
        for (i = 0; i < disabledDays.length; i++) {
            if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1 || new Date() > date) {
                //console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
                return [false];
            }
        }
        //console.log('good:  ' + (m+1) + '-' + d + '-' + y);
        return [true];
    }

    function noWeekendsOrHolidays(date) {
        var noWeekend = jQuery.datepicker.noWeekends(date);
        return noWeekend[0] ? nationalDays(date) : noWeekend;
    }

    $(function() {
        $('#datepicker').datepicker({
            showButtonPanel: true,
            dateFormat: "mm/dd/yy",
            //    beforeShow: function(){
        });

        <?php //GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR
        $icount = 0;
        $dbOpen2 = ("
SELECT DeactivateDay =
      CASE Val1
         WHEN 'Sunday' THEN 'day != 0'
         WHEN 'Monday' THEN 'day != 1'
         WHEN 'Tuesday' THEN 'day != 2'
         WHEN 'Wednesday' THEN 'day != 3'
         WHEN 'Thursday' THEN 'day != 4'
         WHEN 'Friday' THEN 'day != 5'
         WHEN 'Saturday' THEN 'day != 6'
         ELSE ''
      END
from Masters where ItemName='Weekends'");
        include '../login/dbOpen2.php';
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) { //day != 6 && day != 0   DEACTIVATE DAY of THE WEEK ON CALENDAR
            $icount = $icount + 1;
            if ($icount > 1) {
                $SelID = $SelID . " && " . $row2['DeactivateDay'];
            } else {
                $SelID = $row2['DeactivateDay'];
            }
        }
        include '../login/dbClose2.php';

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $Script_Date = "Select Convert(Varchar(11),SetStart,106) LDate_Start, Convert(Varchar(11),SetEnd,106) LDate_End from [Settings] WHERE [Setting]= 'LeaveTyp'";
        $LDate_Start = ScriptRunner($Script_Date, "LDate_Start");
        $date1 = date_create($LDate_Start);
        $day = date_format($date1, 'd');
        $month = date_format($date1, 'm');
        $year = date_format($date1, 'Y');

        //$year = date_format($date1, 'Y-m-d');
        //2007, 1 - 1, 1
        $min_date = "$year, $month - 1, $day";
        ?>

    });
</script>
<script type="text/javascript">
    function setDatePicker(min = 0) {
        $("#StartDt").datepicker("destroy");
        $("#EndDt").datepicker("destroy");

        <?php
        $Script_Edit = "Select [SetValue16] from Settings where [Setting]= 'LeaveTyp'";
        $kk = ScriptRunner($Script_Edit, "SetValue16");

        if ((int) $kk !== 1): ?>

            $("#StartDt").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: min + "D",
                maxDate: "+12M",
                dateFormat: 'dd MM yy'
                <?php if (strlen($SelID) != '') {
                    echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
                } ?>
            })
            // $("#StartDt").datepicker(
            // 	{
            // 		changeMonth: true,
            // 		changeYear: true,
            // 		 showOtherMonths: true,
            // 		 selectOtherMonths: true,
            // 		 minDate: min+"D",
            // 		 maxDate: "+12M",
            // 		dateFormat: 'dd MM yy'

            // 	}
            // 	)


            $("#EndDt").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: min + "D",
                maxDate: "+12M",
                dateFormat: 'dd MM yy'
                <?php if (strlen($SelID) != '') {
                    echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
                } ?>
            })
            // $("#EndDt").datepicker(
            // 	{
            // 		changeMonth: true,
            // 		changeYear: true,
            // 		showOtherMonths: true,
            // 		selectOtherMonths: true,
            // 		minDate: min+"D",
            // 		maxDate: "+12M",
            // 		dateFormat: 'dd MM yy'

            // 	}
            // 	)

        <?php else: ?>
            $("#StartDt").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                //  minDate: min+"D",
                maxDate: "+12M",
                dateFormat: 'dd MM yy'
                <?php if (strlen($SelID) != '') {
                    echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
                } ?>
            })

            // $("#StartDt").datepicker(
            // {
            // 	changeMonth: true,
            // 	changeYear: true,
            // 	 showOtherMonths: true,
            // 	 selectOtherMonths: true,
            // 	//  minDate: min+"D",
            // 	 maxDate: "+12M",
            // 	dateFormat: 'dd MM yy'

            // }
            // )


            $("#EndDt").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                // minDate: min+"D",
                maxDate: "+12M",
                dateFormat: 'dd MM yy'
                <?php if (strlen($SelID) != '') {
                    echo ", beforeShowDay: function(date) {var day = date.getDay(); return [(" . $SelID . "),''];}";
                } ?>
            })

            // $("#EndDt").datepicker(
            // 	{
            // 		changeMonth: true,
            // 		changeYear: true,
            // 		showOtherMonths: true,
            // 		selectOtherMonths: true,
            // 		// minDate: min+"D",
            // 		maxDate: "+12M",
            // 		dateFormat: 'dd MM yy'

            // 	}
            // 	)


        <?php endif; ?>
    }
</script>




<link rel='stylesheet' type='text/css' href='../jqry/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='../jqry/fullcalendar/fullcalendar.min.js'></script>

<script type='text/javascript'>
    $(document).ready(function() {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            editable: false,
            events: 'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic',
            dayClick: function(date, allDay, jsEvent, view) {
                if (allDay) {
                    parent.msgbox(date + ' is free. You can book an off day here', 'green');
                    //alert('Clicked on the entire day: ' + date);
                } else {
                    parent.msgbox(date + ' is free. You can book an off day here', 'green');
                    //alert('Clicked on the slot: ' + date);
                }
            },

            eventClick: function(event) {
                // opens events in a popup window
                parent.ShowDisp('Cancel my leave', 'lms/Nw_LvCanl.php?c=d&LDate=' + event.start, 320, 600, 'Yes')
                //window.open(event.start, 'gcalevent', 'width=700,height=600');
                return false;
            },


            events: [
                /*{
                	title: 'All Day Event',
                	start: '2014,March,2' //new Date(y, m, 1)

                	title: 'Long Event',
                	start: new Date(y, m, d-5),
                	end: new Date(y, m, d-2)

                	or

                	allDay: false // No END date when using this.
                	url: 'http://google.com/' To use hyperlink
                },*/
                // {title: 'Christmas2',	url:'http://www.yahoo.com', start: new Date(14, 3, 25),end: new Date(14, 12, 25);}

                <?php echo $PubHols; ?>

            ],
            color: 'yellow', // an option!
            textColor: 'black' // an option!
        });
    });
</script>

<script>
    $(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(function() {
        $(document).tooltip();
    });

    function MM_swapImgRestore() { //v3.0
        var i, x, a = document.MM_sr;
        for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }

    function MM_preloadImages() { //v3.0
        var d = document;
        if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length,
                a = MM_preloadImages.arguments;
            for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) {
                    d.MM_p[j] = new Image;
                    d.MM_p[j++].src = a[i];
                }
        }
    }

    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }

    function MM_swapImage() { //v3.0
        var i, j = 0,
            x, a = MM_swapImage.arguments;
        document.MM_sr = new Array;
        for (i = 0; i < (a.length - 2); i += 3)
            if ((x = MM_findObj(a[i])) != null) {
                document.MM_sr[j++] = x;
                if (!x.oSrc) x.oSrc = x.src;
                x.src = a[i + 2];
            }
    }
</script>

<style type='text/css'>
    #calendar {
        width: 100%;
        margin: 0 auto;
    }
</style>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
    echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
    <link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
    strong {
        font-weight: 550;
        font-size: 12px;
    }
</style>

<body oncontextmenu="return false;">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
        <input type="hidden" name="posted" value="posted">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Book A TimeOff For Any Employee</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-7 col-md-5"> <label class=" col-form-label" style="font-weight: bolder; font-size: 14px;margin-top: 4%">Select an Employee:</label></div>
                            <div class="input-group input-group-sm col-10 col-md-7  pull-right" style="margin-top: 2%">
                                <select class="form-control" name="AcctNo" id="AcctNo">
                                    <?php
                                    if (isset($_REQUEST["AcctNo"])) {
                                        $SelID = $_REQUEST["AcctNo"];
                                    }
                                    echo '<option value="--" selected="selected">--</option>';
                                    $dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' order by SName Asc");
                                    include '../login/dbOpen2.php';
                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                        if ($SelID == $row2['HashKey']) {
                                            $AllEmployee = $AllEmployee . '<option selected value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                        } else {
                                            $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                        }
                                    }
                                    echo $AllEmployee;
                                    include '../login/dbClose2.php';
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if (strlen($EmpID) != 32) {
                echo '
					<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
					<input name="FAction" id="FAction" type="hidden" />
					<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
					</form>';
                exit;
            }
            ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a class="active" href="#tabs-1" data-toggle="tab">My Calendar</a>
                                </li>
                                <li>
                                    <a href="#tabs-2" data-toggle="tab">Leave Details</a>
                                </li>
                                <li>
                                    <a href="#tabs-3" data-toggle="tab">Unauthorized</a>
                                </li>
                                <li>
                                    <a href="#tabs-4" data-toggle="tab">Authorized</a>
                                </li>
                            </ul>
                            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabs-1">
                                    <div class="row">
                                        <?php if ($got_val != "NO val"): ?>
                                            <div class="col-md-6">
                                                <div id='calendar'></div>
                                            </div>
                                        <?php endif ?>
                                        <p>
                                            <?php
                                            $Script = "Select HashKey from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey='" . ECh($_REQUEST["AcctNo"]) . "'";
                                            if (strlen(ScriptRunner($Script, "HashKey")) != 32) {
                                                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have an active manager tied to your employee record. Contact your HR', 'red'); }</script>");
                                                exit;
                                            }
                                            $Script = "Select Count(LType) Ct from LvSettings where Status='A' and LID='" . $LvGrp . "'";
                                            if (ScriptRunner($Script, "Ct") < 1) {
                                                echo ("<script type='text/javascript'>{parent.msgbox('The selected employee has no leave group defined. Contact your administrator', 'red');}</script>");
                                                exit;
                                            }
                                            $Script = "Select *, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";
                                            if (ScriptRunner($Script, "SetValue") == "Period") {
                                                $StDate = ScriptRunner($Script, "SYear");
                                                $EdDate = ScriptRunner($Script, "EYear");
                                            } else {
                                                $Script = "Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='" . $EmpID . "'";

                                                //Check what year the anniversary falls into [Last year to this year] or [This year to next year]
                                                $StDate = ScriptRunner($Script, "Dt") . ' ' . ScriptRunner($Script, "Yr");
                                                if (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
                                                    $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") + 1);
                                                } elseif (strtotime($StDate) >= strtotime(ScriptRunner($Script, "TDay"))) {
                                                    $StDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") - 1);
                                                    $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr"));
                                                }
                                            }

                                            $Script = "Select DATEDIFF(D,'" . $StDate . "',GETDATE()) Mts";
                                            $Accrued_Months = ScriptRunner($Script, "Mts"); //Used to divide to get Accrued days count - Accrued months is the start month minus the current month as an integer
                                            //AccrueD MONTH MUST BEGIN COUNTING ONLY AFTER YOU ARE SURE OF THE START MONTH EITHER FOR PERIOD BASED OR ANNIVERSARY BASED LEAVE TYPE.

                                            $Display = "";
                                            $dbOpen2 = ("Select LType, HashKey from LvSettings where Status='A' and LID='" . $LvGrp . "'");

                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                $Script = "select Accrue, NDays from LvSettings LvSet where LvSet.LID='" . $LvGrp . "' and LvSet.HashKey='" . $row2['HashKey'] . "' and Status<>'D'";
                                                $SetDays = ScriptRunner($Script, "NDays");

                                                $AccruType = ScriptRunner($Script, "Accrue");

                                                $Script = "(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D' and Status<>'R') and LType='" . $row2['HashKey'] . "' and ([LvDate] between ('" . $StDate . "')	and ('" . $EdDate . "') ) and EmpID='" . $EmpID . "')";
                                                $UsedDays = ScriptRunner($Script, "Ct");

                                                if ($AccruType == 1) {

                                                    if ($Display == "") {
                                                        $Display = $row2['LType'] . " <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
                                                    } //Setup Display
                                                    else {
                                                        $Display = $Display . ", " . $row2['LType'] . " <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
                                                    } //Setup Display
                                                } else {

                                                    if ($Display == "") {
                                                        $Display = $row2['LType'] . ":" . ($SetDays - $UsedDays);
                                                    } //Setup Display
                                                    else {
                                                        $Display = $Display . ", " . $row2['LType'] . ":" . ($SetDays - $UsedDays);
                                                    } //Setup Display
                                                }
                                            }
                                            include '../login/dbClose2.php';
                                            ?>
                                        </p>
                                        <?php $clascolumn = ($got_val != "NO val") ? 'col-md-6' : 'col-md-8'; ?>
                                        <div class="<?= $clascolumn ?>">
                                            <div class="box bg-lightest">
                                                <div class="box-header with-border text-left">
                                                    <div class="row ">
                                                        <div class="col-md-6">
                                                            <h5>Days Left:
                                                                <?php
                                                                //if (isset($_REQUEST["AcctNo"]))
                                                                //{
                                                                if (isset($EmpID) && strlen($EmpID) == 32) {
                                                                    $Script = "SELECT HashKey,(Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where Et.HashKey='" . $EmpID . "'";
                                                                    $EmpID = ScriptRunner($Script, "HashKey");
                                                                    //$EditID=$EmpID;
                                                                    echo '<input name="AcctNo_" id="AcctNo_" type="hidden" value="' . $EmpID . '" />';
                                                                    echo ScriptRunner($Script, "Nm");
                                                                }
                                                                ?>
                                                            </h5>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-default btn-xs pull-right" title="View my time-off detains" name="EditDashboard" id="EditDashboard" onclick="removeElement('DbName_TR');"><i class="fa fa-gears"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <strong>
                                                        <?php
                                                        /*
    $Display="";
    $dbOpen2 = ("Select LType, HashKey from LvSettings where Status='A' and LID='".$LvGrp."'");

    include '../login/dbOpen2.php';
    while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
    {
    $Script = "select Accrue, NDays from LvSettings LvSet where LvSet.LID='".$LvGrp."' and LvSet.HashKey='".$row2['HashKey']."' and Status<>'D'";
    $SetDays = ScriptRunner($Script,"NDays");

    $AccruType = ScriptRunner($Script,"Accrue");

    $Script="(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D') and LType='".$row2['HashKey']."' and ([LvDate] between ('".$StDate."')    and ('".$EdDate."') ) and EmpID='".$EmpID."')";
    $UsedDays = ScriptRunner($Script,"Ct");
    if ($AccruType==1)
    {
    if ($Display=="")
    {$Display=$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);} //Setup Display
    else
    {$Display=$Display.", ".$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);
    } //Setup Display
    }
    }
    include '../login/dbClose2.php';
     */

                                                        echo $Display;
                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>
                                            <?php
                                            //print $got_val;
                                            ?>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Leave Type:</label>
                                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                    <select class="form-control" name="LID" id="LID" onchange="getEarliestAndMax(this)">
                                                        <option value="--" selected="selected">--</option>
                                                        <?php
                                                        if (isset($EditID) && ($EditID != "" && $EditID != "--")) {
                                                            $SelID = ScriptRunner($Script_Edit, "LID");
                                                        }

                                                        $dbOpen2 = ("select LType, HashKey from LvSettings where LID='" . $LvGrp . "'");
                                                        include '../login/dbOpen2.php';
                                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                            if ($SelID == $row2['HashKey']) {
                                                                echo '<option selected value="' . $row2['HashKey'] . '">' . $row2['LType'] . '</option>';
                                                            } else {
                                                                echo '<option value="' . $row2['HashKey'] . '">' . $row2['LType'] . '</option>';
                                                            }
                                                        }
                                                        include '../login/dbClose2.php';
                                                        ?>
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default" title="Select a time-off type you wish to apply for" id="MastersEmpStatus">
                                                            <i class="fa fa-info"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <?php if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Single Day") { ?>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Select Date
                                                        <span style="color: red">*</span>:</label>
                                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                        <input name="StartDt" id="StartDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">From Date
                                                        <span style="color: red">*</span>:</label>
                                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                        <input name="StartDt" id="StartDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" style="padding-right: 0px;">To Date
                                                        <span style="color: red">*</span>:</label>
                                                    <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                        <input name="EndDt" id="EndDt" type="text" class="form-control" value="" size="16" maxlength="12" readonly />
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" style="padding-right: 0px;">Back up Employee
                                                    :</label>
                                                <div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
                                                    <select class="form-control" name="Subst" id="Subst">
                                                        <?php
                                                        $AllEmployee = '';
                                                        echo '<option value="--" selected="selected">--</option>';
                                                        $dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and EmpStatus='Active' and HashKey<>'" . $EditID . "' order by SName Asc");
                                                        include '../login/dbOpen2.php';
                                                        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                            $AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
                                                        }
                                                        echo $AllEmployee;
                                                        include '../login/dbClose2.php';

                                                        if ($EditID != '' && $EditID != '--') {
                                                            $Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
                                                            $empnm = ScriptRunner($Script_Edit, "Nm");
                                                            echo '<option selected="selected" value="' . $EditID . '">' . $empnm . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="input-group-btn ">
                                                        <button type="button" class="btn btn-default" title="Select an employee to work in your stead" id="MastersEmpStatus">
                                                            <i class="fa fa-info"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="exampleInputFile" class="col-sm-4 col-form-label">Upload Document:</label>
                                                <div class="col-sm-8 input-group-sm">
                                                    <input type="file" name="dmsupd" id="dmsupd" class="smallButton" width="100" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> Leave Description:</label>
                                                <div class="col-sm-8" style=" padding-right: 35px">
                                                    <textarea name="GpDesc" rows="2" class="form-control" id="GpDesc"></textarea>
                                                </div>
                                            </div>
                                            <div class="row pull-right mr-20">
                                                <?php
                                                if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Single Day") {
                                                    echo '<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Single" />
																';
                                                } else {
                                                    echo '
																<input name="SMDays_H" type="hidden" class="smallButton" id="SMDays_H" value="Multiple" />
																';
                                                }
                                                //=====================================================================================
                                                //===================== CHECK USER RIGHT FOR ADD UPDATE BUTTON ========================
                                                //=====================================================================================
                                                if (ValidateURths("REQUEST FOR ALL" . "A") == true) {
                                                    if ($EditID == "" || $EditID == '--') {
                                                        $but_Caption = "Apply Now";
                                                        $but_HasRth = ("REQUEST FOR ALL" . "A");
                                                        $but_Type = 'CustomNoSelect';
                                                        include '../main/buttons.php';
                                                    }
                                                    /*else
    {
    echo '<input name="SubmitTrans" disabled="disabled" type="submit" class="smallButton_disabled" id="SubmitTrans" value="Update Record" />';
    }
     */
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                <div class="tab-pane" id="tabs-2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php
                                            $Script = "Select HashKey from EmpTbl where status<>'D' and EmpStatus='Active' and HashKey='" . ECh($_REQUEST["AcctNo"]) . "'";
                                            if (strlen(ScriptRunner($Script, "HashKey")) != 32) {
                                                echo ("<script type='text/javascript'>{ parent.msgbox('You do not have an active manager tied to your employee record. Contact your HR', 'red'); }</script>");
                                                exit;
                                            }

                                            $Script = "Select Count(LType) Ct from LvSettings where Status='A' and LID='" . $LvGrp . "'";
                                            if (ScriptRunner($Script, "Ct") < 1) {
                                                echo ("<script type='text/javascript'>{parent.msgbox('The selected employee has no leave group defined. Contact your administrator', 'red');}</script>");
                                                exit;
                                            }

                                            $Script = "Select *, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";
                                            if (ScriptRunner($Script, "SetValue") == "Period") {
                                                $StDate = ScriptRunner($Script, "SYear");
                                                $EdDate = ScriptRunner($Script, "EYear");
                                            } else {
                                                $Script = "Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='" . $EmpID . "'";

                                                //Check what year the anniversary falls into [Last year to this year] or [This year to next year]
                                                $StDate = ScriptRunner($Script, "Dt") . ' ' . ScriptRunner($Script, "Yr");
                                                if (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
                                                    $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") + 1);
                                                } elseif (strtotime($StDate) >= strtotime(ScriptRunner($Script, "TDay"))) {
                                                    $StDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") - 1);
                                                    $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr"));
                                                }
                                            }

                                            $Script = "Select DATEDIFF(D,'" . $StDate . "',GETDATE()) Mts";
                                            $Accrued_Months = ScriptRunner($Script, "Mts"); //Used to divide to get Accrued days count - Accrued months is the start month minus the current month as an integer
                                            //AccrueD MONTH MUST BEGIN COUNTING ONLY AFTER YOU ARE SURE OF THE START MONTH EITHER FOR PERIOD BASED OR ANNIVERSARY BASED LEAVE TYPE.

                                            echo '<table width="100%"><tr>
												<td align="right" class="tdMenu_HeadBlock_Light">Start Period</td>
												<td align="left" class="subHeader">' . $StDate . '</td>
												<td align="right" class="tdMenu_HeadBlock_Light">End Period</td>
												<td align="left" class="subHeader">' . $EdDate . '</td>
												</tr>';

                                            $Display = "";
                                            $dbOpen2 = ("Select LType, HashKey from LvSettings where Status='A' and LID='" . $LvGrp . "'");

                                            include '../login/dbOpen2.php';
                                            while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                $Script = "select Accrue, NDays from LvSettings LvSet where LvSet.LID='" . $LvGrp . "' and LvSet.HashKey='" . $row2['HashKey'] . "' and Status<>'D'";
                                                $SetDays = ScriptRunner($Script, "NDays");

                                                $AccruType = ScriptRunner($Script, "Accrue");

                                                $Script = "(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D' and Status<>'R') and LType='" . $row2['HashKey'] . "' and ([LvDate] between ('" . $StDate . "')	and ('" . $EdDate . "') ) and EmpID='" . $EmpID . "')";
                                                $UsedDays = ScriptRunner($Script, "Ct");

                                                //CALCULATE AccrueD DAYS
                                                //echo '<tr><td align="center" colspan="4" class="tdMenu_HeadBlock">'.$row2['LType'].$StDate.$EdDate.'</td></tr>
                                                echo '<tr><td align="center" colspan="4" class="tdMenu_HeadBlock">' . $row2['LType'] . '</td></tr>
															  <tr>
															  		<td align="center" class="tdMenu_HeadBlock_Light">Total:&nbsp;' . $SetDays . '</td>';

                                                if ($AccruType == 1) {
                                                    if ($SetDays > 0) {
                                                        echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:&nbsp;' . number_format((($SetDays / 365) * $Accrued_Months), 0) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays) . '</td>';
                                                    } else {
                                                        echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:hh&nbsp;' . number_format((($SetDays / 365) * $Accrued_Months), 0) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																	  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays) . '</td>';
                                                    }

                                                    if ($Display == "") {
                                                        $Display = $row2['LType'] . " <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
                                                    } //Setup Display
                                                    else {
                                                        $Display = $Display . ", " . $row2['LType'] . " <small><i>(accrued)</i></small> " . ":" . (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
                                                    } //Setup Display
                                                } else {
                                                    echo '<td align="center" class="tdMenu_HeadBlock_Light">Accrued:&nbsp;' . $SetDays . '</td>
																				  <td align="center" class="tdMenu_HeadBlock_Light">Used:&nbsp;' . ($UsedDays) . '</td>
																				  <td align="center" class="tdMenu_HeadBlock_Light">Balance:&nbsp;' . ($SetDays - $UsedDays) . '</td>';
                                                    if ($Display == "") {
                                                        $Display = $row2['LType'] . ":" . ($SetDays - $UsedDays);
                                                    } //Setup Display
                                                    else {
                                                        $Display = $Display . ", " . $row2['LType'] . ":" . ($SetDays - $UsedDays);
                                                    } //Setup Display
                                                }
                                                echo '</tr>';
                                            }
                                            include '../login/dbClose2.php';
                                            echo '</table>';
                                            ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="box bg-lightest">
                                                <div class="box-header with-border">
                                                    <div class="row ">
                                                        <div class="col-md-6">
                                                            <h5>Days Left: <?php
                                                                            //if (isset($_REQUEST["AcctNo"]))
                                                                            //{
                                                                            if (isset($EmpID) && strlen($EmpID) == 32) {
                                                                                $Script = "SELECT HashKey,(Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm from EmpTbl Et where Et.HashKey='" . $EmpID . "'";
                                                                                $EmpID = ScriptRunner($Script, "HashKey");
                                                                                //$EditID=$EmpID;
                                                                                echo '<input name="AcctNo_" id="AcctNo_" type="hidden" value="' . $EmpID . '" />';
                                                                                echo ScriptRunner($Script, "Nm");
                                                                            } ?>
                                                            </h5>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-default btn-xs pull-right" title="View my time-off detains" name="EditDashboard21" id="EditDashboard21" onclick="removeElement('DbName_TR');"><i class="fa fa-address-card"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <strong>
                                                        <?php
                                                        /*
    $Display="";
    $dbOpen2 = ("Select LType, HashKey from LvSettings where Status='A' and LID='".$LvGrp."'");

    include '../login/dbOpen2.php';
    while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
    {
    $Script = "select Accrue, NDays from LvSettings LvSet where LvSet.LID='".$LvGrp."' and LvSet.HashKey='".$row2['HashKey']."' and Status<>'D'";
    $SetDays = ScriptRunner($Script,"NDays");

    $AccruType = ScriptRunner($Script,"Accrue");

    $Script="(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D') and LType='".$row2['HashKey']."' and ([LvDate] between ('".$StDate."')    and ('".$EdDate."') ) and EmpID='".$EmpID."')";
    $UsedDays = ScriptRunner($Script,"Ct");
    if ($AccruType==1)
    {
    if ($Display=="")
    {$Display=$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);} //Setup Display
    else
    {$Display=$Display.", ".$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);
    } //Setup Display
    }
    }
    include '../login/dbClose2.php';
     */

                                                        echo $Display;

                                                        ?>
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (isset($_REQUEST["SMDays"]) && $_REQUEST["SMDays"] == "Single Day") { ?>
                                    <?php } else { ?>
                                    <?php } ?>
                                </div>
                                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                <div class="tab-pane" id="tabs-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th valign="middle" bgcolor="#F0FFFF" class="smallText" scope="col"><span class="TinyTextTight">
                                                                <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                                                <label for="DelChk_All"></label>
                                                            </span></th>
                                                        <th width="130" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Applied On</span></th>
                                                        <th width="188" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Timeoff Type</span></th>
                                                        <th width="126" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Start Date</span></th>
                                                        <th width="123" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">End Date</span></th>
                                                        <th width="70" align="center" valign="middle" scope="col"> Days Left</th>
                                                        <th width="354" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Backup Employee</span></th>
                                                        <th width="152" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Status</span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ((isset($_REQUEST["S_RptDate"]) && isset($_REQUEST["E_RptDate"])) && $_REQUEST["S_RptDate"] != '' && $_REQUEST["E_RptDate"] != '') {
                                                        $SearchDt = " and SDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "' or EDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "'";
                                                    } else {
                                                        $SearchDt = '';
                                                    }
                                                    $Del = 0;
                                                    /*
    if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized")
    {
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * (Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='A') and [EmpID]='".$EmpID."' ".$SearchDt." and (LID like '%".$SearchVal."%') order by [SDate] desc, AuthDate desc");
    }
    elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized")
    {
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,*,(Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='N' or Status='U' or Status='PC' or Status='PA')  and [EmpID]=(select HashKey from EmpTbl where EmpID='".$EmpID."' ".$SearchDt." and (LID like '%".$SearchVal."%') order by [SDate] desc, AddedDate desc");
    }
    else
    {
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");
    }
     */
                                                    /*
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * (Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='A') and [EmpID]='".$EmpID."' order by [SDate] desc, AuthDate desc");
     */
                                                    /*
    //if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized"){

    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,
    Convert(Varchar(11),SDate,106) as SDt,
    Convert(Varchar(11),EDate,106) as EDt,
     *
    from LvDetails Ld
    where Status='A' and EmpID='".$EmpID."'");
     */
                                                    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,*,(Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='N' or Status='U' or Status='PC' or Status='PA' or Status='R') and [EmpID]='" . $EmpID . "'  order by [SDate] desc, AuthDate desc");

                                                    /*
    //}else{
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");

    print $SearchVal." BEN HERE";
    //}
    //*/
                                                    include '../login/dbOpen2.php';
                                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                        $Del = $Del + 1;
                                                    ?>
                                                        <tr>
                                                            <td height="27" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
                                                            <td width="126" align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
                                                            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['LType'])); ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col">&nbsp; <?php echo $row2['SDt']; ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php echo $row2['EDt']; ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php
                                                                                                                            $Script = "select Count(*) Ct from [LvIndTOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C')";
                                                                                                                            echo ScriptRunner($Script, "Ct"); ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php
                                                                                                                            $Script = "select Count(*) as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
                                                                                                                            if (ScriptRunner($Script, "Ct") > 0) {
                                                                                                                                $Script = "select [SName] +' '+ [FName]+ ' ' +[ONames]+' ['+[EmpID]+']' as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
                                                                                                                                echo ScriptRunner($Script, "Ct");
                                                                                                                            } ?></td>
                                                            <td width="152" align="center" valign="middle" scope="col" class="TinyText">&nbsp;
                                                                <?php
                                                                if ($row2['Status'] == "PA") {
                                                                    echo "Pending";
                                                                } elseif ($row2['Status'] == "C") {
                                                                    echo "Cancelled";
                                                                } elseif ($row2['Status'] == "R") {
                                                                    echo "Rejected";
                                                                } elseif ($row2['Status'] == "A") {
                                                                    echo "Approved";
                                                                } elseif ($row2['Status'] == "PC") {
                                                                    echo "Pending";
                                                                }
                                                                ?></td>
                                                        </tr>
                                                    <?php }
                                                    include '../login/dbClose2.php'; ?>
                                                </tbody>
                                            </table>
                                            <p>
                                                <?php
                                                //Set FAction Hidden Value for each button
                                                echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
												<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
												<input name="FAction" id="FAction" type="hidden" />
												<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                                //Check if any record was spolled at all before enabling buttons
                                                //Check if user has Delete rights
                                                //$but_HasRth=("REQUEST FOR ALL"."D"); $but_Type = 'Cancel'; include '../main/buttons.php';
                                                //Check if user has Add/Edit rights
                                                //$but_HasRth=("TIMEOFF ** REQUEST"."A"); $but_Type = 'View'; include '../main/buttons.php';
                                                //Check if user has Authorization rights
                                                //$but_HasRth=("REQUEST FOR ALL"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                <div class="tab-pane" id="tabs-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-responsive table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th valign="middle" scope="col"><span class="TinyTextTight">
                                                                <input type="checkbox" id="DelChk_All" onClick="ChkDel();" />
                                                                <label for="DelChk_All"></label>
                                                            </span></th>
                                                        <th width="130" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Applied On</span></th>
                                                        <th width="188" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Timeoff Type</span></th>
                                                        <th width="126" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Start Date</span></th>
                                                        <th width="123" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">End Date</span></th>
                                                        <th width="70" align="center" valign="middle" scope="col"> Days Left</th>
                                                        <th width="354" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Backup Employee</span></th>
                                                        <th width="152" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Status</span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ((isset($_REQUEST["S_RptDate"]) && isset($_REQUEST["E_RptDate"])) && $_REQUEST["S_RptDate"] != '' && $_REQUEST["E_RptDate"] != '') {
                                                        $SearchDt = " and SDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "' or EDate between '" . $_REQUEST["S_RptDate"] . "' and '" . $_REQUEST["E_RptDate"] . "'";
                                                    } else {
                                                        $SearchDt = '';
                                                    }
                                                    $Del = 0;
                                                    /*
if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized")
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * (Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='A') and [EmpID]='".$EmpID."' ".$SearchDt." and (LID like '%".$SearchVal."%') order by [SDate] desc, AuthDate desc");
}
elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized")
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,*,(Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='N' or Status='U' or Status='PC' or Status='PA')  and [EmpID]=(select HashKey from EmpTbl where EmpID='".$EmpID."' ".$SearchDt." and (LID like '%".$SearchVal."%') order by [SDate] desc, AddedDate desc");
}
else
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");
}
 */
                                                    /*
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt, * (Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='A') and [EmpID]='".$EmpID."' order by [SDate] desc, AuthDate desc");
 */
                                                    /*
//if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized"){

$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,
Convert(Varchar(11),SDate,106) as SDt,
Convert(Varchar(11),EDate,106) as EDt,
 *
from LvDetails Ld
where Status='A' and EmpID='".$EmpID."'");
 */
                                                    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SDate,106) as SDt, Convert(Varchar(11),EDate,106) as EDt,*,(Select LType from LvSettings where HashKey=Ld.LID) LType from LvDetails Ld where (Status='A') and [EmpID]='" . $EmpID . "'  order by [SDate] desc, AuthDate desc");

                                                    /*
//}else{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");

print $SearchVal." BEN HERE";
//}
//*/
                                                    include '../login/dbOpen2.php';
                                                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                                        $Del = $Del + 1;
                                                    ?>
                                                        <tr>
                                                            <td height="27" valign="middle" scope="col" class="TinyTextTight"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
                                                            <td width="126" align="center" valign="middle" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['Dt'])); ?></td>
                                                            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['LType'])); ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col">&nbsp; <?php echo $row2['SDt']; ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php echo $row2['EDt']; ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php
                                                                                                                            $Script = "select Count(*) Ct from [LvIndTOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C','R')";
                                                                                                                            echo ScriptRunner($Script, "Ct"); ?></td>
                                                            <td align="center" valign="middle" class="TinyText" scope="col"><?php
                                                                                                                            $Script = "select Count(*) as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
                                                                                                                            if (ScriptRunner($Script, "Ct") > 0) {
                                                                                                                                $Script = "select [SName] +' '+ [FName]+ ' ' +[ONames]+' ['+[EmpID]+']' as Ct from EmpTbl where HashKey='" . $row2['Subst'] . "'";
                                                                                                                                echo ScriptRunner($Script, "Ct");
                                                                                                                            } ?></td>
                                                            <td width="152" align="center" valign="middle" scope="col" class="TinyText">&nbsp;
                                                                <?php
                                                                if ($row2['Status'] == "PA") {
                                                                    echo "Pending";
                                                                } elseif ($row2['Status'] == "C") {
                                                                    echo "Cancelled";
                                                                } elseif ($row2['Status'] == "R") {
                                                                    echo "Rejected";
                                                                } elseif ($row2['Status'] == "A") {
                                                                    echo "Approved";
                                                                } elseif ($row2['Status'] == "PC") {
                                                                    echo "Pending";
                                                                }
                                                                ?></td>
                                                        </tr>
                                                    <?php }
                                                    include '../login/dbClose2.php'; ?>
                                                </tbody>
                                            </table>
                                            <p>
                                                <?php
                                                //Set FAction Hidden Value for each button
                                                echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
												<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
												<input name="FAction" id="FAction" type="hidden" />
												<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
                                                //Check if any record was spolled at all before enabling buttons
                                                //Check if user has Delete rights
                                                //$but_HasRth=("REQUEST FOR ALL"."D"); $but_Type = 'Cancel'; include '../main/buttons.php';
                                                //Check if user has Add/Edit rights
                                                //$but_HasRth=("TIMEOFF ** REQUEST"."A"); $but_Type = 'View'; include '../main/buttons.php';
                                                //Check if user has Authorization rights
                                                //$but_HasRth=("REQUEST FOR ALL"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Bootstrap 4.0-->
    <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>


    <!-- FastClick -->
    <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>

    <!-- MinimalLite Admin App -->
    <script src="../assets/js/template.js"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <script>
        setDatePicker();

        function getEarliestAndMax(e) {
            const hashkey = e.value;
            if (hashkey == '--') {
                setDatePicker();
                return;
            }
            $.post({
                url: 'getEarliestAndMaxAjax.php',
                data: {
                    hashkey,
                    PgDoS: '<?php echo DoSFormToken() ?>'
                },
                cache: false,
                success(res) {
                    // console.log(res.earliest);
                    setDatePicker(res.earliest);
                },
                error(e) {
                    console.log(e);
                }
            })
        }
    </script>
</body>
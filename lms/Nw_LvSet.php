<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

if (ValidateURths("SET LEAVE POLICY" . "V") != true) {
	include '../main/NoAccess.php';
	exit;
}

if (!isset($EditID)) {
	$EditID = "";
}
if (!isset($Script_Edit)) {
	$Script_Edit = "";
}
if (!isset($HashKey)) {
	$HashKey = "";
}
if (!isset($Del)) {
	$Del = 0;
}
if (!isset($AuthRec)) {
	$AuthRec = "";
}

$GoValidate = true;

echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
	unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Update Selected") {
		if (ValidateURths("SET LEAVE POLICY" . "A") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/* Set HashKey of Account to be Updated */
		//$EditID = $_REQUEST["UpdID"];
		//$HashKey = $_REQUEST["UpdID"];

		/*CHeck validation for updating an account */
		$GoValidate = true;

		if (isset($_REQUEST["LeaveTyp"]) && trim($_REQUEST["LeaveTyp"]) == "Period") {
			if ($_REQUEST["StartDt"] == "" || $_REQUEST["EndDt"] == "") {
				echo ("<script type='text/javascript'>{ parent.msgbox('You must select a start and end date for the leave year.', 'red'); }</script>");
				$GoValidate = false;
			}

			if (strtotime($_REQUEST["StartDt"]) >= strtotime($_REQUEST["EndDt"])) {
				echo ("<script type='text/javascript'>{ parent.msgbox('Leave year end date must be a calendar date after the start date.', 'red'); }</script>");
				$GoValidate = false;
			}
		}

		if (isset($_REQUEST["LeaveTyp"]) && trim($_REQUEST["LeaveTyp"]) == "" || trim($_REQUEST["LeaveTyp"]) == "--") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select an organisational leave type.', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["FNotice1"]) == trim($_REQUEST["FNotice2"]) && strlen(trim($_REQUEST["FNotice2"])) == "32") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the final notifier(2).', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["BNotice1"]) == trim($_REQUEST["BNotice2"]) && strlen(trim($_REQUEST["BNotice2"])) == "32") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the request notifier(2).', 'red'); }</script>");
			$GoValidate = false;
		}

		//--------------------------------------------------------------------------------------------------
		if (trim($_REQUEST["CNotice1"]) == trim($_REQUEST["CNotice2"]) && strlen(trim($_REQUEST["CNotice2"])) == "32") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the Cancelled Notification(2).', 'red'); }</script>");
			$GoValidate = false;
		}

		if (trim($_REQUEST["RNotice1"]) == trim($_REQUEST["RNotice2"]) && strlen(trim($_REQUEST["RNotice2"])) == "32") {
			echo ("<script type='text/javascript'>{ parent.msgbox('You must select a different employee for the Rejected Notification(2).', 'red'); }</script>");
			$GoValidate = false;
		}

		//--------------------------------------------------------------------------------------------------

		if (isset($_REQUEST["HRVer"])) {
			$HRVer_ = $_REQUEST["HRVer"];
		} else {
			$HRVer_ = "";
		}
		if (isset($_REQUEST["HRVer_1"])) {
			$HRVer_1 = 1;
		} else {
			$HRVer_1 = "";
		}

		if ($GoValidate == true) {

			$Script = "Update [Settings] set [SetValue]='" . ECh($_REQUEST["LeaveTyp"]) . "',[SetStart]='" . ECh($_REQUEST["StartDt"]) . "',
			[SetEnd]='" . ECh($_REQUEST["EndDt"]) . "',
			[SetValue2]='" . ECh($HRVer_) . "',
			[SetValue16]='" . ECh($HRVer_1) . "',
			[SetValue3]='" . ECh($_REQUEST["BNotice1"]) . "',
			[SetValue4]='" . ECh($_REQUEST["BNotice2"]) . "',
			[SetValue5]='" . ECh($_REQUEST["FNotice1"]) . "',
			[SetValue6]='" . ECh($_REQUEST["FNotice2"]) . "',
			[SetValue7]='" . ECh($_REQUEST["CNotice1"]) . "',
			[SetValue8]='" . ECh($_REQUEST["CNotice2"]) . "',
			[SetValue9]='" . ECh($_REQUEST["RNotice1"]) . "',
			[SetValue10]='" . ECh($_REQUEST["RNotice2"]) . "',
			[SetValue11]='" . ECh($_REQUEST["LeaveBonus"]) . "',
			[SetValue15]='" . ECh($_REQUEST["teamRejection"]) . "',
			[Status]='U',[UpdatedBy]='" . ECh($_SESSION["StkTck" . "HKey"]) . "',
			[UpdatedDate]=GetDate() where [Setting]= 'LeaveTyp'";

			ScriptRunnerUD($Script, "Inst");

			$currentYear = date('Y');
			$sql = "
			UPDATE [LvPHolsDetails]
			SET [SDate] = DATEADD(YEAR, $currentYear - YEAR([SDate]), [SDate]),
				[EDate] = DATEADD(YEAR, $currentYear - YEAR([EDate]), [EDate])
			WHERE YEAR([SDate]) != $currentYear OR YEAR([EDate]) != $currentYear;
		";
			ScriptRunnerUD($sql, "Inst");

			// print_r("$sql");
			// die("sfsf");
			AuditLog("UPDATE", "Organisational leave setting updated");

			echo ("<script type='text/javascript'>{ parent.msgbox('Organisational leave setting updated successfully.', 'green'); }</script>");
			//Clear Selection//

			$Script_Edit = "Select *,Convert(Varchar(11),[SetStart],106) SDt, Convert(Varchar(11),[SetEnd],106) EDt from Settings where [Setting]= 'LeaveTyp'";
			//$EditID="--";
		}

		if ($GoValidate == false) {
			$EditID = "--";
		}
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
		if (ValidateURths("SET LEAVE POLICY" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		/*
        $Script = "UPDATE [Settings]
        SET [Status] = 'A'Selected
        ,[AuthBy]='".$_SESSION["StkTck"."UName"]."'
        ,[AuthDate]=GetDate()
        WHERE [Setting]= 'LeaveTyp'";
        ScriptRunnerUD ($Script, "Authorize");
        AuditLog("AUTHORIZE","Organisational leave setting authorized");
         */

		$Script = "UPDATE [Settings]
		SET [Status] = 'A'
		,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
		,[AuthDate]=GetDate()
		WHERE [Setting]= 'LeaveTyp'";
		ScriptRunnerUD($Script, "Authorize");



		AuditLog("AUTHORIZE", "Organisational leave setting authorized");

		echo ("<script type='text/javascript'>{ parent.msgbox('Organisational leave setting authorized successfully.', 'green'); }</script>");
		$EditID = "--";
	} elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected") {
		if (ValidateURths("SET LEAVE POLICY" . "T") != true) {
			include '../main/NoAccess.php';
			exit;
		}

		$Script = "UPDATE [Settings]
		SET [Status] = 'U'
		,[AuthBy]='" . $_SESSION["StkTck" . "UName"] . "'
		,[AuthDate]=GetDate()
		WHERE [Setting]= 'LeaveTyp'";
		ScriptRunnerUD($Script, "Authorize");
		AuditLog("UNAUTHORIZE", "Organisational leave setting unauthorized");

		echo ("<script type='text/javascript'>{ parent.msgbox('Organisational leave setting unauthorized successfully.', 'green'); }</script>");
		$EditID = "--";
	}
}
?>
<script>
	$(function() {
		$('#datepicker').datepicker({
			showButtonPanel: true,
			dateFormat: "mm/dd/yy",
			//    beforeShow: function(){
		});


		$("#StartDt").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "-12M",
			maxDate: "+12M",
			dateFormat: 'dd M yy'
		})
		$("#EndDt").datepicker({
			changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			minDate: "-12M",
			maxDate: "+12M",
			dateFormat: 'dd M yy'
		})
	});
</script>

<script>
	$(function() {
		$(document).tooltip();
	});
</script>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {
	echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';
} else { ?>
	<link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php } ?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body>
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-12 text-center ">
						<h4>
							Organisational Leave Setting
						</h4>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Organisation Leave Type<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="LeaveTyp" id="LeaveTyp">
									<?php
									$Script_Edit = "Select *, Convert(Varchar(11),[SetStart],106) SDt, Convert(Varchar(11),[SetEnd],106) EDt from Settings where [Setting]= 'LeaveTyp'";
									$DigitsDsp_ = ScriptRunner($Script_Edit, "SetValue");

									if (trim($DigitsDsp_) == '') {
										echo '<option selected="selected" value="Period">Period</option> <option value="Anniversary">Anniversary</option>';
									} elseif (trim($DigitsDsp_) == 'Anniversary') {
										echo '<option selected="selected" value="Anniversary">Anniversary</option> <option value="Period">Period</option>';
									} elseif (trim($DigitsDsp_) == 'Period') {
										echo '<option value="Anniversary">Anniversary</option> <option selected="selected" value="Period">Period</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="demo-checkbox">
							<?php
							$kk = ScriptRunner($Script_Edit, "SetValue2");

							/*if ($AuthRec == "A")
{
if (trim($kk) == 'on')
{echo'<input disabled name="AutoVerID" id="AutoVerID" type="checkbox" checked="checked" />';}
else
{echo'<input disabled name="AutoVerID" id="AutoVerID" type="checkbox" />';}
}
else
{ */
							if (trim($kk) == 'on') {
								echo '<input name="HRVer" id="HRVer" type="checkbox" checked="checked" />';
							} else {
								echo '<input name="HRVer" id="HRVer" type="checkbox" />';
							}
							//}
							?>
							<label for="HRVer">HR Verify & Mgr. Approval</label>
						</div>
						<div class="demo-checkbox">
							<?php
							$kk = ScriptRunner($Script_Edit, "SetValue16");

							/*if ($AuthRec == "A")
{
if (trim($kk) == 'on')
{echo'<input disabled name="AutoVerID" id="AutoVerID" type="checkbox" checked="checked" />';}
else
{echo'<input disabled name="AutoVerID" id="AutoVerID" type="checkbox" />';}
}
else
{ */
							if (trim($kk) == '1') {
								echo '<input name="HRVer_1" id="HRVer_1" type="checkbox" checked="checked" />';
							} else {
								echo '<input name="HRVer_1" id="HRVer_1" type="checkbox" value="1" />';
							}
							//}
							?>
							<label for="HRVer_1">Allow Book Leave Request in Arrears </label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Start of Leave Year<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								echo '<input name="StartDt" id="StartDt" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "SDt") . '" size="16" maxlength="12" readonly="readonly" /></span>';
								?>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">End of Leave Year<span style="color: red">*</span>:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<?php
								echo '<input name="EndDt" id="EndDt" type="text" class="form-control" value="' . ScriptRunner($Script_Edit, "EDt") . '" size="16" maxlength="12" readonly="readonly" /></span>';
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h4 class="page-header text-center">Time-Off Notification Settings</h4>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Request Notification(1)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="BNotice1" id="BNotice1">
									<?php
									$AllEmployee = "";
									$EditID = ScriptRunner($Script_Edit, "SetValue3");
									echo '<option value="--" selected="selected">--</option>';

									$dbOpen2 = ("SELECT * from EmpTbl where EmpStatus='Active' and Status in('A','U','N') and HashKey<>'" . $EditID . "' order by SName Asc");
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//echo '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [".$row2['EmpID']."]" . '</option>';
										$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
									}
									include '../login/dbClose2.php';

									echo $AllEmployee;

									if (strlen($EditID) == 32) {
										//    $kk = ScriptRunner($Script_Edit,"SetValue3");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Request Notification(2)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="BNotice2" id="BNotice2">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;

									$EditID = ScriptRunner($Script_Edit, "SetValue4");
									if (strlen($EditID) == 32) {
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Cancelled Notification(1)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="CNotice1" id="CNotice1">
									<?php
									$AllEmployee = "";
									$EditID = ScriptRunner($Script_Edit, "SetValue7");
									echo '<option value="--" selected="selected">--</option>';

									$dbOpen2 = ("SELECT * from EmpTbl where EmpStatus='Active' and Status in('A','U','N') and HashKey<>'" . $EditID . "' order by SName Asc");
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//echo '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [".$row2['EmpID']."]" . '</option>';
										$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
									}
									include '../login/dbClose2.php';

									echo $AllEmployee;

									if (strlen($EditID) == 32) {
										//    $kk = ScriptRunner($Script_Edit,"SetValue3");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Cancelled Notification(2)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="CNotice2" id="CNotice2">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;

									$EditID = ScriptRunner($Script_Edit, "SetValue8");
									if (strlen($EditID) == 32) {
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>

						<!-- Dropdown for leave type bonus -->
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Select Annual Leave:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="LeaveBonus" id="LeaveBonus">
									<?php

									echo '<option value="--" selected="selected" disabled>--</option>';
									$dbOpen3 = ("SELECT Val1, ParentVal, HashKey from Masters where ItemName='LeaveType' and Status <> 'D' Order by Val1 asc");
									include '../login/dbOpen3.php';
									while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
										echo '<option value="' . $row3['HashKey'] . '">' . $row3['Val1'] . '</option>';
									}
									include '../login/dbClose3.php';

									$EditID = ScriptRunner($Script_Edit, "SetValue11");
									if (strlen($EditID) == 32) {
										$Script_bonus = "SELECT Val1 from Masters where ItemName='LeaveType' and Status<>'D' and HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script_bonus, "Val1") . '</option>';
									}

									?>
								</select>
							</div>


						</div>
						<!-- End of dropdown for leave type bonus -->

					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Approved Notification(1)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="FNotice1" id="FNotice1">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;
									$EditID = ScriptRunner($Script_Edit, "SetValue5");
									if (strlen($EditID) == 32) {
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Approved Notification(2)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="FNotice2" id="FNotice2">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;

									$EditID = ScriptRunner($Script_Edit, "SetValue6");
									if (strlen($EditID) == 32) {
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Rejected Notification(1)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="RNotice1" id="RNotice1">
									<?php
									$AllEmployee = "";
									$EditID = ScriptRunner($Script_Edit, "SetValue9");
									echo '<option value="--" selected="selected">--</option>';

									$dbOpen2 = ("SELECT * from EmpTbl where EmpStatus='Active' and Status in('A','U','N') and HashKey<>'" . $EditID . "' order by SName Asc");
									include '../login/dbOpen2.php';
									while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
										//echo '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [".$row2['EmpID']."]" . '</option>';
										$AllEmployee = $AllEmployee . '<option value="' . $row2['HashKey'] . '">' . $row2['SName'] . " " . $row2['FName'] . " " . $row2['ONames'] . " [" . $row2['EmpID'] . "]" . '</option>';
									}
									include '../login/dbClose2.php';

									echo $AllEmployee;

									if (strlen($EditID) == 32) {
										//    $kk = ScriptRunner($Script_Edit,"SetValue3");
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Rejected Notification(2)
								<span style="color: red">*</span> :</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="RNotice2" id="RNotice2">
									<?php
									echo '<option value="--" selected="selected">--</option>';
									echo $AllEmployee;

									$EditID = ScriptRunner($Script_Edit, "SetValue10");
									if (strlen($EditID) == 32) {
										$Script = "Select HashKey, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $EditID . "'";
										echo '<option selected="selected" value="' . $EditID . '">' . ScriptRunner($Script, "Nm") . '</option>';
									}
									?>
								</select>
							</div>

						</div>

						<div class="form-group row">
							<label class="col-sm-4 col-form-label" style="padding-right: 0px;">Team Rejection:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control" name="teamRejection" id="teamRejection">
									<option selected="selected" value="0">--</option>
									<?php
									$Script_Edit = "Select SetValue15 from Settings where Setting='LeaveTyp'";
									$birthdayID = ScriptRunner($Script_Edit, "SetValue15");

									for ($php_i = 2; $php_i <= 10; $php_i++) {
										if ($birthdayID == $php_i) {
											echo '<option selected=selected value="' . $birthdayID . '">' . $birthdayID . ' days</option>';
										} else {
											echo '<option value="' . $php_i . '">' . $php_i . ' days</option>';
										}
									};
									?>
								</select>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" title="Select days to disable Team Lead Leave rejection before Leave start date" id="teamRejection">
										<i class="fa fa-info"></i>
									</button>
								</span>
							</div>
						</div>


						<div class="row">
							<div class="col-sm-6"></div>
							<div class="col-sm-6">
								<br>
								<?php
								//Check if user has Add/Update User rights
								if (ValidateURths("SET LEAVE POLICY" . "A") == true) {
								?>
									<input name="SubmitTrans" type="button" class="btn btn-danger btn-sm" id="SubmitTrans" value="Update" onclick="YesNo('Update', 'Update')" />
									<?php
								}
								if (ValidateURths("SET LEAVE POLICY" . "T") == true) {
									$Script_Menu = "Select Status from Settings where Setting='LeaveTyp'";
									$AuthRec = ScriptRunner($Script_Menu, "Status");
									if ($AuthRec == "A") { ?>
										<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Unauthorize All" onclick="YesNo('Unauthorize All', 'Unauthorize')" />
									<?php
									} else { ?>
										<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Authorize All" onclick="YesNo('Authorize All', 'Authorize')" />
								<?php
									}
								}

								//'<input name="UpdID" type="hidden" id="UpdID" value="' . $EditID . '" />
								$Del = 1;
								echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
									<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
									<input name="FAction" id="FAction" type="hidden" />
									<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
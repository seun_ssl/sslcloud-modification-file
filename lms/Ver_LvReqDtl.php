<?php session_start();
include '../login/scriptrunner.php';

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = false;
include '../css/myscripts.php';

//Validate user viewing rights
if (ValidateURths("VERIFY TIMEOFF" . "V") != true) {include '../main/NoAccess.php';exit;}

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($Del)) {$Del = 0;}
if (!isset($SelID)) {$SelID = "";}
if (!isset($To)) {$To = "";}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Verify Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("VERIFY TIMEOFF" . "D") != true) {include '../main/NoAccess.php';exit;}

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $i]);

                $Script = "UPDATE [LvDetails]
				SET [VerifiedBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
				,[VerifiedDate] = GetDate()
				,[Verified]=1
				,[Subst]='" . $_REQUEST["Subst$i"] . "'
				WHERE [HashKey]='" . $EditID . "'";
                ScriptRunnerUD($Script, "CA");

                ////////////////////////////////-=============================================////////////////////////////////
                ////////////////////////////////-=============================================////////////////////////////////
                /*    $LoginDet='Hi #MrgName#,<br />
                The timeoff request made by #EmpName# has been successfully verified.<br />
                You may proceed to approve this timeoff.
                <br /><br />
                For more information, please login into your account.';
                 */

                $Script = "Select MMsg, MSub from MailTemp where HashKey='98398de0f6f28wec9bc1b5342ecd9571'"; //MSub='TimeOff Verified'";

                $Script_Edit = "Select Email as EmpEmail,(SName+' '+FName+' ['+Convert(varchar(18),EmpID)+']') EmpName, (Select Et2.Email from EmpTbl Et2 where Et2.HashKey=Et1.EmpMgr) as MgrEmail, (Select (Et3.SName+' '+Et3.FName) from EmpTbl Et3 where Et3.HashKey=Et1.EmpMgr) as MgrName from EmpTbl Et1 where HashKey=(select EmpID from [LvDetails] WHERE [HashKey]='" . $EditID . "')";
                $ToEmp = ScriptRunner($Script_Edit, "EmpEmail");
                $ToMgr = ScriptRunner($Script_Edit, "MgrEmail");
                $EmpName = ScriptRunner($Script_Edit, "EmpName");
                $MgrName = ScriptRunner($Script_Edit, "MgrName");

                $LoginDet = ScriptRunner($Script, "MMsg");
                //$LoginDet=str_replace('#MrgName#',$MgrVal,$LoginDet);
                $LoginDet = str_replace('#EmpName#', $EmpName, $LoginDet);
                $LoginDet = str_replace('#EmpMgr#', $MgrName, $LoginDet);
                $LoginDet = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
                $LoginDet_Backup = str_replace('#ServerName#', $_SERVER['HTTP_HOST'], $LoginDet);
                $LoginDet = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);
                $LoginDet_Backup = str_replace('#Link#', $_SERVER['HTTP_HOST'], $LoginDet);

                /* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */
                $Bdy = $LoginDet;
                $Subj = ScriptRunner($Script, "MSub");

                if (strlen($ToMgr) > 5) {$To = $ToMgr;}

                $To = $To . "," . $_SESSION["StkTck" . "Email"];
                QueueMail($To, $Subj, $Bdy, '', '');

                AuditLog("AUTHORIZE", "Employee leave sucessfully verified for " . $EmpName . ".");
                echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) verified successfully.', 'green'); }</script>");
            }
        }
    }

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unverify Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("VERIFY TIMEOFF" . "D") != true) {include '../main/NoAccess.php';exit;}

        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $i]);

                $Script = "Select Status from [LvDetails] WHERE [HashKey]='" . $EditID . "'";
                $SelID = ScriptRunner($Script, "Status");
                if ($SelID == 'A' || $SelID == 'PC') {
                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff cannot be unverified. Timeoff already approved by manager.', 'red'); }</script>");
                } else {
                    $Script = "UPDATE [LvDetails]
					SET [VerifiedBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
					,[VerifiedDate] = GetDate()
					,[Verified]=0
					WHERE [HashKey]='" . $EditID . "'";
                    ScriptRunnerUD($Script, "CA");

                    ////////////////////////////////-=============================================////////////////////////////////
                    ////////////////////////////////-=============================================////////////////////////////////
                    /*    $LoginDet='Hi #MrgName#,<br />
                    The timeoff request made by #EmpName# has been successfully verified.<br />
                    You may proceed to approve this timeoff.
                    <br /><br />
                    For more information, please login into your account.';
                     */

                    /*
                    $Script="Select MMsg, MSub from MailTemp where HashKey='98398de0f6fqqrec9bc433342ecd9571'"; //MSub='TimeOff Verified'";

                    $Script_Edit="Select Email as EmpEmail,(SName+' '+FName+' ['+Convert(varchar(18),EmpID)+']') EmpName, (Select Et2.Email from EmpTbl Et2 where Et2.HashKey=Et1.EmpMgr) as MgrEmail from EmpTbl Et1 where HashKey=(select EmpID from [LvDetails] WHERE [HashKey]='".$EditID."')";
                    $ToEmp=ScriptRunner($Script_Edit,"EmpEmail");
                    $ToMgr=ScriptRunner($Script_Edit,"MrgEmail");
                    $EmpName=ScriptRunner($Script_Edit,"EmpName");

                    $LoginDet=ScriptRunner($Script,"MMsg");
                    //$LoginDet=str_replace('#MrgName#',$MgrVal,$LoginDet);
                    $LoginDet=str_replace('#EmpName#',$EmpName,$LoginDet);
                    $LoginDet=str_replace('#ServerName#',$_SERVER['HTTP_HOST'],$LoginDet);
                    $LoginDet_Backup=str_replace('#ServerName#',$_SERVER['HTTP_HOST'],$LoginDet);
                    $LoginDet=str_replace('#Link#',$_SERVER['HTTP_HOST'],$LoginDet);
                    $LoginDet_Backup=str_replace('#Link#',$_SERVER['HTTP_HOST'],$LoginDet);

                    // MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email"));
                    $Bdy = $LoginDet;
                    $Subj = ScriptRunner($Script,"MSub");

                    if (strlen($ToMgr)>5)
                    {$To=$ToMgr;}

                    $To=$To.",".$_SESSION["StkTck"."Email"];
                    QueueMail($To,$Subj,$Bdy,'','');

                    AuditLog("AUTHORIZE","Employee timeoff sucessfully unverified for ".$EmpName.".");
                     */
                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee timeoff day(s) unverified successfully.', 'green'); }</script>");
                }
            }
        }
    }

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Cancel Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("VERIFY TIMEOFF" . "A") != true) {include '../main/NoAccess.php';exit;}
        // var_dump('here');
        for ($i = 1; $i <= $_REQUEST["DelMax"]; $i++) {
            if (isset($_REQUEST["DelBox" . $i]) && strlen(trim($_REQUEST["DelBox" . $i])) == 32) {
                $EditID = ECh($_REQUEST["DelBox" . $i]);
                $empid = ECh($_REQUEST["EmpID" . $i]);
                // var_dump($_REQUEST);
                // $Script = "Select Count(*) Ct from LvDetails WHERE Status in ('PA') and [HashKey]='" . $EditID . "'";
                $Script = "select COUNT(*) Ct ,LID from LvIndTOff where LID='" . $EditID . "' and [EmpID]='" . $empid . "' and Status in('PA') group by LID";
                // var_dump($Script);
                // die();
                $coount = ScriptRunner($Script, "Ct");

                if ($coount > 0) {

                    $Script1 = "select * from LvDetails where HashKey='" . $EditID . "'";
                    $RDays = ScriptRunner($Script1, "RDays");
                    $reverse_count = (int) $coount + (int) $RDays;

                    $Script = "UPDATE [LvDetails]
					SET [CancelledBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
					,[CancelledDate] = GetDate()
					,[Verified]=0
					,[Status]='C'
					,RDays='" . $reverse_count . "'
					WHERE [HashKey]='" . $EditID . "'";
                    ScriptRunnerUD($Script, "CA");

                    $Script = "UPDATE [LvIndTOff]
					SET [CancelledBy] ='" . $_SESSION["StkTck" . "HKey"] . "'
					,[CancelledDate] = GetDate()
					,[Status]='C'
					WHERE [LID]='" . $EditID . "'";
                    ScriptRunnerUD($Script, "CA");

                }

                // $Script="Select Count(*) Ct from LvDetails WHERE Status in ('PA') and [HashKey]='".$EditID."'";
                // if (ScriptRunner($Script,"Ct")==1)
                // {
                //     $Script = "UPDATE [LvDetails]
                //     SET [CancelledBy] ='".$_SESSION["StkTck"."HKey"]."'
                //     ,[CancelledDate] = GetDate()
                //     ,[Verified]=0
                //     ,[Status]='C'
                //     WHERE [HashKey]='".$EditID."'";
                //     ScriptRunnerUD($Script,"CA");

                //     $Script = "UPDATE [LvIndTOff]
                //     SET [CancelledBy] ='".$_SESSION["StkTck"."HKey"]."'
                //     ,[CancelledDate] = GetDate()
                //     ,[Status]='C'
                //     WHERE [LID]='".$EditID."'";
                //     ScriptRunnerUD($Script,"CA");
                //     ////////////////////////////////-=============================================////////////////////////////////
                //     ////////////////////////////////-=============================================////////////////////////////////
                //     /*    $LoginDet='Hi #MrgName#,<br />
                //     The timeoff request made by #EmpName# has been successfully verified.<br />
                //     You may proceed to approve this timeoff.
                //     <br /><br />
                //     For more information, please login into your account.';
                //     */

                //     $Script="Select MMsg, MSub from MailTemp where HashKey='12398de0f6f28wezzbc1b5342ecd9571'"; //MSub='TimeOff Verification Cancelled'";
                //     $Script_Edit="Select Email as EmpEmail,(SName+' '+FName+' ['+Convert(varchar(18),EmpID)+']') EmpName, (Select Et2.Email from EmpTbl Et2 where Et2.HashKey=Et1.EmpMgr) as MgrEmail, (Select (Et3.SName+' '+Et3.FName) from EmpTbl Et3 where Et3.HashKey=Et1.EmpMgr) as MgrName from EmpTbl Et1 where HashKey=(select EmpID from [LvDetails] WHERE [HashKey]='".$EditID."')";
                //     $ToEmp=ScriptRunner($Script_Edit,"EmpEmail");
                //     $ToMgr=ScriptRunner($Script_Edit,"MgrEmail");
                //     $EmpName=ScriptRunner($Script_Edit,"EmpName");
                //     $MgrName=ScriptRunner($Script_Edit,"MgrName");

                //     $LoginDet=ScriptRunner($Script,"MMsg");
                //     $LoginDet=str_replace('#EmpName#',$EmpName,$LoginDet);
                //     $LoginDet=str_replace('#EmpMgr#',$MgrName,$LoginDet);
                //     $LoginDet=str_replace('#ServerName#',$_SERVER['HTTP_HOST'],$LoginDet);
                //     $LoginDet_Backup=str_replace('#ServerName#',$_SERVER['HTTP_HOST'],$LoginDet);
                //     $LoginDet=str_replace('#Link#',$_SERVER['HTTP_HOST'],$LoginDet);
                //     $LoginDet_Backup=str_replace('#Link#',$_SERVER['HTTP_HOST'],$LoginDet);

                //     /* MailTrail("NEW USER ACCOUNT","A","","",ScriptRunner($Script,"email")); */
                //     $Bdy = $LoginDet;
                //     $Subj = ScriptRunner($Script,"MSub");
                //     $To="";

                //     if (strlen($ToEmp)>5)
                //     {$To=$ToEmp;}

                //     if (strlen($ToMgr)>5)
                //     {$To=$To.",".$ToMgr;}

                //     $To=$To.",".$_SESSION["StkTck"."Email"];
                //     QueueMail($To,$Subj,$Bdy,'','');

                //     AuditLog("AUTHORIZE","Employee leave sucessfully cancelled for ".$EmpName.".");
                //     echo ("<script type='text/javascript'>{parent.msgbox('Selected employee timeoff day(s) cancelled successfully.','green');}</script>");
                // }
            }
        }
    }
}

$count = 1;
?>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script>
$(function()
  {
	$("#PayMonth_Ed").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "0D", dateFormat: 'dd M yy'})
	$("#PayMonth_St").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "0D", dateFormat: 'dd M yy'})
  });
</script>

<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

<body oncontextmenu="return false;">
	<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-4">[<a href="Ver_LvReqDtl.php">Clear Page</a>]</div>
					<div class="col-md-4 text-center">
						<h4>
						Employee TimeOff Verification
						</h4>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn-sm btn btn-danger" value="VERIFIED"/>
			          	<input name="SubmitTrans" id="SubmitTrans" type="submit" class="btn-sm btn btn-danger" value="UNVERIFIED"/>
					</div>
					<?php $Load_Date = true;
$Load_Search = false;
$Load_Auth = false;include '../main/report_opt.php';?>
				</div>
				<hr style="margin-top: 1.0rem;margin-bottom: .5rem">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
//------------------------
if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] != '')) {

    // $date1 = date_create($_REQUEST["S_RptDate"]);
    // $date1 = date_format($date1, 'Y-m-d');
    // //print "<br />";
    // $date2 = date_create($_REQUEST["E_RptDate"]);
    // $date2 = date_format($date2, 'Y-m-d');
    // $PayMthDt = " and (SDate >= '$date1' AND EDate <= '$date2') ";
} else {
    //$PayMthDt = '';
    $Script_Date = "Select Convert(Varchar(11),SetStart,106) LDate_Start, Convert(Varchar(11),SetEnd,106) LDate_End from [Settings] WHERE [Setting]= 'LeaveTyp'";
    $LDate_Start = ScriptRunner($Script_Date, "LDate_Start");
    $LDate_End = ScriptRunner($Script_Date, "LDate_End");

    $date1 = date_create($LDate_Start);
    $date1 = date_format($date1, 'Y-m-d');

    $date2 = date_create($LDate_End);
    $date2 = date_format($date2, 'Y-m-d');

    // $PayMthDt = " and (SDate >= '$date1' AND EDate <= '$date2') ";
}
if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNVERIFIED")) {
    print "<h4>UNVERIFIED</h4>";

    $dbOpen2 = ("SELECT
								LvD.ID,
								Convert(Varchar(11),LvD.AddedDate,106) as Dt,
								Convert(Varchar(11),LvD.SDate,106) as SDt,
								(select LType from LvSettings where HashKey=LvD.LID) LType,
								Convert(Varchar(11),LvD.EDate,106) as EDt,
								LvD.*,
								([SName] +' '+ [FName]+ ' ' +[ONames]+' ['+Et.EmpID+']') Nm,
								Et.HashKey as EmpHKey,
								Et.EmpMgr
								FROM
								LvDetails LvD, EmpTbl Et
								WHERE
								LvD.EmpID=Et.HashKey
								AND
								LvD.Status not in ('A','C','R')
								AND
								LvD.Verified=0 order by LvD.SDate desc, LvD.AuthDate desc");
    // print_r($dbOpen2);

} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "VERIFIED")) {

    print "<h4>VERIFIED</h4>";
    $dbOpen2 = ("SELECT LvD.ID, Convert(Varchar(11),LvD.AddedDate,106) as Dt, Convert(Varchar(11),LvD.SDate,106) as SDt, (select LType from LvSettings where HashKey=LvD.LID) LType, Convert(Varchar(11),LvD.EDate,106) as EDt, LvD.*, ([SName] +' '+ [FName]+ ' ' +[ONames]+' ['+Et.EmpID+']') Nm, Et.HashKey as EmpHKey, Et.EmpMgr from LvDetails LvD, EmpTbl Et where LvD.EmpID=Et.HashKey and LvD.Status not in ('A','C','R') and LvD.Verified=1 order by LvD.SDate desc, LvD.AuthDate desc");

} else {
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from LvDetails where Status = 'Z' order by LID asc");
}
//------------------------
?>
					</div>
					<div class="col-md-12">
						<table width="100%" align="left" cellpadding="1" cellspacing="1" id="table">
							<thead>
								<tr class="tdMenu_HeadBlock_Light">
									<th width="32" valign="middle" class="smallText" scope="col"><span class="TinyTextTight">
										<input type="checkbox" id="DelChk_All" onClick="ChkDel();"/>
										<label for="DelChk_All"></label>
									</span></th>
									<th width="1262" align="left" valign="middle" class="subHeader" scope="col">Select All</th>
								</tr>
							</thead>
							<tbody>
								<?php
$Del = 0;
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    if ($Del == 1) {$PrevID = $row2['ID'];} else { $NextID = $row2['ID'];}
    ?>
								<tr>
									<td height="70" colspan="2" valign="middle" class="TinyTextTight" scope="col"><table width="100%" border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="32" rowspan="5" align="center" valign="middle" class="tdMenu_HeadBlock_Light"><input name="<?php echo ("DelBox" . $Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label>
											<input type="hidden" name="<?php echo ("EmpID" . $Del); ?>"  value="<?php echo ($row2['EmpID']); ?>"  >
											</td>
											<td width="179" align="right" valign="middle" class="tdMenu_HeadBlock_Light">Employee:</td>
											<td colspan="3" align="left" valign="middle" class="tdMenu_HeadBlock"><?php
echo (trim($row2['Nm'])); ?>  </td>
											<td width="3" rowspan="5" align="left" valign="middle">&nbsp;</td>
											<td colspan="2" align="center" class="tdMenu_HeadBlock">Timeoff Summary</td>
										</tr>
										<tr>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Timeoff Type:</td>
											<td width="275" align="left" valign="middle"><?php echo (trim($row2['LType'])); ?></td>
											<td width="184">&nbsp;</td>
											<td width="183" align="center" valign="middle" class="tdMenu_HeadBlock">Duration:</td>
											<td width="207" align="right" valign="middle" class="tdMenu_HeadBlock_Light">Total:</td>
											<td width="209" class="TinyText">
												<?php
$Script = "Select NDays from LvSettings where HashKey='" . trim($row2['LID']) . "' and LID=(select EmpLeaveGp from EmpTbl where HashKey='" . $row2['EmpHKey'] . "')";
    //echo $Script;
    echo ScriptRunner($Script, "NDays");
    ?> days</td>
										</tr>
										<tr>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Applied Period:</td>
											<td colspan="2" align="left" valign="middle"><?php echo $row2['SDt']; ?> to <?php echo $row2['EDt']; ?></td>
											<td align="center" valign="middle" class="tdMenu_HeadBlock_Light"><?php
$Script = "select Count(*) Ct from [LvIndTOff] where LID='" . $row2['HashKey'] . "' and Status not in ('D','C')";
    echo ScriptRunner($Script, "Ct");?> day(s)</td>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Accrued:</td>
											<td class="TinyText">
												<?php
$Script = "Select *, DATEDIFF(D,[SetStart],GETDATE()) Mts, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";
    $Accrued_Months = ScriptRunner($Script, "Mts"); //Used to divide to get Accrued days count

    if (ScriptRunner($Script, "SetValue") == "Period") {
        $StDate = ScriptRunner($Script, "SYear");
        $EdDate = ScriptRunner($Script, "EYear");
    } else {

        $Script = "Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='" . ECh($_SESSION["StkTck" . "HKey"]) . "'";
        // var_dump($Script);
        //Check what year the anniversary falls into [Last year to this year] or [This year to next year]
        $StDate = ScriptRunner($Script, "Dt") . ' ' . ScriptRunner($Script, "Yr");
        if (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
            $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") + 1);
        } elseif (strtotime($StDate) >= strtotime(ScriptRunner($Script, "TDay"))) {
            $StDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") - 1);
            $EdDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr"));
        }
    }
    $Script = "Select Accrue, NDays from LvSettings where HashKey='" . trim($row2['LID']) . "' and LID=(select EmpLeaveGp from EmpTbl where HashKey='" . $row2['EmpHKey'] . "')";
    //echo $Script;
    $kk = ScriptRunner($Script, "Accrue");
    if ($kk == 1) {
        $kk = ScriptRunner($Script, "NDays");
        if ($kk > 0) {
            $Accu = number_format((($kk / 365) * $Accrued_Months), 0);
            echo $Accu;
        }
    } else {
        $Accu = ScriptRunner($Script, "NDays");
        echo $Accu;
    }
    ?>
											days</td>
										</tr>
										<tr>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Manager:</td>
											<td colspan="2" align="left" valign="middle" class="TinyTextFail"><?php
$Script = "select Count(*) as Ct from EmpTbl where HashKey='" . $row2['EmpMgr'] . "'";
    if (ScriptRunner($Script, "Ct") > 0) {
        $Script = "select [SName] +' '+ [FName]+ ' ['+[EmpID]+']' as Ct from EmpTbl where HashKey=(
															select HashKey from EmpTbl where HashKey='" . $row2['EmpMgr'] . "')";
        echo ScriptRunner($Script, "Ct");
    }?></td>
											<td align="center" class="smallButton_disabled">Status1: <?php
if ($row2['Verified'] == 1) {echo "Verified";} else {echo "Unverified";}
    ?></td>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Used:</td>
											<td class="TinyText">
												<?php
//Leave dates already set from USER DAYS ABOVE
    $Script = "select COUNT(*) Ct from LvIndTOff where (Status='A') and LType='" . $row2['LID'] . "'
												and ([LvDate]
												between
												('" . $StDate . "')
												and
												('" . $EdDate . "')
												)
												and EmpID='" . $row2['EmpHKey'] . "'";
    // var_dump($Script);
    $UsedTO = ScriptRunner($Script, "Ct");
    echo $UsedTO;
    //echo $Script;
    //LType
    ?>
											days</td>
										</tr>
										<tr>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Backup Employee:</td>
											<td>

												<!-- ====================================================== -->
												<?php
$verified_status = $row2['Verified'];
    if ($verified_status == 0) { //Not Verified

        print "<select name=\"Subst$count\" class=\"TextBoxText\" id=\"Subst\">";

        $subst_id = $row2['Subst'];
        //if ($emp_id != '' && $emp_id != '--')
        //{

        $Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $subst_id . "'";

        $empnm = ScriptRunner($Script_Edit, "Nm");
        echo '<option selected="selected" value="' . $subst_id . '">' . $empnm . '</option>';
        //echo '<option value="'.$EditID.'">'. $empnm .'</option>';
        //}

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        $AllEmployee = '';

        $dbOpen7 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and EmpStatus='Active' order by SName Asc");
        include '../login/dbOpen7.php';

        while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {
            $AllEmployee = $AllEmployee . '<option value="' . $row7['HashKey'] . '">' . $row7['SName'] . " " . $row7['FName'] . " " . $row7['ONames'] . " [" . $row7['EmpID'] . "]" . '</option>';
        }
        echo $AllEmployee;
        //include '../login/dbClose2.php';

        print "</select>";

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    } elseif ($verified_status == 1) { //Verified

        $subst_id = $row2['Subst'];

        $Script_Edit = "Select EmpID, (SName + ' ' + FName + ' ' + ONames+' [' + EmpID + ']') as Nm, * from [EmpTbl] where HashKey='" . $subst_id . "'";

        $empnm = ScriptRunner($Script_Edit, "Nm");
        echo $empnm;

    }
    $count++;
    ?>
												<!-- ====================================================== -->



											</td>
											<td>&nbsp;</td>
											<td align="center" class="smallButton_disabled">Status2: <?php
if ($row2['Status'] == 'A') {echo "Approved";} else {echo "Pending";}
    ?></td>
											<td align="right" valign="middle" class="tdMenu_HeadBlock_Light">Balance:</td>
											<td><?php echo ($Accu - $UsedTO); ?></td>
										</tr>
									</table></td>
								</tr>
								<?php
}
include '../login/dbClose2.php';?>
      						</tbody>
      					</table>
      				</div>
      				<div class="col-md-9"></div>
      				<div class="col-md-3 pt-5">
      					<?php
if (isset($_REQUEST["PgTy"])) {
    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
	      					<input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
	      						<input name="FAction" id="FAction" type="hidden">
	      						<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
} else {
    echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
	      					<input name="PgTy" id="PgTy" type="hidden" />
	      						<input name="FAction" id="FAction" type="hidden">
	      						<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">';
}
//Check if any record was spolled at all before enabling buttons
//Check if user has Delete rights

if (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "VERIFIED")) {
    if (ValidateURths("VERIFY TIMEOFF" . "A") == true) { // Cancel
        //$but_HasRth=("VERIFY ** TIMEOFF"."T"); $but_Type = 'Cancel'; include '../main/buttons.php';
        $but_HasRth = ("VERIFY ** TIMEOFF" . "A");
        $but_Type = 'Cancel';include '../main/buttons.php';
    }

    if (ValidateURths("VERIFY TIMEOFF" . "D") == true) { // Cancel
        //$but_HasRth=("VERIFY TIMEOFF".""); $but_Type = 'Unverify'; include '../main/buttons.php';
        $but_HasRth = ("VERIFY TIMEOFF" . "D");
        $but_Type = 'Unverify';include '../main/buttons.php';
    }

} elseif (isset($_REQUEST['SubmitTrans']) && ($_REQUEST['SubmitTrans'] == "UNVERIFIED")) {
    if (ValidateURths("VERIFY TIMEOFF" . "A") == true) { // Cancel
        //$but_HasRth=("VERIFY TIMEOFF"."D"); $but_Type = 'Cancel'; include '../main/buttons.php';
        $but_HasRth = ("VERIFY TIMEOFF" . "A");
        $but_Type = 'Cancel';include '../main/buttons.php';
    }

    if (ValidateURths("VERIFY TIMEOFF" . "D") == true) { // Cancel
        //$but_HasRth=("VERIFY TIMEOFF"."T"); $but_Type = 'Verify'; include '../main/buttons.php';
        $but_HasRth = ("VERIFY TIMEOFF" . "D");
        $but_Type = 'Verify';include '../main/buttons.php';
    }
}
?>
      				</div>

    </tr>

<!-- ======================================== Pagination ===================================-->
	</form>
</table>
</body>

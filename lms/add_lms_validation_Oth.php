<?php
$LeaveID = "";
$Script_lv = "Select *,Convert(Varchar(11),[SetStart],106) SDt, Convert(Varchar(11),[SetEnd],106) EDt from Settings where [Setting]= 'LeaveTyp'";

$leave_type = ScriptRunner($Script_lv, "SetValue");
$end_type = ScriptRunner($Script_lv, "EDt");
$leave_year = date("Y", strtotime($end_type));
$current_year = date('Y');

if ($leave_type === 'Period' && $leave_year !== $current_year) {
    echo ("<script type='text/javascript'>{ parent.msgbox('Sorry you can not book leave at this period. kindly contact the administrator ', 'red'); }</script>");
    $GoValidate = false;
}

if (strlen(trim($_REQUEST["LID"])) != 32) {
    echo ("<script type='text/javascript'>{ parent.msgbox('Select a leave type to apply for', 'red'); }</script>");
    $GoValidate = false;
} else {
    $LeaveID = ECh($_REQUEST["LID"]);
}

if (trim($_REQUEST["StartDt"]) == "") {
    echo ("<script type='text/javascript'>{ parent.msgbox('Specify a start date', 'red'); }</script>");
    $GoValidate = false;
} else {
    $StartDate = ECh($_REQUEST["StartDt"]);
    $EndDate = ECh($_REQUEST["StartDt"]);
}

if (trim($_REQUEST["SMDays_H"]) == "Single") {
    $EndDate = ECh($_REQUEST["StartDt"]);
} else {
    if (trim($_REQUEST["StartDt"]) == "" || trim($_REQUEST["EndDt"]) == "") {
        echo ("<script type='text/javascript'>{ parent.msgbox('You must select a From and To date to continue', 'red'); }</script>");
        $GoValidate = false;
    } else {
        $EndDate = ECh($_REQUEST["EndDt"]);
    }

    if (strtotime($_REQUEST["StartDt"]) > strtotime($_REQUEST["EndDt"])) {
        echo ("<script type='text/javascript'>{ parent.msgbox('Your From Date must be a calendar date before the To Date', 'red'); }</script>");
        $GoValidate = false;
    }
}

//Select all public holidays
$Script = "Select Convert(Varchar(11),PDate,106) as PDt from LvPHolsOff where Status<>'D' and PDate >= '" . $StartDate . "'
UNION
select Convert(varchar(11),LvDate,106) as PDt from [LvIndTOff] where EmpID='" . ECh($_REQUEST["AcctNo_"]) . "'
 and Status not in('D','C','T','R')"; //Where the leave is not Authorized or cancelled
$SelID = ScriptRunnerJoin($Script, "PDt");
if (strlen($SelID) > 5) {
    $SelID = " and LvDate not in (" . $SelID . ") ";
} else {
    $SelID = "";
}

$Script = "Select Val1 from Masters where ItemName='Weekends'";
$Weekends = ScriptRunnerJoin($Script, "Val1");
if (strlen($Weekends) > 5) {
    $Weekends = " and (datename(WEEKDAY,LvDate) not in (" . $Weekends . ")) ";
} else {
    $Weekends = "";
}

$Script = "Select Count(*) Ct from LvDates where LvDate between '" . $StartDate . "' and '" . $EndDate . "' " . $Weekends . $SelID;
$Script_Edit = "Select Convert(varchar(11),LvDate,106) LvDate_ from LvDates where LvDate between '" . $StartDate . "' and '" . $EndDate . "' " . $Weekends . $SelID; //Used later to book indv days
$BookDays = intval(ScriptRunner($Script, "Ct"));

/*
$Script = "Select GpLeave from UGpRights where GpType='P' and HashKey=(Select GpName from Users where EmpID=(Select EmpID from EmpTbl where HashKey='".ECh($_REQUEST["AcctNo_"])."'))";
$LvGrp = ScriptRunner($Script,"GpLeave");
 */
$script = "Select Convert(varchar(11),EmpDt,106) as ED, EmpLeaveGp, EmpID from EmpTbl where HashKey='" . ECh($_REQUEST["AcctNo_"]) . "'";
$EmpDate = ScriptRunner($script, "ED");
$EmpolyeeID = ScriptRunner($script, "EmpID");
$LvGrp = ScriptRunner($script, "EmpLeaveGp");

$script = "Select GpName from Users where EmpID='" . $EmpolyeeID . "'";
$GpName = ScriptRunner($script, "GpName");

$script = "Select GpLeave from UGpRights where GpType='P' and HashKey='" . $GpName . "'";
//$LvGrp = ScriptRunner($script,"GpLeave");

if ($BookDays == 0) {
    echo ("<script type='text/javascript'>{ parent.msgbox('Employee selected off day(s) falls on a Defined Holiday or Weekend OR The selected Date has already been applied for, please check \"My Calendar\".', 'red'); }</script>");
    $GoValidate = false;
}

//$Script="Select MaxBtchDy from LvSettings LvSet where LvSet.LID=(Select GpLeave from UGpRights where HashKey='".$LvGrp."') and LvSet.LType='".$LeaveID."' and Status<>'D'";



//Check number of days left for the secified leave type. Take a count of individual days in LvIndOff where the leave is booked, authoried or take. Hence where it is not cancelled or deleted
$Script = "Select *, Convert(Varchar(11),[SetStart],106) SYear, Convert(Varchar(11),[SetEnd],106) EYear from Settings where [Setting]= 'LeaveTyp'";

if (ScriptRunner($Script, "SetValue") == "Period") {
    $StDate = ScriptRunner($Script, "SYear");
    $EdDate = ScriptRunner($Script, "EYear");
} else {
    $Script = "Select top 1 (Convert(Varchar(6),EmpDt,106)) as Dt, (Convert(Varchar(4),YEAR(GETDATE()))) as Yr, Convert(Varchar(11),GetDate(),106) as TDay from EmpTbl where HashKey='" . ECh($_REQUEST["AcctNo_"]) . "'";

    //Check what year the anniversary falls into [Last year to this year] or [This year to next year]
    $StDate = ScriptRunner($Script, "Dt") . ' ' . ScriptRunner($Script, "Yr");
    if (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
        $EDDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") + 1);
    } elseif (strtotime($StDate) < strtotime(ScriptRunner($Script, "TDay"))) {
        $StDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr") - 1);
        $EDDate = ScriptRunner($Script, "Dt") . ' ' . (ScriptRunner($Script, "Yr"));
    }
}

$Script = "Select DATEDIFF(D,'" . $StDate . "',GETDATE()) Mts";
$Accrued_Months = ScriptRunner($Script, "Mts"); //Used to divide to get Accrued days count - Accrued months is the start month minus the current month as an integer

$Script = "select Accrue,Subst, NDays from LvSettings LvSet where LvSet.LID='" . $LvGrp . "' and LvSet.HashKey='" . $LeaveID . "' and Status<>'D'";
$SetDays = ScriptRunner($Script, "NDays");
$AccruType = ScriptRunner($Script, "Accrue");

//CHECK IF BACKUP EMPLOYEE IS A MUST

/*
$SubSt = ScriptRunner($Script,"Subst");
if ($SubSt=="Yes")
{
//if (isset($_REQUEST["SubSt"]) && strlen($_REQUEST["SubSt"])!=32)
if(isset($_REQUEST["Subst"]) && $_REQUEST["Subst"]=="--"){

echo("<script type='text/javascript'>{parent.msgbox('You must select a backup employee to fill your position during this employees absence. Kindly review','red');}</script>");
$GoValidate = false;
}
}
 */

//-------------- Check if the employee has stayed long enough to apply for a leave --------------------

$Script = "select Convert(varchar(11),Furthest,106) as Fdate from LvSettings LvSet where LvSet.HashKey=('" . $LeaveID . "') and LvSet.PChild='C' and Status<>'D'";
$Furthest = ScriptRunner($Script, "Fdate");

$EmpDate_num = strtotime($EmpDate);
$todays_date = time();
$datediff = $todays_date - $EmpDate_num;
$date_diff = floor($datediff / (60 * 60 * 24));

//4 < 360
if ($date_diff < $Furthest) {

    echo ("<script type='text/javascript'>{parent.msgbox('Sorry; you have only been employeed for ($date_diff days). You will have to stay for ($Furthest days) before you can apply for leave.','red');}</script>");
    $GoValidate = false;
}

$Script = "(select COUNT(*) as Ct from LvIndTOff where (Status<>'C' and Status<>'D' and Status<>'R') and LType='" . $LeaveID . "' and ([LvDate] between ('" . $StDate . "') and ('" . $EdDate . "')) and EmpID='" . ECh($_REQUEST["AcctNo_"]) . "')";

$UsedDays = ScriptRunner($Script, "Ct");

//echo "Acc=".$Accrued_Months."Used=".$UsedDays;

if ($AccruType == 1) {
    $Balance = (number_format((($SetDays / 365) * $Accrued_Months), 0) - $UsedDays);
} else {
    $Balance = ($SetDays - $UsedDays);
}

$DaysLeft = intval($Balance) - intval($BookDays);

if ($DaysLeft < 0 && $GoValidate == true) {
    echo ("<script type='text/javascript'>{parent.msgbox('You do not have sufficient " .

        ScriptRunner("Select LType from LvSettings where Hashkey='" . $LeaveID . "'", "LType")
        . " days to book this timeoff.','red');}</script>");
    /*
    ScriptRunner("Select LType from LvSettings where Hashkey='".$LeaveID."'","LType")
    ." days to book this timeoff. (Leave ID:$LeaveID, Days Left:$DaysLeft(Bal:$Balance, Booked:$BookDays), Emp:$_REQUEST[AcctNo_])','red');}</script>");
     */

    $GoValidate = false;
}

if (isset($_REQUEST["Subst"]) && $_REQUEST["Subst"] != "--") {

    $Script = "select * from LvDetails where EmpID ='" . $_REQUEST["Subst"] . "' and Status ='A' order by ID desc";
    $bac = ScriptRunnercous($Script);
    if ($bac) {
        $s_day = $bac['SDate']->format('Y-m-d');
        $e_day = $bac['EDate']->format('Y-m-d');
        $r_day = date('Y-m-d', strtotime(ECh($_REQUEST["StartDt"])));
        if (($r_day >= $s_day) && ($r_day <= $e_day)) {
            echo ("<script type='text/javascript'>{parent.msgbox(' Backup employee is currently on leave. Kindly review','red');}</script>");
            $GoValidate = false;
        }
    }
}

$Script = "Select MaxBtchDy from LvSettings LvSet where LvSet.LID=('" . $LvGrp . "') and LvSet.HashKey='" . $LeaveID . "' and Status<>'D'";
$MaxDys = intval(ScriptRunner($Script, "MaxBtchDy"));

if (($BookDays > $MaxDys) && $MaxDys != 0) {
    echo ("<script type='text/javascript'>{ parent.msgbox('You applied for $BookDays days. You can only take a maximum of $MaxDys  day(s) at a time.', 'red'); }</script>");
    $GoValidate = false;
}

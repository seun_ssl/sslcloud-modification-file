<?php 
	session_start( );
	include '../login/scriptrunner.php';
	$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';
	//Validate user viewing rights
	if (ValidateURths("TIMEOFF REQUEST"."V")!=true){include '../main/NoAccess.php';exit;}
	$ModMaster=ValidateURths("LEAVE MODULE MASTERS"."A"); // MODULE MASTERS RIGHTS SETTINGS
	$Script="SELECT * FROM EmpTbl WHERE HashKey='".ECh($_SESSION["StkTck"."HKey"])."'";
	$EmpLeaveGp=ScriptRunner($Script,"EmpLeaveGp");
	if (strlen($EmpLeaveGp)!=32) {
		$Script="Select GpLeave from UGpRights where HashKey='".$_SESSION["StkTck"."UGrp"]."'";
		$EmpLeaveGp=ScriptRunner($Script,"GpLeave");
	}
	$Display = "";
	$ClsSet = "";
	$Accrued_Months ="";
	$department = ScriptRunner("SELECT Department FROM EmpTbl WHERE HashKey='".$_SESSION["StkTck"."HKey"]."'","Department");
	$year_start = ScriptRunner("SELECT CONVERT(varchar, SetStart, 102) AS start_date FROM Settings WHERE Setting = 'LeaveTyp'","start_date");
	$year_end = ScriptRunner("SELECT CONVERT(varchar, SetEnd, 102) AS end_date FROM Settings WHERE Setting = 'LeaveTyp'","end_date");
	$dbOpen2 = ("SELECT LType, HashKey FROM LvSettings WHERE Status='A' AND LID='".$EmpLeaveGp."'");
	include '../login/dbOpen2.php';
	while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
		$Script = "SELECT Accrue, NDays FROM LvSettings LvSet WHERE LvSet.LID='".$EmpLeaveGp."' AND LvSet.HashKey='".$row2['HashKey']."' AND Status<>'D'";
		$SetDays = ScriptRunner($Script,"NDays");
		$AccruType = ScriptRunner($Script,"Accrue");
		// var_dump($AccruType);
		$Script="SELECT SUM(days) AS Ct FROM leave_plan WHERE leave_id='".$row2['HashKey']."' AND emp_id='".$_SESSION["StkTck"."HKey"]."'";
		$UsedDays = (int) ScriptRunner($Script,"Ct");
		//CALCULATE AccrueD DAYS 
		//Setup Display
		// if ($AccruType==1) {
		// 	if ($Display=="") {
		// 		$Display=$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);
		// 	} else {
		// 		$Display=$Display.", ".$row2['LType'].":".(number_format( (($SetDays / 12) * $Accrued_Months) ,0) - $UsedDays);
		// 	}
		// } else {
			if ($Display=="") {
				// $Display=$row2['LType'].":".($SetDays - $UsedDays);
				$Display=$row2['LType'].":".($SetDays);
			} else {
				// $Display=$Display.", ".$row2['LType'].":".($SetDays - $UsedDays);
				$Display=$Display.", ".$row2['LType'].":".($SetDays);
			}
		// }
	}
	include '../login/dbClose2.php';
	$icount=0;//GET WEEKEND DAYS TO DEACTIVATE ON CALENDAR
	$dbOpen2 = ("SELECT HashKey, 'Holiday' AS lvType, 0 AS DtDiff, PName AS Nm, PName AS PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,PDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,PDate))) AS PubHol, (CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))+','+CONVERT(VARCHAR(4),(DATEPART(M,PDate)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,PDate) + 1)) AS PubHolEnd, (CONVERT(VARCHAR(4),DATEPART(M,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))) AS start_date, (CONVERT(VARCHAR(4),DATEPART(M,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(D,PDate))+'-'+CONVERT(VARCHAR(4),DATEPART(YEAR,PDate))) AS end_date FROM [LvPHolsOff] WHERE Status='A' UNION SELECT emp_id AS HashKey, leave_id AS lvType, DateDiff(D,GetDate(),end_date) DtDiff, (SELECT SName+' '+FName FROM EmpTbl WHERE HashKey=lv.emp_id) AS Nm, (SELECT LType FROM LvSettings WHERE HashKey=lv.leave_id) AS PDes, (CONVERT(VARCHAR(4),DATEPART(YEAR,start_date))+','+CONVERT(VARCHAR(4),(DATEPART(M,start_date)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,start_date))) AS PubHol, (CONVERT(VARCHAR(4),DATEPART(YEAR,end_date))+','+CONVERT(VARCHAR(4),(DATEPART(M,end_date)-1))+','+CONVERT(VARCHAR(4),DATEPART(D,end_date) + 1)) AS PubHolEnd, CONVERT(VARCHAR(106), start_date) AS start_date, CONVERT(VARCHAR(106), end_date) AS end_date FROM [leave_plan] lv WHERE emp_id IN (SELECT Hashkey FROM EmpTbl WHERE Department = '{$department}')");
	include '../login/dbOpen2.php';
	$PubHols = '';
	$events = [];
	while($row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
		$icount = $icount + 1;
		// if ($row2['Status'] == "A")
		// {$ClsSet=", className: ['smallButton_Auth']";}
		// else
		// {$ClsSet=", className: ['smallButton']";}
		if ($_SESSION["StkTck"."HKey"] === $row2['HashKey']) {
			$ClsSet=", color: '#255b61',";
			$events[$row2['lvType']][] = ['start' => $row2['start_date'], 'end' => $row2['end_date']];
		} else {
			$ClsSet=", color: '#059caf',";
		}
		if ($row2['lvType'] == 'Holiday') {
			$ClsSet=", color: '#7c277d',";
		}
		$editable = ($_SESSION["StkTck"."HKey"] == $row2['HashKey']) ? ", editable: true" : ", editable: false";
		$title = ($_SESSION["StkTck"."HKey"] === $row2['HashKey'] || ($row2['Nm'] === $row2['PDes'])) ? "" : '('.$row2['Nm'].')';
		$PubHols = $PubHols.", {hashkey: '".$row2['HashKey']."', leave: '".$row2['lvType']."', title: '".$row2['PDes'].' '.$title."', start: new Date(".$row2['PubHol']."), end: new Date(".$row2['PubHolEnd'].")".$editable.", allDay: true ".$ClsSet." }";
	}
	include '../login/dbClose2.php';
	$PubHols = ltrim($PubHols, " ,");
	$events = (count($events)) ? json_encode($events) : '{}';
?>
<link href='../packages/core/main.css' rel='stylesheet' />
<link href='../packages/daygrid/main.css' rel='stylesheet' />
<script src='../packages/core/main.js'></script>
<script src='../packages/interaction/main.js'></script>
<script src='../packages/daygrid/main.js'></script>
<script>
	const year_start = '<?php echo $year_start ?>'.split('.').map(Number);
	const year_end = '<?php echo $year_end ?>'.split('.').map(Number);
	// console.log(year_start);
	// console.log(year_end);
	function formatDate(date) {
		let day = date.getDate();
		let month = date.getMonth()+1; 
		let year = date.getFullYear();
		if(day < 10) {
		    day = `0${day}`;
		} 
		if(month < 10) {
		    month = `0${month}`;
		} 
		return `${year}-${month}-${day}`;
	}

	function subtract(date, days = 1) {
		const value = new Date(date.toISOString());
		return new Date(value.setDate(value.getDate() - days));
	}
	//Get Users Hashkey
	const events = <?php echo $events; ?>;
	const hashkey = "<?php echo $_SESSION["StkTck"."HKey"] ?>";
	const tents = {};
	const leaves = ("<?php echo $Display ?>").trim().split(",");
	leaves.forEach(function(element, index) {
		const temp = element.split(':');
		tents[temp[0].trim()] = parseInt(temp[1]);
	});
	function createEvent(info) {
		const leave_type = info.event.extendedProps.leave;
		const start = formatDate(info.event.start);
		const end = info.event.end ?  formatDate(subtract(info.event.end)) : start;
		if (events.hasOwnProperty(leave_type)) {
			events[leave_type].push({start, end});
		} else {
			events[leave_type] = [
				{start, end}
			];
		}
	}
	function deleteEvent(event) {
		const leave_type = event.extendedProps.leave;
		const start = formatDate(event.start);
		if (events.hasOwnProperty(leave_type)) {
			const index = events[leave_type].findIndex(function(i) {
				return i.start === start
			});
			events[leave_type].splice(index, 1);
		}
		console.log(events);
	}
	// https://codepen.io/subodhghulaxe/pen/qEXLLr
	// Check if Event is dragged over thrash div
	function isEventOverTrashDiv(x, y) {
		const trash_div = $('#fcTrash');
        const offset = trash_div.offset();
        offset.right = trash_div.width() + offset.left;
        offset.bottom = trash_div.height() + offset.top;
         // Compare
         return (x >= offset.left && y >= offset.top && x <= offset.right && y <= offset .bottom) ? true : false;
	}

	function createEle() {
		const entries = Object.entries(tents)
		for (const [key, value] of entries) {
			const id = key.trim().replace(/\s+/g,"-").toLowerCase();
			$(`#${id}`).text(` (${value}) `);
		}
	}
	//Only do this when DOM content has Loaded
	document.addEventListener('DOMContentLoaded', function() {
		createEle();
		const Calendar = FullCalendar.Calendar;
		const Draggable = FullCalendarInteraction.Draggable;
		const containerEl = document.getElementById('external-events');
		const calendarEl = document.getElementById('calendar');
		const checkbox = document.getElementById('drop-remove');
		// Initialize the external events
		new Draggable(containerEl, {
			itemSelector: '.fc-event',
			eventData: function(eventEl) {
			  return {
			    // title: eventEl.innerText,
			    title: eventEl.dataset.title,
			    hashkey, //Store the hashkey of current user in each event extended props
			    leave: eventEl.dataset.leave,
			    type: '', //Do not remove
			    color: '#255b61',
			  };
			}
		});
		// Initialize the calendar
		const calendar = new Calendar(calendarEl, {
			plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
			//Calender is editable
		    editable: true,
		    //Events can be dropped onto the calendar
		    droppable: true,
		    //Animation(ms) on unsuccessful drag
		    dragRevertDuration: 0,
		    //Don not show weekends
		    weekends: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay'
			},
			validRange: {
                start: new Date(year_start[0], year_start[1]-1, year_start[2]),
                end: new Date(year_end[0], year_end[1]-1, year_end[2]),
            },
            eventConstraint: {
                start: new Date(),
                end: new Date(year_end[0], year_end[1]-1, year_end[2]),
            },
			// Events List
		    events: [
		    	<?php echo $PubHols;?>,
		    ],
		    //Allow events to overlap if it doesn't belong to same user
		    eventOverlap: function(stillEvent, movingEvent) {
		    	return stillEvent.extendedProps.hashkey !== hashkey && stillEvent.extendedProps.leave != 'Holiday';
	    	},
		    //New Event has droppeed
		    eventReceive: function(info) {
		    	//create the event
		     	createEvent(info);
		     	//get type of event
		     	const key = info.draggedEl.dataset.title;
		     	//get current count of event
		        const count = tents[key];
		        if (count > 0) {
		        	//subtract count by 1
		        	tents[key] = count - 1;
		        	createEle();
		        } else {
		        	//event count used up, delete and remove event
		        	deleteEvent(info.event);
	        	 	info.event.remove();
		        }
		        // if so, remove the element from the "Draggable Events" list
				//info.draggedEl.parentNode.removeChild(info.draggedEl);
		    },
		    //When event has stopped dragging
		    eventDragStop: function(e) {
		    	// Is user trying to delete Event
				if (isEventOverTrashDiv(e.jsEvent.clientX, e.jsEvent.clientY)) {
					const key = e.event.title.replace(/\s+$/g, '');
					let iteration = 0;
					let start = formatDate(e.event.start);
					let end = e.event.end ?  formatDate(subtract(e.event.end)) : start;
					const date_diff = (new Date(end) - new Date(start)) / 86400000 + 1;
					//Count the number of weekends between the start and end date
					while (start <= end) {
						const day = new Date(start).getDay();
						if (day === 0 || day === 6) {
							iteration++;
						}
						start = formatDate(subtract(new Date(start), -1));
					}
					tents[key] = tents[key] + date_diff - iteration;
					createEle();
					deleteEvent(e.event)
				    e.event.remove();
				}
	        },
	        //When user moves event accross calender
		    eventDrop: function(info) {
		    	deleteEvent(info.oldEvent);
		    	createEvent(info);
		    },
			eventResize: function(info) {
				//Get number of dates extended...If negative, user is moving backwards
				let extended_by = info.endDelta.days;
				//Event key name
				const key = info.event.title;
		     	//get current count of event
		        const count = tents[key];
		        //Iteration counter
		        let iteration = 0;
	        	/**
	        		Set start date is the previous end date if set else use the new event start date
	        		and endate is the new end date if set else use start date
	        	*/
				let start = info.prevEvent.end ?  formatDate(subtract(info.prevEvent.end)) : formatDate(info.event.start);
				let end = info.event.end ?  formatDate(subtract(info.event.end)) : start;
		        /**
	        		If user is moving backward, start date is the new event enddate and endate is the previous enddate
	        	*/
		        if (extended_by < 0) {
					start = formatDate(subtract(info.event.end));
					end = formatDate(subtract(info.prevEvent.end));
		        }
		        //Count the number of weekends between the start and end date
				while (start <= end) {
					const day = new Date(start).getDay();
					if (day === 0 || day === 6) {
		        		//If user is moving forward, increment count
						if (extended_by > 0) {
							iteration++;
						} else {
							//If user is moving forward, decrement count
							iteration--;
						}
					}
					start = formatDate(subtract(new Date(start), -1));
				}
				//subtract the iteration from the extended days
				extended_by -= iteration;
				if (extended_by > count) {
					info.revert();
				} else {
					tents[key] = count - extended_by ;
			    	deleteEvent(info.prevEvent);
					createEvent(info);
					createEle();
				}
			}
		});
	  	calendar.render();
	});
</script>
<?php if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
<style>
	.fc-past{
		background: #dfdfdf !important;
	}
	.fc-today{
		background: #cadbff !important;
	}
	#calendar {
		width: 100%;
		margin: 0 auto;
	}
	.calendar-event, .fc-event {
	    background: #255b61;
	}
	.fc-event {
	    color: #fff!important;
	    font-weight: bold;
	}
	.btn-success {
	    background-color: #6dab69;
	    border-color: #6dab69;
	    color: #fff;
	}
	.overlay {
		position: fixed; /* Sit on top of the page content */
		display: block;
		width: 100%; /* Full width (cover the whole page) */
		height: 100%; /* Full height (cover the whole page) */
		top: 0; 
		left: 0;
		right: 0;
		bottom: 0;
		background-color: rgba(0,0,0,0.5); /* Black background with opacity */
		z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
		cursor: pointer; /* Add a pointer on hover */
	}
	#text{
	  position: absolute;
	  top: 30%;
	  left: 50%;
	  font-size: 50px;
	  color: white;
	  transform: translate(-50%,-50%);
	  -ms-transform: translate(-50%,-50%);
	}
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #6dab69;
		border-bottom: 16px solid #6dab69;
		width: 120px;
		height: 120px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
	}
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}
	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>
<body oncontextmenu="return false;">
	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<!-- https://www.w3schools.com/howto/howto_css_overlay.asp -->
					<div id="overlay">
						<!-- https://www.w3schools.com/howto/howto_css_loader.asp -->
						<div id="text"><div id="loader"></div></div>
					</div>
					<div class="row">
						<div class="col-2">
							<h2 id="fcTrash" class="calendar-trash">Trash <i class="fa fa-trash"></i></h2>
							<div id='external-events'>
								<?php
									$dbOpen2 = ("select LType, HashKey from LvSettings where LID='".$EmpLeaveGp."'");
									include '../login/dbOpen2.php';
									while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)): ?>
										<div class='fc-event' data-leave="<?php echo $row2['HashKey']?>"  data-title="<?php echo $row2['LType']?>"><?php echo $row2['LType'] ?><span id="<?php echo preg_replace('/\s+/', '-', preg_replace('/\s+/', ' ', trim(strtolower($row2['LType'])))) ?>"></span></div><br>
								<?php endwhile; include '../login/dbClose2.php'; ?>
							</div>
							<button type="button" class="btn btn-success btn-block" onClick="saveLeavePlan()">Save Plan</button>
						</div>
						<div class="col-10">
							<?php
								print "<div id='calendar'></div>";
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap 4.0-->
	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
																			
	<!-- SlimScroll -->
	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
			
																			
	<!-- FastClick -->
	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>
																			
	<!-- MinimalLite Admin App -->
	<script src="../assets/js/template.js"></script>
																			
	<!-- MinimalLite Admin for demo purposes -->
	<script src="../assets/js/demo.js"></script>
	<script>
		function saveLeavePlan() {
			$('#overlay').addClass('overlay');
			$('#loader').addClass('loader');
			$.post({
				url: 'save_lvPlan.php',
				data: {events, PgDoS: '<?php echo DoSFormToken() ?>'},
				cache: false,
				success(res) {
					parent.msgbox('Leave plan saved successfully.', 'green');
					$('#overlay').removeClass('overlay');
					$('#loader').removeClass('loader');
				},
				error(e) {
					parent.msgbox('Oops...An error occured. Try Again.', 'green');
					$('#overlay').removeClass('overlay');
					$('#loader').removeClass('loader');
				}
			})
		}
	</script>
</body>
<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
$Load_JQuery_Tags = false;
include '../css/myscripts.php';

//$Load_JQuery_Tags=true;
//Include is brought in below

if (!isset($_REQUEST["EID"])) {
    echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");
}

if (ValidateURths("EMPLOYEE" . "V") != true) {include '../main/NoAccess.php';exit;}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("EMPLOYEE" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [LnDetails] WHERE [HashKey]='" . $EditID . "'") != true) {include '../main/NoAuth.php';exit;} /*AuthEdit*/else {
                    $Script_CCode = "SELECT (SName+' '+FName+' ['+convert(varchar(24),EmpID)+']') Nm from EmpTbl where HashKey='" . $EditID . "'";
                    $Script = "UPDATE [LnDetails]
					SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'A'
						WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    $Script = "UPDATE [LnIndvOff]
					SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'A'
						WHERE [Status] In ('N','U') and [LID] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    AuditLog("AUTHORIZE", "Authorization of employee loan request [" . ScriptRunner($Script_CCode, "Nm") . "].");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee loan request authorized successfully.', 'green'); }</script>");

                    //Send loan request approval to emploee with details
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && isset($_REQUEST["DelMax"]) && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("EMPLOYEE" . "T") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [LnDetails] WHERE [HashKey]='" . $EditID . "'") == true) {include '../main/NoAuth.php';exit;} /*AuthEdit*/else {
                    $Script_CCode = "SELECT (SName+' '+FName+' ['+convert(varchar(24),EmpID)+']') Nm from EmpTbl where HashKey='" . $EditID . "'";
                    $Script = "UPDATE [LnDetails]
					SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'U'
						WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    $Script = "UPDATE [LnIndvOff]
					SET [AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[AuthDate] = GetDate()
					  ,[Status] = 'U'
						WHERE [Status] In ('A') and [LID]='" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");

                    AuditLog("AUTHORIZE", "unauthorization of employee loan request [" . ScriptRunner($Script_CCode, "Nm") . "].");

                    echo ("<script type='text/javascript'>{ parent.msgbox('Selected employee loan request unauthorized successfully.', 'green'); }</script>");

                    //Send loan request approval to emploee with details
                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Activate Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("EMPLOYEE" . "A") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") != true) {
                    // var_dump('here');
                    include '../main/NoAuth.php';
                    exit;
                    // echo("<script>location.href = '../hrm/cr_NwEmp?PgTy=Create&AcctNo=".$EditID."&rffbf6545bghjj89jyl8r&amp;pg=jhhyg54fgvgft65t6r53u&amp;mylogin=c&amp;ad=true';</script>");
                    // exit;
                } /*AuthEdit*/

                else {

                    // $script_edit = "SELECT TOP 1  *, DATEDIFF(MONTH, SRepayDate,ERepayDate) GPeriod
					// from LnDetails where Status in ('B') AND HashKey = '" . $EditID . "' ORDER BY SRepayDate";

					// for now will have to revert back
                    $script_edit = "SELECT TOP 1  *, DATEDIFF(MONTH, SRepayDate,ERepayDate) GPeriod
					from LnDetails where HashKey = '" . $EditID . "' ORDER BY SRepayDate";

              

                    $month_interval = ScriptRunner($script_edit, "GPeriod");
                   

                    $script_update = "UPDATE LnDetails
					SET Status = 'A',
					SRepayDate = GETDATE(),
					ERepayDate = DateAdd(m," . $month_interval . ",GETDATE())
					WHERE HashKey = '" . $EditID . "'
					";
       
                    ScriptRunnerUD($script_update, "Inst");
           
                    $month_counter = 0;
                    $dbOpen2 = "SELECT * FROM LnIndvOff
					WHERE LID = '" . $EditID . "' AND PaidBy is NULL AND PaidDate is NUll AND Status NOT IN ('P','D')
					ORDER BY ExpPayDate";
                    include '../login/dbOpen2.php';
                    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

                        $script_update = "UPDATE LnIndvOff
					SET [Status] = 'A',
					AuthDate = GETDATE(),
					ExpPayDate = DateAdd(M," . $month_counter . ",GETDATE())
					WHERE LID = '" . $EditID . "' AND HashKey = '" . $row2["HashKey"] . "' ";
                        ScriptRunnerUD($script_update, "Inst");

                        $month_counter++;
                    }
                    include '../login/dbClose2.php';
                    echo ("<script type='text/javascript'>{parent.msgbox('Selected loan account(s) activated successfully.','green'); }</script>");
                    echo ("<script>location.href = './Bd_EmpLn';</script>");

                }
            }
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("EMPLOYEE" . "D") != true) {include '../main/NoAccess.php';exit;}

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                if (ValidateUpdDel("SELECT Status from [EmpTbl] WHERE [HashKey]='" . $EditID . "'") == true) {
                    $Script_CCode = "SELECT EmpID from EmpTbl where HashKey='" . $EditID . "'";
                    $Script = "UPDATE [EmpTbl]
					SET [DeletedBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
					  ,[DeletedDate] = GetDate()
					  ,[Status] = 'D'
						WHERE [HashKey] = '" . $EditID . "'";
                    //                echo $Script;
                    ScriptRunnerUD($Script, "Delete");
                    AuditLog("DELETE", "Deletion of Staff account [" . ScriptRunner($Script_CCode, "EmpID") . "].");

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected employee account(s) deleted successfully.','green'); }</script>");
                }
            }
        }
    }
}

include '../css/myscripts.php';
?>

<script>
  $(function() {
    $( document ).tooltip();
  });
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/style_main.css" rel="stylesheet" type="text/css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<body oncontextmenu="return false;">
	<form action="#" method="post" name="Records" target="_self" id="Records">
		<div class="box">
			<div class="box-header with-border tdMenu_HeadBlock pt-2 pb-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<p style="color: #000; margin-bottom: 4px;">
						Bad Employee Loans
						</p>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<!-- <tr class="tdMenu_HeadBlock_Light">
						<td align="right"><span class="TinyText">
						  <?php //$Load_Date=true; $Load_Search=true; $Load_Auth=true; include '../main/report_opt.php'; ?>
						</span></td>
						</tr> -->
						<table id="table" class="tablesorter table table-responsive">
							<thead>
								<tr>
									<th valign="middle" scope="col" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label></th>
									<th  align="center" valign="middle" scope="col">Employee</th>
									<th align="center" valign="middle" scope="col">Requested</th>
									<th  align="center" valign="middle" scope="col"> Repayment</th>
									<th align="center" valign="middle" scope="col">Interest</th>
									<th  align="center" valign="middle" scope="col">Repay Start</th>
									<th  align="center" valign="middle" scope="col">Repay End</th>
									<th align="center" valign="middle" scope="col">Tenure</th>
									<th  align="center" valign="middle" scope="col" >Pending Amount</th>
									<th align="center" valign="middle" scope="col" >Status</th>
								</tr>
							</thead>
							<tbody>
						        <?php
/*
if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] <> '') && (isset($_REQUEST["E_RptDate"]) && $_REQUEST["E_RptDate"] <> ''))
{$PayMthDt = " and EmpDt between '".ECh($_REQUEST["S_RptDate"])."' and '".ECh($_REQUEST["E_RptDate"])."' ";}
else
{$PayMthDt = ''; }
 */
$OutSPay = 0;
$ExpPay = 0;
$LnAmt = 0;
$Del = 0;
$RePayAmt = 0;
//if (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Authorized")
//{

$Kk = "";
$Script = "Select distinct(LID) Ld from LnIndvOff where (datediff(D,GETDATE(),ExpPayDate)) <= -30 and Status NOT IN ('P','D')";

$Kk = ScriptRunnerJoin($Script, "Ld");

if ($Kk === "") {
    $Kk = '1234';
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, * from LnDetails where Status in ('B') or HashKey IN (" . $Kk . ")  order by SRepayDate asc");

} else {
    $dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, * from LnDetails where Status in ('B') or HashKey IN (" . $Kk . ")  order by SRepayDate asc");
}

$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, * from LnDetails where Status in ('B') or HashKey IN (" . $Kk . ")  order by SRepayDate asc");

/*}
elseif (isset($_REQUEST["SearchType"]) && $_REQUEST["SearchType"] == "Unauthorized")
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, * from LnDetails where Status in ('N','U') order by SRepayDate asc");
}
else
{exit;}
v
 */

include '../login/dbOpen2.php';

//mssql_num_rows
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {

    //        return ($row2['$rwfield']);
    $Del = $Del + 1;
    //if ($Del==1) {$PrevID=$row2['ID'];} else {$NextID=$row2['ID'];}
    ?>
								<tr>
									<td valign="middle" scope="col" class="TinyTextTight">
										<?php
if ($row2['Status'] == 'P') {
        $SelID = " disabled ";
    } else {
        $SelID = "";
    }?>
										<input name="<?php echo ("DelBox" . $Del); ?>"  <?php echo $SelID; ?> type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
										<td align="center" valign="middle" scope="col"><?php $Script = "Select (SName+' '+FName+' ['+Convert(varchar(24),EmpID)+']') Nm from EmpTbl where HashKey='" . $row2['EID'] . "'";
    echo ScriptRunner($Script, "Nm");?></td>
										<td align="right" valign="middle" scope="col"><?php echo number_format($row2['LnAmt'], 2, '.', ',');
    $LnAmt = $LnAmt + $row2['LnAmt']; ?></td>
										<td align="right" valign="middle" scope="col"><?php echo number_format($row2['RePayAmt'], 2, '.', ',');
    $RePayAmt = $RePayAmt + $row2['RePayAmt']; ?></td>
										<td align="center" valign="middle" scope="col"><?php echo (trim($row2['IntRate'])); ?></td>
										<td align="center" valign="middle" scope="col"><?php echo $row2['SD']; ?></td>
										<td align="center" valign="middle" scope="col"><?php echo (trim($row2['ED'])); ?></td>
										<td align="center" valign="middle" scope="col"><?php echo (trim($row2['Tenure'])); ?></td>
										<td align="right" valign="middle" scope="col"><?php
$PAmt = 0;
    $Script = "Select Sum(Paid) Pd from LnIndvOff where Status='P' and LID='" . $row2['HashKey'] . "'";
    $PAmt = ScriptRunner($Script, "Pd");

    echo number_format($row2['RePayAmt'] - $PAmt, 2, '.', ',');
    $OutSPay = $OutSPay + $row2['OutSPay'];
    ?></td>
										<td align="center" valign="middle" scope="col">
											<?php

    $Script = "Select LID, min((datediff(D,GETDATE(),ExpPayDate))) Dy from LnIndvOff where (datediff(D,GETDATE(),ExpPayDate)) <= -30 and Status NOT IN ('P','D')  group by LID";

    $Dy = ScriptRunner($Script, "Dy");
    if ($Dy < -15 && $Dy > -30) {echo ('<img src="../images/green.png" height="18" title="' . $Dy . ' days overdue" /><img src="../images/amber.png" height="18" title="' . $Dy . ' days overdue" />');} elseif ($Dy < -30 && $Dy > -45) {echo ('<img src="../images/amber.png" height="18" title="' . $Dy . ' days overdue" /><img src="../images/amber.png" height="18" title="' . $Dy . ' days overdue" />');} elseif ($Dy < -45 && $Dy > -60) {echo ('<img src="../images/amber.png" height="18" title="' . $Dy . ' days overdue" /><img src="../images/red.png" height="18" title="' . $Dy . ' days overdue" />');} elseif ($Dy < -60) {echo ('<img src="../images/red.png" height="18" title="' . $Dy . ' days overdue" /><img src="../images/red.png" height="18" title="' . $Dy . ' days overdue" />');}
    ?></td>
									</tr>
									<?php }
include '../login/dbClose2.php';?>
							</tbody>
							<tfoot>
								<tr>
									<th valign="middle" scope="col" data-sorter="false"></th>
									<th align="center" valign="middle" scope="col"></th>
									<th align="right" valign="middle" scope="col"><span class="subHeader pull-right"><?php echo number_format($LnAmt, 2, '.', ','); ?></span></th>
									<th align="right" valign="middle" scope="col"><span class="subHeader pull-right"><?php echo number_format($RePayAmt, 2, '.', ','); ?></span></th>
									<th align="center" valign="middle" scope="col"><span class="subHeader pull-right"><?php echo number_format(($RePayAmt - $LnAmt), 2, '.', ','); ?></span></th>
									<th colspan="2" align="center" valign="middle" scope="col"></th>
									<th align="right" valign="middle" scope="col">&nbsp;</th>
									<th  align="right" valign="middle" class="subHeader" scope="col" data-sorter="false"><span class="pull-right"><?php echo number_format($OutSPay, 2, '.', ','); ?></span></th>
									<th align="right" valign="middle" class="subHeader" scope="col" data-sorter="false">&nbsp;</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="col-md-6 pt-5">
				    	<?php include '../main/pagination.php';?>
				  	</div>
				  	<div class="col-md-6 pt-5">
				  		<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-9">
						  		<?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" />
									  <input name="ActLnk" id="ActLnk" type="hidden" value="">
						  		      <input name="PgTy" id="PgTy" type="hidden" value="' . $_REQUEST["PgTy"] . '" />
						  			  <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
						  			  <input name="FAction" id="FAction" type="hidden">';

//Check if any record was spolled at all before enabling buttons
//Check if user has Delete rights
// $but_HasRth=("EMPLOYEE"."D"); $but_Type = 'Delete'; include '../main/buttons.php';
//Check if user has Add/Edit rights
$but_HasRth = ("EMPLOYEE" . "A");
$but_Type = 'Activate';include '../main/buttons.php';
//Check if user has Authorization rights
// $but_HasRth=("EMPLOYEE"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';
 ?>
						  	</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</form>
</body>

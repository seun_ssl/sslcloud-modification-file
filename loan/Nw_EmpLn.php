<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=false; $Load_JQuery_Tags=true; include '../css/myscripts.php';


//Validate user viewing rights
if (ValidateURths("EMPLOYEE LOAN"."V")!=true){include '../main/NoAccess.php';exit;}

if (!isset($EditID)){$EditID="";}
if (!isset($Script_Edit)){$Script_Edit="";}
if (!isset($Script)){$Script="";}
if (!isset($HashKey)){$HashKey="";}
if (!isset($Del)){$Del=0;}
if (!isset($EmpID)){$EmpID="";}
if (!isset($AllEmployee)){$AllEmployee="";}

$GoValidate=true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");


if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32)
{unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);

/*
$Script ="SELECT Status FROM Fin_PRCore WHERE EmpID ='".ECh($_REQUEST["AcctNo"])."'";
$sel_status = ScriptRunner($Script,"Status");
if($sel_status != "A"){
	echo ("<script type='text/javascript'>{parent.msgbox('Sorry your Loan Payment Template is not Created or Unapproved ', 'red');}</script>");
	$GoValidate = false;
}
*/

	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Get Details")
	{
		
		if (isset($_REQUEST["Tenure"]) && $_REQUEST["Tenure"]<1)
		{
			echo ("<script type='text/javascript'>{ parent.msgbox('Loan tenure must be a minimum of 1 month', 'red'); }</script>");
		}
		else
		{
			echo ("<script type='text/javascript'>{ parent.msgbox('Loan tenure must be a minimum of 1 month', 'red'); }</script>");
		}
	}

	if (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Accept Selected")
	{
		//$Script="Select * from LnCore where HashKey='".ddd."'";
		//$LID= ScriptRunner($Script,"HashKey");
		
		
		
		
		if (floatval($_REQUEST["OutSTotalAmt"])>0)
		{
			echo ("<script type='text/javascript'>{parent.msgbox('Total repayment amount must be equal to the amount granted ', 'red');}</script>");
			$GoValidate = false;			
		}

		if ($GoValidate == true) //Check if loan amount qualifies from this employees salary
		{
			$Script="select Tenure from LnCore where HashKey='".ECh(trim($_REQUEST["LoanScheme"]))."' and Status<>'D'";
			$PayChk = ScriptRunner($Script,"Tenure"); //Get Maximum stay period set in the Loan Settings table

			if ($PayChk < number_format(ECh(trim(str_replace(",","",$_REQUEST["Tenure"]))),2,'.','')) //If set max tenure is less than given tenure
			{
				echo ("<script type='text/javascript'>{parent.msgbox('The selected tenure exceeds the maximum set tenure of <strong>".$PayChk." months</strong> for this loan scheme', 'red');}</script>");
				$GoValidate = false;
			}
		}

		if ($GoValidate == true) //Check if loan amount qualifies from this employees salary
		{
			$Script="select MaxStayP from LnCore where HashKey='".ECh(trim($_REQUEST["LoanScheme"]))."' and Status<>'D'";
			$PayChk = ScriptRunner($Script,"MaxStayP"); //Get Maximum stay period set in the Loan Settings table

			$Script="select DateDiff(M, EmpDt, GETDATE()) StayP from EmpTbl where HashKey='".ECh(trim($_REQUEST["ApprEmpID"]))."' and Status<>'D' and EmpStatus='Active'"; //Get Total months of employement of employee

			if ($PayChk > ScriptRunner($Script,"StayP")) //Check stay period
			{
				echo ("<script type='text/javascript'>{parent.msgbox('The selected employee does not meet the required stay period of <strong>".$PayChk." months</strong> to qualify for this loan scheme', 'red');}</script>");
				$GoValidate = false;
			}
		}

		if ($GoValidate == true) //Check if loan amount qualifies from this employees salary
		{
			
			
			$Script="Select ((NetPay * 
			(select MaxSalPCent from LnCore where HashKey='".ECh(trim($_REQUEST["LoanScheme"]))."'))/100)
			MaxVal from Fin_PRCore where EmpID='".ECh(trim($_REQUEST["ApprEmpID"]))."' and Status='A'";
			
			$PayChk=ScriptRunner($Script,"MaxVal");
			
			
						
			
			if ($PayChk < number_format(ECh(trim(str_replace(",","",$_REQUEST["RePayAmt"]))),2,'.',''))
			{
				echo ("<script type='text/javascript'>{parent.msgbox('The amount given is more than the maximum approved amount of <strong>".$PayChk." </strong> for this employee on this loan scheme <br/> OR <br/> This Employee\'s Payroll Template has not been setup <br/> OR <br/> This Employee\'s Payroll Template has not been authorized.', 'red');}</script>");
				$GoValidate = false;
			}
		}
		
		
		if ($GoValidate == true)
		{
			$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."Loan".$Del.$_SESSION["StkTck"."UName"];
			$HashKey = md5($UniqueKey);


			if (isset($_REQUEST["Actn"]) && $_REQUEST["Actn"]=="RescheduleLn" && isset($_REQUEST["OldLID"]) && strlen($_REQUEST["OldLID"])==32)							
			{
				$HashKey = ECh($_REQUEST["OldLID"]);
				$Script="Update LnDetails set Status='P', FStatus='R' where HashKey='".$HashKey."'";
				//echo $Script."<br>";
				ScriptRunnerUD($Script,"");
				
				$Script="Update LnIndvOff set Status='D' where Status='A' and LID='".$HashKey."'";
				ScriptRunnerUD($Script,"");
				//echo $Script."<br>";
			}

			//Recreate loan with exactly same HashKey for LnDetails and LID for LnIndvOff
			$Script="insert into LnDetails ([EID]
	,[LnAmt],[Tenure],[RePayAmt],[IntRate],[LnDesc],[LoanScheme],[SRepayDate],[ERepayDate],[Status],[AddedBy],[AddedDate],[HashKey]) values ('".ECh(trim($_REQUEST["ApprEmpID"]))."',".number_format(ECh(trim(str_replace(",","",$_REQUEST["LnAmt"]))),2,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["Tenure"]))),0,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["RePayAmt"]))),2,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["IntRate"]))),2,'.','').",'".ECh(trim($_REQUEST["LnDesc"]))."','".ECh(trim($_REQUEST["LoanScheme"]))."','28 ".ECh(trim($_REQUEST["SRepayDate"]))."','28 ".ECh(trim($_REQUEST["ERepayDate"]))."','N','".$_SESSION["StkTck"."HKey"]."',GetDate(),'".$HashKey."')";
	//echo $Script."<br><br><br><br>";
			ScriptRunnerUD($Script,"Insert");
	
	//number_format(,2,'.',',')
	
			for ($i=1;$i<=$_REQUEST["DelMax"];$i++)
			{
				//$Del = $Del + 1;
				$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."Loan".$i.$_SESSION["StkTck"."UName"];
				$SubHashKey = md5($UniqueKey);
	
	
	//echo number_format(ECh(trim(str_replace(",","",$_REQUEST["OutSPay".$i]))),2,'.','')."<br><br><br>";
	
	
				$Script="insert into LnIndvOff ([EID],[LID]
			,[LnAmt],[Tenure],[ExpPay],[Paid],[OutSPay],[IntRate],[ExpPayDate],[Status],[AddedBy],[AddedDate],[HashKey]) values ('".ECh(trim($_REQUEST["ApprEmpID"]))."','".$HashKey."',".number_format(ECh(trim(str_replace(",","",$_REQUEST["LnAmt"]))),2,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["Tenure"]))),2,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["Repayment".$i]))),2,'.','').",0.00,".number_format(ECh(trim(str_replace(",","",$_REQUEST["OutSPay".$i]))),2,'.','').",".number_format(ECh(trim(str_replace(",","",$_REQUEST["IntRate"]))),2,'.','').",'28 ".ECh(trim($_REQUEST["ExpPayDate".$i]))."','N','".$_SESSION["StkTck"."HKey"]."',GetDate(),'".$SubHashKey."')";
	//			echo $Script."<br><br><br><br>";
				ScriptRunnerUD($Script,"Insert");
	
				$Script="Select (SName+' '+FName+' '+ONames+' ['+Convert(varchar(24),EmpID)+']') Nm from EmpTbl where HashKey='".ECh(trim($_REQUEST["ApprEmpID"]))."'";
				
				if (isset($_REQUEST["Actn"]) && $_REQUEST["Actn"]=="RescheduleLn" && isset($_REQUEST["OldLID"]) && strlen($_REQUEST["OldLID"])==32)
				{
					AuditLog("INSERT","Employee loan rescheduled for employee -".ScriptRunner($Script,"Nm")."");
					echo ("<script type='text/javascript'>{ parent.msgbox('Employee loan rescheduled successfully', 'green'); }</script>");
				}
				else
				{
					AuditLog("INSERT","New employee loan schedule created for employee -".ScriptRunner($Script,"Nm")."");
					echo ("<script type='text/javascript'>{ parent.msgbox('Employee loan successfully booked', 'green'); }</script>");
				}
				MailTrail("EMPLOYEE LOAN","T",'','','','');
	
				
				//header("Location:ApprStartEmp.php?AQuartID=".$KPIID."&pid=rffbf6545bghjj89jyl8r&amp;pg=KPI");
			}
		}
	}
	
	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Selected")
	{
		exit;
	}
	
	elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Modify Selected")
	{
		exit;
	}
}
?>

<script>
$(function() {$( "#tabs" ).tabs();});
</script>

<script>
  $(function() {
    $( document ).tooltip();
  });
</script>
<link href="../css/style_main.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function GetOutSTotalAmt()
{
	alert(document.getElementById("OutSTotalAmt").value);
	if (document.getElementById("OutSTotalAmt").value==0) 
	{
		YesNo('Accext','Accept');
		return true;
	}
	else 
	{
		parent.msgbox('Total repayment amount must add up to the amount being granted', 'red');
		return false;
	}
}

function GetTotal()
{
	//SORT OUT ALL VALUES
	//alert("1");
	var $TotalAmt=0;

	for (var i=1; i <= document.getElementById("DelMax").value; i++)
	{
		//alert("2");
		var $ElementNm = document.getElementById("Repayment"+i).value.replace(',', '');
		var $ElementNm = $ElementNm.replace(',', '');
		var $ElementNm = $ElementNm.replace(',', '');
		document.getElementById("Repayment"+i).value=$ElementNm; //Remove all commas
		
		//var $TotalAmt = document.getElementById("Repayment".i).value;
		//alert("3" + $ElementNm);
		$TotalAmt = (parseFloat($TotalAmt).toFixed(2) * 1) + (parseFloat($ElementNm).toFixed(2) * 1);
		
		//alert("4");
		document.getElementById("BlessingSleeping").innerHTML = thousand_separator(parseFloat($TotalAmt).toFixed(2));
		
		//alert("5");
	}

	var $TOutSAmt = parseFloat(document.getElementById("RePayAmt").value).toFixed(2);
	for (var i=1; i <= document.getElementById("DelMax").value; i++)
	{
		var $OutSAmt = document.getElementById("OutSPay"+i).value.replace(',', '');
		var $OutSAmt = $OutSAmt.replace(',', '');
		var $OutSAmt = $OutSAmt.replace(',', '');
		
		if (i==1)
		{
			document.getElementById("OutSPay"+i).value=thousand_separator(parseFloat($TOutSAmt).toFixed(2))
		}
		else
		{
			var $oo = i - 1;
			$TOutSAmt=parseFloat($TOutSAmt).toFixed(2)-parseFloat(document.getElementById("Repayment"+$oo).value).toFixed(2);
			document.getElementById("OutSPay"+i).value=thousand_separator(parseFloat($TOutSAmt).toFixed(2));
			
			//document.getElementById("Repayment"+$oo).value = document.getElementById("Repayment"+$oo).value.replace(',', '');
			//document.getElementById("Repayment"+$oo).value = document.getElementById("Repayment"+$oo).value.replace(',', '');
			//document.getElementById("Repayment"+$oo).value = document.getElementById("Repayment"+$oo).value.replace(',', '');

			document.getElementById("Repayment"+$oo).value=thousand_separator(parseFloat(document.getElementById("Repayment"+$oo).value).toFixed(2));
		}
	}
	$oo = $oo + 1;

	$TOutSAmt=$TOutSAmt - parseFloat(document.getElementById("Repayment"+$oo).value).toFixed(2);

	document.getElementById("OutSTotalAmt").value=$TOutSAmt.toFixed(2);
	document.getElementById("OutSTotalDisplay").innerHTML=thousand_separator($TOutSAmt.toFixed(2));	
	document.getElementById("Repayment"+$oo).value=thousand_separator(parseFloat(document.getElementById("Repayment"+$oo).value).toFixed(2));	

	function thousand_separator (v,s,d)
	{
		if (arguments.length == 2) d=".";
		if (arguments.length == 1) {s = ","; d = ".";}

		v = v.toString();
		// separate the whole number and the fraction if possible
		var a = v.split(d);
		var x = a[0]; // decimal
		var y = a[1]; // fraction
		var z = "";
		var l = x.length;
		while (l > 3){
			z = ","+x.substr(l-3,3)+z;
			l -=3;
		}
		z = v.substr(0,l)+z;

		if(a.length>1)
			z=z+d+y;
		return z;
	}
}
</script>
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
<!-- Theme style -->
<link rel="stylesheet" href="../assets/css/master_style.css">
<link rel="stylesheet" href="../assets/css/responsive.css">
<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
<script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>

<body onLoad="GetTotal();" oncontextmenu="return false;">
	<form action="#" method="get" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-6" >
					</div>
					<div class="col-md-6">
						<div class="row">
					        <?php
								if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open")
								{
									echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="'.ECh($_REQUEST["AcctNo"]).'" />';
									if (isset($_REQUEST["AcctNo"])) 
									{
										$EmpID = ECh($_REQUEST["AcctNo"]);
										$EditID = ECh($_REQUEST["AcctNo"]);
									}	
								}
								else
								{
									echo '<input name="ApprEmpID" id="ApprEmpID" type="hidden" value="'; if (isset($_REQUEST["ApprEmpID"])){echo $_REQUEST["ApprEmpID"];} echo '" />';
									if (isset($_REQUEST["ApprEmpID"])) 
									{
										$EmpID=$_REQUEST["ApprEmpID"];
										$EditID = $_REQUEST["ApprEmpID"];
									}
								}
							?>
							<div class=" col-md-4  col-12"  style="margin-top: 3%;font-weight: bolder; font-size: 14px;"> Select an Employee </div>
							<div class="input-group input-group-sm col-md-6 col-8 " style="margin-top: 2%">
								<select class="form-control" name="AcctNo" id="AcctNo">
									<?php
										echo '<option value="--" selected="selected">--</option>';
										$dbOpen2 = ("SELECT * from EmpTbl where (Status='A' or Status='U' or Status='N') and [EmpStatus]='Active' order by SName Asc");
										include '../login/dbOpen2.php';
										while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
											{
												if ($EditID==$row2['HashKey'])
												{
												$AllEmployee = $AllEmployee.'<option selected value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
												}
												else
												{
												$AllEmployee = $AllEmployee.'<option value="'.$row2['HashKey'].'">'.$row2['SName']." ".$row2['FName']." ".$row2['ONames']." [".$row2['EmpID']."]".'</option>';
												}
											}
											echo $AllEmployee;
										include '../login/dbClose2.php';

										$Script="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.HashKey='".$EditID."'";
										//echo ScriptRunner($Script,"Nm");
									?>
								</select>
								<span class="input-group-btn">
									<input name="SubmitTrans" type="submit" class="btn btn-danger" id="SubmitTrans" value="Open" />
								</span>
							</div>
							<div class=" col-md-1 col-2" style="margin-top:2%;" >
								<a href="javascript: void(0)"  alt="Find a Dashboard" name="FindanEmployee" id="FindanEmployee" onClick="parent.ShowDisp('Search&nbsp;Employee','hrm/srh_Emp.php?PgTy=&RetPg=loan/Nw_EmpLn.php&GroupName=Nationality&amp;LDb=Single&amp;Elmt=Nationality',320,600,'Yes')" class="btn btn-default"><i class="fa fa-search"></i></a>
							</div>
						</div>
					</div>
				</div>
        	</div>
			<?php
				if (strlen($EmpID)!=32)
				{exit;}
			?>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
						  	<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Employee:</label>
						  	<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
						    	<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
						      		<div class="box-body" style="padding: 0.6rem" >
										<p>
											<?php
												$Script="SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames) as Nm, Et.EmpMgr from EmpTbl Et where Et.HashKey='".$EditID."'";
												echo ScriptRunner($Script,"Nm");
											?>
						        		</p>
						      		</div>
						    	</div>
						  	</div>
						</div>
					</div>
					<div class="col-md-6"></div>
					<?php
						if (isset($_REQUEST["SName"]) && strlen($_REQUEST["SName"])==32 && isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"])==32 )
						{
							
							//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							$dbOpen7=("SELECT (select SUM(Paid) from LnIndvOff Where LID=Ld.HashKey and (convert(varchar(11),AddedDate)=convert(varchar(11),Ld.AddedDate)) and Status<>'D' Group By convert(varchar(11),AddedDate), EID, LID) Pd, Convert(Varchar(11),AddedDate,106) as Dt, Convert(Varchar(11),SRepayDate,106) as SD, Convert(Varchar(11),ERepayDate,106) as ED, Convert(Varchar(11),AuthDate,106) AuthDt, * from LnDetails Ld where EID='".ECh($_REQUEST["AcctNo"])."' and Status not in ('D') order by SRepayDate asc");
							include '../login/dbOpen7.php';
							//$LnAmt = 0;
							//$ExpPay =0;
							$OutSPay2= 0;
							$RePayAmt2 = 0;
							while($row7 = sqlsrv_fetch_array($result7,SQLSRV_FETCH_BOTH)){
								$OutSPay2 = $OutSPay2 + $row7['OutSPay'];
								$RePayAmt2 = $RePayAmt2 + $row7['RePayAmt'];
								//print $RePayAmt2."<br />";
								$LnAmt2=$row7['LnAmt'];
							}
								//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							}
						else
						{
					?>
					<div class="col-md-6">
						<div class="form-group row">
						  	<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Loan Scheme:</label>
						  	<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px" >
						    	<select class="form-control" name="SName" id="SName" onchange="getMaxtenure(this)">
			    		            <option value="--" selected="selected">--</option>
			    		            <?php
							    		if (isset($EditID) && ($EditID != "" && $EditID!="--"))
							    		{$SelID = ScriptRunner ($Script_Edit,"SName");}

							    		$dbOpen2 = ("select * from LnCore where Status='A'");
							    		include '../login/dbOpen2.php';
							    		while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
							    		{
							    			if ($SelID==$row2['HashKey'])
							    			{echo '<option selected value="'.$row2['HashKey'].'">'.$row2['SName'].'</option>';}
							    			else
							    			{echo '<option value="'.$row2['HashKey'].'">'.$row2['SName'].'</option>';}
							    		}
							    		include '../login/dbClose2.php';
						    		?>
						    	</select>
						    	<span class="input-group-btn">
						    		<?php 
				    					$imgID="MastersEmpStatus";
				    					$imgPath = "fa fa-info";
				    					$Titl="Select a loan scheme to apply for this loan";
				    					$OnClk ="";
				    					echo '<button type="button" class="btn btn-default" id="'.$imgID.'" title="'.$Titl.'" onClick="'.$OnClk.'"><i class="'.$imgPath.'"></i></button>';
						    		?>
						    	</span>
						  	</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Loan Tenure:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px">
								<select class="form-control"  name="Tenure" id="Tenure">
								</select>
								<span class="input-group-btn ">
									<button type="button" title="Tenure is calculated on a monthly basis. 12 months is the equivalent of 1 year" name="AuthInfo21" id="AuthInfo21" class="btn btn-default "><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row">
						  	<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 12px; " >Grace Period:</label>
						  	<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
						    	<div class="box bg-pale-secondary"  style="margin-bottom: 5px">
						      		<div class="box-body" style="padding: 0.6rem" >
										<input type="hidden" name="GPeriod" id="GPeriod" value="0">	
										<p id="GPeriodText">
											0 Month(s)
						        		</p>
						      		</div>
						    	</div>
						    	<small><strong style="font-weight: 600">Period after which repayment of this loan will start</strong></small>
						  	</div>
						</div>
						<div class="form-group row">
						  	<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 12px; " >Stay Period:</label>
						  	<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
						    	<div class="box bg-pale-secondary"  style="margin-bottom: 5px">
						      		<div class="box-body" style="padding: 0.6rem" >
										<p id="SPeriodText">
											0 Month(s)
						        		</p>
						      		</div>
						    	</div>
						    	<small><strong style="font-weight: 600">Period after employment before an employee can apply for this type of loan</strong></small>
						  	</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" > Repayment Type:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px" >
								<select  name="RepayTyp" class="form-control" id="RepayTyp">
					              	<option selected="selected" value="1">Equal Monthly Repayment</option>
			              	        <option value="2">Graduated Repayment</option>
		              	          	<?php
				              			$Script="Select SetValue from Settings where Setting='AuditTyp'";
				              			$kk = ScriptRunner($Script_Edit,"RepayType");
				              			if (trim($kk) == '' or $kk==1)
				              			{ echo '<option selected="selected" value="1">Equal Monthly Repayment</option>'; }
				              			elseif ($kk == 2)
				              			{ echo '<option selected="selected" value="2">Graduated Repayment</option>'; }
			              			?>
					            </select>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label" style="padding-right: 0px;" >Request Amount:</label>
							<div class="col-sm-8 input-group input-group-sm" style=" padding-right: 35px" >
								<?php
									if ($EditID != "" || $EditID > 0)
									{echo '<input type="text" name="RAmt" maxlength="12" class="form-control" id="RAmt" value="'.ScriptRunner($Script_Edit,"IntPCent").'" />';}
									elseif (isset($_REQUEST["RAmt"]) && (!isset($_REQUEST["SubmitTrans"]) || (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"]!="Clear")))
									{echo '<input type="text" name="RAmt" maxlength="12" class="form-control" id="RAmt" value="'.$_REQUEST["IntPCent"].'" />';}
									else
									{echo '<input type="text" name="RAmt" maxlength="12" class="form-control" id="RAmt" value="" />';}
								?>
								<span class="input-group-btn ">
									<button type="button" class="btn btn-default"  title="Requested loan amount." name="AuthInfo22" id="AuthInfo22"><i class="fa fa-info"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-4 col-form-label">Loan Reason:</label>
							<div class="col-sm-8" style=" padding-right: 35px" >
								<?php 
									if ($EditID != "" || $EditID > 0)
										{echo '<textarea name="LnDesc" rows="2" class="form-control" id="LnDesc">'.ScriptRunner($Script_Edit,"LnDesc").'</textarea>';}        			
									else
									{
										if (isset($_REQUEST["LnDesc"]))
										{echo '<textarea name="LnDesc" rows="2" class="form-control" id="LnDesc">'.$_REQUEST["LnDesc"].'</textarea>';}
										else
										{echo '<textarea name="LnDesc" rows="2" class="form-control" id="LnDesc"></textarea>';}
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">  </div>
							<div class="col-md-2" >
								<div class="demo-checkbox">
									<input type="checkbox" checked="checked" name="AuthNotifier" id="AuthNotifier" />
									<label for="AuthNotifier">
									<button type="button" class="btn btn-default btn-sm" title="Check if you want a request for authorization sent" name="AuthInfo" id="AuthInfo" style="line-height: 17px" ><i class="fa fa-info"></i></button></label>
								</div>
							</div>
							<div class="col-md-4 pull-right">
								<?php
									//if (ValidateURths("SET LEAVE POLICY"."A")==true)
									if (ValidateURths("EMPLOYEE LOAN"."A")==true)	
									{
										if ($EditID =="" || $EditID=='--')
										{?>
							              <input name="SubmitTrans" type="button" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Create Schedule" onClick="YesNo('Create', 'Create')"/>
							              <?php }
										elseif (strlen(trim($EditID))==32)
										{?>
							              <input name="SubmitTrans" type="submit" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Get Details" />
							              <?php }
									}
									//else
									//{
									//	echo '<input name="SubmitTrans" type="button" class="smallButton_disabled" id="SubmitTrans" value="Create Schedule" disabled="disabled" />';
									//}
								?>
							</div>
						</div>
					</div>
				</div>
			    <?php } ?>
				<div class="col-md-12"  style="padding-top: 30px;">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a class="active" href="#book" data-toggle="tab">Booking Schedule</a></li>
							<li><a href="#repayment" data-toggle="tab">Repayment Schedule</a></li>
							<li><a href="#terminate" data-toggle="tab">Terminate Loan</a></li>
							<li><a href="#history" data-toggle="tab"> Loan History</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="book">
								<?php
									//if (!isset($_REQUEST["RAmt"]) || (isset($_REQUEST["RAmt"]) && intval($_REQUEST["RAmt"])<=0))
									//{exit;}
									if (isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"])==32 && isset($_REQUEST["SName"]) && strlen($_REQUEST["SName"])==32 && (isset($_REQUEST["RAmt"]) && floatval($_REQUEST["RAmt"])>=0))
									{
										//+++++++++++++++++++++++++++++++ CHECK TO SEE IF LOAN IS PAID OFF, IF NOT STOP OPERATION ++++++++++++++++++++
										$dbOpen2 = ("SELECT 
											Convert(Varchar(11),Li.AddedDate,106) as Dt, 
											Convert(Varchar(11),Li.ExpPayDate,106) as Rd,
											(select top 1 RePayAmt from LnDetails where HashKey=Li.LID) RePayAmt,
											Li.LID as HKey,
											Li.*
											from LnIndvOff Li where Li.EID='".ECh($_REQUEST["AcctNo"])."' and Li.Status in ('A','B') order by Li.ExpPayDate asc");
											
											include '../login/dbOpen2.php';
											
											$LnAmt = 0;
											$ExpPay =0;
											
											while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)){
												$LnAmt = $row2['RePayAmt'];
												$ExpPay = $ExpPay + $row2['ExpPay'];
											
											}
											if(($OutSPay2 < $RePayAmt2) && (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Reschedule Loan")){
												
												echo ("<script type='text/javascript'>{
														parent.msgbox('Sorry you cannot apply for another Load until already applied Load repayment has been completed. ', 'red');
														}</script>");
													
															$GoValidate = false;
														
												}else{
												//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
												$Script ="Select * from LnCore where Status='A' and HashKey='".ECh($_REQUEST["SName"])."'";
												ScriptRunner($Script,"MaxSalPCent");

												$LoanNm=ScriptRunner($Script,"SName");
												
											  ?>
								<!-- </div> -->
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group row">
													<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Loan Name:</label>
													<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
														<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
															<div class="box-body" style="padding: 0.6rem" >
																<p>
																	<?php echo $LoanNm;
																		echo '<input type="hidden" name="LoanScheme" id="LoanScheme" value="'.ECh($_REQUEST["SName"]).'" />
																		<input type="hidden" name="LnDesc" id="LnDesc" value="'.ECh($_REQUEST["LnDesc"]).'" />
																		';
																	?>
																</p>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Tenure:</label>
													<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
														<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
															<div class="box-body" style="padding: 0.6rem" >
																<p>
																	<?php
																		echo '<input type="hidden" name="Tenure" id="Tenure" value="'.ECh($_REQUEST["Tenure"]).'" />';
																		echo $_REQUEST["Tenure"];
																	?> months
																</p>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Request Amount:</label>
													<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
														<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
															<div class="box-body" style="padding: 0.6rem" >
																<p>
																	<?php 
																		if(isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Reschedule Loan"){
																			$RAmt_ = $LnAmt2;
																			echo 'N'.number_format($RAmt_, 2, '.', ',');
																			echo '<input type="hidden" name="LnAmt" id="LnAmt" value="'.$RAmt_.'" />';
																			//print "<br />$_REQUEST[ReschLn]";
																		}else{
																			if (isset($_REQUEST["RAmt"]))
																			{
																				if (intval($_REQUEST["RAmt"])>0)
																				{
																					$RAmt_ = floatval(number_format(ECh(trim(str_replace(",","",$_REQUEST["RAmt"]))),2,'.',''));
																					//echo 'N'.number_format($RAmt_, 2, '.', ',');
																					echo 'N'.number_format($RAmt_, 2, '.', ',');
																					
																				}
																				else
																				{
																					$RAmt_=0;
																					echo 'N0.00';
																				}
																			}
																			echo '<input type="hidden" name="LnAmt" id="LnAmt" value="'.$RAmt_.'" />';
																		}
																	?>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group row">
													<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Interest Rate:</label>
													<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
														<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
															<div class="box-body" style="padding: 0.6rem" >
																<p>
																	<?php $InPCent= ScriptRunner($Script,"IntPCent"); if ($InPCent==0){echo "0.00";} else {echo $InPCent;}
																		
																		echo '<input type="hidden" name="IntRate" id="IntRate" value="'.$InPCent.'" />';
																	?>
																</p>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label  class="col-sm-4 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Repayment Amount:</label>
													<div class="col-sm-8  input-group-sm" style=" padding-right: 35px" >
														<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
															<div class="box-body" style="padding: 0.6rem" >
																<p>
																	<?php
																		//$RePayAmt=$RePayAmt2;
																		//echo 'N'.number_format($RAmt_, 2, '.', ',');
																		if(isset($_REQUEST["SubmitTrans2"]) && $_REQUEST["SubmitTrans2"] == "Reschedule Loan"){
																			$RePayAmt=$RePayAmt2;//str_replace(",","",$RAmt_);
																			echo 'N'.number_format($RePayAmt, 2, '.', ',');
																			}else{
																			if ($InPCent==0) {
																				$RePayAmt=str_replace(",","",$RAmt_);
																				echo 'N'.number_format($RAmt_, 2, '.', ',');
																			}else{
																					$LnInt=(($InPCent*str_replace(",","",$RAmt_))/100);
																					$RePayAmt=$LnInt + str_replace(",","",$RAmt_);
																			
																					echo 'N'.number_format($RePayAmt, 2, '.', ',');
																				}
																			}
																		echo '<input type="hidden" name="RePayAmt" id="RePayAmt" value="'.$RePayAmt.'" />';
																		if(intval( $_REQUEST["Tenure"]) === 1){
																				echo '<input type="hidden" name="OutSTotalAmt" id="OutSTotalAmt" value="0" />';
																		}else{

																			echo '<input type="hidden" name="OutSTotalAmt" id="OutSTotalAmt" value="'.$RePayAmt.'" />';
																		}
																	?>
																</p>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7">
														<div class="form-group row">
															<label  class="col-sm-5 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >Repayment Start:</label>
															<div class="col-sm-7  input-group-sm" style=" padding-right: 35px" >
																<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
																	<div class="box-body" style="padding: 0.6rem" >
																		<p>
																			<?php
																				if (intval($_REQUEST["GPeriod"])==0) 
																				{$GPeriod_ = 0;}
																				else
																				{$GPeriod_ = intval($_REQUEST["GPeriod"]);}

																				$Script="Select (DateName(M,DateAdd(M,".$GPeriod_.",GETDATE()))+' '+DateName(YYYY,DateAdd(M,".$GPeriod_.",GETDATE()))) Dt";
																				$kk=ScriptRunner($Script,"Dt");
																				echo $kk;
																				
																				echo '<input type="hidden" name="SRepayDate" id="SRepayDate" value="'.$kk.'" />';
																			?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-5">
														<div class="form-group row">
															<label  class="col-sm-3 col-form-label" style="padding-right: 0px; font-weight: bolder; font-size: 14px; " >End:</label>
															<div class="col-sm-9  input-group-sm" style=" padding-right: 35px" >
																<div class="box bg-pale-secondary"  style="margin-bottom: 0px">
																	<div class="box-body" style="padding: 0.6rem" >
																		<p>
																			<?php 
																				//		echo $_REQUEST["GPeriod"];
																				//	echo "-----".$GPeriod_."------".$_REQUEST["Tenure"]."---------";

																					$Script="Select (DateName(M,DateAdd(M,".intval($GPeriod_ - 1 + ($_REQUEST["Tenure"])).",GETDATE()))+' '+DateName(YYYY,DateAdd(M,".intval($GPeriod_ - 1 + ($_REQUEST["Tenure"])).",GETDATE()))) Dt";
																				//	echo $Script;
																				//	exit;
																				$kk=ScriptRunner($Script,"Dt");
																				echo $kk;

																				echo '<input type="hidden" name="ERepayDate" id="ERepayDate" value="'.$kk.'" />';
																			?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<table class="table-bordered table-responsive table" id="">
											<!--<thead> -->
											<tr>
												<th><input type="checkbox" id="DelChk_All" onClick="ChkDel();"/><label for="DelChk_All"></label></th>
												<th>Repayments</th>
												<th>Repayment Month</th>
												<th>Repayment Amount</th>
												<th>Amount Pending</th>
												<th>Status</th>
											</tr>
												<!-- </thead> -->
											<?php
												$Del = 0;
												$RePaid=0;
												//$RePayAmt =0;
												$IDCnt="";
												//Populate SORT ID's 1 - 20
												// var_dump($_REQUEST["Tenure"]);
												for($x=1;$x<=$_REQUEST["Tenure"];$x++)
												{
												$Del = $Del + 1;
												//Disable the menu if the link is authorized
												if ($row2['Status'] == "A") {$MenuStatus="disabled";} else {$MenuStatus="";}
											?>
											<tr>
												<td><input name="<?php echo ("DelBox".$Del); ?>" type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['HashKey']); ?>" /><label for="<?php echo ("DelBox" . $Del); ?>"></label></td>
												<td><?php echo $Del;?></td>
												<td><?php
														$Script="Select (DateName(M,DateAdd(M,".intval($GPeriod_ + ($Del - 1)).",GETDATE()))+' '+DateName(YYYY,DateAdd(M,".intval($GPeriod_ + ($Del-1)).",GETDATE()))) Dt";
														$kk = ScriptRunner($Script,"Dt");
														echo '<input type="hidden" name="ExpPayDate'.$Del.'" id="ExpPayDate'.$Del.'" value="'.$kk.'" />';
												echo $kk;?></td>


												<td align="right" valign="middle"><?php
														echo '<input class="TextBoxText_Medium_Comp_Right" onChange="GetTotal();" type="text" name="Repayment'.$Del.'" id="Repayment'.$Del.'" value="'.number_format($RePayAmt/intval($_REQUEST["Tenure"]), 2, '.', ',').'" />';
												?>&nbsp;</td>


												<td width="343" align="right" valign="middle"><?php
														if ($Del==1)
														{
																$RePaid = $RePayAmt;
																$kk = number_format((intval($RePaid)), 2, '.', ',');
														}
														elseif ($Del==2)
														{
															$RePaid=($RePayAmt/$_REQUEST["Tenure"]);
															$kk = number_format((intval($RePayAmt) - intval($RePaid)), 2, '.', ',');
														}
														else
														{
															$RePaid=$RePaid + ($RePayAmt/$_REQUEST["Tenure"]);
															$kk = number_format((intval($RePayAmt) - intval($RePaid)), 2, '.', ',');
														}
														
														echo '<input type="text" class="TextBoxText_Medium_Comp_Right" readonly name="OutSPay'.$Del.'" id="OutSPay'.$Del.'" value="'.$kk.'" />';
														
												?></td>
												<td align="center" valign="middle"><?php
												echo '<img src="../images/amber.png" height="18" title="Pending acceptance">';?></td>
											</tr>
											<?php
											}?>
											<tr>
												<td height="25" valign="middle" class="TinyTextTight">&nbsp;</td>
												<td align="center" valign="middle">&nbsp;</td>
												<td align="center" valign="middle">&nbsp;</td>
												<td align="right" valign="middle" class="tdMenu_HeadBlock_Light pb-0" id="BlessingSleeping"><?php echo  number_format((intval($RePayAmt)), 2, '.', ',');?>&nbsp;</td>
												<?php 



												// echo '<input type="hidden" name="OutSTotalAmt" id="OutSTotalAmt" value="'.$RePayAmt.'" />';



												 ?>
												<td align="right" valign="middle" id="OutSTotalDisplay" class="tdMenu_HeadBlock_Light pb-0">
													<?php
												
													if (isset($_REQUEST["Tenure"]) && $_REQUEST["Tenure"]>0)
													{
														$RePaid=$RePaid + ($RePayAmt/$_REQUEST["Tenure"]);
													}
													// added by Seun
													if ( intval( $_REQUEST["Tenure"]) === 1) {
														$kk = number_format(0,2);
													
													} else {
														$kk = number_format((intval($RePayAmt) - intval($RePaid)), 2, '.', ',');
													}
													
													echo $kk;
												?></td>
												<td align="center" valign="middle">&nbsp;</td>
											</tr>
	  									</table>
	  								</div>
	  							</div>
								<?php
									}
								?>
								<div class="row">
									<div class="col-md-12">
									    <?php
											if (isset($_REQUEST["Actn"]) && $_REQUEST["Actn"]=="RescheduleLn" && isset($_REQUEST["OldLID"]) && strlen($_REQUEST["OldLID"])==32)
											{
												echo ' <input name="Actn" id="Actn" type="hidden" value="RescheduleLn" />
												<input name="OldLID" id="OldLID" type="hidden" value="'.$_REQUEST["OldLID"].'" />';
											}
										    echo '<input name="DelMax" id="DelMax" type="hidden" value="'.$Del.'" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
												<input name="FAction" id="FAction" type="hidden"/>
											  	<input name="PgDoS" id="PgDoS" type="hidden" value="'.DoSFormToken().'">';
											echo '<input name="UpdID" type="hidden"  id="UpdID" value="'.$EditID.'" />';
											 
										     if (isset($_REQUEST["Tenure"]) && $_REQUEST["Tenure"]>0)
											 {?>
										     <!--<input name="SubmitTrans" type="submit" class="smallButton" id="SubmitTrans" value="Accept Selected" /> -->
										     <input name="SubmitTrans" type="button" class="btn btn-danger btn-sm pull-right" id="SubmitTrans" value="Accept Selected" onClick="GetOutSTotalAmt();" />
									    <?php } ?>
									</div>
								</div>
								<?php 
								}
								else
								{
									echo '<input type="hidden" class="TextBoxText_Medium_Comp_Right" readonly name="OutSTotalAmt" id="OutSTotalAmt" value="0" />';
									echo '<div class="box bg-lightest"  style="margin-top: 20px;">
									  		<div class="box-body  text-center" style="padding: 0.9rem; " >
										';
									    $Script="Select Count(*) Ct from [LnDetails] where Status not in ('D','P','T') and EID='".$EditID."'";
										if (ScriptRunner($Script,"Ct") > 0)
										{
											echo 	"<p>Click on the Repayment Schedule tab to find a detailed list of repayments for this employee.</p>";
										}
										else
										{
											echo 	"<p>There is currently no pending repayment for this employee
													<br />You can create a new loan application.</p>";
										}

										echo '</div>
									  </div>';	
								}
								?>
							</div>
							<div class="tab-pane" id="repayment">
								<?php
									//Resc_Ln.php
									echo '<iframe id="ActiveLoanSchedule" name="ActiveLoanSchedule" src="Resc_Ln.php?PgTy=Create&EID='.ECh($_REQUEST["AcctNo"]).'" marginheight="0" marginwidth="0" width="100%" height="400" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>';
								?>
							</div>
							<div class="tab-pane" id="terminate">
								<?php
									//Term_Ln.php
									echo '<iframe id="3ActiveLoanSchedule" name="3ActiveLoanSchedule" src="Term_Ln.php?PgTy=Create&EID='.ECh($_REQUEST["AcctNo"]).'" marginheight="0" marginwidth="0" width="100%" height="400" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>';
								?>
							</div>
							<div class="tab-pane" id="history">
								<?php
									//Nw_LnHst.php
									echo '<iframe id="LoanHistory" name="LoanHistory" src="Nw_LnHst.php?PgTy=Create&EID='.ECh($_REQUEST["AcctNo"]).'" marginheight="0" marginwidth="0" width="100%" height="400" frameborder="0" scrolling="Yes" allowtransparency="100"><p>Your browser does not support iframes.</p></iframe>';
								?>
							</div>
						</div>
					</div>
				</div>
  			</div>
  		</div>
  	</form>
  	<!-- Bootstrap 4.0-->
  	<script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  																			
  	<!-- SlimScroll -->
  	<script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
  			
  																			
  	<!-- FastClick -->
  	<script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>
  																			
  	<!-- MinimalLite Admin App -->
  	<script src="../assets/js/template.js"></script>
  																			
  	<!-- MinimalLite Admin for demo purposes -->
  	<script src="../assets/js/demo.js"></script>
  	<script>	
		function getMaxtenure(e) {
			$('#Tenure option').remove();
			if (e.value == '--') { 
				$('#GPeriod').val(0);
				$('#GPeriodText').text(`0 Month(s)`);
				$('#SPeriodText').text(`0 Month(s)`);
				return; 
			}
			const hashkey = e.value;
			$.post({
				url: 'getMaxTenureAjax.php',
				data: {hashkey, PgDoS: '<?php echo DoSFormToken() ?>'},
				cache: false,
				success(res) {
					appendToTenure(res.tenure);
					$('#GPeriod').val(res.grace);
					$('#GPeriodText').text(`${res.grace} Month(s)`);
					$('#SPeriodText').text(`${res.stay} Month(s)`);
				},
				error(e) {
					console.log(e);
				}
			})
		}
		function appendToTenure(val) {
			for(let index = 1; index <= val; index++) {
				$('#Tenure').append($("<option></option>").attr("value",index).text(index)); 
			}
		}
  	</script>
</body>

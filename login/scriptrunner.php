<?php
// session_cache_expire(60);
// session_start();

// if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on') {
//     if(!headers_sent()) {
//         header("Status: 301 Moved Permanently");
//         header(sprintf(
//             'Location: https://%s%s',
//             $_SERVER['HTTP_HOST'],
//             $_SERVER['REQUEST_URI']
//         ));
//         exit();
//     }
// }

//If user is not logged in, Session TimeOut would keep resetting to a default 3 minutes
if (!isset($_SESSION["StkTck" . "GpTOut"])) {
    $_SESSION["StkTck" . "GpTOut"] = 5;
}
if (!isset($PgLd)) {
    $PgLd = "";
}
if (!isset($_SESSION["StkTck" . "Login_Status"])) {
    $_SESSION["StkTck" . "Login_Status"] = "";
}
if (!isset($_SESSION["Rst"])) {
    $_SESSION["Rst"] = "";
}
if (!isset($_SESSION["StkTck" . "Session" . "Warning"])) {
    $_SESSION["StkTck" . "Session" . "Warning"] = "";
}
if (!isset($pg)) {
    $pg = "";
}
if (!isset($_REQUEST["PgName"])) {
    $_REQUEST["PgName"] = "";
}

if (!isset($_SESSION["StkTck" . "GpTOut"]) || $_SESSION["StkTck" . "GpTOut"] == "") {
    $_SESSION["StkTck" . "GpTOut"] = 5;
}

if (!isset($_SESSION["StkTck" . "GpTOutWr"]) || $_SESSION["StkTck" . "GpTOutWr"] == "") {
    $_SESSION["StkTck" . "GpTOutWr"] = 1;
}

$inactive = $_SESSION["StkTck" . "GpTOut"] * 60; //300 seconds - 5 minutes
$inactive_wrn = (($_SESSION["StkTck" . "GpTOut"] * 60) - ($_SESSION["StkTck" . "GpTOutWr"] * 60));

if ($PgLd != "Exclude") {
    if ($_SESSION["StkTck" . "Login_Status"] == "True") {
        $session_life = time() - $_SESSION["StkTck" . "StartTime"];
        if ($session_life > $inactive) {
            if ($pg == "indexpg") {
                $_SESSION["StkTck" . "LoginMsg"] = "Your user session has expired. Re-login";
                AuditLog("A-LOGOUT", "User Automatically Logged Out. Session expired for account " . $_SESSION["StkTck" . "UName"]);
                $_SESSION["StkTck" . "Login_Status"] = "false";
                $_SESSION["StkTck" . "GpTOut"] = '';
                header("Location:logout.php");
                exit;
            } else {
                $_SESSION["StkTck" . "LoginMsg"] = "Your user session has expired. Re-login";
                AuditLog("A-LOGOUT", "User Automatically Logged Out. Session expired for account " . $_SESSION["StkTck" . "UName"]);
                $_SESSION["StkTck" . "Login_Status"] = "false";
                $_SESSION["StkTck" . "GpTOut"] = '';
                echo ("<script type='text/javascript'>{ parent.document.location.href = 'http://" . $_SERVER['HTTP_HOST'] . "/login/logout.php'; }</script>");
                // echo ("<script type='text/javascript'>{ parent.document.location.href = 'http://".$_SERVER['HTTP_HOST']."/SSLCloud/login/logout.php'; }</script>");
                // header("Location:logout.php");
                exit;
            }
        }

        if ($_SESSION["Rst"] != "Rem") {
            $_SESSION["StkTck" . "StartTime"] = time();
        }
    } else {
        if ($_SESSION["Rst"] != "Rem") {
            $_SESSION["StkTck" . "StartTime"] = time();
        }
    }
}

//================================================================================================
//==================================== End Session Check =========================================
//================================================================================================

//================================ Inactive session Warning Starts Here ===================================
//================================ Inactive session Warning Starts Here ===================================
if ($_SESSION["StkTck" . "Login_Status"] == "True") {
    //    $session_life = time() - $_SESSION["StkTck"."StartTime"];
    if (!isset($session_life)) {
        $session_life = time() - $_SESSION["StkTck" . "StartTime"];
    }

    if ($session_life >= $inactive_wrn) {
        if ($_SESSION["StkTck" . "Session" . "Warning"] != "Showing") {
            $_SESSION["StkTck" . "Session" . "Warning"] = "Showing";
            echo ("<script type='text/javascript'>{ parent.ShowDisp('Notification','login/ind_logout_note.php',200,405,'center'); }</script>");
            //            exit;
        }
    }
}
//================================ Inactive session Warning Ends Here ===================================
//================================ Inactive session Warning Ends Here ===================================

if ($pg == "indexpg") {
    //    header("Location:../login/signin.php");
}
include "template.php";
function ECh($OldStrVal)
{
    $NewStrVal = str_replace("'", "`", $OldStrVal);
    return $NewStrVal;
}

function ErCh($OldStrVal)
{
    $NewStrVal = str_replace("`", "'", $OldStrVal);
    return $NewStrVal;
}

function RmComma($OldStrVal)
{
    $NewStrVal = str_replace(",", "", $OldStrVal);
    return $NewStrVal;
}

function get_client_ip()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } else if (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } else if (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } else if (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
    } else if (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

function AuditLog($Qry_Ty, $Narr)
{
    if (!isset($_SESSION["StkTck" . "Audit_Type"])) {
        $_SESSION["StkTck" . "Audit_Type"] = "2";
    }

    //Get Audit Type for the action. Set default to SIMPLE if audit is blank
    if ((!isset($Qry_Ty) || !isset($Aud_Lv)) || ((isset($Qry_Ty) && $Qry_Ty == "") || (isset($Aud_Lv) && $Aud_Lv == ""))) {
        $Aud_Lv = 1;
    } elseif (strtoupper($Qry_Ty) == 'LOGIN' || strtoupper($Qry_Ty) == 'LOGOUT' || strtoupper($Qry_Ty) == 'HACK_ATTEMPT') {
        $Aud_Lv = 1;
    } elseif (strtoupper($Qry_Ty) == 'INSERT' || strtoupper($Qry_Ty) == 'UPDATE' || strtoupper($Qry_Ty) == 'DELETE' || strtoupper($Qry_Ty) == 'T.OPENED' || strtoupper($Qry_Ty) == 'T.CLOSED' || strtoupper($Qry_Ty) == 'T.RESPONSE') {
        $Aud_Lv = 2;
    } elseif (strtoupper($Qry_Ty) == 'AUTHORIZE' || strtoupper($Qry_Ty) == 'UNAUTHORIZE' || strtoupper($Qry_Ty) == 'A-LOCK' || strtoupper($Qry_Ty) == 'F-LOGIN' || strtoupper($Qry_Ty) == 'LOGIN-N' || strtoupper($Qry_Ty) == 'A-LOGOUT' || strtoupper($Qry_Ty) == 'T.ESCALATED') {
        $Aud_Lv = 3;
    }

    //echo $_SESSION["StkTck"."Audit_Type"]."<br>".$Qry_Ty."<br>".$Narr."<br>".$Aud_Lv;

    //Create trail if Audit query level **$Aud_Lv** is less than the defined GLOBAL Parameter @ User Login time
    if ($Aud_Lv <= $_SESSION["StkTck" . "Audit_Type"] && $Aud_Lv != 0) {
        if (isset($_SESSION["StkTck" . "HKey"]) && isset($_SESSION["StkTck" . "BranchID"])) // Avoid entering Blank Logs
        {
            //$Script_Audit="insert into AuditTrail ([AID], [AType], [AData], [AIP], [ADate], [BranchID]) values/
            //('".$_SESSION["StkTck"."HKey"]."','".$Qry_Ty."','".$Narr."','".$_SERVER['REMOTE_ADDR']."',GETDATE(),'".$_SESSION["StkTck"."BranchID"]."')";

            $Script_Audit = "insert into AuditTrail ([AID], [AType], [AData], [AIP], [ADate]) values

			('" . $_SESSION["StkTck" . "HKey"] . "','" . $Qry_Ty . "','" . $Narr . "','" . $_SERVER['REMOTE_ADDR'] . "',getdate())";

            ScriptRunnerUD($Script_Audit, "Audit");
        }
    }
}

function QueueMail($To, $Subj, $Bdy, $Atth, $Tm)
{
    if (trim($Tm) == "") {
        $Tm = "GetDate()";
    } else {
        $Tm = "'" . $Tm . "'";
    }

    $Recipients = explode(",", $To);

    $ArrayCount = count($Recipients);
    if ($ArrayCount > 0) {
        for ($i = 0; $i <= ($ArrayCount - 1); $i++) {
            //if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",trim($Recipients[$i])))
            if (filter_var(trim($Recipients[$i]), FILTER_VALIDATE_EMAIL)) {
                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"] . $i);

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . ECh($Subj) . "','" . trim($Recipients[$i]) . "','" . ECh($Bdy) . "','" . ECh($Atth) . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";

                //    var_dump($Script_Mail);
                //    die();
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }
        }
    }
}

function QueueMailRec($To, $Subj, $Bdy, $Atth, $Tm)
{
    if (trim($Tm) == "") {
        $Tm = "GetDate()";
    } else {
        $Tm = "'" . $Tm . "'";
    }

    $Recipients = explode(",", $To);

    $ArrayCount = count($Recipients);
    if ($ArrayCount > 0) {
        for ($i = 0; $i <= ($ArrayCount - 1); $i++) {
            //if (eregi("^[a-zA-Z0-9_.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",trim($Recipients[$i])))
            if (filter_var(trim($Recipients[$i]), FILTER_VALIDATE_EMAIL)) {
                /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
                $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
                // $HashKey = md5($UniqueKey.$_SESSION["StkTck"."UName"].$i);

                $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . ECh($Subj) . "','" . trim($Recipients[$i]) . "','" . ECh($Bdy) . "','" . ECh($Atth) . "','N','PublicRecruitment'," . $Tm . ",0,'PublicRecruitment')";
                ScriptRunnerUD($Script_Mail, "QueueMail");
            }
        }
    }
}

// Look for Lawrence
function MyQueueMail($To, $Subj, $Bdy, $Atth, $Tm)
{
    if (trim($Tm) == "") {
        $Tm = "GetDate()";
    } else {
        $Tm = "'" . $Tm . "'";
    }

    $Recipients = json_decode($To);

    foreach ($Recipients as $Recipient) {

        if (filter_var(trim($Recipient, FILTER_VALIDATE_EMAIL))) {
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . ECh($Subj) . "','" . trim($Recipient) . "','" . ECh($Bdy) . "','" . ECh($Atth) . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($Script_Mail, "QueueMail");
        }
    }
}

function MailTrail($URt1, $URt2, $Atth, $Tm, $Opt_To, $ChgVal) //Pass any value you wish to change as $ChgVal - eg. Emp Name

{
    if (!isset($ChgVal)) {
        $ChgVal = "";
    }

    $To = "";

    $kk = md5($URt1 . $URt2);
    if ($Opt_To == "") {
        /*  // Dont send blank MailTrail, where email is not specify

    $To = ScriptRunner("select top 1 Em.email from EmpTbl Em, Users Ur where Em.EmpID<>'000000' and Em.HashKey<>'f146e0c756af0217c9bf6e4f0f2ac9ee' and Ur.Status='A' and Em.Status='A' and Ur.ULock = 0 and Ur.LStatus='A' and Ur.AcctState='True' and Em.EmpID=Ur.EmpID and Ur.GpName in (Select GpName from [UGpRights] where GpVal=1 and GHashRt='".$kk."' and GPType='C' and [Status]<>'D')","email");

    $Subj = ScriptRunner("select MSub from MailTemp where HashKey='".$kk."'","MSub");
    $Bdy = ScriptRunner("select MMsg from MailTemp where HashKey='".$kk."'","MMsg");
    $Atth = ScriptRunner("select MAttch from MailTemp where HashKey='".$kk."'","MAttch");

    $Subj=str_replace('#ChangeValue#',$ChgVal,$Subj);
    $Bdy=str_replace('#ChangeValue#',$ChgVal,$Bdy);

    QueueMail($To,$Subj,$Bdy,$Atth,$Tm);
     */
    } else {
        $To = $Opt_To;

        $Subj = ScriptRunner("select MSub from MailTemp where HashKey='" . $kk . "'", "MSub");
        $Bdy = ScriptRunner("select MMsg from MailTemp where HashKey='" . $kk . "'", "MMsg");
        $Atth = ScriptRunner("select MAttch from MailTemp where HashKey='" . $kk . "'", "MAttch");

        $Subj = str_replace('#ChangeValue#', $ChgVal, $Subj);
        $Bdy = str_replace('#ChangeValue#', $ChgVal, $Bdy);

        //$Bdy=str_replace('#ChangeValue#',$ChgVal,$Bdy);
        // var_dump($Bdy);var_dump($Subj);
        // die();
        QueueMail($To, $Subj, $Bdy, $Atth, $Tm);
    }
}

//*******************************************************************************************************************
//Checks if the page is just being called from a link and then increaments the view count
//*******************************************************************************************************************
if (isset($_REQUEST["PgName"]) && $_REQUEST["PgName"] != '' && isset($_REQUEST["PgType"]) && $_REQUEST["PgType"] != '') {
    FreqOpened(urldecode($_REQUEST["PgName"]), $_REQUEST["PgType"]);
}
//*******************************************************************************************************************
//*******************************************************************************************************************

function FreqOpened($PgName, $PgType)
{
    //*******************************************************************************************************************
    //Insert or Update details of most recent dhasboard view in DbVHst Dashboard View History
    //*******************************************************************************************************************
    $Script = "Select HID from DbVHst where HID='" . md5($_SESSION["StkTck" . "UName"]) . "' and DbN='" . $PgName . "' and Typ='V' and PgType='" . $PgType . "' and Status<>'D'";
    if (strlen(ScriptRunner($Script, "HID")) > 1) {
        $Script = "Update DbVHst set RTN=(RTN + 1), VDt=GetDate() where HID='" . md5($_SESSION["StkTck" . "UName"]) . "' and DbN='" . $PgName . "' and Typ='V' and PgType='" . $PgType . "' and Status<>'D'";
        ScriptRunnerUD($Script, "HID");
    } else {
        $Script = "Insert into DbVHst (HID, DbN, RTN, VDt, Typ, PgType) values ('" . md5($_SESSION["StkTck" . "UName"]) . "','" . $PgName . "',1,GetDate(),'V','" . $PgType . "')";
        ScriptRunnerUD($Script, "HID");
    }
    //*******************************************************************************************************************
}

function ScriptRunner($query, $rwfield)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open db");
        if (!$selected) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($conn === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }*/ //for testing
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($result === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }*/ //for testing

        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
            //echo $query;
            return ("{$row[$rwfield]}");
        }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunnercous($query)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open db");
        if (!$selected) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($conn === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }*/ //for testing
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($result === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }*/ //for testing

        // while( $row = sqlsrv_fetch_array($result,SQLSRV_FETCH_BOTH))
        // {
        //     //echo $query;
        //     return ("{$row}");
        return $result = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH);
        // }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunnerParent($query)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "dflt_myServer"], $_SESSION["StkTck" . "dflt_myUser"], $_SESSION["StkTck" . "dflt_myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "dflt_myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "dflt_myServer"], $_SESSION["StkTck" . "dflt_myUser"], $_SESSION["StkTck" . "dflt_myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "dflt_myDB"], $dbhandle); // or die("Couldn't open db");
        if (!$selected) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "dflt_myDB"], "UID" => $_SESSION["StkTck" . "dflt_myUser"], "PWD" => $_SESSION["StkTck" . "dflt_myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "dflt_myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($conn === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }*/ //for testing
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($result === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }*/ //for testing

        // while( $row = sqlsrv_fetch_array($result,SQLSRV_FETCH_BOTH))
        // {
        //     //echo $query;
        //     return ("{$row}");
        return $result = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH);
        // }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunnercous1($query)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open db");
        if (!$selected) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($conn === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }*/ //for testing
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($result === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }*/ //for testing

        // while( $row = sqlsrv_fetch_array($result,SQLSRV_FETCH_BOTH))
        // {
        //     //echo $query;
        //     return ("{$row}");
        return true;
        // }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function sanitize($data)
{
    $data = trim($data);
    return htmlentities($data, ENT_QUOTES, "UTF-8");
}

function ScriptRunnercous2($query)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open db");
        if (!$selected) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($conn === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }*/ //for testing
        $result = sqlsrv_query($conn, $query);

        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        /*if ($result === false) //Error Handling if Database Server is down
        {
        echo ("<script type='text/javascript'>{parent.document.location.href='http://".$_SERVER['HTTP_HOST']."/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }*/ //for testing

        // while( $row = sqlsrv_fetch_array($result,SQLSRV_FETCH_BOTH))
        // {
        //     //echo $query;
        //     return ("{$row}");

        $count = sqlsrv_num_rows($result);
        var_dump($count);
        die();
        // }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function IntoMailTrail($URt1, $URt2, $To = '', $subject = [], $data = [])
{
    if (!$To) {
        return;
    }

    $kk = md5($URt1 . $URt2);
    $Script_Mail = "SELECT * FROM MailTemp WHERE HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    foreach ($data as $key => $value) {
        $Subj = str_replace("#$key#", $value, $Subj);
    }
    foreach ($data as $key => $value) {
        $msg = str_replace("#$key#", $value, $msg);
    }
    QueueMail($To, $Subj, $msg, '', '');
}

function CustomizedMailTrail($URt1, $URt2, $Opt_To, $employee, $recepient_name)
{
    if (!isset($ChgVal)) {
        $ChgVal = "";
    }
    $To = "";
    $kk = md5($URt1 . $URt2);
    if ($Opt_To == "") {
        /*  // Dont send blank MailTrail, where email is not specify

    $To = ScriptRunner("select top 1 Em.email from EmpTbl Em, Users Ur where Em.EmpID<>'000000' and Em.HashKey<>'f146e0c756af0217c9bf6e4f0f2ac9ee' and Ur.Status='A' and Em.Status='A' and Ur.ULock = 0 and Ur.LStatus='A' and Ur.AcctState='True' and Em.EmpID=Ur.EmpID and Ur.GpName in (Select GpName from [UGpRights] where GpVal=1 and GHashRt='".$kk."' and GPType='C' and [Status]<>'D')","email");

    $Subj = ScriptRunner("select MSub from MailTemp where HashKey='".$kk."'","MSub");
    $Bdy = ScriptRunner("select MMsg from MailTemp where HashKey='".$kk."'","MMsg");
    $Atth = ScriptRunner("select MAttch from MailTemp where HashKey='".$kk."'","MAttch");

    $Subj=str_replace('#ChangeValue#',$ChgVal,$Subj);
    $Bdy=str_replace('#ChangeValue#',$ChgVal,$Bdy);

    QueueMail($To,$Subj,$Bdy,$Atth,$Tm);
     */
    } else {
        $To = $Opt_To;
        $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
        $msg = ScriptRunner($Script_Mail, "MMsg");
        $Subj = ScriptRunner($Script_Mail, "MSub");
        $msg = str_replace('#recepient#', $recepient_name, $msg);
        $msg = str_replace('#employee#', $employee, $msg);
        QueueMail($To, $Subj, $msg, '', '');
    }
}

function CustomizedMailTrailBKM($URt1, $URt2, $Opt_To, $employee_name, $item_name, $booked_date, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#booked_date#', $booked_date, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function BKMTOEMPSIN($URt1, $URt2, $Opt_To, $employee_name, $item_name, $booked_date, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#admin#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#booked_date#', $booked_date, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function BKMTOEMPMUT($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $cust = '')
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#admin#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);

    if ($cust != '') {
        $msg .= "<br> --------------------------------------<br>PS<br><p>$cust</p>";
    }

    QueueMail($To, $Subj, $msg, '', '');
}

function CustomizedMailTrailBKMCsin($URt1, $URt2, $Opt_To, $employee_name, $item_name, $booked_date, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#booked_date#', $booked_date, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function Adminconfsingle($URt1, $URt2, $Opt_To, $employee_name, $item_name, $booked_date, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#booked_date#', $booked_date, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function Admindeclsingle($URt1, $URt2, $Opt_To, $employee_name, $item_name, $booked_date, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#booked_date#', $booked_date, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function CustomizedMailTrailBKM1($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function CustomizedMailTrailBKMCmult($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function Adminconfmult($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $cust)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);

    if ($cust != '') {
        $msg .= "<br> --------------------------------------<br>PS<br><p>$cust</p>";
    }
    QueueMail($To, $Subj, $msg, '', '');
}

function ThirdPartyconf($URt1, $URt2, $email_of_third, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $name_of_third)
{
    $kk = md5($URt1 . $URt2);
    $To = $email_of_third;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    $msg = str_replace('#name_of_third#', $name_of_third, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function AssessmentMailToApplicant($URt1, $URt2, $applicantName, $applicantEmail, $applicantJobTitle, $stDt, $enDt, $link)
{

    $kk = md5($URt1 . $URt2);

    $To = $applicantEmail;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $Subj = str_replace('#job_title#', strtoupper($applicantJobTitle), $Subj);
    $msg = str_replace('#name_of_applicant#', $applicantName, $msg);
    $msg = str_replace('#job_title#', strtoupper($applicantJobTitle), $msg);
    $msg = str_replace('#start_day#', $stDt, $msg);
    $msg = str_replace('#end_day#', $enDt, $msg);
    $msg = str_replace('#assessment_link#', $link, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}
function DeclineMailToApplicant($URt1, $URt2, $applicantName, $applicantEmail, $applicantJobTitle)
{

    $kk = md5($URt1 . $URt2);

    $To = $applicantEmail;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $Subj = str_replace('#job_title#', strtoupper($applicantJobTitle), $Subj);
    $msg = str_replace('#name_of_applicant#', $applicantName, $msg);
    $msg = str_replace('#job_title#', strtoupper($applicantJobTitle), $msg);
    QueueMail($To, $Subj, $msg, '', '');
}
function AutoReplyApp($URt1, $URt2, $applicantName, $applicantEmail, $applicantJobTitle)
{

    $kk = md5($URt1 . $URt2);

    $To = $applicantEmail;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $Subj = str_replace('#job_title#', strtoupper($applicantJobTitle), $Subj);
    $msg = str_replace('#name_of_applicant#', $applicantName, $msg);
    $msg = str_replace('#job_title#', strtoupper($applicantJobTitle), $msg);
    QueueMailRec($To, $Subj, $msg, '', '');
}

function ThirdPartydecl($URt1, $URt2, $email_of_third, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $name_of_third)
{
    $kk = md5($URt1 . $URt2);
    $To = $email_of_third;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);
    $msg = str_replace('#name_of_third#', $name_of_third, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function Admindeclmult($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $cust)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);

    if ($cust != '') {
        $msg .= "<br> --------------------------------------<br>PS<br><p>$cust</p>";
    }
    QueueMail($To, $Subj, $msg, '', '');
}

function supervisorDecline($URt1, $URt2, $Opt_To, $employee_name, $item_name, $fdate, $tdate, $from_time, $to_time, $cust)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#item_name#', strtoupper($item_name), $msg);
    $msg = str_replace('#fdate#', $fdate, $msg);
    $msg = str_replace('#tdate#', $tdate, $msg);
    $msg = str_replace('#from_time#', $from_time, $msg);
    $msg = str_replace('#to_time#', $to_time, $msg);

    if ($cust != '') {
        $msg .= "<br> --------------------------------------<br>PS<br><p>$cust</p>";
    }
    QueueMail($To, $Subj, $msg, '', '');
}

function suggestionFormMailToEmp($URt1, $URt2, $Opt_To, $employee_name)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function overTimeSuccMailToEmp($URt1, $URt2, $Opt_To, $employee_name)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function overTimeDecMailToEmp($URt1, $URt2, $Opt_To, $employee_name, $dec_res)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#decline_reason#', $dec_res, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function overTimeFormMailToEmp($URt1, $URt2, $Opt_To, $employee_name)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function mailToApproveOvertime2($URt1, $URt2, $Opt_To, $employee_name)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_approver#', $employee_name, $msg);
    QueueMail($To, $Subj, $msg, '', '');
}

function mailToVerifyOvertime($URt1, $URt2, $Opt_To, $employee_name, $employee_hr)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#name_of_verifier#', $employee_hr, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function mailToApproveOvertime($URt1, $URt2, $Opt_To, $employee_name, $employee_hr)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#name_of_approver#', $employee_hr, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}
function suggestionFormMailToHR($URt1, $URt2, $Opt_To, $employee_name, $employee_hr)
{

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $employee_name, $msg);
    $msg = str_replace('#name_of_hr#', $employee_hr, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function CustomizedFourMailTrail($URt1, $URt2, $Opt_To, $name, $managerName, $reason)
{
    if (!isset($ChgVal)) {
        $ChgVal = "";
    }

    $To = "";

    $kk = md5($URt1 . $URt2);
    if ($Opt_To == "") {
        /*  // Dont send blank MailTrail, where email is not specify

    $To = ScriptRunner("select top 1 Em.email from EmpTbl Em, Users Ur where Em.EmpID<>'000000' and Em.HashKey<>'f146e0c756af0217c9bf6e4f0f2ac9ee' and Ur.Status='A' and Em.Status='A' and Ur.ULock = 0 and Ur.LStatus='A' and Ur.AcctState='True' and Em.EmpID=Ur.EmpID and Ur.GpName in (Select GpName from [UGpRights] where GpVal=1 and GHashRt='".$kk."' and GPType='C' and [Status]<>'D')","email");

    $Subj = ScriptRunner("select MSub from MailTemp where HashKey='".$kk."'","MSub");
    $Bdy = ScriptRunner("select MMsg from MailTemp where HashKey='".$kk."'","MMsg");
    $Atth = ScriptRunner("select MAttch from MailTemp where HashKey='".$kk."'","MAttch");

    $Subj=str_replace('#ChangeValue#',$ChgVal,$Subj);
    $Bdy=str_replace('#ChangeValue#',$ChgVal,$Bdy);

    QueueMail($To,$Subj,$Bdy,$Atth,$Tm);
     */
    } else {
        $To = $Opt_To;
        $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
        $msg = ScriptRunner($Script_Mail, "MMsg");
        $Subj = ScriptRunner($Script_Mail, "MSub");
        $msg = str_replace('#recepient#', $managerName, $msg);
        $msg = str_replace('#reason#', $reason, $msg);
        $msg = str_replace('#employee#', $name, $msg);
        QueueMail($To, $Subj, $msg, '', '');
    }
}
function payrollApprover($URt1, $URt2, $Opt_To, $name_of_approver)
{
    // var_dump($Opt_To);
    // var_dump($name_of_approver);
    $To = "";

    $kk = md5($URt1 . $URt2);

    $To = $Opt_To;
    // $Script_Mail = "Select * from MailTemp where HashKey='" . $kk . "'";
    // $msg = ScriptRunner($Script_Mail, "MMsg");
    // $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = "Dear #name_of_approver#,<br /> This is to inform you that employees payments has been created.kindly login to authorize these payments";
    $Subj = "Payroll Authorization Notification";
    $msg = str_replace('#name_of_approver#', $name_of_approver, $msg);
    // var_dump($msg);
    // QueueMail($To, $Subj, $msg, '', '');
    $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
    $HashKey = md5($UniqueKey . $_SESSION["StkTck" . "UName"]);
    $Atth = "";
    $Tm = "GetDate()";

    $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . ECh($Subj) . "','" . $To . "','" . ECh($msg) . "','" . ECh($Atth) . "','N','" . $_SESSION["StkTck" . "HKey"] . "'," . $Tm . ",0,'" . $HashKey . "')";
    ScriptRunnerUD($Script_Mail, "QueueMail");
}

function ScriptRunner_dflt($query, $rwfield)
{
    //    echo "<br>".$query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "dflt_myServer"], $_SESSION["StkTck" . "dflt_myUser"], $_SESSION["StkTck" . "dflt_myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "dflt_myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            return ("{$row[$rwfield]}");
            exit;
        }
        return ("");
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "dflt_myServer"], $_SESSION["StkTck" . "dflt_myUser"], $_SESSION["StkTck" . "dflt_myPass"]); // or die("Couldn't open database ");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "dflt_myDB"], $dbhandle); // or die("Couldn't open database ");
        if (!$selected) {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            return ("{$row[$rwfield]}");
        }
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "dflt_myDB"], "UID" => $_SESSION["StkTck" . "dflt_myUser"], "PWD" => $_SESSION["StkTck" . "dflt_myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "dflt_myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
            return ("{$row[$rwfield]}");
        }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunner_BI($query_bi, $rwfield_bi)
{
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') {
        if ($_SESSION["Bi_CType"] == "OLEDB") {
            $dbhandle_bi = mssql_connect($_SESSION["Bi_SName"], $_SESSION["Bi_SUser"], $_SESSION["Bi_SPass"]); // or die("Error connecting to db");
            if (!$dbhandle_bi) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }

            $selected_bi = mssql_select_db($_SESSION["Bi_SDB"], $dbhandle_bi) or die("Couldn't open database " . $_SESSION["StkTck" . "myDB"]);
            $result_bi = mssql_query($query_bi);
            while ($row_bi = mssql_fetch_array($result_bi)) {
                return ("{$row_bi[$rwfield_bi]}");
            }
            $query_bi = "FLUSH PRIVILEGES";
            mssql_close($dbhandle_bi);
        } elseif ($_SESSION["Bi_CType"] == "ODBC") {
            $dbhandle_bi = odbc_connect($_SESSION["DbS"], $_SESSION["DbU"], $_SESSION["DbP"]);
            if (!$dbhandle_bi) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }

            $rs = odbc_exec($dbhandle_bi, $query_bi);
            if (!$rs) {
                exit("Error in SQL");
            }
            while (odbc_fetch_row($rs)) {
                return (odbc_result($rs, $rwfield_bi));
            }
            odbc_close($dbhandle_bi);
        }
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        if ($_SESSION["Bi_CType"] == "OLEDB") {
            $connectionInfo = array("Database" => $_SESSION["Bi_SDB"], "UID" => $_SESSION["Bi_SUser"], "PWD" => $_SESSION["Bi_SPass"]);
            $conn_bi = sqlsrv_connect($_SESSION["Bi_SName"], $connectionInfo);
            if ($conn_bi === false) {
                die(print_r(sqlsrv_errors(), true));
            }
            $result_bi = sqlsrv_query($conn_bi, $query_bi);
            if ($result_bi === false) {
                die(print_r(sqlsrv_errors(), true));
            }
            while ($row_bi = sqlsrv_fetch_array($result_bi, SQLSRV_FETCH_BOTH)) {
                return ("{$row_bi[$rwfield]}");
            }
            sqlsrv_free_stmt($result_bi);
            sqlsrv_close($conn_bi);
        } elseif ($_SESSION["Bi_CType"] == "ODBC") {
            $dbhandle_bi = odbc_connect($_SESSION["DbS"], $_SESSION["DbU"], $_SESSION["DbP"]);
            if (!$dbhandle_bi) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }

            $rs = odbc_exec($dbhandle_bi, $query_bi);
            if (!$rs) {
                exit("Error in SQL");
            }
            while (odbc_fetch_row($rs)) {
                return (odbc_result($rs, $rwfield_bi));
            }
            odbc_close($dbhandle_bi);
        }
    }
}

//ScriptRunner for fox pro with local DB
function ScriptRunner_Fp($query, $rwfield)
{
    $conn = new COM("ADODB.Connection");
    $conn->Open('Provider=VFPOLEDB.1;Data Source=F:\accts_27_06_2012\SSLDBC\ssldbc.dbc;Collating Sequence=MACHINE');
    // SQL statement to build recordset.
    $rs = $conn->Execute($query); //"SELECT * FROM tranmain";
    // Display all the values in the records set
    while (!$rs->EOF) {
        $fv = $rs->Fields($rwfield); //"imainkey" , "cnumber"
        return $fv->value;
        //$rs->MoveNext();
    }
    $rs->Close();
}

function ScriptRunnerUD($query, $rwfield)
{
    //        echo $query;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query) or die('Error, insert query failed');
        $query = "FLUSH PRIVILEGES"; //mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        //echo $query;
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open database " . $_SESSION["StkTck"."myDB"] );
        if (!$selected) {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        $query = "FLUSH PRIVILEGES"; //mssql_close($dbhandle);
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}

function ScriptRunnerUD_NC($query, $rwfield)
{
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query) or die('Error, insert query failed');
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open database " . $_SESSION["StkTck"."myDB"] );
        if (!$selected) {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        $query = "FLUSH PRIVILEGES"; //mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        $result = sqlsrv_query($conn, $query);

        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunnerJoin($query, $rwfield)
{
    $myval = "";
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            if ($myval == "") {
                $myval = "'" . ("{$row[$rwfield]}") . "'";
            } else {
                $myval = $myval . ",'" . ("{$row[$rwfield]}") . "'";
            }
        }
        $query = "FLUSH PRIVILEGES";
        mysql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') {
        $dbhandle = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }

        $selected = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle); // or die("Couldn't open database " . $_SESSION["StkTck"."myDB"] );
        if (!$selected) {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=2';}</script>");
        }

        $result = mssql_query($query);
        while ($row = mssql_fetch_array($result)) {
            if ($myval == "") {
                $myval = "'" . ("{$row[$rwfield]}") . "'";
            } else {
                $myval = $myval . ",'" . ("{$row[$rwfield]}") . "'";
            }
        }
        return ($myval);
        $query = "FLUSH PRIVILEGES";
        mssql_close($dbhandle);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        //echo $query;
        $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
        $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);
        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {
            die(print_r(sqlsrv_errors(), true));
        }
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
            if ($myval == "") {
                $myval = "'" . ("{$row[$rwfield]}") . "'";
            } else {
                $myval = $myval . ",'" . ("{$row[$rwfield]}") . "'";
            }
        }
        return ($myval);
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);
    }
}

function ScriptRunnerXML($query, $rwfield)
{
    $Del_XML = 0;
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        // No connection parameter for MySQL set yet
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL-PRE2005') //Checks if hosting server is MSSQL
    {
        if ($_SESSION["Bi_CType"] == "OLEDB") {
            $dbhandle = mssql_connect($_SESSION["Bi_SName"], $_SESSION["Bi_SUser"], $_SESSION["Bi_SPass"]) or die("Error connecting to db");
            if (!$dbhandle) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }
            $selected = mssql_select_db($_SESSION["Bi_SDB"], $dbhandle) or die("Couldn't open database " . $_SESSION["StkTck" . "myDB"]);
            $result = mssql_query($query);
            while ($row = mssql_fetch_array($result)) {
                if ($rwfield == 2) {
                    if ($myval == "") {
                        $XVal_Dd = str_replace("&", "&amp;", "{$row[X]}"); // & replace
                        $XVal_Dd = str_replace("'", "&acute;", $XVal_Dd); // ' replace
                        $XVal_Dd = str_replace('"', "&quot;", $XVal_Dd); // " replace

                        $myval = chr(13) . chr(10) . "<set name='" . $XVal_Dd . "' value='" . "{$row[Y]}" . "' " . GetColor($Del_XML) . $Link_XML . " />";
                    } else {
                        //$myval = $myval.(chr(13).chr(10)."<set name='"."{$row[X]}"."' value='"."{$row[Y]}"."'".GetColor($Del).$Link." />";
                        //$YVal =str_replace("&", "&amp;", "{$row_sub[Y]}");
                        $XVal_Dd = str_replace("&", "&amp;", "{$row[X]}"); // & replace
                        $XVal_Dd = str_replace("'", "&acute;", $XVal_Dd); // ' replace
                        $XVal_Dd = str_replace('"', "&quot;", $XVal_Dd); // " replace

                        $myval = $myval . chr(13) . chr(10) . "<set name='" . $XVal_Dd . "' value='" . "{$row[Y]}" . "' " . GetColor($Del) . $Link . " />";
                    }
                }
            }
            return ($myval);
            $query = "FLUSH PRIVILEGES";
            mssql_close($dbhandle);
        } elseif ($_SESSION["Bi_CType"] == "ODBC") {
            $conn = odbc_connect($_SESSION["DbS"], $_SESSION["DbU"], $_SESSION["DbP"]);
            //if (!$conn) {exit("Connection Failed: " . $conn);}
            if (!$conn) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }
            $rs = odbc_exec($conn, $query);
            if (!$rs) {
                exit("Error in SQL");
            }
            while (odbc_fetch_row($rs)) {
                if ($rwfield = 2) {
                    if ($myval == "") {
                        $myval = chr(13) . chr(10) . "<set name='" . odbc_result($rs, "X") . "' value='" . odbc_result($rs, "Y") . "' " . GetColor($Del_XML) . $Link_XML . " />";
                    } else {
                        //$myval = $myval . (chr(13).chr(10) . "<set name='" . "{$row[X]}" . "' value='" . "{$row[Y]}"  . "' ".GetColor($Del).$Link." />";
                        $myval = $myval . chr(13) . chr(10) . "<set name='" . odbc_result($rs, "X") . "' value='" . odbc_result($rs, "Y") . "' " . GetColor($Del) . $Link . " />";
                    }
                }
            }
            return ($myval);
            odbc_close($conn);
        }
    } // ends check for hosting server type
    elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') //MSSQL-PRE2005
    {
        if ($_SESSION["Bi_CType"] == "OLEDB") {
            $connectionInfo = array("Database" => $_SESSION["Bi_SDB"], "UID" => $_SESSION["Bi_SUser"], "PWD" => $_SESSION["Bi_SPass"]);
            $conn_bi = sqlsrv_connect($_SESSION["Bi_SName"], $connectionInfo);
            if ($conn_bi === false) {
                die(print_r(sqlsrv_errors(), true));
            }
            $result_bi = sqlsrv_query($conn_bi, $query_bi);
            if ($result_bi === false) {
                die(print_r(sqlsrv_errors(), true));
            }
            while ($row_bi = sqlsrv_fetch_array($result_bi, SQLSRV_FETCH_BOTH)) {
                if ($rwfield == 2) {
                    if ($myval == "") {
                        $XVal_Dd = str_replace("&", "&amp;", "{$row[X]}"); // & replace
                        $XVal_Dd = str_replace("'", "&acute;", $XVal_Dd); // ' replace
                        $XVal_Dd = str_replace('"', "&quot;", $XVal_Dd); // " replace

                        $myval = chr(13) . chr(10) . "<set name='" . $XVal_Dd . "' value='" . "{$row[Y]}" . "' " . GetColor($Del_XML) . $Link_XML . " />";
                    } else {
                        //$myval = $myval.(chr(13).chr(10)."<set name='"."{$row[X]}"."' value='"."{$row[Y]}"."'".GetColor($Del).$Link." />";
                        //$YVal =str_replace("&", "&amp;", "{$row_sub[Y]}");
                        $XVal_Dd = str_replace("&", "&amp;", "{$row[X]}"); // & replace
                        $XVal_Dd = str_replace("'", "&acute;", $XVal_Dd); // ' replace
                        $XVal_Dd = str_replace('"', "&quot;", $XVal_Dd); // " replace

                        $myval = $myval . chr(13) . chr(10) . "<set name='" . $XVal_Dd . "' value='" . "{$row[Y]}" . "' " . GetColor($Del) . $Link . " />";
                    }
                }
            }
            sqlsrv_free_stmt($result_bi);
            sqlsrv_close($conn_bi);
        } elseif ($_SESSION["Bi_CType"] == "ODBC") {
            $conn = odbc_connect($_SESSION["DbS"], $_SESSION["DbU"], $_SESSION["DbP"]);
            //if (!$conn) {exit("Connection Failed: " . $conn);}
            if (!$conn) //Error Handling if Database Server is down
            {
                echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=3';}</script>");
            }
            $rs = odbc_exec($conn, $query);
            if (!$rs) {
                exit("Error in SQL");
            }
            while (odbc_fetch_row($rs)) {
                if ($rwfield = 2) {
                    if ($myval == "") {
                        $myval = chr(13) . chr(10) . "<set name='" . odbc_result($rs, "X") . "' value='" . odbc_result($rs, "Y") . "' " . GetColor($Del_XML) . $Link_XML . " />";
                    } else {
                        //$myval = $myval . (chr(13).chr(10) . "<set name='" . "{$row[X]}" . "' value='" . "{$row[Y]}"  . "' ".GetColor($Del).$Link." />";
                        $myval = $myval . chr(13) . chr(10) . "<set name='" . odbc_result($rs, "X") . "' value='" . odbc_result($rs, "Y") . "' " . GetColor($Del) . $Link . " />";
                    }
                }
            }
            return ($myval);
            odbc_close($conn);
        }
    }
}

//function GetChartParam($Script_Get_Val,$ChartID)
function GetChartParam($Script_Get_Val)
{
    //echo $Script_Get_Val;
    //exit;
    $myval = "";
    if ($_SESSION["DbaseConTyp"] == 'MySQL') {
        $dbhandle2 = mysql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]) or die('Error connecting to db');
        mysql_select_db($_SESSION["StkTck" . "myDB"]);
        $result2 = mysql_query($Script_Get_Val);
        while ($row2 = mysql_fetch_array($result2, MYSQL_ASSOC)) {
            $_SESSION["CDesc" . $ChartID] = "{$row2[CDesc]}";
            $_SESSION["xAxisName" . $ChartID] = "{$row2[XName]}";
            $_SESSION["yAxisName" . $ChartID] = "{$row2[YName]}";
            $_SESSION["Pcn" . $ChartID] = "{$row2[DPrec]}";
            $_SESSION["CIDName" . $ChartID] = "{$row2[CType]}";
            $_SESSION["SubCap" . $ChartID] = "{$row2[SubCap]}";
            $_SESSION["XPrefix" . $ChartID] = "{$row2[XPrefix]}";
            $_SESSION["XSuffix" . $ChartID] = "{$row2[XSuffix]}";
            $_SESSION["NSuffix" . $ChartID] = "{$row2[NSuffix]}";
            $_SESSION["DspName" . $ChartID] = "{$row2[DspName]}";
            $_SESSION["ChartType" . $ChartID] = "{$row2[ChartType]}";
        }
        $query2 = "FLUSH PRIVILEGES";
        mysql_close($dbhandle2);
    } elseif ($_SESSION["DbaseConTyp"] == 'MSSQL') {
        $dbhandle2 = mssql_connect($_SESSION["StkTck" . "myServer"], $_SESSION["StkTck" . "myUser"], $_SESSION["StkTck" . "myPass"]); // or die("Error connecting to db");
        if (!$dbhandle2) //Error Handling if Database Server is down
        {
            echo ("<script type='text/javascript'>{parent.document.location.href='http://" . $_SERVER['HTTP_HOST'] . "/SSLCloud/error/trap_error.php?ErrTyp=1';}</script>");
        }
        $selected2 = mssql_select_db($_SESSION["StkTck" . "myDB"], $dbhandle2) or die("Couldn't open database " . $_SESSION["StkTck" . "myDB"]);
        $result2 = mssql_query($Script_Get_Val);
        while ($row2 = mssql_fetch_array($result2)) {
            $_SESSION["CDesc" . $ChartID] = "{$row2[CDesc]}";
            $_SESSION["xAxisName" . $ChartID] = "{$row2[XName]}";
            $_SESSION["yAxisName" . $ChartID] = "{$row2[YName]}";
            $_SESSION["Pcn" . $ChartID] = "{$row2[DPrec]}";
            $_SESSION["CIDName" . $ChartID] = "{$row2[CType]}";
            $_SESSION["SubCap" . $ChartID] = "{$row2[SubCap]}";
            $_SESSION["XPrefix" . $ChartID] = "{$row2[XPrefix]}";
            $_SESSION["XSuffix" . $ChartID] = "{$row2[XSuffix]}";
            $_SESSION["NSuffix" . $ChartID] = "{$row2[NSuffix]}";
            $_SESSION["DspName" . $ChartID] = "{$row2[DspName]}";
            $_SESSION["ChartType" . $ChartID] = "{$row2[ChartType]}";
        }
        $query2 = "FLUSH PRIVILEGES";
        mssql_close($dbhandle2);
    }
}

function findexts($filename)
{
    $filename = strtolower($filename);
    $exts = split("[/\\.]", $filename);
    $n = count($exts) - 1;
    $exts = $exts[$n];
    return $exts;
}

function Mod_addslashes($string)
{
    if (get_magic_quotes_gpc() == 1) {
        return ($string);
    } else {
        return (addslashes($string));
    }
}

function ValidateUpdDel($query)
{
    if (ScriptRunner($query, "Status") == 'A') {
        echo ("<script type='text/javascript'>{ parent.msgbox('Cannot update or delete authorized record(s).', 'red'); }</script>");
        $EditID = 0;
        $AuthID = 0;
        return (false);
    } else {
        return (true);
    }
}

// Lawrence update for inactive employee
function ValidateUpdDel1($query)
{
    if (ScriptRunner($query, "EmpStatus") == 'Active') {
        echo ("<script type='text/javascript'>{ parent.msgbox('Cannot update or delete authorized record(s).', 'red'); }</script>");
        $EditID = 0;
        $AuthID = 0;
        return (false);
    } else {
        return (true);
    }
}

function ValidateURths($CodecRt)
{
    if (!isset($_SESSION["StkTck" . "UGrp"])) {
        $_SESSION["StkTck" . "UGrp"] = "";
    }

    $te = "Select count(GpVal) Ct from [UGpRights] where HashKey='" . md5($_SESSION["StkTck" . "UGrp"] . $CodecRt) . "' and GPType='C' and Status<>'D'";
    if (ScriptRunner($te, "Ct") == 0) {
        return (false);
    } else {
        //Gets the user rights and sends it back (return function)
        //If any error from here then the user right is not in the URights Table with that Company ID
        //Add the user right and SET IT TO ZERO "0". NULL gives ERRORS ////

        //The above error has been handled by the ***Count*** Line above
        if (ScriptRunner("Select GpVal from [UGpRights] where HashKey='" . md5($_SESSION["StkTck" . "UGrp"] . $CodecRt) . "' and GPType='C' and Status<>'D'", "GpVal") == 1) {
            //User Does Not Have This Access Right
            return (true);
        } else {
            return (false);
        }
    }
}

function ResetTabs($Rst)
{
    if ($Rst == "True") {
        for ($oo = 1; $oo <= 5; $oo++) {
            echo ("<script type='text/javascript'>{ parent.refresh_2self_frame('frame_" . $oo . "_mini'); }</script>");
        }
    }
}

function CheckPasswordStrength($password)
{
    //------ start
    $Script = "Select SetValue from Settings where Setting='UpperCase'";
    $upper = ScriptRunner($Script, "SetValue");

    $Script = "Select SetValue from Settings where Setting='MinPwdLen'";
    $max_lenght = ScriptRunner($Script, "SetValue");
    //-------- end

    $strength = 0;

    /*$patterns = array('#[a-z]#','#[A-Z]#','#[0-9]#','/[�!"�$%^&*()`{}\[\]:@~;\'#<>?,.\/\\-=_+\|]/');

    foreach($patterns as $pattern)
    {
    if(preg_match($pattern,$password,$matches))
    {
    $strength++;
    }
    }
     */

    //if (strlen($password) >= 6)
    if (strlen($password) >= $max_lenght) {
        $strength++;
    }
    return $strength;
    // 1 - weak     // 2 - not weak     // 3 - acceptable     // 4 - strong  // 5 - very strong
}

class Encryption
{
    public $skey = "BridgeMgr";

    public function safe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value)
    {
        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value)
    {
        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
} // End Encryption Class

//HACK ATTEMP MODULES BELOW
function getRealIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) { //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function writeLog($where)
{

    $ip = getRealIp(); // Get the IP from superglobal
    $host = gethostbyaddr($ip); // Try to locate the host of the attack
    $date = date("d M Y");

    // create a logging message with php heredoc syntax
    $logging = <<<LOG
		\n
		<< Start of Message >>
		There was a hacking attempt on your form. \n
		Date of Attack: {$date}
		IP-Adress: {$ip} \n
		Host of Attacker: {$host}
		Point of Attack: {$where}
		<< End of Message >>
LOG;
    // Awkward but LOG must be flush left

    // open log file
    if ($handle = fopen('hacklog.log', 'a')) {

        fputs($handle, $logging); // write the Data to file
        fclose($handle); // close the file

    } else { // if first method is not working, for example because of wrong file permissions, email the data

        $to = 'customersupport@ss-limited.com ';
        $subject = 'HACK ATTEMPT';
        $header = 'From: SSLCloud';
        if (mail($to, $subject, $logging, $header)) {
            echo "Sent notice to admin.";
        }
    }
}

function verifyFormToken($form)
{

    // check if a session is started and a token is transmitted, if not return an error
    if (!isset($_SESSION[$form . '_token'])) {
        return false;
    }

    // check if the form is sent with token in it
    if (!isset($_POST['token'])) {
        return false;
    }

    // compare the tokens against each other if they are still the same
    if ($_SESSION[$form . '_token'] !== $_POST['token']) {
        return false;
    }

    return true;
}

function DoSFormToken()
{
    // generate a token from an unique value, took from microtime, you can also use salt-values, other crypting methods...
    //$token = md5(uniqid(microtime(), true));
    // Write the generated token to the session variable to check it against the hidden field when the form is sent

    $ScriptGbl = "Select SetValue from Settings where Setting='SrvHeader'";
    $_SESSION["StkTck" . "SHost"] = ScriptRunner($ScriptGbl, "SetValue");

    /*
if ((strtolower(trim($_SERVER['HTTP_HOST'])) != strtolower(trim($_SESSION["StkTck"."SHost"]))) && strtolower(trim($_SERVER['HTTP_HOST'])) != strtolower(trim("www.".$_SESSION["StkTck"."SHost"])))
{
AuditLog("HACK_ATTEMPT","Hack attempt carried out from [".$_SERVER['HTTP_REFERER']."]. Login failed.");

unset($_SESSION["StkTck"."CustID"]);
unset($_SESSION["StkTck"."Cust"]);
unset($_SESSION["StkTck"."UName"]);
unset($_SESSION["StkTck"."UGrp"]);
unset($_SESSION["StkTck"."AcctActive"]);
unset($_SESSION["StkTck"."Email"]);
unset($_SESSION["StkTck"."FName"]);

unset($_SESSION["StkTck"."Mobile"]);
unset($_SESSION["StkTck"."Title"]);
unset($_SESSION["StkTck"."Sex"]);
unset($_SESSION["StkTck"."mylogin_Status"]);
unset($_SESSION["StkTck"."LoginMsg"]);
unset($_SESSION["StkTck"."YesNo_Page"]);
unset($_SESSION["StkTck"."LoggedIn"]);
unset($_SESSION["StkTck"."SysDate"]);
unset($_SESSION["StkTck"."myServer"]);
unset($_SESSION["StkTck"."myUser"]);
unset($_SESSION["StkTck"."myPass"]);
unset($_SESSION["StkTck"."myDB"]);
unset($_SESSION["StkTck"."MailHeader"]);
unset($_SESSION["StkTck"."Audit_Type"]);
unset($_SESSION["StkTck"."Audit_Ret"]);
unset($_SESSION["StkTck"."Session"."Warning"]);
unset($_SESSION["StkTck"."Session"."NotfType"]);
unset($_SESSION["StkTck"."DDoS"]);
unset($_SESSION["StkTck"."SHost"]);

//header("Location:".$_SERVER['HTTP_HOST']); //sign;

exit;
}
else
{
 */
    $token = md5(date('l jS \of F Y h:i:s A') . gettimeofday(true) . "BlockDoSAttack" . $_SESSION["StkTck" . "UName"]);
    $_SESSION['DoS' . $token] = md5('DoS' . $_SESSION["StkTck" . "UName"] . $token);
    //echo $token;
    return $token;
    //}
}

//=================================================================================
//============================== GET BROWSER TYPE =================================
//=================================================================================
function GetBrowserType()
{
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/MSIE/i', $user_agent)) {
        return "MSIE";
    } else {
        return "Non-IE";
    }
}

function getLevel1($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['EmpMgr']);
}
function getLevel2($hash)
{
    $hash = getLevel1($hash);
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['EmpMgr']);
}

function getLevel3($hash)
{
    $hash = getLevel2($hash);
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['EmpMgr']);
}

function getUserDetailsfromHash($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    return ($ok);
}

function taskToDo($Opt_To, $sender_name, $ItemsToDo, $datetimeToDo, $sendDate)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Task Reminder'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_employee#', $sender_name, $msg);
    $msg = str_replace('#ItemsToDo#', strtoupper($ItemsToDo), $msg);
    $msg = str_replace('#datetimeToDo#', $datetimeToDo, $msg);
    $msg = str_replace('#sendDate#', $sendDate, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to an assigned user
 * param $arrayAssignTo : Array of recipients
 */
function assignTaskTo($arrayAssignTo, $sender_name, $ItemsToDo, $datetimeToDo, $assignNotify)
{

    $Script_Mail = "Select * from MailTemp where [MSub]='Assign Task'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#arrayAssignTo#', $arrayAssignTo['SName'], $msg);
    $msg = str_replace('#name_of_employee#', $sender_name, $msg);
    $msg = str_replace('#ItemsToDo#', strtoupper($ItemsToDo), $msg);
    $msg = str_replace('#datetimeToDo#', $datetimeToDo, $msg);
    $msg = str_replace('#assignNotify#', $assignNotify, $msg);

    QueueMail($arrayAssignTo['Email'], $Subj, $msg, '', '');
}

/*
 * Function sends mail to authorizers on shift allocation
 */
function allocationMailToAuthorizers($Opt_To, $authorizer_name, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Authorize Shift Allocation'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_authorizer#', $authorizer_name, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to each authorizers except observer on shift authorization
 */
function sendMailToAuthorizers($Opt_To, $authorizer_name, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Shift Authorization'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_authorizer#', $authorizer_name, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to each authorizers except observer on shift de-authorization
 */
function mailForDeAuthorization($Opt_To, $authorizer_name, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Shift De-Authorization'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_authorizer#', $authorizer_name, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to observer on shift authorization
 */
function sendMailToObserver($Opt_To, $authorizer_name, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Shift Authorization (Observer)'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_authorizer#', $authorizer_name, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to employee that made the swap request upon approval
 */
function swapRequestApprovalMail($Opt_To, $requested_by, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Swap Request Approved'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#name_of_requester#', $authorizer_name, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

/*
 * Function sends mail to employee that just received the new shift allocation
 */
function approvedSwapAllocationMail($Opt_To, $selected_employee, $requested_by, $shift_name)
{

    $To = $Opt_To;
    $Script_Mail = "Select * from MailTemp where [MSub]='Approved Swap Allocation'";

    $msg = ScriptRunner($Script_Mail, "MMsg");
    $Subj = ScriptRunner($Script_Mail, "MSub");
    $msg = str_replace('#seleceted_employee_name#',  $selected_employee, $msg);
    $msg = str_replace('#name_of_requester#', $requested_by, $msg);
    $msg = str_replace('#name_of_shift#', $shift_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}


/*
 * @params $query
 * @response array
 */
function getData($query)
{
    $connectionInfo = array("Database" => $_SESSION["StkTck" . "myDB"], "UID" => $_SESSION["StkTck" . "myUser"], "PWD" => $_SESSION["StkTck" . "myPass"]);
    $conn = sqlsrv_connect($_SESSION["StkTck" . "myServer"], $connectionInfo);

    $result = sqlsrv_query($conn, $query);

    $output = [];
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        array_push($output, $row);
    }
    sqlsrv_close($conn);

    return $output;
}

function isUserBranchContact($emp_hash)
{

    $brach_arry = [];
    $dbOpen2 = "Select * from [BrhMasters] where [Status]='A' and [OContact]='{$emp_hash}'";

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
        $brach_arry[] = $row2;
    }
    include '../login/dbClose2.php';

    $dbOpen2 = "Select * from [BrhMasters] where [Status]='A' and [OContact2]='{$emp_hash}'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
        $brach_arry[] = $row2;
    }
    include '../login/dbClose2.php';

    $dbOpen2 = "Select * from [BrhMasters] where [Status]='A' and [OContact3]='{$emp_hash}'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
        $brach_arry[] = $row2;
    }
    include '../login/dbClose2.php';

    $dbOpen2 = "Select * from [BrhMasters] where [Status]='A' and [OContact4]='{$emp_hash}'";

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
        $brach_arry[] = $row2;
    }
    include '../login/dbClose2.php';

    // var_dump($brach_arry);
    // die();
    return $brach_arry;
}

function getBranchContact($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    $branch_id = $ok['BranchID'];
    $query = "Select * from [BrhMasters] where [Status]='A' and  [HashKey]='" . $branch_id . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['OContact']);
}
function getBranchContact2($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    $branch_id = $ok['BranchID'];
    $query = "Select * from [BrhMasters] where [Status]='A' and [HashKey]='" . $branch_id . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['OContact2']);
}
function getBranchContact3($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    $branch_id = $ok['BranchID'];
    $query = "Select * from [BrhMasters] where [Status]='A' and [HashKey]='" . $branch_id . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['OContact3']);
}
function getBranchContact4($hash)
{
    $query = "Select * from [EmpTbl] where [HashKey]='" . $hash . "'";
    $ok = ScriptRunnercous($query);
    $branch_id = $ok['BranchID'];
    $query = "Select * from [BrhMasters] where [Status]='A' and [HashKey]='" . $branch_id . "'";
    $ok = ScriptRunnercous($query);
    return ($ok['OContact4']);
}

function sendMailToContactOfficer($Opt_To, $recipient_name, $sender_name, $payMonth, $branchName)
{

    $To = $Opt_To;

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has just created salary for #payMonth#.Kindly login and either approve or cancel his/her request.";
    $Subj = "Salary Created Notification ($branchName) - $payMonth";
    $msg = str_replace('#Name#', $recipient_name, $msg);
    $msg = str_replace('#EmpName#', $sender_name, $msg);
    $msg = str_replace('#payMonth#', $payMonth, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function completedSalaryPaymentReviewMail($Opt_To, $recipient_name, $sender_name)
{

    $To = $Opt_To;

    $msg = "Dear #Name#,<br /> This is to inform you that employee #EmpName# has just reviewed the salary payment crated by you.";
    $Subj = "Salary Review Completed Notification";
    $msg = str_replace('#Name#', $recipient_name, $msg);
    $msg = str_replace('#EmpName#', $sender_name, $msg);

    QueueMail($To, $Subj, $msg, '', '');
}

function getPayeOrder($TotTaxable, $payScheme, $GrossPay)
{

    // var_dump($TotTaxable, $payScheme, $GrossPay);

    $script = "select * from [Fin_PRSettings] where [GName] = '" . sanitize($payScheme) . "' ";
    $scheme_record = ScriptRunnercous($script);

    $PayScheme_ = $scheme_record['HashKey'];
    $Script_TAX = "SELECT * from FinTax where HashKey=(SELECT PAYEScheme from Fin_PRSettings where HashKey ='" . $PayScheme_ . "' and Status in ('A'))";


    $PAYE = 0;
    $PAYE0_ = 0;
    $PAYE1_ = 0;
    $PAYE2_ = 0;
    $PAYE3_ = 0;
    $PAYE4_ = 0;
    $PAYE5_ = 0;
    $PAYE6_ = 0;



    $PAYE0 = ScriptRunner($Script_TAX, "Limit0");
    $PAYE1 = ScriptRunner($Script_TAX, "Limit1");
    $PAYE2 = ScriptRunner($Script_TAX, "Limit2");
    $PAYE3 = ScriptRunner($Script_TAX, "Limit3");
    $PAYE4 = ScriptRunner($Script_TAX, "Limit4");
    $PAYE5 = ScriptRunner($Script_TAX, "Limit5");
    $PAYE6 = ScriptRunner($Script_TAX, "Limit6");
    $PAYE7 = ScriptRunner($Script_TAX, "Limit7");
    $PAYE_Comm0 = ScriptRunner($Script_TAX, "pcent0");
    $PAYE_Comm1 = ScriptRunner($Script_TAX, "pcent1");
    $PAYE_Comm2 = ScriptRunner($Script_TAX, "pcent2");
    $PAYE_Comm3 = ScriptRunner($Script_TAX, "pcent3");
    $PAYE_Comm4 = ScriptRunner($Script_TAX, "pcent4");
    $PAYE_Comm5 = ScriptRunner($Script_TAX, "pcent5");
    $PAYE_Comm6 = ScriptRunner($Script_TAX, "pcent6");
    $PAYE_Comm7 = ScriptRunner($Script_TAX, "pcent7");

    if (($TotTaxable < $PAYE0)) {
        $PAYE0_ = ($GrossPay * $PAYE_Comm0) / 100;
    } else {

        //Amount taxed on first band
        if (($TotTaxable <= ($PAYE1)) && ($GrossPay > $PAYE0)) {
            $PAYE_Rem = $TotTaxable;
            $PAYE1_ = (($PAYE_Rem * $PAYE_Comm1) / 100);
        }

        if ($TotTaxable >= ($PAYE1)) {
            $PAYE1_ = $PAYE1_ + (($PAYE1 * $PAYE_Comm1) / 100);
        }

        //If Amount Left is not up to SECOND BAND (PAYE2)
        if (($TotTaxable > ($PAYE1)) && ($TotTaxable < ($PAYE1 + $PAYE2))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1);
            $PAYE2_ = $PAYE2_ + (($PAYE_Rem * $PAYE_Comm2) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2)) {
            $PAYE2_ = $PAYE2_ + (($PAYE2 * $PAYE_Comm2) / 100);
        }

        //If Amount Left is not up to THIRD BAND (PAYE3)
        if (($TotTaxable > ($PAYE1 + $PAYE2)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2);
            $PAYE3_ = ($PAYE3_ + (($PAYE_Rem * $PAYE_Comm3) / 100));
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3)) {
            $PAYE3_ = $PAYE3_ + (($PAYE3 * $PAYE_Comm3) / 100);
        }

        //If Amount Left is not up to FOURTH BAND (PAYE4)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3);
            $PAYE4_ = $PAYE4_ + (($PAYE_Rem * $PAYE_Comm4) / 100);
        }

        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) {
            $PAYE4_ = $PAYE4_ + (($PAYE4 * $PAYE_Comm4) / 100);
        }

        //alert ($TotTaxable);

        //If Amount Left is not up to FIFTH BAND (PAYE5)
        if (($TotTaxable > ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4)) && ($TotTaxable < ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5))) {
            $PAYE_Rem = $TotTaxable - ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4);
            $PAYE5_ = $PAYE5_ + (($PAYE_Rem * $PAYE_Comm5) / 100);
        }
        //If Amount Left is not up to FIFTH BAND (PAYE5)
        if ($TotTaxable >= ($PAYE1 + $PAYE2 + $PAYE3 + $PAYE4 + $PAYE5)) {
            $PAYE5_ = $PAYE5_ + (($PAYE5 * $PAYE_Comm5) / 100);
        }

        //If Amount Left is not up to SIXTH BAND (PAYE6)

        if ($TotTaxable > $PAYE6) {
            $PAYE6_ = $PAYE6_ + ((($TotTaxable - $PAYE6) * $PAYE_Comm6) / 100);
        }
    }


    return [
        "PAYE0" => ($GrossPay * $PAYE_Comm0) / 100,
        "PAYE1" => $PAYE1_,
        "PAYE2" => $PAYE2_,
        "PAYE3" => $PAYE3_,
        "PAYE4" => $PAYE4_,
        "PAYE5" => $PAYE5_,
        "PAYE6" => $PAYE6_,

    ];
}


function getCar($gross, $cra_gross)
{
    $resullt = 0;

    if (is_numeric($gross) && is_numeric($cra_gross)) {

        $resullt = max(0.01 * $gross, 200000) + 0.2 * $cra_gross;
    }


    return $resullt;
};


unset($_SESSION["StkTck" . "SHost"]);
$_SERVER['HTTP_HOST'];

<?php

session_start();
$UserEmail = "";
$PgLd = "Exclude";
$bg_array = [
  'bgimg.png',
  'host.svg',
  'Cart.svg',
  'rel.svg',
  // 'web.svg',
  'flip.svg',
  'project.svg',
  'analyse.svg',
  'idea.svg',
  'blogging.svg',
  'web_flip.svg',

];
$rag = rand(0, (count($bg_array) - 1));
unset($_SESSION["StkTck" . "UName"]);
unset($_SESSION["StkTck" . "AcctActive"]);
unset($_SESSION["StkTck" . "Email"]);
unset($_SESSION["StkTck" . "FName"]);

unset($_SESSION["StkTck" . "Mobile"]);
unset($_SESSION["StkTck" . "Title"]);
unset($_SESSION["StkTck" . "Sex"]);
unset($_SESSION["StkTck" . "GpTOut"]);
unset($_SESSION["StkTck" . "mylogin_Status"]);
unset($_SESSION["StkTck" . "MailHeader"]);
//unset($_SESSION["StkTck"."LoginMsg"]);
unset($_SESSION["StkTck" . "YesNo_Page"]);
//        unset($_SESSION["StkTck"."LoginMsg"]);
unset($_SESSION["StkTck" . "LoggedIn"]);
//$_SESSION["StkTck"."LoggedIn"]= "true"; //Use once mylogin id TRUE

$_SESSION["StkTck" . "Login_Status"] = "False";

$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = false;
include '../css/myscripts.php'; ?>

<head>
  <title>SSL Cloud Services</title>

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <link rel="stylesheet" type="text/css" media="screen" href="bootstrap/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="bootstrap/css/bootstrap-responsive.min.css">
  <link href="../css/core.css" rel="stylesheet" type="text/css" />
  <link href="../css/style_main.css" rel="stylesheet" type="text/css" />




  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../assets/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="../assets/css/responsive.css">
  <style>
    html {
      height: :100vh;
      overflow: hidden;
    }

    body {
      /* height: auto !important; */

      background-image: url(../assets/dist/img/<?php echo "$bg_array[$rag]" ?>), url(../assets/dist/img/A6.jpg) !important;


      /* ,linear-gradient(#ff7979, #644646) !important */
      background-repeat: no-repeat !important;
      background-size: cover !important;
      background-position: 10% 20% !important;
      /* -webkit-mask-image: url(../assets/dist/img/blogging.svg);
  mask-image: url(../assets/dist/img/blogging.svg);
  background-color: red; */

      /* filter: invert(1) sepia(1) saturate(5) hue-rotate(175deg); */

    }


    .btn-danger.active,
    .btn-danger:active,
    .open>.dropdown-toggle.btn-danger {
      color: #fff;
      background-color: #00a65a;
      border-color: #00a65a !important;
    }

    .btn-danger:hover {

      border-color: #00a65a !important;
    }

    .btn-danger.focus,
    .btn-danger:focus {
      color: #fff;
      background-color: #00a65a !important;
      border-color: #00a65a !important;
    }

    @font-face {
      font-family: 'Conv_SHOWG';
      src: url('fonts/SHOWG.eot');
      src: local('☺'), url('fonts/SHOWG.woff') format('woff'), url('fonts/SHOWG.ttf') format('truetype'), url('fonts/SHOWG.svg') format('svg');
      font-weight: normal;
      font-style: normal;
    }

    .login-logo {
      font-family: 'Conv_SHOWG' !important;
      font-style: italic;
    }

    .login-box,
    .register-box {

      width: 404px;
      margin: 4% auto;
      padding-top: 13%;
      overflow: hidden;
      height: 100vh !important;

    }

    .left {
      background: radial-gradient(black, transparent);
      height: :100% !important;
    }

    .login-box-body,
    .register-box-body {
      background: none !important;
      /* padding: 20px; */
      border-top: 0;
      color: #fff;
    }

    .login-page {
      overflow: hidden;

    }

    .i-right {

      background: radial-gradient(black, transparent);
      height: 100vh;
      clip-path: polygon(0 0, 21% 0, 7% 100%, 0 100%);

    }

    @media (min-width:320px) {

      /* smartphones, iPhone, portrait 480x320 phones */
      .login-box,
      .register-box {

        margin-top: 0px !important;
        padding-top: 20%;

        /* overflow: scroll; */

      }

    }

    @media (min-width:481px) {

      /* portrait e-readers (Nook/Kindle), smaller tablets @ 600 or @ 640 wide. */
      .login-box,
      .register-box {

        margin-top: 0px !important;
        padding-top: 20%;

        /* overflow: scroll; */

      }

    }

    @media (min-width:641px) {

      /* portrait tablets, portrait iPad, landscape e-readers, landscape 800x480 or 854x480 phones */
      .login-box,
      .register-box {

        margin-top: 0px !important;
        padding-top: 0%;

        /* overflow: scroll; */

      }

    }

    @media (min-width:961px) {

      /* tablet, landscape iPad, lo-res laptops ands desktops */
      .login-box,
      .register-box {

        margin-top: 0px !important;
        padding-top: 13%;

        /* overflow: scroll; */

      }

    }
  </style>
</head>
<!-- #d73131
#ff7c7c -->

<body class="hold-transition login-page">
  <div class="row">
    <div class="col-sm-6  left">
      <div class="login-box">
        <div class="login-logo">
          <a style="color: #FFF" href=""><b>SSL</b>Cloud <span style="top: 10px; color: #6dab69;" class=" glyphicon glyphicon-cloud"></span> HRMS </a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body" style="">
          <?php if (isset($_REQUEST["Color"]) == false) {
            $_COOKIE[("Color")] = "Blue";
          } else {
            $_COOKIE[("Color")] = $_REQUEST["Color"];
          }
          ?>
          <p class="login-box-msg" style="color: #fff !important;">
            <?php
            if (isset($_SESSION["StkTck" . "LoginMsg"]) && strlen(trim($_SESSION["StkTck" . "LoginMsg"])) > 0) {
              echo ($_SESSION["StkTck" . "LoginMsg"]);
            } else {
              $_SESSION["StkTck" . "LoginMsg"] = "Enter Your Login Details";
            }
            ?>
            <? php; //session_destroy(); //Kill all sessions
            ?>
          </p>
          <form action="checkreg.php" method="post" name="Form_Login" id="Form_Login" autocomplete="off">
            <div class="form-group has-feedback">
              <label>Company ID:</label>
              <input type="text" name="CompID" id="CompID" class="form-control" placeholder="Company ID">
              <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <label>Username:</label>
              <input type="text" name="UName" id="UName" class="form-control" placeholder="Username">
              <span class="glyphicon glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <label>Password:</label>
              <input type="password" name="Password" id="Password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
              <!-- /.col -->
              <div class="col-xs-4">
                <button name="Submit" type="submit" class="btn btn-danger btn-block btn-flat" id="sign_in">Sign In</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <br>

          <!-- /.social-auth-links -->
          <a href="../public/acctrecov.php" class="pull-right" style="color: #fff">Having Login Issues?</a><br>
        </div><br>
        <p class="text-center" style="color: #fff">Soft Solutions Limited &copy; 2017 - <?php echo date('Y'); ?></p>
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
    </div>
    <div class="col-sm-6 i-right">
      <div class="i-clip">

      </div>

    </div>
  </div>





  <script src="../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="../assets/plugins/iCheck/icheck.min.js"></script>
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
    $('font').css('color', 'white');


    // Select the form element
    var form = document.getElementById('Form_Login');
    var submitButton = document.getElementById('sign_in');
    submitButton.addEventListener('click', function(event) {
      submitButton.textContent = "Processing...";
      event.preventDefault();
      form.submit();
    });
  </script>

  <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
</body>
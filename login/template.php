<?php

//  <div class="col-md-12">
//             <div class="form-group row ">
//                 <label class="col-sm-1 col-form-label" >Full Name :</label>
//                 <div class="col-sm-11 input-group input-group-sm" >
//                     <input type="text" name="full_name" value="" class="form-control" required >
//                 </div>
//             </div>
//         </div>

function tem()
{
    return [
        'expense report' => '
            <div class="col-md-12">
                <div class="form-group row ">
                    <label class="col-sm-1 col-form-label" >From :</label>
                    <div class="col-sm-5 input-group input-group-sm" >
                        <input type="date" name="from" value="" class="form-control"  >
                    </div>
                    <label class="col-sm-1 col-form-label" >To :</label>
                    <div class="col-sm-5 input-group input-group-sm" >
                        <input type="date" name="to" value="" class="form-control"   >
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-striped mt-2">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">SN</th>
                    <th scope="col">Date</th>
                    <th scope="col" style="width: 60%;" >Description</th>
                    <th scope="col" style="width: 5%;">Expense type</th>
                    <th scope="col"  style="width: 5%;"> Evidence  </th>
                    <th scope="col" style="width: 30%;" >Amount(&#8358;)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <th><input type="date" name="date_1" value="" class="rt_force" required ></th>
                        <td><textarea rows="4" name="desc_1"   class="rt_force" required ></textarea></td>
                        <td><input type="text" name="type_1" value=""  required  class="rt_force" ></td>
                        <td><input type="file" name="file_1"  style="margin-top: 20px;"  id="file_1"  ></td>
                        <td><input type="number"  step="any"  name="amount_1" value=""  required class="rt_force"   id="amt_1"  onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >2</th>
                        <th ><input type="date" name="date_2" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_2" class="rt_force"  ></textarea></td>
                        <td><input type="text" name="type_2" value="" class="rt_force"  ></td>
                        <td><input type="file" name="file_2"  style="margin-top: 20px;"  id="file_2" ></td>
                        <td><input type="number"  step="any"  name="amount_2" value="" class="rt_force" id="amt_2"  onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >3</th>
                        <th scope="row"><input type="date" name="date_3" value="" class="rt_force"  ></th>
                         <td><textarea rows="4" name="desc_3" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_3" value="" class="rt_force"  ></td>
                        <td><input type="file" name="file_3" style="margin-top: 20px;"  id="file_3" ></td>
                        <td><input type="number"  step="any"  name="amount_3" value="" class="rt_force"  id="amt_3" onchange="getTotal()"  ></td>
                    </tr>
                    <tr>
                       <th scope="row" >4</th>
                        <th scope="row"><input type="date" name="date_4" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_4" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_4" value="" class="rt_force" ></td>
                        <td><input type="file" name="file_4"   style="margin-top: 20px;"  id="file_4"></td>
                        <td><input type="number"  step="any"  name="amount_4" value="" class="rt_force"  id="amt_4" onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >5</th>
                        <th scope="row"><input type="date" name="date_5" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_5" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_5" value=""  class="rt_force"  ></td>
                        <td><input type="file" name="file_5"  style="margin-top: 20px;"  id="file_5" ></td>
                        <td><input type="number"  step="any"  name="amount_5" value="" class="rt_force"  id="amt_5" onchange="getTotal()"  ></td>
                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold text-right mt-5" >Total Expenses (₦) </td>
                         <td colspan="" ><input type="text" name="tot_expense" value="" id="tot_expense" class="rt_force lead"  readonly  style="height: 30px;" ></td>

                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold text-right mt-5" >Advance collected  (₦) </td>
                         <td colspan="" ><input type="text" onchange="getAdvance()" name="advance_collected" value="0" id="advance_collected" class="rt_force lead"    style="height: 30px;" ></td>

                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold  text-right"  ><span style="margin-top: 50px;" >Balance (₦)</span>  </td>
                         <td colspan="" ><input type="text" readonly onchange="getBalance()" name="balance" value="0" id="balance" class="rt_force lead"    style="height: 30px;" ></td>

                    </tr>
                </tbody>
            </table>
            </div>
        ',
        'expense report(dollar)' => '
            <div class="col-md-12">
                <div class="form-group row ">
                    <label class="col-sm-1 col-form-label" >From :</label>
                    <div class="col-sm-5 input-group input-group-sm" >
                        <input type="date" name="from" value="" class="form-control"  >
                    </div>
                    <label class="col-sm-1 col-form-label" >To :</label>
                    <div class="col-sm-5 input-group input-group-sm" >
                        <input type="date" name="to" value="" class="form-control"   >
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-striped mt-2">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">SN</th>
                    <th scope="col">Date</th>
                    <th scope="col" style="width: 60%;" >Description</th>
                    <th scope="col" style="width: 5%;">Expense type</th>
                    <th scope="col"  style="width: 5%;"> Evidence  </th>
                    <th scope="col" style="width: 30%;" >Amount(&dollar;)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <th><input type="date" name="date_1" value="" class="rt_force" required ></th>
                        <td><textarea rows="4" name="desc_1"   class="rt_force" required ></textarea></td>
                        <td><input type="text" name="type_1" value=""  required  class="rt_force" ></td>
                        <td><input type="file" name="file_1"  style="margin-top: 20px;"  id="file_1"  ></td>
                        <td><input type="number"  step="any"  name="amount_1" value=""  required class="rt_force"   id="amt_1"  onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >2</th>
                        <th ><input type="date" name="date_2" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_2" class="rt_force"  ></textarea></td>
                        <td><input type="text" name="type_2" value="" class="rt_force"  ></td>
                        <td><input type="file" name="file_2"  style="margin-top: 20px;"  id="file_2" ></td>
                        <td><input type="number"  step="any"  name="amount_2" value="" class="rt_force" id="amt_2"  onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >3</th>
                        <th scope="row"><input type="date" name="date_3" value="" class="rt_force"  ></th>
                         <td><textarea rows="4" name="desc_3" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_3" value="" class="rt_force"  ></td>
                        <td><input type="file" name="file_3" style="margin-top: 20px;"  id="file_3" ></td>
                        <td><input type="number"  step="any"  name="amount_3" value="" class="rt_force"  id="amt_3" onchange="getTotal()"  ></td>
                    </tr>
                    <tr>
                       <th scope="row" >4</th>
                        <th scope="row"><input type="date" name="date_4" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_4" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_4" value="" class="rt_force" ></td>
                        <td><input type="file" name="file_4"   style="margin-top: 20px;"  id="file_4"></td>
                        <td><input type="number"  step="any"  name="amount_4" value="" class="rt_force"  id="amt_4" onchange="getTotal()" ></td>
                    </tr>
                    <tr>
                       <th scope="row" >5</th>
                        <th scope="row"><input type="date" name="date_5" value="" class="rt_force" ></th>
                         <td><textarea rows="4" name="desc_5" class="rt_force" ></textarea></td>
                        <td><input type="text" name="type_5" value=""  class="rt_force"  ></td>
                        <td><input type="file" name="file_5"  style="margin-top: 20px;"  id="file_5" ></td>
                        <td><input type="number"  step="any"  name="amount_5" value="" class="rt_force"  id="amt_5" onchange="getTotal()"  ></td>
                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold text-right mt-5" >Total Expenses (&dollar;) </td>
                         <td colspan="" ><input type="text" name="tot_expense" value="" id="tot_expense" class="rt_force lead"  readonly  style="height: 30px;" ></td>

                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold text-right mt-5" >Advance collected  (&dollar;) </td>
                         <td colspan="" ><input type="text" onchange="getAdvance()" name="advance_collected" value="0" id="advance_collected" class="rt_force lead"    style="height: 30px;" ></td>

                    </tr>
                    <tr >
                       <th scope="row" ></th>
                        <th></th>
                         <td></td>
                         <td></td>

                         <td class="text-bold  text-right"  ><span style="margin-top: 50px;" >Balance (&dollar;)</span>  </td>
                         <td colspan="" ><input type="text" readonly onchange="getBalance()" name="balance" value="0" id="balance" class="rt_force lead"    style="height: 30px;" ></td>

                    </tr>
                </tbody>
            </table>
            </div>
        ',

    ];
}

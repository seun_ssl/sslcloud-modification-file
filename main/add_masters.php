<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = false;
include '../css/myscripts.php';
$GoValidate = true;

if (!isset($EditID)) {
  $EditID = "";
}
if (!isset($Script_Edit)) {
  $Script_Edit = "";
}
if (!isset($HashKey)) {
  $HashKey = "";
}
if (!isset($smsmsg)) {
  $smsmsg = "";
}
if (!isset($SelID)) {
  $SelID = "";
}

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
  unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

  if (isset($_REQUEST["UpdRec"]) && $_REQUEST["UpdRec"] == "ChangeVal" && isset($_REQUEST["G_Members"])) {
    $EditID = $_REQUEST["G_Members"];
    $Script_Edit = "select * from Masters where HashKey='" . ECh($EditID) . "' and ItemName='" . ECh($_REQUEST["GroupName"]) . "'";
    //echo $Script_Edit;
  }

  if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Add Item") {
    if (isset($_REQUEST["GroupName"]) && (trim($_REQUEST["GroupName"]) == "" || $_REQUEST["GroupName"] == "--")) {
      $smsmsg = "<font color=red>You must select a masters group to add item.</font>";
      $GoValidate = false;
    }

    if (isset($_REQUEST["ItemName"]) && trim($_REQUEST["ItemName"]) == "") {
      $smsmsg = "<font color=red>You must enter an item name to add item.</font>";
      $GoValidate = false;
    }

    if (isset($_REQUEST["ParentVal"])) {
      $ParentVal_ = ECh($_REQUEST["ParentVal"]);
    } else {
      $ParentVal_ = '';
    }

    $Script = "Select Val1, ItemCode from Masters where (Val1='" . ECh(trim($_REQUEST["ItemName"])) . "') and ItemName='" . ECh(trim($_REQUEST["GroupName"])) . "' and ParentVal='$ParentVal_'  and Status <> 'D'";
    //        $Script="Select Val1, ItemCode from Masters where (Val1='".ECh(trim($_REQUEST["ItemName"]))."' or ItemCode='".ECh(trim($_REQUEST["ItemCode"]))."') and ItemName='".ECh(trim($_REQUEST["GroupName"]))."' and ParentVal='$ParentVal_'";
    // echo $Script;
    if (trim(ScriptRunner($Script, "Val1")) != "") {
      $smsmsg = "<font color=red>This item already exist within this masters.</font>";
      $GoValidate = false;
    }

    if ($GoValidate == true) {
      $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "Masters" . $_SESSION["StkTck" . "UName"];
      $HashKey = md5($UniqueKey);

      $Script = "INSERT into Masters (ItemName, ItemCode, Val1, ParentVal, [Desc],[Status], AddedBy, AddedDate,HashKey) values ('" . ECh(trim($_REQUEST["GroupName"])) . "','" . ECh(trim($_REQUEST["ItemCode"])) . "','" . ECh(trim($_REQUEST["ItemName"])) . "','" . ECh($ParentVal_) . "','" . ECh(trim($_REQUEST["ItemDesc"])) . "','N','" . $_SESSION["StkTck" . "HKey"] . "',GetDate(),'" . $HashKey . "')";
      //echo $Script;
      $kk = ScriptRunnerUD($Script, "New_Item");
      $smsmsg = "<color=green>New " . trim($_REQUEST["GroupName"]) . " item successfully created.</font>";

      AuditLog("INSERT", "New Masters item " . ECh($_REQUEST["ItemName"]) . " added to group " . ECh($_REQUEST["GroupName"]));

      echo "<script type='text/javascript'>
			{
				var objSelect = parent.document.getElementById('" . $_REQUEST["Elmt"] . "');
				objSelect.options[objSelect.options.length] = new Option('1','1');
			}	  </script>";
    }
    $EditID = "zzz";
  }

  if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Update") {
    if (isset($_REQUEST["GroupName"]) && (trim($_REQUEST["GroupName"]) == "" || $_REQUEST["GroupName"] == "--")) {
      $smsmsg = "<font color=red>You must select a masters group to add item.</font>";
      $GoValidate = false;
    }

    if (isset($_REQUEST["ItemName"]) && trim($_REQUEST["ItemName"]) == "") {
      $smsmsg = "<font color=red>You must enter an item name to add item.</font>";
      $GoValidate = false;
    }

    if (isset($_REQUEST["ParentVal"])) {
      $ParentVal_ = $_REQUEST["ParentVal"];
    } else {
      $ParentVal_ = '';
    }

    if ($GoValidate == true) {
      $Script = "Update Masters set ItemName = '" . ECh($_REQUEST["GroupName"]) . "', ItemCode = '" . ECh($_REQUEST["ItemCode"]) . "', Val1 = '" . ECh($_REQUEST["ItemName"]) . "', ParentVal='" . ECh($ParentVal_) . "', [Desc] = '" . ECh($_REQUEST["ItemDesc"]) . "', [Status]='U',UpdatedBy = '" . ECh($_SESSION["StkTck" . "UName"]) . "', UpdatedDate = GetDate() where HashKey = '" . ECh(trim($_REQUEST["HashKey"])) . "'";
      ScriptRunnerUD($Script, "Update_Masters");

      AuditLog("UPDATE", "Masters item " . ECh($_REQUEST["ItemName"]) . " updated in group " . ECh($_REQUEST["GroupName"]));
      $smsmsg = "Masters item updated successfully.";
    }
  }

  if (isset($_REQUEST["SubmitContact"]) && $_REQUEST["SubmitContact"] == "Delete") {
    $Script = "Update Masters set Status='D', DeletedBy = '" . ECh($_SESSION["StkTck" . "UName"]) . "', DeletedDate = GetDate() where HashKey = '" . ECh(trim($_REQUEST["HashKey"])) . "'";
    ScriptRunnerUD($Script, "Update_Masters");

    AuditLog("DELETE", "Masters item " . ECh($_REQUEST["ItemName"]) . " deleted from group " . ECh($_REQUEST["GroupName"]));
    $smsmsg = "Masters item deleted successfully.";
  }
}
?>
<html>

<head>
  <script type="text/javascript">
    function ChkVal() {
      document.getElementById("UpdRec").value = "ChangeVal";
      //alert (document.getElementById("G_Members").value);
      document.forms[0].submit('Submit');
      return true;
      //alert (document.getElementById("G_Members").selected(document.getElementById("G_Members").selectedIndex);)
    }

    function contcode() {
      if (document.getElementById("C_Code").value == "234") {
        document.getElementById("contcode_display").innerHTML = "&nbsp;&nbsp;Nigeria";
        document.getElementById("contcode_display").style.color = "green";
      } else if (document.getElementById("C_Code").value == "233") { //isNaN
        document.getElementById("contcode_display").innerHTML = "&nbsp;&nbsp;Ghana";
        document.getElementById("contcode_display").style.color = "green";
      } else {
        document.getElementById("contcode_display").innerHTML = "&nbsp;Country not support.";
        document.getElementById("contcode_display").style.color = "red";
      }
    }
  </script>

  <script type="text/javascript">
    function ChkDel(agree) {
      {
        var agree = confirm("Are you sure you wish to continue?");
        if (agree)
          return true;
        else
          return false;
      }
    }
  </script>
</head>
<link href="../css/style_main.css" rel="stylesheet" type="text/css">

<body oncontextmenu="return false;" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
  <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="websms">
      <!--    <tr>
      <th width="1308" height="27" class="tdMenu_HeadBlock" scope="col">
        <?php if (strlen($_REQUEST["GroupName"]) == 0 || ($_REQUEST["GroupName"] == "Clear")) {
          echo 'Select a group from the dropdown to add or edit';
        } else {
          echo $_REQUEST["GroupName"] . ' on Masters';
        } ?></th>
    </tr> -->
      <tr>
        <td height="174" valign="top">
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td width="394" align="center">
                <table width="100%" height="100%" border="0" align="center" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC">
                  <tr>
                    <td colspan="2" height="1" align="center"><?php if (isset($smsmsg)) {
                                                                echo $smsmsg;
                                                              } ?></td>
                  </tr>
                  <tr>
                    <th width="369" height="5" align="right" class="subHeader" scope="col"><select name="GroupName" class="TextBoxText" id="GroupName" onChange="document.forms[0].submit();">
                        <?php
                        if ($_REQUEST["LDb"] != 'Single') {
                          echo '<option value="--" selected="selected">--</option>';

                          $dbOpen2 = ("SELECT Distinct(ItemName) as GroupName FROM Masters where ItemName<>'" . $_REQUEST["GroupName"] . "' ORDER BY ItemName");
                          include '../login/dbOpen2.php';
                          while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                            echo '<option value="' . $row2['GroupName'] . '">' . $row2['GroupName'] . '</option>';
                          }
                          include '../login/dbClose2.php';
                        }
                        if ($_REQUEST["GroupName"] != "--" && $_REQUEST["GroupName"] != "") {
                          echo '<option selected=selected value="' . $_REQUEST["GroupName"] . '">' . $_REQUEST["GroupName"] . '</option>';
                        }
                        ?>
                      </select></th>
                  </tr>
                  <tr>
                    <th height="100%" scope="col">
                      <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="313" height="20" align="right" valign="middle" class="TinyTextColorBg" style="background: none;" scope="col">Item Name:</td>
                          <td width="990" align="left" valign="middle" scope="col"> &nbsp;<?php
                                                                                          if ($EditID != '') {
                                                                                            echo '<input type="text" name="ItemName" class="TextBoxText_Comp" id="ItemName" value="' . ScriptRunner($Script_Edit, "Val1") . '" />';
                                                                                          } elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {
                                                                                            echo '<input type="text" name="ItemName" class="TextBoxText_Comp" id="ItemName" value="" />';
                                                                                          } else {
                                                                                            if (isset($_REQUEST["ItemName"])) {
                                                                                              echo '<input type="text" name="ItemName" class="TextBoxText" id="ItemName" value="' . $_REQUEST["ItemName"] . '"/>';
                                                                                            } else {
                                                                                              echo '<input type="text" name="ItemName" class="TextBoxText" id="ItemName" value=""/>';
                                                                                            }
                                                                                          }

                                                                                          ?></td>
                        </tr>
                        <tr>
                          <td height="20" align="right" valign="middle" class="TinyTextColorBg" style="background: none;" scope="col">Item Code:</td>
                          <td align="left" valign="middle" scope="col"> &nbsp;<?php
                                                                              if ($EditID != '') {
                                                                                echo '<input type="text" name="ItemCode" maxlength="25" class="TextBoxText" id="ItemCode" value="' . ScriptRunner($Script_Edit, "ItemCode") . '" />';
                                                                              } elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {
                                                                                echo '<input type="text" name="ItemCode" maxlength="25" class="TextBoxText" id="ItemCode" value="" />';
                                                                              } else {
                                                                                if (isset($_REQUEST["ItemCode"])) {
                                                                                  echo '<input type="text" name="ItemCode" maxlength="25" class="TextBoxText" id="ItemCode" value="' . $_REQUEST["ItemCode"] . '"/>';
                                                                                } else {
                                                                                  echo '<input type="text" name="ItemCode" maxlength="25" class="TextBoxText" id="ItemCode" value=""/>';
                                                                                }
                                                                              }
                                                                              ?></td>
                        </tr>
                        <?php if (isset($_REQUEST["PRent"])) { ?>
                          <tr>
                            <td height="20" align="right" valign="middle" class="TinyTextColorBg" style="background: none;" scope="col">Item Parent:</td>
                            <td align="left" valign="middle" scope="col">&nbsp;<select name="ParentVal" class="TextBoxText" id="ParentVal">
                                <option value="--" selected="selected">--</option>
                                <?php
                                if ($EditID != '') {
                                  $SelID = ScriptRunner($Script_Edit, "ParentVal");
                                }

                                $dbOpen2 = ("SELECT Val1 FROM Masters where (ItemName='" . $_REQUEST["PRent"] . "') ORDER BY Val1");
                                include '../login/dbOpen2.php';
                                while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                  if ($row2['Val1'] == $SelID) {
                                    echo '<option selected value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                  } else {
                                    echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';
                                  }
                                }
                                include '../login/dbClose2.php';
                                if ($EditID == "") {
                                  if (isset($_REQUEST["ItemPos"]) && $_REQUEST["ItemPos"] != "--") // && isset($_REQUEST["SubmitTrans"])
                                  {
                                    echo '<option selected=selected value="' . $_REQUEST["ItemPos"] . '">' . $_REQUEST["ItemPos"] . '</option>';
                                  }
                                }
                                ?>
                              </select></td>
                          </tr>
                        <?php
                        } ?>
                        <tr>
                          <td width="313" height="20" align="right" valign="middle" style="background: none;" class="TinyTextColorBg" scope="col">Item Description:</td>
                          <td align="left" valign="middle" scope="col">&nbsp;<?php
                                                                              if ($EditID != "" || $EditID > 0) {
                                                                                echo '<textarea type="text" cols="30" rows="2" name="ItemDesc" size="16" class="TextBoxText" id="ItemDesc"/>' . ScriptRunner($Script_Edit, "Desc") . '</textarea>';
                                                                              } elseif (isset($_REQUEST["SubmitContact"]) && ($_REQUEST["SubmitContact"] == 'Clear' || $_REQUEST["SubmitContact"] == 'Delete')) {
                                                                                echo '<textarea type="text" cols="30" rows="2" name="ItemDesc" size="16" maxlength="250" class="TextBoxText" id="ItemDesc" /></textarea>';
                                                                              } else {
                                                                                if (isset($_REQUEST["ItemDesc"])) {
                                                                                  echo '<textarea type="text" cols="30" rows="2" name="ItemDesc" size="16" class="TextBoxText" id="ItemDesc" />' . $_REQUEST["ItemDesc"] . '</textarea>';
                                                                                } else {
                                                                                  echo '<textarea type="text" cols="30" rows="2" name="ItemDesc" size="16" class="TextBoxText" id="ItemDesc" /></textarea>';
                                                                                }
                                                                              }
                                                                              ?></td>
                        </tr>
                        <tr>
                          <td height="47%" align="center" valign="middle" class="TinyText">
                          </td>
                          <td height="47%" align="left" valign="middle" class="TinyText">
                            <?php

                            if (isset($EditID) && strlen($EditID) !== 0) {
                              echo '<input name="SubmitContact" type="submit" class="smallButton" value="Update" />
								<input name="SubmitContact" type="submit" class="smallButton" value="Delete" onClick=return ChkDel("Delete") />
								<input name="SubmitContact" type="submit" class="smallButton" value="Clear" />';
                            } else {
                              echo '<input name="SubmitContact" type="submit" class="smallButton" value="Add Item" />
								<input name="SubmitContact" type="submit" class="smallButton" value="Clear" />';
                            }

                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td height="48%" align="center" valign="middle" class="TinyText"></td>
                          <td height="48%" align="left" valign="middle" class="TinyText">&nbsp;</td>
                        </tr>
                        <tr>
                          <td height="95%" align="center" valign="middle" class="TinyText">&nbsp;</td>
                          <td height="95%" align="left" valign="middle" class="TinyText"><select name="G_Members" id="G_Members" size="7" class="TextBoxText_Long" onChange="ChkVal();">
                              <?php
                              $dbOpen2 = ("SELECT Val1, ParentVal, HashKey from Masters where ItemName='" . $_REQUEST["GroupName"] . "' and Status<>'D' Order by Val1 asc");
                              include '../login/dbOpen2.php';
                              while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
                                if (isset($_REQUEST["PRent"])) {
                                  echo '<option value="' . $row2['HashKey'] . '">' . $row2['Val1'] . ' - [' . $row2['ParentVal'] . ']</option>';
                                } else {
                                  echo '<option value="' . $row2['HashKey'] . '">' . $row2['Val1'] . '</option>';
                                }
                              }
                              ?>
                            </select></td>
                        </tr>
                        <tr>
                          <td colspan="2" height="95%" align="center" valign="middle" class="TinyText">
                            <div align="center">
                              <br>
                            </div>
                            <div align="center"><br>
                              <?php if ($smsmsg == '') {
                                echo '<br>';
                              }
                              ?>
                            </div>
                            <div align="center" class="TinyText">
                              <?php
                              if ($_REQUEST["GroupName"] == "" || $_REQUEST["GroupName"] == "--") {
                                echo ("List of all values.");
                              } else {
                                echo ("List of values in <strong>" . $_REQUEST["GroupName"] . "</strong> masters.");
                              }

                              if ($EditID != '') { ?>
                                <input type="hidden" name="HashKey" id="HashKey" value="<?php echo $EditID; ?>">
                              <?php } ?>
                              <?php echo '<input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">'; ?>
                              <input type="hidden" name="LDb" id="LDb" value="<?php if (isset($_REQUEST["LDb"])) {
                                                                                echo $_REQUEST["LDb"];
                                                                              } ?>">
                              <input type="hidden" name="Elmt__" id="Elmt__" value="<?php if (isset($_REQUEST["Elmt"])) {
                                                                                      echo $_REQUEST["Elmt"];
                                                                                    } ?>">
                              <input type="hidden" name="UpdRec" id="UpdRec" value="">
                              <input type="hidden" name="GroupName__" id="GroupName__" value="<?php echo $_REQUEST["GroupName"]; ?>">

                            </div>
                          </td>
                        </tr>
                      </table>
                    </th>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </form>
  </table>
</body>
<script>
  setTimeout(function() {
    window.parent.scrollTo(0, 0);
  }, 150);
</script>

</html>
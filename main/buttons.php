<?php
//For buttons with any other caption other than Edit, Update, Delete, Auth or UnAuth
if (!isset($YesNo)) {$YesNo = "false";}
if (!isset($but_Type)) {$but_Type = "";}
if (!isset($StatusCheck)) {$StatusCheck = "";}

if (isset($but_Type) && $but_Type == "CustomNoSelect") {
    if (ValidateURths($but_HasRth) == true) {?>
		<input name="SubmitTrans" id="SubmitTrans" class="btn btn-danger btn-sm" value="<?php echo $but_Caption; ?>" <?php if ($YesNo == true) {echo 'type="button" ';?>onClick="CollateChks(); YesNo('<?php echo $but_Caption; ?> Record', '<?php echo $but_Caption; ?>')" <?php } else {echo ' type="submit" ';}?>/>
<?php } else {?>
		<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Caption; ?>" disabled="disabled" />
<?php }
} elseif ($but_Type == "Custom") {
    if (ValidateURths($but_HasRth) == true) {?>
		<input name="SubmitTrans" id="SubmitTrans" class="btn btn-danger btn-sm" value="<?php echo $but_Caption; ?> Selected" <?php if ($YesNo == true) {echo 'type="button" ';?>onClick="CollateChks(); YesNo('<?php echo $but_Caption; ?> Record', '<?php echo $but_Caption; ?>')" <?php } else {echo ' type="submit" ';}?>/>
<?php } else {?>
		<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Caption; ?> Selected" disabled="disabled" />
<?php }
}

//Not equal to custom button
else {
    //This page sets the button type, then sets if the button is enabled or disabled and the style for that button
    //$but_Type = 'Delete'; $but_HasRth=true;

    if (ValidateURths($but_HasRth) == true && $StatusCheck != "('A')" && $Del > 0) {

        if ($but_Type == "Reactivate with new ID") {?>

	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Type; ?> Selected" onClick="deleteFun();return;
 " />
	<?php }
	
	elseif ($but_Type == "Update with new ID") {?>
		<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Type; ?> Selected" onClick="deleteFun2();return;
 " />


	<?php
}
	elseif ($but_Type != "Authorize") {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Type; ?> Selected" onClick="CollateChks(); YesNo('<?php echo $but_Type; ?> Record', '<?php echo $but_Type; ?>')" />


	<?php
}



elseif ($but_Type == "Authorize") {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="<?php echo $but_Type; ?> Selected" onClick="CollateChks(); YesNo('<?php echo $but_Type; ?> Record', '<?php echo $but_Type; ?>')" />


	<?php
}
    }

    //**********************************************************************************************************//
    //*********************************** Authorized / Unauthorized Selected ***********************************//
    //**********************************************************************************************************//

    elseif (ValidateURths($but_HasRth) == true && $StatusCheck != "('A')" && $but_Type == "Authorize" && $Del > 0) {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Authorize Selected" onClick="CollateChks(); YesNo('Authorize Record', 'Authorize')" />
	<?php } elseif (ValidateURths($but_HasRth) == true && $StatusCheck == "('A')" && $but_Type == "Authorize" && $Del > 0) {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Unauthorize Selected" onClick="CollateChks(); YesNo('Unauthorize Record', 'Unauthorize')" />
	<?php } elseif (($StatusCheck != "('A')" || $StatusCheck != "('')") && $but_Type == "Authorize") {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Authorize Selected" disabled="disabled" />
	<?php } elseif (($StatusCheck == "('A')" || $StatusCheck != "('')") && $but_Type == "Authorize") {?>
	<input name="SubmitTrans" id="SubmitTrans" type="button" class="btn btn-danger btn-sm" value="Unauthorize Selected" disabled="disabled" />
	<?php }
}?>
<?php session_start();
$_SESSION["Rst"] = "Rem";
include '../login/scriptrunner.php';

//=========================== Check Login Time. FORCE LOGOUT if GpFLO was 'on' as at user login time ===========================
if (isset($_SESSION["StkTck" . "GpFLO"]) && $_SESSION["StkTck" . "GpFLO"] == 'on') {
	$Script_Tm = "SELECT count(distinct(convert(varchar(5),GETDATE(),108))) Ct from users where convert(varchar(5),GETDATE(),108) between '" . $_SESSION["StkTck" . "LogInT"] . "' and '" . $_SESSION["StkTck" . "LogOutT"] . "'";
	if (ScriptRunner($Script_Tm, "Ct") == 0) {
		AuditLog("A-LOGOUT", "User Logged Out. Login not permitted between " . $_SESSION["StkTck" . "LogInT"] . " and " . $_SESSION["StkTck" . "LogOutT"] . " for account [" . $_SESSION["StkTck" . "UName"] . "]");
		$_SESSION["StkTck" . "LoginMsg"] = "<font color='red'>Your are not permitted to login between " . $_SESSION["StkTck" . "LogInT"] . " and " . $_SESSION["StkTck" . "LogOutT"] . "</font>";

		// Destroys users session
		$_SESSION["StkTck" . "Login_Status"] == "false";
		$UserEmail = "";
		unset($_SESSION["StkTck" . "CustID"]);
		unset($_SESSION["StkTck" . "UName"]);
		unset($_SESSION["StkTck" . "AcctActive"]);
		unset($_SESSION["StkTck" . "Email"]);
		unset($_SESSION["StkTck" . "FName"]);
		unset($_SESSION["StkTck" . "ONames"]);
		unset($_SESSION["StkTck" . "Mobile"]);
		unset($_SESSION["StkTck" . "Title"]);
		unset($_SESSION["StkTck" . "Sex"]);
		unset($_SESSION["StkTck" . "mylogin_Status"]);
		unset($_SESSION["StkTck" . "YesNo_Page"]);
		unset($_SESSION["StkTck" . "LoggedIn"]);
		unset($_SESSION["StkTck" . "LogOutT"]);
		unset($_SESSION["StkTck" . "LogInT"]);
		unset($_SESSION["StkTck" . "MailHeader"]);
		//session_destroy( );

		$_SESSION["StkTck" . "LoginMsg"] = "Your account is not permitted to login between " . $_SESSION["StkTck" . "LogInT"] . " and " . $_SESSION["StkTck" . "LogOutT"];

		echo ("<script type='text/javascript'>{parent.document.location='../login/logout.php';}</script>");
	}
}


//=========================== NEW: check is status is 1, preventing multiple login OLD: Check IP Address ===============================================

//if emp didnt logout after 30 min, the system assumed that he closes without shutdown, 
//there for allow the next logon id of the user session to replace the previou 
//ELSE is the emp sign out the session wud be clear.
$Script = "Update [Users] set [Session_ID]='" . session_id() . "' WHERE LogDate < DATEADD(minute, -30, GETDATE()) AND LogName='" . $_SESSION["StkTck" . "UName"] . "'";
ScriptRunnerUD($Script, "Inst");


$Script = "Select LogStatus, Session_ID from Users where LogName='" . $_SESSION["StkTck" . "UName"] . "'";

if ((ScriptRunner($Script, "Session_ID") != session_id())) {

	$_SESSION["StkTck" . "Login_Status"] == "false";
	$UserEmail = "";
	unset($_SESSION["StkTck" . "CustID"]);
	unset($_SESSION["StkTck" . "UName"]);
	unset($_SESSION["StkTck" . "AcctActive"]);
	unset($_SESSION["StkTck" . "Email"]);
	unset($_SESSION["StkTck" . "FName"]);
	unset($_SESSION["StkTck" . "ONames"]);
	unset($_SESSION["StkTck" . "Mobile"]);
	unset($_SESSION["StkTck" . "Title"]);
	unset($_SESSION["StkTck" . "Sex"]);
	unset($_SESSION["StkTck" . "mylogin_Status"]);
	unset($_SESSION["StkTck" . "YesNo_Page"]);
	unset($_SESSION["StkTck" . "LoggedIn"]);
	unset($_SESSION["StkTck" . "LogOutT"]);
	unset($_SESSION["StkTck" . "LogInT"]);

	$_SESSION["StkTck" . "LoginMsg"] = "User ID logged in on a different device";
	/*echo ("<script type='text/javascript'>{parent.document.location='../login/logout.php';}</script>");*/
}



//=========================================== Check IP Address ===============================================

$_SESSION["Rst"] = "";




//Get Header and Footer
if (!isset($_SESSION["StkTck" . "MailHeader"]) || (isset($_SESSION["StkTck" . "MailHeader"]) && $_SESSION["StkTck" . "MailHeader"] == '')) //Means no mail has been sent. So get Mail Header in SESSION
{
	$Script = "Select MMsg from MailTemp where HashKey='HEADER'";
	$MailHeader = ScriptRunner($Script, "MMsg") . "<font color='#333333' face='Verdana, Arial, Helvetica, sans-serif' size='2'>";

	//Format MailHeader to have full details
	$Script = "Select SetValue, SetValue2, SetValue3 from settings where Setting='CompLogo'";
	$CompLogo = '<img src="' . ScriptRunner($Script, "SetValue") . '" width="' . ScriptRunner($Script, "SetValue2") . '" height="' . ScriptRunner($Script, "SetValue3") . '">';

	$Script = "Select SetValue from settings where Setting='CompName'";
	$CompName = ScriptRunner($Script, "SetValue");

	$Script = "Select SetValue from settings where Setting='CompAddress'";
	$CompAddress = ScriptRunner($Script, "SetValue");

	$Script = "Select SetValue from settings where Setting='CompTel'";
	$CompTel = ScriptRunner($Script, "SetValue");

	$Script = "Select SetValue from settings where Setting='CompEmail'";
	$CompEmail = ScriptRunner($Script, "SetValue");

	$Script = "Select SetValue from settings where Setting='CompTel'";
	$CompTel = ScriptRunner($Script, "SetValue");

	$Script = "Select SetValue from settings where Setting='CompRC'";
	$CompRC = ScriptRunner($Script, "SetValue");


	//mAIL sENDING mACHINE pARAMETERS
	//	$_SESSION["StkTck"."MailSendType"]=ScriptRunner("Select SetValue from settings where Setting='SMTP'","SetValue");
	//GET MAIL SENDING CREDENTIALS FROM DATABASE
	$key = 'Onyema.Amaobi.Jm';
	$_SESSION["StkTck" . "SMTP"] = ScriptRunner("Select SetValue from settings where Setting='SMTP'", "SetValue");
	// $_SESSION["StkTck" . "SMTP"] = "smtp.zeptomail.com";
	$_SESSION["StkTck" . "SMTPAuth"] = ScriptRunner("Select SetValue from settings where Setting='SMTPAuth'", "SetValue");
	// $_SESSION["StkTck" . "SMTPAuth"] = true;

	$_SESSION["StkTck" . "SMTPFrom"] = ScriptRunner("Select SetValue from settings where Setting='SMTPFrom'", "SetValue");
	// $_SESSION["StkTck" . "SMTPFrom"] = "donotreply@ss-limited.com";
	// $_SESSION["StkTck" . "SMTPFrom"] = "noreply@ss-limited.com";
	$_SESSION["StkTck" . "SMTPPort"] = ScriptRunner("Select SetValue from settings where Setting='SMTPort'", "SetValue");
	// $_SESSION["StkTck" . "SMTPPort"] = '465';
	$_SESSION["StkTck" . "SMTPEmail"] = ScriptRunner("Select SetValue from settings where Setting='SMTPEmail'", "SetValue");
	// $_SESSION["StkTck" . "SMTPEmail"] = "donotreply@ss-limited.com";
	// $_SESSION["StkTck" . "SMTPEmail"] = "donotreply@ss-limited.com";
	$_SESSION["StkTck" . "SMTPwd"] = (new Encryption())->safe_b64decode(ScriptRunner("Select SetValue18 from settings where Setting='SMTPwd'", "SetValue18"));
	// $_SESSION["StkTck" . "SMTPwd"] = "wSsVR6118xH3CvgolDP7LuZukF4AUgv3HEl80VWi7XL1TfjDoMdvk0fKUQOuGKcaRGZoEzER8LIryRwEgDVaidklz1wACyiF9mqRe1U4J3x17qnvhDzCV21ZkhSJKIsPxgtrnWJkFYU=";
	$_SESSION["StkTck" . "SMTPUser"] = "emailapikey";





	//Make changes required with values retrived and passed
	$MailHeader = str_replace('#CompLogo#', $CompLogo, $MailHeader);
	$MailHeader = str_replace('#CompName#', $CompName, $MailHeader);
	//$MailHeader=str_replace('#BranchName#','',$MailHeader);
	$MailHeader = str_replace('#CompAddress#', $CompAddress, $MailHeader);
	$MailHeader = str_replace('#CompTel#', $CompTel, $MailHeader);
	$MailHeader = str_replace('#CompEmail#', $CompEmail, $MailHeader);
	$MailHeader = str_replace('#CompRC#', $CompRC, $MailHeader);

	$_SESSION["StkTck" . "MailHeader"] = $MailHeader; //Set In SeSSION HEADER
	$Script = "Select MMsg from MailTemp where HashKey='FOOTER'";
	$_SESSION["StkTck" . "MailFooter"] = "</font>" . ScriptRunner($Script, "MMsg");
}



//Get the PHP Mailer
if (isset($_SESSION["StkTck" . "SMTP"]) && $_SESSION["StkTck" . "SMTP"] == "smtp.zeptomail.com") {

	// var_dump("wSsVR6118xH3CvgolDP7LuZukF4AUgv3HEl80VWi7XL1TfjDoMdvk0fKUQOuGKcaRGZoEzER8LIryRwEgDVaidklz1wACyiF9mqRe1U4J3x17qnvhDzCV21ZkhSJKIsPxgtrnWJkFYU=");
	require("../phpmailer/class.phpmailer_G.php");
} else {
	// var_dump("her1");
	require("../phpmailer/class.phpmailer.php");
}
include '../css/myscripts.php';



//Checks that any mail that was not fired within the last 3 minutes should now be reset and resent
$Script = "Update MailQueue set Status='U' where Status='P' and DATEDIFF(MINUTE,AddedDate,GETDATE()) > 5 and Subject<>'' and Send_to<>''";
ScriptRunnerUD($Script, "Ct");

//Check if there are pending mails
$Script = "Select Count(*) as Ct from MailQueue where (Status='N' or Status='U') and TrialCnt <= 5";
$kk = ScriptRunner($Script, "Ct");

if ($kk > 0) {
	$kk = 0;
	$dbOpen2 = ("Select * from MailQueue where (Status='N' or Status='U') and TrialCnt <= 5 and (SendTime<=GetDate() or SendTime is NUll)");
	include '../login/dbOpen2.php';
	while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
		$kk = $kk + 1;

		if ($row2['ID'] > 0) {
			$Script = "Update MailQueue set [Status]='P' where ID=" . $row2['ID'];
			ScriptRunnerUD($Script, "Ct");
		}

		$Script = "Select (TE.FName+' '+TE.SName) as Nm from EmpTbl TE where TE.Email='" . $row2['Send_to'] . "'";
		$Nm = ScriptRunner($Script, "Nm");

		$subject = $row2['Subject'];
		$message = str_replace("#Name#", $Nm, $row2['Body']);
		$to = $row2['Send_to'];
		$attch = $row2['Attachment'];


		$BranchName = "";
		$Script = "Select OName from BrhMasters where HashKey=(Select top 1 BranchID from EmpTbl where Email='" . $to . "' and BranchID is not NULL) and Status<>'D'";
		$BranchName = ScriptRunner($Script, "OName");


		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPSecure = 'ssl';
		$mail->Host = $_SESSION["StkTck" . "SMTP"]; //"10.10.1.24"; //"200.200.200.24"; //"smtp.gmail.com";
		$mail->From = $_SESSION["StkTck" . "SMTPEmail"];
		$mail->FromName  = $_SESSION["StkTck" . "SMTPFrom"]; //"SSLCloud Services";

		$mail->AddAddress($to);
		if ($attch) {

			$mail->addAttachment("$attch");
		}

		$mail->SMTPAuth = $_SESSION["StkTck" . "SMTPAuth"]; //"false"; // Unset by Joseph - 3rd July 2014
		// $mail->Username = $_SESSION["StkTck" . "SMTPEmail"];
		$mail->Username = $_SESSION["StkTck" . "SMTPUser"];
		$mail->Password = $_SESSION["StkTck" . "SMTPwd"]; //"Partner@123"; // Unset by Joseph - 3rd July 2014
		// $mail->Password = "wSsVR6118xH3CvgolDP7LuZukF4AUgv3HEl80VWi7XL1TfjDoMdvk0fKUQOuGKcaRGZoEzER8LIryRwEgDVaidklz1wACyiF9mqRe1U4J3x17qnvhDzCV21ZkhSJKIsPxgtrnWJkFYU=";

		$mail->Port = $_SESSION["StkTck" . "SMTPPort"]; //"587"; //"465"; //"587"; // Unset by Joseph - 3rd July 2014 Google 465
		$mail->IsHTML(true);

		$mail->Subject = $subject;
		//$mail->Body = $_SESSION["StkTck"."MailHeader"].$message.$_SESSION["StkTck"."MailFooter"];


		$mail->Body = str_replace('#BranchName#', $BranchName, $_SESSION["StkTck" . "MailHeader"]) . $message . $_SESSION["StkTck" . "MailFooter"];
		$mail->WordWrap = 50;

		// $mail = new PHPMailer();
		// $mail->Encoding = "base64";
		// $mail->SMTPAuth = true;
		// $mail->Host = "smtp.zeptomail.com";
		// $mail->Port = 465;
		// $mail->Username = "emailapikey";
		// $mail->Password = 'wSsVR6118xH3CvgolDP7LuZukF4AUgv3HEl80VWi7XL1TfjDoMdvk0fKUQOuGKcaRGZoEzER8LIryRwEgDVaidklz1wACyiF9mqRe1U4J3x17qnvhDzCV21ZkhSJKIsPxgtrnWJkFYU=';
		// $mail->SMTPSecure = 'ssl';
		// $mail->isSMTP();
		// $mail->IsHTML(true);
		// $mail->CharSet = "UTF-8";
		// $mail->From = "donotreply@ss-limited.com";
		// $mail->AddAddress($to);
		// $mail->Body = "Test email sent successfully.";
		// $mail->Subject = "Test Email";
		$mail->SMTPDebug = 2;

		if (!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
			$Script = "Update MailQueue set [Status]='U', Updatedby='MailBot', UpdatedDate=GetDate(), TrialCnt=(TrialCnt+1) where ID=" . $row2['ID'];
			ScriptRunnerUD($Script, "Ct");
		} else {
			$Script = "Update MailQueue set [Status]='S', Authby='MailBot', AuthDate=GetDate(), TrialCnt=(TrialCnt+1) where ID=" . $row2['ID'];
			ScriptRunnerUD($Script, "Ct");
		}
	}
	//	$query2 = "FLUSH PRIVILEGES"; mssql_close($dbhandle2);
} else {
	echo "No Mail";
}

$dbOpen4 = ("SELECT * from PreEva where Status ='HRA' ");
include '../login/dbOpenmy.php';
while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
	$end = DATE_FORMAT($row['EndDate'], 'Y/m/d H:i:s');
	$end_readable = DATE_FORMAT($row['EndDate'], 'l jS F, Y');
	$post = $row['PostEvalTime'];
	date_default_timezone_set("Africa/Lagos");
	$today = new DateTime();
	$date = $today->format('Y/m/d H:i:s');
	$id = $row['Id'];
	$actionPlan = $row['ActionPlan'];
	$actionPlan = json_decode($actionPlan, true);
	$leng = count($actionPlan);

	$post_date = date("Y/m/d H:i:s", strtotime("+$post seconds", strtotime("$end")));
	$act_plan_rem = date("Y/m/d H:i:s", strtotime("-1 days", strtotime("$end")));
	$post_rem = date("Y/m/d H:i:s", strtotime("-86400 seconds", strtotime("$post_date")));
	$post_rem_mod = date("l jS F, Y", strtotime("+$post seconds", strtotime("$end")));
	//ACTION PLAN REMINDER
	if ($date >= $act_plan_rem) {
		if ($row['RemActPlan'] == NULL) {
			$Script2 = "SELECT * from  EmpTbl Et where EmpID = '" . $row['EmpId'] . "'";
			$empdetails = ScriptRunnercous($Script2);
			$employeeEmail = $empdetails['Email'];
			$name = $empdetails['SName'] . ' ' . $empdetails['FName'] . ' ' . $empdetails['ONames'];
			$Script_Mail = "Select * from MailTemp where HashKey='11a53c7df49e1fe912d4865c25c0eed6'"; //ACTIONPLANREMINDER
			$msg = ScriptRunner($Script_Mail, "MMsg");
			$Subj = ScriptRunner($Script_Mail, "MSub");
			$msg = str_replace('#ChangeValue#', $name, $msg);
			$msg = str_replace('#date#', $end_readable, $msg);
			$To = $employeeEmail;
			QueueMail($To, $Subj, $msg, '', '');

			$Script = "UPDATE PreEva SET RemActPlan = 'S' WHERE Id = '" . $id . "'";
			ScriptRunnerUD($Script, "Inst");
		}
	}

	//ACTION PLAN CREATE
	if ($date >= $end) {
		if ($row['ActionStatus'] == NULL) {
			$Script = "UPDATE PreEva SET ActionStatus = 'E' WHERE Id = '" . $id . "'";
			ScriptRunnerUD($Script, "Inst");
		}
	}
	//POST
	if ($date >= $post_date) {
		if ($row['PostStatus'] == NULL) {
			$Script = "UPDATE PreEva SET PostStatus='M' WHERE Id = '" . $id . "'";
			ScriptRunnerUD($Script, "Inst");
		}
	}
	//POST REMINDER
	if ($date >= $post_rem) {
		if ($row['RemPost'] == NULL) {
			$Script3 = "SELECT * from  EmpTbl Et where EmpID = '" . $row['Manager'] . "'";
			$manager = ScriptRunnercous($Script3);
			$managerEmail = $manager['Email'];
			$managerName = $manager['SName'] . ' ' . $manager['FName'];
			$Script2 = "SELECT * from  EmpTbl Et where EmpID = '" . $row['EmpId'] . "'";
			$empdetails = ScriptRunnercous($Script2);
			$name = $empdetails['SName'] . ' ' . $empdetails['FName'] . ' ' . $empdetails['ONames'];
			$Script_Mail = "Select * from MailTemp where HashKey='e9bce402110bd1961ae7d91b7bc9c2b4'"; //ACTIONPLANREMINDER
			$msg = ScriptRunner($Script_Mail, "MMsg");
			$Subj = ScriptRunner($Script_Mail, "MSub");
			$msg = str_replace('#ChangeValue#', $managerName, $msg);
			$msg = str_replace('#employee#', $name, $msg);
			$msg = str_replace('#date#', $post_rem_mod, $msg);
			$To = $managerEmail;
			QueueMail($To, $Subj, $msg, '', '');

			$Script = "UPDATE PreEva SET RemPost = 'S' WHERE Id = '" . $id . "'";
			ScriptRunnerUD($Script, "Inst");
		}
	}
	// if ($row['ActionStatus'] == 'HR' || $row['ActionStatus'] == 'H') {
	# code...
	for ($i = 1; $i <= $leng; $i++) {
		$ind_act = DateTime::createFromFormat('Y-m-d', $actionPlan[$i - 1]['end_date']);
		$ind_act_mod = $ind_act->format('Y/m/d');
		$measure_date = $ind_act->format('l jS F, Y');
		$ind_act_rem = date("Y/m/d H:i:s", strtotime("-1 days", strtotime("$ind_act_mod")));
		// var_dump($ind_act_mod);
		// var_dump($ind_act_rem);
		// var_dump($date);
		// die();
		if ($date >= $ind_act_rem) {
			if ($actionPlan[$i - 1]['reminder'] == 'False') {
				$Script2 = "SELECT * from  EmpTbl Et where EmpID = '" . $row['EmpId'] . "'";
				$empdetails = ScriptRunnercous($Script2);
				$employeeEmail = $empdetails['Email'];
				$name = $empdetails['SName'] . ' ' . $empdetails['FName'] . ' ' . $empdetails['ONames'];
				$Script_Mail = "Select * from MailTemp where HashKey='b39979b986158662f36dcbf545148298'"; //MEASUREMENTREMINDER
				$msg = ScriptRunner($Script_Mail, "MMsg");
				$Subj = ScriptRunner($Script_Mail, "MSub");
				$msg = str_replace('#ChangeValue#', $name, $msg);
				$msg = str_replace('#date#', $measure_date, $msg);
				$To = $employeeEmail;
				$actionPlan[$i - 1]['reminder'] = 'True';
				QueueMail($To, $Subj, $msg, '', '');
			}
		}
		if ($date >= $ind_act_mod) {
			if ($row['ActPlanStatus'] == NULL) {
				$Script = "UPDATE PreEva SET ActPlanStatus = 'E' WHERE Id = '" . $id . "'";
				ScriptRunnerUD($Script, "Inst");
			}
			if ($row['ActPlanStatus'] == 'M' && $actionPlan[$i - 1]['return_back'] == 'False') {
				$Script = "UPDATE PreEva SET ActPlanStatus = 'E' WHERE Id = '" . $id . "'";
				ScriptRunnerUD($Script, "Inst");
				$actionPlan[$i - 1]['return_back'] = 'True';
			}
		}
	}
	$arry = json_encode($actionPlan);
	$Script = "UPDATE PreEva SET ActionPlan = '" . $arry . "' WHERE Id = '" . $id . "'";
	ScriptRunnerUD($Script, "Inst");
}
// }
include '../login/dbClosemy.php';



//echo $_SESSION["StkTck"."SMTP"]."<br>".
//	$_SESSION["StkTck"."SMTPAuth"]."<br>".
//	$_SESSION["StkTck"."SMTPFrom"]."<br>".
//	$_SESSION["StkTck"."SMTPPort"]."<br>".
//	$_SESSION["StkTck"."SMTPEmail"]."<br>".
//	$_SESSION["StkTck"."SMTPwd"]."<br>";

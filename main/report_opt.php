<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>

<script>
$(function() {
	$("#ClearDate").click(function() {
		document.getElementById("S_RptDate").value='';
		document.getElementById("E_RptDate").value='';
	});
});
</script>

	<?php
		if ($Load_Date==true)
	{
	?>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group row ">
                        <label class="col-sm-3 col-form-label" style="padding-right: 0px;">From
                            :</label>
                        <div class="col-sm-9  input-group-sm" style=" padding-right: 5px">
                            <?php
                        		if (isset($_REQUEST["S_RptDate"]))
                        		{echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="'.($_REQUEST["S_RptDate"]).'" readonly/>';}
                        		else
                        		{echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';}
                			?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row ">
                        <label class="col-sm-2 col-form-label" style="padding-right: 0px;">To
                            :</label>
                        <div class="col-sm-10 input-group input-group-sm" style=" padding-right: 5px">
                            <?php
                        		if (isset($_REQUEST["E_RptDate"]))
                        		{echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="'.$_REQUEST["E_RptDate"].'" readonly />';}
                        		else
                        		{echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';}
                            ?>
                            <span class="input-group-btn ">
                                <button type="button" class="btn btn-default btn-sm" Title="Click to clear date fields" name="ClearDate" id="ClearDate" style="line-height: 17px">
                                    <i class="fa fa-info"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
}?>

<?php
if ($Load_Search==true && $Load_Auth==true)
{
?>
	<div class="col-md-6">
	    <div class="row">
	        <div class="col-md-7">
	            <div class="form-group row ">
	                <label class="col-sm-2 col-form-label" style="padding-right: 0px;">Search:</label>
	                <div class="col-sm-10  input-group-sm" style=" padding-right: 5px">
				        <?php 
					        if (isset($_REQUEST["Search"]))
							{
						        echo '<input name="Search" type="text" class="form-control" id="Search" placeholder="Search" value="'.($_REQUEST["Search"]).'" maxlength="60" />';
							}
							else
							{
					    	    echo '<input name="Search" type="text" class="form-control" id="Search" placeholder="Search" value="" maxlength="60" />';
							}
						?>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-3">
	            <div class="form-group row ">
	                <div class="col-sm-12 input-group input-group-sm" style=" padding-right: 0px; padding-left: 0px;">
						<select name="SearchType" class="form-control" id="SearchType">
							<option value="--" selected="selected">--</option>
							<?php
								if (!isset($_REQUEST["SearchType"]))
								{
									echo '<option value="Authorized">Authorized</option><option value="Unauthorized">Unauthorized</option>';
									$StatusCheck="('')";
								}
								else
								{
									if ($_REQUEST["SearchType"]== "--" || $_REQUEST["SearchType"]== "")
									{
										echo '<option value="Authorized">Authorized</option><option value="Unauthorized">Unauthorized</option>';
										$StatusCheck="('')";
									}
									elseif ($_REQUEST["SearchType"] == 'Authorized')
									{
										echo '<option value="Authorized" selected="selected">Authorized</option><option value="Unauthorized">Unauthorized</option>';
										$StatusCheck="('A')";
									}
									elseif ($_REQUEST["SearchType"] == 'Unauthorized')
									{
										echo '<option value="Authorized">Authorized</option><option selected="selected" value="Unauthorized">Unauthorized</option>';
										$StatusCheck="('N','U','PC','PA')";
									}
								}
								//Sort Searching
								if (!isset($_REQUEST["Search"]))
								{$SearchVal="";}
								else
								{$SearchVal=$_REQUEST["Search"];}
							?>
						</select>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-2">
	        	<input name="DelTrans" type="submit" class="btn btn-danger btn-sm" value="Load" /></td>
	        </div>
	    </div>
	</div>
<?php } ?>




<?php
if ($Load_Search==true && $Load_Auth!=true)
{
?>
	<div class="col-md-6">
        <div class="form-group row ">
            <label class="col-sm-4 col-form-label" style="padding-right: 0px;"></label>
            <div class="col-sm-6  input-group-sm" style=" padding-right: 35px">
		        <?php 
			        if (isset($_REQUEST["Search"]))
					{
				        echo '<input name="Search" type="text" class="form-control" id="Search" placeholder="Search" value="'.($_REQUEST["Search"]).'" maxlength="60" />';
					}
					else
					{
			    	    echo '<input name="Search" type="text" class="form-control" id="Search" placeholder="Search" value="" maxlength="60" />';
					}
					//Sort Searching
					if (!isset($_REQUEST["Search"]))
					{$SearchVal="";}
					else
					{$SearchVal=$_REQUEST["Search"];}
				?>
            </div>
       		<input name="DelTrans" type="submit" class="btn btn-danger btn-sm pull-right" value="Load" />
        </div>
    </div>
	         <!-- <div class="col-md-6">
	        	</td>
	        </div> -->
<?php } ?>
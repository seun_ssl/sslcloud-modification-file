<?php
session_start();
include '../login/scriptrunner.php';

if (isset($_POST)) {
    $all = json_decode($_POST['all'], true);
    $filter = json_decode($_POST['filter'], true);
    $email = sanitize($all['email']);
    $first_name = sanitize($all['first_name']);
    $middle_name = sanitize($all['middle_name']);
    $last_name = sanitize($all['last_name']);
    $jobId = sanitize($all['job_id']);

    $query = "SELECT * FROM [recjobs] WHERE [email] = '$email' AND [job_id] ='$jobId'"; // this query needs to be adjusted to includes job_id
    $check = ScriptRunnercous($query);

    if (!$check) {

        $cust_id = $_POST['cust_id'];

        $path = "uploads_files/cv/$cust_id";

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $temp = explode(".", $_FILES["cv2"]["name"]);

        // $extension = end($temp);

        $filename = $_FILES["cv2"]["tmp_name"];

        $name = md5(uniqid(rand(), true)) . ".pdf";
        if (move_uploaded_file($filename, "$path/$name")) {

            $cv = "$path/$name";

            $rest = [];
            foreach ($all as $key => $value) {
                if ($all['first_name'] === $all[$key] || $all['middle_name'] === $all[$key] || $all['last_name'] === $all[$key] || $all['email'] === $all[$key]) {

                    continue;

                }

                if (is_array($all[$key])) {
                    $rest[$key] = array_map('sanitize', $all[$key]);
                } else {
                    $rest[$key] = sanitize($all[$key]);
                }
            }
            if (array_key_exists('cv', $rest)) {
                unset($rest['cv']);
            }

            $rest = json_encode($rest);

            if (!empty($filter) && in_array(false, $filter)) {
                // do nothing
                // candidate not qualify

            } else {

                $Script = "INSERT INTO [recjobs] ([job_id],[first_name],[middle_name],[last_name],[email],[other_info],[cv],[applied_on]) VALUES ('$jobId','$first_name','$middle_name','$last_name','$email','$rest','$cv',GETDATE())";

                $qrr = ScriptRunnerUD($Script, "Inst");

            }

            $name = "$last_name $first_name $middle_name";
            $JobTil = $_POST['jobTil'];

            AutoReplyApp('AUTO', 'REPLYAPP', $name, $email, $JobTil);

            echo "OK";

        } else {

            echo "wrong";
        }

    } else {

        echo "already applied";

    }

    return false;
    // die();
}

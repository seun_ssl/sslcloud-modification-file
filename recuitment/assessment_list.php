<?php
session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
$EditID = '';
$venue = ScriptRunner("SELECT SetValue FROM Settings WHERE Setting='CompAddress'", "SetValue");
if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {
    unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);
    if (isset($_POST["FAction"]) && $_POST["FAction"] == "Deny" && $_POST["DelMax"] > 0) {
        $ids = json_decode($_POST['DelBox']);
        foreach ($ids as $id) {
            $Script = "UPDATE recjobs SET status='D' WHERE id = '" . $id . "'";
            ScriptRunnerUD($Script, "Inst");
            $applicant_detail = ScriptRunnercous("SELECT first_name, last_name, email FROM recjobs WHERE id = $id");
            $email = $applicant_detail['email'];
            $name = $applicant_detail['first_name'] . ' ' . $applicant_detail['last_name'];
            $job = ScriptRunner("SELECT title FROM jobs WHERE id ='" . $_POST['job_id'] . "'", "title");
            IntoMailTrail('INTERVIEW', 'DENY', $email, ['job_title' => $job], ['name' => $name, 'job_title' => $job]);
        }
        $EditID = $_POST['job_id'];
        echo "<script type='text/javascript'>{ parent.msgbox('Applicant(s) Denied Successfully', 'green'); }</script>";
    }
    if (isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Invite Applicant") {
        if (isset($_POST['applicant_ids']) && !empty($_POST['applicant_ids']) && isset($_POST['job_id']) && !empty($_POST['job_id']) && isset($_POST['venue']) && !empty($_POST['venue']) && isset($_POST['date']) && !empty($_POST['date']) && isset($_POST['time']) && !empty($_POST['time']) && isset($_POST['interviewers']) && !empty($_POST['interviewers'])) {
            $job_id = $_POST['job_id'];
            $EditID = $job_id;
            $venue = sanitize($_POST['venue']);
            $date = $_POST['date'];
            $applicant_ids = json_decode($_POST['applicant_ids']);
            $template_id = sanitize($_POST['template_id']);
            $time = date("h:i a", strtotime($_POST['time']));
            $interviewers = [];
            foreach ($applicant_ids as $applicant_id) {
                $data = ScriptRunnercous("SELECT first_name, last_name, email FROM recjobs WHERE id = $applicant_id");
                if ($data) {
                    $email = $data['email'];
                    $name = $data['first_name'] . ' ' . $data['last_name'];
                    $job = ScriptRunner("SELECT title FROM jobs WHERE id = '" . $job_id . "'", "title");
                    $hashkey = md5(date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff" . $_SESSION["StkTck" . "UName"] . $applicant_id);
                    $Script = "INSERT INTO [interviews] ([job_id], [applicant_id], [template_id], [date], [hashkey]) VALUES ('" . $job_id . "','" . $applicant_id . "','" . $template_id . "','" . $date . "','" . $hashkey . "')";
                    ScriptRunnerUD($Script, "Inst");
                    ScriptRunnerUD("UPDATE recjobs SET status='I' WHERE id = '" . $applicant_id . "'", "Inst");
                    foreach ($_POST['interviewers'] as $value) {
                        $Script = "INSERT INTO [interviewers] ([interview_hashkey], [emp_hashkey]) VALUES ('" . $hashkey . "','" . $value . "')";
                        ScriptRunnerUD($Script, "Inst");
                        if (!in_array($value, $interviewers)) {
                            $interviewers[] = $value;
                        }
                    }
                    //Send Interview Email
                    IntoMailTrail('INVITE', 'INTERVIEW', $email, ['job_title' => $job], ['name' => $name, 'venue' => $venue, 'date' => $date, 'time' => $time, 'job_title' => $job]);
                }
            }
            foreach ($interviewers as $interviewer) {
                $interviewer_detail = ScriptRunnercous("SELECT FName, SName, email FROM EmpTbl WHERE HashKey = '$interviewer'");
                if ($interviewer_detail['email']) {
                    $int_email = $interviewer_detail['email'];
                    $int_name = $interviewer_detail['FName'] . ' ' . $interviewer_detail['SName'];
                    IntoMailTrail('INTERVIEWER', 'MAIL', $int_email, '', ['name' => $int_name, 'interview_date' => $date, 'job_title' => $job, 'time' => $time]);
                }
            }
            echo "<script type='text/javascript'>{ parent.msgbox('Applicant(s) Invited', 'green'); }</script>";
        } else {
            echo "<script type='text/javascript'>{ parent.msgbox('All fields are required ', 'green'); }</script>";
        }
    }
}
if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
    if (isset($_REQUEST["AcctNo"]) && strlen($_REQUEST["AcctNo"]) == 32) {
        $EditID = $_REQUEST["AcctNo"];
    } else {
        echo ("<script type='text/javascript'>{ parent.msgbox('You must select a job record to open', 'red'); }</script>");
    }
}
$applist = function ($value, $key, $te) {
    if (array_key_exists($value, $te)) {
        $value = (is_array($te[$value])) ? implode('   ', $te[$value]) : $te[$value];
        echo "<td align='left' valign='middle' scope='col'>$value</td>";
    } else {
        echo "<td align='left' valign='middle' scope='col'></td>";
    }
};

$repalce = function ($a) {
    $a = str_replace(" ", "_", $a);
    return $a;
};
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

    <link href="../css/style_main.css" rel="stylesheet" type="text/css">
    <!-- Select 2-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/css/master_style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
    <script src="../assets/assets/vendor_components/popper/dist/popper.min.js"></script>
    <link rel="stylesheet" href="../assets/css/jquery-ui.min.css">

    <style>
      #ui-datepicker-div{ z-index:2000 !important; }

      .select2-selection__choice {
          color: #fff !important;
          margin-bottom: 5px;
      }
      .select2-container--default .select2-selection--multiple .select2-selection__rendered {
          padding: 3px 5px !important;
      }
      .select2.select2-container {
        width: 100% !important;
      }
      li.select2-selection__choice {
          background: #6dab69 !important;
          border-color: #6dab69 !important;
          padding: 3px !important;
      }
      strong, b {
        font-weight: 600;
      }
    </style>
  </head>
  <body>
    <section class="content"  style="min-height: 1000px">
	    <div class="box">
	      <div class="box-header with-border">
	        <div class="row">
	          <div class="col-md-6 text-md-left text-center">
	            <h4>Interview Invitation</h4>
	          </div>
	          <div class="col-md-6">
	            <form action="#" method="post" name="Records" target="_self" autocomplete="off" id="open-data">
	              <div class="input-group input-group-sm col-8 pull-right" style="margin-top: 2%;">
	                <select name="AcctNo" class="form-control" id="AcctNo">
	                  <?php
echo '<option value="" selected="selected">--</option>';
$dbOpen2 = ("SELECT * from jobs");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)):  ?>
	                     <option value="<?php echo $row2['id'] ?>" <?php echo ($row2['id'] === $EditID) ? 'selected' : '' ?>><?php echo $row2['title'] ?> </option>
	                  <?php
endwhile;
include '../login/dbClose2.php';
?>
	                </select>
	                <span class="input-group-btn">
	                    <!-- Ensure the name and value of this hidden field is same as button -->
                    	<input type="hidden" value="Open" name="SubmitTrans" />
	                  	<input type="submit" class="btn btn-danger" name="SubmitTrans" value="Open" />
	                </span>
	              </div>
	            </form>
	          </div>
	        </div>
	      </div>
	      <div class="box-body">
	        <?php if (isset($EditID) && !empty($EditID)): ?>
	          <?php
$job = $EditID;
$quey = "SELECT * FROM [jobs] WHERE [id] = '" . $EditID . "'";

$exam_status = "SELECT * FROM recjobs WHERE job_id = '" . $EditID . "' ";
$status_id = ScriptRunner($exam_status, 'status');

$getTemp = ScriptRunnercous($quey);
$tempId = $getTemp['job_application_template'];
$query = "SELECT * from [rectemplate] where  id='1'";
$getcheck = ScriptRunnercous($query);
if (count($getcheck)) {
    $fieldArry = [];
    $groupsarry = json_decode($getcheck['template_name'], true);
    foreach ($groupsarry as $key => $value) {
        if ($value['templateName'] == $tempId) {
            $ind = $key;
        }
    }
    if ($fa = $groupsarry[$ind]) {
        if ($fa['shortanswer'] != null) {
            foreach ($fa['shortanswer'] as $k => $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
        if ($fa['paragraph'] != null) {
            foreach ($fa['paragraph'] as $k => $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
        if ($fa['numeric'] != null) {
            foreach ($fa['numeric'] as $k => $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
        if ($fa['date'] != null) {
            foreach ($fa['date'] as $k => $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
        if ($fa['dropdown'] != null) {
            foreach ($fa['dropdown'] as $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
        if ($fa['checkbox'] != null) {
            foreach ($fa['checkbox'] as $v) {
                $fieldArry[] = ucfirst($v['fieldName']);
            }
        }
    }
}
?>
	          <div class="row">
	            <div class="col-md-12" id="shortlist" style="overflow-x: scroll">
	              <table width="100%" align="center" id="table" class="tablesorter">
	                <thead>
	                  <tr>
	                    <th width="20"valign="middle" scope="col" data-sorter="false"> <input type="checkbox" id="DelChk_All" onClick="ChkDel1();"  /><label for="DelChk_All"></label></th>
                        <th  align="center" valign="middle" scope="col">Application ID</th>
	                    <th align="center" valign="middle" scope="col">Full Name</th>
	                    <th align="center" valign="middle" scope="col">Email</th>
	                    <th align="center" valign="middle" scope="col">CV</th>
                       <th  align="center" valign="middle" scope="col">Application Date</th>

	                    <?php
if (!empty($fieldArry)) {
    array_map(function ($each) {
        echo " <th  align='center' valign='middle'' scope='col'>$each</th>";
    }, $fieldArry);
}
// Integrate Assessment ID
if (isset($status_id) && $status_id == 'A') {
    echo "<th  align='center' valign='middle' scope='col'>Quiz Title</th>";
    echo "<th  align='center' valign='middle' scope='col'>Total Multiple Choice Score</th>";
    echo "<th  align='center' valign='middle' scope='col'>Theory Details</th>";
}
?>
	                  </tr>
	                </thead>
	                <tbody>
	                  <?php
$dbOpen2 = "SELECT * FROM recjobs Where [job_id]='" . $EditID . "' AND ([status] = 'A' OR [status] IS NULL) order by id desc";
include '../login/dbOpen2.php';
$Del = 0;
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    ?>
	                  <tr>
	                    <td width="20" height="20" valign="middle" scope="col" class="TinyTextTight">
	                      <input type="checkbox" id="<?php echo ("DelBox" . $Del); ?>" value="<?php echo ($row2['id']); ?>" />
	                      <label for="<?php echo ("DelBox" . $Del); ?>"></label>
	                    </td>
                         <td><?php echo ($row2['id']); ?></td>
	                    <td align="left" valign="middle" scope="col"><?php echo (trim($row2['first_name'])); ?> <?php echo (trim($row2['middle_name'])); ?> <?php echo (trim($row2['last_name'])); ?></td>
	                    <td align="left" valign="middle" scope="col"><?php echo trim($row2['email']); ?></td>
	                    <td align="left" valign="middle" scope="col">  <a href="../public/<?php echo trim($row2['cv']); ?>" target="_blank" >view</a> </td>
                      
				                                                <td align="left" valign="middle" scope="col"><?php
    echo isset($row2['applied_on']) ? $row2['applied_on']->format("Y-m-d") : '' ?>

	                    <?php
$te = json_decode($row2['other_info'], true);
    if (array_key_exists('job_id', $te)) {
        unset($te['job_id']);
    }
    if (array_key_exists('cust_id', $te)) {
        unset($te['cust_id']);
    }
    if (isset($te) && !empty($te)) {
        $fieldArry = array_map('strtolower', $fieldArry);
        $fieldArry = array_map($repalce, $fieldArry);
        array_walk($fieldArry, $applist, $te);
    }
    // Integrate Assessment ID
    if (isset($row2['assess_id'])) {
        ?>
	                    <td align="left" valign="middle" scope="col">
	                      <?php
$script = "SELECT title, theory, [right] FROM quiz WHERE eid ='" . $row2['assess_id'] . "' ";
        $quizTitle = ScriptRunner($script, 'title');
        echo $quizTitle;
        ?>
	                    </td>
	                    <td align="left" valign="middle" scope="col">
                      <?php
$script_right = " SELECT [right] FROM recAssess WHERE eid ='" . $row2['assess_id'] . "'
                        AND email = '" . $row2['email'] . "' AND job_title ='" . $row2['job_id'] . "' ";
        $right = (int) ScriptRunner($script_right, 'right') * (int) ScriptRunner($script, 'right');
        // $right *= (int)ScriptRunner($script, 'right');

        echo $right;

        ?>
	                    </td>
	                    <td align="left" valign="middle" scope="col">
	                          <?php echo '<a id="theoryId" style="color:white;" data-toggle="modal" data-id="' . $row2['assess_id'] . '" data-email="' . $row2['email'] . '" data-jobId="' . $row2['job_id'] . '" class="btn btn-danger btn-xs" data-target="#theoryModal">View</a>'; ?>
	                    </td>
	                    <?php }?>
	                    <?php }
include '../login/dbClose2.php';?>
	                  </tr>
	                </tbody>
	              </table>
	              <?php include '../main/pagination.php';?>
	              <div style="display: flex">
	                <div class="" style="margin-top: 10px; padding-right: 5px">
	                  <form action="#" method="post" name="Records" target="_self" id="Records" autocomplete="off">
	                    <input type="button" value="Invite For Interview" class="btn btn-sm btn-danger btn" id="inviteInterview">
	                    <input type="hidden" id="DelBox" name="DelBox" value="">
	                    <?php
echo '<input name="DelMax" id="DelMax" type="hidden" value="' . $Del . '" />
	                            <input name="PgDoS" id="PgDoS" type="hidden" value="' . DoSFormToken() . '">
	                            <input name="job_id" type="hidden" value="' . $EditID . '">';
echo "<input name='FAction' id='FAction' type='hidden'>";
?>
	                    <input type="submit" value="Reject Applicant" name="SubmitTrans" class="btn btn-sm btn-danger btn ">
	                  </form>
	                </div>

	                <button class="btn btn-danger btn-xs" id="resend" style="margin-top: 10px">Resend Assessment Link</button>
	              </div>
	            </div>
	          </div>
	        <?php endif?>
	      </div>
	      <!-- /.box-body -->
	    </div>

	    <!-- MODAL FOR THEORY DETAILS -->
	    <div id="theoryModal" class="modal hide fade" role="dialog" aria-labelledby="theoryModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title text-center " id="submitModal">Theory Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>

                                                  <div class="modal-body">
                                                      <div class="container-fluid">
                                                          <div class="row">
                                                          <div id="theoryDetails" class="modal-body"></div>

                                                                    </div>
                                                      </div>
                                                  </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                              </div>

                                          </div>
                                          </div>
                                            </div>
                                            <!-- END OF MODAL FOR THEORY DETAILS -->
	</section>

    <script src="../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../assets/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
    <!-- FastClick -->
    <script src="../assets/assets/vendor_components/fastclick/lib/fastclick.js"></script>
    <!-- MinimalLite Admin App -->
    <script src="../assets/js/template.js"></script>
    <script src="../assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- MinimalLite Admin for demo purposes -->
    <script src="../assets/js/demo.js"></script>
    <script>
    	$("#open-data").submit(function(e) {
	        e.preventDefault();
	        if (!$('#AcctNo').val()) {
	          parent.msgbox('You must select a job record to open', 'green');
	          return;
	        }
	        this.submit();
	    });
      let id = [];
      $('table tbody :checkbox').change(function() {
        if($(this).is(":checked")) {
        	if (!id.includes($(this).val())) {
          		id.push($(this).val());
        	}
        } else {
            id = id.filter(value => value !== $(this).val());
        }
         $('#DelBox').val(JSON.stringify(id));
      });
      $("#Records").submit(function(e) {
        e.preventDefault();
        if (!id.length) {
          parent.msgbox('Select An Applicant', 'green');
          return;
        }
        GeneralYesNo('This will end the selected applicant(s) recruitment process for this job. Are You Sure?', 'Reject Applicant(s)', 'Deny')
      });
      $("#inviteInterview").click(function(e) {
        // alert('here');
        e.preventDefault();
        e.stopPropagation();
        if (!id.length) {
          parent.msgbox('Select An Applicant', 'green');
          return;
        }
        const template = `<form action="#" method="post" name="Records" target="_self" id="inviteForm" autocomplete="off">
          <div class="col-md-12 form-group">
            <label>Venue</label>
              <input type="text" required name="venue" class="form-control" id="venue" value="<?php echo $venue ?>" placeholder="No 4, Test Street, OK.">
          </div>
          <div class="col-md-12 form-group">
            <label>Date</label>
            <input required type="text" name="date" class="form-control"  id="ExDt" readonly >
          </div>
          <div class="col-md-12 form-group">
            <label>Time</label>
            <input type="text" required name="time" class="form-control" id="timepicker" readonly>
          </div>
          <div class="col-md-12 form-group">
            <label>Interviewers</label>
            <select required class="select2 form-control" multiple="multiple" name="interviewers[]">
              <?php
$SelID = (isset($_REQUEST["employees"])) ? $_REQUEST["employees"] : [];
$dbOpen2 = ("SELECT * FROM EmpTbl WHERE Status IN ('A','U','N') AND EmpStatus='Active' ORDER BY SName ASC");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)):
?>
              <option <?php echo (in_array($row2['HashKey'], $SelID)) ? 'selected' : '' ?> value="<?php echo $row2['HashKey']; ?>">
                <?php echo str_replace("`", "", $row2['SName']) . ' ' . str_replace("`", "", $row2['FName']) . ' [' . $row2['EmpID'] . ']'; ?>
              </option>
              <?php endwhile;include '../login/dbClose2.php';?>
            </select>
          </div>
          <div class="col-md-12 form-group">
            <label>Select Interview Template</label>
            <select name="template_id" class="form-control">
              <option value="">--</option>
              <?php
$query = "SELECT * from [interview_template] where  id='1'";
$getcheck = ScriptRunnercous($query);
$groupsarry = json_decode($getcheck['template_name'], true);
?>
              <?php if ($groupsarry): ?>
	              <?php foreach ($groupsarry as $eachgroup): ?>
	              <option value="<?php echo $eachgroup['id']; ?>"><?php echo $eachgroup['templateName']; ?></option>
	              <?php endforeach;?>
              <?php endif;?>
            </select>
          </div>
          <input type="hidden" name="applicant_ids" value='${JSON.stringify(id)}'>
          <input type="hidden" name="job_id" value="<?php echo $EditID ?>">
          <input name="PgDoS" id="PgDoS" type="hidden" value="<?php echo DoSFormToken() ?>">
        </form>`
        $('<div></div>').appendTo('body')
        .html(template)
        .dialog({
          modal: true, title: "Enter Interview Details.", zIndex: 10000, autoOpen: true,
          height: 500 + 6,
          width: 500 + 20,
          buttons: {
            Submit: function () {
              e.preventDefault();
                const form = document.querySelector('#inviteForm');
                const venue = form.venue.value.trim();
                const applicant_ids = form.applicant_ids.value.trim();
                const job_id = form.job_id.value.trim();
                const date = form.date.value.trim();
                const time = form.time.value.trim();
                const interviewers = $(form).find('select[name="interviewers[]"]').val();
                if (!venue || !applicant_ids || !job_id || !date || !time || !interviewers) {
                parent.msgbox('Fill in required fields', 'green');
                return;
              }
              $(form).append('<input type="hidden" name="SubmitTrans" value="Invite Applicant" />');
              form.submit()
            },
            Cancel: function () {
              $(this).dialog("close");
            }
          },
          close: function (event, ui) {
            $(this).remove();
          }
        });
        $('.select2').select2();
        $("#ExDt").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "+0D", maxDate: "+12M", dateFormat: 'dd M yy',yearRange: "-75:+75"})
        $('#timepicker').timepicker({
          showMeridian: false,
        });
      });
    </script>

<script type="text/javascript">
  $(document).on("click", "#theoryId", function() {
    const theoryId = $(this).attr("data-id");
    const theoryEmail = $(this).attr("data-email");
    const theoryJobId = $(this).attr("data-jobId");

    $.ajax({
url: "theory_response.php",
type: "POST",
data: {theoryId: theoryId, theoryEmail:theoryEmail, theoryJobId:theoryJobId},
success: function(response) {
     // Add response in Modal body
     $('#theoryDetails').html(response);
     // Display Modal
     $('#theoryModal').modal('show');
}
});

  });

   // $(document).on('click', '.resend', function(e){


   //      // console.log($(this).data("id"));
   //      const id=$(this).data("id");
   //      $.ajax({
   //      url: "rec_process.php",
   //      type: "POST",
   //      data: {id:id,type:'resend'},
   //      success: function(res) {
   //           { parent.msgbox('Assessment link sent successfully. ', 'green'); };
   //      }
   //      });


   // });

   const resend = document.querySelector('#resend');


   var ChkDel1 = () =>{
	    const allcheckboxs= document.querySelectorAll('table input[type="checkbox"]');
	     // console.log(allcheckboxs[0]);
		if (allcheckboxs[0].checked) {
		    allcheckboxs.forEach(checkbox => {
		    	if (checkbox.id !=="DelChk_All") {
	            	checkbox.checked = true;
	            	$(checkbox).trigger('change')
	            }
	       });
	    } else {
	        allcheckboxs.forEach(checkbox => {
		        if (checkbox.id !=="DelChk_All") {
		            checkbox.checked = false;
	            	id = [];
		        }
		    });
	    }
 	}

   resend.addEventListener('click', e =>{
     const allcheckboxs= document.querySelectorAll('table input[type="checkbox"]');
     const aryallcheckboxs=Array.from(allcheckboxs);

     // console.log(aryallcheckboxs);


      const filte= aryallcheckboxs.filter(eac=>{
         return eac.id !=='DelChk_All';
      });

      const secfilter = filte.filter(input=> input.checked);

      if(!secfilter.length){
         { parent.msgbox('Please select an applicant ', 'red'); };
         return;
      }

      let selectedIds=secfilter.map(input => input.value);
      selectedIds = JSON.stringify(selectedIds);

      // console.log(selectedIds);
      // return;



      $('<div></div>').appendTo('body')
      .html('<div><class=TextBoxText>Are you sure you want to <strong>resend assessment link to the selected.</strong>?</h5></div>')
      .dialog({
        modal: true,
        title: "Resend Assessment Link",
        zIndex: 10000,
        autoOpen: true,
        width: 'auto',
        resizable: true,
        height: 'auto',
        buttons: {
          Yes: function () {

            $.ajax({
                      url:"rec_process.php",
                      method:"POST",
                      data:{selected:selectedIds,type:"resend"},
                      success:function(response){

                          if (response==='OK') {
                            { parent.msgbox('Assessment Link sent successfully. ', 'green'); };

                          }

                    },
                    error:function(){
                    alert('Something went wrong');
                    }
                    });




             $(this).dialog("close");
          },
          No: function () {
            $(this).dialog("close");
          }
        },
        close: function (event, ui) {
          $(this).remove();
        }
      });




   });
  </script>

  </body>
</html>
<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

//Validate user viewing rights
//if (ValidateURths("CREATE APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}

if (!isset($EditID)) {$EditID = "";}
if (!isset($Script_Edit)) {$Script_Edit = "";}
if (!isset($HashKey)) {$HashKey = "";}
if (!isset($SelID)) {$SelID = "";}

$GoValidate = true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");

if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS' . $_REQUEST["PgDoS"]]) && $_SESSION['DoS' . $_REQUEST["PgDoS"]] == md5('DoS' . $_SESSION["StkTck" . "UName"] . $_REQUEST["PgDoS"]) && strlen(DoSFormToken()) == 32) {unset($_SESSION['DoS' . $_REQUEST["PgDoS"]]);

    if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "CREATE Appraisal") {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        //    include 'add_user_validation.php';
        if (isset($_REQUEST["AID"]) && trim($_REQUEST["AID"]) != "") {
            $GoValidate = true;
            if (strlen(trim($_REQUEST["AID"])) < 3) {
                $GoValidate = false;
                echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
            }

            $Script = "Select * from KPIStart where AID='" . ECh($_REQUEST["AID"]) . "' and Status<>'D'";
            if (strlen(ScriptRunner($Script, "LogName")) > 0 || strlen(ScriptRunner($Script, "FullName") > 0)) {
                echo ("<script type='text/javascript'>{parent.msgbox('An existing appraisal with a duplicate name already exist. Please review.', 'red');}</script>");
                $GoValidate = false;
            }
        }

        if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"]) == "") {
            echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
            $GoValidate = false;
        } else //Means the date was selected. Check if it falls within another appraisal period
        {
            $Script = "Select Count(*) Ct from KPIStart where '" . ECh($_REQUEST["SDate"]) . "' between [SDate] and [EDate] and Status<>'D'";
            if (ScriptRunner($Script, "Ct") > 0) {
                echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
                $GoValidate = false;
            }
        }

        if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"]) == "") {
            echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
            $GoValidate = false;
        } else //Means the date was selected. Check if it falls within another appraisal period
        {
            $Script = "Select Count(*) Ct from KPIStart where '" . ECh($_REQUEST["EDate"]) . "' between [SDate] and [EDate] and Status<>'D'";
            if (ScriptRunner($Script, "Ct") > 0) {
                echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
                $GoValidate = false;
            }
        }

        if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"]) == "") {
            echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
            $GoValidate = false;
        }

        if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"]) == "") {
            echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
            $GoValidate = false;
        }

        if (isset($_REQUEST["MDate"]) && trim($_REQUEST["MDate"]) == "") {
            echo ("<script type='text/javascript'>{parent.msgbox('You must select a mediation end date for the selected appraisal period to end. Please review.', 'red');}</script>");
            $GoValidate = false;
        }
        /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "TimeOff" . $_SESSION["StkTck" . "UName"];
        $HashKey = md5($UniqueKey);

        if ($GoValidate == true) {
            $Script = "INSERT INTO [KPIStart] ([AID],[SDate],[EDate],[MDate],[RevSDate],[RevEDate],[ScoreType],[ACount],[ARemark],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('" . ECh($_REQUEST["AID"]) . "','" . ECh($_REQUEST["SDate"]) . "','" . ECh($_REQUEST["EDate"]) . "','" . ECh($_REQUEST["MDate"]) . "','" . ECh($_REQUEST["RevSDate"]) . "','" . ECh($_REQUEST["RevEDate"]) . "','" . ECh($_REQUEST["ScoreType"]) . "'," . ECh($_REQUEST["ACount"]) . ",'" . ECh($_REQUEST["ARemark"]) . "','N','" . ECh($_SESSION["StkTck" . "HKey"]) . "',GetDate(),'" . $HashKey . "')";
            //        echo $Script;
            ScriptRunnerUD($Script, "Inst");

            //Appraisal Audit Trail
            AuditLog("INSERT", "New appraisal start/end [" . ECh($_REQUEST["AID"]) . "] created ");
            if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on") {
                MailTrail("CREATE APPRAISAL", "T", '', '', '', ''); //Sends a mail notifications
            }

            echo ("<script type='text/javascript'>{ parent.msgbox('New organisational appraisal created successfully.', 'green'); }</script>");
        }
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
    }
    //**************************************************************************************************
    //**************************************************************************************************

    elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Appraisal" && isset($_REQUEST["UpdID"]) && $_REQUEST["UpdID"] != "") {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set HashKey of Account to be Updated */
        $EditID = ECh($_REQUEST["UpdID"]);
        $HashKey = ECh($_REQUEST["UpdID"]);

        /*Check validation for updating an account */
        if (strlen($EditID) == 32) {
            if (strlen(trim($_REQUEST["AID"])) < 3) {
                echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
                $GoValidate = false;
            }

            if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"]) == "") {
                echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
                $GoValidate = false;
            } else //Means the date was selected. Check if it falls within another appraisal period
            {
                $Script = "Select Count(*) Ct from KPIStart where '" . ECh($_REQUEST["SDate"]) . "' between [SDate] and [EDate] and Status<>'D' and HashKey<>'" . $EditID . "'";
                if (ScriptRunner($Script, "Ct") > 0) {
                    echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
                    $GoValidate = false;
                }
            }

            if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"]) == "") {
                echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
                $GoValidate = false;
            } else //Means the date was selected. Check if it falls within another appraisal period
            {
                $Script = "Select Count(*) Ct from KPIStart where '" . ECh($_REQUEST["EDate"]) . "' between [SDate] and [EDate] and Status<>'D' and HashKey<>'" . $EditID . "'";
                if (ScriptRunner($Script, "Ct") > 0) {
                    echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
                    $GoValidate = false;
                }
            }

            if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"]) == "") {
                echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
                $GoValidate = false;
            }

            if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"]) == "") {
                echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
                $GoValidate = false;
            }

            if ($GoValidate == true) {
                $Script = "Update [KPIStart] set [AID]='" . ECh($_REQUEST["AID"]) . "',[SDate]='" . ECh($_REQUEST["SDate"]) . "',[EDate]='" . ECh($_REQUEST["EDate"]) . "',[MDate]='" . ECh($_REQUEST["MDate"]) . "',[RevSDate]='" . ECh($_REQUEST["RevSDate"]) . "',[RevEDate]='" . ECh($_REQUEST["RevEDate"]) . "',[ScoreType]='" . ECh($_REQUEST["ScoreType"]) . "',[ACount]=" . ECh($_REQUEST["ACount"]) . ",[ARemark]='" . ECh($_REQUEST["ARemark"]) . "',[Status]='U',[UpdatedBy]='" . $_SESSION["StkTck" . "HKey"] . "',[UpdatedDate]=GetDate() where [HashKey]= '" . $HashKey . "'";
                ScriptRunnerUD($Script, "Inst");

                //Appraisal Audit Trail
                AuditLog("UPDATE", "Appraisal start/end updated for [" . ECh($_REQUEST["AID"]) . "]");
                if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on") {
                    MailTrail("CREATE APPRAISAL", "T", '', '', '', ''); //Sends a mail notifications
                }

                echo ("<script type='text/javascript'>{ parent.msgbox('Organisational appraisal updated successfully.', 'green'); }</script>");
                $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";
            }
        } else {
            $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='" . $EditID . "'";
            include '../main/prmt_blank.php';
        }
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0) {
        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='" . $EditID . "'";
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '" . $EditID . "'") == true) {
                    $Script = "Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '" . $EditID . "'";
                    if (ScriptRunner($Script, "Dt") > 0) {
                        //Mark the record as Authorized
                        $Script = "UPDATE KPIStart
						SET [Status] = 'A'
						,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '" . $EditID . "'";
                        ScriptRunnerUD($Script, "Authorize");
                        AuditLog("AUTHORIZE", "Appraisal record authorized");

                        echo ("<script type='text/javascript'>{ parent.msgbox('Selected appraisal records(s) authorized successfully.','green'); }</script>");
                    } else {
                        echo ("<script type='text/javascript'>{ parent.msgbox('Unable to authorized past appraisal.','red'); }</script>");
                    }
                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='" . $EditID . "'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "A") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select * from [KPIStart] where HashKey='--'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);
                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
                /*                if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".ECh($EditID)."'") != true)
                {echo "Here";
                exit;
                 */
                $Script = "Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '" . $EditID . "'";
                if (ScriptRunner($Script, "Dt") >= 0) {
                    //Mark the record as Authorized
                    $Script = "UPDATE KPIStart
						SET [Status] = 'U'
						,[AuthBy] =  '" . $_SESSION["StkTck" . "HKey"] . "'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '" . $EditID . "'";
                    ScriptRunnerUD($Script, "Authorize");
                    AuditLog("UNAUTHORIZE", "Appraisal record unauthorized");

                    echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) unauthorized successfully', 'green');}</script>");
                    $Script_Edit = "select CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";
                }
//                }
            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='" . $EditID . "'";
    } elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0) {
        if (ValidateURths("CREATE APPRAISAL" . "D") != true) {include '../main/NoAccess.php';exit;}

        /* Set EditID to -- and Script_Edit to request a NULL HashKey */
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='" . $EditID . "'";

        $HashVals = explode(";", $_REQUEST["ActLnk"]);
        $ArrayCount = count($HashVals);
        for ($i = 0; $i <= $ArrayCount - 1; $i++) { //ActLnk
            if (isset($HashVals[$i]) && strlen(trim($HashVals[$i])) == 32) {
                $EditID = ECh($HashVals[$i]);

                //*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************

                /*
            if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".$EditID."'") == true)
            {
            //Mark the record as Authorized

            $Script = "UPDATE KPIStart
            SET [Status] = 'D'
            ,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
            ,[AuthDate] = GetDate()
            WHERE [HashKey] = '".$EditID."'";
            ScriptRunnerUD ($Script, "Authorize");
            AuditLog("DELETE","Appraisal record deleted");

            echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) deleted successfully', 'green');}</script>");
            $Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='".$EditID."'";
            }
             */

            }
        }

        /* Prompt User to Select a record to edit - NO RECORD SELECTED */
        include '../main/prmt_blank.php';
        $EditID = '--';
        $Script_Edit = "select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";

    }
}

$strExp = "";
?>

<?php if (isset($_SESSION["StkTck" . "StlyeSheet"])) {echo '<link href="../css/' . $_SESSION["StkTck" . "StlyeSheet"] . '" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script>
  $(function()
  {
	$("#SDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#EDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#MDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#RevSDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+0D", dateFormat: 'dd M yy'})
	$("#RevEDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+0D", dateFormat: 'dd M yy'})

  });


$(function() {$( "#tabs" ).tabs();});
$(function() {    $( document ).tooltip();  });
</script>


<body oncontextmenu="return false;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

<table width="100%" border="0" align="center" class="tdMenu_HeadBlock">
  <tr>
    <td class="subHeader"><div align="center">Download Employee KPI Details</div></td>
  </tr>
</table>
<form action="#" method="post" enctype="multipart/form-data" name="Records" target="_self" id="Records" autocomplete="off">


<?php include 'rpt_header_print.php';?>

  <tr>
    <td align="center">
    <div class="ViewPatch">


        <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1" id="table">
    	<thead>
      		<tr>
                <th width="30%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Appraisal</span></th>
                <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Appraisee Name</span></th>
                <th width="20%" valign="middle"  scope="col"><span class="TinyTextTightBold">Appraisee Score</span></th>
                <th width="20%" align="center" valign="middle"  scope="col"><span class="TinyTextTightBold">Supervisor Name</span></th>
                <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Supervisor Score</span></th>
                <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">View</span></th>
        	</tr>
        </thead>
        <tbody>
          <?php
$Del = 0;

$dbOpen2 = ("SELECT * from [KPIFinalScore] WHERE Status <> 'D' order by AddedDate desc");
/*
}
else
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from Users where Status = 'Z' order by LogName asc");
}
 */
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    $TotAScoreEmp = 0;
    $TotMgrScore = 0;
    ?>
          <tr>
            <?php
$Script1 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $row2['EID'] . "'"; // Gets Emp Name
    $name1 = ScriptRunner($Script1, "Nm");
    $Script2 = "Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='" . $row2['MID'] . "'"; // Gets Mgr Name
    $name2 = ScriptRunner($Script2, "Nm");

    $KPISetting_hashkey = $row2['KPISetting_hashkey'];

    $Script = ("Select SUM(AScoreMrg) TotMgrScore, SUM(AScoreEmp) TotAScoreEmp  from KPIIndvScore WHERE AID='" . $KPISetting_hashkey . "' AND FinalHashKey = '" . $row2['HashKey'] . "' AND Status <> 'D'");
    $TotAScoreEmp = ScriptRunner($Script, "TotAScoreEmp");
    $TotMgrScore = ScriptRunner($Script, "TotMgrScore");

    $TotAScoreEmp = ($TotAScoreEmp / 2); //50%
    $TotMgrScore = ($TotMgrScore / 2); //50%

    ?>
            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['AQuartNm'])); ?></td>
            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo $name1; ?></td>
            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo $TotAScoreEmp; ?></td>
            <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo $name2; ?></td>
            <td valign="middle" align="center" class="TinyText">&nbsp;<?php echo $TotMgrScore; ?></td>
            <td align="center" valign="middle" class="TinyText">&nbsp;
              <?php
$hash = trim($row2['HashKey']);
    print "<a href=\"fidson_report_3.php?AID=$KPISetting_hashkey&name=$name1&name2=$name2&rn=" . $_REQUEST['rn'] . "&PgName=" . $_REQUEST['PgName'] . "&key=" . $row2['HashKey'] . "\">
				<img src=\"../images/view-icon.png\"  width=\"25\" >
				</a>";
    ?></td>
          </tr>
          <?php }
//include '../login/dbClose2.php';
?>
        </tbody>
      </table>
    </div>
    </td></tr>
    <tr>
      <td align="center">&nbsp;</td></tr>

</form>
</tables>
<p>&nbsp;</p>
<tr>
  <td align="center"></div></td>
  </tr>
  <tr>
    <td align="center"></td>
  </tr>

<p>&nbsp;</p>

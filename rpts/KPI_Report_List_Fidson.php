<?php session_start( );
include '../login/scriptrunner.php';
$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';

//Validate user viewing rights
//if (ValidateURths("CREATE APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}


if (!isset($EditID)){$EditID="";}
if (!isset($Script_Edit)){$Script_Edit="";}
if (!isset($HashKey)){$HashKey="";}
if (!isset($SelID)){$SelID="";}


$GoValidate=true;
echo ("<script type='text/javascript'>{ parent.msgbox('', 'red'); }</script>");


if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32)
{unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);

	if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "CREATE Appraisal")
	{
		if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}

	//	include 'add_user_validation.php';
		if (isset($_REQUEST["AID"]) && trim($_REQUEST["AID"])!="" )
		{
			$GoValidate = true;
			if (strlen(trim($_REQUEST["AID"]))<3)
			{
				$GoValidate = false;
				echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
			}

			$Script ="Select * from KPIStart where AID='".ECh($_REQUEST["AID"])."' and Status<>'D'";
			if (strlen(ScriptRunner($Script, "LogName"))>0 || strlen(ScriptRunner($Script, "FullName")>0))
			{
				echo ("<script type='text/javascript'>{parent.msgbox('An existing appraisal with a duplicate name already exist. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
		}

		if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"])=="")
		{
			echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
			$GoValidate = false;
		}
		else //Means the date was selected. Check if it falls within another appraisal period
		{
			$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["SDate"])."' between [SDate] and [EDate] and Status<>'D'";
			if (ScriptRunner($Script,"Ct")>0)
			{
				echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
		}

		if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"])=="")
		{
			echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
			$GoValidate = false;
		}
		else //Means the date was selected. Check if it falls within another appraisal period
		{
			$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["EDate"])."' between [SDate] and [EDate] and Status<>'D'";
			if (ScriptRunner($Script,"Ct")>0)
			{
				echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
		}


		if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"])=="")
		{
			echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
			$GoValidate = false;
		}

		if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"])=="")
		{
			echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
			$GoValidate = false;
		}
		
		if (isset($_REQUEST["MDate"]) && trim($_REQUEST["MDate"])=="")
		{
			echo ("<script type='text/javascript'>{parent.msgbox('You must select a mediation end date for the selected appraisal period to end. Please review.', 'red');}</script>");
			$GoValidate = false;
		}
		/* Create a HashKey of the Row ID  as identifier for each unique staff record*/
		$UniqueKey = date('l jS \of F Y h:i:s A').gettimeofday(true)."TimeOff".$_SESSION["StkTck"."UName"];
		$HashKey = md5($UniqueKey);
	
		if ($GoValidate == true)
		{
			$Script = "INSERT INTO [KPIStart] ([AID],[SDate],[EDate],[MDate],[RevSDate],[RevEDate],[ScoreType],[ACount],[ARemark],[Status],[AddedBy],[AddedDate],[HashKey]) VALUES ('".ECh($_REQUEST["AID"])."','".ECh($_REQUEST["SDate"])."','".ECh($_REQUEST["EDate"])."','".ECh($_REQUEST["MDate"])."','".ECh($_REQUEST["RevSDate"])."','".ECh($_REQUEST["RevEDate"])."','".ECh($_REQUEST["ScoreType"])."',".ECh($_REQUEST["ACount"]).",'".ECh($_REQUEST["ARemark"])."','N','".ECh($_SESSION["StkTck"."HKey"])."',GetDate(),'".$HashKey."')";
	//		echo $Script;
			ScriptRunnerUD($Script,"Inst");
	
			//Appraisal Audit Trail		
			AuditLog("INSERT","New appraisal start/end [".ECh($_REQUEST["AID"])."] created ");
			if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
			{
				MailTrail("CREATE APPRAISAL","T",'','','',''); //Sends a mail notifications
			}

			echo ("<script type='text/javascript'>{ parent.msgbox('New organisational appraisal created successfully.', 'green'); }</script>");
		}
		$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
	}
	//**************************************************************************************************
	//**************************************************************************************************
	
	elseif (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Update Appraisal" && isset($_REQUEST["UpdID"]) && $_REQUEST["UpdID"] !="")
	{
		if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
		
		/* Set HashKey of Account to be Updated */
		$EditID = ECh($_REQUEST["UpdID"]);
		$HashKey = ECh($_REQUEST["UpdID"]);
		
		/*Check validation for updating an account */
		if (strlen($EditID) ==32)
		{
			if (strlen(trim($_REQUEST["AID"]))<3)
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must enter an appraisal name with at least 3 characters to proceed. Please review', 'red');}</script>");
				$GoValidate = false;
			}
		
			if (isset($_REQUEST["SDate"]) && trim($_REQUEST["SDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a start date for the selected appraisal period to begin. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
			else //Means the date was selected. Check if it falls within another appraisal period
			{
				$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["SDate"])."' between [SDate] and [EDate] and Status<>'D' and HashKey<>'".$EditID."'";
				if (ScriptRunner($Script,"Ct")>0)
				{
					echo ("<script type='text/javascript'>{parent.msgbox('Selected start date falls within another apprisal period. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
			}
	
			if (isset($_REQUEST["EDate"]) && trim($_REQUEST["EDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a end date for the selected appraisal period to end. Please review.', 'red');}</script>");
				$GoValidate = false;
			}
			else //Means the date was selected. Check if it falls within another appraisal period
			{
				$Script="Select Count(*) Ct from KPIStart where '".ECh($_REQUEST["EDate"])."' between [SDate] and [EDate] and Status<>'D' and HashKey<>'".$EditID."'";
				if (ScriptRunner($Script,"Ct")>0)
				{
					echo ("<script type='text/javascript'>{parent.msgbox('Selected end date falls within another apprisal period. Please review.', 'red');}</script>");
					$GoValidate = false;
				}
			}
	
	
			if (isset($_REQUEST["RevSDate"]) && trim($_REQUEST["RevSDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
				$GoValidate = false;
			}
	
			if (isset($_REQUEST["RevEDate"]) && trim($_REQUEST["RevEDate"])=="")
			{
				echo ("<script type='text/javascript'>{parent.msgbox('You must select a From and To date for the period under review.', 'red');}</script>");
				$GoValidate = false;
			}
		
			if ($GoValidate == true)
			{
			$Script = "Update [KPIStart] set [AID]='".ECh($_REQUEST["AID"])."',[SDate]='".ECh($_REQUEST["SDate"])."',[EDate]='".ECh($_REQUEST["EDate"])."',[MDate]='".ECh($_REQUEST["MDate"])."',[RevSDate]='".ECh($_REQUEST["RevSDate"])."',[RevEDate]='".ECh($_REQUEST["RevEDate"])."',[ScoreType]='".ECh($_REQUEST["ScoreType"])."',[ACount]=".ECh($_REQUEST["ACount"]).",[ARemark]='".ECh($_REQUEST["ARemark"])."',[Status]='U',[UpdatedBy]='".$_SESSION["StkTck"."HKey"]."',[UpdatedDate]=GetDate() where [HashKey]= '".$HashKey."'";
				ScriptRunnerUD($Script,"Inst");

				//Appraisal Audit Trail		
				AuditLog("UPDATE","Appraisal start/end updated for [".ECh($_REQUEST["AID"])."]");
				if (isset($_REQUEST["AuthNotifier"]) && $_REQUEST["AuthNotifier"] == "on")
				{
					MailTrail("CREATE APPRAISAL","T",'','','',''); //Sends a mail notifications
				}
	
				echo ("<script type='text/javascript'>{ parent.msgbox('Organisational appraisal updated successfully.', 'green'); }</script>");
				$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";
			}
		}
		else
		{
			$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
			include '../main/prmt_blank.php';
		}
	}

	elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "View Selected" && $_REQUEST["DelMax"] > 0)
	{
		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
	
		$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
		for ($i=0; $i<=$ArrayCount-1; $i++)
		{//ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
			{
				$EditID = ECh($HashVals[$i]);

				$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
	}
	
	elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Authorize Selected" && $_REQUEST["DelMax"] > 0)
	{
		if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
		
		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";
	
		$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
		for ($i=0; $i<=$ArrayCount-1; $i++)
		{//ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
			{
				$EditID = ECh($HashVals[$i]);
			//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
				if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".$EditID."'") == true)
				{
					$Script="Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '".$EditID."'";
					if (ScriptRunner($Script,"Dt")>0)
					{
						//Mark the record as Authorized
						$Script = "UPDATE KPIStart
						SET [Status] = 'A'
						,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '".$EditID."'";
						ScriptRunnerUD ($Script, "Authorize");
						AuditLog("AUTHORIZE","Appraisal record authorized");
		
						echo ("<script type='text/javascript'>{ parent.msgbox('Selected appraisal records(s) authorized successfully.','green'); }</script>");
					}
					else
					{
						echo ("<script type='text/javascript'>{ parent.msgbox('Unable to authorized past appraisal.','red'); }</script>");
					}
				}
			}
		}

		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID='--';
$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
	}
	
	elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Unauthorize Selected" && $_REQUEST["DelMax"] > 0)
	{
		if (ValidateURths("CREATE APPRAISAL"."A")!=true){include '../main/NoAccess.php';exit;}
		
		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
		$Script_Edit ="select * from [KPIStart] where HashKey='--'";
	
		$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
		for ($i=0; $i<=$ArrayCount-1; $i++)
		{//ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
			{
				$EditID = ECh($HashVals[$i]);
			//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
/*				if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".ECh($EditID)."'") != true)
				{echo "Here";
				exit;
*/
					$Script="Select DATEDIFF(D,Getdate(),EDate) Dt from KPIStart where [HashKey] = '".$EditID."'";
					if (ScriptRunner($Script,"Dt")>=0)
					{
						//Mark the record as Authorized
						$Script = "UPDATE KPIStart
						SET [Status] = 'U'
						,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
						,[AuthDate] = GetDate()
						WHERE [HashKey] = '".$EditID."'";
						ScriptRunnerUD ($Script, "Authorize");
						AuditLog("UNAUTHORIZE","Appraisal record unauthorized");

						echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) unauthorized successfully', 'green');}</script>");
						$Script_Edit ="select CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='--'";	
					}
//				}
			}
		}
		
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID='';
		$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='".$EditID."'";
	}
	
	elseif (isset($_REQUEST["FAction"]) && $_REQUEST["FAction"] == "Delete Selected" && $_REQUEST["DelMax"] > 0)
	{
		if (ValidateURths("CREATE APPRAISAL"."D")!=true){include '../main/NoAccess.php';exit;}
		
		/* Set EditID to -- and Script_Edit to request a NULL HashKey */
		$EditID = '--';
$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='".$EditID."'";
	
		$HashVals = explode(";",$_REQUEST["ActLnk"]); $ArrayCount = count($HashVals);
		for ($i=0; $i<=$ArrayCount-1; $i++)
		{//ActLnk
			if (isset($HashVals[$i]) && strlen(trim($HashVals[$i]))==32)
			{
				$EditID = ECh($HashVals[$i]);

			//*************************************CHECK IF RECORD IS AUTHORIZED BEFORE UPDATE OR DELETE**********************************
			
			/*
				if (ValidateUpdDel("SELECT Status from KPIStart WHERE [HashKey] = '".$EditID."'") == true)
				{
				//Mark the record as Authorized
			
					$Script = "UPDATE KPIStart
					SET [Status] = 'D'
					,[AuthBy] =  '".$_SESSION["StkTck"."HKey"]."'
					,[AuthDate] = GetDate()
					WHERE [HashKey] = '".$EditID."'";
					ScriptRunnerUD ($Script, "Authorize");
					AuditLog("DELETE","Appraisal record deleted");
	
					echo ("<script type='text/javascript'>{parent.msgbox('Selected appraisal records(s) deleted successfully', 'green');}</script>");
					$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, * from [KPIStart] where HashKey='".$EditID."'";
				}
			*/
			
			}
		}
		
		/* Prompt User to Select a record to edit - NO RECORD SELECTED */
		include '../main/prmt_blank.php';
		$EditID='--';
		$Script_Edit ="select CONVERT(Varchar(11),MDate,106) MDt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt, CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,* from [KPIStart] where HashKey='--'";		

	}
}
?>

<?php if (isset($_SESSION["StkTck"."StlyeSheet"])) {echo '<link href="../css/'.$_SESSION["StkTck"."StlyeSheet"].'" rel="stylesheet" type="text/css">';} else {?><link href="../css/style_main.css" rel="stylesheet" type="text/css"><?php }?>

<script>
  $(function()
  {
	$("#SDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#EDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#MDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "0D", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#RevSDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+0D", dateFormat: 'dd M yy'})
	$("#RevEDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-1Y", maxDate: "+0D", dateFormat: 'dd M yy'})

  });
  

$(function() {$( "#tabs" ).tabs();});
$(function() {    $( document ).tooltip();  });
</script>


<body oncontextmenu="return false;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

<table width="100%" border="0" align="center" class="tdMenu_HeadBlock">
  <tr>
    <td class="subHeader"><div align="center">View Submitted Appraisal(s)</div></td>
  </tr>
</table>
<form action="#" method="post" enctype="multipart/form-data" 
name="Records" target="_self" id="Records" autocomplete="off">
  
  <tr>
    <td align="center">
    <div class="ViewPatch">
    <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1" id="table">
    <thead>
      <tr>
        <th width="30%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Appraisal Name</span></th>
        <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Start Date</span></th>
        <th width="20%" valign="middle"  scope="col"><div align="center" class="TinyTextTightBold"><strong>End Date</strong></div></th>
        <th width="20%" align="center" valign="middle"  scope="col">Period Under Review</th>
        <th width="20%" align="center" valign="middle" scope="col">Note</th>
        <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">View</span></th>
        <th width="20%" align="center" valign="middle" scope="col"><span class="TinyTextTightBold">Download</span></th>
        </tr>
        </thead>
        <tbody>
      <?php
$Del = 0;

// $dbOpen2 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt,
// CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,
// * from [KPIStart] 
// where Status = 'A' order by SDate desc");

$dbOpen2 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt,
CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt,
* from [KPIStart] 
where Status in ('A','U' )order by SDate desc");
/*
}
else
{
$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt,* from Users where Status = 'Z' order by LogName asc");
}
*/
include '../login/dbOpen2.php';
while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
	{
	$Del = $Del + 1;
	?>
      <tr>
       
<?php $Script="Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where EmpID=(Select EmpID from Users where LogName='".$row2['AuthBy']."' and Status in('A') )"; // Gets staff full details from the Emp Table?> 
        <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo (trim($row2['AID'])); ?></td>
        <td align="center" valign="middle" class="TinyText">&nbsp;<?php echo(trim($row2['SDt']));?></td>
        <td valign="middle" align="center" class="TinyText" scope="col">&nbsp;<?php echo (trim($row2['EDt']));?></td>
        <td valign="middle" align="center" class="TinyText" scope="col"><?php echo (trim($row2['RSDt']))." - ".(trim($row2['REDt']));?></td>
        <td align="center" valign="middle" class="TinyText"><?php echo (trim($row2['ARemark']));?></td>
        
		
		<?php 
/*		
		if ($row2['Status'] == 'A')
		{	
			if (strtotime($row2['TDay']." 00:00:00") > strtotime($row2['EDt']." 23:59:59")) //if ($row2['GtD'] > $row2['EDate'])
			{
				echo "Done";
			}
			elseif (strtotime($row2['TDay']." 00:00:00") >= strtotime($row2['SDt']." 00:00:00") && strtotime($row2['TDay']." 00:00:00") <= strtotime($row2['EDt']." 23:59:59"))
			{
				echo "In Progress";
			}
			else
			{
				echo "Pending";
			}
		}
		else
		{
			echo "Unauthorized";
		}
*/

//$but_HasRth=("CREATE APPRAISAL"."A"); $but_Type = 'View'; 
//include '../main/buttons.php';

$hash = trim($row2['HashKey']);
print "<td><a href=\"KPI_Report_List_Detail_Fidson.php?hash=$hash\"><img src=\"../images/view-icon.png\"  width=\"25\" ></a></td>";
print "<td><a href=\"download_excel_report.php?hash=$hash\"><img src=\"../images/dwn_.jpg\"  width=\"25\" height=\"20\" ></a></td>";	
?>       
        </tr>
      <?php }
//include '../login/dbClose2.php';
?>
	</tbody>
    </table>
    </div>
    </td></tr>
    <tr>
      <td align="center">
<?php

/*
echo '<input name="DelMax" id="DelMax" type="hidden" value="'.$Del.'" /> <input name="ActLnk" id="ActLnk" type="hidden" value="">
      <input name="PgTy" id="PgTy" type="hidden" value="'.$_REQUEST["PgTy"].'" />
  	  <input name="PgDoS" id="PgDoS" type="hidden" value="'.DoSFormToken().'">';
	  
//Check if any record was spolled at all before enabling buttons
//Check if user has Delete rights
	//$but_HasRth=("CREATE APPRAISAL"."D"); $but_Type = 'Delete'; include '../main/buttons.php';
//Check if user has Add/Edit rights
	
//Check if user has Authorization rights
	//$but_HasRth=("CREATE APPRAISAL"."T"); $but_Type = 'Authorize'; include '../main/buttons.php';	
//Set FAction Hidden Value for each button
	echo "<input name='FAction' id='FAction' type='hidden'>";
*/

?>
    
    
</td></tr>

</form>
</tables>
<p>&nbsp;</p>
<tr></tr>
  <tr>
    <td align="center"></td>
  </tr>

<p>&nbsp;</p>

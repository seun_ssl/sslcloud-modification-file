<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>SSLCloud Report</title>
	<link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	<style>
		.options th.narrow {
			width: 150px;
		}

		.columnSelectorWrapper {
			position: relative;
			padding: 1px 6px;
			display: inline-block;
		}

		.columnSelector,
		.hidden {
			display: none;
		}

		#colSelect1:checked+label {
			color: #307ac5;
		}

		#colSelect1:checked~#columnSelector {
			display: block;
		}

		.columnSelector {
			width: 120px;
			position: absolute;
			top: 30px;
			padding: 10px;
			background: #fff;
			border: #99bfe6 1px solid;
			border-radius: 5px;
		}

		.columnSelector label {
			display: block;
			text-align: left;
		}

		.columnSelector label:nth-child(1) {
			border-bottom: #99bfe6 solid 1px;
			margin-bottom: 5px;
		}

		.columnSelector input {
			margin-right: 5px;
		}

		.columnSelector .disabled {
			color: #ddd;
		}
	</style>
	<script>
		$(function() {
			//================================ REPORT DATES ==============================================
			$("#S_RptDate").datepicker({
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: "-60Y",
				maxDate: "+1Y",
				dateFormat: 'dd M yy'
			})
			$("#E_RptDate").datepicker({
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: "-60Y",
				maxDate: "+1Y",
				dateFormat: 'dd M yy'
			})
		});
	</script>
</head>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
	<form action="#" method="post" class="form-inline">

		<div class="mb-3 d-flex">
			<div class="form-group">

				<label>From:</label>
				<?php
				if (isset($_REQUEST["S_RptDate"])) {
					echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
				} else {
					echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
				}
				?>
			</div>

			<div class="form-group">
				<label>To:</label>
				<?php
				if (isset($_REQUEST["E_RptDate"])) {
					echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
				} else {
					echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
				}
				?>
			</div>

			<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; " />


		</div>

		<?php
		$strExp = "";
		include 'rpt_header.php';
		//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
		$strExp .= "Full Name,Department,Line Manager,Leave Type,Leave Balance,Days Taken,Start Date, End Date,Resumption Date";

		$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >
<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" valign="middle" align="left">Leave Type</th>
<th data-placeholder="" valign="middle" align="left">Days Taken</th>
<th data-placeholder="" valign="middle" align="left">Leave Balance</th>
<th data-placeholder="" align="left" valign="middle">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Resumption Date</th>

</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" valign="middle" align="left">Leave Type</th>
<th data-placeholder="" valign="middle" align="left">Days Taken</th>
<th data-placeholder="" valign="middle" align="left">Leave Balance</th>
<th data-placeholder="" align="left" valign="middle">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Resumption Date</th>

</tr>
</tfoot>
<tbody>';


		if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open") &&
			(isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
		) {

			$s_date = $_POST["S_RptDate"];
			$e_date = $_POST["E_RptDate"];
			//$PrintHTML="";
			$Del = 0;
			//SIMON: PUT REPORT QUERY HERE

			//Employees on Leave
			$dbOpen2 = ("
	
	SELECT [Full Name],
       [Department],
       [Line Manager],
       [Leave Type],
       [Leave Balance],
       [Start Date],
       [End Date],
       CONVERT(VARCHAR, DATEADD(DAY, 1, [End Date]), 113) AS [Resumption Date],
       DATEDIFF(DAY, [Start Date], [End Date]) + 1 AS [Days Taken]
FROM [dbo].LeaveDetInfo
WHERE [Start Date] BETWEEN 
CONVERT(DATE, '$s_date', 113)
AND CONVERT(DATE, '$e_date', 113)
AND LvStatus IN ('A', 'PC', 'PA')
AND [Emp Status] = 'Active'
ORDER BY [Start Date] DESC;

	");

			include '../login/dbOpen2.php';
			while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
				$Del = $Del + 1;
				//SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
				//--New
				//SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
				$strExp .= chr(13) . chr(10);
				$strExp .= $row2['Full Name'] . ","
					. $row2['Department'] . ","
					. $row2['Line Manager'] . ","
					. $row2['Leave Type'] . ","
					. $row2['Days Taken'] . ","
					. $row2['Leave Balance'] . ","
					. $row2['Start Date'] . ","
					. $row2['End Date'] . ","
					. $row2['Resumption Date'];

				$PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
	<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
	<td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Days Taken'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
	<td align="left" valign="middle" scope="col">' . trim($row2['Start Date']) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
	<td align="left" valign="middle" scope="col"> ' . $row2['Resumption Date'] . '</td>
	</tr>';
			}
		}







		include '../login/dbClose2.php';
		$PrintHTML .= '</tbody></table>';
		echo $PrintHTML;
		include 'rpt_footer_min.php';
		?>
	</form>
	<?php include 'rpt_footer.php'; ?>
</body>

</html>
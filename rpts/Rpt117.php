<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';

if (isset($_POST['SubmitTrans'])) {
    $name = sanitize($_POST['emp_name']);
    $department = sanitize($_POST['department']);
    $end_date1 = sanitize($_POST['end_date1']);
    $end_date2 = sanitize($_POST['end_date2']);
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
.super_box{
	display:flex;
	justify-content:space-between;
}
.date_box{
	display:flex;
}
.input-group-btn{
	margin-top:10px;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<!-- <form action="#" method="get"> -->
<form action="" method="POST">

	<div class="super_box">
		<div class="row">

			<label  class="col-sm-4 col-form-label">Select an Employee:</label>
			<div class="input-group input-group-sm col-8 pull-right" >
											<select name="emp_name" class="form-control select2" id="emp_name">
												<?php
// $AllEmployee='';
echo '<option value="" selected="selected">--</option>';

$dbOpen2 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

include '../login/dbOpen2.php';
//while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if (isset($_POST['emp_name']) && $_POST['emp_name'] === $row2['EmpID']) {
        echo '<option value="' . $row2['EmpID'] . '"  selected >' . $row2['SName'] . " " . $row2['FName'] . " [" . $row2['EmpID'] . "]" . '</option>';
    } else {
        echo '<option value="' . $row2['EmpID'] . '">' . $row2['SName'] . " " . $row2['FName'] . " [" . $row2['EmpID'] . "]" . '</option>';
    }

}

include '../login/dbClose2.php';
//if (isset($EditID) && $EditID != '' && $EditID != '--')

?>
											</select>

										</div>


		</div>
		<div class="form-group row">
							<label  class="col-sm-4 col-form-label">Department:</label>
							<div class="col-sm-8 input-group input-group-sm" style="<?php if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"] == "ModifySelf") {echo ' background: #e9e9e9; padding: 5px ';}?>">

								<select name="department" id="department" class="form-control">
									<option value="" selected="selected">--</option>
									<?php

$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Department'  and Status<>'D' and Val1<>'') ORDER BY Val1");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if (isset($_POST['department']) && $_POST['department'] === $row2['Val1']) {echo '<option selected value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';} else {echo '<option value="' . $row2['Val1'] . '">' . $row2['Val1'] . '</option>';}
    // echo '<option value="'.$row2['Val1'].'" >'.$row2['Val1'].'</option>';
}
include '../login/dbClose2.php';
?>
								</select>

							</div>
		</div>
		<div>
		<label  class="col-sm-4 col-form-label">Select an End Date range:</label>
			<div  class="date_box" >
				<div class="row">
				<input name="end_date1" id="end_date1" type="text" class="form-control" value="<?php echo isset($_POST['end_date1']) ? $_POST['end_date1'] : "" ?>"  readonly="readonly" />
				-

				</div>

				<div class="row">
			-
				<input name="end_date2" id="end_date2" type="text" class="form-control" value="<?php echo isset($_POST['end_date2']) ? $_POST['end_date2'] : "" ?>"   readonly="readonly" />
				</div>
			</div>
		</div>
		<div class="input-group-btn">
			<span >
				<button type="submit" class="btn btn-danger" name="SubmitTrans" id="SubmitTrans">Submit</button>
			</span>
		</div>
	</div>

</form>
<!-- default -->
<?php if (!isset($_POST['emp_name']) && !isset($_POST['department']) && !isset($_POST['end_date1']) && !isset($_POST['end_date2'])): ?>

<?php

$strExp = "Selected Employee=Default   Department= Default     EndDate = Default---Default" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>



</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
    . $row2['End Date'] . ","
    . $differ . ","
    . $row2['Total Leave'] . ","
    . $row2['Leave Balance'] . ","
    . $row2['SubEmp'] . ","
    . $LvStatus;
       
       

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
    <td align="left" valign="middle" scope="col">' . $differ . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
    <td align="left" valign="middle" scope="col">' . $LvStatus . '</td>




</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>




<!-- TTTT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);

include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
    . $row2['End Date'] . ","
    . $differ . ","
    . $row2['Total Leave'] . ","
    . $row2['Leave Balance'] . ","
    . $row2['SubEmp'] . ","
    . $LvStatus;

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>



</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>




<!-- HTTT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";
$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
    . $row2['End Date'] . ","
    . $differ . ","
    . $row2['Total Leave'] . ","
    . $row2['Leave Balance'] . ","
    . $row2['SubEmp'] . ","
    . $LvStatus;

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>



</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>





<!-- THTT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
        . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        .$row2['Leave Balance'] 
        . $row2['SubEmp'] . ","
        . $LvStatus;
     
        

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td> 
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>



<!-- TTHT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "') AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","    
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
    . $row2['End Date'] . ","
    . $differ . ","
    . $row2['Total Leave'] . ","
    . $row2['Leave Balance'] . ","
    . $row2['SubEmp'] . ","
    . $LvStatus;
        
        

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
    <td align="left" valign="middle" scope="col">' . $differ . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
    <td align="left" valign="middle" scope="col">' . $LvStatus . '</td>




</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>




<!-- TTTH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);

include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . "," 
    . $row2['AddedDate']->format('j M Y') . ","    
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
    . $row2['End Date'] . ","
    . $differ . "," 
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus;

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>



<!-- HTTH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","  
        . $row2['Leave Type'] . "," 
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus;

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>



<!-- THHT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "') AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>





<!-- HTHT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "') AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . "," 
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","  
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . "," 
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td> 
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>





<!-- THTH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","  
        . $row2['Leave Type'] . ","    
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>







<!-- TTHH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "')  AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . "," 
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus;

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>





<!-- HHTT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . "," 
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>







<!-- THHH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] === "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "') AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        .$LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>




<!-- HTHH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] === "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "') AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","    
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>





<!-- HHTH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] === "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("

SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR(DATEADD(YEAR,-5,GETDATE())) AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]

");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
    . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","  
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>


<!-- HHHT -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] === "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("
SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "')  AND GETDATE()
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]
");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
        . $row2['Leave Type'] . ","
        . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] . ","
        . $row2['SubEmp'] . ","
        . $LvStatus; 

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>

</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>



<!-- HHHH -->
<?php if (
    (isset($_POST['emp_name']) && $_POST['emp_name'] !== "")
    && (isset($_POST['department']) && $_POST['department'] !== "")
    && (isset($_POST['end_date1']) && $_POST['end_date1'] !== "")
    && (isset($_POST['end_date2']) && $_POST['end_date2'] !== "")): ?>

<?php

$strExp = "Selected Employee=" . sanitize($_POST['emp_name']) . "  Department=" . sanitize($_POST['department']) . "    EndDate = " . sanitize($_POST['end_date1']) . "---" . sanitize($_POST['end_date2']) . "" . chr(13) . chr(10);

include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Line Manager,Applied On,Time Off Type,Start Date,End Date,Days,No of Leave In a Year,Days Left,Back up Employee,Leave Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Applied On</th>
<th data-placeholder="" valign="middle" align="left">Time Off Type</th>
<th data-placeholder="" valign="middle" align="left">Start Date</th>
<th data-placeholder="" valign="middle" align="left">End Date</th>
<th data-placeholder="" valign="middle" align="left">Days</th>
<th data-placeholder="" valign="middle" align="left">No of Leave In a Year</th>
<th data-placeholder="" valign="middle" align="left">Days Left</th>
<th data-placeholder="" align="left" valign="middle">Back up Employee</th>
<th data-placeholder="" valign="middle" align="left">Leave Status</th>


</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//Employees on Leave

$dbOpen2 = ("
SELECT *
FROM [dbo].[LeaveDetInfo3]
WHERE [Emp Status] = 'Active'
AND [Employee ID] = '" . $name . "'
AND [Department] = '" . $department . "'
AND YEAR ([End Date]) BETWEEN YEAR('" . $end_date1 . "')  AND YEAR('" . $end_date2 . "')
AND   [LvStatus] IN ('A','PA','PC','C')
ORDER BY [LvStatus] ,[EDate]
");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    if ($row2['LvStatus'] == 'A') {$LvStatus = 'Approved';} else { $LvStatus = 'Not Approved';}
    if ($row2['LvStatus'] == 'C') {$LvStatus = 'Cancelled';}
    if ($row2['LvStatus'] == 'PC') {$LvStatus = 'Pending Cancellation';}
    if ($row2['LvStatus'] == 'PA') {$LvStatus = 'Pending Approval';}

    $first_date = new DateTime($row2['Start Date']);
    $second_date = new DateTime($row2['End Date']);
    $difference = $first_date->diff($second_date);
    $differ = $difference->d + 1;
    // var_dump(    $differ  );

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
    . $row2['Department'] . ","
        . $row2['Line Manager'] . ","
    . $row2['AddedDate']->format('j M Y') . ","
    . $row2['Leave Type'] . ","
    . $row2['Start Date'] . ","
        . $row2['End Date'] . ","
        . $differ . ","
        . $row2['Total Leave'] . ","
        . $row2['Leave Balance'] 
    . $row2['SubEmp'] . ","
        . $LvStatus;
        
       
       

    $PrintHTML .= '<tr><td height="20" td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AddedDate']->format('j M Y') . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Type'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Start Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['End Date'] . '</td>
<td align="left" valign="middle" scope="col">' . $differ . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Total Leave'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Leave Balance'] . '</td> 
<td align="left" valign="middle" scope="col">' . $row2['SubEmp'] . '</td>
<td align="left" valign="middle" scope="col">' . $LvStatus . '</td>


</tr>';
}

include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
<!-- </form> -->
<?php include 'rpt_footer.php';?>




<?php endif;?>
















<script>
	$("#end_date1").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, dateFormat: 'dd M yy',yearRange: "-75:+75"});
	$("#end_date2").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, dateFormat: 'dd M yy',yearRange: "-75:+75"});
</script>






</body>
</html>
<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php

$strExp = "";
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Name of Branch,Academic Male,Academic Female,Non-Academic Male,Non-Academic Female,Admin Male,Admin Female,Total Male,Total Female,Total Staff";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >


<th data-placeholder="" align="left" valign="middle">Name of Branch</th>
<th data-placeholder="" align="left" valign="middle">Academic Male</th>
<th data-placeholder="" align="left" valign="middle">Academic Female</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Male</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Female</th>
<th data-placeholder="" align="left" valign="middle">Admin Male</th>
<th data-placeholder="" align="left" valign="middle">Admin Female</th>
<th data-placeholder="" align="left" valign="middle">Total Male</th>
<th data-placeholder="" align="left" valign="middle">Total Female</th>
<th data-placeholder="" align="left" valign="middle">Total Staff</th>


</tr>
</thead>

<tbody>';
//$PrintHTML="";
$Del = 0;
$aca_male_tot = 0;
$aca_female_tot = 0;
$nonaca_male_tot = 0;
$nonaca_female_tot = 0;
$adm_male_tot = 0;
$adm_female_tot = 0;
$tot_male_tot = 0;
$tot_female_tot = 0;
$all_all_tot = 0;

//SIMON: PUT REPORT QUERY HERE

//
// $Script="Select top(1) HashKey from KPIStart where SDate <=GETDATE() and Status='A' order by SDate Desc";

$dbOpen2 = ("
select e.BranchID, max(b.OName) branch_name, count(*) total from EmpTbl e
  INNER JOIN BrhMasters b on e.BranchID = b.HashKey
  where b.Status not in('U','D')
  and e.EmpStatus in('Active')
  and e.Sex in('Male','Female')
  group by e.BranchID
");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    $regrouping = [];
    $regrouping_sex = [];
    if (is_numeric((trim($row2['total'])))) {
        $all_all_tot = $all_all_tot + (int) trim($row2['total']);
    } else {
        $all_all_tot += 0;

    }

    $dbOpen3 = "select Sex,EmpCategory,count(EmpCategory)count_tot from EmpTbl where BranchID ='{$row2['BranchID']}' and Sex in('Male','Female') and EmpStatus in('Active') group by EmpCategory,Sex";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
        $regrouping[] = $row3;
    }
    include '../login/dbClose3.php';

    $dbOpen7 = "select Sex, count(Sex) sex_count from EmpTbl where BranchID ='{$row2['BranchID']}' and Sex in('Male','Female') and EmpStatus in('Active') group by Sex";
    include '../login/dbOpen7.php';
    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_ASSOC)) {
        $regrouping_sex[] = $row7;
    }
    include '../login/dbClose7.php';

    // var_dump($regrouping);
    // var_dump($regrouping_sex);
    // var_dump($row2['total']);
    // echo "<hr/>";
    $aca_male = 0;
    $aca_female = 0;
    $nonaca_male = 0;
    $nonaca_female = 0;
    $adm_male = 0;
    $adm_female = 0;
    $tot_male = 0;
    $tot_female = 0;

    foreach ($regrouping_sex as $value) {
        if ($value['Sex'] === 'Male') {
            $tot_male = $value['sex_count'];
            if (is_numeric($tot_male)) {
                $tot_male_tot = $tot_male_tot + (int) $tot_male;
            } else {
                $tot_male_tot += 0;

            }

        }
        if ($value['Sex'] === 'Female') {
            $tot_female = $value['sex_count'];
            if (is_numeric($tot_female)) {
                $tot_female_tot = $tot_female_tot + (int) $tot_female;
            } else {
                $tot_female_tot += 0;

            }

        }
    }
    foreach ($regrouping as $value) {
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['Sex'] === 'Male') {
            $aca_male = $value['count_tot'];
            if (is_numeric($aca_male)) {
                $aca_male_tot = $aca_male_tot + (int) $aca_male;
            } else {
                $aca_male_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['Sex'] === 'Female') {
            $aca_female = $value['count_tot'];
            if (is_numeric($aca_female)) {
                $aca_female_tot = $aca_female_tot + (int) $aca_female;
            } else {
                $aca_female_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['Sex'] === 'Male') {
            $nonaca_male = $value['count_tot'];
            if (is_numeric($nonaca_male)) {
                $nonaca_male_tot = $nonaca_male_tot + (int) $nonaca_male;
            } else {
                $nonaca_male_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['Sex'] === 'Female') {
            $nonaca_female = $value['count_tot'];
            if (is_numeric($nonaca_female)) {
                $nonaca_female_tot = $nonaca_female_tot + (int) $nonaca_female;
            } else {
                $nonaca_female_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['Sex'] === 'Male') {
            $adm_male = $value['count_tot'];
            if (is_numeric($adm_male)) {
                $adm_male_tot = $adm_male_tot + (int) $adm_male;
            } else {
                $adm_male_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['Sex'] === 'Female') {
            $adm_female = $value['count_tot'];
            if (is_numeric($adm_female)) {
                $adm_female_tot = $adm_female_tot + (int) $adm_female;
            } else {
                $adm_female_tot += 0;

            }

        }
    }

    $strExp .= chr(13) . chr(10);
    $strExp .= (trim($row2['branch_name'])) . ","
        . $aca_male . ","
        . $aca_female . ","
        . $nonaca_male . ","
        . $nonaca_female . ","
        . $adm_male . ","
        . $adm_female . ","
        . $tot_male . ","
        . $tot_female . ","
        . (trim($row2['total']));

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['branch_name'])) . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_male . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_female . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_male . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_female . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_male . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_female . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_male . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_female . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['total']) . '</td>

</tr>';
}
include '../login/dbClose2.php';

$strExp .= chr(13) . chr(10);
$strExp .= "Total,"
    . $aca_male_tot . ","
    . $aca_female_tot . ","
    . $nonaca_male_tot . ","
    . $nonaca_female_tot . ","
    . $adm_male_tot . ","
    . $adm_female_tot . ","
    . $tot_male_tot . ","
    . $tot_female_tot . ","
    . $all_all_tot;
$PrintHTML .= '
<tfoot>
<tr >


<th data-placeholder="" align="left" valign="middle">Total</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_male_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_female_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_male_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_female_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_male_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_female_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_male_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_female_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $all_all_tot . '</th>


</tr>
</tfoot>';

$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
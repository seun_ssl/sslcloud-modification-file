<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php
$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Branch Name,Department,Category,NIN Number,Line Manager,Gender,Marital Status,Employement Status,Status";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employement Status</th>
<th data-placeholder="" align="left" valign="middle">Status</th>

</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employement Status</th>
<th data-placeholder="" align="left" valign="middle">Status</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//
$dbOpen2 = ("

SELECT e.[Full Name]
      ,e.[Department]
      ,e.[Line Manager]
      ,e.[Sex]
      ,e.[Marital Status]
      ,e.[Emp Status]
	  ,e.[Status]
	  ,e.[EmpCategory]
	  ,e.[NIN]
	  ,b.OName branch_name
FROM [dbo].[EmpInfo] e
INNER JOIN BrhMasters b on e.BranchID = b.HashKey
WHERE e.[Status] <>'D'
AND e.[HashKey] <> '057646662de2ee305tr6de611ac0d001'
AND e.[HashKey] <> 'f146e0c756af0217c9bf6e4f0f2ac9ee'
AND b.Status not in('U','D')
ORDER BY e.[Full Name]



");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    ////if ($row2['Status']=='A') {$Status='Authorized';} else {$Status='No';}
    if ($row2['Status'] == 'A') {$Status = 'Authorized';}
    if ($row2['Status'] == 'U') {$Status = 'Un-Authorized';}
    if ($row2['Status'] == 'N') {$Status = 'Un-Authorized';}

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
        . $row2['branch_name'] . ","
        . $row2['Department'] . ","
        . $row2['EmpCategory'] . ","
        . $row2['NIN'] . ","
        . $row2['Line Manager'] . ","
        . $row2['Sex'] . ","
        . $row2['Marital Status'] . ","
        . $row2['Emp Status'] . ","
        . $Status;

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['branch_name']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['EmpCategory']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['NIN']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Sex']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Marital Status']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Emp Status'] . '</td>
<td align="left" valign="middle" scope="col">' . $Status . '</td>
</tr>';
}
include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
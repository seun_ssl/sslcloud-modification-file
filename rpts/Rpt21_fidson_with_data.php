﻿<?php 
	session_start();
	include '../login/scriptrunner.php';
	$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';
	//Validate user viewing rights
	if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Manager") {
		if (ValidateURths("TEAM APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
	}
	elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Personal") {
		if (ValidateURths("APPRAISAL"."T")!=true){include '../main/NoAccess.php';exit;}
	}
	elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Auth") {
		if (ValidateURths("CREATE APPRAISAL"."V")!=true){include '../main/NoAccess.php';exit;}
	}
	if (!isset($KPICount)){$KPICount=0;}
	if (!isset($EditID)){$EditID="";}
	if (!isset($Script_Edit)){$Script_Edit="";}
	if (!isset($HashKey)){$HashKey="";}
	if (!isset($TWeightage)){$TWeightage=0;}
	if (!isset($GpNameHash)){$GpNameHash="";}
	if (!isset($ChkKPIVal)){$ChkKPIVal="";}
	if (!isset($total)){$total=0;}
	if (!isset($total_final)){$total_final=0;}
	if (!isset($EmpStrenght)){$EmpStrenght="";}
	if (!isset($EmpWeakness)){$EmpWeakness="";}
	if (!isset($EmpPerfGap)){$EmpPerfGap="";}
	if (!isset($EmpStrenght)){$EmpStrenght="";}
	if (!isset($MRecommendation)){$MRecommendation="";}
	if (!isset($EComment)){$EComment="";}
	if (!isset($MComment)){$MComment="";}
	if (!isset($AprsComment3)){$AprsComment3="";}
	if (!isset($AprsComment4)){$AprsComment4="";}
	if (!isset($AprsComment5)){$AprsComment5="";}
	//if (!isset($main_total)){$main_total=0;}
	if (!isset($Del)){$Del=0;}
	//$appr_name = md5($row2['AID']);
	if (isset($_REQUEST["apprs"])) {
		$appr_qry="SELECT * FROM KPIStart WHERE HashKey = '".$_REQUEST["apprs"]."' and Status <> 'D' ";
		$appr_name = ScriptRunner($appr_qry,"AID");
	} else {
		$appr_name = "";
	}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }

    </style>
    <script type="text/javascript">
        function MM_swapImgRestore() {
            var i, x, a = document.MM_sr;
            for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
        }

        function MM_preloadImages() {
            var d = document;
            if (d.images) {
                if (!d.MM_p) d.MM_p = new Array();
                var i, j = d.MM_p.length,
                    a = MM_preloadImages.arguments;
                for (i = 0; i < a.length; i++)
                    if (a[i].indexOf("#") != 0) {
                        d.MM_p[j] = new Image;
                        d.MM_p[j++].src = a[i];
                    }
            }
        }

        function MM_findObj(n, d) {
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }

        function MM_swapImage() { //v3.0
            var i, j = 0,
                x, a = MM_swapImage.arguments;
            document.MM_sr = new Array;
            for (i = 0; i < (a.length - 2); i += 3)
                if ((x = MM_findObj(a[i])) != null) {
                    document.MM_sr[j++] = x;
                    if (!x.oSrc) x.oSrc = x.src;
                    x.src = a[i + 2];
                }
        }

    </script>
</head>
<?php
		if (isset($_REQUEST["PgDoS"]) && isset($_SESSION['DoS'.$_REQUEST["PgDoS"]]) && $_SESSION['DoS'.$_REQUEST["PgDoS"]] == md5('DoS'.$_SESSION["StkTck"."UName"].$_REQUEST["PgDoS"]) && strlen(DoSFormToken())==32) {
			unset($_SESSION['DoS'.$_REQUEST["PgDoS"]]);
			if (isset($_REQUEST["SubmitTrans"]) && $_REQUEST["SubmitTrans"] == "Open") {
				$OpenID = $_REQUEST["GrpName"];
				$Script="Select * from KPISetting where HashKey='".ECh($_REQUEST["GrpName"])."'";
				//echo ScriptRunner($Script,"AName");
				$KPICount = ScriptRunner($Script,"AFieldCnt");
				$Script_Emp = "Select (E.SName + ' ' + E.FName) as Nm, Email, (Select (Et.Email), * from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgrEmail, (Select (Et.SName + ' ' + Et.FName) from EmpTbl Et where Et.HashKey=E.EmpMgr) as EmpMgr from EmpTbl E where HashKey=(Select AName from KPISetting where HashKey='".ECh($_REQUEST["GrpName"])."')";
				$name1 = ScriptRunner($Script_name1,"Nm");
				$EmpID = ScriptRunner($Script_name1,"EmpID");
				$EmpMgr = ScriptRunner($Script_name1,"EmpMgr");
				$EmpAddedBy = ScriptRunner($Script_name1,"AddedBy");
				$EmpHashKey = ScriptRunner($Script_name1,"HashKey");
				$emp_dept = ScriptRunner($Script_name1,"Department");
				//Employee Detail
				$Script_dtl="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".ECh($EmpHashKey)."'";
				$emp_job_title = ScriptRunner($Script_dtl,"ItemPos");
				//$emp_dept = ScriptRunner($Script_dtl,"Department");
				
				//Manager Detail
				$Script_dtl2="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".$EmpMgr."'";
				$mgr_job_title = ScriptRunner($Script_dtl2,"ItemPos");
				
				//$Script_dtl2="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".ECh($EmpHashKey)."' ORDER  BY ID DESC";
				//$mgr_job_title = ScriptRunner($Script_dtl2,"ItemPos");
				
				//Employee Manager
				$Script_name2="Select (SName+' '+FName+' '+ONames) Nm, * from [EmpTbl] where HashKey='".$EmpMgr."'";
				$mgr_name = ScriptRunner($Script_name2,"Nm");
			}
		}
		/*
		if(isset($_REQUEST["AcctNo"])){
				
			$Script_name1="Select (SName+' '+FName+' '+ONames) Nm, * from [EmpTbl] where HashKey='".$_REQUEST["AcctNo"]."'";
			$name1 = ScriptRunner($Script_name1,"Nm");
			$EmpID = ScriptRunner($Script_name1,"EmpID");
			$EmpMgr = ScriptRunner($Script_name1,"EmpMgr");
				$EmpAddedBy = ScriptRunner($Script_name1,"AddedBy");
			
			//Employee Detail
			$Script_dtl="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".$_REQUEST["AcctNo"]."'";
			$emp_job_title = ScriptRunner($Script_dtl,"ItemPos");
			
			//Manager Detail
			$Script_dtl2="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".$EmpMgr."'";
			$mgr_job_title = ScriptRunner($Script_dtl2,"ItemPos");
			
			//Employee Manager
			$Script_name2="Select (SName+' '+FName+' '+ONames) Nm, * from [EmpTbl] where HashKey='".$EmpMgr."'";
			$mgr_name = ScriptRunner($Script_name2,"Nm");
			
			
			
			//$Script_name2="Select (SName+' '+FName+' '+ONames) Nm from [EmpTbl] where HashKey='".$AprsID2."'";
			//$name2 = ScriptRunner($Script_name2,"Nm");
		}
		*/
	?>

<body leftmargin="0" topmargin="0" onLoad="MM_preloadImages('../images/bi_butn/Search.jpg')"
    oncontextmenu="return false;">
    <form action="#" method="get">
        <?php
				$Script_Menu="Select SetValue, SetValue2, SetValue3, Status from Settings where Setting='CompLogo'";
				$ComLogo=ScriptRunner($Script_Menu,"SetValue");
				if (strlen($ComLogo)==36)
				{$ComLogo="../pfimg/".$_SESSION["StkTck"."CustID"]."/".$ComLogo;}
				else
				{$ComLogo="../images/InterimLogo.png";}
			?>
        <div class="pagelogo">
            <?php
				if (!isset($wt)){$wt="";}
				if (ScriptRunner($Script_Menu,"SetValue3")>55){$ht= ' height="55" ';} else {$ht= ' height="'.ScriptRunner($Script_Menu,"SetValue3").'" '; $wt="";}
				if (ScriptRunner($Script_Menu,"SetValue2")>150){$wt= ' width="150" ';}// else {$wt= ' width="'.ScriptRunner($Script_Menu,"SetValue2").'" ';}
				print "<img id='PgLogoDisplay' name='PgLogoDisplay' src='$ComLogo' $ht . $wt>";
				?>
        </div>
        <table width="100%" border="0">
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tr>
                            <td width="75%" align="right">
                                <?php
										if (isset($_REQUEST["apprs"])){
											$OpenID3 = $_REQUEST["apprs"];
											//print $_REQUEST["apprs"];
											//print "<br/>";
											//print $_REQUEST["GrpName"];
										}
										else{
											$OpenID3 = "";
										}
									?>
                                <select name="apprs" class="dropdown" id="apprs">
                                    <?php
											echo '<option value="--" selected="selected">--</option>';
											$dbOpen3 = ("SELECT DATEDIFF(D,Getdate(),EDate) GtD, Convert(Varchar(11),GetDate(),106) TDay, Convert(Varchar(11),AddedDate,106) as Dt, CONVERT(Varchar(11),SDate,106) SDt, CONVERT(Varchar(11),EDate,106) EDt,
											CONVERT(Varchar(11),RevSDate,106) RSDt, CONVERT(Varchar(11),RevEDate,106) REDt, * from [KPIStart]
											where Status = 'A' order by SDate desc");
											include '../login/dbOpen3.php';
											while( $row3 = sqlsrv_fetch_array($result3,SQLSRV_FETCH_BOTH)) {
												if ($OpenID3==$row3['HashKey']) {
													if ($OpenID3 != '' && $OpenID3 != '--') {
														echo '<option selected value="'.$row3['HashKey'].'"> <b>' . $row3['AID'] . '</b> (from '.$row3['SDt'].' to '.$row3['EDt'].')</option>';
														//$the_id=$row2['HashKey'];
													}
												} else {
												echo '<option value="' . trim($row3['HashKey']) . '"> <b>' . $row3['AID'] . '</b> (from '.$row3['SDt'].' to '.$row3['EDt'].')</option>';
												}
											}
											include '../login/dbClose3.php';
										?>
                                </select>
                            </td>
                            <td width="9%">
                                <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                    <tr>
                                        <td width="84%" align="right">
                                            <select name="GrpName" class="TextBoxText" id="GrpName">
                                                <?php
														echo '<option value="--" selected="selected">--</option>';
														if (isset($_REQUEST["GrpName"]))
														{$OpenID = $_REQUEST["GrpName"];}
														else
														{$OpenID = "";}
															//======================================================================================
															//===================== SELECTING EMPLOYEES ============================================
															//======================================================================================
															/*
															if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Manager")
															{
																if (ValidateURths("CREATE APPRAISAL"."A")==true)
																{
																	$dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A','U','N')and AType='T' order by AName Asc");
																}
																else
																{
																	$dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('Z')and AType='T' order by AName Asc");
																}
															}
															elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Auth")
															{*/
															$dbOpen2 = ("SELECT AName as Nm, HashKey from KPISetting where Status in ('A')and AType='T' order by AName Asc");
															/*
															}
															*/
															include '../login/dbOpen2.php';
															while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
																if ($OpenID==$row2['HashKey']) {
																	if ($OpenID != '' && $OpenID != '--') {
																		echo '<option selected value="'.$row2['HashKey'].'">['.strtoupper($row2['Nm']).']</option>';
																		//$the_id=$row2['HashKey'];
																	}
																} else {
																	echo '<option value="'.$row2['HashKey'].'">'.$row2['Nm'].'</option>';
																}
															}
															include '../login/dbClose2.php';
															//======================================================================================
															//===================== SELECTING EMPLOYEES ============================================
															//======================================================================================
															/*
															if (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Manager")
															{
																$dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Ks.HashKey from KPISetting Ks, EmpTbl Et where Et.Status in ('A','U','N') and Ks.Status<>'D' and Ks.AName=Et.HashKey and EmpMgr=(Select HashKey from EmpTbl where EmpID=(Select EmpID from Users where LogName='".$_SESSION["StkTck"."UName"]."')) order by Et.SName Asc");
															}
															elseif (isset($_REQUEST["PgTy"]) && $_REQUEST["PgTy"]=="Auth")
															{
																*/
																$dbOpen2 = ("SELECT (Et.SName + ' ' + Et.FName + ' ' + Et.ONames+' [' + Et.EmpID + ']') as Nm, Et.HashKey as EmpHashKey, Ks.* from KPISetting Ks, EmpTbl Et where Ks.Status<>'D' and Et.Status in ('A') and Ks.AName=Et.HashKey order by Et.SName Asc");
															//}
															include '../login/dbOpen2.php';
															while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
																{
																	if ($OpenID==$row2['HashKey'])
																	{
																		if ($OpenID != '' && $OpenID != '--'){
																			echo '<option selected value="'.$row2['HashKey'].'">'.$row2['Nm'].'</option>';
																			$the_id=$row2['EmpHashKey'];
																		}
																	}
																	else
																	{echo '<option value="'.$row2['HashKey'].'">'.$row2['Nm'].'</option>';}
																}
															include '../login/dbClose2.php';
													?>
                                            </select>
                                        </td>
                                        <td width="16%"><input name="SubmitTrans" type="submit" class="smallButton"
                                                id="SubmitTrans" value="Open" /></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="16%"><a href="javascript:window.print()"><img src="../images/icon_print_.gif"
                                        width="25" height="17"></a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#FCFCFC" style="font-size:10px"><strong><?php print strtoupper($appr_name); ?></strong>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td width="14%" style="font-size:10px"><strong>Name:</strong></td>
                            <td width="35%" style="font-size:10px">
                                <?php
										if(isset($the_id)) {
											$emp_hk=$the_id;
											$Script_name1="Select (SName+' '+FName+' '+ONames) Nm, * from [EmpTbl] where HashKey='".$emp_hk."'";
											$Script_mgr1="SELECT * FROM KPIFinalScore WHERE AQuartID = '".$_REQUEST["apprs"]."' and EID='".$emp_hk."' and Status <> 'D' ";
											$name1 = ScriptRunner($Script_name1,"Nm");
											$EmpID = ScriptRunner($Script_name1,"EmpID");
											$EmpMgr = ScriptRunner($Script_mgr1,"MID");
											$EmpAddedBy = ScriptRunner($Script_name1,"AddedBy");
											$emp_dept = ScriptRunner($Script_name1,"Department");
											//Employee Detail
											$Script_dtl="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".$emp_hk."'";
											$emp_job_title = ScriptRunner($Script_dtl,"ItemPos");													
											//Manager Detail
											$Script_dtl2="Select * from [EmpDtl] where ItemCode='EMP' AND ItemStatus='Confirmed' AND StaffID='".$EmpMgr."'";
											$mgr_job_title = ScriptRunner($Script_dtl2,"ItemPos");												
											//Employee Manager
											$Script_name2="Select (SName+' '+FName+' '+ONames) Nm, * from [EmpTbl] where HashKey='".$EmpMgr."'";
											$mgr_name = ScriptRunner($Script_name2,"Nm");												
										}
										if(isset($name1)) {
												print $name1;
										}else {
											print "";
										}
									?>
                            </td>
                            <td width="13%" style="font-size:10px"><strong>Staff ID:</strong></td>
                            <td width="38%" style="font-size:10px">
                                <?php
										if (isset($EmpID)) {
											print $EmpID;
										} else {
											print "";
										}
									?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FCFCFC" style="font-size:10px"><strong>Job Title:</strong></td>
                            <td bgcolor="#FCFCFC" style="font-size:10px">
                                <?php
										if(isset($emp_job_title)) {
											print $emp_job_title;
										} else {
											print "";
										}
									?>
                                </span>
                            </td>
                            <td bgcolor="#FCFCFC" style="font-size:10px"><span style="font-size:10px"><strong>Line
                                        Manager:</strong></span></td>
                            <td bgcolor="#FCFCFC" style="font-size:10px"><span style="font-size:10px">
                                    <?php
										if(isset($mgr_name)) {
											print $mgr_name;
										} else {
											print "";
										}
									?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:10px"><strong>Function:</strong></td>
                            <td style="font-size:10px">
                                <?php
										if(isset($emp_dept)) {
											print $emp_dept;
										} else {
											print "";
										}
									?>
                            </td>
                            <td style="font-size:10px"><strong>Job Title:</strong></td>
                            <td style="font-size:10px"><span style="font-size:10px">
                                    <?php
										if(isset($mgr_job_title)) {
											print $mgr_job_title;
										} else {
											print "";
										}
									?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FCFCFC" style="font-size:10px"><strong>Location:</strong></td>
                            <td bgcolor="#FCFCFC" style="font-size:10px">&nbsp;</td>
                            <td bgcolor="#FCFCFC" style="font-size:10px">&nbsp;</td>
                            <td bgcolor="#FCFCFC" style="font-size:10px">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <?php
								if(isset($_REQUEST["GrpName"])) {
									if (isset($_REQUEST["apprs"])) {
										//$OpenID3 = $_REQUEST["apprs"];
										//print $_REQUEST["apprs"];
										//print "<br/>";
										//print $_REQUEST["GrpName"];
										$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE AQuartID = '".$_REQUEST["apprs"]."' and AID='".$_REQUEST["GrpName"]."' and Status <> 'D' ORDER BY KRA_Group");
									} else {
											$dbOpen2 = ("SELECT Convert(Varchar(11),AddedDate,106) as Dt, * from KPICore where Status = 'A'  and AID='".$_REQUEST["GrpName"]."' order by KRA_Group");
									}
									include '../login/dbOpen2.php';
									print "
										<tr>
										<td width=\"35%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Area of Focus</b></td>
										<td width=\"35%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Sub Area of Focus</b></td>
										<td width=\"25%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Departmental Objectives</b></td>
											<td width=\"25%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Individual Performance Objective</b></td>
											<td width=\"20%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Employee Comment</b></td>
											<td width=\"20%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Supervisor's Comment</b></td>
											<td width=\"10%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Weight</b></td>
											<td width=\"10%\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><b>Final Score</b></td>
										</tr>
									";
									$prevValue = "";
									while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
										$KRA_Group_value = $row2['KRA_Group'];
										if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)) {
											print "
												<tr>
													<td colspan=\"6\" bgcolor=\"#FCFCFC\" style=\"font-size:10px\"><strong><font color=\"#00CC00\">$KRA_Group_value</font></strong></td>
												</tr>
											";
											$prevValue = $KRA_Group_value;
										}
										$obj = "<b>".(trim($row2['KRA']))."</b>";
										$obj_sub = "<b>".(trim($row2['KRA_Sub_Area']))."</b>";
										$artT = "<b>".(trim($row2['ARemark']))."</b>";
										$measurement  = $row2['KPI'];
										$weight  = $row2['Weightage'];
										$AScoreFinal  = $row2['AScoreFinal'];
										$EComm  = $row2['EComm'];
										$MComm  = $row2['MComm'];
										$FinalHashKey  = $row2['FinalHashKey'];
										$total_final = $total_final+$AScoreFinal;
										$total=$total+$weight;
										//Get the Summary Comments
										$Script="Select * from KPIFinalScore WHERE HashKey ='".$FinalHashKey."' and Status <> 'D'";
										$EmpStrenght=ScriptRunner($Script,"EmpStrenght");
										$EmpWeakness=ScriptRunner($Script,"EmpWeakness");
										$EmpPerfGap=ScriptRunner($Script,"EmpPerfGap");
										$EmpStrenght=ScriptRunner($Script,"EmpStrenght");
										$MRecommendation=ScriptRunner($Script,"MRecommendation");
										$EComment=ScriptRunner($Script,"EComment");
										$MComment=ScriptRunner($Script,"MComment");
										$AprsComment3=ScriptRunner($Script,"AprsComment3");
										$AprsComment4=ScriptRunner($Script,"AprsComment4");
										$AprsComment5=ScriptRunner($Script,"AprsComment5");
									?>
                        <?php
										print "
											<tr>
												<td style=\"font-size:10px\">$obj</td>
												<td style=\"font-size:10px\">$obj_sub</td>
												<td style=\"font-size:10px\">$measurement</td>
												<td style=\"font-size:10px\">$artT</td>
												<td style=\"font-size:10px\">$EComm</td>
												<td style=\"font-size:10px\">$MComm</td>
												<td style=\"font-size:10px\">$weight</td>
												<td style=\"font-size:10px\">$AScoreFinal</td>
											</tr>
										";
									}
								}
								print "
									<tr>
										<td colspan=\"6\" align=\"right\"><b>Total</b></td>
										<td style=\"font-size:10px\"><b>$total</b></td>
										<td style=\"font-size:10px\"><b>$total_final</b></td>
									</tr>
								";
							?>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#FCFCFC"><span style="font-size:10px"><strong>
                            <font color="#00CC00">SECTION THREE: Employee Strength(s) and Weakness(es)</font>
                        </strong></span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td width="50%" bgcolor="#FCFCFC" style="font-size:10px">
                                <strong>
                                    <?php
											//Loop through the Header
											$dbOpen2 = ("SELECT [Val1] FROM Masters where (ItemName='Appraisal_Header') AND Status<>'D' ORDER BY Val1");
											include '../login/dbOpen2.php';
											$coun=0;
											while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
												$header[$coun]=$row2['Val1'];
												$coun++;
											}
											if(isset($header[0])) {
												print $header[0];
											} else {
												print " State Employee's Strength(s)";
											}
										?>
                                </strong>
                            </td>
                            <td width="50%" bgcolor="#FCFCFC" style="font-size:10px">
                                <strong>
                                    <?php
											if(isset($header[1])){
												print $header[1];
											}else{
												print "State Employee's perceived weakness(es)";
											}
										?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:10px">
                                <?php print $EmpStrenght; ?>
                            </td>
                            <td style="font-size:10px">
                                <?php print $EmpWeakness; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span style="font-size:10px"><strong>
                            <font color="#00CC00">SECTION FOUR: Learning and Development Actions</font>
                        </strong></span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td width="50%" bgcolor="#FCFCFC" style="font-size:10px">
                                <strong>
                                    <?php
											if(isset($header[2])){
												print $header[2];
											}else{
												print "Indentify Employee's Performance Gaps";
											}
										?>
                                </strong>
                            </td>
                            <td width="50%" bgcolor="#FCFCFC" style="font-size:10px">
                                <strong>
                                    <?php
											if(isset($header[3])){
												print $header[3];
											}else{
												print "State Remedial Actions that will be relevant over the next FY ";
											}
										?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:10px"><?php print $EmpPerfGap; ?></td>
                            <td style="font-size:10px"><?php print $EmpStrenght; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#FCFCFC"><span style="font-size:10px"><strong>
                            <font color="#00CC00">SECTION FIVE: Further Recommendations for the Employee</font>
                        </strong></span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td style="font-size:10px">
                                <?php print $MRecommendation; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="2" bgcolor="#FCFCFC" style="font-size:10px"><strong>
                                    <font color="#00CC00">SECTION SIX: Overall Ratings and Summary</font>
                                </strong></td>
                        </tr>
                        <tr>
                            <td width="28%" height="132" valign="top" style="font-size:10px">
                                <!--
										<table width="100%" border="1" cellpadding="4" cellspacing="0">
											<tr>
												<td width="50%" height="34" style="font-size:10px"><strong>Overall Rating</strong></td>
												<td width="50%" style="font-size:10px"><strong>Scores (marks obtained)</strong></td>
											</tr>
											<tr>
												<td height="38" style="font-size:10px">Functional Objectives</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td height="40" style="font-size:10px">Values Objectives</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td height="42" style="font-size:10px"><strong>Total Overall Rating</strong></td>
												<td>&nbsp;</td>
											</tr>
										</table>
									-->
                                <?php
										/*
										$dbOpen2 = ("Select * from KPIIndvScore where AQuartID='".ECh($_REQUEST["AQuartID"])."' and Status <>'D' and AID=(Select HashKey from KPISetting where Status='A' and AName='".ECh($_SESSION["StkTck"."HKey"])."') ORDER BY KRA_Group");
										*/
										if (isset($_REQUEST["apprs"])) {
											//$OpenID3 = $_REQUEST["apprs"];
											//print $_REQUEST["apprs"];
											//print "<br/>";
											//print $_REQUEST["GrpName"];

											$dbOpen2 = ("SELECT * FROM KPIIndvScore WHERE AQuartID = '".$_REQUEST["apprs"]."' and AID='".$_REQUEST["GrpName"]."' and Status <> 'D' ORDER BY KRA_Group");
											//$dbOpen2 = ("Select * from KPIIndvScore where Status <>'D' and AQuartID='$AQuartID_sel'  and AID='$KPISetting_hashkey' ORDER BY //KRA_Group");
											include '../login/dbOpen2.php';
											$prevValue = "";
											$main_total=0;
											$w_main_total=0;
											print "
												<table width=\"100%\" border=\"1\" cellpadding=\"4\" cellspacing=\"0\">
													<tr>
														<td>OVERALL RATING</td>
														<td>WEIGHTAGE</td>
														<td>SCORE(MARKS OBTAINED)</td>
													</tr>
											";
											while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
												$Del = $Del + 1;
												//$UpdUser = $row2['UpdatedBy'];
												$AuthRec = $row2['Status'];
												$AQuartID_Val = $row2['AQuartID'];
												$KRA_Group_value = $row2['KRA_Group'];
												//records=$row2['KRA_Group'];
												if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)&&($KRA_Group_value != "--")) {
													$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(Weightage) TotWeightage  from KPIIndvScore WHERE KRA_Group ='".$KRA_Group_value."'
													AND Status <>'D' and AQuartID = '".$_REQUEST["apprs"]."' and AID='".$_REQUEST["GrpName"]."'");
													$total_sum = ScriptRunner($Script,"TotMgrScore");
													$total_weightage = ScriptRunner($Script,"TotWeightage");
													//----------------------------
													print "
														<tr>
															<td><b>$KRA_Group_value</b></td>
															<td>$total_weightage</td>
															<td>$total_sum</td>
														</tr>
													";
													$prevValue = $KRA_Group_value;
													$main_total=$main_total+$total_sum;
													$w_main_total=$w_main_total+$total_weightage;
												}
											}
											$appr_yearly_count = ScriptRunner("SELECT num_of_appraisals FROM KPISettingNew WHERE Status = 'A'","num_of_appraisals");
											if (!$appr_yearly_count) {
												$appr_yearly_count = 2;
											}
											$avg_w_main_total = $w_main_total/$appr_yearly_count;
											$avg_main_total = $main_total/$appr_yearly_count;

											print "
												<tr>
													<td><b>TOTAL</b></td>
													<td><b>$w_main_total ($avg_w_main_total)</b></td>
													<td><b>$main_total ($avg_main_total)</b></td>
												</tr>
											";
											
											include '../login/dbClose2.php';

											// Fetch calibrated score for a selected appraisal
											$query = "SELECT * FROM KPIFinalScore WHERE AQuartID = '".$_REQUEST["apprs"]."' and EID='".$emp_hk."' and Status <> 'D'";
											$calibrated_score = ScriptRunner($query, "calibrated_score");
											$average = (int)$calibrated_score/2;
											print "
												<tr>
													<td><b>CALIBRATED SCORE</b></td>
													<td><b></b></td>
													<td><b>".(float)$calibrated_score."($average)</b></td>
												</tr>
											";
											
											print "</table>";
										}
									?>
                            </td>


                            <?php
									$appraisal_grades_count = ScriptRunner("SELECT Count(ItemCode) AS count FROM Masters WHERE ItemName = 'Appraisal Grade' AND Status <> 'D'", 'count');
								?>
                            <?php if ($appraisal_grades_count > 0): ?>
                            <?php
										$dbOpen2 = ("SELECT ItemCode, Val1, Val2, Val3 FROM Masters WHERE ItemName = 'Appraisal Grade' AND Status <> 'D' ORDER BY Val1 DESC");
										include '../login/dbOpen2.php';
				                    	$appraisal_grades = [];
					                    while($row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH)) {
					                    	$appraisal_grades[$row2['ItemCode']] = ['min' => $row2['Val1'], 'max' => $row2['Val2'] , 'badge' => ($row2['Val3']) ? $row2['Val3'] : '#808080' ];
					                    }
					                    include '../login/dbClose2.php';
									?>
                            <td width="72%" valign="top" style="font-size:10px">
                                <table width="100%" border="1" cellpadding="4" cellspacing="0">
                                    <tr>
                                        <?php foreach ($appraisal_grades as $key => $value): ?>
                                        <td align="center" style="font-size:10px"><?php echo $key ?></td>
                                        <?php endforeach ?>
                                    </tr>
                                    <tr>
                                        <?php foreach ($appraisal_grades as $key => $value): ?>
                                        <td align="center" style="font-size:10px">
                                            <strong><?php echo $value['max']. '-'.$value['min'] ?></strong></td>
                                        <?php endforeach ?>
                                    </tr>
                                    <tr>
                                        <?php
													if(!isset($main_total)) {
														foreach ($appraisal_grades as $key => $value) {
															print '
																<td height="25">&nbsp;</td>
															';
														}
													} else {
														foreach ($appraisal_grades as $key => $value) {
															if (($value['min'] <= $main_total) && ($main_total <= $value['max'])) {
																print '
																	<td bgcolor="'.$value['badge'].'">&nbsp;</td>
																';
															} else {
																print '
																	<td>&nbsp;</td>
																';
															}
														}
													}
												?>
                                    </tr>
                                </table>
                            </td>
                            <?php else: ?>
                            <td width="72%" valign="top" style="font-size:10px">
                                <table width="100%" border="1" cellpadding="4" cellspacing="0">
                                    <tr>
                                        <td align="center" style="font-size:10px">Category A</td>
                                        <td align="center" style="font-size:10px">Category B</td>
                                        <td align="center" style="font-size:10px">Category C</td>
                                        <td align="center" style="font-size:10px">Category D</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-size:10px"><strong>43-50</strong></td>
                                        <td align="center" style="font-size:10px"><strong>33-42</strong></td>
                                        <td align="center" style="font-size:10px"><strong>25-32</strong></td>
                                        <td align="center" style="font-size:10px"><strong>0-24</strong></td>
                                    </tr>
                                    <tr>
                                        <?php
													if(!isset($main_total)) {
														print '
															<td height="25">&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														';
													} else {
														$value = $main_total/$appr_yearly_count;
														if((43 <= $value) && ($value <= 50)) {
															print '
																<td height="25" bgcolor="green">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															';
														} elseif ((33 <= $value) && ($value <= 42)) {
															print '
																<td height="25">&nbsp;</td>
																<td bgcolor="yellow">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															';
														} elseif ((25 <= $value) && ($value <= 32)) {
															print '
																<td height="25">&nbsp;</td>
																<td>&nbsp;</td>
																<td bgcolor="orange">&nbsp;</td>
																<td>&nbsp;</td>
															';
														}elseif((0 <= $value) && ($value <= 24)) {
															print '
																<td height="25">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td bgcolor="red">&nbsp;</td>
																';
														}
													}
												?>
                                    </tr>
									<tr>
									<!-- Display coloured field against different score range -->
									<?php if(!isset($main_total)) {
														print '
															<td height="25">&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														';
													} else {
														// $value = $main_total/$appr_yearly_count;
														if((43 <= $average) && ($average <= 50)) {
															print '
																<td height="25" bgcolor="blue">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															';
														} elseif ((33 <= $average) && ($average <= 42)) {
															print '
																<td height="25">&nbsp;</td>
																<td bgcolor="blue">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															';
														} elseif ((25 <= $average) && ($average <= 32)) {
															print '
																<td height="25">&nbsp;</td>
																<td>&nbsp;</td>
																<td bgcolor="blue">&nbsp;</td>
																<td>&nbsp;</td>
															';
														}elseif((0 <= $average) && ($average <= 24)) {
															print '
																<td height="25">&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td bgcolor="red">&nbsp;</td>
																';
														}
													}
												?>
									</tr>
                                </table>
                            </td>
                            <?php endif ?>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td width="7%" bgcolor="#FCFCFC" style="font-size:10px">&nbsp;</td>
                            <td width="23%" bgcolor="#FCFCFC" style="font-size:10px"><strong>Employee</strong></td>
                            <td width="23%" bgcolor="#FCFCFC" style="font-size:10px"><strong>Supervisor</strong></td>
                            <td width="23%" bgcolor="#FCFCFC" style="font-size:10px"><strong>HOD</strong></td>
                            <td width="24%" bgcolor="#FCFCFC" style="font-size:10px"><strong>Divisional Director (Where
                                    Applicable)</strong></td>
                        </tr>
                        <tr>
                            <td style="font-size:10px"><span style="font-size:10px">Comments</span></td>
                            <td style="font-size:10px"><?php print $EComment; ?></td>
                            <td style="font-size:10px"><?php print $MComment; ?></td>
                            <td style="font-size:10px"><?php print $AprsComment3; ?></td>
                            <td style="font-size:10px"><?php print $AprsComment4; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span style="font-size:10px"><strong>MD’s Comments: (Where Applicable)</strong></span></td>
            </tr>
            <tr>
                <td height="21">
                    <table width="100%" border="1" cellpadding="4" cellspacing="0">
                        <tr>
                            <td height="96" style="font-size:10px"><?php print $AprsComment5; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
    <?php //include 'rpt_footer.php';?>
</body>

</html>

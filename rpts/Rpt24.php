<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
function stringToarry($str, $sep)
{

    return explode($sep, $str);
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>
	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
		<script>


			$(function () {
			    "use strict";

			    //Initialize Select2 Elements
			    $('.select2').select2();


			  });
		</script>
</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="" method="POST">
    <div class="row ">
    	<div class="form-group col-12">

	                                  <label  class="col-sm-4 col-form-label">Exclude Columns <span style="color: red">*</span>:</label>
	                                           <div class="col-sm-8 input-group input-group-sm">
	                                             <select class="form-control select2" multiple="multiple" data-placeholder="Select wage items" style="width: 100%;" id="wage_items" name="wage_items[]" >
	                                            <?php
$arr_all = [];
$query = "SELECT Top(1) * FROM [dbo].[payinfo]";
$coulmn_details = ScriptRunnercous($query);
for ($i = 1; $i <= 25; $i++) {
    if ($coulmn_details['PayItemOF' . $i] == 1) {
        $arr_all[] = [strtoupper($coulmn_details["PayItemNm" . $i]), $coulmn_details["PayItemCD" . $i]];

    }
}
foreach ($arr_all as $arr): ?>

	                                               <option value="<?php echo "$arr[0]::$arr[1]"; ?>" ><?php echo $arr[0]; ?></option>

	                                         <?php endforeach;?>
	                                             </select>

	                                            </div>

				</div>
                <br>
<div class="form-group col-3">
        <label class="col-4">Group Name: </label>
										<select name="AcctNo" id="AcctNo" class="form-control col-8">
											<?php
echo '<option value="--" selected="selected">--</option>';
$dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

include '../login/dbOpen3.php';
if (isset($_POST['AcctNo'])) {
    $SelID = sanitize($_POST['AcctNo']);
}
while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
    if ($SelID == $row3['HashKey']) {
        echo '<option selected value="' . $row3['HashKey'] . '">' . $row3['GName'] . '</option>';
    } else {echo '<option value="' . $row3['HashKey'] . '">' . $row3['GName'] . '</option>';}
}
include '../login/dbClose3.php';
?>
	</select>


	<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>

	</div>
</div>

<?php

$QueryStr = "";
$strExp2 = "";

$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open") && $_POST['AcctNo'] !== '--' && isset($_POST['wage_items'])) {
    $scheme = sanitize($_POST['AcctNo']);
    $wages = $_POST['wage_items'];
    $all_wage_name = [];
    foreach ($wages as $wage) {
        $all_wage_name[] = stringToarry($wage, "::")[0];
    }

    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[payinfo] where Scheme ='$scheme'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row2["PayItemNm" . $i]), $all_wage_name)) {
                    continue;
                } else {
                    $QueryStr = $QueryStr . "<th >" . strtoupper($row2["PayItemNm" . $i]) . "</th>";
                    $strExp = $strExp . "," . strtoupper($row2["PayItemNm" . $i]);
                }
            }
        }
    }
    include '../login/dbClose2.php';
    $QueryStr = $QueryStr . "<th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th><th>DEPARTMENT</th><th>BRANCH</th>";
    $strExp = $strExp . ",LEAVE,PENSION,PAYE,NET PAY,GROSS,DEPARTMENT,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

//     echo '<tfoot><tr ></tr></tfoot>

// <tbody>';
    //$PrintHTML="";
    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $dbOpen2 = ("SELECT * FROM [dbo].[PayInfo]
WHERE Status <> 'D' and Scheme ='$scheme'
ORDER BY [Full Name]");
    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $from_net = [];
        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';
        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row2["PayItemNm" . $i]), $all_wage_name)) {
                    $from_net[] = [(float) $row2['PayItem' . $i], $row2['PayItemCD' . $i]];
                    continue;
                } else {
                    $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                    $strExp .= "," . ($row2['PayItem' . $i]);
                    if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                    $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];

                }
            }
        }
        $the_net = $row2['NetPay'];
        $the_gross = $row2['Gross'];
        foreach ($from_net as $to_net) {
            if ($to_net[0] > 0 && ($to_net[1] === 'CR' || $to_net[1] === 'FP')) {
                $the_net = $the_net - $to_net[0];
                $the_gross = $the_gross - $to_net[0];

            } elseif ($to_net[0] > 0 && $to_net[1] === 'DR') {
                $the_net = $the_net + $to_net[0];
                $the_gross = $the_gross + $to_net[0];

            }
        }

        $strExp .= "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . "," . $the_net . "," . $the_gross . "," . $row2['Department'] . "," . $row2['OName'];

        $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_gross, 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $the_net;
        $SumVal_Gross = $SumVal_Gross + $the_gross;
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[payinfo]  where Scheme ='$scheme'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        //$strExp .= ","; NOT REQUIRED

        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                //$strExp .= ",".number_format($SumVal[$i],2);
                $strExp .= "," . ($SumVal[$i]);
            }
        }
    }
    include '../login/dbClose2.php';

    $PrintHTML .= "<th align='right'>" . number_format($SumVal_Leave, 2) . "</th><th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th></th><th></th></tfoot>";
    $strExp .= "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
//$strExp .=",".number_format($SumVal_Leave,2).",".number_format($SumVal_PensionEmployee,2).",".number_format($SumVal_PAYE,2).",".number_format($SumVal_NetPay,2).",".number_format($SumVal_Gross,2).",,";

    // echo '</table>';
    echo $PrintHTML;
    include 'rpt_footer_min.php';

} else if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open") && $_POST['AcctNo'] !== '--') {

    $scheme = sanitize($_POST['AcctNo']);

    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[payinfo] where Scheme ='$scheme'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                $QueryStr = $QueryStr . "<th >" . strtoupper($row2["PayItemNm" . $i]) . "</th>";

                $strExp = $strExp . "," . strtoupper($row2["PayItemNm" . $i]);
            }
        }
    }
    include '../login/dbClose2.php';
    $QueryStr = $QueryStr . "<th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th><th>DEPARTMENT</th><th>BRANCH</th>";
    $strExp = $strExp . ",LEAVE,PENSION,PAYE,NET PAY,GROSS,DEPARTMENT,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

//     echo '<tfoot><tr ></tr></tfoot>

// <tbody>';
    //$PrintHTML="";
    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $dbOpen2 = ("SELECT * FROM [dbo].[PayInfo]
WHERE Status <> 'D' and Scheme ='$scheme'
ORDER BY [Full Name]");
    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';
        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                $strExp .= "," . ($row2['PayItem' . $i]);
                if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];
            }
        }

        $strExp .= "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . "," . $row2['NetPay'] . "," . $row2['Gross'] . "," . $row2['Department'] . "," . $row2['OName'];

        $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['NetPay'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['Gross'], 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $row2['NetPay'];
        $SumVal_Gross = $SumVal_Gross + $row2['Gross'];
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[payinfo]  where Scheme ='$scheme'";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        //$strExp .= ","; NOT REQUIRED

        for ($i = 1; $i <= 25; $i++) {
            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                //$strExp .= ",".number_format($SumVal[$i],2);
                $strExp .= "," . ($SumVal[$i]);
            }
        }
    }
    include '../login/dbClose2.php';

    $PrintHTML .= "<th align='right'>" . number_format($SumVal_Leave, 2) . "</th><th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th></th><th></th></tfoot>";
    $strExp .= "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
//$strExp .=",".number_format($SumVal_Leave,2).",".number_format($SumVal_PensionEmployee,2).",".number_format($SumVal_PAYE,2).",".number_format($SumVal_NetPay,2).",".number_format($SumVal_Gross,2).",,";

    // echo '</table>';
    echo $PrintHTML;
    include 'rpt_footer_min.php';

}
?>



</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
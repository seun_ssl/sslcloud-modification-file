<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post">

  <div class="form-group">
    <label>Employee Status : </label>

    <!-- <select name="type" id="type" class="form-control">
        <option value="all" >All</option>
        <option value="terminated" <?= isset($_POST['type']) && $_POST['type'] === 'terminated' ? 'selected' : ''  ?> >Terminated</option>
        <option  value="resigned" <?= isset($_POST['type']) && $_POST['type'] === 'resigned' ? 'selected' : ''  ?> > Resigned </option>
        <option  value="deactivated"  <?= isset($_POST['type']) && $_POST['type'] === 'deactivated' ? 'selected' : ''  ?> > Deactivated </option>
    </select> -->
<select name="type" id="type" class="form-control">
    <option value="eetet" >-select-</option>
     <option value="all" >All</option>
<?php     

	$dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='EmployeeStatus') ORDER BY Val1");
include '../login/dbOpen2.php' ?>

<?php while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)):?>
<option  <?= isset($_POST['type']) && $_POST['type'] === $row2['Val1'] ? 'selected' : ''  ?>   value="<?= $row2['Val1'] ?>"> <?=$row2['Val1'] ?></option>';
<?php endwhile;?>
 <?php include '../login/dbClose2.php'?>


</select>


    <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" /> 
  </div> 

<?php
$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Branch Name,Department,NIN,Category,Line Manager,Gender,Marital Status,Employee Status,Exit reason,Employment Date,Date";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">NIN</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employee Status</th>
<th data-placeholder="" align="left" valign="middle">Exit reason</th>
<th data-placeholder="" align="left" valign="middle">Employment Date</th>
<th data-placeholder="" align="left" valign="middle">Status Date</th>

</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">NIN</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employee Status</th>
<th data-placeholder="" align="left" valign="middle">Exit reason</th>
<th data-placeholder="" align="left" valign="middle">Employment Date</th>
<th data-placeholder="" align="left" valign="middle">Status Date</th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE


if( isset($_POST['SubmitTrans'])){

  $empStat = ECh($_POST['type']);


  if($empStat === 'all' ){

     $dbOpen2 = ("

    SELECT e.[Full Name]
          ,e.[Department]
		   ,e.[exitreasons] as [Exit reason]
          ,e.[NIN]
          ,e.[EmpCategory]
          ,e.[Line Manager]
          ,e.[Sex]
          ,e.[Marital Status]
          ,e.[Emp Status]
          ,e.[EmpDt]
        ,e.[Status Date]
        , b.OName branch_name
    FROM [dbo].[EmpInfo] e
    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
 
    ORDER BY [Last Name]


    ");



  }else{

     $dbOpen2 = ("

    SELECT e.[Full Name]
          ,e.[Department]
		   ,e.[exitreasons] as [Exit reason]
          ,e.[NIN]
          ,e.[EmpCategory]
          ,e.[Line Manager]
          ,e.[Sex]
          ,e.[Marital Status]
          ,e.[Emp Status]
          ,e.[EmpDt]
        ,e.[Status Date]
        , b.OName branch_name
    FROM [dbo].[EmpInfo] e
    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
    WHERE [Emp Status] = '$empStat'
    ORDER BY [Last Name]


    ");


  }


}


// else{


//     $dbOpen2 = ("

//     SELECT e.[Full Name]
//           ,e.[Department]
// 		   ,e.[exitreasons] as [Exit reason]
//           ,e.[NIN]
//           ,e.[EmpCategory]
//           ,e.[Line Manager]
//           ,e.[Sex]
//           ,e.[Marital Status]
//           ,e.[Emp Status]
//           ,e.[EmpDt]
//     	  ,e.[Status Date]
//     	  , b.OName branch_name
//     FROM [dbo].[EmpInfo] e
//     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
  
//     ORDER BY [Last Name]


//     ");
// }

    include '../login/dbOpen2.php';


    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $Del = $Del + 1;
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
        //--New
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

        $strExp .= chr(13) . chr(10);
        $strExp .= $row2['Full Name'] . ","
            . $row2['branch_name'] . ","
            . $row2['Department'] . ","
            . $row2['NIN'] . ","
            . $row2['EmpCategory'] . ","
            . $row2['Line Manager'] . ","
            . $row2['Sex'] . ","
            . $row2['Marital Status'] . ","
            . $row2['Emp Status'] . ","
			      . $row2['Exit reason'] . ","
            . $row2['EmpDt'] . ","
            . $row2['Status Date'];

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['branch_name']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['NIN']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['EmpCategory']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Line Manager']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Sex']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Marital Status']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Emp Status'] . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Exit reason']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Status Date'] . '</td>
    </tr>';
    }
    include '../login/dbClose2.php';

$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
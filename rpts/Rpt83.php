<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
#to_flex {
	display:flex;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
    $("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
    $("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $from = $_POST["S_RptDate"];
    $to = $_POST["E_RptDate"];

    $dbOpen2 = ("
Select	[Full Name],
		Department,
		[AuthedBy],
        CONVERT(Varchar(11), AuthDate, 106) AS AuthDate,
		LnAmt,
		IntRate,
		Tenure,
		RepayAmt,
		Paid,
		OutSPay,
		SRepayDate,
		ERepayDate
FROM LoanDetails
WHERE Status ='A'
and [AuthDate]  BETWEEN '" . $from . "' and '" . $to . "'
ORDER BY [AuthDate] DESC

");
}

?>
</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post">
	<div id="to_flex" >
                                    <div class="form-group">
                                                                        <label>From:</label>
                            <?php
if (isset($_REQUEST["S_RptDate"])) {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';} else {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';}
?>
                                                                        </div>

																		           <div class="form-group">
                                                                        <label>To:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';}
?>
                                                                                            </div>

                                                                        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>
																		</div>

<?php
$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Department,Approved By,Approved Date,Loan Amount,Interest%,Tenure,Repay Amount,Paid Amount,OutStanding,Start Repay,End Repay";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Approved By</th>
<th data-placeholder="" align="left" valign="middle">Approved Date</th>
<th data-placeholder="" align="left" valign="middle">Loan Amount</th>
<th data-placeholder="" align="left" valign="middle">Interest%</th>
<th data-placeholder="" align="left" valign="middle">Tenure</th>
<th data-placeholder="" align="left" valign="middle">Repay Amount</th>
<th data-placeholder="" align="left" valign="middle">Paid Amount</th>
<th data-placeholder="" align="left" valign="middle">OutStanding</th>
<th data-placeholder="" align="left" valign="middle">Start Repay</th>
<th data-placeholder="" align="left" valign="middle">End Repay</th>

</tr>
</thead>
<tbody>';
//$PrintHTML="";
$Del = 0;
$LnAmt = $IntRate = $Tenure = $RepayAmt = $Paid = $OutSPay = 0;

//SIMON: PUT REPORT QUERY HERE

// Employe Salary History

include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['Full Name'] . ","
        . $row2['Department'] . ","
        . $row2['AuthedBy'] . ","
        . $row2['AuthDate'] . ","
        . $row2['LnAmt'] . ","
        . $row2['IntRate'] . ","
        . $row2['Tenure'] . ","
        . $row2['RepayAmt'] . ","
        . $row2['Paid'] . ","
        . $row2['OutSPay'] . ","
        . $row2['SRepayDate'] . ","
        . $row2['ERepayDate'];

    $LnAmt = $LnAmt + $row2['LnAmt'];
    $IntRate = $IntRate + $row2['IntRate'];
    $Tenure = $Tenure + $row2['Tenure'];
    $RepayAmt = $RepayAmt + $row2['RepayAmt'];
    $Paid = $Paid + $row2['Paid'];
    $OutSPay = $OutSPay + $row2['OutSPay'];

    $PrintHTML .= '<tr><td align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['AuthedBy']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['AuthDate'] . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['LnAmt'], 2) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['IntRate'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Tenure'] . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['RepayAmt'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['Paid'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['OutSPay'], 2) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['SRepayDate'] . '</td>
<td align="left" valign="middle" scope="col">' . $row2['ERepayDate'] . '</td>
</tr>';
}
include '../login/dbClose2.php';
$PrintHTML .= '</tbody>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="right" valign="middle">' . number_format($LnAmt, 2) . '</th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="right" valign="middle">' . number_format($RepayAmt, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($Paid, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($OutSPay, 2) . '</th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>

</tr>
</tfoot>
</table>';
$strExp .= chr(13) . chr(10) . ",,,," . ($LnAmt) . ",,," . ($RepayAmt) . "," . ($Paid) . "," . ($OutSPay) . ",,";
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
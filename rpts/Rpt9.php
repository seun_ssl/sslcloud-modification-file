<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--')) {
    $selectOption = $_POST['employee'];

    // $from = $_POST["S_RptDate"];

    // $end = $_POST["E_RptDate"];

//     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND OName='" . $selectOption . "'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");

    $dbOpen2 = ("

SELECT [Full Name]
      ,[Department]
      ,[Oname]
      ,[PensionEmployee]
      ,[PensionEmployer]
      ,([PensionEmployee] + [PensionEmployer]) as Pensiontotal
      ,[Netpay]
      ,[pfa_code]
      ,[nin]
      ,[PenCustID]
      ,[Gross]
FROM PayInfo2
WHERE Status = 'A'
AND OName='" . $selectOption . "'");

} else {

    // $from = $_REQUEST["S_RptDate"];

    // $end = $_REQUEST["E_RptDate"];

//     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");
    $dbOpen2 = ("

SELECT [Full Name]
      ,[Department]
      ,[Oname]
      ,[PensionEmployee]
      ,[PensionEmployer]
      ,([PensionEmployee] + [PensionEmployer]) as Pensiontotal
      ,[Netpay]
      ,[pfa_code]
      ,[nin]
      ,[PenCustID]
      ,[Gross]
FROM PayInfo2
WHERE Status = 'A'");

}

// print_r($dbOpen2);
?>

<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post" id="attend" class="form-inline">


<div class="form-group">
<label>Branch: </label>
										<select name="employee" id="employee" class="form-control">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
    echo "selected";
}
    ?>><?php echo $row3['OName']; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
									</div>





																		<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>


											<!-- </div> -->
								<br/>
								<br/>

<?php

$strExp = "";include 'rpt_header.php';

//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$strExp .= "Full Name,NIN Number,RSA Pin, PFA Custodian,PFA Code, Department,Branch, Pension (Employee),Pension (Employer),Voluntary (Employee),Voluntary (Employer), Total";
$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">RSA Pin</th>
<th data-placeholder="" align="left" valign="middle">PFA Custodian</th>
<th data-placeholder="" align="left" valign="middle">PFA Code</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Branch</th>
<th data-placeholder="" align="left" valign="middle">Pension (Employee)</th>
<th data-placeholder="" valign="middle" align="left">Pension (Employer)</th>
<th data-placeholder="" align="left" valign="middle">Voluntary (Employee)</th>
<th data-placeholder="" valign="middle" align="left">Voluntary (Employer)</th>
<th data-placeholder="" valign="middle" align="left">Total</th>

</tr>
</thead>
<tbody>';

$Del = 0;
$PensionEmployee = $PensionEmployer = $Pensiontotal = 0;
// var_dump($dbOpen2);
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if (isset($row2['PensionEmployee']) && $row2['PensionEmployee'] > 0) {
        $Del = $Del + 1;
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
        //--New
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
        $PensionEmployee = $PensionEmployee + $row2['PensionEmployee'];
        $PensionEmployer = $PensionEmployer + $row2['PensionEmployer'];
        $Pensiontotal = $Pensiontotal + $row2['Pensiontotal'];

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['nin'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['pfa_code'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['PenCustID'] . '</td>
    <td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Oname'] . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployer'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format(0, 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format(0, 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['Pensiontotal'], 2) . '</td>
</tr>';

        $strExp .= chr(13) . chr(10) . trim($row2['Full Name']) . ","
        . $row2['nin'] . ","
        . $row2['pfa_code'] . ","
        . $row2['PenCustID'] . ","
        . '' . ","
        . trim($row2['Department']) . ","
            . $row2['Oname'] . ","
            . ($row2['PensionEmployee']) . ","
            . $row2['PensionEmployer'] . ","
            . 0 . ","
            . 0 . ","
            . $row2['Pensiontotal'] . ","
        ;
    }
}
include '../login/dbClose2.php';

$PrintHTML .= '</tbody>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="right" valign="middle">' . number_format($PensionEmployee, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($PensionEmployer, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format(0, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format(0, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($Pensiontotal, 2) . '</th>

</tr>
</tfoot>
</table>';

$strExp .= chr(13) . chr(10) . ",,,,,,,"
    . ($PensionEmployee) . ","
    . ($PensionEmployer) . ","
    . (0) . ","
    . (0) . ","
    . ($Pensiontotal) . ","
;
echo $PrintHTML;

include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>


</body>
</html>
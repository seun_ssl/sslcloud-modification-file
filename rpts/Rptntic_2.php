<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php

$strExp = "";
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Name of Branch,Academic Single,Academic Married,Academic Divorced,Academic Widow,Non-Academic Single,Non-Academic Married,Non-Academic Divorced,Non-Academic Widow,Admin Single,Admin Married,Admin Divorced,Admin Widow,Total Single,Total Married,Total Divorced,Total Widow,Total Staff";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >


<th data-placeholder="" align="left" valign="middle">Name of Branch</th>
<th data-placeholder="" align="left" valign="middle">Academic Single</th>
<th data-placeholder="" align="left" valign="middle">Academic Married</th>
<th data-placeholder="" align="left" valign="middle">Academic Divorced</th>
<th data-placeholder="" align="left" valign="middle">Academic Widow</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Single</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Married</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Divorced</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Widow</th>
<th data-placeholder="" align="left" valign="middle">Admin Single</th>
<th data-placeholder="" align="left" valign="middle">Admin Married</th>
<th data-placeholder="" align="left" valign="middle">Admin Divorced</th>
<th data-placeholder="" align="left" valign="middle">Admin Widow</th>
<th data-placeholder="" align="left" valign="middle">Total Single</th>
<th data-placeholder="" align="left" valign="middle">Total Married</th>
<th data-placeholder="" align="left" valign="middle">Total Divorced</th>
<th data-placeholder="" align="left" valign="middle">Total Widow</th>
<th data-placeholder="" align="left" valign="middle">Total Staff</th>


</tr>
</thead>

<tbody>';
//$PrintHTML="";
$Del = 0;
$aca_sig_tot = 0;
$aca_married_tot = 0;
$aca_div_tot = 0;
$aca_widow_tot = 0;
$nonaca_sig_tot = 0;
$nonaca_married_tot = 0;
$nonaca_div_tot = 0;
$nonaca_widow_tot = 0;
$adm_sig_tot = 0;
$adm_married_tot = 0;
$adm_div_tot = 0;
$adm_widow_tot = 0;
$tot_sig_tot = 0;
$tot_married_tot = 0;
$tot_div_tot = 0;
$tot_widow_tot = 0;
$all_all_tot = 0;

//SIMON: PUT REPORT QUERY HERE

//
// $Script="Select top(1) HashKey from KPIStart where SDate <=GETDATE() and Status='A' order by SDate Desc";

$dbOpen2 = ("
select e.BranchID, max(b.OName) branch_name, count(*) total from EmpTbl e
  INNER JOIN BrhMasters b on e.BranchID = b.HashKey
  where b.Status not in('U','D')
   and e.EmpStatus in('Active')
   and e.MStatus in('Divorced','Widow','Married','Single')
  group by e.BranchID
");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    $regrouping = [];
    $regrouping_sex = [];
    if (is_numeric((trim($row2['total'])))) {
        $all_all_tot = $all_all_tot + (int) trim($row2['total']);
    } else {
        $all_all_tot += 0;

    }

    $dbOpen3 = "select MStatus,EmpCategory,count(EmpCategory)count_tot from EmpTbl  where BranchID ='{$row2['BranchID']}'  and EmpStatus in('Active') and MStatus in('Divorced','Widow','Married','Single') group by EmpCategory,MStatus";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
        $regrouping[] = $row3;
    }
    include '../login/dbClose3.php';

    $dbOpen7 = "select MStatus, count(MStatus) stat_count from EmpTbl where BranchID ='{$row2['BranchID']}' and MStatus in('Divorced','Widow','Married','Single') and EmpStatus in('Active') group by MStatus";
    include '../login/dbOpen7.php';
    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_ASSOC)) {
        $regrouping_sex[] = $row7;
    }
    include '../login/dbClose7.php';

    // var_dump($regrouping);
    // var_dump($regrouping_sex);
    // var_dump($row2['total']);
    // echo "<hr/>";
    $aca_sig = 0;

    $aca_married = 0;

    $aca_div = 0;

    $aca_widow = 0;

    $nonaca_sig = 0;

    $nonaca_married = 0;

    $nonaca_div = 0;
    $nonaca_widow = 0;

    $adm_sig = 0;

    $adm_married = 0;
    $adm_div = 0;
    $adm_widow = 0;

    $tot_sig = 0;
    $tot_married = 0;

    $tot_div = 0;

    $tot_widow = 0;

    foreach ($regrouping_sex as $value) {
        if ($value['MStatus'] === 'Single') {
            $tot_sig = $value['stat_count'];
            if (is_numeric($tot_sig)) {
                $tot_sig_tot = $tot_sig_tot + (int) $tot_sig;
            } else {
                $tot_sig_tot += 0;

            }

        }
        if ($value['MStatus'] === 'Married') {
            $tot_married = $value['stat_count'];
            if (is_numeric($tot_married)) {
                $tot_married_tot = $tot_married_tot + (int) $tot_married;
            } else {
                $tot_married_tot += 0;

            }
        }
        if ( strtolower($value['MStatus']) === 'widow') {
            $tot_widow = $value['stat_count'];
            if (is_numeric($tot_widow)) {
                $tot_widow_tot = $tot_widow_tot + (int) $tot_widow;
            } else {
                $tot_widow_tot += 0;

            }
        }
        if ($value['MStatus'] === 'Divorced') {
            $tot_div = $value['stat_count'];
            if (is_numeric($tot_div)) {
                $tot_div_tot = $tot_div_tot + (int) $tot_div;
            } else {
                $tot_div_tot += 0;

            }
        }
    }
    foreach ($regrouping as $value) {
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['MStatus'] === 'Single') {
            $aca_sig = $value['count_tot'];
            if (is_numeric($aca_sig)) {
                $aca_sig_tot = $aca_sig_tot + (int) $aca_sig;
            } else {
                $aca_sig_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['MStatus'] === 'Married') {
            $aca_married = $value['count_tot'];
            if (is_numeric($aca_married)) {
                $aca_married_tot = $aca_married_tot + (int) $aca_married;
            } else {
                $aca_married_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['MStatus'] === 'Divorced') {
            $aca_div = $value['count_tot'];
            if (is_numeric($aca_div)) {
                $aca_div_tot = $aca_div_tot + (int) $aca_div;
            } else {
                $aca_div_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && $value['MStatus'] === 'Widow') {
            $aca_widow = $value['count_tot'];
            if (is_numeric($aca_widow)) {
                $aca_widow_tot = $aca_widow_tot + (int) $aca_widow;
            } else {
                $aca_widow_tot += 0;

            }

        }

        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['MStatus'] === 'Single') {
            $nonaca_sig = $value['count_tot'];
            if (is_numeric($nonaca_sig)) {
                $nonaca_sig_tot = $nonaca_sig_tot + (int) $nonaca_sig;
            } else {
                $nonaca_sig_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['MStatus'] === 'Married') {
            $nonaca_married = $value['count_tot'];
            if (is_numeric($nonaca_married)) {
                $nonaca_married_tot = $nonaca_married_tot + (int) $nonaca_married;
            } else {
                $nonaca_married_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['MStatus'] === 'Divorced') {
            $nonaca_div = $value['count_tot'];
            if (is_numeric($nonaca_div)) {
                $nonaca_div_tot = $nonaca_div_tot + (int) $nonaca_div;
            } else {
                $nonaca_div_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && $value['MStatus'] === 'Widow') {
            $nonaca_widow = $value['count_tot'];
            if (is_numeric($nonaca_widow)) {
                $nonaca_widow_tot = $nonaca_widow_tot + (int) $nonaca_widow;
            } else {
                $nonaca_widow_tot += 0;

            }
        }

        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['MStatus'] === 'Single') {
            $adm_sig = $value['count_tot'];
            if (is_numeric($adm_sig)) {
                $adm_sig_tot = $adm_sig_tot + (int) $adm_sig;
            } else {
                $adm_sig_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['MStatus'] === 'Married') {
            $adm_married = $value['count_tot'];
            if (is_numeric($adm_married)) {
                $adm_married_tot = $adm_married_tot + (int) $adm_married;
            } else {
                $adm_married_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['MStatus'] === 'Divorced') {
            $adm_div = $value['count_tot'];
            if (is_numeric($adm_div)) {
                $adm_div_tot = $adm_div_tot + (int) $adm_div;
            } else {
                $adm_div_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && $value['MStatus'] === 'Widow') {
            $adm_widow = $value['count_tot'];
            if (is_numeric($adm_widow)) {
                $adm_widow_tot = $adm_widow_tot + (int) $adm_widow;
            } else {
                $adm_widow_tot += 0;

            }

        }
    }

    $strExp .= chr(13) . chr(10);
    $strExp .= (trim($row2['branch_name'])) . ","
        . $aca_sig . ","
        . $aca_married . ","
        . $aca_div . ","
        . $aca_widow . ","
        . $nonaca_sig . ","
        . $nonaca_married . ","
        . $nonaca_div . ","
        . $nonaca_widow . ","
        . $adm_sig . ","
        . $adm_married . ","
        . $adm_div . ","
        . $adm_widow . ","
        . $tot_sig . ","
        . $tot_married . ","
        . $tot_div . ","
        . $tot_widow . ","
        . (trim($row2['total']));

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['branch_name'])) . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_sig . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_married . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_div . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_widow . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_sig . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_married . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_div . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_widow . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_sig . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_married . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_div . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_widow . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_sig . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_married . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_div . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_widow . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['total']) . '</td>

</tr>';
}
include '../login/dbClose2.php';

$strExp .= chr(13) . chr(10);
$strExp .= "Total,"
    . $aca_sig_tot . ","
    . $aca_married_tot . ","
    . $aca_div_tot . ","
    . $aca_widow_tot . ","
    . $nonaca_sig_tot . ","
    . $nonaca_married_tot . ","
    . $nonaca_div_tot . ","
    . $nonaca_widow_tot . ","
    . $adm_sig_tot . ","
    . $adm_married_tot . ","
    . $adm_div_tot . ","
    . $adm_widow_tot . ","
    . $tot_sig_tot . ","
    . $tot_married_tot . ","
    . $tot_div_tot . ","
    . $tot_widow_tot . ","
    . $all_all_tot;
$PrintHTML .= '
<tfoot>
<tr >



<th data-placeholder="" align="left" valign="middle">Total</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_sig_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_married_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_div_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_widow_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_sig_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_married_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_div_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_widow_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_sig_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_married_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_div_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_widow_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_sig_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_married_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_div_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_widow_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $all_all_tot . '</th>


</tr>
</tfoot>';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
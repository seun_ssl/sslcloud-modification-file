<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php

$strExp = "";
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Name of Branch,Academic Active,Academic Terminated,Academic Resigned,Non-Academic Active,Non-Academic Terminated,Non-Academic Resigned,Admin Active,Admin Terminated,Admin Resigned,Total Active,Total Terminated,Total Resigned,Total Staff";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >


<th data-placeholder="" align="left" valign="middle">Name of Branch</th>
<th data-placeholder="" align="left" valign="middle">Academic Active</th>
<th data-placeholder="" align="left" valign="middle">Academic Terminated</th>
<th data-placeholder="" align="left" valign="middle">Academic Resigned</th>

<th data-placeholder="" align="left" valign="middle">Non-Academic Active</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Terminated</th>
<th data-placeholder="" align="left" valign="middle">Non-Academic Resigned</th>

<th data-placeholder="" align="left" valign="middle">Admin Active</th>
<th data-placeholder="" align="left" valign="middle">Admin Terminated</th>
<th data-placeholder="" align="left" valign="middle">Admin Resigned</th>

<th data-placeholder="" align="left" valign="middle">Total Active</th>
<th data-placeholder="" align="left" valign="middle">Total Terminated</th>
<th data-placeholder="" align="left" valign="middle">Total Resigned</th>

<th data-placeholder="" align="left" valign="middle">Total Staff</th>


</tr>
</thead>

<tbody>';
//$PrintHTML="";
$Del = 0;
$aca_act_tot = 0;
$aca_ter_tot = 0;
$aca_res_tot = 0;
$nonaca_act_tot = 0;
$nonaca_ter_tot = 0;
$nonaca_res_tot = 0;
$adm_act_tot = 0;
$adm_ter_tot = 0;
$adm_res_tot = 0;
$tot_act_tot = 0;
$tot_ter_tot = 0;
$tot_res_tot = 0;
$all_all_tot = 0;
//SIMON: PUT REPORT QUERY HERE

//
// $Script="Select top(1) HashKey from KPIStart where SDate <=GETDATE() and Status='A' order by SDate Desc";

$dbOpen2 = ("
select e.BranchID, max(b.OName) branch_name, count(*) total from EmpTbl e
INNER JOIN BrhMasters b on e.BranchID = b.HashKey
where b.Status not in('U','D')
 and e.EmpStatus in('Active','Terminated','Resigned')
group by e.BranchID
");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    $regrouping = [];
    $regrouping_sex = [];
    if (is_numeric((trim($row2['total'])))) {
        $all_all_tot = $all_all_tot + (int) trim($row2['total']);
    } else {
        $all_all_tot += 0;

    }

    $dbOpen3 = "select EmpStatus,EmpCategory,count(EmpCategory)count_tot from EmpTbl  where BranchID ='{$row2['BranchID']}' and EmpStatus in('Active','Terminated','Resigned') group by EmpCategory,EmpStatus";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
        $regrouping[] = $row3;
    }
    include '../login/dbClose3.php';

    $dbOpen7 = "select EmpStatus, count(EmpStatus) stat_count from EmpTbl where BranchID ='{$row2['BranchID']}' and EmpStatus in('Active','Terminated','Resigned') group by EmpStatus";
    include '../login/dbOpen7.php';
    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_ASSOC)) {
        $regrouping_sex[] = $row7;
    }
    include '../login/dbClose7.php';

    // var_dump($regrouping);
    // var_dump($regrouping_sex);
    // var_dump($row2['total']);
    // echo "<hr/>";
    $aca_act = 0;
    $aca_ter = 0;
    $aca_res = 0;
    $nonaca_act = 0;
    $nonaca_ter = 0;
    $nonaca_res = 0;
    $adm_act = 0;
    $adm_ter = 0;
    $adm_res = 0;
    $tot_act = 0;
    $tot_ter = 0;
    $tot_res = 0;

    foreach ($regrouping_sex as $value) {
        if (strtoupper($value['EmpStatus']) === 'ACTIVE') {
            $tot_act = $value['stat_count'];
            if (is_numeric($tot_act)) {
                $tot_act_tot = $tot_act_tot + (int) $tot_act;
            } else {
                $tot_act_tot += 0;

            }

        }
        if (strtoupper($value['EmpStatus']) === 'TERMINATED') {
            $tot_ter = $value['stat_count'];
            if (is_numeric($tot_ter)) {
                $tot_ter_tot = $tot_ter_tot + (int) $tot_ter;
            } else {
                $tot_ter_tot += 0;

            }
        }
        if (strtoupper($value['EmpStatus']) === 'RESIGNED') {
            $tot_res = $value['stat_count'];
            if (is_numeric($tot_res)) {
                $tot_res_tot = $tot_res_tot + (int) $tot_res;
            } else {
                $tot_res_tot += 0;

            }
        }

    }
    foreach ($regrouping as $value) {
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && strtoupper($value['EmpStatus']) === 'ACTIVE') {
            $aca_act = $value['count_tot'];
            if (is_numeric($aca_act)) {
                $aca_act_tot = $aca_act_tot + (int) $aca_act;
            } else {
                $aca_act_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && strtoupper($value['EmpStatus']) === 'TERMINATED') {
            $aca_ter = $value['count_tot'];
            if (is_numeric($aca_ter)) {
                $aca_ter_tot = $aca_ter_tot + (int) $aca_ter;
            } else {
                $aca_ter_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ACADEMIC' && strtoupper($value['EmpStatus']) === 'RESIGNED') {
            $aca_res = $value['count_tot'];
            if (is_numeric($aca_res)) {
                $aca_res_tot = $aca_res_tot + (int) $aca_res;
            } else {
                $aca_res_tot += 0;

            }
        }

        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && strtoupper($value['EmpStatus']) === 'ACTIVE') {
            $nonaca_act = $value['count_tot'];
            if (is_numeric($nonaca_act)) {
                $nonaca_act_tot = $nonaca_act_tot + (int) $nonaca_act;
            } else {
                $nonaca_act_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && strtoupper($value['EmpStatus']) === 'TERMINATED') {
            $nonaca_ter = $value['count_tot'];
            if (is_numeric($nonaca_ter)) {
                $nonaca_ter_tot = $nonaca_ter_tot + (int) $nonaca_ter;
            } else {
                $nonaca_ter_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'NON-ACADEMIC' && strtoupper($value['EmpStatus']) === 'RESIGNED') {
            $nonaca_res = $value['count_tot'];
            if (is_numeric($nonaca_res)) {
                $nonaca_res_tot = $nonaca_res_tot + (int) $nonaca_res;
            } else {
                $nonaca_res_tot += 0;

            }

        }

        if (strtoupper($value['EmpCategory']) === 'ADMIN' && strtoupper($value['EmpStatus']) === 'ACTIVE') {
            $adm_act = $value['count_tot'];
            if (is_numeric($adm_act)) {
                $adm_act_tot = $adm_act_tot + (int) $adm_act;
            } else {
                $adm_act_tot += 0;

            }

        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && strtoupper($value['EmpStatus']) === 'TERMINATED') {
            $adm_ter = $value['count_tot'];
            if (is_numeric($adm_ter)) {
                $adm_ter_tot = $adm_ter_tot + (int) $adm_ter;
            } else {
                $adm_ter_tot += 0;

            }
        }
        if (strtoupper($value['EmpCategory']) === 'ADMIN' && strtoupper($value['EmpStatus']) === 'RESIGNED') {
            $adm_res = $value['count_tot'];
            if (is_numeric($adm_res)) {
                $adm_res_tot = $adm_res_tot + (int) $adm_res;
            } else {
                $adm_res_tot += 0;

            }

        }

    }

    $strExp .= chr(13) . chr(10);
    $strExp .= (trim($row2['branch_name'])) . ","
        . $aca_act . ","
        . $aca_ter . ","
        . $aca_res . ","
        . $nonaca_act . ","
        . $nonaca_ter . ","
        . $nonaca_res . ","
        . $adm_act . ","
        . $adm_ter . ","
        . $adm_res . ","
        . $tot_act . ","
        . $tot_ter . ","
        . $tot_res . ","
        . (trim($row2['total']));

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['branch_name'])) . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_act . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_ter . '</td>
  <td align="left" valign="middle" scope="col">' . $aca_res . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_act . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_ter . '</td>
  <td align="left" valign="middle" scope="col">' . $nonaca_res . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_act . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_ter . '</td>
  <td align="left" valign="middle" scope="col">' . $adm_res . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_act . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_ter . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_res . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['total']) . '</td>

</tr>';
}
include '../login/dbClose2.php';
$strExp .= chr(13) . chr(10);
$strExp .= "Total,"
    . $aca_act_tot . ","
    . $aca_ter_tot . ","
    . $aca_res_tot . ","
    . $nonaca_act_tot . ","
    . $nonaca_ter_tot . ","
    . $nonaca_res_tot . ","
    . $adm_act_tot . ","
    . $adm_ter_tot . ","
    . $adm_res_tot . ","
    . $tot_act_tot . ","
    . $tot_ter_tot . ","
    . $tot_res_tot . ","
    . $all_all_tot;
$PrintHTML .= '<tfoot>
<tr >
<th data-placeholder="" align="left" valign="middle">Total</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_act_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_ter_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $aca_res_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_act_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_ter_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $nonaca_res_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_act_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_ter_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $adm_res_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_act_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_ter_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_res_tot . '</th>
<th data-placeholder="" align="left" valign="middle">' . $all_all_tot . '</th>


</tr>
</tfoot>';

$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
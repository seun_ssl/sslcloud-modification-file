<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php

$strExp = "";
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Name of Branch,0-1 Male,0-1 Female,2-5 Male,2-5 Female,6-10 Male,6-10 Female,Greater than 11 Male,Greater than 11 Female,Total Male,Total Female,Total Staff";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >


<th data-placeholder="" align="left" valign="middle">Name of Branch</th>
<th data-placeholder="" align="left" valign="middle">0-1 Male</th>
<th data-placeholder="" align="left" valign="middle">0-1 Female</th>
<th data-placeholder="" align="left" valign="middle">2-5 Male</th>
<th data-placeholder="" align="left" valign="middle">2-5 Female</th>
<th data-placeholder="" align="left" valign="middle">6-10 Male</th>
<th data-placeholder="" align="left" valign="middle">6-10 Female</th>
<th data-placeholder="" align="left" valign="middle">Greater than 11 Male</th>
<th data-placeholder="" align="left" valign="middle">Greater than 11 Female</th>
<th data-placeholder="" align="left" valign="middle">Total Male</th>
<th data-placeholder="" align="left" valign="middle">Total Female</th>
<th data-placeholder="" align="left" valign="middle">Total Staff</th>


</tr>
</thead>

<tbody>';
//$PrintHTML="";
$Del = 0;
$a0_1_male_tol = 0;
$a0_1_female_tol = 0;
$a2_5_male_tol = 0;
$a2_5_female_tol = 0;
$a6_10_male_tol = 0;
$a6_10_female_tol = 0;
$am11_male_tol = 0;
$am11_female_tol = 0;
$tot_female_tol = 0;
$tot_male_tol = 0;
$all_all_tol = 0;
//SIMON: PUT REPORT QUERY HERE

//
// $Script="Select top(1) HashKey from KPIStart where SDate <=GETDATE() and Status='A' order by SDate Desc";

$dbOpen2 = ("
select e.BranchID, max(b.OName) branch_name, count(*) total from EmpTbl e
INNER JOIN BrhMasters b on e.BranchID = b.HashKey
  where b.Status not in('U','D')
  and e.EmpStatus in('Active')
  group by e.BranchID
  ");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    $regrouping = [];
    $regrouping_sex = [];
    if (is_numeric((trim($row2['total'])))) {
        $all_all_tol = $all_all_tol + (int) trim($row2['total']);
    } else {
        $all_all_tol += 0;

    }

    $dbOpen3 = "select t.range as [year_range], count(*) as [year_count],t.Sex
from (
  select
  Sex,
  case
    when DATEDIFF(year, EmpDt,GETDATE())  between 0 and 1 then '0-1'
    when DATEDIFF(year, EmpDt,GETDATE())  between 2 and 5 then '2-5'
	when DATEDIFF(year, EmpDt,GETDATE())  between 6 and 10 then '6-10'
    else 'greater than 11' end as range
  from EmpTbl where BranchID ='{$row2['BranchID']}' and EmpStatus ='Active' ) t
group by t.range,t.Sex";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
        $regrouping[] = $row3;
    }
    include '../login/dbClose3.php';

    $dbOpen7 = "select Sex, count(Sex) sex_count from EmpTbl where BranchID ='{$row2['BranchID']}' and EmpStatus ='Active' group by Sex";
    include '../login/dbOpen7.php';
    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_ASSOC)) {
        $regrouping_sex[] = $row7;
    }
    include '../login/dbClose7.php';

    // var_dump($regrouping);
    // var_dump($regrouping_sex);
    // var_dump($row2['total']);
    // echo "<hr/>";
    $a0_1_male = 0;
    $a0_1_female = 0;
    $a2_5_male = 0;
    $a2_5_female = 0;
    $a6_10_male = 0;
    $a6_10_female = 0;
    $am11_male = 0;
    $am11_female = 0;
    $tot_male = 0;
    $tot_female = 0;

    foreach ($regrouping_sex as $value) {
        if ($value['Sex'] === 'Male') {
            $tot_male = $value['sex_count'];
            if (is_numeric($tot_male)) {
                $tot_male_tol += (int) $tot_male;
            } else {
                $tot_male_tol += 0;

            }
        }
        if ($value['Sex'] === 'Female') {
            $tot_female = $value['sex_count'];
            if (is_numeric($tot_female)) {
                $tot_female_tol = $tot_female_tol + (int) $tot_female;
            } else {
                $tot_female_tol += 0;

            }

        }
    }
    foreach ($regrouping as $value) {
        if ($value['year_range'] === '0-1' && $value['Sex'] === 'Male') {
            $a0_1_male = $value['year_count'];
            if (is_numeric($a0_1_male)) {
                $a0_1_male_tol += (int) $a0_1_male;
            } else {
                $a0_1_male_tol += 0;

            }

        }
        if ($value['year_range'] === '0-1' && $value['Sex'] === 'Female') {
            $a0_1_female = $value['year_count'];
            if (is_numeric($a0_1_female)) {
                $a0_1_female_tol += (int) $a0_1_female;
            } else {
                $a0_1_female_tol += 0;

            }
        }
        if ($value['year_range'] === '2-5' && $value['Sex'] === 'Male') {
            $a2_5_male = $value['year_count'];
            if (is_numeric($a2_5_male)) {
                $a2_5_male_tol += (int) $a2_5_male;
            } else {
                $a2_5_male_tol += 0;

            }

        }
        if ($value['year_range'] === '2-5' && $value['Sex'] === 'Female') {
            $a2_5_female = $value['year_count'];
            if (is_numeric($a2_5_female)) {
                $a2_5_female_tol += (int) $a2_5_female;
            } else {
                $a2_5_female_tol += 0;

            }

        }
        if ($value['year_range'] === '6-10' && $value['Sex'] === 'Male') {
            $a6_10_male = $value['year_count'];
            if (is_numeric($a6_10_male)) {
                $a6_10_male_tol += (int) $a6_10_male;
            } else {
                $a6_10_male_tol += 0;

            }

        }
        if ($value['year_range'] === '6-10' && $value['Sex'] === 'Female') {
            $a6_10_female = $value['year_count'];
            if (is_numeric($a6_10_female)) {
                $a6_10_female_tol += (int) $a6_10_female;
            } else {
                $a6_10_female_tol += 0;

            }
        }
        if ($value['year_range'] === 'greater than 11' && $value['Sex'] === 'Male') {
            $am11_male = $value['year_count'];
            if (is_numeric($am11_male)) {
                $am11_male_tol += (int) $am11_male;
            } else {
                $am11_male_tol += 0;

            }

        }
        if ($value['year_range'] === 'greater than 11' && $value['Sex'] === 'Female') {
            $am11_female = $value['year_count'];
            if (is_numeric($am11_female)) {
                $am11_female_tol += (int) $am11_female;
            } else {
                $am11_female_tol += 0;

            }

        }

    }

    $strExp .= chr(13) . chr(10);
    $strExp .= (trim($row2['branch_name'])) . ","
        . $a0_1_male . ","
        . $a0_1_female . ","
        . $a2_5_male . ","
        . $a2_5_female . ","
        . $a6_10_male . ","
        . $a6_10_female . ","
        . $am11_male . ","
        . $am11_female . ","
        . $tot_male . ","
        . $tot_female . ","
        . (trim($row2['total']));

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['branch_name'])) . '</td>
  <td align="left" valign="middle" scope="col">' . $a0_1_male . '</td>
  <td align="left" valign="middle" scope="col">' . $a0_1_female . '</td>
  <td align="left" valign="middle" scope="col">' . $a2_5_male . '</td>
  <td align="left" valign="middle" scope="col">' . $a2_5_female . '</td>
  <td align="left" valign="middle" scope="col">' . $a6_10_male . '</td>
  <td align="left" valign="middle" scope="col">' . $a6_10_female . '</td>
  <td align="left" valign="middle" scope="col">' . $am11_male . '</td>
  <td align="left" valign="middle" scope="col">' . $am11_female . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_male . '</td>
  <td align="left" valign="middle" scope="col">' . $tot_female . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['total']) . '</td>

</tr>';
}
include '../login/dbClose2.php'; 

$strExp .= chr(13) . chr(10);

$strExp .= "Total,"
    . $a0_1_male_tol . ","
    . $a0_1_female_tol . ","
    . $a2_5_male_tol . ","
    . $a2_5_female_tol . ","
    . $a6_10_male_tol . ","
    . $a6_10_female_tol . ","
    . $am11_male_tol . ","
    . $am11_female_tol . ","
    . $tot_male_tol . ","
    . $tot_female_tol . ","
    . $all_all_tol;

$PrintHTML .= '
<tfoot>
<tr >


<th data-placeholder="" align="left" valign="middle">Total</th>
<th data-placeholder="" align="left" valign="middle">'.$a0_1_male_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$a0_1_female_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$a2_5_male_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$a2_5_female_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$a6_10_male_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$a6_10_female_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$am11_male_tol.'</th>
<th data-placeholder="" align="left" valign="middle">'.$am11_female_tol.'</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_male_tol . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tot_female_tol . '</th>
<th data-placeholder="" align="left" valign="middle">' . $all_all_tol . '</th>


</tr>
</tfoot>
';

$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
$dbOpen3 = "select * from [Fin_PRSettings] where Status = 'A'";
include '../login/dbOpen3.php';
$header_arry = [];
while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
    // var_dump($row3);

    for ($i = 1; $i <= 25; $i++) {
        // var_dump($row3["PayItemNm$i"]);
        if ($row3["PayItemOF$i"] == '1' && $row3["PayItemNm$i"] !== '' && !is_null($row3["PayItemNm$i"])) {
            $header_arry[strtoupper($row3["PayItemNm$i"])] = [$row3["PayItemCD$i"], $i];
        }
    }

}
include '../login/dbClose3.php';
// var_dump($header_arry);

$static_component = ['Paye', 'LEAVEALLOWANCE', 'NetPay', 'PensionEmployee', 'PensionEmployer']
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'M yy'})
  });
</script>
</head>







<?php
// ALL GROUP SELECTION
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open") && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $comp_name = "Select SetValue from Settings where Setting='CompName'";
    $comp_name = ScriptRunner($comp_name, "SetValue");

    $end = $_POST["E_RptDate"];

    // print_r($dbOpen2);

}
?>


<body oncontextmenu="return false;" topmargin="0" leftmargin="0">

<form action="#" method="post" id="attend" class="form-inline">

<!-- <div class="row col-12"> -->


																		<div class="form-group col-4 pull-right">
																		<label class="col-4">Select Month:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control col-8"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control col-8"  value="" readonly />';}
?>
																							</div>



									<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans"/>

											<!-- </div> -->
								<br/>
                                <br/>
<?php
include 'rpt_header_mod.php';
$strExp = "";
$PrintHTML = "";
?>
<?php if(isset($_REQUEST["E_RptDate"])): ?>
<table  id="table"  class="tablesorter" >
	<thead>
        <tr>
<th colspan="7" class="text-center" style="background: #fff;"> <?=$comp_name?></th>
<?php $strExp .= chr(13) . chr(10) . $comp_name;?>
</tr>
<tr>
<th colspan="7" class="text-center" style="background: #fff;">  General Journal Transaction </th>
<?php $strExp .= chr(13) . chr(10) . 'General Journal Transaction';?>
</tr>
<tr>
<th colspan="7" class="text-center" style="background: #fff;"> <?=$end?> </th>
<?php $strExp .= chr(13) . chr(10) . $end;?>


</tr>
<tr>
		<tr>
			<th>Description</th>
			<th>Account Code</th>
			<th>Credit</th>
			<th>Debit</th>
		</tr>
	</thead>
<?php $strExp .= chr(13) . chr(10) . "Description,Account Code,Credit,Debit";?>

	<tbody>
        <?php
$sum_cr = 0;
$sum_dr = 0;
$all_sum_cr = 0;
$all_sum_dr = 0;
foreach ($header_arry as $header => $val):
    $feild = "PayItem$val[1]";
    $feildname = "PayItemNm$val[1]";
    $code = $val[0];
    $codename = "PayItemCD$val[1]";

    $script = ("SELECT Sum($feild) as Sm FROM [dbo].[PayInfo_Monthly]
																WHERE $feildname = '$header'  and $codename ='$code' and Status ='A' AND MONTH([Pay Month]) = MONTH('" . $end . "') AND YEAR([Pay Month])=YEAR('" . $end . "') ");

    if ($code == 'CR' || $code == 'FP') {
        $sum_dr = ScriptRunner($script, "Sm");
        $sum_cr = 0;
        // var_dump($code);
        $all_sum_dr += $sum_dr;
    } elseif ($code == 'DR') {
    $sum_cr = ScriptRunner($script, "Sm");
    $sum_dr = 0;
    // var_dump($code);

    $all_sum_cr += $sum_cr;

}

?>

						<tr>
							<td> <?=$header?>  </td>
							<td></td>
							<td><?=number_format($sum_cr, 2)?></td>
							<td><?=number_format($sum_dr, 2)?></td>
						</tr>
            <?php $strExp .= chr(13) . chr(10) . "$header,,$sum_cr,$sum_dr";?>

		<?php endforeach;?>
    <?php foreach ($static_component as $component):
    $script = ("SELECT Sum($component) as Sm FROM [dbo].[PayInfo_Monthly]
																WHERE Status ='A' AND MONTH([Pay Month]) = MONTH('" . $end . "') AND YEAR([Pay Month])=YEAR('" . $end . "') ");

    $com_dr = ScriptRunner($script, "Sm");
    $all_sum_dr += $com_dr;

    ?>
					    	<tr>
												<td> <?=strtoupper($component)?>  </td>
												<td></td>
												<td><?=number_format(0, 2)?></td>
												<td><?=number_format($com_dr, 2)?></td>
											</tr>
            <?php $strExp .= chr(13) . chr(10) . "$component,,0,$com_dr";?>
					    <?php endforeach;?>

	</tbody>
    <tfoot>
        <tr>
            <th> Total </th>
            <th></th>
            <th><?=number_format($all_sum_cr, 2)?></th>
            <th><?=number_format($all_sum_dr, 2)?></th>
        </tr>
    </tfoot>
      <?php $strExp .= chr(13) . chr(10) . "Total,,$all_sum_cr,$all_sum_dr";?>
</table>
<?php endif; ?>


<?php
include 'rpt_footer_min.php';
?>

</form>

<?php include 'rpt_footer.php';?>
<!-- <script>

        function save() {
            document.attend.submit();
          }

</script> -->
</body>
</html>
USE [SSLCloud_0019]
GO

/****** Object:  View [dbo].[PayInfo2]    Script Date: 8/27/2021 2:46:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[PayInfo2]
AS

SELECT     dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName + ' [' + dbo.EmpTbl.EmpID + ']' AS [Full Name],dbo.EmpTbl.PENCommNo as pfa_code,dbo.EmpTbl.NIN as nin,dbo.EmpTbl.PenCustID,PR.TRelief,PR.TTaxable, dbo.EmpTbl.SName, dbo.EmpTbl.FName, dbo.EmpTbl.ONames, dbo.EmpTbl.EmpID, PR.PayItem1 AS BASIC, PR.PayItem2 AS HOUSING, PR.PayItem1, 
                      PS.PayItemNm1, PS.PayItemOF1, PR.PayItem2, PS.PayItemNm2, PS.PayItemOF2, PR.PayItem3, PS.PayItemNm3, PS.PayItemOF3, PR.PayItem4, PS.PayItemNm4, 
                      PS.PayItemOF4, PR.PayItem5, PS.PayItemNm5, PS.PayItemOF5, PR.PayItem3 AS TRANSPORT, PR.PensionEmployee, PR.PensionEmployer, PR.PAYE, PR.NetPay, 
                      PR.Gross, dbo.BrhMasters.OName, dbo.EmpTbl.Department, PR.PayItem4 AS OverTime, PR.PayItem5 AS Loan, PR.PayItem6, PS.PayItemNm6, PS.PayItemOF6, 
                      PR.PayItem7, PS.PayItemNm7, PS.PayItemOF7, PR.PayItem8, PS.PayItemNm8, PS.PayItemOF8, PR.PayItem9, PS.PayItemNm9, PS.PayItemOF9, PR.PayItem10, 
                      PS.PayItemNm10, PS.PayItemOF10, PR.PayItem11, PS.PayItemNm11, PS.PayItemOF11, PR.PayItem12, PS.PayItemNm12, PS.PayItemOF12, PR.PayItem13, 
                      PS.PayItemNm13, PS.PayItemOF13, PR.PayItem14, PS.PayItemNm14, PS.PayItemOF14, PR.PayItem15, PS.PayItemNm15, PS.PayItemOF15, PR.PayItem16, 
                      PS.PayItemNm16, PS.PayItemOF16, PR.PayItem17, PS.PayItemNm17, PS.PayItemOF17, PR.PayItem18, PS.PayItemNm18, PS.PayItemOF18, PR.PayItem19, 
                      PS.PayItemNm19, PS.PayItemOF19, PR.PayItem20, PS.PayItemNm20, PS.PayItemOF20, PR.PayItem21, PS.PayItemNm21, PS.PayItemOF21, PR.PayItem22, 
                      PS.PayItemNm22, PS.PayItemOF22, PR.PayItem23, PS.PayItemNm23, PS.PayItemOF23, PR.PayItem24, PS.PayItemNm24, PS.PayItemOF24, PR.PayItem25, 
                      PS.PayItemNm25, PS.PayItemOF25, PR.LeaveAllowance, PR.Status, dbo.EmpTbl.HashKey, dbo.EmpTbl.Mobile, dbo.EmpTbl.COO, dbo.EmpTbl.TaxID, 
                      dbo.EmpDtl.ItemPos
FROM         dbo.Fin_PRCore AS PR INNER JOIN
                      dbo.EmpTbl ON PR.EmpID = dbo.EmpTbl.HashKey INNER JOIN
                      dbo.BrhMasters ON dbo.EmpTbl.BranchID = dbo.BrhMasters.HashKey INNER JOIN
                      dbo.Fin_PRSettings AS PS ON PR.Scheme = PS.HashKey INNER JOIN
                      dbo.EmpDtl ON dbo.EmpTbl.HashKey = dbo.EmpDtl.StaffID
WHERE     (dbo.EmpTbl.EmpStatus = 'Active') AND (PR.Status IN ('A', 'U'))


GO



<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }
    </style>

    <script>
        $(function() {
            //================================ REPORT DATES ==============================================
            $("#S_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd M yy'
            })
            $("#E_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd M yy'
            })
        });
    </script>

</head>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
    <form action="#" method="post" class="form-inline mb-4">

        <div class="form-group mr-2">
            <label>Branch: </label>
            <select name="employee" id="employee" class="form-control">
                <?php
                echo '<option value="all" selected="selected">All</option>';
                $dbOpen3 = ("SELECT OName,[HashKey] FROM [BrhMasters] where Status not in('D') ORDER BY OName");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['HashKey']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['OName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group mr-2">
            <label>Employee Category : </label>


            <select name="type_cat" id="type_cat" class="form-control">

                <option value="all">All</option>
                <?php

                $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Category') ORDER BY Val1");
                include '../login/dbOpen2.php' ?>

                <?php while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                    <option <?= isset($_POST['type_cat']) && $_POST['type_cat'] === $row2['Val1'] ? 'selected' : ''  ?> value="<?= $row2['Val1'] ?>"> <?= $row2['Val1'] ?></option>';
                <?php endwhile; ?>
                <?php include '../login/dbClose2.php' ?>


            </select>


        </div>


        <div class="form-group">
            <label>From:</label>
            <?php
            if (isset($_REQUEST["S_RptDate"])) {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
            } else {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
            }
            ?>
        </div>
        <div class="form-group">
            <label>To:</label>
            <?php
            if (isset($_REQUEST["E_RptDate"])) {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
            } else {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
            }
            ?>
        </div>


        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" />
        <?php
        if (isset($_POST['SubmitTrans'])) {

            $empCat = ECh($_POST['type_cat']);
            $bran = ECh($_POST['employee']);
            $start = ECh($_POST['S_RptDate']);
            $end = ECh($_POST['E_RptDate']);
            if ($start !== '') {

                $Sdate = DateTime::createFromFormat('d M Y', $start);
                $start = $Sdate->format('Y-m-d');
            }

            if ($end !== '') {

                $Edate = DateTime::createFromFormat('d M Y', $end);
                $end = $Edate->format('Y-m-d');
            }



            // var_dump($start);
            // var_dump($end);
        }


        $strExp = "";
        include 'rpt_header.php';
        //SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
        $strExp .= "Department,Nigeria,Expatriate,Others,Total";

        $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Nigeria</th>
<th data-placeholder="" align="left" valign="middle">Expatriate</th>
<th data-placeholder="" align="left" valign="middle">Others</th>
<th data-placeholder="" align="left" valign="middle">Total</th>

</tr>
</thead>

<tbody>';
        //$PrintHTML="";
        $Del = 0;
        //SIMON: PUT REPORT QUERY HERE

        // PAYE Report

        if (
            isset($bran) && $bran !== 'all' &&
            isset($empCat) && $empCat !== 'all'
        ) {

            if ($start === "" && $end === "") {
                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            AND e.[EmpCategory] = '$empCat'
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            } else {
                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            AND e.[EmpCategory] = '$empCat'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'

                         

                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                              AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                             AND e.[EmpCategory] = '$empCat'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            }
        } elseif (
            isset($bran) && $bran !== 'all' &&
            isset($empCat) && $empCat == 'all'
        ) {

            if ($start === "" && $end === "") {

                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            
                    ) AS EXPATRIATE

                    ,
                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                         AND e.[BranchID] = '$bran'
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            } else {
                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[BranchID] = '$bran'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS EXPATRIATE

                    ,
                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                         AND e.[BranchID] = '$bran'
                         AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            }
        } elseif (
            isset($bran) && $bran == 'all' &&
            isset($empCat) && $empCat !== 'all'
        ) {

            if ($start === "" && $end === "") {

                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                           AND e.[EmpCategory] = '$empCat'
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[EmpCategory] = '$empCat'
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                           AND e.[EmpCategory] = '$empCat'
                            
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                        AND e.[EmpCategory] = '$empCat'
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            } else {
                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                           AND e.[EmpCategory] = '$empCat'
                          AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                            AND e.[EmpCategory] = '$empCat'
                             AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                           AND e.[EmpCategory] = '$empCat'
                            AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                        AND e.[EmpCategory] = '$empCat'
                         AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            }
        } elseif (
            isset($bran) && $bran == 'all' &&
            isset($empCat) && $empCat == 'all'
        ) {

            if ($start === "" && $end === "") {

                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                          
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                           
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                          
                            
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                      
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            } else {

                $dbOpen2 = ("

                SELECT 
                    Mv.Val1,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                             AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                          
                            
                    ) AS ActiveEmp
                    ,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'NIGERIA'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                             AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                           
                            
                    ) AS NIGERIA
                    ,

                        (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'EXPATRIATE'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                             AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                          
                            
                    ) AS EXPATRIATE

                    ,


                            (
                        SELECT 
                            COUNT(*)
                        FROM 
                            EmpInfo e
                        INNER JOIN 
                            Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                        INNER JOIN 
                            Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                        WHERE 
                            Department = Mv.Val1 
                            AND e.HashKey NOT IN ('057646662de2ee305tr6de611ac0d001', 'f146e0c756af0217c9bf6e4f0f2ac9ee')
                            AND PS.GName = 'OTHERS'
                        AND [Emp Status] = 'Active'
                            AND PC.[Status] = 'A'
                            AND PS.[Status] = 'A'
                             AND CONVERT(DATE, e.[Added Date], 106) between '$start' and '$end'
                      
                            
                    ) AS OTHERS
                FROM 
                    MastersView Mv 
                WHERE 
                    ItemName = 'Department' 

                ORDER BY Mv.Val1

             ");
            }
        }

        // print_r($dbOpen2);
        include '../login/dbOpen2.php';

        $tol_nig = 0;
        $tol_exp = 0;
        $tol_others = 0;
        $tol_active = 0;
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $Del = $Del + 1;
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            //--New
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            if ($row2['ActiveEmp'] == 0) {
                continue;
            }
            $strExp .= chr(13) . chr(10);
            $strExp .= $row2['Val1'] . ","
                . $row2['NIGERIA'] . ","
                . $row2['EXPATRIATE'] . ","
                . $row2['OTHERS'] . ","
                . $row2['ActiveEmp'];

            $tol_nig = $tol_nig +  number_format($row2['NIGERIA']);
            $tol_exp = $tol_exp + number_format($row2['EXPATRIATE']);
            $tol_others = $tol_others + number_format($row2['OTHERS']);
            $tol_active = $tol_active + number_format($row2['ActiveEmp']);

            $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Val1'])) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['NIGERIA']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['EXPATRIATE']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['OTHERS']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['ActiveEmp']) . '</td>

</tr>';
        }
        include '../login/dbClose2.php';
        $PrintHTML .= '</tbody>';
        $PrintHTML .= '<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Total</th>
<th data-placeholder="" align="left" valign="middle">' . $tol_nig . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tol_exp . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tol_others . '</th>
<th data-placeholder="" align="left" valign="middle">' . $tol_active . '</th>


</tr>
</tfoot></table>';



        echo $PrintHTML;
        include 'rpt_footer_min.php';
        ?>
    </form>
    <?php include 'rpt_footer.php'; ?>
</body>

</html>
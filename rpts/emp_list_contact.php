<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }
    </style>
    <script>
        $(function() {
            //================================ REPORT DATES ==============================================
            $("#S_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd M yy'
            })
            $("#E_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd M yy'
            })
        });
    </script>

</head>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
    <form action="#" method="post" class="form-inline mb-4">
        <div class="form-group mr-2">
            <label>Branch: </label>
            <select name="branch" id="branch" class="form-control">
                <?php
                echo '<option value="all" selected="selected">All</option>';
                $dbOpen3 = ("SELECT OName,[HashKey] FROM [BrhMasters] where Status not in('D') ORDER BY OName");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['branch']) && $_POST['branch'] == $row3['OName']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['OName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>

        <div class="form-group mr-2">
            <label>Employee Status : </label>


            <select name="type" id="type" class="form-control">

                <option value="all">All</option>
                <?php

                $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='EmployeeStatus') ORDER BY Val1");
                include '../login/dbOpen2.php' ?>

                <?php while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                    <option <?= isset($_POST['type']) && $_POST['type'] === $row2['Val1'] ? 'selected' : ''  ?> value="<?= $row2['Val1'] ?>"> <?= $row2['Val1'] ?></option>';
                <?php endwhile; ?>
                <?php include '../login/dbClose2.php' ?>


            </select>


        </div>
        <div class="form-group mr-2">
            <label>Employee Category : </label>


            <select name="type_cat" id="type_cat" class="form-control">

                <option value="all">All</option>
                <?php

                $dbOpen2 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Employee Category') ORDER BY Val1");
                include '../login/dbOpen2.php' ?>

                <?php while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) : ?>
                    <option <?= isset($_POST['type_cat']) && $_POST['type_cat'] === $row2['Val1'] ? 'selected' : ''  ?> value="<?= $row2['Val1'] ?>"> <?= $row2['Val1'] ?></option>';
                <?php endwhile; ?>
                <?php include '../login/dbClose2.php' ?>


            </select>


        </div>


        <div class="form-group mr-2">
            <label>Department: </label>
            <select name="employee" id="employee" class="form-control">
                <?php
                echo '<option value="all" selected="selected">All</option>';
                $dbOpen3 = ("SELECT Val1, Val2 FROM Masters where (ItemName='Department'  and Status<>'D' and Val1<>'') ORDER BY Val1");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['Val1']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['Val1'])
                                                                        echo "selected"; ?>><?php echo $row3['Val1']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>

        <div class="form-group mr-2">
            <label class="col-5">Payment Group:</label>
            <select name="payGroup" id="payGroup" class="form-control col-7">
                <?php
                echo '<option value="all" selected="selected">All</option>';
                $dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['payGroup']) && $_POST['payGroup'] == $row3['HashKey']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['GName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>

        <div class="form-group mr-2">
            <label>From:</label>
            <?php
            if (isset($_REQUEST["S_RptDate"])) {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
            } else {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
            }
            ?>
        </div>
        <div class="form-group mr-2">
            <label>To:</label>
            <?php
            if (isset($_REQUEST["E_RptDate"])) {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
            } else {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
            }
            ?>
        </div>


        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" />

        <?php
        $strExp = "";
        include 'rpt_header.php';
        //SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
        $strExp .= "Full Name,Employee ID,Department,Designation,Branch Name,Payment Group,Category,Employee Status,Gender,Date of Birth,Marital Status,Email Address,Mobile Number,Employment Date,Home Address";

        $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto" class"mt-2">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Employee ID</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Designation </th>
<th data-placeholder="" align="left" valign="middle">Branch Name </th>
<th data-placeholder="" align="left" valign="middle">Payment Group</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">Employee Status</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Date of Birth</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Email Address</th>
<th data-placeholder="" align="left" valign="middle">Mobile Number</th>
<th data-placeholder="" align="left" valign="middle">Employment Date</th>
<th data-placeholder="" align="left" valign="middle">Home Address</th>


</tr>
</thead>

<tbody>';
        //$PrintHTML="";
        $Del = 0;
        //SIMON: PUT REPORT QUERY HERE


        if (isset($_POST['SubmitTrans'])) {

            $empStat = ECh($_POST['type']);
            $empCat = ECh($_POST['type_cat']);
            $empdept = ECh($_POST['employee']);
            $empgrp = ECh($_POST['payGroup']);
            $branch = ECh($_POST['branch']);
            $start = ECh($_POST['S_RptDate']);
            $end = ECh($_POST['E_RptDate']);
            if ($start !== '') {

                $Sdate = DateTime::createFromFormat('d M Y', $start);
                $start = $Sdate->format('Y-m-d');
            }

            if ($end !== '') {

                $Edate = DateTime::createFromFormat('d M Y', $end);
                $end = $Edate->format('Y-m-d');
            }


            if ($empStat === 'all'  && $empdept === 'all'  && $empgrp === 'all'  && $empCat == 'all' && $branch == 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("
                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add] 
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                   
                
                    ORDER BY [Department],[Full Name]"

                    );
                } else {

                    $dbOpen2 = ("
                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add] 
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    where CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                   
                
                    ORDER BY [Department],[Full Name]"

                    );
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp === 'all'  && $empCat == 'all' && $branch !== 'all') {

                if ($start === "" && $end === "") {


                    $dbOpen2 = ("
                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add] 
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     where b.OName  = '$branch'
                   
                
                    ORDER BY [Department],[Full Name]"

                    );
                } else {

                    $dbOpen2 = ("
                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add] 
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     where b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                   
                
                    ORDER BY [Department],[Full Name]"

                    );
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp === 'all'  && $empCat !== 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                   WHERE [EmpCategory]= '$empCat'
                
                    ORDER BY [Department],[Full Name]");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                   WHERE [EmpCategory]= '$empCat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp === 'all'  && $empCat !== 'all'  && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                   WHERE [EmpCategory]= '$empCat'
                   and b.OName  = '$branch'
                
                    ORDER BY [Department],[Full Name]");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                   WHERE [EmpCategory]= '$empCat'
                   and b.OName  = '$branch'
                   AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp !== 'all' && $empCat == 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                
                    ORDER BY [Department],[Full Name]");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp !== 'all' && $empCat == 'all' && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    Where PS.HashKey = '$empgrp'
                    and b.OName  = '$branch'
                
                    ORDER BY [Department],[Full Name]");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    Where PS.HashKey = '$empgrp'
                    and b.OName  = '$branch'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp !== 'all' && $empCat !== 'all' && $branch === 'all') {


                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                     and [EmpCategory] = '$empCat'
                
                    ORDER BY [Department],[Full Name]");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                     and [EmpCategory] = '$empCat'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat === 'all'  && $empdept === 'all'  && $empgrp !== 'all' && $empCat !== 'all' && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                     and [EmpCategory] = '$empCat'
                      and b.OName  = '$branch'
                
                    ORDER BY [Department],[Full Name]");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                     FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                     Where PS.HashKey = '$empgrp'
                     and [EmpCategory] = '$empCat'
                      and b.OName  = '$branch'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                
                    ORDER BY [Department],[Full Name]");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp === 'all'  && $empCat === 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp === 'all'  && $empCat === 'all' && $branch !== 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp === 'all'  && $empCat !== 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] =  '$empCat'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] =  '$empCat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp === 'all'  && $empCat !== 'all'  && $branch !== 'all') {
                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] =  '$empCat'
                    and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                     INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] =  '$empCat'
                    and b.OName  = '$branch'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp !== 'all' && $empCat === 'all' && $branch === 'all') {
                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp !== 'all' && $empCat === 'all' && $branch !== 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and b.OName  = '$branch'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp !== 'all' && $empCat !== 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] = '$empCat'
                    and PS.HashKey = '$empgrp'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] = '$empCat'
                    and PS.HashKey = '$empgrp'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept === 'all'  && $empgrp !== 'all' && $empCat !== 'all' && $branch !== 'all') {


                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] = '$empCat'
                    and PS.HashKey = '$empgrp'
                      and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Emp Status] = '$empStat'
                    and [EmpCategory] = '$empCat'
                    and PS.HashKey = '$empgrp'
                      and b.OName  = '$branch'
                      AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp === 'all' && $empCat === 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'

                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp === 'all' && $empCat === 'all' && $branch !== 'all') {

                if ($start === "" && $end === "") {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and b.OName  = '$branch'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp === 'all' && $empCat !== 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and [EmpCategory] = '$empCat'
                    ORDER BY [Department],[Full Name] ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and [EmpCategory] = '$empCat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name] ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp === 'all' && $empCat !== 'all'  && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and [EmpCategory] = '$empCat'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name] ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                    and [EmpCategory] = '$empCat'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name] ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat === 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and PS.HashKey = '$empgrp'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and PS.HashKey = '$empgrp'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat === 'all' && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat !== 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and [EmpCategory] = '$empCat'
                     and PS.HashKey = '$empgrp'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and [EmpCategory] = '$empCat'
                     and PS.HashKey = '$empgrp'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat === 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat !== 'all'  && $branch !== 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and [EmpCategory] = '$empCat'
                     and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                    WHERE [Department] = '$empdept'
                     and [EmpCategory] = '$empCat'
                     and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp === 'all'  && $empCat === 'all' && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp === 'all'  && $empCat === 'all' && $branch !== 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp === 'all'  && $empCat !== 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    And [EmpCategory] = '$empCat'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    And [EmpCategory] = '$empCat'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp === 'all'  && $empCat !== 'all'  && $branch !== 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    And [EmpCategory] = '$empCat'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {

                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    And [EmpCategory] = '$empCat'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat === 'all'  && $branch === 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat === 'all'  && $branch !== 'all') {

                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]
                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]
                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat !== 'all'  && $branch === 'all') {


                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and [EmpCategory]= '$empCat'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and [EmpCategory]= '$empCat'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            } else if ($empStat !== 'all' &&  $empdept !== 'all' && $empgrp !== 'all' && $empCat !== 'all'  && $branch !== 'all') {
                if ($start === "" && $end === "") {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and [EmpCategory]= '$empCat'
                     and b.OName  = '$branch'
                    ORDER BY [Department],[Full Name]


                    ");
                } else {
                    $dbOpen2 = ("

                    SELECT Distinct e.[Full Name2] as [Full Name]
                    ,e.[Employee ID] as emp_id
                        ,e.[Department]
                        ,e.[Email]
                        ,e.[Phone #] as phone
                        ,e.[Next of Kin1]
                        ,e.[NOK1Rel]
                        ,e.[Phone-NOK1]
                        ,CAST(e.[NOK1Add] AS VARCHAR(MAX)) AS [NOK1Add]
                        ,e.[Nationality]
                        ,e.[LGA]
                        ,e.[State of Origin] as SOO
                        ,e.[SalBank]
                        ,e.[Emp_type]
                        ,e.[SalAcctNo]
                        ,e.[bank_code]
                        ,e.[PENCOM #]
                        ,e.[pfa_code]
                        ,e.[Tax ID]
                        ,e.[HInsurNo]
                        ,e.[PenCustID]
                        ,e.[Designation]
                        ,e.[Date of Birth] as dob
                        ,e.[exitreasons]
                        ,e.[NIN]
                        ,CAST(e.[Home Addr] AS VARCHAR(MAX)) AS [Home Addr]
                        ,e.[EmpCategory]
                        ,e.[Line Manager]
                        ,e.[Sex]
                        ,e.[ExitDt]
                        ,e.[Marital Status]
                        ,e.[Emp Status]
                        ,e.[EmpDt]
                        ,e.[Status Date]
                        ,PS.[GName]
                        , b.OName branch_name
                    FROM [dbo].[EmpInfo] e
                    INNER JOIN BrhMasters b on e.BranchID = b.HashKey
                    FULL JOIN Fin_PRCore AS PC ON e.HashKey = PC.EmpID 
                    FULL JOIN Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey
                
                    WHERE [Department] = '$empdept'
                    And [Emp Status] = '$empStat'
                    and PS.HashKey = '$empgrp'
                    and [EmpCategory]= '$empCat'
                     and b.OName  = '$branch'
                     AND CONVERT(DATE, e.[Emp Start Date], 106) between '$start' and '$end'
                    ORDER BY [Department],[Full Name]


                    ");
                }
            }
        }




        include '../login/dbOpen2.php';

        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $Del = $Del + 1;
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            //--New
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            if (!isset($row2['Full Name'])) {
                continue;
            }
            $act_no = $row2['SalAcctNo'] ?     "'" . $row2['SalAcctNo'] : '';
            $bank_code =  $row2['bank_code'] ? "'" . $row2['bank_code'] : '';
            $pfa_code = $row2['pfa_code'] ?  "'" . $row2['pfa_code'] : '';
            $tax_id = $row2['Tax ID'] ?   "'" . $row2['Tax ID'] : '';
            $nhf =  $row2['HInsurNo'] ?  "'" . $row2['HInsurNo'] : '';
            $addr =  $row2['Home Addr'] ?  "'" . $row2['Home Addr'] . "'" : '';
            $phone =  $row2['phone'] ?  "'" . $row2['phone'] : '';
            $dob =  $row2['dob'] ?  "'" . $row2['dob'] : '';
            $EmpDt =  $row2['EmpDt'] ?  "'" . $row2['EmpDt'] : '';


            $strExp .= chr(13) . chr(10);
            $strExp .= $row2['Full Name'] . ","
                . $row2['emp_id'] . ","
                . $row2['Department'] . ","
                . $row2['Designation'] . ","
                . $row2['branch_name'] . ","
                . $row2['GName'] . ","
                . $row2['EmpCategory'] . ","
                . $row2['Emp Status'] . ","
                . $row2['Sex'] . ","
                . $dob . ","
                . $row2['Marital Status'] . ","
                . $row2['Email'] . ","
                . $phone . ","
                . $EmpDt . ","
                . $addr;

            $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['emp_id']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Designation']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['branch_name']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['GName']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['EmpCategory']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Emp Status'] . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Sex']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['dob'] . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Marital Status']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Email']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['phone']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['EmpDt']) . '</td>
    <td align="left" valign="middle" scope="col">' . trim($row2['Home Addr']) . '</td>
   
    </tr>';
        }
        include '../login/dbClose2.php';

        $PrintHTML .= '</tbody></table>';
        echo $PrintHTML;
        include 'rpt_footer_min.php';
        ?>
    </form>
    <?php include 'rpt_footer.php'; ?>
</body>

</html>
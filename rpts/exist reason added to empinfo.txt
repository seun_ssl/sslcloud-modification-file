USE [SSLCloud_0019]
GO

/****** Object:  View [dbo].[EmpInfo]    Script Date: 7/13/2023 9:44:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/* Batch submitted through debugger: SQLQuery3.sql|0|0|C:\Users\Onyema Joseph\AppData\Local\Temp\~vsCC4D.sql*/
CREATE VIEW [dbo].[EmpInfo]
AS
SELECT     EmpID AS [Employee ID], FName AS [First Name], SName AS [Last Name], Sex,NIN,CONVERT(Varchar(11), EmpDt, 106) AS EmpDt,EmpCategory, EmpType AS [Emp Type], Email, Mobile AS [Phone #], Department, exitreasons,
                      EmpStatus AS [Emp Status],
                          (SELECT     '[' + EmpID + '] ' + FName + ' ' + SName AS MgrName
                            FROM          dbo.EmpTbl
                            WHERE      (EmpTbl_1.EmpMgr = HashKey)) AS [Line Manager],

							 (SELECT   FName + ' ' + SName + '-' + SUBSTRING(EmpID,9,4)  AS MgrName
                            FROM          dbo.EmpTbl
                            WHERE      (EmpTbl_1.EmpMgr = HashKey)) AS [Line ManagerTrim],


                          (SELECT     TOP (1) ItemPos
                            FROM          dbo.EmpDtl
                            WHERE      (ItemCode = 'Emp') AND (StaffID = EmpTbl_1.HashKey)
                            ORDER BY ItemStart DESC) AS Designation,
                          (SELECT     SetValue2
                            FROM          dbo.Settings
                            WHERE      (SetValue =
                                                       (SELECT     TOP (1) ItemLevel
                                                         FROM          dbo.EmpDtl AS EmpDtl_1
                                                         WHERE      (ItemCode = 'Emp') AND (StaffID = EmpTbl_1.HashKey)
                                                         ORDER BY ItemStart)) AND (Setting = 'JobRoleLevel')) AS JobLevel, CONVERT(Varchar(11), DOB, 106) AS [Date of Birth], DOB, HmAdd AS [Home Addr], 
                      AddedBy, CONVERT(Varchar(11), AddedDate, 106) AS [Added Date], UpdatedBy, CONVERT(Varchar(11), UpdatedDate, 106) AS [Updated Date], 
                      NOK1Name AS [Next of Kin1], NOK1Mobile AS [Phone-NOK1], NOK2Name AS [Next of Kin2], NOK2Mobile AS [Phone-NOK2], MStatus AS [Marital Status], 
                      CONVERT(Varchar(11), EmpDt, 106) AS [Emp Start Date], ONames AS [Other Names], Title, COO AS Nationality, SOO AS [State of Origin], LGA, Relg AS Religion, Phone, 
                      OffAdd AS [Office Addr], NOK1Rel, NOK2Rel, NOK1Add, NOK2Add, SalBank, SalAcctNo, PENCommNo AS [PENCOM #], HInsurNo AS [Home Insure #], Status, DeletedBy, 
                      CONVERT(Varchar(11), DeletedDate, 106) AS [Deleted Date], AuthBy, CONVERT(Varchar(11), AuthDate, 106) AS [Auth Date], MediCareNo AS [HMO #], EmpLeaveGp, 
                      SalSortCode, EmpIReason, EmpTrmDt, BloodGrp, GenoType, EmpDisabled, KnownIllment, TaxID AS [Tax ID], SName + ' ' + FName + ' [' + EmpID + ']' AS [Full Name], 
                      DATENAME(MM, DOB) + ' ' + DATENAME(DD, DOB) AS Birthday, CONVERT(Varchar(11), ExitDt, 106) AS [Status Date], BranchID, PenCustID, Expatriate, HashKey
FROM         dbo.EmpTbl AS EmpTbl_1




GO



<?php session_start( );
include '../login/scriptrunner.php';
$Load_JQuery_Home=false; $Load_MsgBox=false; $Load_JQueryPopUp=false; $Load_YesNo=true; $Load_JQuery=true; $Load_JQuery_DataSet=false; $Load_ImgSwap=true; $Load_Mult_Select=true; $Load_TableSorter=true; include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">


<button onclick="goBack()">Go Back</button>
<script>
function goBack() {
    // window.history.back();
    parent.ShowDisp('Download Employee KPI Details-Download_Employee_KPI_Details','rpts/Download_Employee_KPI_Details.php?HID=KPI&amp;rn=Download Employee KPI Details&amp;PgName=Download+Employee+KPI+Details&amp;PgType=RptPg',600,1120,'Yes');
}
</script>
&nbsp;&nbsp;&nbsp;

<?php
$strExp =""; 
//include 'rpt_header.php';
if(isset($_REQUEST['AID'])){
	$AID=$_REQUEST['AID'];
}else{
	$AID="";
}

if(isset($_REQUEST['key'])){
	$hkey=$_REQUEST['key'];
}else{
	$hkey="";
}

if(isset($_REQUEST['name'])){
	$emp_name=strtoupper($_REQUEST['name']);	
}else{
	$emp_name="";
}

if(isset($_REQUEST['name2'])){
	$mgr_name=strtoupper($_REQUEST['name2']);
}else{
	$mgr_name="";
}




print "<a href=\"download_excel_report2.php?hash=$AID&emp_name=$emp_name&mgr_name=$mgr_name\"><img src=\"../images/_dwn_.jpg\"  width=\"25\" ></a>
&nbsp;&nbsp;&nbsp;&nbsp;
<a href=\"javascript:window.print()\"><img src=\"../images/icon_print_.gif\"  width=\"25\" ></a>
<br/><br/>
";

//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Area of Focus,Sub Area of Focus,Departmental Objectives,Individual Performance Objectives,Weightage,Employee Score,Supervisor Score";


$PrintHTML='<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >
<th data-placeholder="" align="left" valign="middle">Area of Focus</th>
<th data-placeholder="" align="left" valign="middle">Sub Area of Focus</th>
	<th data-placeholder="" align="left" valign="middle">Departmental Objectives</th>
	<th data-placeholder="" align="left" valign="middle">Individual Performance Objectives</th>
	<th data-placeholder="" align="left" valign="middle">Weightage</th>
	<th data-placeholder="" align="left" valign="middle">Employee ('.$emp_name.') Score</th>
	<th data-placeholder="" align="left" valign="middle">Employee Comment</th>
	<th data-placeholder="" align="left" valign="middle">Supervisor ('.$mgr_name.') Score</th>
	<th data-placeholder="" align="left" valign="middle">Supervisor Comment</th>
</tr>
</thead>


<tbody>';
//$PrintHTML="";



$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//

$dbOpen2 = ("
	
SELECT [KRA]
      ,[KRA_Sub_Area]
      ,[ARemark]
      ,[KPI]
	  ,[HashKey]
      ,[Weightage]
      ,[AScoreEmp]
	  ,[EComm]
      ,[AScoreMrg]
	  ,[MComm]
	  ,[KRA_Group]
	  ,[FinalHashKey] 
FROM [dbo].[KPIIndvScore]
WHERE [Status] <>'D'
AND [AID] = '$AID' AND [FinalHashKey] = '$hkey' 
ORDER BY KRA_Group
");
include '../login/dbOpen2.php';

$TWeightage=0;
$TAScoreEmp=0;
$TAScoreMrg=0;

$prevValue = ""; //group

while( $row2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_BOTH))
{
	/*
	//------------------------------- extended ----------------------------------
	$Script=("Select SUM(AScoreMrg) TotMgrScore, SUM(AScoreEmp) TotAScoreEmp  from KPIIndvScore WHERE FinalHashKey = '".$row2['HashKey']."' AND Status <> 'D'");
			$TotAScoreEmp = ScriptRunner($Script,"TotAScoreEmp");
			$TotMgrScore = ScriptRunner($Script,"TotMgrScore");
	//---------------------------------------------------------------------------
	*/
	
	$Del = $Del + 1;
	
	$TWeightage=$TWeightage+$row2['Weightage'];
	$TAScoreEmp=$TAScoreEmp+$row2['AScoreEmp'];
	$TAScoreMrg=$TAScoreMrg+$row2['AScoreMrg'];
	//$TAScoreEmp=$TAScoreEmp+$TotAScoreEmp;
	//$TAScoreMrg=$TAScoreMrg+$TotMgrScore;
		
	$strExp .= chr(13).chr(10);
	$strExp .= $row2['KRA'].","  
	.$row2['KRA_Sub_Area'].","
	.$row2['KPI'].","
	.$row2['ARemark'].","
	.$row2['Weightage'].","
	.$row2['AScoreEmp'].","
	.$row2['EComm'].","
	.$row2['AScoreMrg'].","
	.$row2['MComm'];
	
	$KRA_Group_value = $row2['KRA_Group']; //group

	if((isset($KRA_Group_value))&&($KRA_Group_value != $prevValue)){ //group
		
		$PrintHTML .= "
		<tr>
			<td align=\"center\"><b>$KRA_Group_value</b></td>   
		</tr> 
		";
		
		
		$prevValue = $KRA_Group_value;
	}	//group
		
		$PrintHTML .='<tr>
		<td align="left" valign="middle" scope="col">'.(trim($row2['KRA'])).'</td>
		<td align="left" valign="middle" scope="col">'.(trim($row2['KRA_Sub_Area'])).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['KPI']).'</td>
		<td align="left" valign="middle" scope="col">'.(trim($row2['ARemark'])).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['Weightage']).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['AScoreEmp']).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['EComm']).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['AScoreMrg']).'</td>
		<td align="left" valign="middle" scope="col">'.trim($row2['MComm']).'</td>
		</tr>';
		
		$Script28="Select * from KPIFinalScore where HashKey = '".$row2['FinalHashKey']."'";
}


//-------------------------------------

$EmpStrenght=ScriptRunner($Script28,"EmpStrenght");
$EmpWeakness=ScriptRunner($Script28,"EmpWeakness");
$EmpPerfGap=ScriptRunner($Script28,"EmpPerfGap");
$EmpRemAct=ScriptRunner($Script28,"EmpRemAct");
$MedComment=ScriptRunner($Script28,"MedComment");

$PrintHTML .= '
<tr>
		<td align="left" valign="middle" scope="col" colspan="9">&nbsp;</td>		
</tr>
<tr>
		<td align="left" valign="middle" scope="col"><b>Employee\'s Strength(s):</b></td>
		<td align="left" valign="middle" scope="col" colspan="9">'.$EmpStrenght.'</td>
</tr>
<tr>
		<td align="left" valign="middle" scope="col"><b>Staff weakness:</b></td>
		<td align="left" valign="middle" scope="col" colspan="9">'.$EmpWeakness.'</td>
</tr>
<tr>
		<td align="left" valign="middle" scope="col"><b>Identify Employee\'s Performance Gaps:</b></td>
		<td align="left" valign="middle" scope="col" colspan="9">'.$EmpPerfGap.'</td>
</tr>
<tr>
		<td align="left" valign="middle" scope="col"><b>State Remedial Actions that will be relevant over the next FY:</b></td>
		<td align="left" valign="middle" scope="col" colspan="9">'.$EmpRemAct.'</td>
</tr>
<tr>
		<td align="left" valign="middle" scope="col"><b>Medical Fitness and Alertness:</b></td>
		<td align="left" valign="middle" scope="col" colspan="9">'.$MedComment.'</td>
</tr>


';

//------------------------------------


$TWeightage=($TWeightage/2);
$TAScoreEmp=($TAScoreEmp/2);
$TAScoreMrg=($TAScoreMrg/2);

$PrintHTML .='
<tfoot>
<tr>
<td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col"><b>'.$TWeightage.'</b></td>
<td align="left" valign="middle" scope="col"><b>'.$TAScoreEmp.'</b></td>
<td align="left" valign="middle" scope="col"></td>
<td align="left" valign="middle" scope="col"><b>'.$TAScoreMrg.'</b></td>
<td align="left" valign="middle" scope="col"></td>
</tr>
</tfoot>
';


include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';

echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }
    </style>
    <script>
        $(function() {
            //================================ REPORT DATES ==============================================
            $("#S_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
            $("#E_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
        });
    </script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {
    $selectOption = $_POST['employee'];

    $from = $_POST["S_RptDate"];

    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("SELECT  REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.DOB, 105), '.', '-') AS DOB, dbo.EmpTbl.EmpID AS EmployeeID, dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName AS [Full Name], dbo.EmpTbl.EmpCategory, 
						dbo.EmpTbl.MStatus, dbo.EmpDtl.ItemPos,
	                    REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.EmpDt, 105), '.', '-') AS EmpDt, dbo.EmpTbl.Sex, dbo.EmpTbl.EmpType, dbo.EmpTbl.Status, dbo.EmpTbl.Department, dbo.EmpTbl.EmpStatus, 
						  dbo.EmpTbl.EmpMgr, dbo.Fin_PRSettings.GName, dbo.Fin_PRSettings.HashKey, dbo.Fin_PRCore.Scheme,
	                      dbo.Fin_PRCore.EmpID, dbo.EmpTbl.SName, dbo.EmpTbl.BranchID,  dbo.Fin_PRCore.exitReason, 
						  dbo.Fin_PRCore.numberOfYears,  REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.payoffDate, 105), '.', '-') AS payoffDate, 
                          REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.exitDate, 105), '.', '-') AS exitDate, dbo.BrhMasters.OName,
	                      dbo.EmpTbl.ONames, dbo.EmpTbl.FName, dbo.EmpTbl.HashKey, dbo.Fin_PRCore.lastMonthNetSalary,
						  dbo.Fin_PRCore.totalPayoffAmount, dbo.Fin_PRCore.Status
	FROM         dbo.Fin_PRCore INNER JOIN
	                      dbo.EmpTbl ON dbo.Fin_PRCore.EmpID = dbo.EmpTbl.HashKey INNER JOIN  dbo.EmpDtl ON 
	                      dbo.EmpDtl.StaffID =  dbo.EmpTbl.HashKey INNER JOIN dbo.Fin_PRSettings ON 
						  dbo.Fin_PRSettings.HashKey = dbo.Fin_PRCore.Scheme 
                          INNER JOIN dbo.BrhMasters ON dbo.BrhMasters.HashKey = dbo.EmpTbl.BranchID where dbo.Fin_PRCore.Status = 'A'
 AND dbo.BrhMasters.OName =  '" . $selectOption . "'
    AND dbo.Fin_PRCore.payoffDate BETWEEN '" . $from . "' and '" . $end . "' order by dbo.EmpTbl.SName ASC");

} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '')) {
            // var_dump(123);

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $dbOpen2 = ("SELECT  REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.DOB, 105), '.', '-') AS DOB, dbo.EmpTbl.EmpID AS EmployeeID, dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName AS [Full Name], dbo.EmpTbl.EmpCategory, 
						dbo.EmpTbl.MStatus, dbo.EmpDtl.ItemPos,
	                    REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.EmpDt, 105), '.', '-') AS EmpDt, dbo.EmpTbl.Sex, dbo.EmpTbl.EmpType, dbo.EmpTbl.Status, dbo.EmpTbl.Department, dbo.EmpTbl.EmpStatus, 
						  dbo.EmpTbl.EmpMgr, dbo.Fin_PRSettings.GName, dbo.Fin_PRSettings.HashKey, dbo.Fin_PRCore.Scheme,
	                      dbo.Fin_PRCore.EmpID, dbo.EmpTbl.SName, dbo.EmpTbl.BranchID,  dbo.Fin_PRCore.exitReason, 
						  dbo.Fin_PRCore.numberOfYears,  REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.payoffDate, 105), '.', '-') AS payoffDate, 
                          REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.exitDate, 105), '.', '-') AS exitDate, dbo.BrhMasters.OName,
	                      dbo.EmpTbl.ONames, dbo.EmpTbl.FName, dbo.EmpTbl.HashKey, dbo.Fin_PRCore.lastMonthNetSalary,
						  dbo.Fin_PRCore.totalPayoffAmount, dbo.Fin_PRCore.Status
	FROM         dbo.Fin_PRCore INNER JOIN
	                      dbo.EmpTbl ON dbo.Fin_PRCore.EmpID = dbo.EmpTbl.HashKey INNER JOIN  dbo.EmpDtl ON 
	                      dbo.EmpDtl.StaffID =  dbo.EmpTbl.HashKey INNER JOIN dbo.Fin_PRSettings ON 
						  dbo.Fin_PRSettings.HashKey = dbo.Fin_PRCore.Scheme 
                          INNER JOIN dbo.BrhMasters ON dbo.BrhMasters.HashKey = dbo.EmpTbl.BranchID where dbo.Fin_PRCore.Status = 'A'
    AND dbo.Fin_PRCore.payoffDate BETWEEN '" . $from . "' and '" . $end . "' order by dbo.EmpTbl.SName ASC");
}

if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];


    $dbOpen2 = ("SELECT  REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.DOB, 105), '.', '-') AS DOB, dbo.EmpTbl.EmpID AS EmployeeID, dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName AS [Full Name], dbo.EmpTbl.EmpCategory, 
						dbo.EmpTbl.MStatus, dbo.EmpDtl.ItemPos,
	                    REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.EmpDt, 105), '.', '-') AS EmpDt, dbo.EmpTbl.Sex, dbo.EmpTbl.EmpType, dbo.EmpTbl.Status, dbo.EmpTbl.Department, dbo.EmpTbl.EmpStatus, 
						  dbo.EmpTbl.EmpMgr, dbo.Fin_PRSettings.GName, dbo.Fin_PRSettings.HashKey, dbo.Fin_PRCore.Scheme,
	                      dbo.Fin_PRCore.EmpID, dbo.EmpTbl.SName, dbo.EmpTbl.BranchID,  dbo.Fin_PRCore.exitReason, 
						  dbo.Fin_PRCore.numberOfYears,  REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.payoffDate, 105), '.', '-') AS payoffDate, 
                          REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.exitDate, 105), '.', '-') AS exitDate, dbo.BrhMasters.OName,
	                      dbo.EmpTbl.ONames, dbo.EmpTbl.FName, dbo.EmpTbl.HashKey, dbo.Fin_PRCore.lastMonthNetSalary,
						  dbo.Fin_PRCore.totalPayoffAmount, dbo.Fin_PRCore.Status
	FROM         dbo.Fin_PRCore INNER JOIN
	                      dbo.EmpTbl ON dbo.Fin_PRCore.EmpID = dbo.EmpTbl.HashKey INNER JOIN  dbo.EmpDtl ON 
	                      dbo.EmpDtl.StaffID =  dbo.EmpTbl.HashKey INNER JOIN dbo.Fin_PRSettings ON 
						  dbo.Fin_PRSettings.HashKey = dbo.Fin_PRCore.Scheme 
                          INNER JOIN dbo.BrhMasters ON dbo.BrhMasters.HashKey = dbo.EmpTbl.BranchID where dbo.Fin_PRCore.Status = 'A'
                          AND dbo.BrhMasters.OName ='" . $selectOption . "' AND dbo.Fin_PRCore.payoffDate BETWEEN '" . $from . "' and '" . $end . "' 
                          AND dbo.Fin_PRSettings.HashKey = '" . $payGrp . "' order by dbo.EmpTbl.SName ASC");

} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];

    $dbOpen2 = ("SELECT  REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.DOB, 105), '.', '-') AS DOB, dbo.EmpTbl.EmpID AS EmployeeID, dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName AS [Full Name], dbo.EmpTbl.EmpCategory, 
						dbo.EmpTbl.MStatus, dbo.EmpDtl.ItemPos,
	                    REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.EmpDt, 105), '.', '-') AS EmpDt, dbo.EmpTbl.Sex, dbo.EmpTbl.EmpType, dbo.EmpTbl.Status, dbo.EmpTbl.Department, dbo.EmpTbl.EmpStatus, 
						  dbo.EmpTbl.EmpMgr, dbo.Fin_PRSettings.GName, dbo.Fin_PRSettings.HashKey, dbo.Fin_PRCore.Scheme,
	                      dbo.Fin_PRCore.EmpID, dbo.EmpTbl.SName, dbo.EmpTbl.BranchID,  dbo.Fin_PRCore.exitReason, 
						  dbo.Fin_PRCore.numberOfYears,  REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.payoffDate, 105), '.', '-') AS payoffDate, 
                          REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.exitDate, 105), '.', '-') AS exitDate, dbo.BrhMasters.OName,
	                      dbo.EmpTbl.ONames, dbo.EmpTbl.FName, dbo.EmpTbl.HashKey, dbo.Fin_PRCore.lastMonthNetSalary,
						  dbo.Fin_PRCore.totalPayoffAmount, dbo.Fin_PRCore.Status
	FROM         dbo.Fin_PRCore INNER JOIN
	                      dbo.EmpTbl ON dbo.Fin_PRCore.EmpID = dbo.EmpTbl.HashKey INNER JOIN  dbo.EmpDtl ON 
	                      dbo.EmpDtl.StaffID =  dbo.EmpTbl.HashKey INNER JOIN dbo.Fin_PRSettings ON 
						  dbo.Fin_PRSettings.HashKey = dbo.Fin_PRCore.Scheme 
                          INNER JOIN dbo.BrhMasters ON dbo.BrhMasters.HashKey = dbo.EmpTbl.BranchID where dbo.Fin_PRCore.Status = 'A'
        AND dbo.Fin_PRCore.payoffDate BETWEEN '" . $from . "' and '" . $end . "' 
      AND dbo.Fin_PRSettings.HashKey = '" . $payGrp . "' order by dbo.EmpTbl.SName ASC");
}
// ALL GROUP SELECTION
elseif ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] == '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] == '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];
    $pyGrp = $_POST['payGroup'];
    $from = $_POST["S_RptDate"];
    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("SELECT  REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.DOB, 105), '.', '-') AS DOB, dbo.EmpTbl.EmpID AS EmployeeID, dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName AS [Full Name], dbo.EmpTbl.EmpCategory, 
						dbo.EmpTbl.MStatus, dbo.EmpDtl.ItemPos,
	                    REPLACE(CONVERT(VARCHAR(10), dbo.EmpTbl.EmpDt, 105), '.', '-') AS EmpDt, dbo.EmpTbl.Sex, dbo.EmpTbl.EmpType, dbo.EmpTbl.Status, dbo.EmpTbl.Department, dbo.EmpTbl.EmpStatus, 
						  dbo.EmpTbl.EmpMgr, dbo.Fin_PRSettings.GName, dbo.Fin_PRSettings.HashKey, dbo.Fin_PRCore.Scheme,
	                      dbo.Fin_PRCore.EmpID, dbo.EmpTbl.SName, dbo.EmpTbl.BranchID,  dbo.Fin_PRCore.exitReason, 
						  dbo.Fin_PRCore.numberOfYears,  REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.payoffDate, 105), '.', '-') AS payoffDate, 
                          REPLACE(CONVERT(VARCHAR(10), dbo.Fin_PRCore.exitDate, 105), '.', '-') AS exitDate, dbo.BrhMasters.OName,
	                      dbo.EmpTbl.ONames, dbo.EmpTbl.FName, dbo.EmpTbl.HashKey, dbo.Fin_PRCore.lastMonthNetSalary,
						  dbo.Fin_PRCore.totalPayoffAmount, dbo.Fin_PRCore.Status
	FROM         dbo.Fin_PRCore INNER JOIN
	                      dbo.EmpTbl ON dbo.Fin_PRCore.EmpID = dbo.EmpTbl.HashKey INNER JOIN  dbo.EmpDtl ON 
	                      dbo.EmpDtl.StaffID =  dbo.EmpTbl.HashKey INNER JOIN dbo.Fin_PRSettings ON 
						  dbo.Fin_PRSettings.HashKey = dbo.Fin_PRCore.Scheme 
                          INNER JOIN dbo.BrhMasters ON dbo.BrhMasters.HashKey = dbo.EmpTbl.BranchID where dbo.Fin_PRCore.Status = 'A' AND 
    dbo.Fin_PRCore.payoffDate BETWEEN '" . $from . "' and '" . $end . "' 
    order by dbo.EmpTbl.SName ASC");
}


// print_r($dbOpen2);
?>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
    <form action="#" method="post" id="attend" class="form-inline">


        <div class="form-group">
            <label>Branch: </label>
            <select name="employee" id="employee" class="form-control">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['OName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group col-3">
            <label class="col-5">Payment Group:</label>
            <select name="payGroup" id="payGroup" class="form-control col-7">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['payGroup']) && $_POST['payGroup'] == $row3['HashKey']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['GName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group">
            <label>From:</label>
            <?php
            if (isset($_REQUEST["S_RptDate"])) {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
            } else {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
            }
            ?>
        </div>
        <div class="form-group">
            <label>To:</label>
            <?php
            if (isset($_REQUEST["E_RptDate"])) {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
            } else {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
            }
            ?>
        </div>





        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; " />


        <!-- </div> -->
        <br />
        <br />

        <?php

        $strExp = "";
        include 'rpt_header.php';

        //SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

        // $strExp .= "Last Name,First Name,Middle Name,Payment Date,PFA Code, RSA PIN,Employer Contribution,Employee Contribution,Voluntary Contribution,Payment Month, Payment Year";
        $strExp .= "Full name,Employee ID,Department,Designation,Category,Gender,Date ofBirth,Marital status,Employment Status,Employment type,Employment date,Exit date,Exit Reason,Number of years in Nizamiye,Last month Net Salary,Total Payoff Amount,Payoff Date";
        $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr>

<th data-placeholder="" align="left" valign="middle">Full name</th>
<th data-placeholder="" align="left" valign="middle">Employee ID</th>
<th data-placeholder="" align="left" valign="middle">Department</th>
<th data-placeholder="" align="left" valign="middle">Designation</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle"> Gender </th>
<th data-placeholder="" align="left" valign="middle">Date of Birth</th>
<th data-placeholder="" align="left" valign="middle">Marital status</th>
<th data-placeholder="" align="left" valign="middle">Employment Status</th>
<th data-placeholder="" align="left" valign="middle">Employment Type</th>
<th data-placeholder="" align="left" valign="middle">Employment date</th>
<th data-placeholder="" align="left" valign="middle">Exit date</th>
<th data-placeholder="" align="left" valign="middle">Exit Reason</th>
<th data-placeholder="" align="left" valign="middle">Number of years in Nizamiye</th>
<th data-placeholder="" align="left" valign="middle">Last month Net Salary</th>
<th data-placeholder="" align="left" valign="middle">Total Payoff Amount</th>
<th data-placeholder="" align="left" valign="middle">Payoff Date</th>
</tr>
</thead>
<tbody>';

        $Del = 0;
        // $total_gross = $total_tIncome = $total_paye = $total_mpay = $total_trelief = 0;
        // var_dump($dbOpen2);
        include '../login/dbOpen2.php';
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            $Del = $Del + 1;
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            //--New
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

            $PrintHTML .=
                '<tr>
                <td align="left" valign="middle" scope="col">' . $row2['Full Name'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['EmployeeID'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['ItemPos'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['EmpCategory'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['Sex'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['DOB']. '</td>
    <td align="left" valign="middle" scope="col">' . $row2['MStatus'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['EmpStatus'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['EmpType'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['exitDate'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['exitReason'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['numberOfYears'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['lastMonthNetSalary'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['totalPayoffAmount'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['payoffDate'] . '</td>
   </tr>';
                // $strExp .= chr(13) . chr(10);
            $strExp .= chr(13) . chr(10) . trim($row2['Full Name']) . ","
                . $row2['EmployeeID'] . ","
                . $row2['Department'] . ","
                . $row2['ItemPos'] . ","
                . $row2['EmpCategory'] . ","
                . $row2['Sex'] . ","
                . $row2['DOB'] . ","
                . $row2['MStatus'] . ","
                . $row2['EmpStatus'] . ","
                . $row2['EmpType'] . ","
                . $row2['EmpDt'] . ","
                . $row2['exitDate'] . ","
                . $row2['exitReason'] . ","
                . $row2['numberOfYears'] . ","
                . $row2['lastMonthNetSalary'] . ","
                . $row2['totalPayoffAmount'] . ","
                . $row2['payoffDate'] . ",";
        }

        include '../login/dbClose2.php';

        $PrintHTML .= '</tbody>

</table>';

        echo $PrintHTML;

        include 'rpt_footer_min.php';
        ?>
    </form>
    <?php include 'rpt_footer.php'; ?>


</body>

</html>
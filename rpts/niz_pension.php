<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }
    </style>
    <script>
        $(function() {
            //================================ REPORT DATES ==============================================
            $("#S_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
            $("#E_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
        });
    </script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {
    $selectOption = $_POST['employee'];

    $from = $_POST["S_RptDate"];

    $end = $_POST["E_RptDate"];

    //     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND OName='" . $selectOption . "'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");

    $dbOpen2 = ("

SELECT [Full Name2] as [Full Name]
      ,CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month]
      ,[mont]
     ,[SName]
      ,[FName]
      ,[ONames]
      ,[Mont]
      ,[yea]
      ,[Department]
      ,[bank_code]
      ,[Netpay]
       ,[PensionEmployee]
      ,[PensionEmployer]
    ,[SalAcctNo]
    ,[HInsurNo]
    ,[pfa_code]
    ,[PayItem19] as bonus
      ,[SalBank]
      ,[PENCommNo]
FROM PayInfo_Monthly2
WHERE Status = 'A'
AND OName='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' order by [Pay Month], [SName] ASC
");
} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    //     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");
    $dbOpen2 = ("
SELECT [Full Name2] as [Full Name]
      ,CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month]
      ,[mont]
      ,[SName]
      ,[FName]
      ,[ONames]
      ,[Mont]
      ,[yea]
      ,[Department]
      ,[bank_code]
       ,[PensionEmployee]
      ,[PensionEmployer]
      ,[Netpay]
      ,[HInsurNo]
      ,[pfa_code]
    ,[SalAcctNo]
    ,[PayItem19] as bonus
      ,[SalBank]
      ,[PENCommNo]
FROM PayInfo_Monthly2
WHERE Status = 'A'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' order by [Pay Month], [SName] ASC
");
}

if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
WHERE Status ='A'
AND HashKey='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "'
ORDER BY [Full Name]");
} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
  WHERE Status ='A' AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "'
	ORDER BY [Full Name]");
}
// ALL GROUP SELECTION
elseif ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] == '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] == '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];
    $pyGrp = $_POST['payGroup'];
    $from = $_POST["S_RptDate"];
    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
	WHERE Status ='A' AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' ORDER BY [Full Name] ");

    // $get_row = ("SELECT COUNT(*) ct FROM [dbo].[PayInfo_Monthly]
    // WHERE Status ='A' AND [Pay Month] BETWEEN '".$from."' and '".$end."' ORDER BY [Full Name] ");

    //     $row_counter = ScriptRunner($get_row,'ct');

}


// print_r($dbOpen2);
?>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
    <form action="#" method="post" id="attend" class="form-inline">


        <div class="form-group">
            <label>Branch: </label>
            <select name="employee" id="employee" class="form-control">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['OName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group col-3">
            <label class="col-5">Payment Group:</label>
            <select name="payGroup" id="payGroup" class="form-control col-7">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['payGroup']) && $_POST['payGroup'] == $row3['HashKey']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['GName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group">
            <label>From:</label>
            <?php
            if (isset($_REQUEST["S_RptDate"])) {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
            } else {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
            }
            ?>
        </div>
        <div class="form-group">
            <label>To:</label>
            <?php
            if (isset($_REQUEST["E_RptDate"])) {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
            } else {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
            }
            ?>
        </div>





        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; " />


        <!-- </div> -->
        <br />
        <br />

        <?php

        $strExp = "";
        include 'rpt_header.php';

        //SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

        // $strExp .= "Last Name,First Name,Middle Name,Payment Date,PFA Code, RSA PIN,Employer Contribution,Employee Contribution,Voluntary Contribution,Payment Month, Payment Year";
        $strExp .= "PFA Code, RSA PIN,Last Name,First Name,Middle Name,Employer Contribution,Employee Contribution,Voluntary Contribution,Payment Month, Payment Year";
        $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr>

<th data-placeholder="" align="left" valign="middle">PFA Code</th>
<th data-placeholder="" align="left" valign="middle">RSA PIN </th>
<th data-placeholder="" align="left" valign="middle">Last Name</th>
<th data-placeholder="" align="left" valign="middle">First Name</th>
<th data-placeholder="" align="left" valign="middle">Middle Name</th>
<th data-placeholder="" align="left" valign="middle"> Employer Contribution </th>
<th data-placeholder="" align="left" valign="middle">Employee Contribution</th>
<th data-placeholder="" align="left" valign="middle">Voluntary Contribution</th>
<th data-placeholder="" align="left" valign="middle">Payment Month</th>
<th data-placeholder="" align="left" valign="middle">Payment Year</th>
</tr>
</thead>
<tbody>';

        $Del = 0;
        // $total_gross = $total_tIncome = $total_paye = $total_mpay = $total_trelief = 0;
        // var_dump($dbOpen2);
        include '../login/dbOpen2.php';
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            // var_dump($row2);
            $Del = $Del + 1;
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            //--New
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            // $total_gross = $total_gross + $row2['Gross'];
            // $total_tIncome = $total_tIncome + $row2['TTaxable'];
            // $total_paye = $total_paye + $row2['PAYE'];
            // $total_mpay = $total_mpay + $row2['Monthly_paye'];
            // $total_trelief = $total_trelief + $row2['TRelief'];
            // $PensionEmployer = $PensionEmployer + $row2['PensionEmployer'];
            // $Pensiontotal = $Pensiontotal + $row2['Pensiontotal'];

            $PrintHTML .=
                '<tr>
                <td align="left" valign="middle" scope="col">' . $row2['pfa_code'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['PENCommNo'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['SName'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['FName'] . '</td>
                <td align="left" valign="middle" scope="col">' . $row2['ONames'] . '</td>
    <td align="left" valign="middle" scope="col">' . number_format($row2['PensionEmployer'], 2) . '</td>
    <td align="left" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
    <td align="left" valign="middle" scope="col"></td>
     <td align="left" valign="middle" scope="col">' . $row2['Mont'] . '</td>
     <td align="left" valign="middle" scope="col">' . $row2['yea'] . '</td>
   </tr>';
            $pfa_code = $row2['pfa_code'] == '' ? '' : "'" . $row2['pfa_code'];
            $strExp .= chr(13) . chr(10) . trim($pfa_code) . ","
                . $row2['PENCommNo'] . ","
                . $row2['SName'] . ","
                . $row2['FName'] . ","
                . $row2['ONames'] . ","
                . $row2['PensionEmployer'] . ","
                . $row2['PensionEmployee'] . ","
                . '' . ","
                . $row2['Mont'] . ","
                . $row2['yea'] . ",";
        }

        include '../login/dbClose2.php';

        $PrintHTML .= '</tbody>

</table>';

        echo $PrintHTML;

        include 'rpt_footer_min.php';
        ?>
    </form>
    <?php include 'rpt_footer.php'; ?>


</body>

</html>
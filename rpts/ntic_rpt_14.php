<?php session_start();
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>

<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>

</head>
<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="get">
<?php
$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
$strExp .= "Full Name,Employee ID,Branch Name,Position,Job Description,Category,NIN Number,Line Manager,Gender,Marital Status,Employement Status,Status,Start Date,Disengagement Date,Birth Date,Religion,Nationality,State of Origin,Email,Telephone,Blood Group,Next of Kin,NOK Telephone,Ticked box in Empleyment Date. YES/NO";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Employee ID</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Position</th>
<th data-placeholder="" align="left" valign="middle">Job Description</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employement Status</th>
<th data-placeholder="" align="left" valign="middle">Status</th>
<th data-placeholder="" align="left" valign="middle">Start Date</th>
<th data-placeholder="" align="left" valign="middle">Disengagement Date</th>
<th data-placeholder="" align="left" valign="middle">Birth Date</th>
<th data-placeholder="" align="left" valign="middle">Religion</th>
<th data-placeholder="" align="left" valign="middle">Nationality</th>
<th data-placeholder="" align="left" valign="middle">State of Origin</th>
<th data-placeholder="" align="left" valign="middle">Email</th>
<th data-placeholder="" align="left" valign="middle">Telephone</th>
<th data-placeholder="" align="left" valign="middle">Blood Group</th>
<th data-placeholder="" align="left" valign="middle">Next of Kin</th>
<th data-placeholder="" align="left" valign="middle">NOK Telephone</th>
<th data-placeholder="" align="left" valign="middle">Ticked box in Empleyment Date. YES/NO </th>

</tr>
</thead>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Employee ID</th>
<th data-placeholder="" align="left" valign="middle">Branch Name</th>
<th data-placeholder="" align="left" valign="middle">Position</th>
<th data-placeholder="" align="left" valign="middle">Job Description</th>
<th data-placeholder="" align="left" valign="middle">Category</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">Line Manager</th>
<th data-placeholder="" align="left" valign="middle">Gender</th>
<th data-placeholder="" align="left" valign="middle">Marital Status</th>
<th data-placeholder="" align="left" valign="middle">Employement Status</th>
<th data-placeholder="" align="left" valign="middle">Status</th>
<th data-placeholder="" align="left" valign="middle">Start Date</th>
<th data-placeholder="" align="left" valign="middle">Disengagement Date</th>
<th data-placeholder="" align="left" valign="middle">Birth Date</th>
<th data-placeholder="" align="left" valign="middle">Religion</th>
<th data-placeholder="" align="left" valign="middle">Nationality</th>
<th data-placeholder="" align="left" valign="middle">State of Origin</th>
<th data-placeholder="" align="left" valign="middle">Email</th>
<th data-placeholder="" align="left" valign="middle">Telephone</th>
<th data-placeholder="" align="left" valign="middle">Blood Group</th>
<th data-placeholder="" align="left" valign="middle">Next of Kin</th>
<th data-placeholder="" align="left" valign="middle">NOK Telephone</th>
<th data-placeholder="" align="left" valign="middle">Ticked box in Empleyment Date. YES/NO </th>

</tr>
</tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

//
$dbOpen2 = ("

SELECT e.[First Name]  + ' '+ e.[Last Name] as FullName
      ,e.[Employee ID]
      ,e.[Department]
      ,e.[Line ManagerTrim]
      ,e.[Emp Start Date]
      ,e.[Status Date]
      ,e.[Date of Birth] as Birthday
      ,e.[Religion]
      ,e.[Nationality]
      ,e.[State of Origin]
      ,e.[Email]
      ,e.[Phone #]
      ,e.[Phone-NOK1]
      ,e.[BloodGrp]
      ,e.[Designation]
      ,e.[Next of Kin1]
      ,e.[Expatriate]
      ,e.[Sex]
      ,e.[Marital Status]
      ,e.[Emp Status]
	  ,e.[Status]
	  ,e.[EmpCategory]
	  ,e.[NIN]
	  ,b.OName branch_name
FROM [dbo].[EmpInfo] e
INNER JOIN BrhMasters b on e.BranchID = b.HashKey
WHERE e.[Status] <>'D'
AND e.[HashKey] <> '057646662de2ee305tr6de611ac0d001'
AND e.[HashKey] <> 'f146e0c756af0217c9bf6e4f0f2ac9ee'
AND b.Status not in('U','D')
ORDER BY e.[Full Name]



");
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
    //--New
    //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL

    ////if ($row2['Status']=='A') {$Status='Authorized';} else {$Status='No';}
    if ($row2['Status'] == 'A') {$Status = 'Authorized';}
    if ($row2['Status'] == 'U') {$Status = 'Un-Authorized';}
    if ($row2['Status'] == 'N') {$Status = 'Un-Authorized';}
    if ($row2['Expatriate'] == '1') {$expatriate = 'YES';}else{$expatriate = 'NO'; }

    $strExp .= chr(13) . chr(10);
    $strExp .= $row2['FullName'] . ","
        . substr(trim($row2['Employee ID']), 8, -5)  . ","
        . $row2['branch_name'] . ","
        . $row2['Department'] . ","
        . $row2['Designation'] . ","
        . $row2['EmpCategory'] . ","
        . $row2['NIN'] . ","
        . $row2['Line ManagerTrim'] . ","
        . $row2['Sex'] . ","
        . $row2['Marital Status'] . ","
        . $row2['Emp Status'] . ","
        . $Status .","
        . $row2['Emp Start Date'].","
        . $row2['Status Date'].","
        . $row2['Birthday'].","
        . $row2['Religion'].","
        . $row2['Nationality'].","
        . $row2['State of Origin'].","
        . $row2['Email'].","
        . $row2['Phone #'].","
        . $row2['BloodGrp'].","
        . $row2['Next of Kin1'].","
        . $row2['Phone-NOK1'].","
        . $expatriate;

    $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['FullName'])) . '</td>
<td align="left" valign="middle" scope="col">' . substr(trim($row2['Employee ID']), 8, -5)  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['branch_name']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Designation']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['EmpCategory']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['NIN']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Line ManagerTrim']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Sex']) . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Marital Status']) . '</td>
<td align="left" valign="middle" scope="col">' . $row2['Emp Status'] . '</td>
<td align="left" valign="middle" scope="col">' . $Status . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Emp Start Date'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Status Date'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Birthday'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Religion'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Nationality'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['State of Origin'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Email'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Phone #'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['BloodGrp'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Next of Kin1'])  . '</td>
<td align="left" valign="middle" scope="col">' . trim($row2['Phone-NOK1'])  . '</td>
<td align="left" valign="middle" scope="col">' .  $expatriate . '</td>
</tr>';
}
include '../login/dbClose2.php';
$PrintHTML .= '</tbody></table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>
</body>
</html>
<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {
    $selectOption = $_POST['employee'];

    $from = $_POST["S_RptDate"];

    $end = $_POST["E_RptDate"];

//     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND OName='" . $selectOption . "'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");

    $dbOpen2 = ("

SELECT [Full Name]
,CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month]
,[NIN] as nin
,[Gross]
,[annual_Gross]
,[PayItem9] as nhf
,[annual_Nhf] as annual_nhf
 ,[PayItem1]
      ,[PayItemNm1]
      ,[PayItemOF1]
      ,[PayItem2]
      ,[PayItemNm2]
      ,[PayItemOF2]
      ,[PayItem3]
      ,[PayItemNm3]
      ,[PayItemOF3]
      ,[PayItem4]
      ,[PayItemNm4]
      ,[PayItemOF4]
      ,[PayItem5]
      ,[PayItemNm5]
      ,[PayItemOF5]
       ,[PayItem6]
      ,[PayItemNm6]
      ,[PayItemOF6]
      ,[PayItem7]
      ,[PayItemNm7]
      ,[PayItemOF7]
      ,[PayItem8]
      ,[PayItemNm8]
      ,[PayItemOF8]
      ,[PayItem9]
      ,[PayItemNm9]
      ,[PayItemOF9]
      ,[PayItem10]
      ,[PayItemNm10]
      ,[PayItemOF10]
       ,[PayItem11]
      ,[PayItemNm11]
      ,[PayItemOF11]
      ,[PayItem12]
      ,[PayItemNm12]
      ,[PayItemOF12]
      ,[PayItem13]
      ,[PayItemNm13]
      ,[PayItemOF13]
      ,[PayItem14]
      ,[PayItemNm14]
      ,[PayItemOF14]
      ,[PayItem15]
      ,[PayItemNm15]
      ,[PayItemOF15]
       ,[PayItem16]
      ,[PayItemNm16]
      ,[PayItemOF16]
      ,[PayItem17]
      ,[PayItemNm17]
      ,[PayItemOF17]
      ,[PayItem18]
      ,[PayItemNm18]
      ,[PayItemOF18]
      ,[PayItem19]
      ,[PayItemNm19]
      ,[PayItemOF19]
      ,[PayItem20]
      ,[PayItemNm20]
      ,[PayItemOF20]
      ,[PayItem21]
      ,[PayItemNm21]
      ,[PayItemOF21]
       ,[PayItem22]
      ,[PayItemNm22]
      ,[PayItemOF22]
      ,[PayItem23]
      ,[PayItemNm23]
      ,[PayItemOF23]
      ,[PayItem24]
      ,[PayItemNm24]
      ,[PayItemOF24]
      ,[PayItem25]
      ,[PayItemNm25]
      ,[PayItemOF25]
,[PensionEmployee]
,[PensionEmployer]
,[annual_PensionEmployee]
,[annual_PensionEmployer]  
,[NetPay]
,[PAYE]
,[PayItem8] as nsitf
FROM PayInfo_Monthly2
WHERE Status = 'A'
AND OName='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' order by [Pay Month]
");

} else if ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

//     $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
    //    WHERE Status = 'A'
    // AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");
    $dbOpen2 = ("

SELECT [Full Name]
,CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month]
,[NIN] as nin
,[Gross]
,[annual_Gross]
,[HashKey]
,[PayItem9] as nhf
,[annual_Nhf] as annual_nhf
 ,[PayItem1]
      ,[PayItemNm1]
      ,[PayItemOF1]
      ,[PayItem2]
      ,[PayItemNm2]
      ,[PayItemOF2]
      ,[PayItem3]
      ,[PayItemNm3]
      ,[PayItemOF3]
      ,[PayItem4]
      ,[PayItemNm4]
      ,[PayItemOF4]
      ,[PayItem5]
      ,[PayItemNm5]
      ,[PayItemOF5]
       ,[PayItem6]
      ,[PayItemNm6]
      ,[PayItemOF6]
      ,[PayItem7]
      ,[PayItemNm7]
      ,[PayItemOF7]
      ,[PayItem8]
      ,[PayItemNm8]
      ,[PayItemOF8]
      ,[PayItem9]
      ,[PayItemNm9]
      ,[PayItemOF9]
      ,[PayItem10]
      ,[PayItemNm10]
      ,[PayItemOF10]
       ,[PayItem11]
      ,[PayItemNm11]
      ,[PayItemOF11]
      ,[PayItem12]
      ,[PayItemNm12]
      ,[PayItemOF12]
      ,[PayItem13]
      ,[PayItemNm13]
      ,[PayItemOF13]
      ,[PayItem14]
      ,[PayItemNm14]
      ,[PayItemOF14]
      ,[PayItem15]
      ,[PayItemNm15]
      ,[PayItemOF15]
       ,[PayItem16]
      ,[PayItemNm16]
      ,[PayItemOF16]
      ,[PayItem17]
      ,[PayItemNm17]
      ,[PayItemOF17]
      ,[PayItem18]
      ,[PayItemNm18]
      ,[PayItemOF18]
      ,[PayItem19]
      ,[PayItemNm19]
      ,[PayItemOF19]
      ,[PayItem20]
      ,[PayItemNm20]
      ,[PayItemOF20]
      ,[PayItem21]
      ,[PayItemNm21]
      ,[PayItemOF21]
       ,[PayItem22]
      ,[PayItemNm22]
      ,[PayItemOF22]
      ,[PayItem23]
      ,[PayItemNm23]
      ,[PayItemOF23]
      ,[PayItem24]
      ,[PayItemNm24]
      ,[PayItemOF24]
      ,[PayItem25]
      ,[PayItemNm25]
      ,[PayItemOF25]
,[PensionEmployee]
,[PensionEmployer]
,[annual_PensionEmployee]
,[annual_PensionEmployer]
,[NetPay]   
,[PAYE]
,[PayItem8] as nsitf
FROM PayInfo_Monthly2
WHERE Status = 'A'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "'  order by [Pay Month]");

}

// print_r($dbOpen2);
?>

<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post" id="attend" class="form-inline">


<div class="form-group">
<label>Branch: </label>
										<select name="employee" id="employee" class="form-control">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
    echo "selected";
}
    ?>><?php echo $row3['OName']; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
									</div>


									<div class="form-group">
																		<label>From:</label>
                            <?php
if (isset($_REQUEST["S_RptDate"])) {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';} else {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';}
?>
																		</div>

																		<div class="form-group">
																		<label>To:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';}
?>
																							</div>





																		<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>


											<!-- </div> -->
								<br/>
								<br/>

<?php

$strExp = "";include 'rpt_header.php';

//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$strExp .= "Full Name,Pay Month,NIN Number,
Monthly Gross,
Annual Gross,
Monthly NHF,
Annual NHF,
Monthly Pension Employee,
Annual Pension Employee,
Monthly Pension Employer,
Annual Pension Employer,
NSITF,
Monthly Tax (PAYE),
Total Deduction,
Net Pay,
";




$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">Full Name</th>
<th data-placeholder="" align="left" valign="middle">Pay Month</th>
<th data-placeholder="" align="left" valign="middle">NIN Number</th>
<th data-placeholder="" align="left" valign="middle">Monthly Gross </th>
<th data-placeholder="" align="left" valign="middle">Annual Gross </th>
<th data-placeholder="" align="left" valign="middle">Monthly NHF </th>
<th data-placeholder="" align="left" valign="middle">Annual NHF </th>
<th data-placeholder="" align="left" valign="middle">Monthly Pension Employee </th>
<th data-placeholder="" align="left" valign="middle">Annual Pension Employee</th>
<th data-placeholder="" align="left" valign="middle">Monthly Pension Employer </th>
<th data-placeholder="" align="left" valign="middle">Annual Pension Employer</th>
<th data-placeholder="" align="left" valign="middle">NSITF</th>
<th data-placeholder="" align="left" valign="middle">Monthly Tax (PAYE)</th>
<th data-placeholder="" align="left" valign="middle">Total Deduction</th>
<th data-placeholder="" align="left" valign="middle">Net Pay</th>


</tr>
</thead>
<tbody>';

$Del = 0;
$PensionEmployee = $PensionEmployer =$aPensionEmployee = $aPensionEmployer = $mgross = $agross =$mnhf = $anhf = $netPay=$deduction = $nsitf =$paye =0;
// var_dump($dbOpen2);
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    if (isset($row2['PensionEmployee']) && $row2['PensionEmployee'] > 0) {
        $Del = $Del + 1;
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
        //--New
        //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL


          for ($i = 1; $i <= 25; $i++) {
        if ( strtolower( $row2['PayItemNm' . $i]) === 'nhf') {
          $nun = $i;

            continue;
          
        }

         if ( strtolower( $row2['PayItemNm' . $i]) === 'nsitf') {
          $nui = $i;

            break;
          
        }
    }


      $query="SELECT * from [PayInfo2] where  HashKey = '{$row2['HashKey']}'";
       $getcheck=ScriptRunnercous($query);

           for ($j = 1; $j <= 25; $j++) {
        if ( strtolower( $row2['PayItemNm' . $j]) === 'nhf') {
          $anun = $j;

            break;
          
        }

       
    }




        $rowdect= $row2['PayItem'.$nun] + $row2['PensionEmployee'] + $row2['PayItem'.$nui] + $row2['PAYE']; 
        $PensionEmployee = $PensionEmployee + $row2['PensionEmployee'];
        $PensionEmployer = $PensionEmployer + $row2['PensionEmployer']; 
        $aPensionEmployee = $aPensionEmployee + $row2['annual_PensionEmployee'];
        $aPensionEmployer = $aPensionEmployer + $row2['annual_PensionEmployer'];
        $mgross = $mgross + $row2['Gross'];
        $agross = $agross + $row2['annual_Gross']; 
        $mnhf = $mnhf + $row2['PayItem'.$nun];
        $anhf = $anhf + $getcheck['PayItem'.$anun];
        $netPay = $netPay + $row2['NetPay'];
        $nsitf = $nsitf + $row2['PayItem'.$nui];
        $paye = $paye + $row2['PAYE'];
        $deduction = $deduction + $rowdect;


       

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['Pay Month'] . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['nin'] . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['Gross'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['annual_Gross'], 2) . '</td> 
    <td align="right" valign="middle" scope="col">' . number_format($row2['PayItem'.$nun], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($getcheck['PayItem'.$anun], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['annual_PensionEmployee'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployer'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['annual_PensionEmployer'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['PayItem'.$nui] ,2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($rowdect, 2) . '</td>
    <td align="right" valign="middle" scope="col">' . number_format($row2['NetPay'], 2) . '</td>

</tr>';

        $strExp .= chr(13) . chr(10) . trim($row2['Full Name']) . ","
        . $row2['Pay Month'] . ","
        . $row2['nin'] . ","
        . $row2['Gross'] . ","
        . $row2['annual_Gross'] . "," 
        . $row2['PayItem'.$nun] . ","
        . $getcheck['PayItem'.$anun] . ","
        . $row2['PensionEmployee'] . ","
        . $row2['annual_PensionEmployee'] . ","
        . $row2['PensionEmployer'] . ","
        . $row2['annual_PensionEmployer'] . ","
        . $row2['PayItem'.$nui] . ","
        . $row2['PAYE'] . ","
        . $rowdect . ","
        . $row2['NetPay'] . ","
        ;
    }
}
include '../login/dbClose2.php';

$PrintHTML .= '</tbody>
<tfoot>
<tr >

<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>
<th data-placeholder="" align="left" valign="middle"></th>

<th data-placeholder="" align="left" valign="middle">' . number_format($mgross, 2) . '</th>
<th data-placeholder="" align="left" valign="middle">' . number_format($agross, 2) . '</th>
<th data-placeholder="" align="left" valign="middle">' . number_format($mnhf, 2) . '</th>
<th data-placeholder="" align="left" valign="middle">' . number_format($anhf, 2) . '</th>
<th data-placeholder="" align="left" valign="middle">' . number_format($PensionEmployee, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($aPensionEmployee, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($PensionEmployer, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($aPensionEmployer, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($nsitf, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($paye, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($deduction, 2) . '</th>
<th data-placeholder="" align="right" valign="middle">' . number_format($netPay, 2) . '</th>


</tr>
</tfoot>
</table>';

$strExp .= chr(13) . chr(10) . ",,,"
    . ($mgross) . ","
    . ($agross) . ","
    . ($mnhf) . ","
    . ($anhf) . ","
    . ($PensionEmployee) . ","
    . ($aPensionEmployee) . ","
    . ($PensionEmployer) . ","
    . ($aPensionEmployer) . ","
    . ($nsitf) . ","
    . ($paye) . ","
    . ($deduction) . ","
    . ($netPay) . ","
;
echo $PrintHTML;

include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>


</body>
</html>
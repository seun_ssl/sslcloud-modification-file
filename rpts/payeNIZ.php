<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';


$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SSLCloud Report</title>
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .options th.narrow {
            width: 150px;
        }

        .columnSelectorWrapper {
            position: relative;
            padding: 1px 6px;
            display: inline-block;
        }

        .columnSelector,
        .hidden {
            display: none;
        }

        #colSelect1:checked+label {
            color: #307ac5;
        }

        #colSelect1:checked~#columnSelector {
            display: block;
        }

        .columnSelector {
            width: 120px;
            position: absolute;
            top: 30px;
            padding: 10px;
            background: #fff;
            border: #99bfe6 1px solid;
            border-radius: 5px;
        }

        .columnSelector label {
            display: block;
            text-align: left;
        }

        .columnSelector label:nth-child(1) {
            border-bottom: #99bfe6 solid 1px;
            margin-bottom: 5px;
        }

        .columnSelector input {
            margin-right: 5px;
        }

        .columnSelector .disabled {
            color: #ddd;
        }
    </style>
    <script>
        $(function() {
            //================================ REPORT DATES ==============================================
            $("#S_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
            $("#E_RptDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                minDate: "-60Y",
                maxDate: "+1Y",
                dateFormat: 'dd M yy'
            })
        });
    </script>
</head>

<?php


if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open") &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];


    $selectOption = $_POST['employee'];

    $payGrp = $_POST['payGroup'];

    if ($selectOption == '--' && $payGrp == '--') {


        $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
	WHERE Status ='A' AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' ORDER BY [Full Name] ");
    } elseif ($selectOption != '--' && $payGrp != '--') {


        $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
WHERE Status ='A' AND HashKey='" . $selectOption . "' AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' ORDER BY [Full Name]");
    } elseif ($selectOption == '--' && $payGrp != '--') {
        $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
WHERE Status ='A' AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "' ORDER BY [Full Name]");
    } elseif ($selectOption != '--' && $payGrp == '--') {

        $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month] FROM [dbo].[PayInfo_Monthly2]
        WHERE Status ='A'
        AND HashKey='" . $selectOption . "'
        AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "'
        ORDER BY [Full Name]");
    }
}

// print_r($dbOpen2);
?>

<body oncontextmenu="return false;" topmargin="0" leftmargin="0">
    <form action="#" method="post" id="attend" class="form-inline">


        <div class="form-group">
            <label>Branch: </label>
            <select name="employee" id="employee" class="form-control">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['OName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>
        <div class="form-group col-3">
            <label class="col-5">Payment Group:</label>
            <select name="payGroup" id="payGroup" class="form-control col-7">
                <?php
                echo '<option value="--" selected="selected">All</option>';
                $dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

                include '../login/dbOpen3.php';

                while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                    <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['payGroup']) && $_POST['payGroup'] == $row3['HashKey']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row3['GName']; ?></option>
                <?php
                }
                include '../login/dbClose3.php';
                ?>
            </select>
        </div>

        <div class="form-group">

            <label>From:</label>
            <?php
            if (isset($_REQUEST["S_RptDate"])) {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';
            } else {
                echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';
            }
            ?>
        </div>

        <div class="form-group">
            <label>To:</label>
            <?php
            if (isset($_REQUEST["E_RptDate"])) {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';
            } else {
                echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';
            }
            ?>
        </div>





        <input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; " />


        <!-- </div> -->
        <br />
        <br />

        <?php

        $strExp = "";
        include 'rpt_header.php';

        $comp_name = "Select SetValue from Settings where Setting='CompName'";
        $comp_name = ScriptRunner($comp_name, "SetValue");
        $address = "Select SetValue from Settings where Setting='CompAddress'";
        $address = ScriptRunner($address, "SetValue");
        $year = date('Y', strtotime($_REQUEST["S_RptDate"]));

        //SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT
        $strExp .= chr(13) . chr(10) . "NAME OF ORGANIZATION:$comp_name";
        $strExp .= chr(13) . chr(10) . "ADDRESS: $address";
        $strExp .= chr(13) . chr(10) . "ASSESSMENT YEAR: $year";

        $strExp .= chr(13) . chr(10) . "";

        $strExp .= "S/N,TIN,FULL NAME,PAY MONTH,DEPARTMENT,BRANCH,PAY GROUP,GROSS INCOME PER AUNNUM,GROSS INCOME PER ANNUM FOR CRA,CRA[N200k+20%GROSS INCOME,PENSION,NHIS,NHF,VOLUNTARY PENSION,LIFE ASSURANCE,GRATUITY,TOTAL RELIEF,CHARGEABLE INCOME,1ST N300000@7%,NEXT N300000@11%,NEXT N500000@15%,NEXT N500000@19%,NEXT N1600000@21%,ABOVE N3200000@24%,TAX PAYABLE PER ANNUM,MIN TAX PAYABLE PER ANNUM@1% GROSS INCOME,STATUTORY TAX PAYABLE PER ANNUM,TAX PAYABLE PER MONTH";

        // $strExp = trim($strExp);
        $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead>
<tr >

<th data-placeholder="" align="left" valign="middle">S/N</th>
<th data-placeholder="" align="left" valign="middle">TIN</th>
<th data-placeholder="" align="left" valign="middle">FULL NAME</th>
<th data-placeholder="" align="left" valign="middle">PAY MONTH</th>
<th data-placeholder="" align="left" valign="middle">DEPARTMENT</th>
<th data-placeholder="" align="left" valign="middle">BRANCH</th>
<th data-placeholder="" align="left" valign="middle">PAY GROUP</th>
<th data-placeholder="" align="left" valign="middle"> GROSS INCOME PER AUNNUM</th>
<th data-placeholder="" align="left" valign="middle"> GROSS INCOME PER ANNUM FOR CRA </th>
<th data-placeholder="" align="left" valign="middle"> CRA[N200k+20%GROSS INCOME </th>
<th data-placeholder="" align="left" valign="middle">PENSION</th>
<th data-placeholder="" align="left" valign="middle">NHIS</th>
<th data-placeholder="" align="left" valign="middle">NHF</th>
<th data-placeholder="" align="left" valign="middle">VOLUNTARY PENSION</th>
<th data-placeholder="" align="left" valign="middle"> LIFE ASSURANCE </th>
<th data-placeholder="" align="left" valign="middle"> GRATUITY </th>
<th data-placeholder="" align="left" valign="middle"> TOTAL RELIEF </th>
<th data-placeholder="" align="left" valign="middle"> CHARGEABLE INCOME </th>
<th data-placeholder="" align="left" valign="middle"> 1ST N300,000@7% </th>
<th data-placeholder="" align="left" valign="middle"> NEXT N300,000@11% </th>
<th data-placeholder="" align="left" valign="middle"> NEXT N500,000@15% </th>
<th data-placeholder="" align="left" valign="middle"> NEXT N500,000@19% </th>
<th data-placeholder="" align="left" valign="middle"> NEXT N1,600,000@21% </th>
<th data-placeholder="" align="left" valign="middle"> ABOVE N3,200,000@24% </th>
<th data-placeholder="" align="left" valign="middle">TAX PAYABLE PER ANNUM</th>
<th data-placeholder="" align="left" valign="middle">MIN. TAX PAYABLE PER ANNUM@1% GROSS INCOME</th>
<th data-placeholder="" valign="middle" align="left">STATUTORY TAX PAYABLE PER ANNUM</th>
<th data-placeholder="" valign="middle" align="left">TAX PAYABLE PER MONTH</th>


</tr>
</thead>
<tbody>';

        $Del = 0;
        $total_gross = $total_tIncome = $total_paye = $total_mpay = $total_trelief = $total_gross_annum_cra = $total_trelief_annum = $total_gross_cra  = 0;
        // var_dump($dbOpen2);
        include '../login/dbOpen2.php';
        while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
            // var_dump($row2);

            $Del = $Del + 1;
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            //--New
            //SIMON: CHANGE COLUMN NAME WITHINT THE [ ] TO THE COLUMN YOU WISH TO SPOOL
            // $total_gross = $total_gross + $row2['Gross'];
            // $total_gross_annum = $total_gross_annum + $row2['annual_Gross'];
            // $total_gross_annum_cra = $total_gross_annum_cra + $row2['gross_cra'];
            // $total_tIncome = $total_tIncome + $row2['TTaxable'];
            // $total_paye = $total_paye + $row2['annual_Paye'];
            // $total_mpay = $total_mpay + $row2['Monthly_paye'];
            // $total_trelief = $total_trelief + $row2['TRelief'];
            // $total_trelief_annum = $total_trelief_annum + $row2['annual_TRelief'];
            // $PensionEmployer = $PensionEmployer + $row2['PensionEmployer'];
            // $Pensiontotal = $Pensiontotal + $row2['Pensiontotal'];

            $paye_arry =  getPayeOrder($row2['annual_TTaxable'], $row2['GName'], $row2['annual_Gross']);

            $PrintHTML .= '<tr>
            <td height="20" align="left" valign="middle" scope="col">' . (trim($Del)) . '</td>
            <td height="20" align="left" valign="middle" scope="col">' . (trim($row2['TaxID'])) . '</td>
            <td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name2'])) . '</td>
       <td align="left" valign="middle" scope="col">' . $row2['Pay Month'] . '</td>
        <td align="left" valign="middle" scope="col">' . trim($row2['Department']) . '</td>
    <td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
       <td align="left" valign="middle" scope="col">' . $row2['GName'] . '</td>
       <td align="left" valign="middle" scope="col">' . number_format($row2['annual_Gross'], 2) . '</td>
       <td align="left" valign="middle" scope="col">' . number_format($row2['gross_cra'], 2) . '</td>
       <td align="left" valign="middle" scope="col">' . number_format(getCar($row2['annual_Gross'], $row2['gross_cra']), 2) . '</td>
       
       
       <td align="left" valign="middle" scope="col">' . number_format($row2['annual_PensionEmployee'], 2) . '</td>
       <td align="left" valign="middle" scope="col">' . number_format($row2['annual_Nsitf'], 2) . '</td>
       <td align="left" valign="middle" scope="col">' . number_format($row2['annual_Nhf'], 2) . '</td>
       <td align="left" valign="middle" scope="col"> -- </td>
       <td align="left" valign="middle" scope="col"> -- </td>
       <td align="left" valign="middle" scope="col"> -- </td>


   
<td align="right" valign="middle" scope="col">' . number_format($row2['annual_TRelief'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['annual_TTaxable'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE1'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE2'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE3'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE4'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE5'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE6'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['annual_Paye'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($paye_arry['PAYE0'], 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format(max($paye_arry['PAYE0'], $row2['annual_Paye']), 2) . '</td>
<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>

</tr>';
            $m = max($paye_arry['PAYE0'], $row2['annual_Paye']);
            $aa = getCar($row2['annual_Gross'], $row2['gross_cra']);
            $tax_id = isset($row2['TaxID']) && $row2['TaxID'] != "" ? '`' . $row2['TaxID'] : null;
            // var_dump($aa);
            $strExp .= chr(13) . chr(10) . trim($Del) . ","
                . $tax_id . ","
                . trim($row2['Full Name2']) . ","
                . $row2['Pay Month'] . ","
                . trim($row2['Department']) . ","
                . $row2['OName'] . ","
                . $row2['GName'] . ","
                . $row2['annual_Gross'] . ","
                . $row2['gross_cra'] . ","
                . $aa . ","
                . $row2['annual_PensionEmployee'] . ","
                . $row2['annual_Nsitf'] . ","
                . $row2['annual_Nhf'] . ",,,,"
                . $row2['annual_TRelief'] . ","
                . $row2['annual_TTaxable'] . ","
                . $paye_arry['PAYE1'] . ","
                . $paye_arry['PAYE2'] . ","
                . $paye_arry['PAYE3'] . ","
                . $paye_arry['PAYE4'] . ","
                . $paye_arry['PAYE5'] . ","
                . $paye_arry['PAYE6'] . ","
                . $row2['annual_Paye'] . ","
                . $paye_arry['PAYE0'] . ","
                . $m . ","
                . $row2['PAYE'] . ",";
        }

        include '../login/dbClose2.php';

        $PrintHTML .= '</tbody>

</table>';



        echo $PrintHTML;


        ?>
    </form>
    <?php include 'rpt_footer.php'; ?>


</body>

</html>
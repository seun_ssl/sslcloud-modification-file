<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
function stringToarry($str, $sep)
{

    return explode($sep, $str);
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
 	<link rel="stylesheet" href="../assets/assets/vendor_components/select2/dist/css/select2.min.css">
	<script src="../assets/assets/vendor_components/select2/dist/js/select2.full.js"></script>
		<script>


			$(function () {
			    "use strict";
			    //Initialize Select2 Elements
			    $('.select2').select2();


			  });
		</script>
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>
</head>
<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], CONVERT(Varchar(11), [EmpDt], 106) AS [EmpDt],
     REPLACE([dbo].[PayInfo_Monthly_Preview].Designation,',', '') AS Designation

     FROM [dbo].[PayInfo_Monthly_Preview]
WHERE Status IN('U','N')
AND HashKey='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "'
ORDER BY [Full Name]");

} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '') && (isset($_POST['payGroup']) && $_POST['payGroup'] != '--')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $payGrp = $_POST['payGroup'];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], CONVERT(Varchar(11), [EmpDt], 106) AS [EmpDt],
      REPLACE([dbo].[PayInfo_Monthly_Preview].Designation,',', '') AS Designation
     FROM [dbo].[PayInfo_Monthly_Preview]
  WHERE Status IN('U','N') AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' AND GrpName = '" . $payGrp . "'
	ORDER BY [Full Name]");

}
// ALL GROUP SELECTION
elseif ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] == '--') && (isset($_POST['payGroup']) && $_POST['payGroup'] == '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {

    $selectOption = $_POST['employee'];
    $pyGrp = $_POST['payGroup'];
    $from = $_POST["S_RptDate"];
    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("SELECT *, CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], CONVERT(Varchar(11), [EmpDt], 106) AS [EmpDt],
      REPLACE([dbo].[PayInfo_Monthly_Preview].Designation,',', '') AS Designation
     FROM [dbo].[PayInfo_Monthly_Preview]
	WHERE Status IN('U','N') AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' ORDER BY [Full Name] ");

// $get_row = ("SELECT COUNT(*) ct FROM [dbo].[PayInfo_Monthly_Preview]
    // WHERE Status ='A' AND [Pay Month] BETWEEN '".$from."' and '".$end."' ORDER BY [Full Name] ");

//     $row_counter = ScriptRunner($get_row,'ct');

}

?>

<body oncontextmenu="return false;"topmargin="0" leftmargin="0">

<form action="#" method="post" id="attend" class="form-inline">
	<div class="form-group row col-12">

	                                  <label  class="col-sm-4 col-form-label">Exclude Columns <span style="color: red"></span>:</label>
	                                           <div class="col-sm-8 input-group input-group-sm">
	                                             <select class="form-control select2" multiple="multiple" data-placeholder="Select wage items" style="width: 100%;" id="wage_items" name="wage_items[]">
	                                            <?php
$arr_all = [];
$query = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview]";
$coulmn_details = ScriptRunnercous($query);
$arr_all[] = ['LOAN', 'DR'];
for ($i = 1; $i <= 25; $i++) {
    if ($coulmn_details['PayItemOF' . $i] == 1) {
        $arr_all[] = [strtoupper($coulmn_details["PayItemNm" . $i]), $coulmn_details["PayItemCD" . $i]];

    }
}
foreach ($arr_all as $arr): ?>

	                                               <option value="<?php echo "$arr[0]::$arr[1]"; ?>" ><?php echo $arr[0]; ?></option>

	                                         <?php endforeach;?>

	                                             </select>

	                                            </div>

					        	</div>
<!-- <div class="row col-12"> -->
<div class="form-group col-3">
<label class="col-4">Name: </label>
										<select name="employee" id="employee" class="form-control col-8">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active' order by SName Asc");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['HashKey']) {
    echo "selected";
}
    ?>><?php echo $row3['SName'] . " " . $row3['FName'] . " " . $row3['ONames'] . " [" . $row3['EmpID'] . "]"; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
																											</div>

																											<div class="form-group col-3">
<label class="col-5">Payment Group:</label>
										<select name="payGroup" id="employee" class="form-control col-7">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT * from Fin_PRSettings where Status in ('A','U','N') order by GName Asc");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['HashKey']; ?>" <?php if (isset($_POST['payGroup']) && $_POST['payGroup'] == $row3['HashKey']) {
    echo "selected";
}
    ?>><?php echo $row3['GName']; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
                                                      </div>

                          <div class="form-group col-2">
																		<label class="col-4">From:</label>
                            <?php
if (isset($_REQUEST["S_RptDate"])) {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control col-8" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';} else {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control col-8" value="" readonly/>';}
?>
						</div>

						<div class="form-group col-2">
								<label class="col-4">To:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control col-8"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control col-8"  value="" readonly />';}
?>
						</div>







									<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>



											<!-- </div> -->
								<br/>
                                <br/>


<?php
$QueryStr = "";
$strExp2 = "";

$strExp = "";

?>
<?php
include 'rpt_header.php';

if (isset($_POST['wage_items'])
) {

    // var_dump('here');
    $wages = $_POST['wage_items'];

    $all_wage_name = [];
    foreach ($wages as $wage) {
        $all_wage_name[] = stringToarry($wage, "::")[0];
    }

    // Begin loop

    $dbOpen3 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] ";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $QueryStr = $QueryStr . '<td height="20" align="left" valign="middle" scope="col">Pay Month</td>';
                $strExp .= ",Pay Month";
            }

            if ($row3['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row3["PayItemNm" . $i]), $all_wage_name)) {
                    continue;
                } else {

                    $QueryStr = $QueryStr . "<th >" . strtoupper($row3["PayItemNm" . $i]) . "</th>";
                    $strExp = $strExp . "," . strtoupper($row3["PayItemNm" . $i]);
                }

            }
        }
        if (!in_array('LOAN', $all_wage_name)) {
            $QueryStr = $QueryStr . "<th>LOAN</th>";
            $strExp = $strExp . ",LOAN";
        }
    }
    include '../login/dbClose3.php';
    $QueryStr = $QueryStr . "<th>LATENESS</th> <th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th>
    <th>DESIGNATION</th>
    <th>GRADE</th>
    <th>DEPARTMENT</th>
    <th>EMPLOYMENT DATE</th>
    <th>BRANCH</th>";
    $strExp = $strExp . ",LATENESS,LEAVE,PENSION,PAYE,NET PAY,GROSS,DESIGNATION,GRADE,DEPARTMENT,EMPLOYMENT DATE,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

    echo '<tfoot><tr ></tr></tfoot>

<tbody>';

    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = $SumVal_Loan = $SumVal_Lateness = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        // $SumVal_Leave=$SumVal_PensionEmployee=$SumVal_PAYE=$SumVal_NetPay=$SumVal_Gross=$SumVal_Loan=$SumVal_Lateness=0;
        $from_net = [];

        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));
        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Pay Month'])) . '</td>';
                $strExp .= "," . ($row2['Pay Month']);
            }

            if ($row2['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row2["PayItemNm" . $i]), $all_wage_name)) {

                    $from_net[] = [(float) $row2['PayItem' . $i], $row2['PayItemCD' . $i]];
                    continue;
                } else {

                    $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                    $strExp .= "," . ($row2['PayItem' . $i]);
                    if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                    $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];
                }

            }
        }

        if (!in_array('LOAN', $all_wage_name)) {
            $strExp .= "," . $row2['loanAmount'];
            $PrintHTML = $PrintHTML . '
	     <td align="right" valign="middle" scope="col">' . number_format($row2['loanAmount'], 2) . '</td>';
        }
        $the_net = $row2['NetPay'];
        $the_gross = $row2['Gross'];
        foreach ($from_net as $to_net) {
            if ($to_net[0] > 0 && ($to_net[1] === 'CR' || $to_net[1] === 'FP')) {
                $the_net = $the_net - $to_net[0];
                // if($to_net[1] === 'CR'){
                //     $the_gross = $the_gross - $to_net[0];
                // }
            } elseif ($to_net[0] > 0 && $to_net[1] === 'DR') {
                $the_net = $the_net + $to_net[0];
                // $the_gross = $the_gross + $to_net[0];

            }
        }
        if (in_array('LOAN', $all_wage_name)) {
            $the_net = $the_net + $row2['loanAmount'];
            //  $the_gross = $the_gross + $row2['loanAmount'];
        }

        $strExp .= "," . $row2['LatenessAmount'] . "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . ","
            . $the_net . ","
            . $the_gross . ","
            . $row2['Designation'] . ","
            . $row2['EmpLevel'] . ","
            . $row2['Department'] . ","
            . $row2['EmpDt'] . ","
            . $row2['OName'];

        $PrintHTML = $PrintHTML . '

	<td align="right" valign="middle" scope="col">' . number_format($row2['LatenessAmount'], 2) . '</td>

	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_gross, 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Designation'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpLevel'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Loan = $SumVal_Loan + $row2['loanAmount'];
        $SumVal_Lateness = $SumVal_Lateness + $row2['LatenessAmount'];

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $the_net;
        $SumVal_Gross = $SumVal_Gross + $the_gross;
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] ";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        $strExp .= "TOTAL";

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">&nbsp;</td>';
                $strExp .= ",";
            }

            if ($row2['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row2["PayItemNm" . $i]), $all_wage_name)) {

                    continue;
                } else {

                    $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                    $strExp .= "," . ($SumVal[$i]);
                }
            }
        }
    }
    include '../login/dbClose2.php';
    if (!in_array('LOAN', $all_wage_name)) {
        $PrintHTML .= "
    <th align='right'>" . number_format($SumVal_Loan, 2) . "</th>";
    }
    $PrintHTML .= "


<th align='right'>" . number_format($SumVal_Lateness, 2) . "</th>

<th align='right'>" . number_format($SumVal_Leave, 2) . "</th>
<th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th>
</th><th>
</th><th>
</th><th>
</th><th>
</th></tfoot>";

    if (!in_array('LOAN', $all_wage_name)) {
        $strExp .= "," . ($SumVal_Loan);
    }
    $strExp .= "," . ($SumVal_Lateness) . "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);

    echo '</table>';
    echo $PrintHTML;

// End of the loop
} elseif (isset($selectOption) && ($selectOption == '--')
    && isset($pyGrp) && ($pyGrp == '--')
    && isset($from) && ($from != '')
    && isset($end) && ($end != '')) {

    // Begin loop

    $dbOpen3 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] ";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $QueryStr = $QueryStr . '<td height="20" align="left" valign="middle" scope="col">Pay Month</td>';
                $strExp .= ",Pay Month";
            }

            if ($row3['PayItemOF' . $i] == 1) {
                $QueryStr = $QueryStr . "<th >" . strtoupper($row3["PayItemNm" . $i]) . "</th>";
                $strExp = $strExp . "," . strtoupper($row3["PayItemNm" . $i]);
            }
        }
    }
    include '../login/dbClose3.php';
    $QueryStr = $QueryStr . "<th>LOAN</th><th>LATENESS</th> <th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th>
    <th>DESIGNATION</th>
    <th>GRADE</th>
    <th>DEPARTMENT</th>
    <th>EMPLOYMENT DATE</th>
    <th>BRANCH</th>";
    $strExp = $strExp . ",LOAN,LATENESS,LEAVE,PENSION,PAYE,NET PAY,GROSS,DESIGNATION,GRADE,DEPARTMENT,EMPLOYMENT DATE,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

    echo '<tfoot><tr ></tr></tfoot>

<tbody>';

    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = $SumVal_Loan = $SumVal_Lateness = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        // $SumVal_Leave=$SumVal_PensionEmployee=$SumVal_PAYE=$SumVal_NetPay=$SumVal_Gross=$SumVal_Loan=$SumVal_Lateness=0;
        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';
        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Pay Month'])) . '</td>';
                $strExp .= "," . ($row2['Pay Month']);
            }

            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                $strExp .= "," . ($row2['PayItem' . $i]);
                if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];
            }
        }

        $strExp .= "," . $row2['loanAmount'] . "," . $row2['LatenessAmount'] . "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . "," . $row2['NetPay'] . "," . $row2['Gross'] . ","
            . $row2['Designation'] . ","
            . $row2['EmpLevel'] . ","
            . $row2['Department'] . ","
            . $row2['EmpDt'] . ","
            . $row2['OName'];

        $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($row2['loanAmount'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['LatenessAmount'], 2) . '</td>

	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['NetPay'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['Gross'], 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Designation'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpLevel'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Loan = $SumVal_Loan + $row2['loanAmount'];
        $SumVal_Lateness = $SumVal_Lateness + $row2['LatenessAmount'];

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $row2['NetPay'];
        $SumVal_Gross = $SumVal_Gross + $row2['Gross'];
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] ";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        $strExp .= "TOTAL";

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">&nbsp;</td>';
                $strExp .= ",";
            }

            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                $strExp .= "," . ($SumVal[$i]);
            }
        }
    }
    include '../login/dbClose2.php';

    $PrintHTML .= "
<th align='right'>" . number_format($SumVal_Loan, 2) . "</th>
<th align='right'>" . number_format($SumVal_Lateness, 2) . "</th>

<th align='right'>" . number_format($SumVal_Leave, 2) . "</th>
<th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th>
</th><th>
</th><th>
</th><th>
</th><th>
</th></tfoot>";
    $strExp .= "," . ($SumVal_Loan) . "," . ($SumVal_Lateness) . "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);

    echo '</table>';
    echo $PrintHTML;

} elseif (
    isset($payGrp) && ($payGrp != '--') && isset($_POST['wage_items'])
) {

// Begin loop
    $wages = $_POST['wage_items'];
    $all_wage_name = [];

    foreach ($wages as $wage) {
        $all_wage_name[] = stringToarry($wage, "::")[0];
    }
// var_dump( $all_wage_name);

    $dbOpen3 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] WHERE GrpName = '" . $payGrp . "' ";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $QueryStr = $QueryStr . '<td height="20" align="left" valign="middle" scope="col">Pay Month</td>';
                $strExp .= ",Pay Month";
            }

            if ($row3['PayItemOF' . $i] == 1) {

                if (in_array(strtoupper($row3["PayItemNm" . $i]), $all_wage_name)) {
                    continue;
                } else {

                    $QueryStr = $QueryStr . "<th >" . strtoupper($row3["PayItemNm" . $i]) . "</th>";
                    $strExp = $strExp . "," . strtoupper($row3["PayItemNm" . $i]);
                }
            }
        }
    }
    include '../login/dbClose3.php';
    $QueryStr = $QueryStr . "<th>LOAN</th><th>LATENESS</th> <th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th>
     <th>DESIGNATION</th>
    <th>GRADE</th>
    <th>DEPARTMENT</th>
    <th>EMPLOYMENT DATE</th>
    <th>BRANCH</th>";
    $strExp = $strExp . ",LOAN,LATENESS,LEAVE,PENSION,PAYE,NET PAY,GROSS,DESIGNATION,GRADE,DEPARTMENT,EMPLOYMENT DATE,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

    echo '<tfoot><tr ></tr></tfoot>

<tbody>';

    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = $SumVal_Loan = $SumVal_Lateness = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        // $SumVal_Leave=$SumVal_PensionEmployee=$SumVal_PAYE=$SumVal_NetPay=$SumVal_Gross=$SumVal_Loan=$SumVal_Lateness=0;
        $from_net = [];

        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';
        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Pay Month'])) . '</td>';
                $strExp .= "," . ($row2['Pay Month']);
            }

            if ($row2['PayItemOF' . $i] == 1) {
                if (in_array(strtoupper($row2["PayItemNm" . $i]), $all_wage_name)) {
                    $from_net[] = [(float) $row2['PayItem' . $i], $row2['PayItemCD' . $i]];
                    continue;

                } else {
                    $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                    $strExp .= "," . ($row2['PayItem' . $i]);
                    if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                    $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];

                }
            }
        }
        $the_net = $row2['NetPay'];
        $the_gross = $row2['Gross'];
        foreach ($from_net as $to_net) {
            if ($to_net[0] > 0 && ($to_net[1] === 'CR' || $to_net[1] === 'FP')) {
                $the_net = $the_net - $to_net[0];
                // $the_gross = $the_gross - $to_net[0];
            } elseif ($to_net[0] > 0 && $to_net[1] === 'DR') {
                $the_net = $the_net + $to_net[0];
                // $the_gross = $the_gross + $to_net[0];

            }
        }

        $strExp .= "," . $row2['loanAmount'] . "," . $row2['LatenessAmount'] . "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . "," . $the_net . "," . $the_gross . ","
            . $row2['Designation'] . ","
            . $row2['EmpLevel'] . ","
            . $row2['Department'] . ","
            . $row2['EmpDt'] . ","
            . $row2['OName'];

        $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($row2['loanAmount'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['LatenessAmount'], 2) . '</td>

	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($the_gross, 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Designation'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpLevel'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Loan = $SumVal_Loan + $row2['loanAmount'];
        $SumVal_Lateness = $SumVal_Lateness + $row2['LatenessAmount'];

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $the_net;
        $SumVal_Gross = $SumVal_Gross + $the_gross;
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] WHERE GrpName = '" . $payGrp . "' ";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        $strExp .= "TOTAL";

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">&nbsp;</td>';
                $strExp .= ",";
            }

            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                $strExp .= "," . ($SumVal[$i]);
            }
        }
    }
    include '../login/dbClose2.php';

    $PrintHTML .= "
<th align='right'>" . number_format($SumVal_Loan, 2) . "</th>
<th align='right'>" . number_format($SumVal_Lateness, 2) . "</th>

<th align='right'>" . number_format($SumVal_Leave, 2) . "</th>
<th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th>
</th><th>
</th><th>
</th><th>
</th><th>
</th></tfoot>";
    $strExp .= "," . ($SumVal_Loan) . "," . ($SumVal_Lateness) . "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);

    echo '</table>';
    echo $PrintHTML;

// End of the loop

} elseif (isset($payGrp) && ($payGrp != '--')) {

// Begin loop

    $dbOpen3 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] WHERE GrpName = '" . $payGrp . "' ";
    include '../login/dbOpen3.php';
    while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
        $strExp .= "Full Name";
        $QueryStr = '<th>FULL NAME</th>'; //<td>BASIC</td><td>HOUSING</td><td>TRANSPORT</td>';

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $QueryStr = $QueryStr . '<td height="20" align="left" valign="middle" scope="col">Pay Month</td>';
                $strExp .= ",Pay Month";
            }

            if ($row3['PayItemOF' . $i] == 1) {
                $QueryStr = $QueryStr . "<th >" . strtoupper($row3["PayItemNm" . $i]) . "</th>";
                $strExp = $strExp . "," . strtoupper($row3["PayItemNm" . $i]);
            }
        }
    }
    include '../login/dbClose3.php';
    $QueryStr = $QueryStr . "<th>LOAN</th><th>LATENESS</th> <th>LEAVE</th><th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th>
     <th>DESIGNATION</th>
    <th>GRADE</th>
    <th>DEPARTMENT</th>
    <th>EMPLOYMENT DATE</th>
    <th>BRANCH</th>";
    $strExp = $strExp . ",LOAN,LATENESS,LEAVE,PENSION,PAYE,NET PAY,GROSS,DESIGNATION,GRADE,DEPARTMENT,EMPLOYMENT DATE,BRANCH";

    $PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

    echo '<tfoot><tr ></tr></tfoot>

<tbody>';

    $Del = 0;
//SIMON: PUT REPORT QUERY HERE

    $QueryStr = "";
    $SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = $SumVal_Loan = $SumVal_Lateness = 0;

    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        // $SumVal_Leave=$SumVal_PensionEmployee=$SumVal_PAYE=$SumVal_NetPay=$SumVal_Gross=$SumVal_Loan=$SumVal_Lateness=0;
        $Del = $Del + 1;
        $strExp .= chr(13) . chr(10);
        $strExp .= (trim($row2['Full Name']));

        $PrintHTML .= '<tr><td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Full Name'])) . '</td>';
        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">' . (trim($row2['Pay Month'])) . '</td>';
                $strExp .= "," . ($row2['Pay Month']);
            }

            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i], 2) . '</td>';
                $strExp .= "," . ($row2['PayItem' . $i]);
                if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
                $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i];
            }
        }

        $strExp .= "," . $row2['loanAmount'] . "," . $row2['LatenessAmount'] . "," . $row2['LeaveAllowance'] . "," . $row2['PensionEmployee'] . "," . $row2['PAYE'] . "," . $row2['NetPay'] . "," . $row2['Gross'] . ","
            . $row2['Designation'] . ","
            . $row2['EmpLevel'] . ","
            . $row2['Department'] . ","
            . $row2['EmpDt'] . ","
            . $row2['OName'];

        $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($row2['loanAmount'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['LatenessAmount'], 2) . '</td>

	<td align="right" valign="middle" scope="col">' . number_format($row2['LeaveAllowance'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['NetPay'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['Gross'], 2) . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Designation'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpLevel'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['Department'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['EmpDt'] . '</td>
	<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
	</tr>';

        $SumVal_Loan = $SumVal_Loan + $row2['loanAmount'];
        $SumVal_Lateness = $SumVal_Lateness + $row2['LatenessAmount'];

        $SumVal_Leave = $SumVal_Leave + $row2['LeaveAllowance'];
        $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee'];
        $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE'];
        $SumVal_NetPay = $SumVal_NetPay + $row2['NetPay'];
        $SumVal_Gross = $SumVal_Gross + $row2['Gross'];
//    $SumVal_PensionEmployer = $SumVal_PensionEmployer + $row2['PensionEmployer'];
    }

    $PrintHTML .= '</tbody><tfoot>';

//********************************
    //GETTING THE FOOTER VALUES ADDED
    //********************************
    include '../login/dbClose2.php';
    $dbOpen2 = "SELECT Top(1) * FROM [dbo].[PayInfo_Monthly_Preview] WHERE GrpName = '" . $payGrp . "' ";
    include '../login/dbOpen2.php';
    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $PrintHTML .= '<th></th>';
        $strExp .= chr(13) . chr(10);
        $strExp .= "TOTAL";

        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $PrintHTML = $PrintHTML . '<td height="20" align="left" valign="middle" scope="col">&nbsp;</td>';
                $strExp .= ",";
            }

            if ($row2['PayItemOF' . $i] == 1) {
                $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
                $strExp .= "," . ($SumVal[$i]);
            }
        }
    }
    include '../login/dbClose2.php';

    $PrintHTML .= "
<th align='right'>" . number_format($SumVal_Loan, 2) . "</th>
<th align='right'>" . number_format($SumVal_Lateness, 2) . "</th>

<th align='right'>" . number_format($SumVal_Leave, 2) . "</th>
<th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th>
</th><th>
</th><th>
</th><th>
</th><th>
</th></tfoot>";
    $strExp .= "," . ($SumVal_Loan) . "," . ($SumVal_Lateness) . "," . ($SumVal_Leave) . "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);
    $strExp .= chr(13) . chr(10);

    echo '</table>';
    echo $PrintHTML;

// End of the loop

}

include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>

<script>

        function save() {
            document.attend.submit();
          }

</script>

</body>
</html>

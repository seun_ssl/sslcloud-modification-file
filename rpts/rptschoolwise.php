<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;
include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {
    $selectOption = $_POST['employee'];

    $from = $_POST["S_RptDate"];

    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
   WHERE Status = 'A'
AND OName='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");

} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $dbOpen2 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], sum(NetPay) as net_pay, sum(Gross) as gross, OName from PayInfo_Monthly2
   WHERE Status = 'A'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "' group by OName,[Pay Month] order by OName");

}

// print_r($dbOpen2);
?>

<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post" id="attend" class="form-inline">


<div class="form-group">
<label>Branch: </label>
										<select name="employee" id="employee" class="form-control">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
    echo "selected";
}
    ?>><?php echo $row3['OName']; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
									</div>

									<div class="form-group">
																		<label>From:</label>
                            <?php
if (isset($_REQUEST["S_RptDate"])) {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';} else {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';}
?>
																		</div>

																		<div class="form-group">
																		<label>To:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';}
?>
																							</div>

																		<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>


											<!-- </div> -->
								<br/>
								<br/>

<?php
$QueryStr = "";
$tot_net_pay_tot = $tot_gross_pay_tot = $aca_net_tot = $aca_gross_tot = $nonaca_net_tot = $nonaca_gross_tot = $admaca_net_tot = $admaca_gross_tot = 0;
$strExp = "";
include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$QueryStr = $QueryStr . "<th>Branch </th><th>Pay Month </th><th>Academic Net Pay</th><th>Academic Gross Pay</th>
<th>Non Academic Net Pay</th><th>Non Academic Gross Pay</th>
<th>Admin Net Pay</th><th>Admin Gross Pay</th>
<th>Total Net Pay</th><th>Total Gross Pay</th>
";
$strExp = $strExp . "Branch,Pay Month,Academic Net Pay,Academic Gross Pay,Non Academic Net Pay,Non Academic Gross Pay,Admin Net Pay,Admin Gross Pay,Total Net Pay,Total Gross Pay";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

echo '<tfoot><tr ></tr></tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

$QueryStr = "";

include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;

    $branch = $row2['OName'];
    $pay_month = $row2['Pay Month'];
    $tot_net_pay = $row2['net_pay'];
    $tot_gross_pay = $row2['gross'];
    $aca_net = $aca_gross = 0;
    $nonaca_net = $nonaca_gross = 0;
    $admaca_net = $admaca_gross = 0;

    $dbOpen7 = ("select CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month], EmpCategory, sum(NetPay) as net_pay, sum(Gross) as gross, max(OName) from PayInfo_Monthly2
   WHERE Status = 'A'
    AND OName='" . $branch . "'
AND [Pay Month] ='" . $pay_month . "' group by EmpCategory, [Pay Month] ");
    include '../login/dbOpen7.php';

    while ($row7 = sqlsrv_fetch_array($result7, SQLSRV_FETCH_BOTH)) {

        if (strtoupper($row7['EmpCategory']) === 'ACADEMIC') {
            $aca_net = $row7['net_pay'];
            $aca_gross = $row7['gross'];

        }
        if (strtoupper($row7['EmpCategory']) === 'NON-ACADEMIC') {
            $nonaca_net = $row7['net_pay'];
            $nonaca_gross = $row7['gross'];

        }
        if (strtoupper($row7['EmpCategory']) === 'ADMIN') {
            $admaca_net = $row7['net_pay'];
            $admaca_gross = $row7['gross'];

        }

    }
    include '../login/dbClose7.php';
    // var_dump($tot_net_pay);
    $PrintHTML .= '<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
				 <td align="left" valign="middle" scope="col">' . $row2['Pay Month'] . '</td>';
    $PrintHTML = $PrintHTML . '
	<td align="right" valign="middle" scope="col">' . number_format($aca_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($aca_gross, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($nonaca_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($nonaca_gross, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($admaca_net, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($admaca_gross, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($tot_net_pay, 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($tot_gross_pay, 2) . '</td>
	</tr>';
    $strExp .= chr(13) . chr(10);
    $strExp .= trim($row2['OName']) . "," . $row2['Pay Month'];
    $strExp .= "," . $aca_net . "," . $aca_gross . "," . $nonaca_net . "," . $nonaca_gross
        . "," . $admaca_net
        . "," . $admaca_gross
        . "," . $tot_net_pay
        . "," . $tot_gross_pay;

    $tot_net_pay_tot += $tot_net_pay;
    $tot_gross_pay_tot += $tot_gross_pay;
    $aca_net_tot += $aca_net;
    $aca_gross_tot += $aca_gross;
    $nonaca_net_tot += $nonaca_net;
    $nonaca_gross_tot += $nonaca_gross;
    $admaca_net_tot += $admaca_net;
    $admaca_gross_tot += $admaca_gross;

}
include '../login/dbClose2.php';

$PrintHTML .= '</tbody><tfoot>';

//********************************
//GETTING THE FOOTER VALUES ADDED
//********************************

$PrintHTML .= "
               <th></th>
               <th></th>
               <th align='right'>" . number_format($aca_net_tot, 2) . "</th>
               <th align='right'>" . number_format($aca_gross_tot, 2) . "</th>
               <th align='right'>" . number_format($nonaca_net_tot, 2) . "</th>
               <th align='right'>" . number_format($nonaca_gross_tot, 2) . "</th>
               <th align='right'>" . number_format($admaca_net_tot, 2) . "</th>
               <th align='right'>" . number_format($admaca_gross_tot, 2) . "</th>
               <th align='right'>" . number_format($tot_net_pay_tot, 2) . "</th>
               <th align='right'>" . number_format($tot_gross_pay_tot, 2) . "</th>

               </tfoot>";
$strExp .= chr(13) . chr(10);

$strExp .= '' . ","
    . '' . ","
    . ($aca_net_tot) . ","
    . ($aca_gross_tot) . ","
    . ($nonaca_net_tot) . ","
    . ($nonaca_gross_tot) . ","
    . ($admaca_net_tot) . ","
    . ($admaca_gross_tot) . ","
    . ($tot_net_pay_tot) . ","
    . ($tot_gross_pay_tot);
//$strExp .=",".number_format($SumVal_Leave,2).",".number_format($SumVal_PensionEmployee,2).",".number_format($SumVal_PAYE,2).",".number_format($SumVal_NetPay,2).",".number_format($SumVal_Gross,2).",,";

echo '</table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>

<script>

        function save() {
            document.attend.submit();
          }

</script>
</body>
</html>
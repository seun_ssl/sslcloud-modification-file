<?php session_start();
error_reporting(E_ERROR | E_PARSE);
include '../login/scriptrunner.php';
$Load_JQuery_Home = false;
$Load_MsgBox = false;
$Load_JQueryPopUp = false;
$Load_YesNo = true;
$Load_JQuery = true;
$Load_JQuery_DataSet = false;
$Load_ImgSwap = true;
$Load_Mult_Select = true;
$Load_TableSorter = true;include '../css/myscripts.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SSLCloud Report</title>
 <!-- Bootstrap 4.0-->
 <link rel="stylesheet" href="../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
<style>
.options th.narrow {
width: 150px;
}
.columnSelectorWrapper {
position: relative;
padding: 1px 6px;
display: inline-block;
}
.columnSelector, .hidden {
display: none;
}
#colSelect1:checked + label {
color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
display: block;
}
.columnSelector {
width: 120px;
position: absolute;
top: 30px;
padding: 10px;
background: #fff;
border: #99bfe6 1px solid;
border-radius: 5px;
}
.columnSelector label {
display: block;
text-align: left;
}
.columnSelector label:nth-child(1) {
border-bottom: #99bfe6 solid 1px;
margin-bottom: 5px;
}
.columnSelector input {
margin-right: 5px;
}
.columnSelector .disabled {
color: #ddd;
}
</style>
<script>
  $(function()
  {
//================================ REPORT DATES ==============================================
	$("#S_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
	$("#E_RptDate").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, selectOtherMonths: true, minDate: "-60Y", maxDate: "+1Y", dateFormat: 'dd M yy'})
  });
</script>
</head>

<?php
if ((isset($_POST["SubmitTrans"]) && $_POST["SubmitTrans"] == "Open")
    && (isset($_POST['employee']) && $_POST['employee'] != '--') &&
    (isset($_POST["S_RptDate"]) && $_POST["S_RptDate"] != '') && (isset($_POST["E_RptDate"]) && $_POST["E_RptDate"] != '')
) {
    $selectOption = $_POST['employee'];

    $from = $_POST["S_RptDate"];

    $end = $_POST["E_RptDate"];

    $dbOpen2 = ("SELECT
CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month],
sum(PayItem1) as PayItem1_,
sum(PayItem2) as PayItem2_,
sum(PayItem3) as PayItem3_,
sum(PayItem4) as PayItem4_,
sum(PayItem5) as PayItem5_,
sum(PayItem6) as PayItem6_,
sum(PayItem7) as PayItem7_,
sum(PayItem8) as PayItem8_,
sum(PayItem9) as PayItem9_,
sum(PayItem10) as PayItem10_,
sum(PayItem11) as PayItem11_,
sum(PayItem12) as PayItem12_,
sum(PayItem13) as PayItem13_,
sum(PayItem14) as PayItem14_,
sum(PayItem15) as PayItem15_,
sum(PayItem16) as PayItem16_,
sum(PayItem17) as PayItem17_,
sum(PayItem18) as PayItem18_,
sum(PayItem19) as PayItem19_,
sum(PayItem20) as PayItem20_,
sum(PayItem21) as PayItem21_,
sum(PayItem22) as PayItem22_,
sum(PayItem23) as PayItem23_,
sum(PayItem24) as PayItem24_,
sum(PayItem25) as PayItem25_,
SUM(LeaveAllowance) LeaveAllowance_,
SUM(PensionEmployee) PensionEmployee_,
SUM(PAYE) PAYE_,
SUM(NetPay) NetPay_,
SUM(Gross) Gross_,
OName,
PayItemOF1,
PayItemOF2,
PayItemOF3,
PayItemOF4,
PayItemOF5,
PayItemOF6,
PayItemOF7,
PayItemOF8,
PayItemOF9,
PayItemOF10,
PayItemOF11,
PayItemOF12,
PayItemOF13,
PayItemOF14,
PayItemOF15,
PayItemOF16,
PayItemOF17,
PayItemOF18,
PayItemOF19,
PayItemOF20,
PayItemOF21,
PayItemOF22,
PayItemOF23,
PayItemOF24,
PayItemOF25
FROM [dbo].[PayInfo_Monthly]
WHERE Status = 'A'
AND OName='" . $selectOption . "'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "'
GROUP BY [OName],[Pay Month],PayItemOF1,
PayItemOF2,
PayItemOF3,
PayItemOF4,
PayItemOF5,
PayItemOF6,
PayItemOF7,
PayItemOF8,
PayItemOF9,
PayItemOF10,
PayItemOF11,
PayItemOF12,
PayItemOF13,
PayItemOF14,
PayItemOF15,
PayItemOF16,
PayItemOF17,
PayItemOF18,
PayItemOF19,
PayItemOF20,
PayItemOF21,
PayItemOF22,
PayItemOF23,
PayItemOF24,
PayItemOF25
ORDER BY [OName]");

} elseif ((isset($_REQUEST["S_RptDate"]) && $_REQUEST["S_RptDate"] != '') && (isset($_REQUEST["E_RptDate"])
    && $_REQUEST["E_RptDate"] != '')) {

    $from = $_REQUEST["S_RptDate"];

    $end = $_REQUEST["E_RptDate"];

    $dbOpen2 = ("SELECT
CONVERT(Varchar(11), [Pay Month], 106) AS [Pay Month],
sum(PayItem1) as PayItem1_,
sum(PayItem2) as PayItem2_,
sum(PayItem3) as PayItem3_,
sum(PayItem4) as PayItem4_,
sum(PayItem5) as PayItem5_,
sum(PayItem6) as PayItem6_,
sum(PayItem7) as PayItem7_,
sum(PayItem8) as PayItem8_,
sum(PayItem9) as PayItem9_,
sum(PayItem10) as PayItem10_,
sum(PayItem11) as PayItem11_,
sum(PayItem12) as PayItem12_,
sum(PayItem13) as PayItem13_,
sum(PayItem14) as PayItem14_,
sum(PayItem15) as PayItem15_,
sum(PayItem16) as PayItem16_,
sum(PayItem17) as PayItem17_,
sum(PayItem18) as PayItem18_,
sum(PayItem19) as PayItem19_,
sum(PayItem20) as PayItem20_,
sum(PayItem21) as PayItem21_,
sum(PayItem22) as PayItem22_,
sum(PayItem23) as PayItem23_,
sum(PayItem24) as PayItem24_,
sum(PayItem25) as PayItem25_,
SUM(LeaveAllowance) LeaveAllowance_,
SUM(PensionEmployee) PensionEmployee_,
SUM(PAYE) PAYE_,
SUM(NetPay) NetPay_,
SUM(Gross) Gross_,
OName,
PayItemOF1,
PayItemOF2,
PayItemOF3,
PayItemOF4,
PayItemOF5,
PayItemOF6,
PayItemOF7,
PayItemOF8,
PayItemOF9,
PayItemOF10,
PayItemOF11,
PayItemOF12,
PayItemOF13,
PayItemOF14,
PayItemOF15,
PayItemOF16,
PayItemOF17,
PayItemOF18,
PayItemOF19,
PayItemOF20,
PayItemOF21,
PayItemOF22,
PayItemOF23,
PayItemOF24,
PayItemOF25
FROM [dbo].[PayInfo_Monthly]
WHERE Status = 'A'
AND [Pay Month] BETWEEN '" . $from . "' and '" . $end . "'
GROUP BY [OName],[Pay Month],PayItemOF1,
PayItemOF2,
PayItemOF3,
PayItemOF4,
PayItemOF5,
PayItemOF6,
PayItemOF7,
PayItemOF8,
PayItemOF9,
PayItemOF10,
PayItemOF11,
PayItemOF12,
PayItemOF13,
PayItemOF14,
PayItemOF15,
PayItemOF16,
PayItemOF17,
PayItemOF18,
PayItemOF19,
PayItemOF20,
PayItemOF21,
PayItemOF22,
PayItemOF23,
PayItemOF24,
PayItemOF25
ORDER BY [OName]");

}

?>

<body oncontextmenu="return false;"topmargin="0" leftmargin="0">
<form action="#" method="post" id="attend" class="form-inline">


<div class="form-group">
<label>Branch: </label>
										<select name="employee" id="employee" class="form-control">
											<?php
echo '<option value="--" selected="selected">All</option>';
$dbOpen3 = ("SELECT OName FROM [BrhMasters] where Status not in('D') ORDER BY OName");

include '../login/dbOpen3.php';

while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) { ?>
                                                <option value="<?php echo $row3['OName']; ?>" <?php if (isset($_POST['employee']) && $_POST['employee'] == $row3['OName']) {
    echo "selected";
}
    ?>><?php echo $row3['OName']; ?></option>
                                                <?php
}
include '../login/dbClose3.php';
?>
										</select>
									</div>

									<div class="form-group">
																		<label>From:</label>
                            <?php
if (isset($_REQUEST["S_RptDate"])) {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="' . ($_REQUEST["S_RptDate"]) . '" readonly/>';} else {echo '<input placeholder="State Date" name="S_RptDate" id="S_RptDate" type="text" class="form-control" value="" readonly/>';}
?>
																		</div>

																		<div class="form-group">
																		<label>To:</label>
                            <?php
if (isset($_REQUEST["E_RptDate"])) {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="' . $_REQUEST["E_RptDate"] . '" readonly />';} else {echo '<input placeholder="End Date" name="E_RptDate" id="E_RptDate" type="text" class="form-control"  value="" readonly />';}
?>
																							</div>

																		<input type="submit" value="Open" class="btn btn-success btn-sm" type="button" name="SubmitTrans" id="SubmitTrans" onclick=" save(); return false; "/>


											<!-- </div> -->
								<br/>
								<br/>

<?php
$QueryStr = "";
$SumVal_Leave = $SumVal_PensionEmployee = $SumVal_PAYE = $SumVal_NetPay = $SumVal_Gross = 0;

$strExp = "";include 'rpt_header.php';
//SIMON: REPLACE TABLE HEADERS AND FOOTERS AS YOU WANT THEM TO APPEAR IN THE REPORT

$dbOpen3 = "SELECT Top(1) * FROM [dbo].[payinfo]";
include '../login/dbOpen3.php';
while ($row3 = sqlsrv_fetch_array($result3, SQLSRV_FETCH_BOTH)) {
    $strExp .= "BRANCH, PAY MONTH";
    $QueryStr = '<th>BRANCH</th><th>PAY MONTH</th>';
    for ($i = 1; $i <= 25; $i++) {
        if ($row3['PayItemOF' . $i] == 1) {

            if(strtoupper($row3["PayItemNm" . $i])  === 'TRANSPORT'){

                $transport_no = $i;
                continue;

            }


            if(strtoupper($row3["PayItemNm" . $i])  === 'HOUSING'){

                $house_no = $i;
                continue;

            }

           if(strtoupper($row3["PayItemNm" . $i])  === 'OVERTIME'){

                $overtime_no = $i;
                continue;

             }

            $QueryStr = $QueryStr . "<th >" . strtoupper($row3["PayItemNm" . $i]) . "</th>";
            $strExp = $strExp . "," . strtoupper($row3["PayItemNm" . $i]);
        }
    }
}
include '../login/dbClose3.php';
$QueryStr = $QueryStr . "<th>PENSION</th><th>PAYE</th><th>NET PAY</th><th>GROSS</th>";
$strExp = $strExp . ",PENSION,PAYE,NET PAY,GROSS";

$PrintHTML = '<table width="100%" align="left" id="table" border="1" class="tablesorter" style="width:auto">
<thead><tr>' . $QueryStr . '</tr></thead>';

echo '<tfoot><tr ></tr></tfoot>
<tbody>';
//$PrintHTML="";
$Del = 0;
//SIMON: PUT REPORT QUERY HERE

$QueryStr = "";

include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $Del = $Del + 1;

    $PrintHTML .= '<td align="left" valign="middle" scope="col">' . $row2['OName'] . '</td>
				<td align="left" valign="middle" scope="col">' . $row2['Pay Month'] . '</td>';

    $strExp .= chr(13) . chr(10);
    $strExp .= trim($row2['OName']) . "," . $row2['Pay Month'];

    for ($i = 1; $i <= 25; $i++) {
        if ($row2['PayItemOF' . $i] == 1) {

            if($i === $transport_no){

                continue;
            }
            if($i === $house_no){

                 continue;
             } 

             if($i === $overtime_no){

                 continue;
             }

            $PrintHTML = $PrintHTML . '<td align="right" valign="middle" scope="col">' . number_format($row2['PayItem' . $i . '_'], 2) . '</td>';
            $strExp .= "," . ($row2['PayItem' . $i . '_']);
            if (!isset($SumVal[$i])) {$SumVal[$i] = 0;}
            $SumVal[$i] = $SumVal[$i] + $row2['PayItem' . $i . '_'];
        }
    }

    $strExp .= "," . $row2['LeaveAllowance_'] . "," . $row2['PensionEmployee_'] . "," . $row2['PAYE_'] . "," . $row2['NetPay_'] . "," . $row2['Gross_'];

    $PrintHTML = $PrintHTML . '

	<td align="right" valign="middle" scope="col">' . number_format($row2['PensionEmployee_'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['PAYE_'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['NetPay_'], 2) . '</td>
	<td align="right" valign="middle" scope="col">' . number_format($row2['Gross_'], 2) . '</td>
	</tr>';

    $SumVal_PensionEmployee = $SumVal_PensionEmployee + $row2['PensionEmployee_'];
    $SumVal_PAYE = $SumVal_PAYE + $row2['PAYE_'];
    $SumVal_NetPay = $SumVal_NetPay + $row2['NetPay_'];
    $SumVal_Gross = $SumVal_Gross + $row2['Gross_'];
}

$PrintHTML .= '</tbody><tfoot>';

//********************************
//GETTING THE FOOTER VALUES ADDED
//********************************

$dbOpen2 = "SELECT Top(1) * FROM [dbo].[payinfo]";
include '../login/dbOpen2.php';
while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
    $PrintHTML .= '<th></th><th></th>';
    $strExp .= chr(13) . chr(10);
    $strExp .= ",";

    for ($i = 1; $i <= 25; $i++) {
        if ($row2['PayItemOF' . $i] == 1) {

        if($i === $transport_no){
               continue; 
            }
        if($i === $house_no){
               continue; 
            }
        if($i === $overtime_no){
               continue; 
            }
            $PrintHTML .= "<th align='right'>" . number_format($SumVal[$i], 2) . "</th>";
            //$strExp .= ",".number_format($SumVal[$i],2);
            $strExp .= "," . ($SumVal[$i]);
        }
    }
}
include '../login/dbClose2.php';

$PrintHTML .= "<th align='right'>" . number_format($SumVal_PensionEmployee, 2) . "</th><th align='right'>" . number_format($SumVal_PAYE, 2) . "</th><th align='right'>" . number_format($SumVal_NetPay, 2) . "</th><th align='right'>" . number_format($SumVal_Gross, 2) . "</th><th></th><th></th></tfoot>";
$strExp .= "," . ($SumVal_PensionEmployee) . "," . ($SumVal_PAYE) . "," . ($SumVal_NetPay) . "," . ($SumVal_Gross) . ",,";
//$strExp .=",".number_format($SumVal_Leave,2).",".number_format($SumVal_PensionEmployee,2).",".number_format($SumVal_PAYE,2).",".number_format($SumVal_NetPay,2).",".number_format($SumVal_Gross,2).",,";

echo '</table>';
echo $PrintHTML;
include 'rpt_footer_min.php';
?>
</form>
<?php include 'rpt_footer.php';?>

<script>

        function save() {
            document.attend.submit();
          }

</script>
</body>
</html>
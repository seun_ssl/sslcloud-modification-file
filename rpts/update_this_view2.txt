USE [SSLCloud_0019]
GO

/****** Object:  View [dbo].[PayInfo_Monthly2]    Script Date: 8/30/2021 3:57:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[PayInfo_Monthly2]
AS
SELECT     dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName + ' [' + dbo.EmpTbl.EmpID + ']' AS [Full Name],dbo.EmpTbl.SName + ' ' + dbo.EmpTbl.FName + ' ' + dbo.EmpTbl.ONames  as [Full Name2],dbo.EmpTbl.EmpCategory as EmpCategory,dbo.EmpTbl.NIN,dbo.EmpTbl.HInsurNo,


   (SELECT     TOP (1) ItemPos
                            FROM          dbo.EmpDtl
                            WHERE      (ItemCode = 'Emp') AND (StaffID = dbo.EmpTbl.HashKey)
                            ORDER BY ID DESC) AS Designation,dbo.EmpTbl.MStatus,dbo.EmpTbl.DOB,dbo.EmpTbl.EmpDt,

dbo.EmpTbl.Sex,dbo.EmpTbl.SalAcctNo,dbo.EmpTbl.SalBank,PR.PayItem1 AS BASIC, PR.PayItem2 AS HOUSING, PR.PayItem1, 
                      PS.PayItemNm1, PS.PayItemOF1, PR.PayItem2, PS.PayItemNm2, PS.PayItemOF2, PR.PayItem3, PS.PayItemNm3, PS.PayItemOF3, PR.PayItem4, PS.PayItemNm4, 
                      PS.PayItemOF4, PR.PayItem5, PS.PayItemNm5, PS.PayItemOF5, PR.PayItem3 AS TRANSPORT, PR.PensionEmployee, PR.PensionEmployer, PR.PAYE, PR.NetPay, 
                      PR.Gross, dbo.BrhMasters.OName, dbo.EmpTbl.Department, PR.PayItem4 AS OverTime, PR.PayItem5 AS Loan, PR.PayItem6, PS.PayItemNm6, PS.PayItemOF6, 
                      PR.PayItem7, PS.PayItemNm7, PS.PayItemOF7, PR.PayItem8, PS.PayItemNm8, PS.PayItemOF8, PR.PayItem9, PS.PayItemNm9, PS.PayItemOF9, PR.PayItem10, 
                      PS.PayItemNm10, PS.PayItemOF10, PR.PayItem11, PS.PayItemNm11, PS.PayItemOF11, PR.PayItem12, PS.PayItemNm12, PS.PayItemOF12, PR.PayItem13, 
                      PS.PayItemNm13, PS.PayItemOF13, PR.PayItem14, PS.PayItemNm14, PS.PayItemOF14, PR.PayItem15, PS.PayItemNm15, PS.PayItemOF15, PR.PayItem16, 
                      PS.PayItemNm16, PS.PayItemOF16, PR.PayItem17, PS.PayItemNm17, PS.PayItemOF17, PR.PayItem18, PS.PayItemNm18, PS.PayItemOF18, PR.PayItem19, 
                      PS.PayItemNm19, PS.PayItemOF19, PR.PayItem20, PS.PayItemNm20, PS.PayItemOF20, PR.PayItem21, PS.PayItemNm21, PS.PayItemOF21, PR.PayItem22, 
                      PS.PayItemNm22, PS.PayItemOF22, PR.PayItem23, PS.PayItemNm23, PS.PayItemOF23, PS.HashKey AS GrpName, PR.PayItem24, PS.PayItemNm24, 
                      PS.PayItemOF24, PR.PayItem25, PS.PayItemNm25, PS.PayItemOF25, PR.LeaveAllowance, PR.Status, PR.PayMonth AS [Pay Month],DATENAME(month,PR.PayMonth) as Mont,DATENAME(year,PR.PayMonth) as yea, dbo.EmpTbl.TaxState, 
                      dbo.EmpTbl.HashKey, PS.PayItemCode1, PS.PayItemCode2, PS.PayItemCode3, PS.PayItemCode4, PS.PayItemCode5, PS.PayItemCode6, PS.PayItemCode7, 
                      PS.PayItemCode8, PS.PayItemCode9, PS.PayItemCode10, PS.PayItemCode11, PS.PayItemCode12, PS.PayItemCode13, PS.PayItemCode14, PS.PayItemCode15, 
                      PS.PayItemCode16, PS.PayItemCode17, PS.PayItemCode18, PS.PayItemCode19, PS.PayItemCode20, PS.PayItemCode21, PS.PayItemCode22, PS.PayItemCode23, 
                      PS.PayItemCode24, PS.PayItemCode25, PS.PensionEmployeeCode, PS.PensionEmployerCode, PS.LeaveAllowanceCode, PS.PAYECode, PS.TTaxableCode, 
                      PS.NetPayCode, PS.PayItemCD1, PS.PayItemCD2, PS.PayItemCD3, PS.PayItemCD4, PS.PayItemCD5, PS.PayItemCD6, PS.PayItemCD7, PS.PayItemCD8, 
                      PS.PayItemCD9, PS.PayItemCD10, PS.PayItemCD11, PS.PayItemCD12, PS.PayItemCD13, PS.PayItemCD14, PS.PayItemCD15, PS.PayItemCD16, PS.PayItemCD17, 
                      PS.PayItemCD18, PS.PayItemCD19, PS.PayItemCD20, PS.PayItemCD21, PS.PayItemCD22, PS.PayItemCD23, PS.PayItemCD24, PS.PayItemCD25, PS.ShowPaySlip1, 
                      PS.ShowPaySlip2, PS.ShowPaySlip3, PS.ShowPaySlip4, PS.ShowPaySlip5, PS.ShowPaySlip6, PS.ShowPaySlip7, PS.ShowPaySlip8, PS.ShowPaySlip9, 
                      PS.ShowPaySlip10, PS.ShowPaySlip11, PS.ShowPaySlip12, PS.ShowPaySlip13, PS.ShowPaySlip14, PS.ShowPaySlip15, PS.ShowPaySlip16, PS.ShowPaySlip17, 
                      PS.ShowPaySlip18, PS.ShowPaySlip19, PS.ShowPaySlip20, PS.ShowPaySlip21, PS.ShowPaySlip22, PS.ShowPaySlip23, PS.ShowPaySlip24, PS.ShowPaySlip25, 
                      PR.LatenessAmount, PR.loanAmount, PR.AbsentAmount
FROM         dbo.Fin_PRIndvPay AS PR INNER JOIN
                      dbo.Fin_PRCore AS PC ON PR.EmpID = PC.EmpID INNER JOIN
                      dbo.Fin_PRSettings AS PS ON PC.Scheme = PS.HashKey INNER JOIN
                      dbo.EmpTbl ON PR.EmpID = dbo.EmpTbl.HashKey INNER JOIN
                      dbo.BrhMasters ON dbo.EmpTbl.BranchID = dbo.BrhMasters.HashKey
WHERE     (PR.Status = 'A') AND (PC.Status = 'A') AND (PS.Status = 'A')






GO



<?php
function dd($value='')
{
  var_dump($value);
  die;
}
function br()
{
  echo '<br>';
}
//To use query builder you must have declare $_conn property in the class
trait QueryBuilder {
  private static $_instance, $_results = null;
  public function runQuery($sql)
  {
    $result = sqlsrv_query($this->_conn, $sql);
    if( $result === false) {die(print_r( sqlsrv_errors(), true));}
    self::$_results = sqlsrv_fetch_object($result);
    return $this;
  }

  public function insert($table, $fields = [])
  {
    if(!empty($table) && count($fields)) {
      $name = '';
      $values = '';
      $sql = "INSERT INTO {$table} (";
      foreach ($fields as $key => $value) {
        $seperator = (!is_numeric($value)) ? "'" : '';
        $name .= $key.", ";
        $values .= $seperator.$value.$seperator.", ";
      }
      $name = rtrim($name, ', ').') ';
      $values = rtrim($values, ', ').') ';
      $sql .= $name.' VALUES ('.$values;
      return $this->runQuery($sql);
    }
    return false;
  }

  public function select($table, $fields = [], $params = [])
  {
    if(!empty($table)) {
      $sql = "SELECT ";
      if(count($fields)){
        $sql .= implode(', ', $fields);
      } else {
        $sql .= '*';
      }
      $sql .= ' FROM '.$table;
      if(count($params) === 3){
        $sql .= " WHERE {$params[0]} {$params[1]} '{$params[2]}'";
      }
      return $this->runQuery($sql);
    }
    return false;
  }

  public function update($table, $fields = [], $params = [])
  {
    if(!empty($table)) {
      $sql = "UPDATE {$table}";
      if(count($fields)){
        $sql .= " SET ";
        foreach ($fields as $key => $value) {
          $seperator = (!is_numeric($value)) ? "'" : '';
          $sql .= $key." = ".$seperator.$value.$seperator.", ";
        }
        $sql = rtrim($sql, ', ');
      } else {
        return false;
      }
      if(count($params)){
        $sql .= " WHERE ";
        foreach ($params as $key => $value) {
          $seperator = (!is_numeric($value)) ? "'" : '';
          $sql .= $key." = ".$seperator.$value.$seperator." AND ";
        }
        $sql = rtrim($sql, ' AND ');
      }
      return $this->runQuery($sql);
    }
    return false;
  }

  public function query($query)
  {
    $result = sqlsrv_query($this->_conn, $query, array(), array( "Scrollable" => 'static' ));
    if( $result === false)
    {
      die(print_r( sqlsrv_errors(), true));
    }
    return $result;
  }

  public function get()
  {
    return self::$_results;
  }
}
//Connect to parent Database
class ParentDB
{
  use QueryBuilder;
  private $_conn;
  const HOST = 'localhost';
  const USER = 'SSLCloud';
  const DATABASE = 'SSLCloud';
  const PASSWORD = 'ssl@123';
  private function __construct()
  {
    $connectionInfo = array( "Database"=>self::DATABASE,"UID"=>self::USER, "PWD"=>self::PASSWORD);
    return $this->_conn = sqlsrv_connect(self::HOST, $connectionInfo );
  }

  public static function getInstance()
  {
    if (!isset(self::$_instance)) {
      self::$_instance = new ParentDB();
    }
    return self::$_instance;
  }

  public function closeConnection()
  {
    self::$_instance = null;
    sqlsrv_close($this->_conn);
  }
}

//Connect to Child DB
class ChildDB
{
  use QueryBuilder;
  private $_conn, $_host, $_database, $_username, $_password = 'ssl@123';
  private function __construct($host, $username, $database)
  {
    $this->_host = $host;
    $this->_username = $username;
    $this->_database = $database;
    $connectionInfo = array( "Database"=>$this->_database,"UID"=>$this->_username, "PWD"=>$this->_password);
    return $this->_conn = sqlsrv_connect($this->_host, $connectionInfo );
  }

  public static function getInstance($host = '', $username = '', $database = '')
  {
    if (!isset(self::$_instance)) {
      self::$_instance = new ChildDB($host, $username, $database);
    }
    return self::$_instance;
  }

  public function closeConnection()
  {
    self::$_instance = null;
    sqlsrv_close($this->_conn);
  }
}

//Handles all system processes
class System {
  private $_db, $_childDB;
  public function __construct()
  {
    $this->_db = ParentDB::getInstance();
  }

  public function shouldLock($id)
  {
    $data = $this->_db->select('DbConnStr', ['Ulock'], ['CustID', '=', $id])->get();
    return ($data->Ulock === 1) ? true : false;
  }

  public function lockModule($id, $module)
  {
    $this->_db->update('CLicense', ['ModStatus' => 0], ['CID' => $id, 'CModule' => $module]);
  }

  public function connectToChild($id)
  {
    $data = $this->_db->select('DbConnStr', [], ['CustID', '=', $id])->get();
    $this->_childDB = ChildDB::getInstance($data->CUServer, $data->CUName, $data->CDbName);
    return $this;
  }

  public function closeChildConnectiion()
  {
    $this->_childDB->closeConnection();
    $this->_childDB = null;
  }

  public static function getModuleName($name)
  {
    switch ($name) {
        case 'LMS':
            return 'Leave Management';
            break;
        case 'PREF':
            return 'Prefernces';
            break;
        case 'ACCT':
            return 'Accounting';
            break;
        case 'HRM':
          return 'Human Resource Management';
        break;
        case 'BI':
            return 'Book Keeping';
            break;
        case 'DMS':
            return 'Document Management Module';
            break;
        case 'KPI':
            return 'Appraisal Management Module';
            break;
        case 'PAY':
            return 'Payroll Management Module';
            break;
        case 'TAM':
            return 'Training Assessment Module';
            break;
        default:
            break;
    }
  }

  public function getInterval($id)
  {
    return $this->_db->select('DbConnStr', ['Interval'], ['CustID', '=', $id])->get()->Interval;
  }

  public function setNextWarnDate($id, $module)
  {
    $data = $this->getInterval($id);
    if ($data) {
      $nextDate = $this->CheckNextWarnDate($id, $module, $data);
      ($nextDate) ? $this->saveNextWarnDate($id, $module, $nextDate) : 'NULL';
      return true;
    }
    return false;
  }

  public function CheckNextWarnDate($id, $module, $interval)
  {
    $data = $this->_db->runQuery("SELECT Convert(Varchar(11),CEDate,106) AS EndDate, Convert(Varchar(11),CWDate,106) as WarnDate FROM CLicense WHERE CID = '$id' AND CModule = '$module'")->get();
    if (isset($data->WarnDate)) {
      $endDate = $data->EndDate;
      $warnDate = $data->WarnDate;
      //next warn date
      $temp = date('Y-m-d', strtotime($warnDate. " + $interval days"));
      //if next date exceeds end date, set warn date to end date
      if (DateUtility::isGreater($temp, $endDate) || DateUtility::isEqual($temp, $endDate)) {
        $nextDate = $endDate;
      } else {
        $nextDate = $temp;
      }
      //return next date
      return $nextDate;
    }
    return false;
  }

  public function saveNextWarnDate($id, $module, $date)
  {
    $this->_db->update('CLicense', ['CWDate' => $date], ['CID' => $id, 'CModule' => $module]);
  }

  public function checkWarnRights($id)
  {
    $data = $this->_db->select('DbConnStr', [], ['CustID', '=', $id])->get();
    return (isset($data->Uwarn) && isset($data->Interval) && isset($data->UwarnDays)) ? true : false;
  }

  public function getCompanyName()
  {
    $data = $this->_childDB->select('settings', ['SetValue'], ['Setting', '=', 'CompName'])->get();
    return ucwords($data->SetValue);
  }

  public function getRecepientsDetails($EmpID) {
    $data = $this->_childDB->select('EmpTbl', ['SName', 'FName', 'Email'], ['EmpID', '=', $EmpID])->get();
    return $data;
  }

  public function getRecepients()
  {
    $recepients = [];
    $data = $this->_childDB->query("SELECT EmpID FROM Users WHERE Status= 'A' and GpName = (SELECT HashKey FROM UGpRights WHERE GpName = 'SysAdmin')");
    while($row = sqlsrv_fetch_object($data)) {
      $details = $this->getRecepientsDetails($row->EmpID);
      if ($details) {
        $recepients[$details->SName.' '.$details->FName] = $details->Email;
      }
    }
    return $recepients;
  }

  public function sendMail($msg, $recepients = [])
  {
    if (count($recepients)) {
      foreach ($recepients as $name => $email) {
        if ($email) {
          $todaysDate = new DateTime();
          $body = str_replace("#Name#",$name,$msg);
          $this->_childDB->insert('MailQueue', [
            'Subject' => 'License Expiry Notice For: '.$this->getCompanyName(),
            'Send_to' => $email,
            'body' => $body,
            'Status' => 'N',
            'AddedBy' => 'CronJob',
            'AddedDate' => $todaysDate->format('Y-m-d H:i:s'),
            'TrialCnt' => 0,
            'HashKey' => 'CronJob'
          ]);
        }
      }
    }
  }
}

//Compare dates
class DateUtility {
  public static function isPastToday($time)
  {
    return (new DateTime())->format('Y-m-d') > (new DateTime($time))->format('Y-m-d');
  }
  public static function isFutureToday($time)
  {
    return (new DateTime())->format('Y-m-d') < (new DateTime($time))->format('Y-m-d');
  }
  public static function isEqualToday($time)
  {
    return (new DateTime())->format('Y-m-d') === (new DateTime($time))->format('Y-m-d');
  }
  public static function isEqual($time, $time2)
  {
    return (new DateTime($time))->format('Y-m-d') === (new DateTime($time2))->format('Y-m-d');
  }
  //if time is greater than time2
  public static function isGreater($time, $time2)
  {
    return (new DateTime($time))->format('Y-m-d') > (new DateTime($time2))->format('Y-m-d');
  }
  //if time is less than time2
  public static function isLess($time, $time2)
  {
    return (new DateTime($time))->format('Y-m-d') < (new DateTime($time2))->format('Y-m-d');
  }
}

$db = ParentDB::getInstance();
$system = new System;

$data = $db->query("SELECT DATEDIFF(day, getdate(), license.CEDate) AS DaysRemaining, license.* FROM CLicense as license WHERE ModStatus = 1");
while($row = sqlsrv_fetch_object($data)){
  if ($row->DaysRemaining < 0) {
    if($system->shouldLock($row->CID)) {
      $system->lockModule($row->CID, $row->CModule);
    }
  }
}

//Send Warning Mail to hr -
$data = $db->query("SELECT Convert(Varchar(11),CEDate,106) AS EndDate, Convert(Varchar(11),CWDate,106) as WarnDate, DATEDIFF(day, CWDate, CEDate) AS DateDiff, * FROM CLicense  WHERE ModStatus = 1");
$companyList = [];
//Get all modules that warning is available for with its days left and cust id
while($row2 = sqlsrv_fetch_object($data)){
  if (isset($row2->CWDate)) {
    if ($system->checkWarnRights($row2->CID)) {
      if(DateUtility::isEqualToday($row2->WarnDate)) {
        if (array_key_exists($row2->CID, $companyList)) {
          $companyList[$row2->CID][$row2->CModule] = $row2->DateDiff;
        } else {
          $companyList[$row2->CID] = [$row2->CModule => $row2->DateDiff];
        }
      }
      //If warn date and end date is same, means this is the final mail send to null
      if (DateUtility::isEqual($row2->EndDate, $row2->WarnDate)) {
        //No more Reminders
        $db->runQuery("UPDATE CLicense SET CWDate = NULL WHERE CID = '$row2->CID' AND CModule = '$row2->CModule'");
      }
    }
  }
}
foreach ($companyList as $company => $content) {
  // Construct Message
  $msg = '';
  $msg = '<p style="margin-left:13px;">Dear #Name#, <br><br>';
  $msg .= 'License for the following module(s) will be expiring soon <br><br>';
  $msg .= '<strong>Product Description</strong><br><br>';
  foreach ($content as $module => $days) {
    $system->setNextWarnDate($company, $module);
    $daysLeft = '';
    if ($days > 0) {
      $suffix =($days > 1) ? 'days' : 'day';
      $daysLeft = $days.' '.$suffix;
    } else if ($days === 0) {
      $daysLeft = 'Today';
    }
    $msg .= 'Name: <strong>'.System::getModuleName($module)." ({$daysLeft}) </strong><br>";
    $msg .= 'Expiry Date: '.Date('jS F, Y', strtotime("+ $days days")) .'<br><br>';
  }
  $msg .= '<br>Contact Phone #: 01-761-2051-2<br>';
  $msg .= '<br>Automatically generated from SSLCloud HRM System.<br>';
  $msg .= '<br>Please ignore this alert if it was sent to you in error. Do not reply as such replies may not reach the vendor. <br/><br> Thank you.</p>';
  // Connect to child db - $company
  $system->connectToChild($company);
  $recepients = $system->getRecepients();
  $system->sendMail($msg, $recepients);
  $system->closeChildConnectiion($company);
}

$db->closeConnection();

exit;

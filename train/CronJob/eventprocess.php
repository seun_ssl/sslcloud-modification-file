<?php
date_default_timezone_set('Africa/Lagos');
$db_name = [];
$db_username = [];
$db_host = [];
$db_password = [];
$parentDBName = [];

$parent_serverName = "localhost";
$parent_database = "SSLCloud";
$parent_userName = "SSLCloud";
$parent_password = "ssl@123";
$child_password = "ssl@123";

$connectionInfo = array("Database" => "{$parent_database}", "UID" => "{$parent_userName}", "PWD" => "{$parent_password}");
$conn11 = sqlsrv_connect($parent_serverName, $connectionInfo);

if ($conn11) {
    // echo "Connection established.<br />";
} else {
    echo "Connection could not be established.<br />";
    die(print_r(sqlsrv_errors(), true));
}

$sql2 = "SELECT * FROM master.sys.databases";
$query23 = sqlsrv_query($conn11, $sql2);
while ($row22 = sqlsrv_fetch_array($query23, SQLSRV_FETCH_BOTH)) {
    //echo $query;
    $parentDBName[] = $row22['name'];
}

$sql = "SELECT DISTINCT CLicense.CID AS CustomerID, DbConnStr.CustName AS CustomerName, DbConnStr.CUServer AS HostName, DbConnStr.CDbName AS DBName,
DbConnStr.CUName AS DBUsername FROM CLicense LEFT JOIN DbConnStr ON CLicense.CID=DbConnStr.CustID WHERE DbConnStr.Status='A'";
$query = sqlsrv_query($conn11, $sql);
// var_dump($query);
while ($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_BOTH)) {
    //echo $query;
    $db_name[] = $row['DBName'];
    $db_username[] = $row['DBUsername'];
    $db_host[] = $row['HostName'];
    $db_password[] = $child_password; // child db password goes here with the assumption that all child db has same password

}

$dbOpen4 = "SELECT * from EmpTbl where Status in ('A','U','N') and EmpStatus='Active'";

function ScriptRunner($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => "{$child_password}");
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
        return ("{$row[$rwfield]}");
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunnercous($my_database, $my_user, $my_host, $query)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    return sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH);

    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function Scriptrunnerok($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
        // return ( isset($row[$rwfield]) ? $row[$rwfield] : 0);
        return ("{$row[$rwfield]}");
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunnerUD($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function QueueMail($my_database, $my_user, $my_host, $To, $Subj, $Bdy, $Atth, $Tm)
{

    $Recipient = $To;

    if (filter_var(trim($Recipient), FILTER_VALIDATE_EMAIL)) {
        /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
        $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
        $HashKey = md5($UniqueKey . $Recipient);
        $key = md5($Recipient);

        $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],
                [AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . trim($Recipient) . "','" . $Bdy . "','" . $Atth . "','N','" . $key . "',GETDATE(),0,'" . $HashKey . "')";
        ScriptRunnerUD($my_database, $my_user, $my_host, $Script_Mail, "QueueMail");
    }
}

function MyQueueMail($my_database, $my_user, $my_host, $To, $Subj, $Bdy, $Atth, $Tm)
{
    if (trim($Tm) == "") {$Tm = "GetDate()";} else { $Tm = "'" . $Tm . "'";}
    // var_dump($To);
    $Recipients = json_decode($To, true);

    // var_dump($Recipients);

    foreach ($Recipients as $Recipient) {

        if (filter_var(trim($Recipient, FILTER_VALIDATE_EMAIL))) {
            /* Create a HashKey of the Row ID  as identifier for each unique staff record*/
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = md5($UniqueKey . $Recipient);
            $key = md5($Recipient);

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],
                [AddedDate],[TrialCnt],[HashKey])
                VALUES('" . $Subj . "','" . trim($Recipient) . "','" . $Bdy . "','" . $Atth . "','N','" . $key . "',GETDATE(),0,'" . $HashKey . "')";
            ScriptRunnerUD($my_database, $my_user, $my_host, $Script_Mail, "QueueMail");
        }
    }
}

for ($i = 0; $i < count($db_name); $i++) {
    $my_host = $db_host[$i];
    $my_user = $db_username[$i];
    $my_database = $db_name[$i];
    $my_password = $child_password;

    $dbOpen2 = "SELECT *, Convert(varchar, sendDate, 23) sendDate_ FROM Notify WHERE [Status] = 'N'";

    $connectionInfo2 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);
    $conn2 = sqlsrv_connect($my_host, $connectionInfo2);
    $result2 = sqlsrv_query($conn2, $dbOpen2);

    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $id1 = $row2['ID'];
        $date = date('Y-m-d', strtotime(date('y-m-d')));
        $sendDate = date('Y-m-d', strtotime($row2['sendDate_']));
        $sendToAll = $row2['sendToAll'];

        if ($date == $sendDate) {
            var_dump('here');
            if ($sendToAll == 'on') {
                var_dump('here1');
                $connectionInfo4 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);

                $conn4 = sqlsrv_connect($my_host, $connectionInfo4);
                $result4 = sqlsrv_query($conn4, $dbOpen4);

                $title = $row2['title'];
                $message = $row2['body'];
                // var_dump($dbOpen4);
                // var_dump($result4);
                // var_dump(sqlsrv_fetch_array($result4, SQLSRV_FETCH_BOTH));
                while ($row4 = sqlsrv_fetch_array($result4, SQLSRV_FETCH_BOTH)) {
                    var_dump('here2');
                    $To = $row4['Email'];
                    QueueMail($my_database, $my_user, $my_host, $To, $title, $message, "", "");

                }

                sqlsrv_free_stmt($result4);
                sqlsrv_close($conn4);

                $interval = $row2['interval'];
                $updatedDate = $row2['sendDate'];

                // $newUpdatedDate = $row2['updatedDate'] + (24*60*60*$row2['interval']);
                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');
                // var_dump($row2['sendDate_']);
                // var_dump($convdate);

                // update status record to S = Sent
                $scriptUpdate = "UPDATE Notify SET [Status] = 'S', updatedDate = '$convdate'
            WHERE [ID] ='{$id1}' and sendDate = '" . $row2['sendDate_'] . "' ";
                // ScriptRunnerUD($scriptUpdate, "Inst");
                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, 'Inst');

                // var_dump('here');
            } else {

                // Send mail
                $sendTo = $row2['sendTo'];
                $arraySendTo = $sendTo;
                $title = $row2['title'];
                $message = $row2['body'];

                MyQueueMail($my_database, $my_user, $my_host, $arraySendTo, $title, $message, "", "");

                $interval = $row2['interval'];
                $updatedDate = $row2['sendDate'];

                // $newUpdatedDate = $row2['updatedDate'] + (24*60*60*$row2['interval']);
                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');
                // var_dump($row2['sendDate_']);
                // var_dump($convdate);
                // var_dump('here');

                // update status record to S = Sent
                $scriptUpdate = "UPDATE Notify SET [Status] = 'S', updatedDate = '$convdate'
             WHERE [ID] ='{$id1}' and sendDate = '" . $row2['sendDate_'] . "' ";
                // var_dump('here');

                // ScriptRunnerUD($scriptUpdate, "Inst");
                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, 'Inst');

            }
        }
    }

    sqlsrv_free_stmt($result2);

// Start of second business logic

    $dbOpen2 = "SELECT *, Convert(varchar, updatedDate, 23) updatedDate_ FROM Notify WHERE [Status] = 'S' ";

    $connectionInfo2 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);

    $conn2 = sqlsrv_connect($my_host, $connectionInfo2);
    $result2 = sqlsrv_query($conn2, $dbOpen2);

    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $id = $row2['ID'];
        $interval = $row2['interval'];
        $title = $row2['title'];
        $message = $row2['body'];
        // var_dump('here');
        // $scriptInterval = "SELECT Convert(varchar, eventDate, 23) eventDate_,
        // Convert(varchar, DATEADD(dd, $interval, '".$row2['updatedDate_']."' ), 23) dateModified
        // FROM Notify";
        $scriptInterval = "SELECT Convert(varchar, eventDate, 23) eventDate_,
    Convert(varchar, updatedDate, 23) dateModified
    FROM Notify where [ID] = '{$id}' ";

        $dateInterval = date('Y-m-d', strtotime(ScriptRunner($my_database, $my_user, $my_host, $scriptInterval, "dateModified")));
        $eventDate = date('Y-m-d', strtotime(ScriptRunner($my_database, $my_user, $my_host, $scriptInterval, "eventDate_")));
        $sendToAll = isset($row2['sendToAll']) ? $row2['sendToAll'] : 'off';

        // var_dump('dateModified');
        // var_dump($dateInterval);
        // var_dump($eventDate);
        // var_dump(date("Y-m-d"));
        // var_dump($scriptInterval);

        if ((date("Y-m-d") == $dateInterval) && ($dateInterval < $eventDate)) {

            // var_dump('dateModified');
            // var_dump('here2');
            // FIRE EMAIL TO ALL EMPLOYEES
            if ($sendToAll == 'on') {

                $connectionInfo4 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);

                $conn4 = sqlsrv_connect($my_host, $connectionInfo4);
                $result4 = sqlsrv_query($conn4, $dbOpen4);

                while ($row4 = sqlsrv_fetch_array($result4, SQLSRV_FETCH_BOTH)) {
                    $To = $row4['Email'];

                    QueueMail($my_database, $my_user, $my_host, $To, $title, $message, "", "");

                }

                sqlsrv_free_stmt($result4);
                sqlsrv_close($conn4);

                $interval = $row2['interval'];
                $updatedDate = $row2['updatedDate'];

                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');

                // update status record to S = Sent
                $scriptUpdate = "UPDATE Notify SET updatedDate = '$convdate'
            WHERE [ID] ='{$id}' and updatedDate = '" . $row2['updatedDate_'] . "' ";
                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, "Inst");

            } else {
                // FIRE EMAIL NOTIFICATION TO SELECTED EMPLOYEES
                // var_dump('Sent mail');
                $sendTo = $row2['sendTo'];
                // $arraySendTo=json_encode($sendTo);
                $arraySendTo = $sendTo;
                $title = $row2['title'];
                $message = $row2['body'];

                MyQueueMail($my_database, $my_user, $my_host, $arraySendTo, $title, $message, "", "");

                $interval = $row2['interval'];
                $updatedDate = $row2['updatedDate'];

                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');

                // var_dump($convdate);

                // $dateInterval = $convdate;

                // update status record to S = Sent

                $scriptUpdate = "UPDATE Notify SET updatedDate = '$convdate'
            WHERE [ID] ='{$id}' and updatedDate =  '" . $row2['updatedDate_'] . "'";

                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, "Inst");

            }
        }
    }
    sqlsrv_free_stmt($result2);

// start of third business logic
    $dbOpen2 = "SELECT *, Convert(varchar, eventDate, 23) eventDate_ FROM Notify WHERE [Status] = 'S' ";

    $connectionInfo2 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);

    $conn2 = sqlsrv_connect($my_host, $connectionInfo2);
    $result2 = sqlsrv_query($conn2, $dbOpen2);

    while ($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_BOTH)) {
        $id3 = $row2['ID'];
        $interval = $row2['interval'];

        $title = $row2['title'];
        $message = $row2['body'];

        $scriptInterval = "SELECT Convert(varchar, eventDate, 23) eventDate_
    FROM Notify where [ID] ='{$id3}' ";

        $eventDate = date('Y-m-d', strtotime(ScriptRunner($my_database, $my_user, $my_host, $scriptInterval, "eventDate_")));
        $sendToAll = isset($row2['sendToAll']) ? $row2['sendToAll'] : 'off';

        // var_dump('here3');
        // var_dump(date("Y-m-d") == $eventDate);
        // var_dump(date("Y-m-d"));
        // var_dump($eventDate);
        // var_dump('here');

        if ((date("Y-m-d") == $eventDate)) {
            // var_dump('here4');
            // var_dump('here1');

            // FIRE EMAIL TO ALL EMPLOYEES
            if ($sendToAll == 'on') {
                // var_dump('here2');

                $connectionInfo4 = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);

                $conn4 = sqlsrv_connect($my_host, $connectionInfo4);
                $result4 = sqlsrv_query($conn4, $dbOpen4);

                while ($row4 = sqlsrv_fetch_array($result4, SQLSRV_FETCH_BOTH)) {
                    $To = $row4['Email'];
                    // var_dump('here3');

                    QueueMail($my_database, $my_user, $my_host, $To, $title, $message, "", "");

                }

                sqlsrv_free_stmt($result4);
                sqlsrv_close($conn4);

                $interval = $row2['interval'];
                $updatedDate = $row2['updatedDate'];

                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');

                // update status record to S = Sent
                $scriptUpdate = "UPDATE Notify SET updatedDate = '$convdate'
            WHERE [ID] ='{$id3}' and eventDate = '" . $row2['eventDate_'] . "' ";

                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, "Inst");

            } else {
                // FIRE EMAIL NOTIFICATION TO SELECTED EMPLOYEES
                var_dump('Sent mail');
                $sendTo = $row2['sendTo'];
                // $arraySendTo=json_encode($sendTo);
                $arraySendTo = $sendTo;
                $title = $row2['title'];
                $message = $row2['body'];

                MyQueueMail($my_database, $my_user, $my_host, $arraySendTo, $title, $message, "", "");

                $interval = $row2['interval'];
                $updatedDate = $row2['updatedDate'];

                $newUpdatedDate = $updatedDate->modify("+$interval day");
                $convdate = $newUpdatedDate->format('Y-m-d');

                // update status record to S = Sent

                $scriptUpdate = "UPDATE Notify SET updatedDate = '$convdate'
            WHERE [ID] ='{$id3}' and eventDate = '" . $row2['eventDate_'] . "' ";

                ScriptRunnerUD($my_database, $my_user, $my_host, $scriptUpdate, "Inst");

            }
        }
    }
    sqlsrv_free_stmt($result2);
    sqlsrv_close($conn2);

// sqlsrv_close( $conn2 );
} // end for loop

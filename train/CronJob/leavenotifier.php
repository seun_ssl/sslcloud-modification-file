<?php
$db_name = [];
$db_username = [];
$db_host = [];
$db_password = [];
$parentDBName = [];

$parent_serverName = "localhost";
$parent_database = "SSLCloud";
$parent_userName = "SSLCloud";
$parent_password = "ssl@123";
$child_password = "ssl@123";

$connectionInfo = array("Database" => "{$parent_database}", "UID" => "{$parent_userName}", "PWD" => "{$parent_password}");
$conn11 = sqlsrv_connect($parent_serverName, $connectionInfo);

if ($conn11) {
    // echo "Connection established.<br />";
} else {
    echo "Connection could not be established.<br />";
    die(print_r(sqlsrv_errors(), true));
}

$sql2 = "SELECT * FROM master.sys.databases";
$query23 = sqlsrv_query($conn11, $sql2);
while ($row22 = sqlsrv_fetch_array($query23, SQLSRV_FETCH_BOTH)) {
    //echo $query;
    $parentDBName[] = $row22['name'];
}

$sql = "SELECT DISTINCT CLicense.CID AS CustomerID, DbConnStr.CustName AS CustomerName, DbConnStr.CUServer AS HostName, DbConnStr.CDbName AS DBName,
DbConnStr.CUName AS DBUsername FROM CLicense LEFT JOIN DbConnStr ON CLicense.CID=DbConnStr.CustID WHERE DbConnStr.Status='A'";
// var_dump($sql);
$query = sqlsrv_query($conn11, $sql);
while ($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_BOTH)) {
    //echo $query;
    $db_name[] = $row['DBName'];
    $db_username[] = $row['DBUsername'];
    $db_host[] = $row['HostName'];
    $db_password[] = $child_password; // child db password goes here with the assumption that all child db has same password
}

// var_dump($db_name);
// die();

function ScriptRunner($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => "{$child_password}");
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
        return ("{$row[$rwfield]}");
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunnercous($my_database, $my_user, $my_host, $query)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    return sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH);

    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function Scriptrunnerok($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    //  var_dump($my_database,$my_user,$my_host);
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH)) {
        // return ( isset($row[$rwfield]) ? $row[$rwfield] : 0);
        return ("{$row[$rwfield]}");
    }
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

function ScriptRunnerUD($my_database, $my_user, $my_host, $query, $rwfield)
{
    global $child_password;
    $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $child_password);
    $conn = sqlsrv_connect($my_host, $connectionInfo);
    if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
    $result = sqlsrv_query($conn, $query);
    if ($result === false) {die(print_r(sqlsrv_errors(), true));}
    sqlsrv_free_stmt($result);
    sqlsrv_close($conn);
}

for ($i = 0; $i < count($db_name); $i++) {
    $my_host = $db_host[$i];
    $my_user = $db_username[$i];
    $my_database = $db_name[$i];
    $my_password = $child_password;

// var_dump( $my_database);

    $script = "Select SetValue17 from Settings where Setting='HRSignedDoc'";
// var_dump($script);
    $headHr = Scriptrunner($my_database, $my_user, $my_host, $script, 'SetValue17');
    if ($headHr) {
        // var_dump($headHr);
        $scriptDetails = "SELECT SName + ' ' + FName + ' ' + ONames AS [FullName], Email
        FROM EmpTbl WHERE HashKey = '" . $headHr . "' ";

        $HRName = ScriptRunner($my_database, $my_user, $my_host, $scriptDetails, "FullName");
        $To = ScriptRunner($my_database, $my_user, $my_host, $scriptDetails, "Email");
        // var_dump($HRName);
        $date = date('Y-m-d', strtotime("+1 day"));
        // $date2 = $date->format('Y-m-d');

        $query = "Select * from [LvDetails] where [Status]='A' and [EDate] ='" . $date . "'";
        // var_dump($query);
        // $ress = ScriptRunnercous($my_database, $my_user, $my_host, $script);
        $connectionInfo = array("Database" => $my_database, "UID" => $my_user, "PWD" => $my_password);
        $conn = sqlsrv_connect($my_host, $connectionInfo);
        if ($conn === false) {die(print_r(sqlsrv_errors(), true));}
        $result = sqlsrv_query($conn, $query);
        if ($result === false) {die(print_r(sqlsrv_errors(), true));}
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {

            // var_dump($row);

            $scriptEmp = "SELECT SName + ' ' + FName + ' ' + ONames AS [FullName], Email
            FROM EmpTbl WHERE HashKey = '" . $row['EmpID'] . "' ";
            $empName = ScriptRunner($my_database, $my_user, $my_host, $scriptEmp, "FullName");

            $scriptEmp = "SELECT  [LType]
            FROM [LvSettings] WHERE HashKey = '" . $row['LID'] . "' ";
            $leavetype = ScriptRunner($my_database, $my_user, $my_host, $scriptEmp, "LType");
            // var_dump($leavetype);
            $msg = "Dear #Name#,<br /> This is just soft reminder that employee #EmpName#(on #leaveType#) will be resuming from his/her leave tommorow($date).";

            // $msg = ScriptRunner($Script_Mail, "MMsg");
            $Subj = "Leave Resumption Reminder";
            $msg = str_replace('#Name#', $HRName, $msg);
            $msg = str_replace('#EmpName#', $empName, $msg);
            $msg = str_replace('#leaveType#', $leavetype, $msg);
            $UniqueKey = date('l jS \of F Y h:i:s A') . gettimeofday(true) . "MailQueue";
            $HashKey = 'Cronjob';
            $Atth = "";
            $Tm = "GetDate()";

            $Script_Mail = "INSERT INTO [MailQueue]([Subject],[Send_to],[Body],[Attachment],[Status],[AddedBy],[AddedDate],[TrialCnt],[HashKey])
				VALUES('" . $Subj . "','" . $To . "','" . $msg . "','" . $Atth . "','N','Cronjob'," . $Tm . ",0,'" . $HashKey . "')";
            ScriptRunnerUD($my_database, $my_user, $my_host, $Script_Mail, '');

            // var_dump($empName);
        }
        sqlsrv_free_stmt($result);
        sqlsrv_close($conn);

    }

}
